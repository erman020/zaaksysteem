#!/usr/bin/env perl

use strict;
use warnings;

use Moose;
use Data::Dumper;

use Text::CSV;
use Unicode::String;
use utf8;

use FindBin;
use lib "$FindBin::Bin/../lib";

use Catalyst qw/
    ConfigLoader
/;

use Catalyst::Log;
use Catalyst::Model::DBIC::Schema;
use Zaaksysteem::Zaken;
use Zaaksysteem::Constants;

### Start logging
my $log         = Catalyst::Log->new();


error("USAGE: $0 [dsn] [user] [password] [commit]") unless @ARGV && scalar @ARGV >= 3;

my ($dsn, $user, $password, $commit, $table) = @ARGV;
my $dbic = database($dsn); #, $user, $password);

$dbic->txn_do(sub {
    eval {
        do_import($dbic);

        die "rollback" unless $commit;
    };

    if ($@) {
        die("Error: " . $@);
    }
});



sub database {
    my ($dsn, $user, $password) = @_;

    Catalyst::Model::DBIC::Schema->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => {
            dsn             => $dsn,
            pg_enable_utf8  => 1,
            user            => $user,
            password        => $password
        }
    );

    my $db = Catalyst::Model::DBIC::Schema->new();
    my $dbic = $db->schema;

    $log->info('Connection to DB established');
    return $dbic;
}


sub error {
    my ($msg) = @_;

    $log->error($msg);
    $log->_flush;

    exit;
}


sub do_import {
    my ($dbic) = @_;

    my $resultset = $dbic->resultset("ZaaktypeNotificatie")->search({
        'zaaktype_node_id.deleted' => undef,
        'zaaktype_id.deleted' => undef,
    }, {
        join => {'zaaktype_node_id' => {'zaaktype_id' => 'bibliotheek_categorie_id'} }
    });

    while(my $zaaktype_notificatie = $resultset->next()) {
        $zaaktype_notificatie->import_bibliotheek_notificatie();
    }
}







__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

