# `/frontend`

> This is the legacy part of the Zaaksyteem front-end.

## Development activities

- fix bugs
- improve build time
- remove parts that have been migrated

## Build

Dependencies are installed with:

- `bower`
- `npm`

The main build is done with `gulp`, assets are build on the fly
to `/root/(css|html|js)`

### Known issues

Production mode CSS is build to both `[basename].css` and `[basename].min.css`,
development mode only builds `[basename].css` (because minification is slow).

CSS links in the Perl Template Toolkit files *should* reference 
the unminified files, but in some cases they don't, so you won't 
see your changes until you run a production rebuild.

## Internal dependencies

`/frontend` depends on application code from:

- `/client`
    - This code is build with `webpack`;
      there is no development mode for this.
      Cf. *Cross dependencies* in the `/client` `README` 
    - Note that `/client` also depends on `/frontend`.
- `/root/tpl/zaak_v1/nl_NL/js`
    - This includes static (i.e. under version control) vendor libraries, e.g. 
        - jQuery
        - jQuery UI
        - lots of jQuery plugins
        - highcharts
        - global `loadWebform` function (webform.js)
    - you can find the complete list in the `config` object in the `gulpfile`
  