import angular from 'angular';
import 'lodash';
import 'jquery';
import 'jquery-migrate';
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/effect';
import 'jquery-ui/ui/position';
import 'jquery-ui/ui/button';
import 'jquery-ui/ui/draggable';
import 'jquery-ui/ui/droppable';
import 'jquery-ui/ui/resizable';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/selectmenu';
import 'jquery-ui/ui/datepicker';
import 'jquery-ui/ui/sortable';
import 'jquery-ui/ui/accordion';
import 'jquery-ui/ui/tabs';
import 'jquery-ui/ui/autocomplete';
import 'jquery-ui/ui/dialog';
import resourceModule from '../client/src/shared/api/resource';
import zsResourceConfiguration from '../client/src/shared/api/resource/resourceReducer/zsResourceConfiguration';
import zsInternModule from '../client/src/intern/zsIntern/zsViewlessIntern/index.js';
import zsTruncateModule from '../client/src/shared/ui/zsTruncate';
import zsTooltipModule from '../client/src/shared/ui/zsTooltip';
import zsSnackbarModule from '../client/src/shared/ui/zsSnackbar';
import createCaseRegistrationModule from '../client/src/shared/case/createCaseRegistration';
import createContactModule from '../client/src/shared/contact/createContact';
import createContactMomentModule from '../client/src/intern/views/root/createContactMoment';
import contextualActionServiceModule from '../client/src/shared/ui/zsContextualActionMenu/contextualActionService';
import viewTitleModule from '../client/src/shared/util/route/viewTitle/index.js';
import zsVormTemplateModifierModule from '../client/src/shared/zs/vorm/zsVormTemplateModifier';
import caseStatusLabelFilterModule from '../client/src/shared/case/caseStatusLabelFilter';
import caseStatusIconModule from '../client/src/shared/case/zsCaseStatusIcon';
import sessionServiceModule from '../client/src/shared/user/sessionService';
import zsStorageModule from '../client/src/shared/util/zsStorage';
import assign from 'lodash/object/assign';
import difference from 'lodash/array/difference';

window.angular.module('zs-new', [
    resourceModule,
    zsTruncateModule,
    zsTooltipModule,
    zsInternModule,
    zsSnackbarModule,
    createCaseRegistrationModule,
    createContactModule,
    createContactMomentModule,
    contextualActionServiceModule,
    viewTitleModule,
    zsVormTemplateModifierModule,
    caseStatusIconModule,
    caseStatusLabelFilterModule,
    sessionServiceModule,
    zsStorageModule
])
    .constant('HOME', '/')
    .run([
        '$rootScope', 'contextualActionService', 'composedReducer', 'sessionService', 'zsStorage',
        ( $rootScope, contextualActionService, composedReducer, sessionService, zsStorage ) => {
            let currentLocation = window.location.pathname;

            if (currentLocation.indexOf('/auth/') === 0) {
                zsStorage.clear();
            }

            if (currentLocation.indexOf('/auth/') !== 0 && currentLocation.indexOf('/pip') !== 0 && currentLocation.indexOf('/form') !== 0) {
                let permissions,
                    userResource = sessionService.createResource($rootScope, { cache: { every: 15 * 60 * 1000 } }),
                    getPermissions;

                let includesRoles = ( session, roles ) => {
                    return difference(roles, session.instance.logged_in_user.system_roles).length < roles.length;
                };

                permissions = composedReducer({ scope: $rootScope }, userResource)
                    .reduce(( user ) => {

                        return {
                            contact: includesRoles(user, ['Administrator', 'Zaaksysteembeheerder', 'Contactbeheerder']),
                            contactmoment: includesRoles(user, ['Administrator', 'Zaaksysteembeheerder', 'Contactbeheerder']),
                            case: true
                        };
                    });

                getPermissions = permissions.data;

                let actions = [
                    {
                        name: 'zaak',
                        label: 'Zaak aanmaken',
                        iconClass: 'folder-outline',
                        template:
                            `<create-case-registration
                                     on-close="close($promise)"
                                     casetype-id="casetypeId"
                                     data-file="params.file"
                                     data-requestor="params.requestor"
                                     data-file="params.file"
                                 ></create-case-registration>`,
                        params: {
                            casetypeId: null
                        }
                    },
                    {
                        name: 'contact',
                        label: 'Contact aanmaken',
                        iconClass: 'account-plus',
                        template: '<create-contact on-close="close($promise)"></create-contact>',
                        when: ['viewController', () => !!getPermissions().contact]
                    },
                    {
                        name: 'contact-moment',
                        label: 'Contactmoment toevoegen',
                        iconClass: 'comment-plus-outline',
                        template:
                            `<create-contact-moment
                                     on-submit="close($promise)"
                                     data-subject="params.subject"
                                     case-id="params.caseId"
                                 ></create-contact-moment>`
                    }
                ]
                    .map(action => {

                        return assign({
                            click: () => {
                                contextualActionService.openAction(action);
                            }
                        }, action);
                    });

                contextualActionService.getAvailableActions = () => actions;
            }

        }])
    .run([
        '$window', '$rootScope', '$document', 'viewTitle',
        ( $window, $rootScope, $document, viewTitle ) => {
            let title = {};

            let getLegacyTitle = () => {
                let titleEl = $document[0].querySelector('.block-header-title'),
                    fallbackEl = $document.find('title');

                title.mainTitle = titleEl ? titleEl.textContent : fallbackEl[0].textContent;

                if (!title) {
                    title.mainTitle = 'Zaaksysteem';
                }

                if ($window.location.href.indexOf('/beheer') !== -1) {
                    title.mainTitle = 'Beheer';
                }

            };

            viewTitle.get = () => title;
            $rootScope.$watch(getLegacyTitle);
        }])
    .config(['resourceProvider', ( resourceProvider ) => {
        zsResourceConfiguration(resourceProvider.configure);
    }]);

window.angular = angular;
// window._ = _;
