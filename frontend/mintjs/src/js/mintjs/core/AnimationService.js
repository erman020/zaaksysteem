/*global angular,setTimeout,clearTimeout*/
(function ( ) {
	
	angular.module('MintJS.core')
		.factory('animationService', [ '$document', function ( $document ) {
			
			var animationService = {},
				transitionEnd;
				
			transitionEnd = (function ( document ) {
				
				var el = document.createElement('div'),
					transEndEventNames = {
						'WebkitTransition': 'transitionend webkitTransitionEnd',
						'MozTransition': 'transitionend',
						'OTransition': 'oTransitionEnd otransitionend',
						'transition': 'transitionend'
					},
					key;
					
				for(key in transEndEventNames){
					if(el.style[key] !== undefined) {
						return transEndEventNames[key];
					}
				}
				
				return undefined;
				
			})($document[0]);
			
			if(transitionEnd) {
				transitionEnd = 'transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd';
			}
			
			animationService.onEnd = function ( element, callback ) {
				var timeoutId,
					isTransitionRunning,
					cancel;
				
				cancel = function ( ) {
					element.unbind(transitionEnd, onEnd);
					if(timeoutId) {
						clearTimeout(timeoutId);
					}
				};
				
				function onEnd ( event ) {
					callback(event);
				}
					
				timeoutId = setTimeout(function ( ) {
					timeoutId = undefined;
					if(!isTransitionRunning) {
						cancel();
					}
				}, 0);
				
				if(transitionEnd) {
					isTransitionRunning = true;
					element.bind(transitionEnd, onEnd);
				}
				
				return cancel;
			};
			
			return animationService;
			
		}]);
	
})();