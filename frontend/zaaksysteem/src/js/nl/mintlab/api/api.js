/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.api')
		.factory('zsApi', [ '$http', '$q', function ( $http, $q ) {
			
			var api = {};
			
			function resolve ( deferred, response, options ) {
				if(response.result.type === 'set') {
					deferred.resolve({
						data: response.result.instance.rows,
						pager: createPager(options, response.result.instance.pager)
					});
				} else {
					deferred.resolve({
						data: [ response.result ],
						pager: null
					});
				}	
			}
			
			function err ( deferred, response ) {
				var error = response;
				
				if(response.result && response.result.instance) {
					error = response.result;
				}
				
				deferred.reject(error);
			}
			
			function createPager ( options, pager ) {
				
				var pg = {};
				
				pg.hasNext = function ( ) {
					return !!pager.next;
				};
				
				pg.hasPrev = function ( ) {
					return !!pager.prev;
				};
				
				pg.next = function ( ) {
					return api.get(_.extend({}, options, { method: 'GET', url: pager.next } ));
				};
				
				pg.prev = function ( ) {
					return api.get(_.extend({}, options, { method: 'GET', url: pager.prev } ));
				};
				
				return pg;
				
			}
			
			api.post = function ( options ) {
					
				var deferred = $q.defer();

				$http(_.extend({}, options, { method: 'POST' }))
					.success(function ( response ) {
						
						resolve(deferred, response, options);
						
					})
					.error(function ( response ) {
						err(deferred, response);
					});
				
				return deferred.promise;
				
			};
			
			api.get = function ( options ) {
					
				var deferred = $q.defer();
				
				$http(_.extend({}, options, { method: 'GET' }))
					.success(function ( response ) {
						resolve(deferred, response, options);
					})
					.error(function ( response ) {
						resolve(deferred, response);
					});
				
				return deferred.promise;
				
			};
			
			api.getLegacyFormValidations = function ( fields, error ) {
				return _(fields)
					.filter(function ( field ) {
						return error.instance[field.name] !== undefined && error.instance[field.name].validity !== 'valid';
					})
					.map(function ( field ) {
						return {
							result: error.instance[field.name].validity,
							message: error.instance[field.name].message,
							parameter: field.name
						};
					})
					.value();
			};
			
			return api;
			
		}]);
	
})();
