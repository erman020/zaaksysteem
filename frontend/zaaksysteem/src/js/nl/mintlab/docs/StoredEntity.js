/*global fetch, define*/
(function ( ) {

	window.zsDefine('nl.mintlab.docs.StoredEntity', function ( ) {
		
		var inherit = window.zsFetch('nl.mintlab.utils.object.inherit'),
			clone = window.zsFetch('nl.mintlab.utils.object.clone'),
			EventDispatcher = window.zsFetch('nl.mintlab.events.EventDispatcher'),
			generateUid = window.zsFetch('nl.mintlab.utils.generateUid');
		
		function StoredEntity ( params ) {
			
			var key;
			
			this.name = 'Unknown';
			this.id = -1; // unsaved
			this._parent = null;
			this._depth = 0; // top level
			this._selected = false;
			
			this._uid = generateUid(this.getEntityType() + '_');
			
			for(key in params) {
				this[key] = params[key];
			}
		}
		
		inherit(StoredEntity, EventDispatcher);
		
		StoredEntity.prototype.getPath = function ( ) {
			var path = [ this.getUid() ],
				parent = this.getParent();
			
			while(parent) {
				path.unshift(parent.getUid());
				parent = parent.getParent();
			}
			path.shift();
			return path;
		};
		
		StoredEntity.prototype.getParent = function ( ) {
			return this._parent;
		};
		
		StoredEntity.prototype.setParent = function ( parent ) {
			this._parent = parent;
		};
		
		StoredEntity.prototype.getDepth = function ( ) {
			return this._depth;
		};
		
		StoredEntity.prototype.setDepth = function ( depth ) {
			this._depth = depth;
		};
		
		StoredEntity.prototype.getEntityType = function ( ) {
			return this._entityType;
		};
		
		StoredEntity.prototype.getSelected = function ( ) {
			return this._selected;
		};
		
		StoredEntity.prototype.setSelected = function ( selected ) {
			this._selected = selected;
		};
		
		StoredEntity.prototype.getUid = function ( ) {
			var uid;
			if(this.uuid !== undefined) {
				uid = this.getEntityType() + '_' + this.uuid;
			} else {
				uid = this._uid;
			}
			return uid;
		};
		
		StoredEntity.prototype.getMimeType = function ( ) {
			var mimetype = "Unknown";
			if(this.filestore_id && this.filestore_id.mimetype) {
				mimetype = this.filestore_id.mimetype.replace('application/', '').replace('image/', '');
			}
			return mimetype;
		};
		
		StoredEntity.prototype.updateWith = function ( data ) {
			for(var key in data) {
				// case_documents are ids only when retrieving data from the server
				// so we'll just ignore it here
				if(key !== 'case_documents') {
					this[key] = data[key];	
				}
			}
		};
		
		StoredEntity.prototype.clone = function ( ) {
			return clone(this, true);
		};
		
		
		return StoredEntity;
	});
})();
