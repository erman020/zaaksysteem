/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.TemplateController', [ '$scope', 'smartHttp', 'translationService', '$timeout', 'caseDocumentService', 'systemMessageService', function ( $scope, smartHttp, translationService, $timeout, caseDocumentService, systemMessageService ) {

			var defaultTemplateId;
			
			$scope.setTemplateId = function ( tplId ) {
				var i,
					l,
					tpl,
					templates = $scope.templates;
				
				for(i = 0, l = templates.length; i < l; ++i) {
					tpl = templates[i];
					if(tpl.id === tplId) {
						$scope.template = tpl;
						return;
					}
				}
				
				defaultTemplateId = tplId;
				
			};
			
			$scope.$watch('templates', function ( nw/*, old*/ ) {
				if(nw && nw.length) {
					if(!defaultTemplateId) {
						$scope.template = $scope.templates[0];
					} else {
						$scope.setTemplateId(defaultTemplateId);
					}
				}
			});
			
			$scope.$watch('template', function ( nw, old ) {
				var template = $scope.template,
					name = $scope.name;
					
				if(!name || old && old.bibliotheek_sjablonen_id.naam === name) {
					if(!template) {
						name = '';
					} else {
						name = template.bibliotheek_sjablonen_id.naam;
					}
					$scope.name = name;
				}
				
				$scope.targetFormat = $scope.template && $scope.template.target_format ? $scope.template.target_format : 'odt';
				
			});

			$scope.actions = [
				{
					name: 'submit',
					type: 'primary',
					label: 'Aanmaken'
				}
			];
			
			
		}]);
	
})();
