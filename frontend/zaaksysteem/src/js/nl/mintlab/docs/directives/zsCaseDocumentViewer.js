/*global angular, _*/
(function ( ) {
	angular.module('Zaaksysteem.docs')
		.directive('zsCaseDocumentViewer', [ '$http', function ($http) {

			var useFlexpaper = false;
			$http.get('/api/config/use_flexpaper')
				.success(function(data) {
					useFlexpaper = _.filter(data.result, { use_flexpaper: true }).length > 0;
				});

			return {
				restrict: 'E',
				scope: {
					'caseId': '&',
					'fileId': '&',
					'extension': '&',
					'isIntake': '&'
				},
				template: '<a class="download" href="{{caseDocumentViewer.getDownloadUrl()}}" target="_blank" rel="noopener"></a><iframe id="document-viewer" data-ng-src="{{caseDocumentViewer.getUrl()}}" width="400" height="300" allowfullscreen webkitallowfullscreen></iframe>',
				controller: [ '$scope', '$element', '$document', function ( $scope, $element, $document ) {
					var ctrl = this,
						iframe = $document[0].getElementById('document-viewer');
					
					iframe.addEventListener('load', function() { // this loadevent is needed so we can inject css.
						var iframeDoc = iframe.contentWindow.document,
							link = iframeDoc.createElement('link');
						link.rel = 'stylesheet';
						link.type = 'text/css';
						link.href = '/css/pdfjs-custom.css';
						iframeDoc.getElementsByTagName('head')[0].appendChild(link);
					});

					ctrl.getDownloadUrl = function ( ) {
						var url;

						if($scope.isIntake()) {
							url = '/zaak/intake/' + $scope.fileId() + '/download';
						} else {
							url = '/zaak/' + $scope.caseId() + '/document/' + $scope.fileId() + '/download';
						}

						return url;
					};

					ctrl.getUrl = function ( ) {

						var url,
							suffix = '/pdf';

						if($scope.extension() === '.pdf') {
							suffix = '?inline=1';
						}	

						if (useFlexpaper) {
							if($scope.isIntake()) {
								url = '/file/preview/' + $scope.fileId();
							} else {
								url = '/zaak/' + $scope.caseId() + '/flexpaper/' + $scope.fileId();
							}
						} else {
							url = '/pdf.js-with-viewer/web/viewer.html?file=' + ctrl.getDownloadUrl() + suffix;
						}

						return url;
					};

					return ctrl;
				}],
				controllerAs: 'caseDocumentViewer'
			};
		}]);
})();
