/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.instances')
		.directive('zsInstancesCrud', [ '$q', 'instancesService', 'systemMessageService', 'translationService', 'dateFilter', function ( $q, instancesService, systemMessageService, translationService, dateFilter ) {

			var otapLabels = 
				_(instancesService.getOriginOptions())
					.indexBy('value')
					.mapValues('label')
					.value();
					
			return {
				require: [ 'zsInstancesCrud', '^zsInstancesView' ],
				controller: [ function ( ) {
					
					var ctrl = this,
						dataProvider,
						zsInstancesView,
						config = {
							columns: [
								{
									id: 'status',
									label: 'Status',
									templateUrl: '/html/admin/instances/instances.html#status',
									locked: true,
									dynamic: true
								},
								{
									id: 'label',
									label: 'Titel',
									resolve: 'instance.label',
									locked: true,
									dynamic: true,
									template: 
										'<i class="icon-font-awesome icon-lock icon-only" data-ng-show="instancesCrud.isProtected(item)" data-zs-title="Deze omgeving is beschermd"></i> <[item.instance.label]>' 
								},
								{
									id: 'fqdn',
									label: 'Webadres',
									resolve: 'instance.fqdn',
									locked: true,
									dynamic: true
								},
								{
									id: 'template',
									label: 'Template',
									resolve: 'instance.template',
									filter: 'capitalize',
									locked: true,
									dynamic: true
								},
								{
									id: 'otap',
									label: 'Gebruiksdoel',
									template: '<[instancesCrud.getOtapLabel(item)]>',
									locked: true,
									dynamic: true
								},
								{
									id: 'hosts',
									label: 'Hosts',
									resolve: 'instance.hosts',
									locked: true,
									dynamic: true,
									template: 
										'<ul data-ng-show="item.instance.hosts.rows.length">' + 
											'<li ng-repeat="host in item.instance.hosts.rows">' +
												'<[host.instance.fqdn]>' +
											'</li>' + 
										'</ul>' +
										'<span data-ng-show="!item.instance.hosts.rows.length">Geen</span>'
											
								},
								{
									id: 'actions',
									label: 'Acties',
									templateUrl: '/html/admin/instances/instances.html#crud-menu',
									locked: true,
									dynamic: true,
									sort: false
								}
							],
							options: {
								select: 'single',
								resolve: 'reference'
							},
							actions: [
								{
									id: 'edit',
									type: 'popup',
									label: 'Bewerken',
									data: {
										url: '/html/admin/instances/instances.html#edit'
									}
								},
								{
									id: 'protect',
									type: 'click',
									label: 'Beschermen'
								},
								{
									id: 'unprotect',
									type: 'click',
									label: 'Vrijgeven'
								},
								{
									id: 'disable',
									type: 'click',
									label: 'Offline'
								},
								{
									id: 'enable',
									type: 'click',
									label: 'Online'
								},
								{
									id: 'details',
									type: 'popup',
									label: 'Details',
									data: {
										url: '/html/admin/instances/instances.html#details'
									}
								}/*,
								{
									id: 'delete',
									type: 'confirm',
									label: 'Verwijderen',
									data: {
										verb: 'Verwijderen',
										label: 'Weet u zeker dat u deze omgeving wil verwijderen? Dit kan niet ongedaan gemaakt worden.'
									}
								},
								{
									id: 'recover',
									type: 'click',
									label: 'Herstellen'
								}*/
							],
							style: {
								classes: {
									'instance-crud-item-deleted': 'instance.status==="deleted"',
									'instance-crud-item-active': 'instance.status==="active"',
									'instance-crud-item-processing': 'instance.status==="processing"',
									'instance-crud-item-disabled': 'instance.status==="disabled"',
									'instance-crud-item-protected': 'instance.status=="protected"'
								}
							},
							url: undefined
						};

					config.actions.forEach(function ( action ) { 
						if(action.id !== 'edit' && action.id !== 'details') {
							action.data = _.extend(action.data || {}, {
								click: 'instancesCrud.handleClick("' + action.id + '", $selection[0])'
							});
						}
						action.when = 'selectedItems.length&&instancesCrud.isAllowed("' + action.id + '", selectedItems[0])';
					});
						
					ctrl.options = {
						showDeleted: false
					};

					function getParams ( action ) {
						var key,
							value,
							params;

						switch(action) {
							case 'protect':
							case 'unprotect':
							key = 'protected';
							value = action === 'protect';
							break;

							case 'delete':
							case 'recover':
							key = 'deleted';
							value = action === 'delete';
							break;

							case 'disable':
							case 'enable':
							key = 'disabled';
							value = action === 'disable';
							break;
						}

						if(key) {
							params = {};
							params[key] = value;
						} 

						return params;
					}
					
					ctrl.link = function ( controllers ) {
						zsInstancesView = controllers[0];
					};
					
					ctrl.getInstances = function ( ) {
						return zsInstancesView ? zsInstancesView.getInstances() : null;
					};
					
					ctrl.getCrudConfig = function ( ) {
						if (!zsInstancesView.isPip()) {
							config.columns.push({
								id: 'login',
								label: 'Login',
								templateUrl: '/html/admin/instances/instances.html#login',
								locked: true,
								dynamic: true,
								sort: false
							});
						}
						return config;	
					};

					ctrl.handleClick = function ( action, instance ) {
						var params = getParams(action);
						if(params) {
							instancesService.updateInstance(ctrl.getControlPanelId(), instance, params)
								.then(function ( ) {
									systemMessageService.emitSave();
									zsInstancesView.onHostUpdate();
								})
								.catch(function ( ) {
									systemMessageService.emitSaveError();
								});
						}
					};

					ctrl.isAllowed = function ( action, instance ) {
						var allowed = false,
							params = getParams(action),
							active = false;

						switch(action) {
							default:
							allowed = ctrl.canEdit() || ctrl.canEditInstance(instance);
							break;

							case 'details':
							allowed = ctrl.canEdit();
							break;

							case 'protect':
							case 'unprotect':
							allowed = ctrl.canEdit() && ctrl.canEditInstance(instance);
							break;
						}

						active = _.every(params, function ( value, key ) {
							var instanceVal = instance.instance[key];
							return value ? !!instanceVal : !instanceVal;
						});

						return allowed && (!active || action === 'edit' || action === 'details');
					};
					
					ctrl.isActive = function ( instance ) {
						return ctrl.getStatus(instance) === 'active';
					};
					
					ctrl.isDisabled = function ( instance ) {
						return ctrl.getStatus(instance) === 'disabled';	
					};
					
					ctrl.isDeleted = function ( instance ) {
						return ctrl.getStatus(instance) === 'deleted';	
					};
					
					ctrl.isProcessing = function ( instance ) {
						return ctrl.getStatus(instance) === 'processing';
					};
					
					ctrl.isProtected = function ( instance ) {
						return instance.instance.protected === true;
					};
					
					ctrl.getStatus = function ( instance ) {
						return instance.instance.status;
					};
					
					ctrl.getStatusLabel = function ( instance ) {
						
						var status = ctrl.getStatus(instance),
							label;
							
						switch(status) {
							case 'processing':
							label = translationService.get('Bezig');
							break;
							
							case 'active':
							label = translationService.get('Actief');
							break;
							
							case 'disabled':
							label = translationService.get('Inactief');
							break;
							
							case 'deleted':
							label = translationService.get('Wordt verwijderd');
							if(instance.instance.delete_on !== undefined) {
								label += 'op ' + dateFilter(instance.instance.delete_on, 'mediumDate');
							}
							break;
						}
						
						return label;
						
					};

					ctrl.getLoginURL = function ( instance ) {
						return "/auth/token/remote_login?instance_id=" + instance.reference;
					}
					
					ctrl.setDataProvider = function ( provider ) {
						dataProvider = provider;
					};
					
					ctrl.getCustomerConfig = function ( ) {
						return zsInstancesView ? zsInstancesView.getCustomerConfig() : null;	
					};
					
					ctrl.getControlPanelId = function ( ) {
						var config = ctrl.getCustomerConfig();					
						return config && config.reference;	
					};
					
					ctrl.canEdit = function ( ) {
						return zsInstancesView && !zsInstancesView.isPip();
					};
					
					ctrl.canEditInstance = function ( instance ) {
						return ctrl.canEdit() || (!instance.instance.protected && zsInstancesView.isPipWritable());
					};

					ctrl.getOtapLabel = function ( item ) {
						return otapLabels[item.instance.otap] || 'Onbekend';
					};
					
					return ctrl;
				}],
				controllerAs: 'instancesCrud',
				templateUrl: '/html/admin/instances/instances.html',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].link(controllers.splice(1));
				}
			};
			
		}]);
	
})();
