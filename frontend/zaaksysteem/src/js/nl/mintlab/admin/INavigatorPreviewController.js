/*global angular,console*/
(function () {
    'use strict';
    angular.module('Zaaksysteem.admin')
        .controller('nl.mintlab.admin.INavigatorPreviewController', [ '$scope', '$window', 'smartHttp', 'translationService', function ($scope, $window, smartHttp, translationService) {


            // some field key are like 'node.properties.bla', we want to
            // interpret this as node[properties][bla]. so split and
            // dereference. (better functional way to do this is using recursion so
            // no variables are changed.)
            $scope.currentSetting = function (target, path) {
                _.map(path.split('.'), function (part) {
                    target = target[part];
                });
                return target;
            };

            $scope.newSetting = function (field) {
                var raw = $scope.casetype.general[field.field];
                return field.type === 'checkbox' ? (raw ? 'Ja' : 'Nee') : raw;
            };

            $scope.hasChanges = function (segment) {
                if (segment === 'general') {
                    return $scope.generalFields.filter($scope.changedGeneralFields).length > 0;
                }

                if (segment === 'documents') {
                    // see if you can find any changed document
                    return _.find($scope.casetype.documents, function (document) {
                        return document.changed;
                    });
                }

                if (segment === 'results') {
                    return _.find($scope.casetype.results, function (result) {
                        return result.changed;
                    });
                }

                return true;
            };

            $scope.changedGeneralFields = function (field) {
                var current = $scope.currentSetting($scope.selectedCasetype, field.field),
                    imported = $scope.casetype.general[field.field],
                    currentSetting = String(current || ""),
                    importedSetting = String(imported || "");

                function zsBoolean(string) {
                    return string === 'Ja';
                }

                if (field.type === 'checkbox') {
                    // make boolean out of current, contains 0 or 1
                    return imported !== zsBoolean(current);
                }

                return !(field.field === 'node.titel' && !$scope.casetype.general.modify_title) &&
                    currentSetting !== importedSetting;
            };

            // determine wether the settings of an existing document have been modified,
            // so the user can be informed that something will be changed
            $scope.documentChanges = function (document) {
                var changes = [];
                if (document.existingKenmerk) {
                    // cast to boolean: !!, then compare these booleans.
                    if (!!document.value_mandatory !== !!document.existingKenmerk.value_mandatory) {
                        changes.push('Verplicht');
                    }

                    if (!!document.publish_pip !== !!document.existingKenmerk.pip) {
                        changes.push('Publiceren op PIP');
                    }

                    if (!!document.publish_website !== !!document.existingKenmerk.publish_public) {
                        changes.push('Publiceren op website');
                    }
                }
                return changes;
            };

            $scope.changedDocuments = function () {
                return $scope.casetype.documents.filter(function (document) {
                    return $scope.documentChanges(document).length || (document.enabled && document.phase && !document.phaseFound);
                });
            };

            $scope.changedChecklistItems = function () {
                return $scope.casetype.checklistitems.filter(function (item) {
                    return item.enabled && item.phase && !item.existing;
                });
            };

            $scope.resultChanges = function (result) {
                var changes = [];

                if (result.existingResult) {
                    if (result.label !== result.existingResult.label) {
                        changes.push('Naam (omschrijving)');
                    }
                    if (result.resultaat !== result.existingResult.resultaat) {
                        changes.push('Resultaattype');
                    }
                    if (result.selectielijst !== result.existingResult.selectielijst) {
                        changes.push('Selectielijst');
                    }

                    if ($scope.archiefnominatieOptions[result.archiefnominatie] !== result.existingResult.archiefnominatie) {
                        changes.push('Archiefnominatie');
                    }
                    if (result.bewaartermijn !== String(result.existingResult.bewaartermijn)) {
                        changes.push('Bewaartermijn');
                    }
                    if (result.comments !== result.existingResult.comments) {
                        changes.push('Toelichting');
                    }
                }
                return changes;
            };

            $scope.init = function () {
                $scope.casetype = $scope.$parent.casetype;
                $scope.selectedCasetype = $scope.$parent.selectedCasetype;

                _.each($scope.casetype.documents, function (document) {
                    document.changes = $scope.documentChanges(document);
                    document.changed = !!document.changes.length || (document.enabled && !!document.phase && !document.phaseFound);
                });
                _.each($scope.casetype.results, function (result) {
                    result.changes = $scope.resultChanges(result);
                    result.changed = (result.existingResult && !!result.changes.length) ||
                        (!result.existingResult && result.enabled);
                });
            };

        }]);
}());