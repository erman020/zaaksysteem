/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.core.detail')
		.controller('nl.mintlab.core.detail.DetailActionController', [ '$scope', function ( $scope ) {
			
			function confirm ( action ) {
				$scope.confirm(action.data.confirm.label, action.data.confirm.verb, function ( ) {
					performAction(action);
				});
			}
			
			function performAction ( action ) {
				switch(action.type) {
					case 'popup':
					$scope.openPopup();
					break;
					
					case 'update':
					$scope.update(action);
					break;
					
					case 'delete':
					$scope.del(action);
					break;
				}
			}
			
			$scope.handleActionClick = function ( action/*, event*/ ) {
				if(action.data.confirm) {
					confirm(action);
				} else {
					performAction(action);
				}
			};
			
		}]);
	
})();