/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.core.data')
		.factory('dataStore', [ '$window', '$timeout', '$rootScope', function ( $window, $timeout, $rootScope ) {
			
			var EventDispatcher = window.zsFetch('nl.mintlab.events.EventDispatcher'),
				dataStore = new EventDispatcher(),
				data = {},
				listeners = {};
				
			function getListener ( id ) {
				var listener = listeners[id];
				if(!listener) {
					listener = listeners[id] = createListener(id);
				}
				return listener;
			}
			
			function createListener ( id ) {
				var dispatcher = new EventDispatcher();
				return {
					observe: function ( callback ) {
						dispatcher.subscribe('dataChange', callback);
						return function ( ) {
							dispatcher.unsubscribe('*', callback);
						};
					},
					trigger: function ( ) {
						dispatcher.publish('dataChange', dataStore.get(id));
					}
				};
			}
				
			dataStore.set = function ( id, value ) {
				data[id] = value;	
			};
			
			dataStore.get = function ( id ) {
				return data[id];
			};
			
			dataStore.push = function ( id, value ) {
				var arr = data[id];
				if(!angular.isArray(arr)) {
					arr = [];
				}
				arr.push(value);
				dataStore.set(id, arr);
			};
			
			dataStore.unset = function ( id ) {
				dataStore.set(id, undefined);
				delete data[id];
			};
			
			dataStore.observe = function ( id, callback ) {
				$timeout(function ( ) {
					getListener(id).trigger();
				}, 0);
				return getListener(id).observe(callback);
			};
			
			dataStore.read = function ( key ) {
				return $rootScope[key];
			};
			
			$window.dataStore = dataStore;
			
			return dataStore;
			
		}]);
	
})();
