/*global define*/
(function ( ) {
	
	function TimelineEvent ( ) {
	}
	
	TimelineEvent.prototype.getMimetype = function ( ) {
		var mimetype = this.mimetype || 'Unknown';
		mimetype = mimetype.replace('application/', '').replace('image/','');
		return mimetype;
	};

	window.zsDefine('nl.mintlab.timeline.TimelineEvent', function ( ) {
		return TimelineEvent;
	});
	
})();
