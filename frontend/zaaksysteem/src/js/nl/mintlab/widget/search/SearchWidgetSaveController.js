/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.SearchWidgetSaveController', [ '$scope', '$window', '$timeout', '$q', 'searchService', 'systemMessageService', 'smartHttp', 'translationService', function ( $scope, $window, $timeout, $q, searchService, systemMessageService, smartHttp, translationService ) {
			
			var defaultFilters = [],
				loadedFilters = [],
				loadingFilter = null,
				showAllSearches = false;
			
			$scope.filters = [];
			$scope.loaded = false;
			
			$scope.collapsed = false;
			
			function initialize ( ) {
				
				var lastFilter = null,
					filterId = $scope.searchId || $window.location.hash,
					selected,
					allFilters;
				
				defaultFilters = _.map($scope.filters, function ( filter ) {
					var search = getDefaultSearch();

					if(typeof filter.values.query === 'string') {
						filter.values = JSON.parse(filter.values.query);
					}

					_.merge(search, filter);
					
					search.values.predefined = true;
					
					return search;
				});
					
				allFilters = $scope.getFilters();
				
				$scope.loaded = true;

				$scope.toggleSearches = function () {
					showAllSearches = !showAllSearches;
				}

				$scope.getLimit = function() {
					return showAllSearches ? undefined : 8;
				}
				
				if(filterId) {
					selected = _.find(allFilters, { 'id': filterId.replace('#', '') });
					if(selected && (!lastFilter || lastFilter.id !== selected.id)) {
						lastFilter = null;
					}
				}
				
				if(!selected && lastFilter) {
					selected = _.find(allFilters, { 'id': lastFilter.id });
				}
				
				if(!selected) {
					lastFilter = null;
					selected = allFilters[0];
				}

				$scope.setSearch(selected)
					.then(function ( ) {
						if(lastFilter) {
							
							$scope.updateColumns(lastFilter.values.columns);
							
							$scope.setActiveValues(lastFilter.values.values);
							$scope.setActiveColumns(lastFilter.values.columns);
							$scope.setActiveSort(lastFilter.values.options.sort);
							$scope.setLocationVisibility(lastFilter.values.options.showLocation);
							$scope.setActiveObjectType(lastFilter.values.objectType);
						}
					});
			}
			
			function getDefaultSearch ( ) {
				
				var search = $scope.search.createFilter();
				search.values.label = translationService.get('Nieuwe zoekopdracht');
				search.security_rules.length = 0;
				
				return search;
			}
			
			$scope.getFilters = function ( ) {
				return defaultFilters.concat(loadedFilters);	
			};
			
			$scope.createSearch = function ( ) {
				var search = getDefaultSearch();

				showAllSearches = true;
				
				$scope.addSearch(search);
				$scope.setSearch(search);
				
				return search;
			};
			
			$scope.addSearch = function ( search ) {
				$scope.search.addFilter(search);
			};
			
			$scope.removeSearch = function ( search ) {
				$scope.search.deleteFilter(search);
				if(search === $scope.activeSearch) {
					$scope.setSearch($scope.getFilters()[0]);
				}
			};
			
			$scope.handleFilterClick = function ( filter ) {
				
				loadingFilter = filter.id;
				
				// use a $timeout to allow the browser to visually update elements
				// before processing heavy js ops
				function setFilter ( ) {
					$timeout(function ( ) {
						$scope.setSearch(filter)
							.then(function ( ) {
								loadingFilter = null;
							});	
					}, 100);	
				}
				
				if(!angular.equals(filter.values.objectType, $scope.getObjectType()) && filter.values.objectType) {
					searchService.getConfig(filter.values.objectType.object_type)
						.then(function ( ) {
							setFilter();
						});
				} else {
					setFilter();
				}
				
			};
			
			$scope.isFilterLoading = function ( filter ) {
				return loadingFilter === filter.id;
			};
			
			$scope.handleUnload = function ( /*$event*/ ) {
				// disable saving of current filter to localStorage,
				// as it was doing more harm than good
			};

			(
				!$scope.isPublic() ?
					$scope.search.getSavedFilters({ baseUrl: $scope.getApiBaseUrl() + '/search' })
						['catch'](function ( ) {
							systemMessageService.emitLoadError('de zoekopdrachten');
							return $q.reject();
						})

					: $q.when([])
			)
				.then(function ( filters ) {
					loadedFilters = filters || [];
					initialize();
				});

		}]);
	
})();
