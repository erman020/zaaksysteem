/*global define*/
(function ( ) {
	
window.zsDefine('nl.mintlab.utils.object.clone', function ( ) {
		
		return function clone ( obj, deep ) {
			
			var cl = new obj.constructor(),
				key,
				val;
			
			if(deep) {
				for(key in obj) {
					cl[key] = obj[key];
				}
			} else {
				for(key in obj) {
					val = obj[key];
					if(!(val === null || val === undefined || typeof(val) !== 'object')) {
						val = clone(obj[key]);
					}
					cl[key] = val;
				}
			}
			
			return cl;
			
		};
	});
	
})();
