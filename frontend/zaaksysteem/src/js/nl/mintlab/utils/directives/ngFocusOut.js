/*global angular*/
(function ( ) {

	angular.module('Zaaksysteem')
		.directive('ngFocusOut', function ( ) {
			
			return function ( scope, element, attrs ) {
				element.bind('focusout', function ( ) {
					scope.$eval(attrs.ngFocusOut);
				});
			};
			
		});
	
})();