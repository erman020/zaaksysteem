/*global angular,fetch*/
(function ( ) {

	angular.module('Zaaksysteem.directives')
		.directive('zsDropdownMenuDecorate', [ '$document', function ( $document ) {
			
			var contains = window.zsFetch('nl.mintlab.utils.dom.contains'),
				body = $document.find('body');
			
			return {
				restrict: 'A',
				link: function ( scope, element/*, attrs*/ ) {

					var menu = element.find('ul'),
						button = element.find('button').eq(0);
					
					function isOpen ( ) {
						return menu.hasClass('menu-open');
					}
					
					function onBodyClick ( event ) {
						var targetEl = event.target,
							menuContains = contains(menu[0], targetEl),
							elementContains = !menuContains && contains(element[0], targetEl);

						if(isOpen() && (!menuContains && !elementContains)) {
							closeMenu();
						}
						
					}

					function openMenu ( ) {
						menu.addClass('menu-open');
						body.bind('click', onBodyClick);
					}
					
					function closeMenu ( ) {
						menu.removeClass('menu-open');
						body.unbind('click', onBodyClick);
					}

					function toggleMenu ( ) {
						if(isOpen()) {
							closeMenu();
						} else {
							openMenu();
						}
					}
					
					button.bind('click', function ( ) {
						toggleMenu();
					});
						
					closeMenu();
					
				}
			};
			
		}]);
})();
