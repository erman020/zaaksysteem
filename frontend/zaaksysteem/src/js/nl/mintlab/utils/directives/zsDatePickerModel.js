/*global angular*/
(function ( ) {

	angular.module('Zaaksysteem.directives')
		.directive('zsDatePickerModel', [ function ( ) {

			return {
				require: [ 'zsDatePickerModel', 'ngModel', 'zsDateRangePicker' ],
				controller: [ function ( ) {

					var ctrl = this,
						ngModel,
						zsDateRangePicker;

					function setDatePickerDate ( date ) {
						zsDateRangePicker.select(date, new Date(date.getFullYear(), date.getMonth(), date.getDate()+1).getTime()-1);
						zsDateRangePicker.setNow(date);
					}

					ctrl.link = function ( controllers ) {
						ngModel = controllers[0];
						zsDateRangePicker = controllers[1];

						ngModel.$formatters.unshift(function ( value ) {
							var date = new Date(value);
							setDatePickerDate(date);
							return value;
						});

						zsDateRangePicker.onSelect.push(function ( event, from/*, to*/ ) {
							if(from || from === 0) {
								ngModel.$setViewValue(from);
							}
						});

						setDatePickerDate(new Date(ngModel.$modelValue));
						
					};

				}],
				link: function ( scope, element, attrs, controllers ) {
					controllers.shift().link(controllers);
				}
				
			};

		}]);

})();
