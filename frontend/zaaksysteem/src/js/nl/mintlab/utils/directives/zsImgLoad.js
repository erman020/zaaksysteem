/*global angular*/
(function () {
    "use strict";
    angular.module('Zaaksysteem')
        .directive('zsImgLoad', function () {

            return function (scope, element, attrs) {
                element.bind('load', function () {

                    // needs to be made generic, need css help to do that
                    element.parent().removeClass('woz-photo-unavailable');
                    element.parent().removeClass('img-not-available');
                });
            };

        });
}());
