/*global angular,_,updateField,$,fetch,updateDeleteButtons*/
(function () {

	angular.module('Zaaksysteem.case')
		.directive('zsCaseWebformField', [
			'$document', '$timeout', '$locale', '$sce', 'objectService', 'currencyFilter', 'dateFilter',
			function ( $document, $timeout, $locale, $sce, objectService, currencyFilter, dateFilter ) {
				var contains = window.zsFetch('nl.mintlab.utils.dom.contains');

				return {
					scope: true,
					require: ['zsCaseWebformField', '^zsCaseWebform', '?^zsCasePhaseView'],
					controller: [
						'$scope', '$element', '$attrs',
						function ( $scope, $element, $attrs ) {
							var ctrl = this,
								zsCaseWebform,
								zsCasePhaseView,
								setter,
								getter;

							function numericToValuta( input ) {
								var value = (currencyFilter(input, '') || input);

								if (typeof value === 'string') {
									return value.trim();
								}

								return value;
							}

							function valutaToNumeric( value ) {
								return value
									.replace(/\./g, '')
									.replace(',', '.');
							}

							function isInFocus() {
								var activeElement = $document[0].activeElement,
									doesContain = contains($element[0], activeElement),
									inFocus = (activeElement && doesContain);

								return inFocus;
							}

							function hasFormElement() {
								return $element[0].querySelectorAll('input, textarea, select').length > 0;
							}

							function getInputElement() {
								return $element[0].querySelector('[name="' + ctrl.getAttributeId() + '"]');
							}

							function getInputElements() {
								return $element[0].querySelectorAll('[name="' + ctrl.getAttributeId() + '"]');
							}

							function getValueDefault() {
								var value = [],
									elements = getInputElements();

								_.each(elements, function ( el ) {
									value.push(el.value || '');
								});

								return value;
							}

							function getValueCheckbox() {
								var value = [],
									el = $element[0].querySelectorAll('[name="' + ctrl.getAttributeId() + '"]');

								_.each(el, function ( e ) {
									if (e.checked) {
										value.push(e.value);
									}
								});

								if (value.length === 1) {
									value = value[0];
								} else if (value.length === 0) {
									value = null;
								}

								return value;
							}

							function getValueOption() {
								var values = [];

								_.each($element[0].querySelectorAll('[name="' + ctrl.getAttributeId() + '"]'), function ( element ) {
									if (element.checked) {
										values.push(element.value);
									}
								});

								return values;
							}

							function getValueValuta() {
								var el = getInputElement(),
									groupSep = $locale.NUMBER_FORMATS.GROUP_SEP,
									decSep = $locale.NUMBER_FORMATS.DECIMAL_SEP,
									value = el.value;

								value = value.replace(new RegExp('\\' + groupSep, 'g'), '');
								value = value.replace(new RegExp('\\' + decSep, 'g'), '.');

								value = parseFloat(value);

								if (isNaN(value)) {
									value = null;
								}

								return value;
							}

							function getValueValutaBtw() {
								var wholeEl = $element[0].querySelector('[name="eur_' + ctrl.getAttributeId() + '"]'),
									decEl = $element[0].querySelector('[name="cnt_' + ctrl.getAttributeId() + '"]'),
									whole,
									dec,
									value;

								whole = Math.ceil(wholeEl.value);

								dec = Math.round(parseFloat('0.' + decEl.value) * 100) / 100;

								value = whole + dec;

								if ((wholeEl.value === '' && decEl.value === '') || isNaN(value)) {
									value = null;
								}

								return value;
							}

							function getValueDate() {
								var el = getInputElement(),
									value = el.value,
									match = value.match(/^(\d{1,2})-(\d{1,2})-(\d{4})$/),
									date,
									stamp = null;

								if (match) {
									date = new Date(parseInt(match[3], 10), parseInt(match[2], 10) - 1, parseInt(match[1], 10));
									stamp = dateFilter(date, 'd-M-yyyy');
								}

								return stamp;
							}

							function setValueDefault( value ) {
								var elements = getInputElements(),
									removed;

								if (!value) {
									value = [];
								}

								if (!_.isArray(value)) {
									value = [value];
								}

								_.each(elements, function ( el, index ) {

									var val = value[index];

									if (val === undefined || val === null) {
										// why is checkbox here?
										val = el.type === 'checkbox' ? !!val : '';
									}

									el.value = val;

									if (index > value.length) {
										// remove redundant input elements
										el.parentNode.parentNode.removeChild(el.parentNode);
										removed = true;
									}

								});

								if (removed) {
									// make sure delete buttons are disabled when apt
									updateDeleteButtons();
								}


							}

							function setValueCheckbox( value ) {
								setValueOption(value);
							}

							function setValueOption( value ) {

								if (!_.isArray(value)) {
									value = [value];
								}

								_.each($element[0].querySelectorAll('input[name="' + ctrl.getAttributeId() + '"]'), function ( el ) {
									el.checked = _.indexOf(value, el.value) !== -1 ? true : false;
								});
							}

							function setValueValuta( value ) {
								var el = getInputElement();

								el.value = numericToValuta(value);
							}

							function setValueValutaBtw( value ) {
								var wholeEl = $element[0].querySelector('[name="eur_' + ctrl.getAttributeId() + '"]'),
									decEl = $element[0].querySelector('[name="cnt_' + ctrl.getAttributeId() + '"]'),
									rounded = value === null ? NaN : Math.round(value * 100) / 100,
									dec,
									match;

								if (isNaN(rounded)) {
									wholeEl.value = decEl.value = '';
								} else {
									match = rounded.toString().match(/(\d+)(\.(\d+)$)?/);

									dec = match[3] !== undefined ? match[3] : '00';

									if (dec.length === 1) {
										dec = dec + '0';
									}

									wholeEl.value = match[1];
									decEl.value = dec;

								}

							}

							function setValueDate( value ) {
								var el = getInputElement(),
									text = dateFilter(value, 'd-M-yyyy') || '';

								el.value = text;
							}

							function setValueFile( /*value*/ ) {

								// ZS-3872: we're not going do anything, too complex

							}

							ctrl.link = function ( controllers ) {

								zsCaseWebform = controllers[0];
								zsCasePhaseView = controllers[1];

								zsCaseWebform.addControl(ctrl);

								$scope.$on('$destroy', function () {
									zsCaseWebform.removeControl(ctrl);
								});

								if (zsCasePhaseView) {
									zsCasePhaseView.addAttrControl(ctrl);

									$scope.$on('$destroy', function () {
										zsCasePhaseView.removeAttrControl(ctrl);
									});
								}

							};

							ctrl.getAttributeName = function () {
								return $attrs.zsCaseWebformFieldName;
							};

							ctrl.getAttributeId = function () {
								return $attrs.zsCaseWebformFieldId;
							};

							ctrl.getAttributeType = function () {
								return $attrs.zsCaseWebformFieldType;
							};

							ctrl.getGroupId = function () {
								return parseInt($attrs.fieldGroupId, 10);
							};

							ctrl.getValue = function () {
								var type = $attrs.zsCaseWebformFieldType,
									value;

								if (getter) {
									value = getter();
								} else {
									switch (type) {
									default:
										value = ctrl.getFormValue();
										break;
									}
								}

								return value;
							};

							ctrl.getFormValue = function () {
								var type = $attrs.zsCaseWebformFieldType,
									value;

								if (!hasFormElement()) {
									return undefined;
								}

								switch (type) {
								default:
									value = getValueDefault();
									break;

								case 'checkbox':
									value = getValueCheckbox();
									break;

								case 'option':
								case 'bag_adres':
								case 'bag_adressen':
								case 'bag_straat_adres':
								case 'bag_straat_adressen':
								case 'file':
									value = getValueOption();
									break;

								case 'valuta':
									value = getValueValuta();
									break;

								case 'valutaex':
								case 'valutaex21':
								case 'valutain':
								case 'valutain6':
								case 'valutain21':
									value = getValueValutaBtw();
									break;

								case 'date':
									value = getValueDate();
									break;
								}

								return value;
							};

							ctrl.setValue = function ( value, options ) {

								var type = $attrs.zsCaseWebformFieldType,
									focused,
									override = options && options.override === true;

								if (!override && objectService.isEqualValue(value, ctrl.getValue())) {
									return;
								}

								focused = isInFocus();

								if (focused || (!setter && !hasFormElement())) {
									return;
								}

								if (setter) {
									setter(value);
								} else {
									switch (type) {
									default:
										setValueDefault(value);
										break;

									case 'checkbox':
										setValueCheckbox(value);
										break;

									case 'option':
									case 'bag_adres':
									case 'bag_adressen':
									case 'bag_straat_adres':
									case 'bag_straat_adressen':
									case 'file':
										setValueOption(value);
										break;

									case 'valuta':
										setValueValuta(value);
										break;

									case 'valutain':
									case 'valutaex':
									case 'valutain6':
									case 'valutain21':
									case 'valutaex21':
										setValueValutaBtw(value);
										break;

									case 'date':
										setValueDate(value);
										break;

									case 'file':
										setValueFile(value);
										break;
									}
								}
							};

							ctrl.setGetter = function ( g ) {
								getter = g;
							};

							ctrl.setSetter = function ( s ) {
								setter = s;
							};

							ctrl.invalidate = function () {
								// wait until the dom is rendered
								$timeout(function () {
									updateField($($element[0]), null, ctrl.getAttributeId());
								}, 0);
							};

							ctrl.isVisible = function () {
								return zsCaseWebform.isFieldVisible(ctrl.getGroupId()) && zsCaseWebform.isFieldVisible(ctrl.getAttributeName());
							};

							ctrl.isFixedValue = function () {
								return zsCaseWebform.isFixedValue(ctrl.getAttributeName());
							};

							ctrl.getFixedValue = function () {
								var value = zsCaseWebform.getFixedValue(ctrl.getAttributeName()),
									type = $attrs.zsCaseWebformFieldType,
									disp;

								if (_.isArray(value)) {
									disp = '<ul>' + _.map(value, function ( v ) {
											return '<li>' + v + '</li>';
										}).join('') + '</ul>';
								} else if (type === 'valuta') {
									disp = numericToValuta(value);
								} else {
									disp = value;
								}

								return disp;
							};

							ctrl.getFixedValueHtml = function () {
								return $sce.trustAsHtml(ctrl.getFixedValue());
							};

							ctrl.isValid = function () {
								return !ctrl.isVisible() || !$element[0].querySelector('.ng-invalid');
							};

							$element.on('focusout', function () {
								// format number when focus is lost
								ctrl.setValue(ctrl.getValue(), { override: true });
							});

							$(document).on('ng:webform:presubmit', function () {
								var inputElement = getInputElement();

								if (inputElement && (ctrl.getAttributeType() === 'valuta')) {
									inputElement.value = valutaToNumeric(inputElement.value);
								}
							});

							return ctrl;
						}],
					controllerAs: 'caseWebformField',
					link: function ( scope, element, attrs, controllers ) {

						controllers[0].link(controllers.slice(1));

					}
				};

			}]);

})();
