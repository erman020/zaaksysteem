/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case.relation', [
		'Zaaksysteem.case',
		'Zaaksysteem.locale',
		'Zaaksysteem.net',
		'Zaaksysteem.message',
		'Zaaksysteem.events',
		'Zaaksysteem.filters',
		'Zaaksysteem.object',
		'Zaaksysteem.core.zql'
	]);
	
})();
