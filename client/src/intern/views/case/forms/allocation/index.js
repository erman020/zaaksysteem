import propCheck from './../../../../../shared/util/propCheck';

export default ( data ) => {

	let capabilities = {};

	propCheck.throw(
		propCheck.shape({

		}),
		data
	);

	return {
		getDefaults: ( ) => {

			return {};

		},
		processChange: ( name, value, values ) => {

			let vals = values;

			return vals.merge({ [name]: value });

		},
		getCapabilities: ( ) => capabilities,
		fields: ( ) => {
			return [
				{
					name: 'allocation',
					template: 'org-unit',
					label: 'Toewijzing',
					required: true,
					data: {
						depth: 1
					}
				}
			];
		}
	};

};
