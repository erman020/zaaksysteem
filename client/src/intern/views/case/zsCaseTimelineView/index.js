import angular from 'angular';
import ocLazyLoadModule from 'oclazyload/dist/ocLazyLoad';
import _ from 'lodash';
import get from 'lodash/get';
import tpl from '../../../../../../frontend/zaaksysteem/src/html/timeline/timeline.swig';
import './timeline.scss';

window._ = _;

let testsContext;

require('../../../../../../frontend/zaaksysteem/src/js/_namespace.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/fromLocalToGlobal.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getViewportSize.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/events/addEventListener.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/events/removeEventListener.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/events/cancelEvent.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/object/inherit.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/shims/indexOf.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/shims/trim.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/safeApply.js');


require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getViewportPosition.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getWindowHeight.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/contains.js');

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/filters/_filters.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/filters/capitalize.js');

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/core/form/_form.js');

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/_directives.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsInfiniteScroll.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsDropdownMenu.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsAutogrow.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsPlaceholder.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsSpotEnlighter.js');
require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsSelect.js');

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/dom/_dom.js');

testsContext = require.context('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/dom', true, /^[^_]+\.js$/);
testsContext.keys()
	.forEach(testsContext);

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/events/_events.js');

testsContext = require.context('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/events', true, /^[^_]+\.js$/);
testsContext.keys()
	.forEach(testsContext);

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/net/_net.js');

testsContext = require.context('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/net', true, /^[^_]+\.js$/);
testsContext.keys()
	.forEach(testsContext);

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/locale/_locale.js');

testsContext = require.context('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/locale', true, /^[^_]+\.js$/);
testsContext.keys()
	.forEach(testsContext);

require('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/timeline/_timeline.js');
testsContext = require.context('../../../../../../frontend/zaaksysteem/src/js/nl/mintlab/timeline', true, /^[^_]+\.js$/);
testsContext.keys()
	.forEach(testsContext);

export default
	angular.module('zsCaseTimelineView', [
		ocLazyLoadModule
	])
		.directive('zsCaseTimelineView', [ '$http', '$compile', '$templateCache', '$interpolate', '$ocLazyLoad', ( $http, $compile, $templateCache, $interpolate, $ocLazyLoad ) => {

			let legacyTpl =
				tpl.replace(/<\[/g, $interpolate.startSymbol())
					.replace(/\]>/g, $interpolate.endSymbol())
					.replace(/%%(.*?)%%/g, '$1');

			$templateCache.put('/html/timeline/timeline.html', legacyTpl);

			return {
				restrict: 'E',
				scope: {
					eventId: '&',
					caseId: '&',
					requestor: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope, element ) {

					let ctrl = this,
						timelineScope = scope.$new(true);

					timelineScope.caseId = ctrl.caseId();
					timelineScope.eventId = ctrl.eventId();
					timelineScope.aanvrager =
						{
							betrokkene_id: null,
							betrokkene_type: get(ctrl.requestor(), 'instance.subject_type'),
							deleted: null,
							gegevens_magazijn_id: null,
							id: null,
							magic_string_prefix: null,
							naam: get(ctrl.requestor(), 'instance.name'),
							rol: null,
							subject_identifier: get(ctrl.requestor(), 'instance.id'),
							uuid: null,
							verificatie: null
						};

					$ocLazyLoad.load(
						'timeline'
							.split(' ')
							.map(name => {
								return {
									name: `Zaaksysteem.${name}`
								};
							})
					)
						.then( ( ) => {
								$compile(
									`<div class="timeline clearfix">
									    <div
									    	ng-include="'/html/timeline/timeline.html'"
									    	ng-controller="nl.mintlab.timeline.TimelineController"
									    	ng-init="initialize('case', caseId);">
									    </div>
									</div>`
								)(timelineScope, ( el ) => {
									element.append(el);
								});
						})
						.catch(( err ) => {

							console.log(err);

						});

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
