import angular from 'angular';
import zsCasePhaseViewModule from './zsCaseView/zsCasePhaseView';
import zsCaseAdminViewModule from './zsCaseView/zsCaseAdminView';
import zsCaseNavigationModule from './zsCaseView/zsCaseNavigation';
import zsCaseSummaryModule from './zsCaseView/zsCaseSummary';
import zsCaseSettingsModule from './zsCaseSettings';
import zsCaseMapViewModule from './zsCaseMapView';
import zsCaseFileUploadModule from './zsCaseFileUpload';
import zsCaseTemplateGenerateModule from './zsCaseTemplateGenerate';
import zsCaseAddSubjectModule from './zsCaseAddSubject';
import zsCaseSendEmailModule from './zsCaseSendEmail';
import zsCaseAboutViewModule from './zsCaseAboutView';
import zsCasePlanModule from './zsCasePlan';
import './styles.scss';

export default
	angular.module('Zaaksysteem.intern.case', [
		zsCasePhaseViewModule,
		zsCaseAdminViewModule,
		zsCaseNavigationModule,
		zsCaseSummaryModule,
		zsCaseSettingsModule,
		zsCaseMapViewModule,
		zsCaseFileUploadModule,
		zsCaseTemplateGenerateModule,
		zsCaseAddSubjectModule,
		zsCaseSendEmailModule,
		zsCaseAboutViewModule,
		zsCasePlanModule
	])
		.name;
