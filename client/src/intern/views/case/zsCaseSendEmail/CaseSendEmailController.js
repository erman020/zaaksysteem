import get from 'lodash/get';
import seamlessImmutable from 'seamless-immutable';
import email from '../forms/email';

export default class CaseSendEmailController {

	static get $inject() {
		return [
			'$scope', '$http', 'resource', 'composedReducer', 'vormValidator', 'snackbarService'
		];
	}

	constructor( scope, $http, resource, composedReducer, vormValidator, snackbarService ) {
		const ctrl = this;
		let values = seamlessImmutable({});

		const formReducer = composedReducer({ scope }, ctrl.templates, ctrl.requestor)
			.reduce(( templates, requestor ) => {
				const id = get(requestor, 'instance.id', []).split('-');
				const requestorName = get(requestor, 'instance.name', 'Onbekend');
				const requestorLink = `/${id[0]}/${id[2]}/?gm=1&type=${id[1]}`;

				if (templates && requestorName) {
					return email({
						templates: seamlessImmutable(templates).asMutable({ deep: true }),
						requestorName,
						requestorLink
					});
				}

				return null;
			});

		formReducer.onUpdate(() => {
			let form = formReducer.data();
			
			if (form) {
				values = values
					.merge(
						seamlessImmutable(form.getDefaults())
					)
					.merge([
						values,
						{
							requestor_address: get(ctrl.requestor(), 'instance.email')
						}
					]);
			}
		});

		const roleReducer = resource('/api/v1/subject/role', { scope })
			.reduce((options, data) => {
				if (data) {
					return data
						.asMutable()
						.map(item => {
							const { label } = item.instance;

							return {
								label,
								value: label
							};
						});
				}

				return null;
			});

		const fieldReducer = composedReducer({ scope }, formReducer, roleReducer)
			.reduce((form, roleOptions) => {
				return form ? form.fields(roleOptions) : [];
			});

		const validityReducer = composedReducer({ scope }, fieldReducer, () => values)
			.reduce(vormValidator);

		ctrl.getValues = () => values;

		ctrl.handleChange = ( name, value ) => {
			if (name === 'recipient_type') {
				if (value === 'aanvrager') {
					values = values.merge([values, { recipient_address: values.requestor_address }]);
				} else {
					values = values.merge([values, { recipient_address: '' }]);
				}
			} else {
				values = values.merge(values);
			}
			
			values = formReducer.data().processChange(name, value, values);
		};

		ctrl.getValidity = () => get(validityReducer.data(), 'validations');

		ctrl.isDisabled = () => !get(validityReducer.data(), 'valid', false);

		ctrl.getFields = fieldReducer.data;

		ctrl.handleSubmit = () => {
			const data = {
				recipient_type: values.recipient_type,
				betrokkene_role: values.betrokkene_role,
				log_error: 0,
				notificatie_cc: values.recipient_cc,
				notificatie_bcc: values.recipient_bcc,
				notificatie_onderwerp: values.email_subject,
				notificatie_bericht: values.email_content,
				update: 1,
				no_redirect: 1,
				mailtype: values.template ?
					'bibliotheek_notificatie'
					: 'specific_mail'
			};
			let promise;

			if (values.recipient_type === 'behandelaar') {
				data.medewerker_betrokkene_id = `betrokkene-medewerker-${get(values.recipient_medewerker, 'data.id')}`;
			} else if (values.recipient_type === 'overig') {
				data.notificatie_email = values.recipient_address;
			}

			promise = $http({
				url: `/zaak/${ctrl.caseId()}/send_email`,
				method: 'POST',
				data
			});

			snackbarService.wait('Uw e-mail wordt verstuurd.', {
				promise,
				then: () => 'Uw e-mail is verstuurd.',
				catch: () => 'De e-mail kon niet worden verstuurd.'
			});

			ctrl.onEmailSend({ $promise: promise });
		};
	}

}
