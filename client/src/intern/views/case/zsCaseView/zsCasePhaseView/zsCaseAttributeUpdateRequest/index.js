import angular from 'angular';
import template from './template.html';

export default
	angular.module('zsCaseAttributeUpdateRequest', [
	])
		.directive('zsCaseAttributeUpdateRequest', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					onFormOpen: '&',
					onFormClose: '&',
					onSubmit: '&',
					isOpen: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this;

					ctrl.description = '';

					ctrl.handleSubmitClick = ( ) => {
						ctrl.onSubmit({ $description: ctrl.description });
						ctrl.description = '';
					};

					ctrl.handleCloseClick = ( ) => {

						ctrl.onFormClose();

						ctrl.description = '';

					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
