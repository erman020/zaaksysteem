import get from 'lodash/get';
import first from 'lodash/first';
import filter from 'lodash/filter';

export default ( phase ) => {

	let fields = get(phase, 'fields', []),
		groups;

	if (!get(first(fields), 'is_group')) {

		fields = [
			{
				is_group: true,
				label: 'Benodigde gegevens',
				help: ''
			}
		].concat(fields);

	}

	groups = filter(fields, { is_group: true });

	return groups.map(
		( group, index ) => {

			let isFirst = index === 0,
					isLast = index === groups.length - 1,
					from = isFirst ? 1 : fields.indexOf(group) + 1,
					to = isLast ? fields.length : fields.indexOf(groups[index + 1]);

			return {
				id: group.id,
				label: group.label,
				description: group.help,
				fields: fields.slice(from, to)
			};
		}
	);

};
