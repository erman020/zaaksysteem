import angular from 'angular';
import mutationServiceModule from './../../../shared/api/resource/mutationService';
import propCheck from '../../../shared/util/propCheck';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import identity from 'lodash/identity';

export default
	angular.module('Zaaksysteem.meeting.proposalActions', [
		mutationServiceModule,
		snackbarServiceModule
	])
	.run([ 'snackbarService', 'mutationService', ( snackbarService, mutationService ) => {

		let proposalActions = [
			{
				type: 'VOTE_FOR_PROPOSAL',
				request: ( mutationData ) => {

					propCheck.throw(
						propCheck.shape({
							proposalReference: propCheck.string,
							voteData: propCheck.object,
							interfaceId: propCheck.number
						}),
						mutationData
					);

					return {
						url: `/api/v1/app/app_meeting/case/${mutationData.proposalReference}/update_attributes`,
						data: mutationData.voteData,
						headers: {
							'API-Interface-Id': mutationData.interfaceId
						}
					};
				},
				reduce: ( data, mutationData ) => {

					return data.map(
						proposal => {

							if (proposal.reference === mutationData.proposalId) {

								return proposal.merge({
									instance: {
										attributes: mutationData.voteData.values
									}
								}, { deep: true });
							}

							return proposal;

						}
					);

				},
				error: ( /*data*/ ) => {

					snackbarService.error('Er ging iets mis bij het opslaan van uw stem. Neem contact op met uw beheerder voor meer informatie.');

				}

			},
			{
				type: 'SAVE_NEW_PROPOSAL_NOTE',
				request: ( mutationData ) => {

					propCheck.throw(
						propCheck.shape({
							proposalReference: propCheck.string,
							noteContent: propCheck.string,
							interfaceId: propCheck.number
						}),
						mutationData
					);

					return {
						url: `/api/v1/case/${mutationData.proposalReference}/note/create`,
						data: {
							content: mutationData.noteContent
						},
						headers: {
							'API-Interface-Id': mutationData.interfaceId
						}
					};
				},
				reduce: ( data, mutationData ) => {
					return data.concat(mutationData);
				},
				error: ( /*data*/ ) => {

					snackbarService.error('Er ging iets mis bij het opslaan van uw notitie. Neem contact op met uw beheerder voor meer informatie.');

				},
				options: {
					reloadOnComplete: true
				}
			},
			{
				type: 'UPDATE_PROPOSAL_NOTE',
				request: ( mutationData ) => {

					propCheck.throw(
						propCheck.shape({
							proposalReference: propCheck.string,
							noteContent: propCheck.string,
							noteReference: propCheck.string,
							interfaceId: propCheck.number
						}),
						mutationData
					);

					return {
						url: `/api/v1/case/${mutationData.proposalReference}/note/${mutationData.noteReference}/update`,
						data: {
							content: mutationData.noteContent
						},
						headers: {
							'API-Interface-Id': mutationData.interfaceId
						}
					};

				},
				reduce: ( data, mutationData ) => {

					return data.map(
						note => {

							if (note.reference === mutationData.proposalReference) {

								return note.merge({
									instance: {
										content: mutationData.noteContent
									}
								}, { deep: true });
							}

							return note;

						}
					);
				},
				error: ( /*data*/ ) => {

					snackbarService.error('Er ging iets mis bij het bijwerken van uw notitie. Neem contact op met uw beheerder voor meer informatie.');

				},
				options: {
					reloadOnComplete: true
				}
			},
			{
				type: 'DELETE_PROPOSAL_NOTE',
				request: ( mutationData ) => {

					propCheck.throw(
						propCheck.shape({
							proposalReference: propCheck.string,
							noteReference: propCheck.string,
							interfaceId: propCheck.number
						}),
						mutationData
					);

					return {
						url: `/api/v1/case/${mutationData.proposalReference}/note/${mutationData.noteReference}/delete`,
						headers: {
							'API-Interface-Id': mutationData.interfaceId
						}
					};

				},
				reduce: ( data, mutationData ) => {
	
					return data.map( note => {
						if (mutationData.proposalReference !== note.instance.reference) {
							return note;
						}
						return undefined;
					}).filter(identity);
				},
				error: ( /*data*/ ) => {

					snackbarService.error('Er ging iets mis bij het verwijderen van uw notitie. Neem contact op met uw beheerder voor meer informatie.');

				},
				options: {
					reloadOnComplete: true
				}
			}
		];

		proposalActions.forEach(( action ) => {
			mutationService.register(action);
		});

	}])
	.name;
