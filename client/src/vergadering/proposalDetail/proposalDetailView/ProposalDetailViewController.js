import find from 'lodash/find';
import first from 'lodash/head';
import get from 'lodash/get';
import includes from 'lodash/includes';
import isArray from 'lodash/isArray';
import isEmpty from 'lodash/isEmpty';
import identity from 'lodash/identity';
import sortBy from 'lodash/sortBy';
import uniqBy from 'lodash/uniqBy';
import seamlessImmutable from 'seamless-immutable';
import shortid from 'shortid';
import getAttributes from '../../shared/getAttributes';
import getFormattedTitle from '../../shared/getFormattedTitle';
import scopedReducerFactory from '../../../shared/api/resource/composedReducer/scopedReducerFactory';
import { angularElement } from '../../../shared/angularElement';

/**
 * Controller for the proposalDetailView component.
 * Status: Decoupled and superficially refactored.
 * ZS-TODO: split constructor into instance methods
 */
export default class ProposalDetailViewController {

	static get $inject() {
		return [
			'$scope', '$sce', '$window', '$timeout', '$document', '$http', '$compile', '$state',
			'dateFilter', 'zsModal',
			'auxiliaryRouteService', 'rwdService', 'sessionService', 'snackbarService'
		];
	}

	constructor(
		$scope, $sce, $window, $timeout, $document, $http, $compile, $state,
		dateFilter, zsModal,
		auxiliaryRouteService, rwdService, sessionService, snackbarService
	) {
		const ctrl = this;
		const scopedReducer = scopedReducerFactory($scope);
		const userResource = sessionService.createResource($scope);
		let loading = false;


		const appConfigReducer = scopedReducer(ctrl.appConfig)
			.reduce(config => config);

		const casetypeAttributesReducer = scopedReducer(ctrl.casetype)
			.reduce(casetype =>
				uniqBy(
					casetype
						.instance
						.phases
						.flatMap(phase => phase.fields),
					'magic_string'
				)
			);

		const proposalReducer = scopedReducer(ctrl.proposal())
			.reduce(proposal => proposal);

		const voteAttributesReducer = scopedReducer(appConfigReducer, casetypeAttributesReducer)
			.reduce(( config, casetypeAttributes ) => {
				const {
					instance: {
						interface_config: {
							magic_strings_accept,
							magic_strings_comment
						}
					}
				} = config;

				/* eslint-disable camelcase */
				let attributes = magic_strings_accept ?
					magic_strings_accept.concat(magic_strings_comment)
					: seamlessImmutable([]);
				/* eslint-enable camelcase */

				return attributes
					.map(configAttribute =>
						find(
							casetypeAttributes,
							attribute => (
								attribute.magic_string === configAttribute
									.object
									.column_name
									.replace('attribute.', '')
							)
						))
					.filter(identity);
			});

		const voteBarConfigReducer = scopedReducer(voteAttributesReducer, userResource)
			.reduce(( attributes, user ) => {
				let orgUnits = get(user, 'instance.logged_in_user.legacy.parent_ou_ids')
					.concat(get(user, 'instance.logged_in_user.legacy.ou_id'));
				let roles = get(user, 'instance.logged_in_user.legacy.role_ids');
				let findAttr = type =>
					find(
						attributes,
						attribute => {
							if (
								(get(attribute, 'type') === type)
								&& attribute.permissions.length
							) {
								return (
									includes(orgUnits, first(attribute.permissions).group.instance.id)
									&& includes(roles, first(attribute.permissions).role.instance.id)
								);
							}

							return false;
						}
					);

				return {
					choice: (
						findAttr('select')
						|| findAttr('option')
						|| undefined
					),
					comment: findAttr('textarea')
				};
			});

		const voteBarConfigValueReducer = scopedReducer(voteBarConfigReducer)
			.reduce(attributes => ({
				choice: {
					magic_string: get(attributes, 'choice.magic_string'),
					values: sortBy(
						get(attributes, 'choice.values', [])
							.map(value => {
								return value.active ?
									value
									: null;
							})
							.filter(identity),
						( value ) => value.sort_order
					)
				},
				comment: attributes.comment
			}));

		const voteBarDataReducer = scopedReducer(voteBarConfigReducer, proposalReducer)
			.reduce(( attributes, proposal ) => {
				return (
					(attributes.choice !== undefined)
					&& (attributes.comment !== undefined)
				) ?
					{
						choice: (
							first(get(
								proposal,
								`instance.attributes.${attributes.choice.magic_string}`
							))
							|| ''
						),
						comment: (
							first(get(
								proposal,
								`instance.attributes.${attributes.comment.magic_string}`
							))
							|| ''
						)
					}
					: null;
			});

		const fieldReducer = scopedReducer(proposalReducer, appConfigReducer, casetypeAttributesReducer)
			.reduce(( proposal, config, casetypeAttributes ) => {
				let fieldConfig = getAttributes(config).voorstel;
				const {
					attributes,
					casetype,
					confidentiality,
					number,
					requestor,
					result
				} = proposal.instance;

				return fieldConfig
					.filter(
						field =>
							(field.external_name.indexOf('voorstelbijlage') === -1)
					)
					.asMutable()
					.map(field => {
						const { internal_name } = field;
						let attribute = get(field, 'internal_name.searchable_object_id');
						let label = (
							internal_name.searchable_object_label_public
							|| internal_name.searchable_object_label
						);
						let value = (
							first(attributes[internal_name.searchable_object_id])
							|| '-'
						);
						let tpl = (
							String(value)
							|| '-'
						);
						let type = get(
							find(
								casetypeAttributes,
								casetypeAttribute =>
									(casetypeAttribute.magic_string === attribute)
							),
							'type'
						);

						if (isArray(value)) {
							if (value.length > 1) {
								tpl = '<ul>';
								value.map(( el ) => {
									tpl += `<li>${el}</li>`;
								});
								tpl += '</ul>';
							} else {
								tpl = `${value.join()}`;
							}
						}

						if (type === 'date') {
							tpl = String(
								dateFilter(value, 'dd MMM yyyy')
								|| '-'
							);
						}

						switch (label) {
						case 'zaaknummer':
							value = number;
							tpl = String(value);
							break;
						case 'zaaktype':
							tpl = String(casetype.instance.name || '-');
							break;
						case 'vertrouwelijkheid':
							tpl = String(confidentiality.mapped);
							break;
						case 'aanvrager_naam': {
							const {
								first_names,
								surname
							} = requestor.instance.subject.instance;

							label = 'Aanvrager naam';
							// eslint-disable-next-line camelcase
							tpl = `${first_names} ${surname}`;
							break;
						}
						case 'resultaat':
							tpl = String(result || '-');
							break;
						}

						return ((tpl !== '') && (tpl !== '-')) ?
							{
								id: shortid(),
								label: (
									label.charAt(0).toUpperCase()
									+ label.slice(1)
								),
								value,
								template: $sce.trustAsHtml(tpl)
							}
							: false;
					})
					.filter(identity);
			});

		const styleReducer = scopedReducer(appConfigReducer)
			.reduce(config => ({
				'background-color': get(config, 'instance.interface_config.header_bgcolor', '#FFF')
			}));

		const titleReducer = scopedReducer(proposalReducer, appConfigReducer)
			.reduce(getFormattedTitle);

		const proposalResultsReducer = scopedReducer(proposalReducer, voteAttributesReducer)
			.reduce(( proposal, attributes ) => {
				return attributes ?
					attributes
						.asMutable()
						.map(attribute => {
							let attributeName = attribute.magic_string;
							let value = (
								first(proposal.instance.attributes[attributeName])
								|| ''
							);
							let tpl = (
								String(value)
								|| '-'
							);

							if (isArray(value)) {
								if (value.length > 1) {
									tpl = '<ul>';
									value.map(( el ) => {
										tpl += `<li>${el}</li>`;
									});
									tpl += '</ul>';
								} else {
									tpl = `${value.join()}`;
								}
							}

							return {
								id: shortid(),
								label: (
									attribute.public_label
									|| attribute.label
								),
								value,
								template: $sce.trustAsHtml(tpl)
							};
						})
					: null;
			});

		const noteReducer = scopedReducer(ctrl.notes())
			.reduce(notes => notes);

		const noteStyleReducer = scopedReducer(noteReducer)
			.reduce(notes => ({
				paddingTop: notes ?
					`${notes.length * 50 + 50}px`
					: '50px'
			}));

		ctrl.getNoteStyle = noteStyleReducer.data;

		ctrl.getNotes = noteReducer.data;

		ctrl.proposalResults = proposalResultsReducer.data;

		ctrl.voteBarData = voteBarDataReducer.data;

		ctrl.voteBarConfig = voteBarConfigValueReducer.data;

		ctrl.getFields = fieldReducer.data;

		ctrl.getAttachments = ctrl.documents;

		ctrl.getStyle = styleReducer.data;

		ctrl.getTitle = titleReducer.data;

		ctrl.proposalId = () => ctrl
			.proposal()
			.data()
			.instance.number;

		ctrl.getProposalReference = () => ctrl
			.proposal()
			.data()
			.reference;

		ctrl.isWriteModeEnabled = () => (
			(appConfigReducer.data().instance.interface_config.access === 'rw')
			&& (userResource.state() === 'resolved')
		);

		ctrl.isVotingBarEnabled = () => (
			(appConfigReducer.data().instance.interface_config.access === 'rw')
			&& (userResource.state() === 'resolved')
			&& voteBarConfigValueReducer.data()
		);

		const voteRightsReducer = scopedReducer(proposalReducer, userResource)
			.reduce(( proposal, user ) => {
				const { system_roles } = user.instance.logged_in_user;

				// If you are an administrator, you can vote regardless
				if (
					includes(system_roles, 'Administrator')
					|| includes(system_roles, 'Zaaksysteembeheerder')
				) {
					return true;
				}

				// If you are not, the proposal needs an assignee AND a coordinator.
				// Otherwise the backend will not accept case mutations of which voting is one
				const {
					assignee,
					coordinator
				} = proposal.instance;

				if (
					isEmpty(assignee)
					|| isEmpty(coordinator)
				) {
					return false;
				}

				return true;
			});

		ctrl.isVotingEnabled = voteRightsReducer.data;

		ctrl.isCaseClosed = () =>
			(proposalReducer.data().instance.status === 'resolved');

		ctrl.getFileUrl = documentId => {
			const { reference } = ctrl.proposal().data();

			return `/api/v1/case/${reference}/document/${documentId}/download`;
		};

		ctrl.isLoading = () => loading;

		ctrl.handleVote = ( proposalReference, voteData ) =>
			ctrl
				.proposal()
				.mutate('VOTE_FOR_PROPOSAL', {
					proposalReference,
					voteData,
					interfaceId: appConfigReducer.data().instance.id
				})
				.asPromise();

		ctrl.handleNoteAction = ( proposalReference, noteContent, noteReference ) => {
			const actionObject = JSON.parse(
				JSON.stringify({
					proposalReference,
					noteContent,
					noteReference,
					interfaceId: appConfigReducer.data().instance.id
				})
			);
			let type;

			loading = true;

			if (noteReference && noteContent) {
				type = 'UPDATE_PROPOSAL_NOTE';
			} else if (!noteContent) {
				type = 'DELETE_PROPOSAL_NOTE';
			} else {
				type = 'SAVE_NEW_PROPOSAL_NOTE';
			}

			return ctrl
				.notes()
				.mutate(type, actionObject)
				.asPromise()
				.then(() => {
					loading = false;
				});
		};

		ctrl.previewFile = file => {
			const modalScope = $scope.$new();
			const modal = zsModal({
				title: `Bekijk ${file.instance.name}`,
				el:
					$compile(
						`<zs-pdf-viewer url="/zaak/${ctrl.proposalId()}/document/${file.reference}/download/pdf"></zs-pdf-viewer>`
					)(modalScope),
				classes: 'full-screen-modal preview-modal'
			});

			modal.open();
			modal.onClose(() => {
				modalScope.$destroy();
			});
		};

		ctrl.handleFileClick = ( file, event ) => {
			event.preventDefault();
			snackbarService
				.wait(
					'Het bestand wordt gedownload',
					{
						promise:
							$http({
								url: ctrl.getFileUrl(file.reference),
								responseType: 'blob'
							})
								.then(response => {
									const URL = ('URL' in $window) ?
										$window.URL
										: $window.webkitURL;
									const blobUrl = URL.createObjectURL(response.data, {
										type: 'text/bin'
									});
									const anchor = angularElement('<a></a>');

									anchor.attr('href', blobUrl);
									anchor.attr('download', file.instance.name);
									$document.find('body').append(anchor);
									anchor[0].click();
									$timeout(() => {
										anchor.remove();
										URL.revokeObjectURL(blobUrl);
									}, 300);
								}),
						then() {
							return '';
						},
						catch() {
							return 'Het bestand kon niet worden gedownload. Neem contact op met uw beheerder voor meer informatie.';
						}
					}
				);
		};

		ctrl.getViewSize = () => {
			if (includes(rwdService.getActiveViews(), 'small-and-down')) {
				return 'small';
			}

			return 'wide';
		};

		ctrl.goBack = () => {
			$state.go('^');
		};
	}

}
