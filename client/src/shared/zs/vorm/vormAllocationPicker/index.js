import angular from 'angular';
import composedReducerModule from './../../../api/resource/composedReducer';
import vormTemplateServiceModule from './../../../vorm/vormTemplateService';
import zsObjectSuggestModule from './../../../object/zsObjectSuggest';
import zsRolePickerModule from './../../../ui/zsRolePicker';
import vormObjectSuggestDisplayModule from './../../../object/vormObjectSuggest/vormObjectSuggestDisplay';
import roleServiceModule from './../../../user/roleService';
import options from './options';
import assign from 'lodash/fp/assign';
import get from 'lodash/get';
import find from 'lodash/find';
import merge from 'lodash/merge';
import includes from 'lodash/includes';
import template from './template.html';
import defaults from './defaults';
import './styles.scss';

export default
  angular.module('vormAllocation', [
    zsRolePickerModule,
    zsObjectSuggestModule,
    vormTemplateServiceModule,
    composedReducerModule,
    vormObjectSuggestDisplayModule,
    roleServiceModule
  ])
    .component('vormAllocation', {
      template,
      bindings: {
        depth: '<',
        types: '<'
      },
      require: {
        ngModel: 'ngModel'
      },
      controller: [
        '$scope', '$element', '$animate', 'composedReducer', 'roleService',
        function (scope, $element, $animate, composedReducer, roleService) {

          let ctrl = this;

          ctrl.$onInit = () => {

            let optionReducer,
              selfOnlyOptionReducer;

            let handleEnter = () => {
              // set focus after enter animation has started
              // and focusable element is in DOM
              $animate.off('leave', $element);
              scope.$$postDigest(() => {
                let focusable = $element[0].querySelector('input, select');

                if (focusable) {
                  focusable.focus();
                }

              });
            };

            let getType = () => get(ctrl.ngModel.$modelValue, 'type');

            optionReducer = composedReducer({ scope }, getType, () => ctrl.types)
              .reduce((opt, types) => {

                let availableTypes = types || options.map(option => option.name);

                return options
                  .filter(option => includes(availableTypes, option.name))
                  .map(option => {

                    let selected = option.name === opt;

                    return assign(
                      option,
                      {
                        icon: selected ?
                          'radiobox-marked'
                          : 'radiobox-blank',
                        selected,
                        classes: {
                          selected,
                          'self-assign': !!(availableTypes.length > 1 && option.name === 'me')
                        }
                      }
                    );
                  });

              });

            selfOnlyOptionReducer = composedReducer({ scope }, optionReducer)
              .reduce(opts => {

                if (opts.length === 1 && find(opts, opt => opt.name === 'me')) {
                  return true;
                }
                return false;

              });

            ctrl.getIsSelfOnlyOption = selfOnlyOptionReducer.data;

            if (ctrl.getIsSelfOnlyOption()) {
              ctrl.ngModel.$setViewValue(
                {
                  type: 'me',
                  data: {
                    me: true
                  }
                }
              );
            }

            ctrl.handleAssignSelf = () => {
              const val = Boolean(get(ctrl.ngModel, '$modelValue.data.me'));

              ctrl.ngModel.$setViewValue(merge(
                {},
                {
                  type: 'me',
                  data: {
                    me: !val,
                    changeDept: !val
                  }
                }
              ));
            };

            ctrl.isAssignedToSelf = () => {
              return get(ctrl.ngModel, '$modelValue.data.me') === true;
            };

            ctrl.handleChangeDepartment = (checked) => {
              ctrl.ngModel.$setViewValue(merge(
                {},
                get(ctrl.ngModel, '$modelValue'), {
                  data: {
                    changeDept: Boolean(checked)
                  }
                })
              );
            };

            ctrl.handleInformAssignee = () => {
              const isInformAssigneeSet = ctrl.getIsInformAssigneeChecked();
              ctrl.ngModel.$setViewValue(merge(
                {},
                get(ctrl.ngModel, '$modelValue'),

                {
                  data: {
                    informAssignee: !isInformAssigneeSet
                  }
                }
              ));
            };

            ctrl.getIsDepartmentChecked = () => {
              return Boolean(get(ctrl.ngModel, '$modelValue.data.changeDept'));
            };

            ctrl.getIsInformAssigneeChecked = () => {
              return Boolean(get(ctrl.ngModel, '$modelValue.data.informAssignee'));
            };

            ctrl.getIsAssigneeEmpty = () => {
              return get(ctrl.ngModel, '$modelValue.type') === 'coworker' && !get(ctrl.ngModel, '$modelValue.data.id');
            };

            ctrl.handleOrgUnitChange = (unit, role) => {
              ctrl.ngModel.$setViewValue(
                {
                  type: 'org-unit',
                  data: {
                    unit,
                    role
                  }
                }
              );
            };

            ctrl.handleSuggest = (contact) => {
              ctrl.ngModel.$setViewValue(merge(
                {},
                get(ctrl.ngModel, '$modelValue'),
                {
                  type: 'coworker',
                  data: {
                    type: 'medewerker',
                    label: contact.label,
                    id: contact.data.id,
                    uuid: contact.data.uuid,
                    route: contact.data.route,
                    role: contact.data.role
                  }
                })
              );
            };

            ctrl.clearCoworker = () => {
              ctrl.ngModel.$setViewValue(merge(
                {},
                {
                  type: 'coworker',
                  data: null
                }
              ));
              ctrl.applyDefaults();
            };

            ctrl.handleTypeClick = (type) => {
              if (type !== getType()) {

                let data = merge({}, { type, data: type === 'org-unit' ? roleService.defaults() : {} });

                ctrl.ngModel.$setViewValue(data);
                ctrl.applyDefaults();

                if (type === 'coworker' || type === 'org-unit') {
                  $animate.on('leave', $element, handleEnter);
                }
              }
            };

            // Apply default values on the data model based on the type ('me', 'coworker', 'org-unit'). 
            // See ./defaults.js.
            // This ensures CaseRegistrationController always has a true or false value to work with,
            // even if the user did not change anything in the GUI.
            ctrl.applyDefaults = () => {
              const type = getType();

              if (!defaults[type]) {
                return;
              }

              ctrl.ngModel.$setViewValue(merge(
                {},
                get(ctrl.ngModel, '$modelValue'),
                defaults[type]
              ));
            };

            ctrl.getTypeOptions = optionReducer.data;

            scope.$$postDigest(ctrl.applyDefaults);
          };
        }]
    })

    // Seperate component for the 'change department' feature. Behaviour for this needs to
    // be different for 3 use cases:
    // 1) Assign to self as part of the 3 available options
    // 2) Assign to self, when assign to self is the only available option. Then, assign to self is a checkbox.
    // 3) Assign to a co-worker / different user.
    .component('changeDepartment', {
      bindings: {
        onClick: '<',
        isDisabled: '<',
        isChecked: '<'
      },
      template:
        `<label data-name="change_department" ng-class="{ 'disabled': $ctrl.isDisabled }">
						<input 
							type="checkbox"
							ng-disabled="$ctrl.isDisabled"
							ng-click="$ctrl.onClick(value)"
							ng-model="value"
							ng-checked="$ctrl.isChecked"
						/>
						Ook afdeling wijzigen
				</label>`
    })

    // ZS-TODO: verify if this display-only component is actually being displayed anywhere.
    // vormTemplateService/compiler.js will display it, if delegate is not empty, but
    // when is this the case for the allocation picker?
    .component('vormAllocationDisplay', {
      bindings: {
        value: '<'
      },
      template:
        '<span class="delegate-display">{{$ctrl.getLabel()}}</span>',
      controller: [
        '$scope', 'composedReducer', 'roleService',
        function (scope, composedReducer, roleService) {

          let ctrl = this;

          ctrl.getLabel = composedReducer({ scope }, roleService.createResource(scope), () => ctrl.value)
            .reduce((units, value) => {

              let type = get(value, 'type'),
                label = '';

              switch (type) {
                case 'me':
                  label = 'Zelf in behandeling nemen';
                  break;

                case 'org-unit': {
                  let unit = find(units, { org_unit_id: get(value.data, 'unit') }),
                    role = unit ?
                      find(unit.roles, { role_id: get(value.data, 'role') })
                      : null;

                  if (unit) {
                    label = `${unit.name}`;
                  }

                  if (role) {
                    label += `/${role.name}`;
                  }
                }
                  break;

                case 'coworker':
                  if (value.data) {
                    label = value.data.label;
                  }
                  break;

              }

              return label;

            }).data;

        }]
    })
    .run(['vormTemplateService', (vormTemplateService) => {

      vormTemplateService.registerType('allocation', {
        control: angular.element(
          `<vorm-allocation
						data-depth="vm.invokeData('depth')"
						data-types="vm.invokeData('types')"
						ng-model
					>vorm</vorm-allocation>`
        ),
        display: angular.element(
          `<vorm-allocation-display
						data-value="delegate.value"
					>
					</vorm-allocation-display>`
        )
      });

    }])
    .name;
