import angular from 'angular';
import zsFileUploadModule from './../../../ui/zsFileUpload';
import vormTemplateServiceModule from './../../vormTemplateService';
import vormFileEditModule from './vormFileEdit';
import vormFileDisplayModule from './vormFileDisplay';

export default
	angular.module('vorm.types.file', [
			vormTemplateServiceModule,
			zsFileUploadModule,
			vormFileEditModule,
			vormFileDisplayModule
		])
			.run([ 'vormTemplateService', function ( vormTemplateService ) {
				
				vormTemplateService.registerType(
					'file',
					{
						control:
							angular.element(
								`<vorm-file-edit
									ng-model
								></vorm-file-edit>`
							),
						display:
							angular.element(
								`<vorm-file-display
									data-file="delegate.value"
									data-formatter="vm.templateData().display"
								></vorm-file-display>`
							),
						defaults: {
							editMode: 'empty'
						}
					}
				);
				
			}])
			.name;
