import assign from 'lodash/assign';
import first from 'lodash/first';
import get from 'lodash/get';
import identity from 'lodash/identity';
import includes from 'lodash/includes';
import mapKeys from 'lodash/mapKeys';
import omit from 'lodash/omit';
import pickBy from 'lodash/pickBy';
import isRequired from './common.js';
import { toUtc } from '../../../util/date/utc';

export default (locals = {
  modules: [],
  user: []
}) => {
  const modules = locals.modules;
  const user = locals.user;
  let remoteField;

  if (modules.length === 1 && get(first(modules), 'instance.interface_config.gbav_search')) {
    remoteField = {
      name: 'remote',
      template: 'checkbox',
      label: '',
      data: {
        checkboxLabel: 'Zoeken in BRP'
      },
      when: get(first(modules), 'instance.interface_config.gbav_search_role_restriction') ?
        includes(user.instance.logged_in_user.capabilities, 'contact_search_extern')
        : true
    };
  } else if (modules.length > 1) {
    remoteField = {
      name: 'remote',
      template: 'select',
      label: 'Bron',
      data: {
        notSelectedLabel: 'Intern',
        options: modules.asMutable().map(
          module => {
            const option = {
              value: module.instance.id,
              label: module.instance.interface_config.search_form_title || module.instance.name
            };
            const gbavSearchEnabled = Boolean(get(module, 'instance.interface_config.gbav_search'));
            const gbavSearchRoleRestriction = get(module, 'instance.interface_config.gbav_search_role_restriction');
            const contactSearchExternPermission = includes(
              user.instance.logged_in_user.capabilities,
              'contact_search_extern'
            );

            if (
              gbavSearchEnabled
              && (
                (
                  gbavSearchRoleRestriction
                  && contactSearchExternPermission
                )
                || !gbavSearchRoleRestriction
              )
            ) {
              return option;
            }

            return null;
          }
        ).filter(identity)
      }
    };
  }

  return {
    format: values => {
      if (modules.length === 1) {
        return values.merge({
          remote: values.remote ?
            modules[0].instance.id
            : null
        });
      }

      return values;
    },
    parse: values => {
      if (modules.length === 1) {
        return values.merge({ remote: !!values.remote });
      }

      return values;
    },
    request: values => {
      const dateOfBirthSet = get(values, 'date_of_birth');
      let processedValues;

      if (dateOfBirthSet) {
        const date_of_birth = toUtc(dateOfBirthSet);

        processedValues = assign({}, values, {
          date_of_birth,
        });
      }

      return {
        method: 'POST',
        url: !values.remote ?
          '/api/v1/subject'
          : `/api/v1/subject/remote_search/${values.remote}`,
        data: {
          query: {
            match: assign(
              {
                subject_type: 'person'
              },
              mapKeys(
                omit(
                  pickBy(dateOfBirthSet ? processedValues : values, identity),
                  'remote'
                ),
                (value, key) => `subject.${key}`
              )
            )
          }
        }
      };
    },
    fields: [
      ...(remoteField ? [remoteField] : []),
      {
        name: 'personal_number',
        label: 'BSN',
        template: 'text',
        required: ['$values', vals =>
          isRequired(vals, [
            'date_of_birth',
            'address_residence.zipcode',
            'address_residence.street_number'
          ])]
      },
      {
        name: 'date_of_birth',
        label: 'Geboortedatum',
        template: 'date',
        required: ['$values', (vals) => {
          return isRequired(vals, [
            'personal_number',
            'address_residence.zipcode',
            'address_residence.street_number'
          ]);
        }]
      },
      {
        name: 'prefix',
        label: 'Voorvoegsel',
        template: 'text'
      },
      {
        name: 'family_name',
        label: 'Achternaam',
        template: 'text',
        required: ['$values', (vals) => {
          return isRequired(vals, [
            'personal_number',
            'address_residence.zipcode',
            'address_residence.street_number'
          ]);
        }]
      },
      {
        name: 'address_residence.zipcode',
        label: 'Postcode',
        template: 'text',
        required: ['$values', (vals) => {
          return isRequired(vals, [
            'personal_number',
            'date_of_birth',
          ]);
        }]
      },
      {
        name: 'address_residence.street_number',
        label: 'Huisnummer',
        template: 'text',
        required: ['$values', (vals) => {
          return isRequired(vals, [
            'personal_number',
            'date_of_birth',
          ]);
        }]
      },
    ],
    columns: [
      {
        id: 'gender',
        template:
          `<zs-icon
							icon-type="alert-circle"
							ng-show="item.messages"
							zs-tooltip="{{::item.messages || ''}}"
						></zs-icon>`,
        label: ''
      },
      {
        id: 'first_names',
        label: 'Voornamen'
      },
      {
        id: 'family_name',
        label: 'Achternaam'
      },
      {
        id: 'date_of_birth',
        template: '<span>{{::item.instance.subject.instance.date_of_birth | date:"dd-MM-yyyy"}}</span>',
        label: 'Geboortedatum'
      },
      {
        id: 'address',
        label: 'Adres',
        template:
          '<span>{{::item.address}}</span>'
      }
    ]
  };
};
