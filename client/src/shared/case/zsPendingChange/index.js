import angular from 'angular';
import template from './template.html';
import zsConfirmModule from './../../ui/zsConfirm';
import './styles.scss';

export default
	angular.module('zsPendingChange', [
		zsConfirmModule
	])
		.directive('zsPendingChange', [ 'zsConfirm', ( zsConfirm ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					change: '&',
					disabled: '&',
					onAccept: '&',
					onReject: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this;

					ctrl.getRequestorName = ( ) => ctrl.change().created_by_name;

					ctrl.getValue = ( ) => ctrl.change().value;

					ctrl.getReason = ( ) => ctrl.change().reason;

					ctrl.accept = ( ) => {
						ctrl.onAccept({ $change: ctrl.change() } );
					};

					ctrl.reject = ( ) => {
						zsConfirm(
							'Weet u zeker dat u deze wijzigingsaanvraag wilt afwijzen? Let op, als behandelaar bent u verantwoordelijk om dit te melden aan de aanvrager.',
							'Afwijzen'
						)
							.then( ( ) => {
								ctrl.onReject( { $change: ctrl.change() } );
							});
						
					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
