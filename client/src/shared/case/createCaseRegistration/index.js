import angular from 'angular';
import template from './template.html';
import inputModule from './../../vorm/types/input';
import radioModule from './../../vorm/types/radio';
import vormObjectSuggestModule from './../../object/vormObjectSuggest';
import vormFieldsetModule from './../../vorm/vormFieldset';
import userSettingsModule from './../../user/userSettings';
import vormValidatorModule from './../../vorm/util/vormValidator';
import resourceModule from './../../api/resource';
import composedReducerModule from './../../api/resource/composedReducer';
import { convertObject } from './../../object/mock';
import get from 'lodash/get';
import first from 'lodash/head';
import capitalize from 'lodash/capitalize';
import uniq from 'lodash/uniq';
import sortBy from 'lodash/sortBy';
import immutable from 'seamless-immutable';
import sessionServiceModule from '../../user/sessionService';
import seamlessImmutable from 'seamless-immutable';
import './styles.scss';

export default
	angular.module('createCaseRegistration', [
		vormObjectSuggestModule,
		vormFieldsetModule,
		inputModule,
		radioModule,
		vormValidatorModule,
		userSettingsModule,
		composedReducerModule,
		resourceModule,
		sessionServiceModule
	])
		.directive('createCaseRegistration', [
			'$window', '$http', '$location', '$q', '$animate', '$timeout', '$state', 'resource', 'snackbarService', 'composedReducer', 'userSettings', 'vormValidator', 'sessionService',
			( $window, $http, $location, $q, $animate, $timeout, $state, resource, snackbarService, composedReducer, userSettings, vormValidator, sessionService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					casetypeId: '&',
					requestor: '&',
					file: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( $scope, $element ) {

					let ctrl = this,
						fields,
						userResource = sessionService.createResource($scope),
						values =
							immutable({}).merge({
								requestor: ctrl.requestor(),
								requestor_type: get(ctrl.requestor(), 'type')
							}),
						locals = immutable({ isCasetypeResolved: false }),
						casetypeResource,
						sourceReducer,
						typeReducer,
						localsReducer,
						validityReducer;

					if (ctrl.file()) {
						values = values.merge({
							file: ctrl.file()
						});
					}

					let isCasetypeResolved = ( ) => !!(casetypeResource.request() && casetypeResource.state() === 'resolved');

					let getContactLabel = ( prefix, type ) => {
						let label = prefix;

						switch (type) {
							case 'natuurlijk_persoon':
							label += ' (burger)';
							break;

							case 'medewerker':
							label += ' (medewerker)';
							break;

							case 'bedrijf':
							label += ' (organisatie)';
							break;
						}

						return label;
					};

					localsReducer = composedReducer({ scope: $scope }, ( ) => locals, ( ) => values)
						.reduce( ( l, v ) => {

							return l.merge({ $values: v });

						});

					validityReducer = composedReducer( { scope: $scope }, localsReducer, ( ) => fields)
						.reduce( ( l, f ) => {

							return vormValidator(f, l.$values, null, l);
						});


					casetypeResource = resource( ( ) => {
						let casetypeId = ctrl.casetypeId() || get(values, 'casetype.reference') || (values.casetype === undefined && get(userSettings.get('remember_casetype'), 'casetype_id'));

						return casetypeId ? `/api/v1/casetype/${casetypeId}` : null;
					}, { scope: $scope })
						.reduce( ( requestOptions, data ) => {

							let casetype = first(data);
							
							if (casetype) {
								casetype = casetype.merge({ label: casetype.instance.title });
							}

							return casetype;
						});

					casetypeResource.onUpdate( casetype => {

						locals = locals.merge( { isCasetypeResolved: isCasetypeResolved() });

						if (casetype && casetype.instance.preset_client && !values.requestor) {

							let betrokkeneIdentifier = casetype.instance.preset_client.instance.old_subject_identifier,
								id = betrokkeneIdentifier.split('-')[2],
								type = betrokkeneIdentifier.split('-')[1],
								label = casetype.instance.preset_client.instance.display_name;
							
							values = values.merge({
								requestor: {
									id,
									label,
									data: {
										id,
										uuid: casetype.instance.preset_client.reference,
										label
									},
									type
								},
								requestor_type: type
							});

						}

						if (!values.casetype && casetype) {

							values = values.merge({ casetype });

						}

					});

					sourceReducer = composedReducer({ scope: $scope }, casetypeResource)
						.reduce( casetype => {

							let options = (get(casetype, 'instance.sources') || [])
								.map( source => ({
									value: source,
									label: capitalize(source)
								}));

							return immutable(options).asMutable( { deep: true });
						});

					sourceReducer.subscribe( ( ) => {

						if (!values.source) {
							values = values.merge({
								source: get(first(sourceReducer.data()), 'value')
							});
						}

					});

					typeReducer = composedReducer({ scope: $scope }, casetypeResource)
						.reduce( ( casetype ) => {

							let requestorTypes = get(casetype, 'instance.subject_types', []),
								recipientTypes = 'natuurlijk_persoon medewerker bedrijf'.split(' ');

							if (get(casetype, 'instance.trigger') === 'extern') {
								requestorTypes = requestorTypes.filter(type => type !== 'medewerker');
							}

							let mapOptions = ( types ) => {
								return uniq(
										types.filter(type => type !== 'preset_client')
											.map(type => {
												return type === 'niet_natuurlijk_persoon' ?
													'bedrijf'
													: (type === 'natuurlijk_persoon_na' ? 'natuurlijk_persoon' : type);
											})
									)
									.map(
										type => {

											let label;

											switch (type) {
												case 'bedrijf':
												label = 'Organisatie';
												break;

												case 'medewerker':
												label = 'Medewerker';
												break;

												case 'natuurlijk_persoon':
												label = 'Burger';
												break;
											}

											return {
												value: type,
												label
											};
										}
									);
							};

							return {
								requestor: sortBy(mapOptions(requestorTypes), ( option ) => [ 'natuurlijk_persoon', 'bedrijf', 'medewerker' ].indexOf(option.value) ),
								recipient:
									sortBy(
										mapOptions(recipientTypes).map(
											option => {

												if (option.value === 'medewerker') {
													return {
														value: 'medewerker',
														label: 'Geen (intern proces)'
													};
												}

												return option;

											}
										),
										( option ) => [ 'medewerker', 'natuurlijk_persoon', 'bedrijf' ].indexOf(option.value)
									)
							};

						});

					fields = [
						{
							name: 'file',
							label: 'Document',
							template: 'object-suggest',
							data: {
								icon: 'file',
								display: ( file ) => file.filestore_id.original_name
							},
							when: [ '$values', vals => !!vals.file ],
							disabled: true
						},
						{
							name: 'casetype',
							label: 'Zaaktype',
							template: {
								inherits: 'object-suggest',
								wrapper: ( el ) => {

									let tpl =
										angular.element(
											`<div class="create-case-remember-casetype">
												<div class="create-case-remember-casetype-switch zs-switch"
													ng-class="{ 'zs-switch-active': vm.invokeData('isCasetypeRemembered') }"
												>
													<input
														type="checkbox"
														id="remember-casetype"
														ng-checked="vm.invokeData('isCasetypeRemembered')"
														ng-click="vm.invokeData('click')"
														class="zs-switch-input"
													/>
													<label class="zs-switch-label" for="remember-casetype"></label>
												</div>
												<span class="create-case-remember-casetype-label">Onthouden</span>
											</div>`
										),
										wrapped = angular.element('<div></div>');

									wrapped.append(el);

									angular.element(
										wrapped[0].querySelector('.form-field-meta')
									).append(tpl);

									return el;

								}
							},
							data: {
								isCasetypeRemembered: ( ) => ctrl.isCasetypeRemembered(),
								click: ( ) => {
									ctrl.handleRememberCasetypeClick();
								},
								objectType: 'casetype',
								format: ( source ) => convertObject(source, 'v0', 'v1')
							},
							required: true
						},
						{
							name: 'requestor_type',
							label: 'Type aanvrager',
							template: 'radio',
							data: {
								options: [ ( ) => get(typeReducer.data(), 'requestor') ]
							},
							required: true,
							when: [ 'isCasetypeResolved', resolved => {
								return resolved
									&& get(typeReducer.data(), 'requestor.length') > 0;
							}]
						},
						{
							name: 'requestor',
							label: [ '$values', ( vals ) => {
								return getContactLabel('Aanvrager', vals.requestor_type);
							}],
							template: 'object-suggest',
							data: {
								objectType: [ '$values', ( vals ) => vals.requestor_type ]
							},
							when: [ 'isCasetypeResolved', '$values', ( resolved, vals ) => {
								return resolved && vals.requestor_type;
							}],
							required: true,
							disabled: [ '$values', ( vals ) => {
								return vals.requestor && get(typeReducer.data(), 'requestor.length') ===/*za*/ 0;
							}]
						},
						{
							name: 'recipient_type',
							label: 'Type ontvanger',
							template: 'radio',
							data: {
								options: [ ( ) => get(typeReducer.data(), 'recipient') ]
							},
							when: [ 'isCasetypeResolved', '$values', ( resolved, vals ) => {
								return resolved && vals.requestor_type === 'medewerker';
							}],
							required: true
						},
						{
							name: 'recipient',
							label: [ '$values', ( vals ) => {
								return getContactLabel('Ontvanger', vals.recipient_type);
							}],
							template: 'object-suggest',
							data: {
								objectType: [ '$values', ( vals ) => vals.recipient_type ]
							},
							when: [ 'isCasetypeResolved', '$values', ( resolved, vals ) => {
								return resolved && vals.requestor_type === 'medewerker' && (vals.recipient_type && vals.recipient_type !== 'medewerker');
							}],
							required: true
						},
						{
							name: 'source',
							label: 'Contactkanaal',
							template: 'select',
							data: {
								options: [ sourceReducer.data ]
							},
							when: [ 'isCasetypeResolved', ( resolved ) => {
								return !!resolved;
							}],
							required: true
						}
					];

					ctrl.getFields = ( ) => fields;

					ctrl.isCasetypeRemembered = ( ) => get(userSettings.get('remember_casetype'), 'remember', false);

					ctrl.handleRememberCasetypeClick = ( ) => {
						let isRemembered = ctrl.isCasetypeRemembered(),
							value,
							op = isRemembered ? 'removeClass' : 'addClass';

						// wait before mutating resource to prevent animation stutter

						$animate[op](angular.element($element[0].querySelector('.zs-switch')), 'zs-switch-active')
							.then(( ) => {

								if (isRemembered) {
									value = { remember: false };
								} else {
									value = { remember: true, casetype_id: get(values, 'casetype.reference', null) };
								}

								userSettings.set('remember_casetype', value);
							});

					};

					ctrl.handleSubmit = ( ) => {

						let vals =
							{
								casetypeId: get(values, 'casetype.reference'),
								aanvrager: get(values, 'requestor.data.uuid'),
								ontvanger: get(values, 'recipient.data.uuid'),
								contactkanaal: values.source,
								document: get(values, 'file.id')
							},
							link = $state.href('register', vals);

						if (
							(values.requestor && !vals.aanvrager)
							|| (values.recipient && !vals.ontvanger)
						) {
							snackbarService.error(`De geselecteerde ${vals.aanvrager ? 'ontvanger' : 'aanvrager' } heeft geen uniek identificatienummer en kan daarom niet gebruikt worden voor het registreren van een zaak. Neem contact op met uw beheerder voor meer informatie.`);
							return;
						}

						if (link) {
							$state.go('register', vals, { location: true });
						} else {
							// we're in a legacy part of ZS
							$window.location = `/intern/aanvragen/${vals.casetypeId}/?aanvrager=${vals.aanvrager || '' }&ontvanger=${vals.ontvanger || ''}&contactkanaal=${vals.contactkanaal}&document=${vals.document || ''}`;
						}

					};

					ctrl.handleFieldChange = ( name, value ) => {

						if (name === 'casetype' && value && ctrl.isCasetypeRemembered()) {
							
							userSettings.set('remember_casetype', { remember: true, casetype_id: get(value, 'reference') } );

						}

						// Reset all values but document when changing the casetype
						if (name === 'casetype' && !value) {
							values = seamlessImmutable({ file: values.file });
						}

						// reset the requestor/recipient if the type is changed
						if (name === 'recipient_type') {
							values = values.merge({ recipient: null });
						} else if (name === 'requestor_type' && value !== 'medewerker') {

							// only clear requestor if selected requestor is not of the same type
							if (get(values.requestor, 'type') !== value) {
								values = values.merge({ requestor: null });
							}
							

						} else if (name === 'requestor_type' && value === 'medewerker') {
							let user = get(userResource.data(), 'instance.logged_in_user');

							values = values.merge({
								requestor: {
									type: 'medewerker',
									label: user.display_name,
									data: {
										id: user.id,
										uuid: user.uuid
									}
								}
							});
						}
						
						values = values.merge({ [name]: value });

					};

					ctrl.values = ( ) => values;

					ctrl.locals = localsReducer.data;

					ctrl.isCasetypeResolved = isCasetypeResolved;

					ctrl.isCasetypeLoading = ( ) => casetypeResource.state() === 'pending';

					ctrl.isFormValid = ( ) => !!get(validityReducer.data(), 'valid');

					ctrl.getValidations = ( ) => get(validityReducer.data(), 'validations');

					ctrl.getPrefillName = ( ) => get(ctrl.requestor(), 'label');

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
