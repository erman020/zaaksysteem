import angular from 'angular';
import invoke from 'lodash/invokeMap';
import appUnloadModule from './../appUnload';
import uniq from 'lodash/uniq';
import startsWith from 'lodash/startsWith';
import debounce from 'lodash/debounce';

export default
	angular.module('shared.util.zsStorage', [
		appUnloadModule
	])
	.provider('zsStorage', [ ( ) => {

		const PREFIX = 'zsStorage';

		return {
			$get: [ '$rootScope', '$window', '$document', 'appUnload', ( $rootScope, $window, $document, appUnload ) => {

				let updateListeners = [],
					cached = {},
					queue = [],
					fromStorage = [],
					pendingChanges = false;

				let invokeUpdate = ( key ) => {
					invoke(updateListeners, 'call', null, key);
				};

				let syncToStorage = ( ) => {

					let key;

					while ((key = queue.shift())) {

						let value = cached[key];

						$window.localStorage.setItem(`${PREFIX}.${key}`, JSON.stringify(value));
						delete cached[key];
					}
				};

				let updateFromStorage = ( ) => {
						
					let keys = uniq(fromStorage);

					keys.forEach(key => {
						cached[key] = JSON.parse($window.localStorage.getItem(`${PREFIX}.${key}`));
					});

					while (keys.length) {
						invokeUpdate(keys.shift());
					}

					fromStorage = [];

				};

				let scheduleSyncToStorage = ( ) => {
					$rootScope.$evalAsync(( ) => {
						// make sure data that gets overridden is always fresh
						updateFromStorage();
						syncToStorage();
					});
				};

				// debounce the update, to prevent issues w/ event order

				let scheduleUpdateFromStorage = debounce(( ) => {

					// make sure changes are not applied in an unfocused window
					
					if ($document[0].hidden) {
						pendingChanges = true;
						return;
					}

					$rootScope.$apply( ( ) => {

						pendingChanges = false;

						updateFromStorage();

					});
					
				}, 100);

				let push = ( key, value ) => {

					if (!queue.length) {
						scheduleSyncToStorage();
					}

					queue = uniq(queue.concat(key));

					cached[key] = value;

				};

				let onStorage = ( event) => {
					scheduleUpdateFromStorage();
					fromStorage.push(event.key.replace(`${PREFIX}.`, ''));
				};

				let onVisibilityChange = ( ) => {

					if (pendingChanges) {
						scheduleUpdateFromStorage();
					}
				};


				$window.addEventListener('storage', onStorage);
				$document[0].addEventListener('visibilitychange', onVisibilityChange);

				appUnload.onUnload(( ) => {
					$window.removeEventListener('storage', onStorage);
					$document[0].removeEventListener('visibilitychange', onVisibilityChange);
				});

				return {
					set: ( key, value, update = true ) => {
						push(key, value);
						if (update) {
							invokeUpdate(key);
						}
					},
					get: ( key ) => {
						return key in cached ? cached[key] : JSON.parse($window.localStorage.getItem(`${PREFIX}.${key}`));
					},
					clear: ( ) => {
						
						queue.length = 0;
						cached = {};

						for (let i in localStorage) {
							if (startsWith(i, PREFIX)) {
								localStorage.removeItem(i);
							}
						}
					},
					onUpdate: updateListeners,
					flush: syncToStorage,
					prefix: ( ) => PREFIX
				};

			}]
		};

	}])
		.name;
