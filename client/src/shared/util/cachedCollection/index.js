import angular from 'angular';
import zsStorageModule from './../zsStorage';
import invoke from 'lodash/invokeMap';
import propCheck from './../propCheck';
import assign from 'lodash/assign';

export default
	angular.module('shared.util.cachedCollection', [
		zsStorageModule
	])
		.factory('cachedCollection', [ 'zsStorage', ( zsStorage ) => {

			let collections = {};

			let save = ( name, value, update ) => {
				zsStorage.set(name, value, update);
			};

			zsStorage.onUpdate.push(( key ) => {
				
				let collection = collections[key];

				if (collection) {
					invoke(collection.onUpdate, 'call', null, zsStorage.get(key), collection.value());
				}
					
			});

			return {
				array: ( name, initial ) => {

					propCheck.throw(
						propCheck.shape({
							name: propCheck.string,
							value: propCheck.array.optional
						}),
						{ name }
					);

					let collection = {},
						value,
						updateListeners;

					if (collections[name]) {
						throw new Error(`Cannot register collection with name ${name} because it already exists`);
					}

					collections[name] = collection;
					
					updateListeners = [ ( ) => {
						value = zsStorage.get(name);
						Object.freeze(value);
					}];

					assign(collection, {
						replace: ( val, update = true ) => {
							save(name, val, update);
						},
						value: ( ) => value,
						onUpdate: updateListeners
					});

					save(name, zsStorage.get(name) || initial || []);

					return collection;

				}
			};

		}])
		.name;
