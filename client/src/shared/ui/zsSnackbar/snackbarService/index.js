import angular from 'angular';
import assign from 'lodash/assign';
import propCheck from './../../../util/propCheck';
import immutable from 'seamless-immutable';
import shortid from 'shortid';
import isArray from 'lodash/isArray';
import first from 'lodash/head';
import get from 'lodash/get';

const DEFAULT_TIMEOUT = 5000;
const DEFAULT_COLLAPSE = 2500;

export default angular
	.module('shared.ui.zsSnackbar.snackbarService', [])
	.factory('snackbarService', [
		'$timeout', '$q',
		( $timeout, $q ) => {
			let snackbarService = {};
			let snacks = immutable([]);

			let add = preferredSnack => {
				let snack = preferredSnack;

				propCheck.throw(
					propCheck.shape({
						message: propCheck.any,
						type: propCheck.oneOf(['error', 'info', 'wait']),
						actions: propCheck.array.optional,
						timeout: propCheck.number,
						collapse: propCheck.oneOfType([propCheck.number, propCheck.bool]).optional
					}),
					snack
				);

				snack = immutable(snack)
					.merge({
						$id: shortid()
					});

				if (snack.timeout) {
					snack = snack.merge(
						{
							$timeoutPromise:
								$timeout(
									() => {
										snack = snack.without('$timeoutPromise');
										snackbarService.remove(snack);
									},
									snack.timeout
								)
						}
					);
				}

				snacks = snacks.concat(snack);

				return snack;
			};

			snackbarService.info = ( message, options = {} ) => {
				return add(
					assign(
						{
							message,
							timeout: get(options, 'actions', []).length ?
								DEFAULT_TIMEOUT * 2
								: DEFAULT_TIMEOUT,
							type: 'info'
						},
						options
					)
				);
			};

			snackbarService.error = ( message, options = {} ) =>
				add(
					assign(
						{
							message
						},
						{
							type: 'error'
						},
						options,
						{
							timeout: 0
						}
					)
				);

			snackbarService.wait = ( message, options = {} ) => {
				propCheck.throw(
					propCheck.shape({
						promise: propCheck.oneOfType([propCheck.object, propCheck.array])
					}),
					options
				);

				let isMultiple = isArray(options.promise);
				let promises = isMultiple ? options.promise : [options.promise];
				let queue = immutable({
					resolved: 0,
					rejected: 0,
					fulfilled: 0,
					total: promises.length
				});
				let collapsed = false;
				let opts = assign(
					{
						message,
						type: 'wait'
					},
					options,
					{
						timeout: 0,
						queue: () => queue,
						collapsed: () => collapsed
					}
				);
				let snack;
				let collapseTimeout;

				if (opts.collapse !== undefined) {
					collapsed = opts.collapse === 0 || false;

					if (!collapsed) {
						collapseTimeout =
							$timeout(
								() => {
									collapsed = true;
									collapseTimeout = null;
								},
								typeof opts.collapse === 'number' ?
									opts.collapse
									: DEFAULT_COLLAPSE
							);
					}
				}

				snack = add(opts);

				promises.forEach(p => {
					p
						.then(data => {
							queue = queue.merge({
								resolved: queue.resolved + 1
							});

							return $q.resolve(data);
						})
						.catch(err => {
							queue = queue.merge({
								rejected: queue.rejected + 1
							});

							return $q.reject(err);
						})
						.finally(() => {
							queue = queue.merge({
								fulfilled: queue.fulfilled + 1
							});
						});
				});

				let handleFulfilment = ( state, value ) => {
					let promiseMethod;
					let snackType;
					let label;
					let args = !isMultiple && state === 'resolve' ? first(value) : value;

					switch (state) {
					case 'resolve':
						promiseMethod = 'then';
						snackType = 'info';
						break;
					case 'reject':
						promiseMethod = 'catch';
						snackType = 'error';
						break;
					}

					label = promiseMethod in opts ? opts[promiseMethod](args) : '';

					if (label) {
						let snackOpts;

						if (typeof label !== 'string') {
							snackOpts = label;
							label = opts.message;
						}

						return $q[state](snackbarService[snackType](label, snackOpts));
					}

					return $q[state](args);
				};

				return $q
					.all(promises).finally(() => {
						snackbarService.remove(snack);

						if (collapseTimeout) {
							$timeout.cancel(collapseTimeout);
							collapseTimeout = null;
						}
					})
					.then(handleFulfilment.bind(null, 'resolve'))
					.catch(handleFulfilment.bind(null, 'reject'));

			};

			snackbarService.remove = snack => {
				snacks = snacks.filter(s => s.$id !== snack.$id);

				if (snack.$timeoutPromise) {
					$timeout.cancel(snack.$timeoutPromise);
				}
			};

			snackbarService.getSnacks = () => snacks;

			return snackbarService;
		}
	])
	.name;
