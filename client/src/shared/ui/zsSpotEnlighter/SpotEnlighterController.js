import immutable from 'seamless-immutable';
import assign from 'lodash/assign';
import capitalize from 'lodash/capitalize';
import debounce from 'lodash/debounce';
import find from 'lodash/find';
import flatten from 'lodash/flatten';
import fuzzy from 'fuzzy';
import get from 'lodash/get';
import includes from 'lodash/includes';
import isArray from 'lodash/isArray';
import shortid from 'shortid';
import links from './../../navigation/zsSideMenu/links';
import { searchHash } from '../../util/searchHash';
import { getIcon } from './getIcon';

const CACHE_INVALIDATION_TIME = 5 * 60 * 1000;
const DESCRIPTION_MAX = 80;
const EXTERNAL_DOCUMENT = 'external-document';
const EXTERNAL_DOSSIER = 'external-dossier';
const QUERY_TYPE = 'default';

export default class SpotEnlighterController {

	static get $inject() {
		return [
			'$rootScope', '$scope', '$state', '$element', '$window', '$timeout', '$location', '$document', '$compile', '$sce',
			'resource', 'composedReducer', 'savedSearchesService', 'objectSuggestionService',
			'contextualActionService', 'auxiliaryRouteService', 'mutationService', 'externalSearchService'
		];
	}

	constructor(
		$rootScope, $scope, $state, $element, $window, $timeout, $location, $document, $compile, $sce,
		resource, composedReducer, savedSearchesService, objectSuggestionService,
		contextualActionService, auxiliaryRouteService, mutationService, externalSearchService
	) {
		const ctrl = this;
		let queryType = QUERY_TYPE;
		let committedQuery = '';
		let input = $element.find('input');
		let open = false;
		let selectedObjectType = 'all';

		let getActions = item => {
			switch (item.type) {
			case 'natuurlijk_persoon':
			case 'bedrijf':
				return [{
					name: 'activate',
					label: 'Activeer',
					click( event ) {
						mutationService.add({
							type: 'active_subject/enable',
							// make sure mutation gets picked up by resource
							request: {
								url: '/betrokkene/get_session',
								params: {
									zapi_num_rows: 20,
									zapi_page: 1
								}
							},
							data: {
								subjectId: `betrokkene-${item.data.object_type}-${item.data.id}`,
								name: item.label
							}
						}).asPromise()
							.then(() => {
								ctrl.close();
							});

						event.preventDefault();
						event.stopPropagation();
					}
				}];
			}

			return [];
		};

		let commitQueryChange = () => {
			committedQuery = ctrl.query;
		};

		ctrl.handleChangeQuery = ( query ) => {
			queryType = 'alternative';
			ctrl.query = query;
			committedQuery = ctrl.query;
		};

		const savedSearchesListResource = resource(savedSearchesService.getRequestOptions(), { scope: $scope });

		const savedSearchesMatchingResource = resource(
			() => {
				let opts;

				if (open && committedQuery && (selectedObjectType === 'all' || selectedObjectType === 'saved_search')) {
					opts = savedSearchesService.getRequestOptions(committedQuery);
				} else {
					opts = null;
				}
				return opts;
			},
			{
				scope: $scope,
				cache: { disabled: true }
			}
		);

		const suggestionResource = resource(
			() => {
				let opts = null;

				if (open && (selectedObjectType !== 'saved_search' && selectedObjectType !== 'shortcut')) {
					opts = objectSuggestionService.getRequestOptions(
						committedQuery,
						selectedObjectType === 'all' ? '' : selectedObjectType
					);
				}

				return opts;
			},
			{
				scope: $scope,
				cache: { disabled: true }
			}
		)
			.reduce(( requestOptions, data ) => {
				let suggestions = data;

				if (!isArray(suggestions)) {
					suggestions = immutable([]);
				}

				suggestions = objectSuggestionService.reduce(suggestions || immutable([]))
					.map(suggestion =>
						suggestion
							.merge({
								iconClass: getIcon(suggestion.type),
								actions: getActions(suggestion)
							}));

				return suggestions;
			});

		const externalSearchResource = resource(
			() => {
				return committedQuery.length > 2 ?
					externalSearchService.getRequestOptions(committedQuery, '_all', queryType)
					: null;
			},
			{
				scope: $scope,
				cache: { disabled: true }
			}
		)
			.reduce(( requestOptions, response ) => {
				if (response) {
					switch (response.code) {
					case 401:
						console.warn('Error: Currently logged in ZS user is not known in Next2Know. Next2Know won\'t return results for this user.');
						return null;
					case 500:
						console.error('Error: Next2Know has a server error.');
						return null;
					default:
						return response.data;
					}
				}

				return null;
			});

		const externalSearchReducer = composedReducer({
			scope: $scope,
			mode: 'hot',
			waitUntilResolved: false
		}, externalSearchResource)
			.reduce(searchResult => {
				return searchResult ? searchResult.hits : immutable([]);
			});

		// We need to explicitly set this reducer to resolved even when the server response
		// comes back as rejected to ensure the reducer chain doesn't break. That would lead
		// to results by the ZS search engine not being displayed.
		externalSearchResource.onStateChange(() => {
			externalSearchReducer.$setState('resolved');
		});

		const externalSearchResultsCountReducer = composedReducer({
			scope: $scope,
			mode: 'hot',
			waitUntilResolved: false
		}, externalSearchResource)
			.reduce(searchResult => {
				return searchResult ? searchResult.total : '';
			});

		ctrl.getExternalSearchResultsCount = externalSearchResultsCountReducer.data;

		const linkReducer = composedReducer({ scope: $scope }, () => selectedObjectType, ctrl.user)
			.reduce(( objectType, user ) => {

				return objectType === 'shortcut' ?
					flatten(links({
						user,
						auxiliaryRouteService,
						$state
					}).map(cat => cat.children))
					: null;
			});

		const objectTypeResource = resource(objectSuggestionService.getRequestOptions('', 'objecttypes'), {
			scope: $scope,
			cache: { every: CACHE_INVALIDATION_TIME }
		})
			.reduce(( requestOptions, data ) => {
				return immutable(
					[
						{
							name: 'all',
							label: 'Zoek in alles'
						},
						{
							name: 'saved_search',
							label: 'Zoekopdrachten'
						},
						{
							name: 'contact',
							label: 'Contacten'
						},
						{
							name: 'file',
							label: 'Documenten'
						},
						{
							name: 'shortcut',
							label: 'Snelkoppelingen'
						}
					]
				)
					.concat(
						(data || [])
							.map(type => ({
								name: type.object_type,
								label: type.label
							}))
					);
			});

		const objectTypeReducer = composedReducer({
			scope: $scope,
			waitUntilResolved: false
		}, objectTypeResource, () => selectedObjectType)
			.reduce(( objectTypes, selected ) => {

				return objectTypes.map(( item ) => {
					return item.merge({
						iconClass: item.name === selected ? 'checkbox-marked-circle' : 'checkbox-blank-circle-outline',
						click: () => {
							ctrl.selectObjectType(item.name);
							$timeout(() => {
								$element.find('input')[0].focus();
							}, false);
						}
					});

				});
			});

		const savedSearchesReducer = composedReducer({
			scope: $scope,
			waitUntilResolved: false
		}, savedSearchesListResource, savedSearchesMatchingResource, () => selectedObjectType)
			.reduce(( listedSearches, matchedSearches, objectType ) => {
				let searches = immutable([]);

				if (objectType === 'all' || objectType === 'saved_search') {
					searches =
						(committedQuery ?
								matchedSearches

								: savedSearchesService.getPredefinedSearches().concat(listedSearches || [])
						)
						|| immutable([]);
				}

				return searches.map(item => ({
					id: item.id,
					type: 'saved_search',
					label: item.label,
					iconClass: getIcon('saved_search'),
					link: `/search/${item.id}`,
					data: item.data
				}));
			});

		const suggestionReducer = composedReducer({ scope: $scope }, suggestionResource, savedSearchesReducer, externalSearchReducer, () => open, () => committedQuery, () => selectedObjectType, linkReducer)
			.reduce(( loadedSuggestions, savedSearches, externalSuggestions, isOpen, query, objectType, availableLinks ) => {
				let matches;
				let suggestions = loadedSuggestions || [];

				if (!isOpen) {
					return [];
				}

				suggestions = suggestions
					.concat(savedSearches)
					.asMutable({
						deep: true
					});

				if (objectType === 'shortcut') {
					suggestions =
						suggestions.concat(
							contextualActionService.getAvailableActions()
								.filter(
									action => !query || includes(action.label.toLowerCase(), query.toLowerCase())
								)
								.map(action => {
									return {
										id: action.name,
										type: 'action',
										label: action.label,
										iconClass: action.iconClass,
										click: () => {
											contextualActionService.openAction(action);
										}
									};
								})
						)
							.concat(
								availableLinks.filter(
									link => {
										return (link.when || link.when === undefined)
											&& (!query || includes(link.label.toLowerCase(), query.toLowerCase()));
									}
								)
									.map(link => {
										return {
											id: link.name,
											label: link.label,
											link: link.href,
											iconClass: link.icon
										};
									})
							);
				}

				function getDescription( suggestion ) {
					const description = get(suggestion, '_source.Omschrijving');

					if (typeof description === 'string') {
						if (description.length > DESCRIPTION_MAX) {
							return `${description.substring(0, DESCRIPTION_MAX)}…`;
						}

						return description;
					}

					return '';
				}

				function formatExternalSuggestions( suggestions ) {
					return suggestions.map(suggestion => {
						if ( suggestion.doc_count ) {
							return {
								id: suggestion.key,
								description: `Aantal documenten: ${suggestion.doc_count}`,
								iconClass: 'package-variant-closed',
								label: suggestion.key,
								type: EXTERNAL_DOSSIER
							};
						}

						return {
							id: shortid(),
							// ZS-TODO: what's the use case for `data`?
							data: get(suggestion, '_source.Naam'),
							description: getDescription(suggestion),
							iconClass: 'package-variant-closed',
							label: get(suggestion, '_source.Naam'),
							dossier: get(suggestion, '_source.Externe_identificatiekenmerken_nummer_binnen_systeem'),
							showDossierLink: queryType === QUERY_TYPE,
							link: `/intern/externaldocument/${get(suggestion, '_index')}/${get(suggestion, '_id')}${searchHash}${encodeURIComponent(query)}`,
							type: EXTERNAL_DOCUMENT
						};
					});
				}

				if ( query && externalSuggestions ) {
					const formattedExternalSuggestions = formatExternalSuggestions(externalSuggestions);

					suggestions = suggestions.concat(formattedExternalSuggestions);
				}

				matches = query ?
					fuzzy.filter(query || '', suggestions, {
						pre: '<strong>',
						post: '</strong>',
						extract: el => el.label
					})
						.map(el => assign(el.original, { label: el.string }))
					: suggestions;

				suggestions = matches
					.concat(
						suggestions.filter(suggestion => !find(matches, {
							id: suggestion.id
						}))
					)
					.map(suggestion => {
						return suggestion.type === 'case' ?
							assign(
								{},
								suggestion,
								{
									label: `${suggestion.data.id}: ${suggestion.data.zaaktype_node_id.titel} <span class="case-status item-label item-label-default">${capitalize(get(suggestion.data, 'status_formatted', ''))}</span>`,
									description:
										('status_formatted' in suggestion.data) ?
											`<span class="case-requestor" ${!suggestion.data.aanvrager ? ' hidden' : ''}><i class="mdi mdi-account"></i> ${get(suggestion.data, 'aanvrager.naam', '')}</span>
											<span class="case-extra-information" ${!suggestion.data.description ? ' hidden' : ''}><i class="mdi mdi-file-document-box"></i> ${get(suggestion.data, 'description') || ''}</span>`
											: ''
								}
							)
							: suggestion;
					})
					.map(suggestion => assign({}, suggestion, {
						label: $sce.trustAsHtml(suggestion.label),
						description: $sce.trustAsHtml(suggestion.description)
					}));

				return suggestions;
			});

		const changeHandlerReducer = composedReducer({ scope: $scope }, () => selectedObjectType)
			.reduce(objectType => {

				let fn = (objectType === 'shortcut') ?
					commitQueryChange
					: debounce(
						() => {
							$scope.$evalAsync(commitQueryChange);
						},
						250,
						{
							leading: false,
							trailing: true
						}
					);

				return fn;
			});

		ctrl.isEmpty = () => (
			suggestionReducer.state() === 'resolved'
			&& get(suggestionReducer.data(), 'length', 0) === 0
		);

		ctrl.getSuggestions = () => {
			return suggestionReducer.state() !== 'rejected' ?
				suggestionReducer.data()
				: null;
		};

		ctrl.getKeyInputDelegate = () => ({ input });

		ctrl.open = context => {
			open = true;

			// `context` is truthy (i.e. `$scope`) when called with `$apply`
			if (typeof context === 'string') {
				ctrl.query = committedQuery = context;
			}

			ctrl.onOpen();
		};

		ctrl.close = () => {
			open = false;
			selectedObjectType = 'all';
			ctrl.query = committedQuery = '';
			ctrl.onClose();
		};

		ctrl.handleBackClick = ctrl.close;

		ctrl.handleQueryChange = () => {
			queryType = QUERY_TYPE;

			return changeHandlerReducer.data()();
		};

		ctrl.handleMagnifyClick = () => {
			ctrl.open();
			$timeout(() => {
				input[0].focus();
			}, 0, false);
		};

		ctrl.isOpen = () => open;

		ctrl.isLoading = () => (
			suggestionResource.request()
			&& suggestionResource.state() !== 'resolved'
		);

		ctrl.getSuggestionListLabel = () => {
			let isEmpty = ctrl.isEmpty();
			let hasErrored = (
				!suggestionResource.request()
				&& suggestionReducer.state() === 'rejected'
			);
			let label = '';

			if (isEmpty && (committedQuery.length > 3 || selectedObjectType === 'shortcut')) {
				label = 'Geen resultaten gevonden voor deze zoekopdracht';
			} else if (hasErrored) {
				label = 'Er ging iets mis met het opvragen van de resultaten voor deze zoekopdracht.';
			} else if (selectedObjectType !== 'shortcut' && (committedQuery.length > 0 && committedQuery.length < 3)) {
				label = 'Typ minimaal drie karakters om resultaten te tonen';
			}

			return label;
		};

		ctrl.handleSuggestionSelect = ( suggestion, event ) => {
			if (event instanceof KeyboardEvent && suggestion.link) {
				let base = $document.find('base').attr('href');

				if (ctrl.useLocation() && suggestion.link.indexOf(base) !== -1) {
					$location.url(suggestion.link.replace(base, '/'));
				} else {
					$window.location.href = suggestion.link;
				}
			} else if (typeof suggestion.click === 'function') {
				suggestion.click(event);
				event.preventDefault();
			}

			ctrl.close();
		};

		if (ctrl.isActive) {
			ctrl.isActive({ $getter: ctrl.isOpen });
		}

		ctrl.getObjectTypeOptions = objectTypeReducer.data;

		ctrl.query = committedQuery;

		ctrl.selectObjectType = ( type ) => {
			selectedObjectType = type;
		};

		ctrl.getObjectTypeLabel = () => {
			let objectTypes = objectTypeReducer.data();
			let selectedItem = objectTypes.filter(item => item.name === selectedObjectType)[0];

			return selectedItem.label;
		};

		ctrl.getSelectedObjectType = () => selectedObjectType;

		ctrl.handleObjectTypeChange = ( objectType ) => {
			selectedObjectType = objectType;
		};

		ctrl.getExternalSearchSettings = () => {
			return {
				active: !!(get(externalSearchService.getApiInfo(), 'token') !== null),
				errorMessage: get(
					externalSearchService.getApiInfo(),
					'error_text',
					'Er kon geen verbinding gemaakt worden met Next2Know. Geen zoekresultaten uit Next2Know beschikbaar.'
				)
			};
		};

		$element
			.find('input')
			.bind('focus', event => {
				if (!open && !$element[0].contains(event.relatedTarget)) {
					$scope.$apply(ctrl.open);
				}
			});

		$document
			.bind('keyup', event => {
				if (event.keyCode === 27) {
					input[0].blur();

					$scope.$evalAsync(() => {
						ctrl.close();
					});

				} else if (event.keyCode === 191 && event.ctrlKey) {
					if (event.shiftKey) {
						selectedObjectType = 'shortcut';
					}

					input[0].focus();
				}
			});

		$rootScope.$on('spotenlighter:open', ( event, query ) => {
			ctrl.open(query);
		});
	}

}
