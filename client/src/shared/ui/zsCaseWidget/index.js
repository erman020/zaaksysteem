import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import assign from 'lodash/assign';
import find from 'lodash/find';
import first from 'lodash/head';
import get from 'lodash/get';
import seamlessImmutable from 'seamless-immutable';
import shortid from 'shortid';
import sortBy from 'lodash/sortBy';

import caseActions from '../../case/caseActions';
import composedReducerModule from '../../api/resource/composedReducer';
import getFullName from '../../util/subject/getFullName';
import resourceModule from '../../api/resource';
import rwdServiceModule from '../../util/rwdService';
import sessionServiceModule from '../../user/sessionService';
import snackbarServiceModule from '../zsSnackbar/snackbarService';
import zsCaseAdminViewModule from '../../../intern/views/case/zsCaseView/zsCaseAdminView';
import zsCaseIntakeActionListModule from '../../case/zsCaseIntakeActionList';
import zsCaseStatusIconModule from '../../case/zsCaseStatusIcon';
import zsCaseWidgetResultCompactModule from './zsCaseWidgetResultCompact';
import zsConfirmModule from '../zsConfirm';
import zsModalModule from '../zsModal';
import zsPaginationModule from '../zsPagination';
import zsProgressBarModule from '../zsProgressBar';
import zsSpinnerModule from '../zsSpinner';
import zsTableModule from '../zsTable';
import zsTruncateModule from '../zsTruncate';
import { confirmCaseRejectAllocation } from '../../case/caseActions/allocation';

import './styles.scss';
import template from './template.html';

export default angular.module('zsCaseWidget', [
		angularUiRouterModule,
		resourceModule,
		composedReducerModule,
		zsPaginationModule,
		zsTableModule,
		sessionServiceModule,
		zsSpinnerModule,
		zsProgressBarModule,
		zsCaseAdminViewModule,
		zsModalModule,
		zsConfirmModule,
		snackbarServiceModule,
		zsCaseIntakeActionListModule,
		zsTruncateModule,
		zsCaseWidgetResultCompactModule,
		zsCaseStatusIconModule,
		rwdServiceModule
	])
		.component('zsCaseWidget', {
			bindings: {
				objectType: '&',
				widgetData: '&',
				widgetTitle: '&',
				loading: '&',
				onDataChange: '&',
				filterQuery: '&',
				settings: '&',
				compact: '&',
				noItemsMessage: '@'
			},
			controller: [ '$scope', '$element', '$compile', '$parse', '$state', '$timeout', '$http', 'resource', 'composedReducer', 'sessionService', 'zsModal', 'zsConfirm', 'snackbarService', 'dateFilter',
				function ( $scope, $element, $compile, $parse, $state, $timeout, $http, resource, composedReducer, sessionService, zsModal, zsConfirm, snackbarService, dateFilter ) {

				/**

				This module expects v1 case objects.

				There is a similar looking directive for dasboard widgets (zsDashboardWidgetSearchResult)
				that was built in the v0 era and thus expects v0 style objects.

				In the zsDashboardWidgetSearchResult directive, the column configuration is retrieved
				from the backend as part of the api/search/config/<objectType> call.
				As this call was built in the v0 era, the column definitions in this call don't
				match with the current v1 output of cases and objects.

				As the primary use case for this directive doesn't specifically require configurable
				columns at this stage, in this directive we have opted for statically defined columns.
				(See below: const COLUMNS)

				If you want to replace the Dashboard Widgets with this one, or the use case changes
				that columns DO need to be configurable from the backend, feel free to retrieve the
				definition externally through an endpoint.

				Also please note that the zsDashboardWidgetSearchResult can display cases as well as
				objects. This component in this iteration is aimed at displaying cases _only_. It needs
				to be expanded to also allow displaying of objects.

				**/

				const COLUMNS = {
					normal: [
						{
							name: 'status',
							label: '',
							template: '<zs-case-status-icon data-status="item.values.status" archival-state="item.values.archivalState" data-destructable="item.values.destructable" data-updates="item.values.numUnacceptedUpdates + item.values.numUnacceptedUpdates">',
							sort: true
						},
						{
							name: 'number',
							label: '#',
							template: '<a href=\'/intern/zaak/{{::item.values.number}}\'>{{::item.values.number}}</a>',
							sort: {
								type: 'numeric'
							}
						},
						{
							name: 'progress',
							label: 'Voortgang',
							template: '<zs-progress-bar data-progress="{{::item.values.progress}}"></zs-progress-bar>',
							sort: {
								type: 'numeric'
							}
						},
						{
							name: 'casetypeName',
							label: 'Zaaktype',
							template: '<span>{{::item.values.casetypeName}}</span>',
							sort: true
						},
						{
							name: 'extraInfo',
							label: 'Extra informatie',
							template: '<a href=\'/intern/zaak/{{::item.values.number}}\' data-zs-truncate="{{::item.values.extraInfo}}" zs-truncate-length="100"></a>',
							sort: true
						},
						{
							name: 'requestorName',
							label: 'Aanvrager',
							template: '<span>{{::item.values.requestorName}}</span>',
							sort: true
						},
						{
							name: 'daysLeft',
							label: 'Dagen',
							template: '<span data-ng-show="[ \'resolved\', \'overdragen\' ].indexOf(item.values.status) === -1" data-ng-class="{\'urgentie_high\': item.values.days_left < 0, \'urgentie_low\': item.values.days_left >= 0 }">{{::item.values.days_left}}</span>',
							sort: {
								type: 'numeric'
							}
						},
						{
							name: 'actions',
							label: '',
							template: '<zs-dropdown-menu options=\'item.itemActions\' empty-label=\'Geen acties beschikbaar\' button-icon=\'dots-vertical\' on-open=\'handleActionMenuClick(item, $event)\' is-fixed="true" button-style=\'{ btn: true, "btn-small": true, "btn-secondary": true, "btn-flat": true, "btn-open": zsDropdownMenu.isMenuOpen() }\'></zs-dropdown-menu>',
							sort: false
						}
					],
					compact: [ 'status', 'title', 'requestor', 'days', 'actions' ]
				};

				let ctrl = this,
					searchReducer,
					configReducer,
					resultReducer,
					userResource = sessionService.createResource($scope),
					columnReducer,
					itemReducer,
					sortReducer,
					userSort = seamlessImmutable({}),
					objectTypeReducer;

				ctrl.getNoItemsFoundMessage = ( ) => {
					return ctrl.noItemsMessage ? ctrl.noItemsMessage : 'Geen resultaten gevonden';
				};

				objectTypeReducer = composedReducer({ scope: $scope }, ctrl.objectType )
					.reduce( objectType => {
						return objectType ? objectType : 'case';
					});


				let createCaseResource = ( caseObj ) => {
					return resource(
						`/api/v1/case/${caseObj.reference}`,
						{
							scope: $scope
						}
					)
						.reduce( ( requestOptions, data ) => {
							return first((data || []));
						});
				};

				let handleActionModal = ( caseObj, action ) => {

					let scope = $scope.$new(true),
						modal;

					let cleanup = ( ) => {
						scope.caseResource.destroy();
						scope.casetypeResource.destroy();
					};

					scope.action = action;

					scope.close = ( promise ) => {

						modal.hide();

						promise.then( ( ) => {

							ctrl.onDataChange();

							modal.close();
							cleanup();

						})
							.catch(( ) => {
								modal.show();
							});
					};

					scope.user = userResource.data().instance.logged_in_user;

					scope.caseResource = createCaseResource(caseObj);

					scope.casetypeResource = resource(
						( ) => {

							let caseTypeObj = get(scope.caseResource.data(), 'instance.casetype');

							return caseTypeObj ? {
									url: `/api/v1/casetype/${caseTypeObj.reference}`,
									params: {
										version: caseTypeObj.instance.version
									}
								}
								: null;
						},
						{
							scope: $scope
						}
					)
						.reduce( ( requestOptions, data ) => {
							return first(data);
						});

					modal = zsModal({
						title: action.label,
						el: $compile(
							`<zs-case-admin-view
								data-action="action"
								data-user="user"
								case-resource="caseResource"
								casetype-resource="casetypeResource"
								on-submit="close($promise)"
							>
							</zs-case-admin-view>`
						)(scope)
					});

					modal.open();

					modal.onClose( ( ) => {
						cleanup();
					});
				};

				let handleActionConfirm = ( caseObj, action ) => {

					let caseResource = createCaseResource(caseObj),
						mutation = action.mutate();

					zsConfirm(action.confirm.label, action.confirm.verb)
						.then( ( ) => {
							return caseResource.mutate(mutation.type, mutation.data).asPromise();
						})
						.finally( ( ) => {
							caseResource.destroy();
						});

				};

				let handleActionClick = ( caseObj, action ) => { //eslint-disable-line

					if (action.confirm) {
						handleActionConfirm(caseObj, action);
					} else {
						handleActionModal(caseObj, action);
					}

				};

				resultReducer = composedReducer({ scope: $scope }, ctrl.widgetData )
					.reduce( cases => cases );

				// Using a reducer here so you can easily slot in either an external
				// column definition API response, or a prop retrieved from scope
				configReducer = composedReducer({ scope: $scope }, ( ) => COLUMNS )
					.reduce( columns => columns );

				columnReducer = composedReducer({ scope: $scope }, configReducer)
					.reduce( config => {

						let columns = config.normal;

						return columns
							.map( columnToMap => {

								let column = seamlessImmutable(columnToMap);

								return column.merge({
									id: columnToMap.name
								});

							});

					});

				sortReducer = composedReducer({ scope: $scope }, searchReducer, columnReducer, ( ) => userSort)
					.reduce( ( search, columns, userSortData ) => {

						let sort,
							searchSort = { type: 'number' },
							defaultSort = seamlessImmutable({});

						sort = defaultSort.merge([ searchSort, userSortData ]);

						sort = sort.merge( { type: get(find(columns, { name: sort.by }), 'sort.type', 'alphanumeric') });

						if (!sort.by) {
							sort = {
								by:
									get(
										find(
											columns, ( col ) => {
												return col.name === 'number';
											}
										),
									'name'),
								order: 'desc'
							};
						}

						return seamlessImmutable(sort);
					});

				itemReducer = composedReducer({ scope: $scope }, resultReducer, columnReducer, userResource, searchReducer, objectTypeReducer, sortReducer)
					.reduce( ( resultData, columns, user, search, objectType, sort ) => {

						let items = resultData;

						// make sure we'll able to differentiate between empty and non-existent result sets
						if (!items) {
							return null;
						}

						if (objectType === 'case') {
							items = items.map(
								item => {

									let caseId = item.instance.number,
										reference = item.id;

									return {
										id: shortid(),
										href: $state.href('case', { caseId }),
										itemActions: caseActions({
											caseObj: item,
											casetype: null,
											user: user.instance.logged_in_user,
											$state,
											acls: []
										})
											.filter(action => action.context !== 'admin')
											.map(action => {
												return {
													name: action.name,
													label: action.label,
													click: ( ) => {
														handleActionClick(item, action);
													}
												};
											}),
										values: {
											id: shortid(),
											number: item.instance.number,
											link: `zaak/${item.instance.number}`,
											extraInfo: item.instance.subject, // Please note: this is extra information field, NOT the subject as in person / company field.
											status: item.instance.status,
											progress: get(item, 'instance.milestone.instance.milestone_sequence_number') / get(item, 'instance.milestone.instance.last_sequence_number') * 100,
											archivalState: item.instance.archival_state,
											destructable: get(item, 'instance.destructable'), // ZS-TODO: Add this to api/v1/output
											numUnacceptedFiles: get(item, 'instance.num_unaccepted_files'), // ZS-TODO: Add this to api/v1/output
											numUnacceptedUpdates: get(item, 'instance.num_unaccepted_updates'), // ZS-TODO: Add this to api/v1/output
											casetypeName: item.instance.casetype.instance.name,
											daysLeft: get(item, 'instance.days_left'), // ZS-TODO: Add this to api/v1/output. See ZS-14954 for more details.
											dateCreated: dateFilter(item.instance.date_created, 'dd-MM-yyyy'),
											requestorName: getFullName(item.instance.requestor)
										},
										data: {
											user,
											onReject: ( ) => {

												confirmCaseRejectAllocation({
													http     : $http,
													resource : ctrl.onDataChange,
													caseUUID : reference,
													settings : ctrl.settings(),
													snackbar : snackbarService,
													confirm  : zsConfirm,
												});

											},
											onSelfAssign: ( ) => {

												snackbarService.wait('Zaak wordt in behandeling genomen.', {
													promise: $http({
														url: `/zaak/${caseId}/open`,
														method: 'POST'
													}),
													then: ( ) => {
														return 'Zaak is in behandeling genomen.';
													},
													catch: ( ) => {
														return 'Zaak kon niet in behandeling worden genomen. Neem contact op met uw beheeerder voor meer informatie.';
													}
												})
													.then( ( ) => {
														$state.go('case', { caseId });
													});

											}
										}
									};
								}
							);
						} else {
							items = items.map(
								item => {

									return item.merge({
										id: item.id,
										href: `/object/${item.id}`,
										values: item.instance
									});
								}
							);
						}

						items = sortBy(items, ( item ) => item.values[sort.by] );

						return sort.order === 'asc' ? items : items.reverse();


					});

				ctrl.getRows = itemReducer.data;

				itemReducer.subscribe( items => {
					if (items && items.length) {
						$element.removeClass('empty');
					} else {
						$element.addClass('empty');
					}
				});

				ctrl.getColumns = composedReducer({ scope: $scope }, columnReducer, sortReducer )
					.reduce( ( columns, sort ) => {

						let visibleColumns = columns;

						let styledColumns = visibleColumns.map( ( column ) => {

							let sortedOn = column.id === get(sort, 'by'),
								sortReversed = sortedOn && get(sort, 'order', 'asc') === 'desc',
								iconType;

							if (sortedOn) {
								iconType = `chevron-${sortReversed ? 'up' : 'down'}`;
							}

							return column.merge({
								style: {
									'column-sort-active': sortedOn,
									'column-sort-reversed': sortReversed,
									'column-sort-supported': column.sort
								},
								iconType
							});
						});

						return styledColumns;

					}).data;

				ctrl.handleColumnClick = ( columnId ) => {

					let sort = sortReducer.data(),
						column = find(columnReducer.data(), { id: columnId });

					if (!get(column, 'sort', null)) {
						return;
					}

					if (sort.by === columnId) {
						sort = assign({}, { by: columnId, order: sort.order === 'desc' ? 'asc' : 'desc' });
					} else {
						sort = assign({}, { by: columnId, order: 'asc' });
					}

					userSort = sort;
				};

				ctrl.isEmpty = ( ) => get(ctrl.getRows(), 'length', false) === 0;


			}],
			template
		})
		.name;
