import angular from 'angular';
import template from './template.html';

export default
	angular.module('zsNotificationCounter', [
	])
		.directive('zsNotificationCounter', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					count: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this;
				
					ctrl.getCount = ctrl.count;

				}],
				controllerAs: 'zsNotificationCounter'
			};

		}])
		.name;
