import get from 'lodash/get';
import { hasSameOrigin } from '../util/origin';

export const CONFIGURATION_INCOMPLETE = 'api/v1/configuration_incomplete';

/**
 * `configuration_incomplete` implies the API is not configured for public accces,
 * which means we don't have a logged in user
 *
 * @param {Object} reason
 * @return {boolean}
 */
export const isIncomplete = ( type, url ) =>
	((type === CONFIGURATION_INCOMPLETE) && hasSameOrigin(url));

/**
 * Check to see if the rejection actually comes from our backend or is the result
 * of a call to another service
 *
 * @param {number} status
 * @param {string} url
 * @return {boolean}
 */
export const isUnauthorized = ( status, url ) =>
	((status === 401) && hasSameOrigin(url));

function isApiDenied( reason ) {
	const {
		status,
		config: {
			url
		}
	} = reason;
	const type = get(reason, 'data.result.instance.type');

	return (isUnauthorized(status, url) || isIncomplete(type, url));
}

export default isApiDenied;
