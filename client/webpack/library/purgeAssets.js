const rimraf = require('rimraf');
const { PUBLIC_PATH, ROOT } = require('./constants');
const catchError = require('./catchError');

try {
  rimraf.sync(`${ROOT}/${PUBLIC_PATH}/**/*`);
  require('./deployVendorBundle');
} catch (error) {
  catchError(error);
}
