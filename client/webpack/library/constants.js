const { resolve } = require('path');
const {
  DEV_SERVER_HOSTNAME,
  DEV_SERVER_PORT,
} = require('./packageConfig');

const { argv } = process;

const DEV_SERVER_ORIGIN = `https://${DEV_SERVER_HOSTNAME}:${DEV_SERVER_PORT}`;
const IS_DEBUG_BUILD = (argv.slice(-2).join(' ') === '--env debug');
const IS_DEV = /[/\\]webpack-dev-server(?:\.js)?$/.test(argv[1]);
const PUBLIC_PATH = '/assets/';
const ROOT = resolve('..', 'root');
const VENDOR = 'vendors.bundle.js';
const COMMONS_CHUNK = 'shared';

module.exports = {
  COMMONS_CHUNK,
  DEV_SERVER_HOSTNAME,
  DEV_SERVER_ORIGIN,
  DEV_SERVER_PORT,
  IS_DEBUG_BUILD,
  IS_DEV,
  PUBLIC_PATH,
  ROOT,
  VENDOR,
};
