package Zaaksysteem::Object::Types::ObjectRelationAttrs;

use Moose;

extends 'Zaaksysteem::Object';

has any_object => (
    is => 'rw',
    traits => [qw[OR]],
    label => 'One relation to rule them all'
);

has self_object => (
    is => 'rw',
    type => 'object_relation_attrs', # refers to __PACKAGE__
    traits => [qw[OR]],
    label => 'One self relation to rule them all'
);

has derp_object => (
    is => 'rw',
    type => 'derp',
    traits => [qw[OR]],
    label => 'One derp relation to rule them all',
);

has override_isa_object => (
    is => 'rw',
    isa => 'Zaaksysteem::Object',
    traits => [qw[OR]],
    label => 'One object relation to rule them all'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
