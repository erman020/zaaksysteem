#!/usr/bin/env perl
use warnings;
use strict;

package Test::Package;
# ABSTRACT: Test a package to the fullest extend

use Pod::Elemental;
use Test::Compile;
use Test::More;
use Test::Pod::Coverage;
use Test::Pod;

use base 'Exporter';

our @EXPORT = qw(all_package_files_ok manifest_ok);

sub all_package_files_ok {
    my %args = @_;

    if (!exists $args{skip}) {
        $args{skip} = {};
    }

    subtest 'Compile modules tests' => sub {
        if ($args{skip}{pm}) {
            plan skip_all => 'Skipping module compile tests on request';
        }
        else {
            if (!defined $args{pm}) {
                $args{pm} = [ 'blib', 'lib' ];
            }
            my @mods;
            push(@mods, all_pm_files(@{$args{pm}}));
            my $ok = all_pm_files_ok(@mods);
            BAIL_OUT("Package compilation fails") if (!$ok);
        }
    };

    subtest 'Compile script tests' => sub {
        if ($args{skip}{pl}) {
            plan skip_all => 'Skipping script files tests on request';
        }
        else {
            if (!defined $args{pl}) {
                $args{pl} = [ 'bin', 'script', 'dev-bin' ];
            }

            my @scripts;
            push(@scripts, all_pl_files(@{$args{pl}}));
            if (!@scripts) {
                plan skip_all => 'No pl files found';
            }
            else {
                all_pl_files_ok(@scripts);
            }
        }
    };

    subtest 'POD compile tests' => sub {
        if ($args{skip}{pod}) {
            plan skip_all => 'Skipping pod compile tests on request';
        }
        else {
            all_pod_files_ok();
        }
    };

    subtest 'POD coverage tests' => sub {
        if ($args{skip}{pod_coverage}) {
            plan skip_all => 'Skipping pod coverage tests on request';
        }
        else {
            all_pod_coverage_ok({ trustme => [qr/^[A-Z0-9_]+$/] });
        }
    };

    subtest 'EUPL license tests' => sub {
        if ($args{skip}{eupl}) {
            plan skip_all => 'Skipping EUPL license tests on request';
        }
        else {
            my @poddirs = qw(lib t/inc t/lib script dev-bin bin);
            my @podfiles = sort { $a cmp $b } all_pod_files(@poddirs);

            foreach (@podfiles) {
                next if $_ eq 'lib/Zaaksysteem/CONTRIBUTORS.pod';
                if ((stat($_))[7] == 0) {
                    diag ("$_ is empty!");
                    next;
                }
                eupl_ok($_);
            }
        }

    };

    done_testing();
}

sub manifest_ok {
    TODO: {
        local $TODO = "Manifest files need to be checked";
        fail("Not tested yet");
    }
}

sub eupl_ok {
    my $file     = shift;
    my $document = Pod::Elemental->read_file($file);

    my $ok = 0;
    foreach my $child (@ {  $document->children } ) {
        if (ref $child eq 'Pod::Elemental::Element::Generic::Command') {
            my $content = $child->content;
            chomp($content);
            if ($content eq 'COPYRIGHT and LICENSE') {
                $ok = 1;
            }
            elsif ($content eq 'COPYRIGHT' || $content eq 'LICENSE') {
                note("Old skool license found in $file, consider updating");
                $ok = 1;
            }
        }
    }
    return ok($ok, "$file: EUPL license found");
}

1;

__END__

=head1 NAME

Test::Package

=head1 AUTHOR

Wesley Schwengle C<< wesley at schwengle.net >>

=head1 LICENSE

BSD

=head1 MINTLAB

Adapted for Mintlab to include EUPL license checks.

=head1 METHODS

=head2 all_package_files_ok

Tests all the relevant items of a Perl package:

=over

=item all_pm_files_ok

=item all_pl_files_ok

=item all_pod_compile_ok

=item all_pod_coverage_ok

=item manifest_ok (not yet)

=back

=head2 manifest_ok

Test the MANIFEST file

=cut
