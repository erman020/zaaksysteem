package TestFor::General::Backend::Rules::Rule::Condition::Area;
use base qw(Test::Class);

use Moose;
use TestSetup;
use File::Spec::Functions qw(catfile);

use Zaaksysteem::Backend::Rules;
use JSON qw/decode_json encode_json/;


=head1 NAME

TestFor::General::Backend::Rules::Rule::Condition::Area - Rule condition: Area

=head1 CODE TESTS

Code tests, testing the implementation itself

=head2 rules_rule_condition_area_wijk

=cut

sub rules_rule_condition_area_aanvrager : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rules_for_area(aanvrager => 1);

        my $case     = $zs->create_case_ok(zaaktype => $casetype);

        my $engine   = Zaaksysteem::Backend::Rules->new_from_case(
            $case,
            {
                'case.number_status'   => 1,
            }
        );

        ### Second condition, with undef values, should be validated true
        is($engine->rules->[$_]->conditions->[0]->{validation_type}, 'fixed', 'Fixed validation type for rule number: ' . $_) for 0..2;

        is($engine->rules->[1]->then->[0]->data->{value}, 'Geen', 'Geen wijk: Condition is none');
        is($engine->rules->[1]->conditions->[0]->{validates_true}, 1, 'Geen wijk: Validates to true');
        is($engine->rules->[2]->then->[0]->data->{value}, 'Oost', 'Oostwijk: Condition is "Oost"');
        is($engine->rules->[2]->conditions->[0]->{validates_true}, 0, 'Oostwijk: Validates to false');

        ### Now create an area for this person in west
        $schema->resultset('Parkeergebied')->create({
            postcode    => $case->aanvrager_object->postcode,
            huisnummer  => $case->aanvrager_object->huisnummer,
            straatnaam  => $case->aanvrager_object->straatnaam,
            parkeergebied => 'West',
        });

        $engine   = Zaaksysteem::Backend::Rules->new_from_case(
            $case,
            {
                'case.number_status'   => 1,
            }
        );

        ### Second condition, with undef values, should be validated false, because we have a parkeergebied
        is($engine->rules->[1]->then->[0]->data->{value}, 'Geen', 'Geen wijk: Condition is none');
        is($engine->rules->[1]->conditions->[0]->{validates_true}, 0, 'Geen wijk: Validates to false');

        ### West should now be validated true
        is($engine->rules->[0]->then->[0]->data->{value}, 'West', 'Westwijk: Condition is "West"');
        is($engine->rules->[0]->conditions->[0]->{validates_true}, 1, 'Westwijk: Validates to true');

        ### Oost should be false
        is($engine->rules->[2]->then->[0]->data->{value}, 'Oost', 'Oostwijk: Condition is "Oost"');
        is($engine->rules->[2]->conditions->[0]->{validates_true}, 0, 'Oostwijk: Validates to false');

    }, 'condition::area: checked area from aanvrager');

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rules_for_area();

        my $case     = $zs->create_case_ok(zaaktype => $casetype);

        ### Now create an area for this person in west
        my $parkeer  = $schema->resultset('Parkeergebied')->create({
            postcode    => '1051JL',
            huisnummer  => 23,
            huisletter  => 'A',
            huisnummertoevoeging => '1rec',
            straatnaam  => 'Donker Curtiusstraat',
            parkeergebied => 'Zuid',
        });

        $zs->create_bag_records;

        my $engine   = Zaaksysteem::Backend::Rules->new_from_case(
            $case,
            {
                'case.number_status'   => 1,
                'attribute.bag_adres'  => 'nummeraanduiding-9876543218375842',
            }
        );

        ### Second condition, with undef values, should be validated true
        is($engine->rules->[0]->then->[0]->data->{value}, 'West', 'Westwijk: Condition is "West"');
        is($engine->rules->[0]->conditions->[0]->{validates_true}, 0, 'Westwijk: Validates to false');
        is($engine->rules->[1]->then->[0]->data->{value}, 'Oost', 'Oostwijk: Condition is "Oost"');
        is($engine->rules->[1]->conditions->[0]->{validates_true}, 0, 'Oostwijk: Validates to false');
        is($engine->rules->[2]->then->[0]->data->{value}, 'Zuid', 'Zuidwijk: Condition is "Oost"');
        is($engine->rules->[2]->conditions->[0]->{validates_true}, 1, 'Zuidwijk: Validates to TRUE :)');

        is($engine->rules->[2]->conditions->[0]->{validation_type}, 'revalidate', '"revalidate" validation type for last rule');

        $parkeer->delete;
        $engine   = Zaaksysteem::Backend::Rules->new_from_case(
            $case,
            {
                'case.number_status'   => 1,
                'attribute.bag_adres'  => 'nummeraanduiding-9876543218375842',
            }
        );

        is($engine->rules->[2]->then->[0]->data->{value}, 'Zuid', 'Zuidwijk: Condition is "Oost"');
        is($engine->rules->[2]->conditions->[0]->{validates_true}, 0, 'Zuidwijk: Validates FALSE :)');
        is($engine->rules->[3]->then->[0]->data->{value}, 'Geen', 'Geen wijk: Condition is "Oost"');
        is($engine->rules->[3]->conditions->[0]->{validates_true}, 1, 'Geen wijk: Validates to TRUE :)');
    }, 'condition::area: checked area from bag_address');

}

sub _generate_rules_for_area {
    my $self            = shift;
    my (%opts)          = @_;

    my $zaaktype_node   = $zs->create_zaaktype_node_ok;
    my $casetype        = $zs->create_zaaktype_ok(node => $zaaktype_node);
    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );
    $zs->create_zaaktype_status_ok(
        status => 3,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    ### Generate zaaktype_kenmerken, checkboxes
    my $zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'checkbox_wijken',
            magic_string    => 'checkbox_wijken',
            value_type      => 'checkbox',
            values          => [
                {
                    value   => 'Noord',
                    active  => 1,
                },
                {
                    value   => 'Oost',
                    active  => 1,
                },
                {
                    value   => 'Zuid',
                    active  => 1,
                },
                {
                    value   => 'West',
                    active  => 1,
                },
                {
                    value   => 'Geen',
                    active  => 1,
                }
            ]
        )
    );

    ### Generate zaaktype_kenmerken, checkboxes
    my $bag_adres = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'bag_adres',
            magic_string    => 'bag_adres',
            value_type      => 'bag_adres',
        )
    );

    $zs->create_zaaktype_regel_ok(
        status  => $zaaktype_status,
        # node => $zaaktype_node,
        naam    => 'Rule check: aanvrager_wijk ',
        settings => {
            'voorwaarde_1_kenmerk'          => 'aanvrager_wijk',
            'voorwaarde_1_value'            => 'West',
            'voorwaarde_1_value_checkbox'   => '1',
            'voorwaarden'                   => '1',


            'actie_1'                       => 'vul_waarde_in',
            'actie_1_kenmerk'               => $zt_kenmerk->bibliotheek_kenmerken_id->id,
            'actie_1_value'                 => 'West',
            'acties'                        => '1',

            'naam'                          => 'Vul west in',
        }
    );

    if ($opts{aanvrager}) {
        $zs->create_zaaktype_regel_ok(
            status  => $zaaktype_status,
            # node => $zaaktype_node,
            naam    => 'Rule check: aanvrager_wijk_empty ',
            settings => {
                'voorwaarde_1_kenmerk'          => 'aanvrager_wijk',
                'voorwaarde_1_value_checkbox'   => '1',
                'voorwaarden'                   => '1',


                'actie_1'                       => 'vul_waarde_in',
                'actie_1_kenmerk'               => $zt_kenmerk->bibliotheek_kenmerken_id->id,
                'actie_1_value'                 => 'Geen',
                'acties'                        => '1',

                'naam'                          => 'Vul "Geen" in',
            }
        );
    }

    $zs->create_zaaktype_regel_ok(
        status  => $zaaktype_status,
        # node => $zaaktype_node,
        naam    => 'Rule check: aanvrager_wijk ',
        settings => {
            'voorwaarde_1_kenmerk'          => 'aanvrager_wijk',
            'voorwaarde_1_value'            => 'Oost',
            'voorwaarde_1_value_checkbox'   => '1',
            'voorwaarden'                   => '1',


            'actie_1'                       => 'vul_waarde_in',
            'actie_1_kenmerk'               => $zt_kenmerk->bibliotheek_kenmerken_id->id,
            'actie_1_value'                 => 'Oost',
            'acties'                        => '1',

            'naam'                          => 'Vul oost in',
        }
    );

    $zs->create_zaaktype_regel_ok(
        status  => $zaaktype_status,
        # node => $zaaktype_node,
        naam    => 'Rule check: bag_kenmerk',
        settings => {
            'voorwaarde_1_kenmerk'          => $bag_adres->bibliotheek_kenmerken_id->id . '-wijk',
            'voorwaarde_1_value'            => 'Zuid',
            'voorwaarde_1_value_checkbox'   => '1',
            'voorwaarden'                   => '1',


            'actie_1'                       => 'vul_waarde_in',
            'actie_1_kenmerk'               => $zt_kenmerk->bibliotheek_kenmerken_id->id,
            'actie_1_value'                 => 'Zuid',
            'acties'                        => '1',

            'naam'                          => 'Vul zuid in',
        }
    );

    if (!$opts{aanvrager}) {
        $zs->create_zaaktype_regel_ok(
            status  => $zaaktype_status,
            # node => $zaaktype_node,
            naam    => 'Rule check: bag_kenmerk empty',
            settings => {
                'voorwaarde_1_kenmerk'          => $bag_adres->bibliotheek_kenmerken_id->id . '-wijk',
                'voorwaarde_1_value_checkbox'   => '1',
                'voorwaarden'                   => '1',


                'actie_1'                       => 'vul_waarde_in',
                'actie_1_kenmerk'               => $zt_kenmerk->bibliotheek_kenmerken_id->id,
                'actie_1_value'                 => 'Geen',
                'acties'                        => '1',

                'naam'                          => 'Vul geen in',
            }
        );
    }

    return ($casetype, $zaaktype_status);
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
