package TestFor::General::StUF::GeneralInstance;
use base qw(Test::Class);

use TestSetup;
use File::Spec::Functions qw(catfile);

use Math::BigFloat;

use Zaaksysteem::StUF::GeneralInstance;

sub stuf_general_retrieve_xml_string : Tests {
    my $class = Dummy::GeneralInstance->new;

    throws_ok(
        sub { $class->retrieve_xml_string },
        qr/Validation of profile fail/,
    );

    throws_ok(
        sub { $class->retrieve_xml_string({ xml => ['bla'] }) },
        qr/option does not contain plain xml/,
    );

    throws_ok(
        sub { $class->retrieve_xml_string({ xml => '' }) },
        qr/Validation of profile fail/,
    );

    my $xml;
    lives_ok(
        sub { $xml = $class->retrieve_xml_string({ xml => '<xml>' }) },
        'Got valid xml from string'
    );

    is($xml, '<xml>', 'Retrieved valid xml');

    throws_ok(
        sub { $class->retrieve_xml_string({ filename => catfile(STUF_TEST_XML_PATH, '0204', 'prs/nonexistent') }) },
        qr/Could not open file.*nonexistent/,
    );

    lives_ok(
        sub { $xml = $class->retrieve_xml_string({ filename => catfile(STUF_TEST_XML_PATH, '0204', 'prs/101-prs-create-tinus.xml') }) },
        'Got valid xml from filename'
    );

    like($xml, qr/PRS/, 'Retrieved valid xml from filename');
}

sub stuf_general_retrieve_xml_element : Tests {
    my $class = Dummy::GeneralInstance->new;

    throws_ok(
        sub { $class->retrieve_xml_element },
        qr/Validation of profile fail/,
    );

    throws_ok(
        sub { $class->retrieve_xml_element({ method => 'bla' }) },
        qr/Validation of profile fail/,
        'Did not pass inherited retrieve_xml_string validation'
    );

    throws_ok(
        sub { $class->retrieve_xml_element({ filename => catfile(STUF_TEST_XML_PATH, '0204', 'prs/101-prs-create-tinus.xml') }) },
        qr/Validation of profile fail/,
        'Did not pass validation of require_some: element or method'
    );

    throws_ok(
        sub {
            $class->retrieve_xml_element(
                {
                    filename    => catfile(STUF_TEST_XML_PATH, '0204', 'prs/101-prs-create-tinus.xml'),
                    method      => 'kennisgevingsberichtt'
                }
            )
        },
        qr/Given method "kennisgevingsberichtt" was not found/,
    );

    my ($element) = $class->retrieve_xml_element(
        {
            filename    => catfile(STUF_TEST_XML_PATH, '0204', 'prs/101-prs-create-tinus.xml'),
            method      => 'kennisgevingsbericht'
        }
    );

    isa_ok($element, 'XML::LibXML::Element');
}

sub stuf_general_plaintext_value : Tests {
    my $class = Dummy::GeneralInstance->new;

    ok(!ref $class->plaintext_value({ '_' => 'jadda' }), 'Got plain text value for { "_" => "jadda" } values');
    is($class->plaintext_value({ '_' => 'jadda' }), 'jadda', 'Got jadda for { "_" => "jadda" } values');

    is($class->plaintext_value('Return this'), 'Return this', 'Got response for normal values');
    ok(grep(/^[12]$/, @{ $class->plaintext_value([1,2]) }), 'Got response for array values');

    is($class->plaintext_value({ dos => "2"})->{dos}, 2, 'Got response for normal HASH values');
    is($class->plaintext_value(Math::BigFloat->new(242424)), 242424, 'Got response for Math::BigFloat');

    is($class->plaintext_value({ '_' => Math::BigFloat->new(242424) }), 242424, 'Check for blessed value in value');

    throws_ok(
        sub {
            $class->plaintext_value(bless({}, 'Unkown'))
        },
        qr/Don't know how to return a string/,
    );


}

sub stuf_general_get_params_by_definition : Tests {
    my $class = Dummy::GeneralInstance->new;

    is_deeply(
        $class->get_params_by_definition(
            {
                hash => {
                    person => {
                        firstname => 'Wim',
                        lastname  => 'de Bie',
                        birthdate => Math::BigFloat->new(19830610),
                    }
                },
                definition => [
                    {
                        path    => '$.person.firstname',
                        name    => 'first_name'
                    },
                    {
                        path    => '$.person.lastname',
                        name    => 'last_name'
                    },
                    {
                        path    => '$.person.birthdate',
                        name    => 'geboortedatum'
                    }
                ]
            }
        ),
        {
            first_name      => 'Wim',
            last_name       => 'de Bie',
            geboortedatum   => '19830610'
        }
    );
}

package Dummy::GeneralInstance;

use Moose;
with 'Zaaksysteem::StUF::GeneralInstance';

has 'elements' => (
    is  => 'rw',
    default => sub {
        return     [
            {
                element  => '{http://www.egem.nl/StUF/sector/bg/0204}kennisgevingsBericht',
                compile  => 'RW',
                method   => 'kennisgevingsbericht',
            }
        ];
    }
);

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

