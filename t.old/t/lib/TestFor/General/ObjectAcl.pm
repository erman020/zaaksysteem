package TestFor::General::ObjectAcl;
use base qw(ZSTest);

use TestSetup;

use Zaaksysteem::Object::Model;

sub acl_crud : Tests {
    $zs->zs_transaction_ok(sub {
        my $acls = $schema->resultset('ObjectAclEntry');

        my $count = $acls->count;

        my $auth = $zs->create_zaaktype_authorisation_ok;

        is $acls->count, $count + 2, 'Implicitly created ACL entry through zaaktype_authorisation creation';
    }, 'object/acl creating zaaktype creates acl entries');

    # ZS-2157 - ACLs must be deleted with an object deletion
    $zs->zs_transaction_ok(sub {
        my $auth = $zs->create_zaaktype_authorisation_ok;

        my $acls = $schema->resultset('ObjectAclEntry');

        my $total_count = $acls->count;

        my $objects = $zs->object_model;

        my $object = $objects->rs->find({ object_class => 'casetype', object_id => $auth->get_column('zaaktype_id') });

        isa_ok $object, 'Zaaksysteem::Backend::Object::Data::Component', 'object found';

        lives_ok sub { $object->delete }, 'object/acl object with acls deletes ok';

        is $acls->count, ($total_count - 2), 'object/acl acls implicitly deleted through cascade';
    });
}

# TODO: Extend tests when create_case_ok doesn't fail

sub without_user : Tests {
    # The following two tests 'prove' the most important aspects of ObjectAcl
    # interactions. The proof lies with the subquery being properly generated.
    # The 'properness' of the subquery is of course my interpretation.
    $zs->zs_transaction_ok(sub {
        lives_ok(
            sub { Zaaksysteem::Object::Model->new( schema => $schema )->rs },
            'object/acl object returns resultset'
        );
    }, 'object/acl returns resultset without user');
}

sub with_user : Tests {
    $zs->zs_transaction_ok(sub {
        my $user = $zs->create_subject_ok;
        my $case = $zs->create_case_ok;

        my $groupnames = $zs->object_model->rs->_get_groupnames;
        is_deeply($groupnames, ['public'], "Got all the groupnames");

        my $object_user = Zaaksysteem::Object::Model->new(
            schema => $schema,
            user   => $user,
        );

        my $rs = $object_user->new_resultset;
        is($rs->{attrs}{zaaksysteem}{user}->id, $user->id, "Have the same user via ->new_resultset()");

        {
            $rs = $object_user->rs;
            is($rs->{attrs}{zaaksysteem}{user}->id, $user->id, "Have the same user via ->rs()");

            $rs = $rs->search_rs(undef, { foo => 'bar' });
            is($rs->{attrs}{zaaksysteem}{user}->id, $user->id, "Have the same user via ->search_rs()");

            $rs = $rs->search(undef, { foo => 'bar' });
            is($rs->{attrs}{zaaksysteem}{user}->id, $user->id, "Have the same user via ->search()");
        }

        my $results = $rs->search_rs({ object_class => 'case' });

        my $ok = is($results->count, 1, 'object/acl/casetype user can find object after sync');

        my $other_user = $zs->create_subject_ok(group_ids => [ 6 ], role_ids => [ 12 ]);
        $object_user = Zaaksysteem::Object::Model->new(
            schema => $schema,
            user   => $other_user,
        );

        is $object_user->rs->search({ object_class => 'case' })->count, 0, 'object/acl/casetype user cant find object';


    }, 'object/acl ACLs actually hide objects in resultset');

    # TODO: Extend tests here with a zs_transaction_ok that checks multi-object
    # permissions
    # This is not yet possible because of limitations in LDAP mocking (every
    # user has every role, so you can't contrast one user's result to another)
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
