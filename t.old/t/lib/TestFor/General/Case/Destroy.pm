package TestFor::General::Case::Destroy;
use base qw(Test::Class);
use TestSetup;

sub count_cases {
    return $schema->resultset('Zaak')->search_extended->count;
}

sub check_destroy : Tests {

    $zs->set_current_user(username => 'admin');

    $zs->zs_transaction_ok(sub {

        my $start_count = count_cases;

        my $case = $zs->create_case_ok();

        is count_cases, $start_count + 1, 'one new case in the pocket';

        $schema->resultset('Zaak')->destroy_cases;

        is count_cases, $start_count + 1, 'still one case in the pocket, case wasnt deletable';
    }, 'Test undeletable cases are not destroyed');

    # need transaction because pesky moose caches retain can_delete status
    $zs->zs_transaction_ok(sub {

        my $start_count = count_cases;
        my $case = $zs->create_case_ok();

        # do the groundwork for deletion
        $case->status('resolved');
        $case->vernietigingsdatum(DateTime->now->add(seconds => -1));
        $case->update;

        is count_cases, $start_count + 1, 'we got one new case';
        ok $case->can_delete,'which can be deleted';

        $schema->resultset('Zaak')->destroy_cases;

        is $schema->resultset('Zaak')->search({deleted => undef})->count, $start_count, 'case is deleted now';

    }, 'Test: Deletable case will be destroyed');
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

