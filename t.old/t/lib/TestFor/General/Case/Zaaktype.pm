package TestFor::General::Case::Zaaktype;
use base qw(Test::Class);

use TestSetup;
use BTTW::Tools;
use Zaaksysteem::Zaken::DelayedTouch;

sub zaak_zaaktype_wijzigen : Tests {
    $zs->zs_transaction_ok(
        sub {
            $zs->set_current_user;

            my $fremium = $zs->create_zaaktype_predefined_ok();
            my $premium = $zs->create_zaaktype_predefined_ok();

            $premium->zaaktype_node_id->zaaktype_definitie_id->update({
                afhandeltermijn => 2,  # default 10 (in testsuite)
                servicenorm     => 1,  # default 5  (in testsuite)
            });

            my $opts = { };
            my $case = $zs->create_case_ok(zaaktype => $fremium);
            $case = change_casetype($case, $premium, $opts);
            $case = change_casetype($case, $fremium, $opts);
            $case = change_casetype($case, $premium, $opts);

            $case = change_casetype($case, $fremium, $opts, 3);
            $case = change_casetype($case, $premium, $opts, 3);
            $case = change_casetype($case, $fremium, $opts, 3);

            $case = change_casetype($case, $premium, $opts, 4);
            $case = change_casetype($case, $fremium, $opts, 4);
            $case = change_casetype($case, $premium, $opts, 4);
        },
        'Change case type for a case',
    );
}

sub zs_zaak_zaaktype_wijzigen_documentlabels : Tests {
    $zs->txn_ok(
        sub {
            # Setup the zaaktypes
            my $zt_met_doc    = $zs->create_zaaktype_predefined_ok;
            my $zt_zonder_doc = $zs->create_zaaktype_predefined_ok;
            my $zt_met_doc_2  = $zs->create_zaaktype_predefined_ok;

            my $document_kenmerk = $zs->create_bibliotheek_kenmerk_ok(
                value_type => 'file',
            );

            my $ztk_d  = _add_kenmerk_to_zaaktype($zt_met_doc, $document_kenmerk);
            my $ztk_d2 = _add_kenmerk_to_zaaktype($zt_met_doc_2, $document_kenmerk);

            my $case = $zs->create_case_ok(zaaktype => $zt_met_doc);
            my $case_document = $zs->create_file_ok(
                db_params => {
                    accepted => 1,
                    case => $case,
                },
                case_document_ids => [ $ztk_d->id ],
            );

            my $cd     = $case_document->case_documents->first;
            my $ztk    = $cd->case_document_id;
            my $bieb   = $ztk->bibliotheek_kenmerken_id;

            $case = change_casetype($case, $zt_zonder_doc);
            is($case_document->case_documents->count, 0, "No case documents after wijzig_zaaktype");

            {
                my $case = $zs->create_case_ok(zaaktype => $zt_met_doc);
                my $case_document = $zs->create_file_ok(
                    db_params => {
                        accepted => 1,
                        case => $case,
                    },
                    case_document_ids => [ $ztk_d->id ],
                );
                $case = change_casetype($case, $zt_met_doc_2);
                is($case_document->case_documents->count, 1, "Still have a case document");
                my $cd = $case_document->case_documents->first;
                my $ztk_2   = $cd->case_document_id;
                my $bieb_2  = $ztk_2->bibliotheek_kenmerken_id;

                isnt($ztk_2->id, $ztk->id, "Not the same kenmerk");
                is($bieb_2->id, $bieb->id, "Same bieb ID");
            }

        },
        "ZS-5748: Zaaktype wijzigen leidt tot lege documentlabels"
    );
}

sub _add_kenmerk_to_zaaktype {
    my ($zt, $k) = @_;
    my $node = $zt->zaaktype_node_id;
    my ($status) = $node->zaaktype_statussen;
    return $zs->create_zaaktype_kenmerk_ok(
        status              => $status,
        bibliotheek_kenmerk => $k,
    );
}

sub change_casetype {
    my ($zaak, $zaaktype, $opts, $milestone) = @_;
    $opts //= {};

    $zaak->update({milestone => $milestone}) if $milestone;

    my $case = $zaak->wijzig_zaaktype({
        zaaktype_id => $zaaktype->id,
        %$opts
    });

    subtest 'zaaktype_has_changed' => sub {
        is($case->id, $zaak->id, "Case ID is the same");

        is_deeply(
            {
                node     => $case->zaaktype_node_id->id,
                zaaktype => $case->zaaktype_id->id,
            },
            {
                node     => $zaaktype->zaaktype_node_id->id,
                zaaktype => $zaaktype->id,
            },
            "Zaaktype is gewijzigd"
        );

        is(
            $case->streefafhandeldatum,
            $case->registratiedatum->add({
                days => $zaaktype->zaaktype_node_id->zaaktype_definitie_id->servicenorm
            }),
            "Streefafhandeldatum is correct",
        );
    };

    return $case;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

