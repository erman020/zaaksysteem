package TestFor::Testsuite;
use base 'ZSTest';

use TestSetup;

sub zs_create_natuurlijk_persoon_ok : Tests {
    $zs->txn_ok(
        sub {
            my $profile = Zaaksysteem::TestUtils::AANVRAGER_NP_PROFILE();

            delete $profile->{defaults}{contactgegevens};

            my %info;
            foreach (keys %{ $profile->{defaults} }) {
                $info{"np-$_"} = ref $profile->{defaults}{$_} eq 'CODE' ? $profile->{defaults}{$_}->() : $profile->{defaults}{$_};
            }

            $info{'npc-email'} = 'devnull@zaaksysteem.nl';
            my $aanvrager = $zs->create_aanvrager_np_ok(burgerservicenummer => $info{'np-burgerservicenummer'});
            my $expect    = {
                create          => \%info,
                betrokkene_type => 'natuurlijk_persoon',
                verificatie     => 'medewerker',
            };

            is_deeply($aanvrager, $expect, "Aanvrager is ok");

            my %args = map { $_ => 1 } keys %{ $profile->{optional} };
            $expect = {
                create          => \%args,
                betrokkene_type => 'natuurlijk_persoon',
                verificatie     => 'medewerker',
            };

            my $np  = $zs->create_natuurlijk_persoon_ok();
            isa_ok($np, "Zaaksysteem::Model::DB::NatuurlijkPersoon");

            my $dt = DateTime->new(
                year      => 1940,
                month     => 5,
                day       => 16,
                hour      => 0,
                minute    => 0,
                second    => 0,
                time_zone => "floating",
            );
            $np  = $zs->create_natuurlijk_persoon_ok(geboortedatum => $dt);
            my $dob = $np->geboortedatum->set_time_zone('UTC')->set_time_zone('Europe/Amsterdam')->iso8601;
            is($dob, "1940-05-16T02:00:00", "Able to convert to timestamp");

        },
        "Create aanvrager natuurlijk persoon ok"
    );
}

1;

__END__

=head1 NAME

TestFor::Testsuite::NatuurlijkPersoon - Test all basic testsuite functions of natuurlijk persoon

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
