package TestFor::Catalyst::Controller::API::V1::App::BBV;
use base qw(ZSTest::Catalyst Zaaksysteem::API::V1::Roles::TestPreperation);

use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller::API::V1::App::BBV - APP BBV tests

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/APP/BBV.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/app/bbv> namespace.

=head1 USAGE

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 wms_layers

url: /api/v1/app/bbv/update_attributes

=cut

sub cat_api_v1_update_values : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech        = $zs->mech;
        $mech->zs_login;

        $zs->create_interface_ok(
            module => 'bbvapp',
            name   => 'BBV App',
            active => 1,
            interface_config => {
                access => 'rw',
            }
        );

        my $casetype_data = $self->api_test_zaaktype;
        my $casetype      = $casetype_data->{casetype};
        my $zaak          = $zs->create_case_ok(zaaktype => $casetype);

        my $json          = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/app/bbvapp/case/' . $zaak->object_data->uuid . '/update_attributes',
            {
                values  => {
                    test_comment => ['test1'],
                }
            }
        );

        is($json->{result}->{type}, 'case', "Got case as response");
        my $case            = $json->{result}->{instance};

        ok($case->{attributes}, 'Got attributes');
        ok($case->{attributes}->{test_comment}, 'Got attribute "test_comment"');
        is($case->{attributes}->{test_comment}->[0], 'test1', 'Attribute "test_comment" succesfully updated');
    }, 'api/v1/app/bbv/update_attributes: Update attributes without permissions');

    $zs->zs_transaction_ok(sub {
        my $mech        = $zs->mech;
        $mech->zs_login;

        $zs->create_interface_ok(
            module => 'bbvapp',
            name   => 'BBV App',
            active => 1,
            interface_config => {
                access => 'rw',
            }
        );

        my $casetype_data = $self->api_test_zaaktype({ attribute_permissions => 1 });
        my $casetype      = $casetype_data->{casetype};
        my $zaak          = $zs->create_case_ok(zaaktype => $casetype);

        my $json          = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/app/bbvapp/case/' . $zaak->object_data->uuid . '/update_attributes',
            {
                values  => {
                    test_comment => ['test1'],
                }
            }
        );

        is($json->{result}->{type}, 'case', "Got case as response");
        my $case            = $json->{result}->{instance};

        $json          = $mech->post_json(
            $mech->zs_url_base . '/api/v1/app/bbvapp/case/' . $zaak->object_data->uuid . '/update_attributes',
            {
                values  => {
                    test_conclusion => ['test1'],
                }
            }
        );

        $json           = $mech->_get_json_as_perl($mech->content);

        is($json->{result}->{type}, 'exception', "Got an exception, not proper permission");
        like($json->{result}->{instance}->{message}, qr/You do not have the proper rights to update kenmerk: test_conclusion/, 'Permission invalid');
    }, 'api/v1/app/bbv/update_attributes: Update attributes with permissions');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
