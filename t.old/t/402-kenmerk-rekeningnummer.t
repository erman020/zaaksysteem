#! perl
use TestSetup;
initialize_test_globals_ok;

use Zaaksysteem::Constants;

$zs->zs_transaction_ok(sub {
    my $kenmerk = $zs->create_bibliotheek_kenmerk_ok(value_type => 'bankaccount');

    ok $kenmerk->id, "Allowed to create kenmerk with type 'bankaccount'";

    ok exists ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{bankaccount}, "Configuration for bankaccount present in Constants";

}, 'Kenmerk type rekeningnummer');


zs_done_testing();
