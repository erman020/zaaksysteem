user nobody nogroup;
worker_processes auto;          # auto-detect number of logical CPU cores

events {
    worker_connections 512;       # set the max number of simultaneous connections (per worker process)
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    server_tokens off;

    keepalive_timeout  65;

    limit_conn_zone $host zone=perserver:10m;

    gzip  on;
    gzip_http_version 1.0;
    gzip_comp_level 2;
    gzip_proxied any;
    gzip_vary off;
    gzip_types text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript application/json;
    gzip_min_length  1000;
    gzip_disable     "MSIE [1-6]\.";

    server_names_hash_bucket_size 128;
    types_hash_max_size 2048;
    types_hash_bucket_size 64;

    log_format logstash_json '{ "@timestamp": "$time_iso8601", '
                            '"zs_component": "zaaksysteem-frontend", '
                            '"zs_pod_name": "$hostname", '
                            '"message": "$request", '
                            '"req": { '
                            '"remote_addr": "$remote_addr", '
                            '"hostname": "$host", '
                            '"instance_hostname": "$upstream_http_zs_req_instance_hostname", '
                            '"request_id": "$upstream_http_zs_req_id", '
                            '"session_id": "$upstream_http_zs_req_session_id", '
                            '"action_path": "$upstream_http_zs_req_action_path", '
                            '"zs_version": "$upstream_http_zs_version", '
                            '"remote_user": "$upstream_http_zs_req_user", '
                            '"body_bytes_sent": $body_bytes_sent, '
                            '"status": $status, '
                            '"request_uri": "$request_uri", '
                            '"request_method": "$request_method", '
                            '"request_time": $request_time, '
                            '"upstream_time": $upstream_http_zs_upstream_time, '
                            '"http_referrer": "$http_referer", '
                            '"http_user_agent": "$http_user_agent", '
                            '"ssl_protocol":"$ssl_protocol", '
                            '"ssl_cipher":"$ssl_cipher"'
                            '} }';
    access_log /dev/stdout logstash_json;

    # When proxy tells us via "X-Forwarded-Proto: https", set the variable $fe_https to "on"
    # default to off.
    map $http_x_forwarded_proto $fe_https {
      default off;
      https on;
    }
    
    server {
        listen 80 default_server;
        server_name zaaksysteem;        # Mandatory to make limit_conn work...
        error_log /dev/stderr debug;

        include /etc/nginx/security.conf;
        include /etc/nginx/zaaksysteem.conf;
        include /etc/nginx/error_pages.conf;

        location ^~ /\.ht {
            deny all;
        }
    }

    server {
        listen 443 ssl http2 default_server;
        server_name zaaksysteem;        # Mandatory to make limit_conn work...
        error_log /dev/stderr debug;

        ### Make sure fastcgi knows were talking https here, see X-Forwarded-Proto above
        set $fe_https on;

        include /etc/nginx/security.conf;
        include /etc/nginx/zaaksysteem.conf;
        include /etc/nginx/error_pages.conf;

        location ^~ /\.ht {
            deny all;
        }
    }
}
