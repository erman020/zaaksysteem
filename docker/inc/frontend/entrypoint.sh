#!/bin/sh

#
# USAGE: ZAAKSYSTEEM_API_HOST=zaysteem-api.default.svc.cluster.local ./entrypoint.sh
#

cp zaaksysteem.conf.tpl zaaksysteem.conf

DNS_RESOLVER=$(grep nameserver /etc/resolv.conf| head -n 1| awk '{ print $2; }');

sed -i \
    -e "s/ZAAKSYSTEEM_HOST_API/${ZAAKSYSTEEM_HOST_API-backend}/" \
    -e "s/ZAAKSYSTEEM_HOST_CSV/${ZAAKSYSTEEM_HOST_CSV-api2csv}/" \
    -e "s/ZAAKSYSTEEM_HOST_SWAGGER/${ZAAKSYSTEEM_HOST_SWAGGER-swaggerui}/" \
    -e "s/ZAAKSYSTEEM_HOST_BBV/${ZAAKSYSTEEM_HOST_BBV-bbvproxy}/" \
    -e "s/DNS_RESOLVER/${DNS_RESOLVER}/" \
    -e "s/CONNECTION_PER_INSTANCE_LIMIT/${CONNECTION_PER_INSTANCE_LIMIT-10}/" \
        zaaksysteem.conf

exec "$@"
