package Mail::Track::Message;
use Moose;

with 'MooseX::Log::Log4perl';

use autodie qw(open);
use Email::Address;
use Email::Sender::Simple;
use Email::Sender::Transport::SMTP;
use Encode qw(encode decode);
use File::Basename;
use File::MMagic;
use IO::All;
use File::Spec::Functions qw(catfile);
use File::Temp qw/ :seekable /;
use MIME::Base64 qw(encode_base64 decode_base64);
use MIME::Parser;
use Mail::Track::Message::Attachment;
use Mail::Track::Message::Body;

=head1 NAME

Mail::Track::Message - Mail message object

=head1 SYNOPSIS

    my $message     = Mail::Track::Message->new(
        # Both are optional
        message               => $content,
        subject_prefix_regex  => qr/some regexp/,
    );

    my $attachments = $mailer->attachments;
    my $string_body = $mailer->body;


=head1 DESCRIPTION

Returned message object for L<Mail::Track>, please DO NOT USE THIS
OBJECT DIRECTLY. Use L<Mail::Track>

=head1 ATTRIBUTES

=head2 extra_headers

A hash containing extra headers to add to the email message.

=cut

has extra_headers => (
    is      => 'ro',
    isa     => 'HashRef',
    default => sub { {} },
);

=head2 subject_prefix_regex

For documentation see L<Mail::Track/subject_prefix_regex>

=cut

has subject_prefix_regex => (
    is      => 'rw',
    isa     => 'RegexpRef',
    lazy    => 1,
    default => sub { return qr/.+/ },
);

=head2 subject_prefix_name

For documentation see L<Mail::Track/subject_prefix_name>

=cut

has subject_prefix_name => (
    is      => 'rw',
    isa     => 'Str',
    default => sub { return '' },

);

=head2 sender

TODO: Improve docs

=cut

has sender => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return Email::Sender::Simple->new();
    },
);

=head2 transport

An L<Email::Sender::Transport> object, defaults to a L<Email::Sender::Transport::SMTP> object.

=cut

has transport => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        my $self = shift;

        my %options;
        if ($ENV{SMTP_SERVER}) {
            $options{host} = $ENV{SMTP_SERVER};
        }
        if ($ENV{SMTP_PORT}) {
            $options{port} = $ENV{SMTP_PORT};
        }

        return Email::Sender::Transport::SMTP->new(%options);
    },
);

=head2 message

isa: Str

MIME Message to parse

=cut

has message => (
    is      => 'rw',
    isa     => 'Str',
    trigger => \&parse_message,
);

=head2 tmpdir

Temporarily directory to save parts into.

Defaults to a random dir in /tmp, gets cleaned up if the object goes out of scope.

=head3 ARGUMENTS

=over

=item File::Temp::Dir object

=back

=cut

has tmpdir => (
    is      => 'ro',
    lazy    => 1,
    isa     => 'File::Temp::Dir',
    default => sub {
        my $ft = File::Temp->new();
        return $ft->newdir(DIR => '/tmp');
    },
);

=head2 entity

isa: MIME::Message

Result object of the mime parser

=cut

has entity => (
    is  => 'rw',
    isa => 'MIME::Entity',
);

=head2 parser

isa: MIME::Parser

Parser object for this MIME::Message
The default object extracts uuencode and does NOT parse nested messages.
Nested messages are parsed seperatly and can be retrieved by the L<nested_messages> method.

=cut

has parser => (
    is      => 'ro',
    isa     => 'MIME::Parser',
    lazy    => 1,
    default => sub {
        my $self   = shift;
        my $parser = MIME::Parser->new;
        $parser->extract_uuencode(1);
        $parser->output_under($self->tmpdir);
        $parser->parse_nested_messages(0);
        return $parser;
    },
);

=head2 identifier

isa: Str

Returns the identifier of this object

=cut

has identifier => (
    is      => 'rw',
    isa     => 'Maybe[Str]',
    lazy    => 1,
    default => sub {
        my $self = shift;

        my $regex = $self->subject_prefix_regex;

        unless ($self->subject_raw =~ /$regex/) {
            return;
        }

        my ($before, $id, $subject) = $self->subject_raw =~ /$regex/;

        return if (!defined $subject);
        $self->subject($before . $subject);

        return $id;
    }
);

=head2 to

isa: str

Return the recipient for this message

=cut

has to => (
    is      => 'rw',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->get_from_entity_head('to');
    }
);

=head2 from

isa: str

Result object of the mime parser

=cut

has from => (
    is      => 'rw',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->get_from_entity_head('from');
    }
);

=head2 date

Result object of the mime parser

=cut

has date => (
    is      => 'rw',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->get_from_entity_head('date');
    }
);

=head2 cc

Result object of the mime parser

=cut

has cc => (
    is      => 'rw',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->get_from_entity_head('cc');
    }
);

=head2 bcc

Result object of the mime parser

=cut

has bcc => (
    is      => 'rw',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->get_from_entity_head('bcc');
    }
);

=head2 subject

isa: str

Result object of the mime parser

=cut

has subject => (
    is      => 'rw',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;

        my $regex = $self->subject_prefix_regex;

        unless ($self->subject_raw =~ /$regex/) {
            return $self->subject_raw;
        }

        my ($before, $id, $subject) = $self->subject_raw =~ /$regex/;

        $self->identifier($id);

        return ($before // '') . ($subject // '');
    }
);


=head2 identifier_regex

isa: Str

The regex (string) for the identifier part of the subject string. It defaults to C<#(\d+\-\d+\-\d+[A-Z])>,
which enables subject parsing like '#1234-4892-4390A';

=cut

has 'identifier_regex' => (
    'is'      => 'rw',
    'isa'     => 'RegexpRef',
    'default' => sub { return qr/#(\d+\-\d+\-\d+[A-Z])/; },
);

=head2 subject_raw

isa: str

Result object of the mime parser

=cut

has 'subject_raw' => (
    'is'      => 'rw',
    'isa'     => 'Str',
    'lazy'    => 1,
    'default' => sub {
        my $self = shift;
        return $self->get_from_entity_head('subject');
    }
);

=head2 attachments

isa: Maybe[ArrayRef[Mail::Track::Message::Attachment]]

returns an ArrayRef of attachments (when there are)

=cut

has 'attachments' => (
    'is'      => 'rw',
    'isa'     => 'Maybe[ArrayRef[Mail::Track::Message::Attachment]]',
    'default' => sub { [] },
);

=head2 nested_messages

=head3 RETURNS

An ArrayRef of Mail::Track::Message::Attachement objects, could be empty, which means there are not nested messages;

=cut

has nested_messages => (
    is      => 'rw',
    isa     => 'Maybe[ArrayRef[Mail::Track::Message]]',
    default => sub { return [] },
);


=head2 body

isa: 'Str'

Returns the body as string

=cut

has body => (
    is    => 'rw',
    isa   => 'Maybe[Mail::Track::Message::Body]',
    lazy  => 1,
    default => sub {
        return Mail::Track::Message::Body->new();
    },
);

=head2 body_parts

isa: 'ArrayRef'

All body parts for this e-mail.

=cut

has 'body_parts' => (
    'is'      => 'rw',
    'isa'     => 'ArrayRef',
    'default' => sub { []; }
);

has encoding => (
    is => 'ro',
    isa => 'Str',
    default => sub {
        my $self = shift;
        return 'UTF-8';
    },
);

=head2 dkim

A L<Mail::DKIM::Signer> instance, used to add a DKIM signature to the outgoing
email.

=cut

has dkim => (
    is => 'ro',
    isa => 'Mail::DKIM::Signer',
);

=head1 METHODS

=head2 get_from_entity_head

A method to get Header information from an MIME::Entity object, more specificly, our own object.

=cut

sub get_from_entity_head {
    my ($self, $param) = @_;
    return '' if (!$self->entity || !$self->entity->head);

    my $text = $self->entity->head->get($param);
    return '' if (!defined $text);
    chomp($text);

    return decode('MIME-Header', $text);
}

=head2 $message->parse_message()

Will parse the given C<< $self->message >>, so we have a body and attachments
in the end.

=cut

sub parse_message {
    my $self = shift;

    return unless $self->message;

    my $temp_handle = File::Temp->new();

    print $temp_handle encode('UTF-8', $self->message);

    $temp_handle->seek( 0, SEEK_SET );

    $self->entity($self->parser->parse($temp_handle));

    my @parts = scalar($self->entity->parts) ? $self->entity->parts : $self->entity;

    map { $self->_parse_part($_) } @parts;
}

=head2 add_body(\%OPTIONS)

Return value: $TRUE_OR_FALSE

    $msg->add_body(
        {
            content         => 'This is a text string',
            content_type    => 'text/plain',
        }
    ),

Adds a body to the message.

B<Options>

=over 4

=item content

The string content of the body, could be either a text string or an html string.

=item content_type

The content type of the body, possible values are: C<<text/plain>> or C<<text/html>>. The default is text/plain.

=back

=cut

use constant ADD_BODY_PROFILE => {
    required           => [qw/content/],
    optional           => [qw(content_type charset)],
    defaults           => { content_type => 'text/plain' },
    constraint_methods => { content_type => qr/^text\/html|text\/plain$/, }
};

sub add_body {
    my $self = shift;

    ### Parameter validation
    my $dv = Data::FormValidator->check(shift, ADD_BODY_PROFILE);

    die('Missing or invalid params, need content or content_type')
        unless $dv->success;

    my $params = $dv->valid;

    if ($params->{content_type} eq 'text/plain') {
        $self->body->as_text($params->{content});
    }
    elsif ($params->{content_type} eq 'text/html') {
        $self->body->as_html($params->{content});
    }

    return 1;
}

=head2 add_attachment

Add an attachment to the mail.

=head3 SYNOPSIS

    $msg->add_attchment(
       filename => $filename,
       path     => "/path/to/$filename",
       mimetype => "image/gif",
    );

=head3 ARGUMENTS

=over

=item * path

Required. The full path to the attachment.

=item * filename

Optional. The module tries to figure this out based on the path, but allows you to overwrite it.

=item * mimetype

Optional. The module tries to figure this out based on the file, but allows you to overwrite it.

=back

=head3 RETURNS

True on success, dies in case of failure.

=cut

sub _parse_filename {
    my ($filename, undef, $suffix) = fileparse(shift, '\.[^\.]*');
    return "$filename$suffix";
}

sub add_attachment {
    my ($self, %options) = @_;

    die sprintf("%s is does not exist or isn't readable", $options{path})
        if !-f $options{path} || !-r $options{path};

    my $name
        = defined $options{filename}
        ? _parse_filename($options{filename})
        : _parse_filename($options{path});

    my $mimetype = $options{mimetype};
    if (!defined $mimetype) {
        my $mm = new File::MMagic;
        $mimetype = $mm->checktype_filename($options{path});
    }

    push(
        @{ $self->attachments },
        Mail::Track::Message::Attachment->new(
            entity => MIME::Entity->build(
                Path        => $options{path},
                Filename    => encode('MIME-Header', $name),
                Type        => $mimetype,
                Disposition => "attachment",
            )
        )
    );

    return 1;
}

=head2 send

Return value: $BOOLEAN_SUCCESS

    $msg->send();

Sends the prepared message to the outside world

=cut

use constant BUILD_MAP => {
    'to'   => 'To',
    'from' => 'From',
    'cc'   => 'Cc',
};

sub send {
    my $self = shift;

    my $entity = $self->_build_entity;

    if ($self->dkim) {
        my $email_full = $entity->stringify;
        $email_full =~ s/\n/\015\012/gs;

        $self->dkim->PRINT($email_full);
        $self->dkim->CLOSE();

        my $sig = $self->dkim->signature;
        my ($header_name, $header_content) = split /:\s*/, $sig->as_string, 2;

        $entity->head->replace($header_name, $header_content);
    }
    else {
        $self->log->trace("DKIM not enabled");
    }

    $self->sender->send($entity, { transport => $self->transport })
        or die 'Unable to send message';
    return $entity if !$self->bcc;

    my @bcc = Email::Address->parse($self->bcc);
    foreach (@bcc) {
        $self->sender->send(
            $entity,
            {
                transport => $self->transport,
                to        => $_->address,
            }
        ) or warn "Unable to send BCC message to " . $_->address;
    }

    return $entity;
}

=head2 _build_entity

Build the actual MIME::Entity we can use for sending or calculating the size of the message.

=cut

sub _build_entity {
    my $self = shift;

    my %prebuilt_params = map {
        BUILD_MAP->{$_} => $self->$_
    } keys %{ BUILD_MAP() };

    my %params = (
        # Extra headers shouldn't be allowed to override from/to/cc and Subject headers
        %{ $self->extra_headers },
        %prebuilt_params,
        Subject => encode('MIME-Header', $self->_build_subject()),
    );

    my $type = 'multipart/alternative';

    if (@{ $self->attachments }) {
        $type = 'multipart/mixed';
    }

    my $entity = MIME::Entity->build(%params, Type => $type);

    $self->_build_body($entity);
    $self->_build_attachments($entity);

    return $entity;
}

sub _build_subject {
    my ($self) = @_;

    my $srp = $self->subject_prefix_regex;
    my $ir  = $self->identifier_regex;

    my $subject = sprintf("[%s %s] %s",
        $self->subject_prefix_name, $self->identifier, $self->subject);

    return $subject if ($subject =~ /$srp/);
    return $self->subject;

}

sub _build_body {
    my $self   = shift;
    my $entity = shift;

    my $multipart;
    if ($entity->effective_type ne 'multipart/alternative') {
        $multipart = $entity->attach(Type => 'multipart/alternative');
    }
    else {
        $multipart = $entity;
    }

    if (my $str = $self->body->as_text) {
        $multipart->attach(
            Data     => $str,
            Type     => 'text/plain',
            Charset  => $self->encoding,
            Encoding => 'base64',
        );
    }

    if (my $str = $self->body->as_html) {
        $multipart->attach(
            Data     => $str,
            Type     => 'text/html',
            Charset  => $self->encoding,
            Encoding => 'base64',
        );
    }
}

sub _build_attachments {
    my $self   = shift;
    my $entity = shift;

    for my $attachment (@{ $self->attachments }) {
        $entity->attach(
            Filename => encode('UTF-8', $attachment->filename),
            Path     => sprintf("%s", $attachment->path),
            Type     => $attachment->mimetype,
            Encoding => 'base64',
        );
    }
}

=head1 INTERNAL METHODS

These methods are for internal reference only, please do not use these,
because the interface could change. Heck, they are prefixed with an underscore,
so do not use it ;)

=head2 _get_filename_from_part

Helper method to extract the filename from a single part: C<< $message->entity->parts >>

=cut

sub _get_filename_from_part {
    my ($self, $part) = @_;

    my $raw_filename = $part->head->recommended_filename;
    if (!defined $raw_filename) {
        my $bh = $part->bodyhandle;
        if (defined $bh) {
            $raw_filename = $bh->path;
        }
    }

    return undef unless defined $raw_filename;
    # It is called "evil", but it ain't
    if ($self->parser->filer->evil_filename($raw_filename)) {
        $raw_filename = $self->parser->filer->exorcise_filename($raw_filename);
    }
    if (!-f $raw_filename) {
        $raw_filename = catfile($self->parser->filer->output_dir($part->head), $raw_filename);
    }
    return $raw_filename;

}

=head2 $message->_parse_part()

Parse a single part, from e.g. C<< $message->entity->parts >>, and detect if it
contains an attachment or body information

See the URL L<http://msdn.microsoft.com/en-us/library/gg672007(v=exchg.80).aspx>
for more information about determination of body parts

=cut

sub _parse_part {
    my $self = shift;
    my $part = shift;

    my $content_type = lc($part->effective_type);

    my $f = $self->_get_filename_from_part($part);
    my ($filename) = fileparse($f, '\.[^.]*') if defined $f;

    if ($filename && $filename =~ /^msg-/) {
        return 1 if $self->_parse_body_part($part);
    }

    if ($content_type eq 'message/rfc822') {
        my $contents = io->catfile($f)->slurp;
        my $nm       = Mail::Track::Message->new(
            message              => $contents,
            subject_prefix_regex => $self->subject_prefix_regex,
        );
        push(@{ $self->nested_messages }, $nm);
        return 1;
    }

    if ($content_type =~ m#^multipart/(?:alternative|related)$#) {
        $self->_parse_part($_) for $part->parts;
        return 1;
    }

    if ($content_type eq 'multipart/mixed' && $part->parts) {
        return 1 if $self->_parse_body_part(($part->parts)[0]);
    }

    if ($filename) {
        return 1 if $self->_parse_attachment_part($part);
    }

    return 0;
}

=head2 $message->_parse_attachment_part($part)

Return value: $TRUE_OR_FALSE

Will add an entry to C<< $message->attachments >> when this part looks
like an attachment (if the recommended_filename is set)

=cut

sub _parse_attachment_part {
    my $self = shift;
    my $part = shift;

    return 0 if !$self->_get_filename_from_part($part);

    push(
        @{ $self->attachments },
        Mail::Track::Message::Attachment->new(entity => $part)
    );
    return 1;
}

=head2 size

Get the size of the base64 encoded message

=cut

sub size {
    my $self = shift;
    my $data = $self->_build_entity->stringify;
    return length $data;
}

=head2 $message->_parse_body_part($part)

Will add an entry to C<< $message->body_parts >> when this part looks
like a body part (content_type is one of C< BODY_TYPES >.

In the end of parsing we ensure the body is properly set.

=cut

sub _parse_body_part {
    my $self = shift;
    my $part = shift;

    my $content_type = Mail::Track::Message::Body->get_body_content_type($part)
        or return;

    my $body = $part->bodyhandle
        or return;

    push(@{ $self->body->parts }, $part);

}

=head2 DEMOLISH

Will make sure temporarily files will be cleaned up when destroying this
object

=cut

sub DEMOLISH {
    my $self = shift;

    $self->parser->filer->purge() if $self->parser;
}

=pod

## TODO:
# TO and FROM _JSON for coole shizzle and the nizzle

sub TO_JSON {
    my $self = shift;
    my %params = map({ BUILD_MAP->{$_} => $self->$_, }
        keys %{ BUILD_MAP() });

    my $type = 'multipart/alternative';

    if (@{ $self->attachments }) {
        $type = 'multipart/mixed';
    }

    my $entity = MIME::Entity->build(%params, Type => $type);

    $self->_build_body($entity);
    $self->_build_attachments($entity);
    return $entity->stringify;
}

=cut

1;

__END__

=head1 EXAMPLES

See L<#SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
