use utf8;
package Zaaksysteem::Schema::Zaaktype;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Zaaktype

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaaktype>

=cut

__PACKAGE__->table("zaaktype");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 object_type

  data_type: 'text'
  default_value: 'zaaktype'
  is_nullable: 1

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=head2 search_order

  data_type: 'text'
  is_nullable: 1

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_id_seq'

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 version

  data_type: 'integer'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 deleted

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 bibliotheek_categorie_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 active

  data_type: 'boolean'
  default_value: true
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "object_type",
  { data_type => "text", default_value => "zaaktype", is_nullable => 1 },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
  "search_order",
  { data_type => "text", is_nullable => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_id_seq",
  },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "version",
  { data_type => "integer", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "deleted",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "bibliotheek_categorie_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "active",
  { data_type => "boolean", default_value => \"true", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 bibliotheek_categorie_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekCategorie>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_categorie_id",
  "Zaaksysteem::Schema::BibliotheekCategorie",
  { id => "bibliotheek_categorie_id" },
);

=head2 interfaces

Type: has_many

Related object: L<Zaaksysteem::Schema::Interface>

=cut

__PACKAGE__->has_many(
  "interfaces",
  "Zaaksysteem::Schema::Interface",
  { "foreign.case_type_id" => "self.id" },
  undef,
);

=head2 zaak_onafgeronds

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaakOnafgerond>

=cut

__PACKAGE__->has_many(
  "zaak_onafgeronds",
  "Zaaksysteem::Schema::ZaakOnafgerond",
  { "foreign.zaaktype_id" => "self.id" },
  undef,
);

=head2 zaaks

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->has_many(
  "zaaks",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.zaaktype_id" => "self.id" },
  undef,
);

=head2 zaaktype_authorisations

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeAuthorisation>

=cut

__PACKAGE__->has_many(
  "zaaktype_authorisations",
  "Zaaksysteem::Schema::ZaaktypeAuthorisation",
  { "foreign.zaaktype_id" => "self.id" },
  undef,
);

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);

=head2 zaaktype_node_moeder_zaaktype_ids

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->has_many(
  "zaaktype_node_moeder_zaaktype_ids",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { "foreign.moeder_zaaktype_id" => "self.id" },
  undef,
);

=head2 zaaktype_node_zaaktype_ids

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->has_many(
  "zaaktype_node_zaaktype_ids",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { "foreign.zaaktype_id" => "self.id" },
  undef,
);

=head2 zaaktype_relaties

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeRelatie>

=cut

__PACKAGE__->has_many(
  "zaaktype_relaties",
  "Zaaksysteem::Schema::ZaaktypeRelatie",
  { "foreign.relatie_zaaktype_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-07-12 14:41:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:9XYAv9xiwfaRrW5iojSmxA

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::Zaaktype');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::Zaaktype",
    "+Zaaksysteem::Helper::ToJSON",
    __PACKAGE__->load_components()
);

__PACKAGE__->add_columns('last_modified',
    { %{ __PACKAGE__->column_info('last_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});

__PACKAGE__->has_many(
  "zaaktype_authorisaties",
  "Zaaksysteem::Schema::ZaaktypeAuthorisation",
  { "foreign.zaaktype_id" => "self.id" },
);

=head2 zaaktype_nodes

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

SchemaLoader has so it's own quirks, like renaming that realtion, because there
are now two ralations to the same table. Historical 'zaaktype_nodes' has been
used when there was only one relation and didn't need to disambiguate between
'zaaktype_node_moeder_zaaktype_ids' and 'zaaktype_node_zaaktype_ids'

=cut

__PACKAGE__->has_many(
  "zaaktype_nodes",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { "foreign.zaaktype_id" => "self.id" },
  undef,
);






# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

