use utf8;
package Zaaksysteem::Schema::BibliotheekNotificaties;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::BibliotheekNotificaties

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<bibliotheek_notificaties>

=cut

__PACKAGE__->table("bibliotheek_notificaties");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 object_type

  data_type: 'text'
  default_value: 'bibliotheek_notificaties'
  is_nullable: 1

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=head2 search_order

  data_type: 'text'
  is_nullable: 1

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bibliotheek_notificaties_id_seq'

=head2 bibliotheek_categorie_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 subject

  data_type: 'text'
  is_nullable: 1

=head2 message

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 deleted

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 sender

  data_type: 'text'
  is_nullable: 1

=head2 sender_address

  data_type: 'text'
  is_nullable: 1

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "object_type",
  {
    data_type     => "text",
    default_value => "bibliotheek_notificaties",
    is_nullable   => 1,
  },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
  "search_order",
  { data_type => "text", is_nullable => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bibliotheek_notificaties_id_seq",
  },
  "bibliotheek_categorie_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "subject",
  { data_type => "text", is_nullable => 1 },
  "message",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "deleted",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "sender",
  { data_type => "text", is_nullable => 1 },
  "sender_address",
  { data_type => "text", is_nullable => 1 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 bibliotheek_categorie_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekCategorie>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_categorie_id",
  "Zaaksysteem::Schema::BibliotheekCategorie",
  { id => "bibliotheek_categorie_id" },
);

=head2 bibliotheek_notificatie_kenmerks

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekNotificatieKenmerk>

=cut

__PACKAGE__->has_many(
  "bibliotheek_notificatie_kenmerks",
  "Zaaksysteem::Schema::BibliotheekNotificatieKenmerk",
  { "foreign.bibliotheek_notificatie_id" => "self.id" },
  undef,
);

=head2 zaaktype_notificaties

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeNotificatie>

=cut

__PACKAGE__->has_many(
  "zaaktype_notificaties",
  "Zaaksysteem::Schema::ZaaktypeNotificatie",
  { "foreign.bibliotheek_notificaties_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-05-29 10:10:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:GvVDARy2xceYSyfCUxfmqA

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::BibliotheekNotificaties",
    "+Zaaksysteem::Helper::ToJSON",
    __PACKAGE__->load_components()
);
__PACKAGE__->has_many(
  "zaaktype_notificaties",
  "Zaaksysteem::Schema::ZaaktypeNotificatie",
  { "foreign.bibliotheek_notificaties_id" => "self.id" },
);

# You can replace this text with custom content, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
