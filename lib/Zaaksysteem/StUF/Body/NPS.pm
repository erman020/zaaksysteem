package Zaaksysteem::StUF::Body::NPS;

use Moose;

extends 'Zaaksysteem::StUF::Body';

use constant    OBJECT_NAME     => 'NPS';

use constant    OBJECT_VALUES   => [
    {
        name        => 'inp.bsn',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'inp.a-nummer',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'voornamen',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'voorletters',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'voorvoegselGeslachtsnaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'geslachtsnaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'adellijkeTitelPredikaat',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'geboortedatum',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'geslachtsaanduiding',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    },
    {
        name        => 'geslachtsnaamPartner',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    },
    {
        name        => 'overlijdensdatum',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    },
    {
        name        => 'inp.indicatieGeheim',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'inp.burgerlijkeStaat',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
        convenient  => 'burgerlijkeStaat',
    },
    {
        name        => 'inp.gemeenteVanInschrijving',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'aanduidingNaamgebruik',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    ### Relationships
    {
        name        => 'verblijfsadres',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Relationship]',
        related     => 'Adres0310',
        local       => 1,
    },
    {
        name        => 'inp.heeftAlsEchtgenootPartner',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Relationship]',
        related     => 'NPS',
        via         => 'gerelateerde'
    },
    {
        name        => 'sub.verblijfBuitenland',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Relationship]',
        related     => 'Adres0310',
        local       => 1,
    },
    {
        name        => 'sub.correspondentieAdres',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Relationship]',
        related     => 'Adres0310',
        local       => 1,
    },
];

### Generate Moose Attributes
for my $value (@{ OBJECT_VALUES() }) {
    my %attr_opts = map {
        $_      => $value->{$_}
    } grep ({ exists($value->{$_}) } qw/is isa default lazy/);
    $attr_opts{is}          = 'rw' unless $attr_opts{is};
    $attr_opts{predicate}   = 'has_' . $value->{name};
    $attr_opts{clearer}     = 'clear_' . $value->{name};

    has $value->{name} => %attr_opts;
}

has '_object_params'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_VALUES;
    }
);

has '_object_name'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_NAME;
    }
);

__PACKAGE__->meta->make_immutable;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 OBJECT_NAME

TODO: Fix the POD

=cut

=head2 OBJECT_VALUES

TODO: Fix the POD

=cut

