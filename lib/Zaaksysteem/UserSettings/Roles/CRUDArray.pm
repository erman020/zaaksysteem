package Zaaksysteem::UserSettings::Roles::CRUDArray;

use Moose::Role;
use List::Util qw(max);
use Data::UUID;


=head1 NAME

Zaaksysteem::UserSettings::Roles::CRUDArray - Implements a CRUD interface on an array based usersetting

=head1 DESCRIPTION

This role gives the ability to run crud style actions on an array

=head1 METHODS

=head2 search

Arguments: none

    my @rows = $rows->search();

    Returns an array of all rows

=cut

sub search {
    my $self        = shift;
    my $params      = shift || {};

    my @rv;
    for my $row (@{ $self->_store }) {
        my $continue = 1;
        for my $key (keys %$params) {
            ### Prevent undefined
            if (
                (defined $params->{$key} && !defined $row->$key) || 
                (!defined $params->{$key} && defined $row->$key)
            ) {
                $continue = 0;
            } elsif (
                $params->{$key} ne $row->$key
            ) {
                $continue = 0;
            }
        }

        push(@rv, $row) if $continue;
    }

    return @rv;
}


=head2 create

Arguments: \%params

Return value: L<$self->_row_class>

    $rows->create(\%params)

=cut

sub create {
    my $self        = shift;
    my $params      = shift;

    my $rowclass    = $self->_crud_settings->{row_class};
    my $prikey      = $self->_crud_settings->{primary_key};
    my $ordercol    = $self->_crud_settings->{order_column};

    ### Increment primary key
    my $new_id      = $self->_get_new_primary if $prikey;
    my $new_order   = $self->_get_new_order($params) if $ordercol;

    my $row         = $rowclass->new(
        $self->_prepare_row_params(
            {
                $new_order ? ($ordercol   => $new_order) : (),      # When not given, autoincrement (place it last)
                %$params,
                $prikey ? ($prikey     => $new_id) : (),            # Primary keys cannot be set...
            }
        )
    );

    push(@{ $self->_store }, $row);

    return $row;
}

=head2 _prepare_row_params

=over 4

=item Arguments: \%params_for_row_class [, $optional_row]

=item Return value: \%params

=back

    $self->_prepare_row_params({ id => 'abc-88890ac-accab-acbacb78-78989', label =>'blabla'});

Will return a hash containing the params given in the argument. Say what?!. Yes, it is possible to override
this function to include more params at instantiation. Think of dynamic columns.

=cut

sub _prepare_row_params {
    my ($self, $opts, $row) = @_;

    return %$opts;
}

=head2 _get_new_primary

Arguments: none

Return value: $integer

Finds the new primary key, a UUID

=cut

sub _get_new_primary {
    my $self        = shift;

    return Data::UUID->new->create_str();
}


=head2 _get_new_order

Arguments: none

Return value: $integer

Finds the max of the order column set by _crud_settings, and returns it + 1

=cut

sub _get_new_order {
    my $self        = shift;
    my $params      = shift;

    my $ordercol    = $self->_crud_settings->{order_column};
    my $orderdep    = $self->_crud_settings->{order_dependency};

    return (max(map { $_->$ordercol } grep(
        { $_->$ordercol &&  (!$orderdep || ($_->$orderdep || '') eq ($params->{$orderdep} || '')) }
        @{ $self->_store })
    ) || 10) + 10;
}


=head2 _reorder_rows

Arguments: none

Return value: $integer

Finds the max of the order column set by _crud_settings, and returns it + 1

=cut

sub _reorder_rows {
    my $self        = shift;
    my $currow      = shift;
    my $ordercol    = $self->_crud_settings->{order_column};
    my $orderdep    = $self->_crud_settings->{order_dependency};

    return unless $ordercol;

    my $i = 0;
    my $setter = '_set_' . $ordercol;
    my @rows;
    for my $row (
        sort(
            { ($a->$ordercol || 0) <=> ($b->$ordercol || 0) } grep (
                { !$currow || (!$orderdep || ($_->$orderdep || '') eq ($currow->$orderdep || '')) }
                @{ $self->_store }
            )
        )
    ) {
        $i += 10;
        $row->$setter($i);
    }

    $self->_store(
        [
            sort(
                { ($a->$ordercol || 0) <=> ($b->$ordercol || 0) } @{ $self->_store }
            )
        ]
    );

    return 1;
}

=head2 update

Arguments: $ROW, \%params

Return value: L<$self->_row_class>

=cut

sub update {
    my $self        = shift;
    my $row         = shift;
    my $params      = shift;

    my $ordercol    = $self->_crud_settings->{order_column};

    my $order;
    $order = $self->_get_new_order($params) if (exists $params->{$ordercol} && !$params->{$ordercol});

    for my $key (keys %{ { $self->_prepare_row_params($params, $row) } }) {
        ### Skip primary key
        next if $self->_crud_settings->{primary_key} && $key eq $self->_crud_settings->{primary_key};

        my $setter = '_set_' . $key;
        $row->$setter($params->{$key});
    }

    if ($order) {
        my $setter = '_set_' . $ordercol;
        $row->$setter($order);
    }

    ### Reorder
    $self->_reorder_rows($row);

    return $row;
}

=head2 delete

Arguments: $ROW

Return value: $TRUE_OR_FALSE

=cut

sub delete {
    my $self        = shift;
    my $row         = shift;
    my $prikey      = $self->_crud_settings->{primary_key};

    my @rows        = grep({ $_->$prikey ne $row->$prikey } @{ $self->_store });

    $self->_store(\@rows);

    ### Reorder
    $self->_reorder_rows;
    
    return 1;
}


=head1 PRIVATE METHODS

=head2 _inflate_from_settings

Inflates this object from the given settings from our database

=cut

sub _inflate_from_settings {
    my $self        = shift;
    my $setting     = shift;
    my $rowclass    = $self->_crud_settings->{row_class};

    return 1 unless $setting;

    my @store;

    if (my $ordercol = $self->_crud_settings->{order_column}) {
        for my $row (sort { ($a->{$ordercol} || 0) <=> ($b->{$ordercol} || 0) } @$setting) {
            push(@store, $rowclass->new(%$row))
        }
    } else {
        @store  = @{ $self->_store };
    }
    
    $self->_store(\@store);

    return 1;
}

=head2 _deflate_to_settings

Deflates this object to a valid HashRef for the database

=cut

sub _deflate_to_settings {
    my $self        = shift;
    my $rowclass    = $self->_crud_settings->{row_class};
    
    my @rows;
    for my $row (@{ $self->_store }) {
        push(@rows, $row->deflate_to_settings)
    }

    return \@rows;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
