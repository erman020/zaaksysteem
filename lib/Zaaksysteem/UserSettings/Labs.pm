package Zaaksysteem::UserSettings::Labs;

use Moose;
use BTTW::Tools;

use Zaaksysteem::Types qw/UUID/;
use Moose::Util::TypeConstraints;

with qw/
    Zaaksysteem::UserSettings::Roles::SimpleSettings
/;

=head1 NAME

Zaaksysteem::UserSettings::Labs - Experimental features

=head1 SYNOPSIS

    my $labs = $usersettings->labs;

    print $labs->case_version

    # Return: 2 or 1



=head1 DESCRIPTION

Experimental features of zaaksysteem in usersettings

=head1 ATTRIBUTES

=head2 _setting_list

A list of settings which exposes this object.

=cut

has '+_setting_list'     => (
    default     => sub { return [qw/case_version desktop_version/] },
);

=head2 case_version

The version of the current "zaakdossier"

=cut

has 'case_version'  => (
    is          => 'rw',
    default     => 2,
    predicate   => 'has_case_version',
);

=head2 desktop_version

The version of the current "Desktop"

=cut

has 'desktop_version'  => (
    is          => 'rw',
    default     => 2,
    predicate   => 'has_desktop_version',
);


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
