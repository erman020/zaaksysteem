package Zaaksysteem::EZSearch::Casetype;

use Moose::Role;

=head1 NAME

Zaaksysteem::EZSearch::Casetype - Casetype search_type for EZSearch

=head1 DESCRIPTION

This module handles the seaching within the search_type casetype

=head1 PRIVATE METHODS

=head2 _search

Modifies the search to look into casetypes when search_type is set to C<casetype>

=cut

around '_search' => sub {
    my $orig    = shift;
    my $self    = shift;

    my @hits    = $self->$orig(@_);

    return @hits unless $self->search_type eq 'casetype';

    push(@hits, $self->_get_casetype_results);

    return @hits;
};

=head2 _get_casetype_results 

Loads the actual case results

=cut

sub _get_casetype_results {
    my $self        = shift;

    my $rs          = $self->schema->resultset('Zaaktype')->search_with_options(
        {
            term        => $self->query,
            ($self->filters->{betrokkene_type}
                ? (betrokkene_type => $self->filters->{betrokkene_type})
                : ()
            ),
            ($self->filters->{trigger}
                ? (trigger => $self->filters->{trigger})
                : ()
            ),
        },
    );

    ### Only active results
    my @results     = $rs->search(
        {
            'me.active' => 1
        },
        {
            order_by    => 'me.id',
            rows        => $self->max_results_per_type,
        }
    )->all;

    my @rows;
    for my $result (@results) {
        push(
            @rows,
            $self->_add_result(
                label           => $result->zaaktype_node_id->titel,
                description     => $result->zaaktype_node_id->zaaktype_omschrijving,
                reference       => $result->_object->uuid,
                search_type     => 'casetype',
            )
        );
    }

    return @rows;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
