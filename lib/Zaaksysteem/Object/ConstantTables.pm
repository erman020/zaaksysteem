package Zaaksysteem::Object::ConstantTables;

use strict;
use warnings;
use utf8;

require Exporter;
our @ISA        = qw/Exporter/;
our @EXPORT_OK  = qw/
    COUNTRY_TABLE
    MUNICIPALITY_TABLE
    LEGAL_ENTITY_TYPE_TABLE
    ZAAKSYSTEEM_CONFIG_DEFINITIONS
/;

=head1 NAME

Zaaksysteem::Object::Types::ConstantTables - Tables built from Zaaksysteem Constants

=head1 DESCRIPTION

Some types are loaded with static data, like "countrycodes"

=head1 CONSTANTS

=head2 LEGAL_ENTITY_TYPE_TABLE

=cut

use constant LEGAL_ENTITY_TYPE_TABLE => [
    {
        id       => '645cf805-6ef9-42e4-bf32-ede66aecc885',
        code     => '1',
        label    => 'Eenmanszaak',
        active   => 1,
    },
    {
        id       => '1873346f-927c-4d23-9322-186e8aea758a',
        code     => '2',
        label    => 'Eenmanszaak met meer dan één eigenaar',
    },
    {
        id       => '9f0d0a7d-6ece-4a54-823e-8b5b93e048b6',
        code     => '3',
        label    => 'N.V./B.V. in oprichting op A-formulier',
    },
    {
        id       => 'ce7f249e-d996-4987-b660-b66067af0c7d',
        code     => '5',
        label    => 'Rederij',
    },
    {
        id       => '8171d3a2-0576-459f-b940-88e5a0d875d1',
        code     => '7',
        label    => 'Maatschap',
        active   => 1,
    },
    {
        id       => '284c6cef-b3d5-47e9-986f-23476d732825',
        code     => '11',
        label    => 'Vennootschap onder firma',
        active   => 1,
    },
    {
        id       => '0053b5a3-f2da-43c8-a736-125a5cad5a62',
        code     => '12',
        label    => 'N.V/B.V. in oprichting op B-formulier',
    },
    {
        id       => 'c0e2868d-ba56-4d4f-bbbe-4bf53120dd43',
        code     => '21',
        label    => 'Commanditaire vennootschap met een beherend vennoot',
        active   => 1,
    },
    {
        id       => '5d5bbe2c-c74b-4427-9f55-62470737e7f4',
        code     => '22',
        label    => 'Commanditaire vennootschap met meer dan één beherende vennoot',
    },
    {
        id       => '62fc1e5f-f04b-4bcd-a4db-0dd8a298a5d0',
        code     => '23',
        label    => 'N.V./B.V. in oprichting op D-formulier',
    },
    {
        id       => '4344ce11-6cb7-433e-9eb1-8526ff9a4a55',
        code     => '40',
        label    => 'Rechtspersoon in oprichting',
    },
    {
        id       => '6ed6a0de-bbb9-432a-8e03-4f3294f6e10a',
        code     => '41',
        label    => 'Besloten vennootschap met gewone structuur',
        active   => 1,
    },
    {
        id       => '4ffaf90c-db27-42c2-9e4a-400424bad6e5',
        code     => '42',
        label    => 'Besloten vennootschap blijkens statuten structuurvennootschap',
    },
    {
        id       => '5db79ef3-651a-4fd0-9211-ae7f4bb39ce7',
        code     => '51',
        label    => 'Naamloze vennootschap met gewone structuur',
        active   => 1,
    },
    {
        id       => '0973fbd2-ed83-4dff-9d34-51f745b391c5',
        code     => '52',
        label    => 'Naamloze vennootschap blijkens statuten structuurvennootschap',
    },
    {
        id       => '1ede5f01-6d76-49aa-ae26-75f2f4b20ce0',
        code     => '53',
        label    => 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal',
    },
    {
        id       => 'e7fe016f-37cc-4e34-947e-ebfd49877952',
        code     => '54',
        label    => 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap',
    },
    {
        id       => '912835bf-9f98-40d2-aba1-b8866e2b98ee',
        code     => '55',
        label    => 'Europese naamloze vennootschap (SE) met gewone structuur',
        active   => 1,
    },
    {
        id       => '230077a3-b0a9-445f-885f-5890c3bfe133',
        code     => '56',
        label    => 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap',
    },
    {
        id       => '686e0b53-91e7-4a5b-af08-5c512de94ba9',
        code     => '61',
        label    => 'Coöperatie U.A. met gewone structuur',
    },
    {
        id       => 'f24d328e-1122-434f-b40b-dc4f5fe38c82',
        code     => '62',
        label    => 'Coöperatie U.A. blijkens statuten structuurcoöperatie',
    },
    {
        id       => 'e04e5ee9-d712-43c7-8e3a-1bd8c84fa39d',
        code     => '63',
        label    => 'Coöperatie W.A. met gewone structuur',
    },
    {
        id       => '5bbc2951-5f0e-4203-8be6-259912272ead',
        code     => '64',
        label    => 'Coöperatie W.A. blijkens statuten structuurcoöperatie',
    },
    {
        id       => '27662752-6f20-4cf1-b081-662def3503c6',
        code     => '65',
        label    => 'Coöperatie B.A. met gewone structuur',
    },
    {
        id       => 'bf276571-9380-4ad2-ab52-14b84ff133c6',
        code     => '66',
        label    => 'Coöperatie B.A. blijkens statuten structuurcoöperatie',
    },
    {
        id       => 'edc1477f-2fc5-4e1f-bb61-233b9823509a',
        code     => '70',
        label    => 'Vereniging van eigenaars',
        active   => 1,
    },
    {
        id       => '3918d3ab-e11d-43ff-ae1c-51f5f523338f',
        code     => '71',
        label    => 'Vereniging met volledige rechtsbevoegdheid',
    },
    {
        id       => 'fc7f7654-b23c-4ecd-b719-39d80f6c98c9',
        code     => '72',
        label    => 'Vereniging met beperkte rechtsbevoegdheid',
    },
    {
        id       => '759b76bf-bceb-49d0-948b-4bbe17992326',
        code     => '73',
        label    => 'Kerkgenootschap',
        active   => 1,
    },
    {
        id       => 'c91215d9-14b5-43e0-ba2b-1b8487e2b889',
        code     => '74',
        label    => 'Stichting',
        active   => 1,
    },
    {
        id       => 'fc01a26d-1ec9-419a-b05d-c3c0a53dcab6',
        code     => '81',
        label    => 'Onderlinge waarborgmaatschappij U.A. met gewone structuur',
    },
    {
        id       => '3c718f04-77e3-4ec7-87fc-6a896c16c8a4',
        code     => '82',
        label    => 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge',
    },
    {
        id       => '649dba84-8c72-4a94-aa91-617db9dab51c',
        code     => '83',
        label    => 'Onderlinge waarborgmaatschappij W.A. met gewone structuur',
    },
    {
        id       => '755f456d-a94f-44bb-bfe8-47277ee360ab',
        code     => '84',
        label    => 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge',
    },
    {
        id       => '71580ddf-7ba8-4ee1-a383-fc20d562ed20',
        code     => '85',
        label    => 'Onderlinge waarborgmaatschappij B.A. met gewone structuur',
    },
    {
        id       => '38fc7147-724a-4c30-9d3b-204aa99b859f',
        code     => '86',
        label    => 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge',
    },
    {
        id       => '423a8ddf-81d3-4499-a0f1-33fad764839c',
        code     => '88',
        label    => 'Publiekrechtelijke rechtspersoon',
        active   => 1,
    },
    {
        id       => '292840a2-4e03-47f8-b00b-d5d1ffe0392a',
        code     => '89',
        label    => 'Privaatrechtelijke rechtspersoon',
    },
    {
        id       => '7861a128-8419-4d09-9647-e31f3498faf0',
        code     => '91',
        label    => 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland',
    },
    {
        id       => 'c164718f-fd53-43bb-8b99-b403cf5cff43',
        code     => '92',
        label    => 'Nevenvest. met hoofdvest. in buitenl.',
    },
    {
        id       => '0d566451-9f52-40f7-95e6-79033101d13f',
        code     => '93',
        label    => 'Europees economisch samenwerkingsverband',
    },
    {
        id       => '86641b3f-3e1f-45c7-87dc-9ba24b92c10d',
        code     => '94',
        label    => 'Buitenl. EG-venn. met onderneming in Nederland',
    },
    {
        id       => '454fc546-0d05-404a-80f4-8f2c3e2e9355',
        code     => '95',
        label    => 'Buitenl. EG-venn. met hoofdnederzetting in Nederland',
    },
    {
        id       => '47c9ab5c-1900-4e9c-8a2a-26ded7105a82',
        code     => '96',
        label    => 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland',
    },
    {
        id       => '4c8a1012-8f38-4b41-8583-79f48f98c581',
        code     => '97',
        label    => 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland',
    },
    {
        id       => '8a5398b1-4b27-4395-9a52-568dff425aae',
        code     => '201',
        label    => 'Coöperatie',
        active   => 1,
    },
    {
        id       => '4e038ac3-8f8e-4391-b8e6-1f8ffb1a23fe',
        code     => '202',
        label    => 'Vereniging',
        active   => 1,
    },
];

=head2 COUNTRY_TABLE

    [
        {
            id         => '8c4d9e76-4098-46ee-9879-4503c102de07',
            dutch_code => '0',
            label      => 'Onbekend'
        },
        ...
    ],

=cut

use constant COUNTRY_TABLE => [
    {
        id         => '8c4d9e76-4098-46ee-9879-4503c102de07',
        dutch_code => '0',
        label      => 'Onbekend'
    },
    {
        id         => '8adf58c4-da89-4271-adaa-915f34145ca2',
        dutch_code => '5001',
        label      => 'Canada'
    },
    {
        id         => '4d0247d5-58dc-40cb-94ae-786dfe07eef2',
        dutch_code => '5002',
        label      => 'Frankrijk'
    },
    {
        id         => '26b94232-d7e9-4e4f-91c6-9535c3e4cc48',
        dutch_code => '5003',
        label      => 'Zwitserland'
    },
    {
        id         => '1161a3b8-ba49-4f34-9baf-5c52a78e143b',
        dutch_code => '5004',
        label      => 'Rhodesië'
    },
    {
        id         => '8d8de9fa-bf0f-4a0e-a61c-3763ab67932f',
        dutch_code => '5005',
        label      => 'Malawi'
    },
    {
        id         => 'bbd83925-4abd-4db3-b531-b15bf5eb9e4e',
        dutch_code => '5006',
        label      => 'Cuba'
    },
    {
        id         => '58eaa052-9cb4-4983-a3cb-b06f627ad9dd',
        dutch_code => '5007',
        label      => 'Suriname'
    },
    {
        id         => '39fe4557-44d3-4428-a842-4447b2e0a078',
        dutch_code => '5008',
        label      => 'Tunesië'
    },
    {
        id         => 'bbb7d007-8147-4106-8028-ddc8cd8141e1',
        dutch_code => '5009',
        label      => 'Oostenrijk'
    },
    {
        id         => 'cc3ca75d-44fc-45be-bd09-2ade8d56c903',
        dutch_code => '5010',
        label      => 'België'
    },
    {
        id         => '5fd4820d-8d63-48a6-9704-8d16ad0a4104',
        dutch_code => '5011',
        label      => 'Botswana'
    },
    {
        id         => 'fc650929-463b-49ea-94a9-a779414a1667',
        dutch_code => '5012',
        label      => 'Iran'
    },
    {
        id         => 'f40662e1-003d-49b0-a84b-ff8e59e577a2',
        dutch_code => '5013',
        label      => 'Nieuwzeeland'
    },
    {
        id         => 'db2c06ec-226d-4bb5-9050-a772767f0b34',
        dutch_code => '5014',
        label      => 'Zuidafrika'
    },
    {
        id         => '4b1015c9-eacf-452f-88e8-1ac387dff42d',
        dutch_code => '5015',
        label      => 'Denemarken'
    },
    {
        id         => '56acb7ae-cf92-424c-9627-40974dd34076',
        dutch_code => '5016',
        label      => 'Noordjemen'
    },
    {
        id         => '94a19427-cd1a-4c64-b79e-2451841f4aee',
        dutch_code => '5017',
        label      => 'Hongarije'
    },
    {
        id         => '533e9055-8eaa-4a64-bd5a-be9b52fbdf46',
        dutch_code => '5018',
        label      => 'Saoediarabië'
    },
    {
        id         => '18cc2ab8-60fd-4b41-b004-a63f48075c9d',
        dutch_code => '5019',
        label      => 'Liberia'
    },
    {
        id         => 'c0e9cc8e-a61a-436c-a1e9-75878774e094',
        dutch_code => '5020',
        label      => 'Etiopië'
    },
    {
        id         => '4a5109ff-d001-4b83-8cc9-521305d96d41',
        dutch_code => '5021',
        label      => 'Chili'
    },
    {
        id         => 'a968a228-6ca4-450e-a3ea-8e70b13b4131',
        dutch_code => '5022',
        label      => 'Marokko'
    },
    {
        id         => 'a8a8049e-1665-4669-a311-2ae65612647e',
        dutch_code => '5023',
        label      => 'Togo'
    },
    {
        id         => 'b39e31a0-a263-4744-85df-6572c2188c32',
        dutch_code => '5024',
        label      => 'Ghana'
    },
    {
        id         => '441169f1-4ab4-4a73-b0bd-7be91cd4fb68',
        dutch_code => '5025',
        label      => 'Laos'
    },
    {
        id         => '7ba530ca-046d-4822-9096-f5538d5505ce',
        dutch_code => '5026',
        label      => 'Angola'
    },
    {
        id         => '6860355f-330b-4598-b5a7-366482ac6a5d',
        dutch_code => '5027',
        label      => 'Filipijnen'
    },
    {
        id         => '6f4bb2ec-e112-4535-98fc-369fd8c3fa39',
        dutch_code => '5028',
        label      => 'Zambia'
    },
    {
        id         => '31c22b77-60b9-4264-bb8e-cf879c851907',
        dutch_code => '5029',
        label      => 'Mali'
    },
    {
        id         => '4d39afd5-8ad8-4846-8e75-5bae2fd4cdac',
        dutch_code => '5030',
        label      => 'Ivoorkust'
    },
    {
        id         => '6641dfdb-6d5f-4c59-9247-d2d575e3ed60',
        dutch_code => '5031',
        label      => 'Burma'
    },
    {
        id         => '6d993e10-dd01-4c29-8fd2-41efccdd6a7c',
        dutch_code => '5032',
        label      => 'Monaco'
    },
    {
        id         => '71373604-2785-4f9f-a7e3-6a6ff368f222',
        dutch_code => '5033',
        label      => 'Colombia'
    },
    {
        id         => '12e8c61b-55b6-4bc5-a707-15e774938c36',
        dutch_code => '5034',
        label      => 'Albanië'
    },
    {
        id         => 'ccd550fe-a36d-45cc-a085-cbf14dbed29c',
        dutch_code => '5035',
        label      => 'Kameroen'
    },
    {
        id         => 'b18e1fde-471b-44f1-8cde-a3d02c054fbd',
        dutch_code => '5036',
        label      => 'Zuidviëtnam'
    },
    {
        id         => 'db77f9ed-4383-4d97-9c7b-97d228be619f',
        dutch_code => '5037',
        label      => 'Singapore'
    },
    {
        id         => '1cb92ee8-18a9-40d2-b740-65398e8da2fd',
        dutch_code => '5038',
        label      => 'Paraguay'
    },
    {
        id         => 'ffaff274-be90-4de0-a59f-17a8c7a1b599',
        dutch_code => '5039',
        label      => 'Zweden'
    },
    {
        id         => '05d2b497-6420-47f7-ba9c-f2de7befca87',
        dutch_code => '5040',
        label      => 'Cyprus'
    },
    {
        id         => '02feaad1-880a-43fb-a036-2a6b78078492',
        dutch_code => '5041',
        label      => 'Australisch Nieuwguinea'
    },
    {
        id         => '35f96565-5bcd-4499-8a73-ceeb5d0e17d5',
        dutch_code => '5042',
        label      => 'Brunei'
    },
    {
        id         => 'e626b869-235d-40a5-9906-0a35b99885bd',
        dutch_code => '5043',
        label      => 'Irak'
    },
    {
        id         => 'bcf923b4-ac4d-45af-975e-222380e28dc1',
        dutch_code => '5044',
        label      => 'Mauritius'
    },
    {
        id         => '45409407-2a9f-47ef-a9fa-a1af588710e4',
        dutch_code => '5045',
        label      => 'Vaticaanstad'
    },
    {
        id         => 'c07ef39d-aa05-4c24-b809-a4783369bb46',
        dutch_code => '5046',
        label      => 'Kashmir'
    },
    {
        id         => 'b5373d85-db45-45c1-9932-4b3bc59988fc',
        dutch_code => '5047',
        label      => 'Myanmar'
    },
    {
        id         => '114283d5-53b9-45f1-80c5-14c4215f79e4',
        dutch_code => '5048',
        label      => 'Jemen'
    },
    {
        id         => 'b3dbf73f-5497-4869-aa8b-66cf70cac342',
        dutch_code => '5049',
        label      => 'Slovenië'
    },
    {
        id         => 'ff496536-da1b-47a1-908c-6faaa9c598fc',
        dutch_code => '5050',
        label      => 'Zaïre'
    },
    {
        id         => 'e227c2b6-d84c-4617-81a8-020bb2c38bfe',
        dutch_code => '5051',
        label      => 'Kroatië'
    },
    {
        id         => '85098eac-18bf-4c56-9346-03ea22fb1fc3',
        dutch_code => '5052',
        label      => 'Taiwan'
    },
    {
        id         => '4b426a6e-9f0e-4008-b2ab-efa404fe1950',
        dutch_code => '5053',
        label      => 'Rusland'
    },
    {
        id         => 'fc0793f5-6cb0-49a4-aba3-4a7302beaa77',
        dutch_code => '5054',
        label      => 'Armenië'
    },
    {
        id         => 'e91e301f-43c9-459f-8bee-efb7b5c4235f',
        dutch_code => '5055',
        label      => 'Ascension'
    },
    {
        id         => '146c312e-75a3-4d24-adbc-c2a3f54b0ecc',
        dutch_code => '5056',
        label      => 'Azoren'
    },
    {
        id         => '95397498-d604-436b-8be5-60dd56aed51c',
        dutch_code => '5057',
        label      => 'Bahrein'
    },
    {
        id         => '2b09fa1b-8256-4682-a908-dd23c1bd6656',
        dutch_code => '5058',
        label      => 'Bhutan'
    },
    {
        id         => '2d14788d-4ee0-45d7-83e5-68d8b49a65f8',
        dutch_code => '5059',
        label      => 'Britse Antillen'
    },
    {
        id         => 'b9c2ec7a-7aa8-428b-a16f-517d05967d04',
        dutch_code => '5060',
        label      => 'Comoren'
    },
    {
        id         => '5e809664-1ad1-4bdb-90dc-912c505f7363',
        dutch_code => '5061',
        label      => 'Falklandeilanden'
    },
    {
        id         => '805de983-4657-4121-8ba3-ea3be534f959',
        dutch_code => '5062',
        label      => 'Frans Guyana'
    },
    {
        id         => '7e6a503c-9b1b-4c04-b1db-581d51a51c8d',
        dutch_code => '5063',
        label      => 'Frans Somaliland'
    },
    {
        id         => '58cef3fe-9ec9-47b1-99dc-06117bc99f6f',
        dutch_code => '5064',
        label      => 'Gilbert- en Ellice-eilanden'
    },
    {
        id         => 'a00bec36-5107-4bf1-9dc0-a83f3b641ff0',
        dutch_code => '5065',
        label      => 'Groenland'
    },
    {
        id         => '60f8455e-6960-4fc9-b506-ef29a71756ea',
        dutch_code => '5066',
        label      => 'Guadeloupe'
    },
    {
        id         => 'eeb3064c-173d-436f-9d66-b43b2a483f91',
        dutch_code => '5067',
        label      => 'Kaapverdische Eilanden'
    },
    {
        id         => '22995326-b921-4db7-8c40-92d2832e3cbf',
        dutch_code => '5068',
        label      => 'Macau'
    },
    {
        id         => '6fc85e28-1fcd-4767-a97d-960b6d0ec5ee',
        dutch_code => '5069',
        label      => 'Martinique'
    },
    {
        id         => 'd9844782-12aa-4e47-9c3e-2d0a7f177495',
        dutch_code => '5070',
        label      => 'Mozambique'
    },
    {
        id         => '486ceb15-1ea9-41ab-adf9-ae0e7f8a51ef',
        dutch_code => '5071',
        label      => 'Pitcairneilanden'
    },
    {
        id         => '8179f736-20eb-4dc1-9b2a-878b529b1386',
        dutch_code => '5072',
        label      => 'Guinee Bissau'
    },
    {
        id         => '47ff206a-48ed-42dc-97ae-5986e9eab649',
        dutch_code => '5073',
        label      => 'Réunion'
    },
    {
        id         => 'a144f97f-9a8b-467c-b6a8-cd71ddc68162',
        dutch_code => '5074',
        label      => 'Saint Pierre en Miquelon'
    },
    {
        id         => '8241d12a-f3a7-47b0-ab8f-3539e28f8f89',
        dutch_code => '5075',
        label      => 'Seychellen en Amiranten'
    },
    {
        id         => '7984b38e-655c-4295-906f-01e5b8178916',
        dutch_code => '5076',
        label      => 'Tonga'
    },
    {
        id         => '7c21d2f5-3c54-4925-8edc-9203bafd0f6a',
        dutch_code => '5077',
        label      => 'Wallis en Futuna'
    },
    {
        id         => 'bbc69a03-9f7d-4d05-b4c7-4f9ee5721a1b',
        dutch_code => '5078',
        label      => 'Zuidwest Afrika'
    },
    {
        id         => 'ede0f32d-da81-4cf3-899b-6ddb88b71fa4',
        dutch_code => '5079',
        label      => 'Frans Indië'
    },
    {
        id         => '44241712-c420-4aae-8197-21d01d89ee78',
        dutch_code => '5080',
        label      => 'Johnston'
    },
    {
        id         => 'fa99cc4f-5b0f-400f-ad7b-0001bc6394fb',
        dutch_code => '5081',
        label      => 'Kedah'
    },
    {
        id         => '2daf0fe5-ccc0-4755-986e-9d17855d7a85',
        dutch_code => '5082',
        label      => 'Kelantan'
    },
    {
        id         => '28a741e5-28c9-4d86-a9c9-b4fc42761a15',
        dutch_code => '5083',
        label      => 'Malakka'
    },
    {
        id         => '5dee1c45-3c90-4076-ade4-5a244d3c8a87',
        dutch_code => '5084',
        label      => 'Mayotte'
    },
    {
        id         => '75c73f4f-eb12-4474-8049-f351f42f3ae0',
        dutch_code => '5085',
        label      => 'Negri Sembilan'
    },
    {
        id         => '2d188b18-5845-4b0f-b747-316fdb47ce67',
        dutch_code => '5086',
        label      => 'Pahang'
    },
    {
        id         => 'e921f615-fe47-4f4a-8e76-d551d56cbb77',
        dutch_code => '5087',
        label      => 'Perak'
    },
    {
        id         => '617dfeb2-57f0-4f5e-9b22-fc0f15d9274b',
        dutch_code => '5088',
        label      => 'Perlis'
    },
    {
        id         => '731bbdf5-5593-4e73-a066-42a83c50c0a2',
        dutch_code => '5089',
        label      => 'Portugees Indië'
    },
    {
        id         => 'e2a26870-a634-4291-bdf2-69220eba6d28',
        dutch_code => '5090',
        label      => 'Selangor'
    },
    {
        id         => '2bcb8a79-0d63-4cb9-aab3-c3a839989be4',
        dutch_code => '5091',
        label      => 'Sikkim'
    },
    {
        id         => 'e50a4910-a4ce-4406-bac3-4ed83700cf80',
        dutch_code => '5092',
        label      => 'Sint Vincent en de Grenadinen'
    },
    {
        id         => 'dfb4e564-e134-4f5a-81d7-2786eedbb9fd',
        dutch_code => '5093',
        label      => 'Spitsbergen'
    },
    {
        id         => '9148b728-9ea8-4398-8fc4-d7d5f887e701',
        dutch_code => '5094',
        label      => 'Trengganu'
    },
    {
        id         => '3d9d9e62-5a03-475b-925b-2a59f1cbacdc',
        dutch_code => '5095',
        label      => 'Aruba'
    },
    {
        id         => 'e26dbdf8-6a4d-43f7-aa6d-62580d8d2045',
        dutch_code => '5096',
        label      => 'Burkina Faso'
    },
    {
        id         => 'f425bc25-87e4-4521-8724-30151be3c023',
        dutch_code => '5097',
        label      => 'Azerbajdsjan'
    },
    {
        id         => '46b75766-555f-4ef1-a373-854f30c771d4',
        dutch_code => '5098',
        label      => 'Belarus (Wit-Rusland)'
    },
    {
        id         => '015c4e28-782b-4a6a-94f5-2033c297c3c7',
        dutch_code => '5099',
        label      => 'Kazachstan'
    },
    {
        id         => '630a06fa-b2c7-4f8d-a891-99055b26e51e',
        dutch_code => '5100',
        label      => 'Macedonië'
    },
    {
        id         => 'c90e86c1-cc61-43c2-93ed-4ed0ab8b9e66',
        dutch_code => '5101',
        label      => 'Timor Leste'
    },
    {
        id         => '4e3ac244-24a0-498f-a3a9-fa7247c3435f',
        dutch_code => '5102',
        label      => 'Servië en Montenegro'
    },
    {
        id         => 'a915e82a-4f84-4218-bf9a-cd7f1c21ee75',
        dutch_code => '5103',
        label      => 'Servië'
    },
    {
        id         => 'c0e95934-fe5a-4864-850a-418b757a6fa3',
        dutch_code => '5104',
        label      => 'Montenegro'
    },
    {
        id         => '88a681be-3675-460f-aa6c-688a4ff2e9f5',
        dutch_code => '5105',
        label      => 'Kosovo'
    },
    {
        id         => 'c3702979-ab74-4da3-875b-f66d77989b5a',
        dutch_code => '5106',
        label      => 'Bonaire'
    },
    {
        id         => '51496db9-9af0-44dc-ac20-0c9e530d8ed1',
        dutch_code => '5107',
        label      => 'Curaçao'
    },
    {
        id         => 'd9f2984e-c6e2-438e-9ec9-acbbb879c316',
        dutch_code => '5108',
        label      => 'Saba'
    },
    {
        id         => '97459cd6-b765-4bcd-a507-65dc91ee5a1a',
        dutch_code => '5109',
        label      => 'Sint Eustatius'
    },
    {
        id         => 'da0037bd-2b3b-43ee-9d69-0a41e86c62c9',
        dutch_code => '5110',
        label      => 'Sint Maarten'
    },
    {
        id         => '93088821-1894-4277-8ccf-3008d4f9e4a2',
        dutch_code => '5111',
        label      => 'Zuid-Soedan'
    },
    {
        id         => 'aa8bde64-d1a9-4fff-b6b7-a57b8858fd18',
        dutch_code => '6000',
        label      => 'Moldavië'
    },
    {
        id         => 'c3eb674e-3542-4468-a563-57cdcc8148ab',
        dutch_code => '6001',
        label      => 'Burundi'
    },
    {
        id         => '259da240-86ff-4e11-ac10-87b74abb006e',
        dutch_code => '6002',
        label      => 'Finland'
    },
    {
        id         => '746442e7-32de-4688-8b88-b0061fa25346',
        dutch_code => '6003',
        label      => 'Griekenland'
    },
    {
        id         => '9d697682-7e15-420a-8706-adc0e28942d8',
        dutch_code => '6004',
        label      => 'Guatemala'
    },
    {
        id         => 'dae91f34-825c-4df0-b733-da9069d030e0',
        dutch_code => '6005',
        label      => 'Nigeria'
    },
    {
        id         => '0129250d-b384-497f-bd43-4ce0d814033e',
        dutch_code => '6006',
        label      => 'Libië'
    },
    {
        id         => 'adb95688-9e5d-4c11-8691-425f2b67ac97',
        dutch_code => '6007',
        label      => 'Ierland'
    },
    {
        id         => '2c8391fc-b141-4c64-acb6-92bbc8b22282',
        dutch_code => '6008',
        label      => 'Brazilië'
    },
    {
        id         => 'a9183af4-c876-4776-8a80-03afd1dbcda8',
        dutch_code => '6009',
        label      => 'Rwanda'
    },
    {
        id         => '36473bd7-62a0-4d29-9119-391e93db51fb',
        dutch_code => '6010',
        label      => 'Venezuela'
    },
    {
        id         => 'caa48d0e-ea55-46e6-98c6-b79bbaa3071e',
        dutch_code => '6011',
        label      => 'IJsland'
    },
    {
        id         => '9786d7d7-a7c6-4b83-ac7f-cd678bdc5a1b',
        dutch_code => '6012',
        label      => 'Liechtenstein'
    },
    {
        id         => 'cb8a68a6-d05e-4a6c-99ea-066664ef59d3',
        dutch_code => '6013',
        label      => 'Somalia'
    },
    {
        id         => '9721d22d-3fe4-4faa-a189-46707b35eda7',
        dutch_code => '6014',
        label      => 'Verenigde Staten van Amerika'
    },
    {
        id         => '7d9f0a76-d158-4f26-bed4-ff9727846522',
        dutch_code => '6015',
        label      => 'Bolivia'
    },
    {
        id         => '4759081e-2307-44ac-a764-438797d7078b',
        dutch_code => '6016',
        label      => 'Australië'
    },
    {
        id         => '693e5f3f-4687-41ac-8689-fb7d0a46d70e',
        dutch_code => '6017',
        label      => 'Jamaica'
    },
    {
        id         => '25460c43-16be-4bcb-bd03-5a584c113ecc',
        dutch_code => '6018',
        label      => 'Luxemburg'
    },
    {
        id         => '6080c4c5-31e8-4fd9-a7c9-299ae87c361a',
        dutch_code => '6019',
        label      => 'Tsjaad'
    },
    {
        id         => 'b5f4ac43-afc4-4063-81a8-4aeacebb6cd8',
        dutch_code => '6020',
        label      => 'Mauritanië'
    },
    {
        id         => 'c6949f47-52e4-44cd-ba66-6a11863d1e42',
        dutch_code => '6021',
        label      => 'Kyrgyzstan'
    },
    {
        id         => 'db660873-7ee9-4921-9922-bc5cf585841b',
        dutch_code => '6022',
        label      => 'China'
    },
    {
        id         => '349931d6-6ed9-472f-aa7b-6bf3e5ecc4e9',
        dutch_code => '6023',
        label      => 'Afganistan'
    },
    {
        id         => 'dfdfc950-311f-4832-a196-02b3596978f6',
        dutch_code => '6024',
        label      => 'Indonesië'
    },
    {
        id         => 'cb28a2c0-3106-4637-b03e-8d1ec80cfe81',
        dutch_code => '6025',
        label      => 'Guyana'
    },
    {
        id         => '44094964-7355-40da-a8fe-7672a8ccbfff',
        dutch_code => '6026',
        label      => 'Noordviëtnam'
    },
    {
        id         => '4c649f77-8857-4fd8-8bfe-25cd1fcc011e',
        dutch_code => '6027',
        label      => 'Noorwegen'
    },
    {
        id         => '38f5c18d-3c4b-4ec0-bf09-50fc48bb7d99',
        dutch_code => '6028',
        label      => 'San Marino'
    },
    {
        id         => '968b623e-183b-4e4c-8580-233499d0164b',
        dutch_code => '6029',
        label      => 'Duitsland'
    },
    {
        id         => '7139d886-5bd8-4bd1-b2ae-78d5822163aa',
        dutch_code => '6030',
        label      => 'Nederland'
    },
    {
        id         => '6ecb7173-799f-4194-86f2-6a6926a2529a',
        dutch_code => '6031',
        label      => 'Kambodja'
    },
    {
        id         => 'e8b462ac-b0cd-4d32-ba34-ba73ddb96b6c',
        dutch_code => '6032',
        label      => 'Fiji'
    },
    {
        id         => '6f2d5d65-1ec7-48ec-a05e-22baf04445d3',
        dutch_code => '6033',
        label      => 'Bahama-eilanden'
    },
    {
        id         => 'dd789db4-7491-437a-bc68-87529abf954f',
        dutch_code => '6034',
        label      => 'Israël'
    },
    {
        id         => '7aae4ca0-ca2f-42ee-b2a2-7e9debdbcb3d',
        dutch_code => '6035',
        label      => 'Nepal'
    },
    {
        id         => 'a5ab3b45-8dee-4d0a-84ac-6218cae04bcb',
        dutch_code => '6036',
        label      => 'Zuidkorea'
    },
    {
        id         => '4758bc4c-ec92-49b0-b6c6-af5e05f00fa2',
        dutch_code => '6037',
        label      => 'Spanje'
    },
    {
        id         => 'bd59b067-780e-4c4d-bfb9-6ab48db669c5',
        dutch_code => '6038',
        label      => 'Oekraine'
    },
    {
        id         => '9bc9dedb-5cfd-4c24-a650-cddc4edd0c4d',
        dutch_code => '6039',
        label      => 'Grootbrittannië'
    },
    {
        id         => '09fac1fc-5204-4f86-b7f1-399f6bed0793',
        dutch_code => '6040',
        label      => 'Niger'
    },
    {
        id         => 'd597dbdf-1740-4f53-a543-a10b13b85edf',
        dutch_code => '6041',
        label      => 'Haïti'
    },
    {
        id         => '0884ddac-89a6-46aa-9ffc-e4a78453ec72',
        dutch_code => '6042',
        label      => 'Jordanië'
    },
    {
        id         => 'd2cc7dea-01fc-42d1-a68b-982f31c26ecb',
        dutch_code => '6043',
        label      => 'Turkije'
    },
    {
        id         => '3ad069b6-32eb-49c5-a10b-6c266c5e2f71',
        dutch_code => '6044',
        label      => 'Trinidad en Tobago'
    },
    {
        id         => '09db526b-e012-4311-8a90-430cbb4b846f',
        dutch_code => '6045',
        label      => 'Joegoslavië'
    },
    {
        id         => 'cee5a033-05ec-4211-9f5b-8d759181878b',
        dutch_code => '6046',
        label      => 'Bovenvolta'
    },
    {
        id         => 'a1a63cbf-bd03-4b24-b13d-91a64b7007d2',
        dutch_code => '6047',
        label      => 'Algerije'
    },
    {
        id         => '1e05a11a-52cc-40c2-a173-e6453c76fc81',
        dutch_code => '6048',
        label      => 'Gabon'
    },
    {
        id         => '0dc8ac02-694e-4c4e-9fc8-b7b958c62c91',
        dutch_code => '6049',
        label      => 'Noordkorea'
    },
    {
        id         => 'bfff608f-4e2b-4933-ac0f-3c4f070765fe',
        dutch_code => '6050',
        label      => 'Oezbekistan'
    },
    {
        id         => '83132fc4-911e-4d88-bcdb-c1e9d7bb7526',
        dutch_code => '6051',
        label      => 'Sierra Leone'
    },
    {
        id         => '0d0c070b-399a-4db2-a24f-6ce5d0c6cd4f',
        dutch_code => '6052',
        label      => 'Brits Honduras'
    },
    {
        id         => 'ce798fd1-264e-42ef-8e1c-ef34076f8897',
        dutch_code => '6053',
        label      => 'Canarische eilanden'
    },
    {
        id         => 'd8b26ecc-a839-449f-a611-59b4501967a4',
        dutch_code => '6054',
        label      => 'Frans Polynesië'
    },
    {
        id         => '35a56bc8-5068-47e6-8543-76b937ad8535',
        dutch_code => '6055',
        label      => 'Gibraltar'
    },
    {
        id         => 'cde9cad7-4cb8-4f39-89eb-7b65dcdd7768',
        dutch_code => '6056',
        label      => 'Portugees Timor'
    },
    {
        id         => 'ccd305ee-6d89-4c34-afae-978773cbff51',
        dutch_code => '6057',
        label      => 'Tadzjikistan'
    },
    {
        id         => 'b1e94303-00b6-4220-96ae-85cfd7dba581',
        dutch_code => '6058',
        label      => 'Britse Salomons-eilanden'
    },
    {
        id         => '88b207b1-5f59-45b8-8fa6-5aeb31da28ab',
        dutch_code => '6059',
        label      => 'São Tomé en Principe'
    },
    {
        id         => 'dd70b434-3db1-4b73-8ee8-34e358ee3a20',
        dutch_code => '6060',
        label      => 'Sint Helena'
    },
    {
        id         => '8c6841de-2128-433e-a3f0-b6103830d670',
        dutch_code => '6061',
        label      => 'Tristan Da Cunha'
    },
    {
        id         => 'a18618db-9088-4ae9-abab-6c33efa9e8ac',
        dutch_code => '6062',
        label      => 'Westsamoa'
    },
    {
        id         => '5b7b18f2-a614-4c28-847f-df8b7289fde4',
        dutch_code => '6063',
        label      => 'Toerkmenistan'
    },
    {
        id         => '077c6260-c5f2-4a14-a2a8-1e1ae702f9e3',
        dutch_code => '6064',
        label      => 'Georgië'
    },
    {
        id         => '49724f62-ba06-4f68-90f9-8e8a0021e0a9',
        dutch_code => '6065',
        label      => 'Bosnië-Herzegovina'
    },
    {
        id         => '5307797a-16c8-4dc9-9dfc-e188fdf266ff',
        dutch_code => '6066',
        label      => 'Tsjechië'
    },
    {
        id         => '1318b978-2b2f-440b-aebe-26a45f3ed99d',
        dutch_code => '6067',
        label      => 'Slowakije'
    },
    {
        id         => 'cc7cd0b7-9535-4e89-b3a1-c1ce76a11fd8',
        dutch_code => '6068',
        label      => 'Federale Republiek Joegoslavië'
    },
    {
        id         => 'be1a854f-3706-4dec-a5d6-d8e565104498',
        dutch_code => '6069',
        label      => 'Democratische Republiek Congo'
    },
    {
        id         => '655024f0-9b42-4aed-8624-e70e97554ff9',
        dutch_code => '7001',
        label      => 'Uganda'
    },
    {
        id         => '52b88ba2-ccd2-4d14-a228-d5c3a1a4a406',
        dutch_code => '7002',
        label      => 'Kenya'
    },
    {
        id         => '73228ccb-7e83-4adf-8bda-7c552fc76944',
        dutch_code => '7003',
        label      => 'Malta'
    },
    {
        id         => 'faf8efa0-ad65-4ec4-a834-6cb7fc643419',
        dutch_code => '7004',
        label      => 'Barbados'
    },
    {
        id         => 'e3e09124-992f-41d2-b581-50961898ff17',
        dutch_code => '7005',
        label      => 'Andorra'
    },
    {
        id         => 'ae54f982-f25f-4079-8da2-867346836e06',
        dutch_code => '7006',
        label      => 'Mexico'
    },
    {
        id         => '8f63aa17-1317-4e2c-81f8-8a83e9949884',
        dutch_code => '7007',
        label      => 'Costa Rica'
    },
    {
        id         => '7482e043-727c-472d-803f-1325b286711e',
        dutch_code => '7008',
        label      => 'Gambia'
    },
    {
        id         => '06c5524e-1b7f-4de6-8807-b3db0aff7ee9',
        dutch_code => '7009',
        label      => 'Syrië'
    },
    {
        id         => '41bc6e4b-159c-41f3-b2eb-446ca449424e',
        dutch_code => '7011',
        label      => 'Nederlandse Antillen'
    },
    {
        id         => '63b94fef-6c8d-42bc-b410-82c08d776ffa',
        dutch_code => '7012',
        label      => 'Zuidjemen'
    },
    {
        id         => '39db2d68-8742-48b1-8dfc-586364af73a0',
        dutch_code => '7014',
        label      => 'Egypte'
    },
    {
        id         => '671e9025-0664-4419-bf1b-b554cdea0be7',
        dutch_code => '7015',
        label      => 'Argentinië'
    },
    {
        id         => 'c9b53645-5188-4b4e-8d13-f9adb1bcefe7',
        dutch_code => '7016',
        label      => 'Lesotho'
    },
    {
        id         => '70ca7335-40e1-4e0b-adad-451625b7f4ab',
        dutch_code => '7017',
        label      => 'Honduras'
    },
    {
        id         => '07c9624c-0ca2-45e7-8ecd-443b6cd2987f',
        dutch_code => '7018',
        label      => 'Nicaragua'
    },
    {
        id         => 'd62d8919-70bf-40da-92d2-746788d2d7e9',
        dutch_code => '7020',
        label      => 'Pakistan'
    },
    {
        id         => 'd38ada9f-55be-4fc5-b819-7e9359727f10',
        dutch_code => '7021',
        label      => 'Senegal'
    },
    {
        id         => 'd81adfa9-df43-4685-9b66-17464992eda4',
        dutch_code => '7023',
        label      => 'Dahomey'
    },
    {
        id         => '75d8e7a7-19f1-4956-8344-4adb0be2c4ec',
        dutch_code => '7024',
        label      => 'Bulgarije'
    },
    {
        id         => '2f4653e3-9629-4768-884d-3d64cab9deff',
        dutch_code => '7026',
        label      => 'Maleisië'
    },
    {
        id         => 'bd6bc111-31ad-441e-ad9b-90bf7f8fee04',
        dutch_code => '7027',
        label      => 'Dominicaanse Republiek'
    },
    {
        id         => 'd786ae5b-a541-4ee3-a5c9-1e640497588c',
        dutch_code => '7028',
        label      => 'Polen'
    },
    {
        id         => '450e154a-cb09-4001-ab24-9dfd7d831bf5',
        dutch_code => '7029',
        label      => 'Rusland (oud)'
    },
    {
        id         => 'ff9a54a2-0f05-4e56-8128-d875fb96a4ef',
        dutch_code => '7030',
        label      => 'Britse Maagdeneilanden'
    },
    {
        id         => '86c8ddaf-0232-467f-b49d-d9049e76c71f',
        dutch_code => '7031',
        label      => 'Tanzania'
    },
    {
        id         => '8008566e-7243-40c2-9bbe-83606f0add9e',
        dutch_code => '7032',
        label      => 'El Salvador'
    },
    {
        id         => 'd45dd2d2-1ad7-4d25-a159-1a212d97ad2f',
        dutch_code => '7033',
        label      => 'Sri Lanka'
    },
    {
        id         => '1233c50d-b3f1-4b28-a37b-903fc161304a',
        dutch_code => '7034',
        label      => 'Soedan'
    },
    {
        id         => '1dbb3cac-c11d-4a0d-b33e-877e440a68d6',
        dutch_code => '7035',
        label      => 'Japan'
    },
    {
        id         => '3ecc0327-037b-4689-95d1-bb71aa60a85e',
        dutch_code => '7036',
        label      => 'Hongkong'
    },
    {
        id         => 'b1fed16a-c7a3-4fcb-90f9-901c8fa54461',
        dutch_code => '7037',
        label      => 'Panama'
    },
    {
        id         => '7c8418b0-872e-45eb-94ea-5de597b49641',
        dutch_code => '7038',
        label      => 'Uruguay'
    },
    {
        id         => '2a20c63d-5ac1-44ec-b825-17f02dcc609b',
        dutch_code => '7039',
        label      => 'Ecuador'
    },
    {
        id         => '5efced5c-e1b6-46bb-acbf-ddd0428085f0',
        dutch_code => '7040',
        label      => 'Guinee'
    },
    {
        id         => 'a3b58073-ce44-4c8a-9b65-9e20dcd48c6f',
        dutch_code => '7041',
        label      => 'Maldiven'
    },
    {
        id         => '2f220e29-4e88-4c46-b56e-a2e6fa82c9fd',
        dutch_code => '7042',
        label      => 'Thailand'
    },
    {
        id         => '1450d655-5e68-4a50-b404-572148fe5b8d',
        dutch_code => '7043',
        label      => 'Libanon'
    },
    {
        id         => 'a5ac9d79-04df-4a5a-8345-bf63a0632af5',
        dutch_code => '7044',
        label      => 'Italië'
    },
    {
        id         => 'c6c7bd22-8f86-4bcf-8631-5085e3c8c397',
        dutch_code => '7045',
        label      => 'Koeweit'
    },
    {
        id         => '2314933b-acb8-44d8-b1d7-a71a0d75314d',
        dutch_code => '7046',
        label      => 'India'
    },
    {
        id         => 'e13aa2d1-2abc-408e-ae53-2519ea881d97',
        dutch_code => '7047',
        label      => 'Roemenië'
    },
    {
        id         => 'a5ad94ce-0960-4322-89e9-ec731cee6587',
        dutch_code => '7048',
        label      => 'Tsjechoslowakije'
    },
    {
        id         => '3e61bfa4-b7ef-47b2-9f44-bc26faba9175',
        dutch_code => '7049',
        label      => 'Peru'
    },
    {
        id         => '0f564722-86ad-4113-952d-a91c0b1e2a9e',
        dutch_code => '7050',
        label      => 'Portugal'
    },
    {
        id         => '901b2edf-0379-48fd-83a3-3cd45c2437d1',
        dutch_code => '7051',
        label      => 'Oman'
    },
    {
        id         => 'f0433d2b-9eac-4305-ad10-51fe848ffdbe',
        dutch_code => '7052',
        label      => 'Mongolië'
    },
    {
        id         => 'fc88817a-90ea-4eaf-9496-cae7671b13a1',
        dutch_code => '7054',
        label      => 'Verenigde Arabische Emiraten'
    },
    {
        id         => 'b8e5e916-0a42-4bbc-b109-0c717d48b1fa',
        dutch_code => '7055',
        label      => 'Tibet'
    },
    {
        id         => '15185d83-950e-475a-a917-405fa06075e1',
        dutch_code => '7057',
        label      => 'Nauru'
    },
    {
        id         => '6ce7416d-e13d-4359-b65a-e517a741d1c2',
        dutch_code => '7058',
        label      => 'Nederlands Nieuwguinea'
    },
    {
        id         => '93556942-ca91-40a2-ac1d-7c2f4c78c0c3',
        dutch_code => '7059',
        label      => 'Tanganyika'
    },
    {
        id         => 'd9ecff10-d1b4-4c02-9a5f-46df85811cf9',
        dutch_code => '7060',
        label      => 'Palestina'
    },
    {
        id         => 'dea2b0eb-0043-403b-93d4-fd38d1b47f8b',
        dutch_code => '7062',
        label      => 'Brits Westindië'
    },
    {
        id         => '53ca81d1-4207-40be-b5e8-ddaaa50fd8df',
        dutch_code => '7063',
        label      => 'Portugees Afrika'
    },
    {
        id         => '68083864-4e84-4cba-992b-b3ab72fdad00',
        dutch_code => '7064',
        label      => 'Letland'
    },
    {
        id         => '544740b9-a9ca-47cc-bce2-663390da3511',
        dutch_code => '7065',
        label      => 'Estland'
    },
    {
        id         => '15607076-de33-4e2a-b19c-b977d21a5471',
        dutch_code => '7066',
        label      => 'Litouwen'
    },
    {
        id         => '062b3889-efa0-4d8b-ab7c-8ab5a1049648',
        dutch_code => '7067',
        label      => 'Brits Afrika'
    },
    {
        id         => '77067ff3-0c12-4de2-a106-6b388f68d2d7',
        dutch_code => '7068',
        label      => 'Belgisch Congo'
    },
    {
        id         => '1253a4c5-8f8c-4edf-847f-bca0ae78644f',
        dutch_code => '7070',
        label      => 'Brits Indië'
    },
    {
        id         => '821f283f-54e0-4e2c-bfcf-25e966aefaa7',
        dutch_code => '7071',
        label      => 'Noordrhodesië'
    },
    {
        id         => 'fa2b779e-2ab2-4b9e-bdc7-b8ff5e20cfef',
        dutch_code => '7072',
        label      => 'Zuidrhodesië'
    },
    {
        id         => '24ccd504-dab9-409b-9ce1-4fd8eeb84712',
        dutch_code => '7073',
        label      => 'Saarland'
    },
    {
        id         => '166fe0c3-6a25-40b8-ad35-318bf4aaa823',
        dutch_code => '7074',
        label      => 'Frans Indo China'
    },
    {
        id         => '5dc98a4c-3388-404b-9d0d-3e2cee2d4416',
        dutch_code => '7075',
        label      => 'Brits Westborneo'
    },
    {
        id         => '201a03d3-7040-4e1d-ab2e-9fc6247d9bff',
        dutch_code => '7076',
        label      => 'Goudkust'
    },
    {
        id         => '92bc5370-cdbf-49ac-8523-e9ff22fa42e1',
        dutch_code => '7077',
        label      => 'Ras-El-Cheima'
    },
    {
        id         => '6bee2ab5-ec83-4147-b867-7dad693238fc',
        dutch_code => '7079',
        label      => 'Frans Congo'
    },
    {
        id         => '02b22e8b-54dd-4942-bb0d-a08cf200881e',
        dutch_code => '7080',
        label      => 'Siam'
    },
    {
        id         => 'af4ce11a-fa05-4b13-b40d-73048d107876',
        dutch_code => '7082',
        label      => 'Brits Oostafrika'
    },
    {
        id         => '0423ad04-a2a8-432d-be33-2b7e7429cf7d',
        dutch_code => '7083',
        label      => 'Brits Noordborneo'
    },
    {
        id         => '67b43859-1767-4733-ad2d-589d3f78d8d3',
        dutch_code => '7084',
        label      => 'Bangladesh'
    },
    {
        id         => 'f1d4d8c8-0025-4c7a-a8e0-9d38f1bc34d0',
        dutch_code => '7085',
        label      => 'Duitse Democratische Republiek'
    },
    {
        id         => 'ca1fa6c7-03ea-4235-8667-829a92448cc1',
        dutch_code => '7087',
        label      => 'Madeira-eilanden'
    },
    {
        id         => '60d7c96e-f3f3-4936-a1f5-a4e2be456a25',
        dutch_code => '7088',
        label      => 'Amerikaanse Maagdeneilanden'
    },
    {
        id         => 'f9863a02-5f1f-462b-a802-31a79d547ee0',
        dutch_code => '7089',
        label      => 'Australische Salomonseilanden'
    },
    {
        id         => '1a0f4afd-bc8d-49b1-b2d9-328c35d38a79',
        dutch_code => '7091',
        label      => 'Spaanse Sahara'
    },
    {
        id         => '8a62bc62-1d6f-432c-a866-f4e57a53b9c3',
        dutch_code => '7092',
        label      => 'Caymaneilanden'
    },
    {
        id         => 'b4c73c5f-e53b-4101-afd1-8eb8810cfe29',
        dutch_code => '7093',
        label      => 'Caicoseilanden'
    },
    {
        id         => '927d9285-5710-4c3d-8b43-ca5a435fbfeb',
        dutch_code => '7094',
        label      => 'Turkseilanden'
    },
    {
        id         => 'bee125e2-1d9d-447b-8bde-4e2366b3551b',
        dutch_code => '7095',
        label      => 'Brits Territorium in Antarctica'
    },
    {
        id         => 'faa62abc-fec3-4594-83d8-4b7585a94586',
        dutch_code => '7096',
        label      => 'Brits Territorium in de Indische Oceaan'
    },
    {
        id         => '8e3a8da6-d79c-4413-9e0a-7eb1a90f595a',
        dutch_code => '7097',
        label      => 'Cookeilanden'
    },
    {
        id         => 'f98cd51a-5e33-455e-9111-3891377fa8cf',
        dutch_code => '7098',
        label      => 'Tokelau-eilanden'
    },
    {
        id         => '9c2f11b7-e118-4bce-85a1-e94f92b94d2d',
        dutch_code => '7099',
        label      => 'Nieuwcaledonië'
    },
    {
        id         => '57bf4be2-f820-4303-80b7-a4d250cd371a',
        dutch_code => '8000',
        label      => 'Hawaii-eilanden'
    },
    {
        id         => 'e39f94bb-7c2a-4471-bb51-972539cb2337',
        dutch_code => '8001',
        label      => 'Guam'
    },
    {
        id         => '15a43e89-e057-4d82-8a33-5646544c13cb',
        dutch_code => '8002',
        label      => 'Amerikaans Samoa'
    },
    {
        id         => '93b607f9-4594-4e8d-9fee-355338ee8f16',
        dutch_code => '8003',
        label      => 'Midway'
    },
    {
        id         => '04105bce-d192-47cc-99b3-87abdd1a73a2',
        dutch_code => '8004',
        label      => 'Ryukyueilanden'
    },
    {
        id         => '73573ced-075c-45c0-8a47-fecb83f77ffc',
        dutch_code => '8005',
        label      => 'Wake'
    },
    {
        id         => '9c95658d-9789-4aff-bb0f-a9a2539837a3',
        dutch_code => '8006',
        label      => 'Pacific eilanden'
    },
    {
        id         => 'a0abb52a-64c4-4166-b80c-502a63c2a717',
        dutch_code => '8008',
        label      => 'Grenada'
    },
    {
        id         => '76eb1759-70cd-4383-8535-945ca156617e',
        dutch_code => '8009',
        label      => 'Marianen'
    },
    {
        id         => 'be94ae0a-f07e-4ee1-951d-75544abad18c',
        dutch_code => '8010',
        label      => 'Cabinda'
    },
    {
        id         => 'e903a2b6-8434-4e71-b8cf-6d90fc211075',
        dutch_code => '8011',
        label      => 'Canton en Enderbury'
    },
    {
        id         => '614a5466-4774-464e-87c6-481f5bbe3776',
        dutch_code => '8012',
        label      => 'Christmaseiland'
    },
    {
        id         => '77edcf3a-47ed-4562-ab0b-6dd3571d75a8',
        dutch_code => '8013',
        label      => 'Cocoseilanden'
    },
    {
        id         => '35ec0828-4829-489b-944b-afabec555ae0',
        dutch_code => '8014',
        label      => 'Faeröer'
    },
    {
        id         => '10853987-4dd1-4ae6-a388-4e9f163c4736',
        dutch_code => '8015',
        label      => 'Montserrat'
    },
    {
        id         => '08fc5d42-aeef-4b44-9c3a-3eb6a74ecca0',
        dutch_code => '8016',
        label      => 'Norfolk'
    },
    {
        id         => 'b7f2c849-88a7-4972-87eb-c3a1c071d5d8',
        dutch_code => '8017',
        label      => 'Belize'
    },
    {
        id         => '24446b25-8484-4124-91c7-9d1268104ec8',
        dutch_code => '8018',
        label      => 'Tasmanië'
    },
    {
        id         => '4a5e6707-64e0-4539-8910-5a93594607ec',
        dutch_code => '8019',
        label      => 'Turks- en Caicoseilanden'
    },
    {
        id         => '7fe6caad-b96a-478b-a4e2-63090d781da6',
        dutch_code => '8020',
        label      => 'Puerto Rico'
    },
    {
        id         => '06fd57ac-3696-4994-a670-f9b18876f80e',
        dutch_code => '8021',
        label      => 'Papua-Nieuwguinea'
    },
    {
        id         => '71887499-d7fd-44a9-b53e-d8b96901d09d',
        dutch_code => '8022',
        label      => 'Solomoneilanden'
    },
    {
        id         => '4c4f4464-8a8b-4af3-98ff-8aaa4a0ba91b',
        dutch_code => '8023',
        label      => 'Benin'
    },
    {
        id         => '00af179c-c58d-4892-b87f-42c2c74eee39',
        dutch_code => '8024',
        label      => 'Viëtnam'
    },
    {
        id         => 'e31c4b20-fe5f-4864-958e-98df3b685aab',
        dutch_code => '8025',
        label      => 'Kaapverdië'
    },
    {
        id         => '0226318d-f3ce-44a4-9b4d-d8e88d4048b9',
        dutch_code => '8026',
        label      => 'Seychellen'
    },
    {
        id         => '3112a681-6a66-4a6a-9dde-980594ff81e0',
        dutch_code => '8027',
        label      => 'Kiribati'
    },
    {
        id         => 'eba96c80-d2c8-4a8a-8b40-a9460f5ca0b7',
        dutch_code => '8028',
        label      => 'Tuvalu'
    },
    {
        id         => '17bd2118-5b3d-4724-87fc-205f21b046e3',
        dutch_code => '8029',
        label      => 'Sint Lucia'
    },
    {
        id         => '73ead23b-d804-4984-9fa8-f3f320e744b3',
        dutch_code => '8030',
        label      => 'Dominica'
    },
    {
        id         => '661a10f9-931f-4bc9-b3bf-97d3e642e7ec',
        dutch_code => '8031',
        label      => 'Zimbabwe'
    },
    {
        id         => '6683759c-f7d2-41b9-8bdf-ab9879b9c41e',
        dutch_code => '8032',
        label      => 'Doebai'
    },
    {
        id         => '68ce232f-cf0d-41d5-90ce-9ae2d75d6344',
        dutch_code => '8033',
        label      => 'Nieuwehebriden'
    },
    {
        id         => 'ccd85a43-9aeb-4852-be4e-0d3192a3194d',
        dutch_code => '8034',
        label      => 'Kanaaleilanden'
    },
    {
        id         => '020ee313-4860-466d-9d7b-7721b23721cc',
        dutch_code => '8035',
        label      => 'Man'
    },
    {
        id         => '62c706a7-8187-487c-b0fd-e5261d246509',
        dutch_code => '8036',
        label      => 'Anguilla'
    },
    {
        id         => 'f73ea327-eec7-4587-a6d8-1c9cf1ab1664',
        dutch_code => '8037',
        label      => 'Saint Kitts-Nevis'
    },
    {
        id         => 'd1916f6e-ed77-4539-acf2-125188c32a82',
        dutch_code => '8038',
        label      => 'Antigua'
    },
    {
        id         => '2680234b-6b07-4b32-b1aa-5c8ea883879d',
        dutch_code => '8039',
        label      => 'Sint Vincent'
    },
    {
        id         => '371b7a84-2ca3-4b23-85de-397d2a92f3a7',
        dutch_code => '8040',
        label      => 'Gilberteilanden'
    },
    {
        id         => 'cc142aa8-8b28-44d2-8d08-2b21e0b67538',
        dutch_code => '8041',
        label      => 'Panamakanaalzone'
    },
    {
        id         => 'ce8eb788-f324-49af-8149-6b062650a8c8',
        dutch_code => '8042',
        label      => 'Saint Kitts-Nevis-Anguilla'
    },
    {
        id         => '4cd268d9-f416-4ebf-a111-f14965af891a',
        dutch_code => '8043',
        label      => 'Belau'
    },
    {
        id         => 'df7204f2-f97f-41ee-ac5d-2328b8a8ccae',
        dutch_code => '8044',
        label      => 'Republiek van Palau'
    },
    {
        id         => 'b3ea09c2-74dc-44fe-9698-df0b3f7b4742',
        dutch_code => '8045',
        label      => 'Antigua en Barbuda'
    },
    {
        id         => 'e04fef14-9e6f-48dd-90e9-a6fba8dc0e6d',
        dutch_code => '9000',
        label      => 'Newfoundland'
    },
    {
        id         => '4f516597-ecfc-4d5c-a896-4d5c498ff337',
        dutch_code => '9001',
        label      => 'Nyasaland'
    },
    {
        id         => '9049d172-44a9-4ba5-9c96-2d5c4bd2ed88',
        dutch_code => '9003',
        label      => 'Eritrea'
    },
    {
        id         => '6c264d6b-bdfc-49d9-a454-7a102a70ef7e',
        dutch_code => '9005',
        label      => 'Ifni'
    },
    {
        id         => 'cc9a03d5-3465-4a2c-9d96-a7e40fdae8ea',
        dutch_code => '9006',
        label      => 'Brits Kameroen'
    },
    {
        id         => 'd2b6e78a-aeb8-43fa-9780-88ed3e6e23c0',
        dutch_code => '9007',
        label      => 'Kaiser Wilhelmsland'
    },
    {
        id         => '3bb4c8bf-4ddd-469f-b755-bf8c71f5ec4c',
        dutch_code => '9008',
        label      => 'Kongo'
    },
    {
        id         => 'b4ecf2c8-8cab-425c-b180-9aa071d4c30a',
        dutch_code => '9009',
        label      => 'Kongo Kinshasa'
    },
    {
        id         => 'eeb59ff5-306a-46b8-b076-e49192ac3f33',
        dutch_code => '9010',
        label      => 'Madagaskar'
    },
    {
        id         => 'f0ebe912-bc1f-4ef1-97d8-9889a3e03fbf',
        dutch_code => '9013',
        label      => 'Kongo Brazzaville'
    },
    {
        id         => '6dc5cd3b-e62c-4490-885d-80c671ac766d',
        dutch_code => '9014',
        label      => 'Leewardeilanden'
    },
    {
        id         => 'e28ca37f-5663-46df-8ad0-33a5d03f3d40',
        dutch_code => '9015',
        label      => 'Windwardeilanden'
    },
    {
        id         => 'c312feb8-edb7-4e07-9b45-f55f1de8497a',
        dutch_code => '9016',
        label      => 'Frans gebied van Afars en Issa\'s'
    },
    {
        id         => '41f834a7-3d67-4219-ae4e-9ee3fc6aa74d',
        dutch_code => '9017',
        label      => 'Phoenixeilanden'
    },
    {
        id         => '028ce053-5f08-47e1-a82a-d8eb1a283a56',
        dutch_code => '9020',
        label      => 'Portugees Guinee'
    },
    {
        id         => '046b2d28-d356-488e-834a-bfe78cab611c',
        dutch_code => '9022',
        label      => 'Duits Zuidwestafrika'
    },
    {
        id         => 'e0ce9839-8885-4272-b91a-6222360fc06c',
        dutch_code => '9023',
        label      => 'Namibië'
    },
    {
        id         => '0624db74-24d5-4443-a497-5287b5898b1c',
        dutch_code => '9027',
        label      => 'Brits Somaliland'
    },
    {
        id         => 'cc698900-2ebd-4511-9e2d-d7f6947dcf95',
        dutch_code => '9028',
        label      => 'Italiaans Somaliland'
    },
    {
        id         => 'a23db607-f297-4601-aed8-0ca6b1635918',
        dutch_code => '9030',
        label      => 'Nederlands Indië'
    },
    {
        id         => '7f053f21-230d-4e28-86fd-317c06d69851',
        dutch_code => '9031',
        label      => 'Brits Guyana'
    },
    {
        id         => 'cd9ca617-4069-4e70-b616-4e6094c4fbf0',
        dutch_code => '9036',
        label      => 'Swaziland'
    },
    {
        id         => '6af8ee03-40dc-4618-b544-0014d6028074',
        dutch_code => '9037',
        label      => 'Qatar'
    },
    {
        id         => 'd33e458b-5307-4155-8b28-b3a084793030',
        dutch_code => '9041',
        label      => 'Aden'
    },
    {
        id         => '40f75506-88fb-4ef0-af95-c9343bd1921d',
        dutch_code => '9042',
        label      => 'Zuidarabische Federatie'
    },
    {
        id         => '058b8779-b7da-45ac-819e-da95732f5ed5',
        dutch_code => '9043',
        label      => 'Equatoriaalguinee'
    },
    {
        id         => '34c277ba-9e80-4d72-90a9-36f56a576c9b',
        dutch_code => '9044',
        label      => 'Spaans Guinee'
    },
    {
        id         => '52e9de2c-b6ba-43d5-8211-1bf924ea9125',
        dutch_code => '9047',
        label      => 'Verenigde Arabische Republiek'
    },
    {
        id         => '59099e78-63bd-46ab-8d1c-76f82338e1e8',
        dutch_code => '9048',
        label      => 'Bermuda'
    },
    {
        id         => 'd4596a1e-66a4-42bb-a1ae-3df092ed8658',
        dutch_code => '9049',
        label      => 'Sovjetunie'
    },
    {
        id         => '6078bdb0-ff47-4599-a606-2a0c67426167',
        dutch_code => '9050',
        label      => 'Duits Oostafrika'
    },
    {
        id         => '9b2a1615-a0f8-485a-be91-97eb87fd5143',
        dutch_code => '9051',
        label      => 'Zanzibar'
    },
    {
        id         => '246cec1d-4293-4647-b0cc-f0086468ece2',
        dutch_code => '9052',
        label      => 'Ceylon'
    },
    {
        id         => '0458bf24-7165-40bf-8054-3021ee0452da',
        dutch_code => '9053',
        label      => 'Muscat en Oman'
    },
    {
        id         => 'c98f006c-f8c2-4acc-bd42-cf050622e6b0',
        dutch_code => '9054',
        label      => 'Trucial Oman'
    },
    {
        id         => '9999b199-151a-4b8e-86d3-960807c4f537',
        dutch_code => '9055',
        label      => 'Indo China'
    },
    {
        id         => 'b8ac54cd-167f-4abc-9d4c-b3c93bcc9a4f',
        dutch_code => '9056',
        label      => 'Marshalleilanden'
    },
    {
        id         => 'e2702fd0-fcff-4ab5-b16c-78f1542d6a89',
        dutch_code => '9057',
        label      => 'Sarawak'
    },
    {
        id         => '30c8b430-32a2-4e4a-a8ca-3391c56a148b',
        dutch_code => '9058',
        label      => 'Brits Borneo'
    },
    {
        id         => 'e02a904e-e316-4c83-bd62-b93b563d40b9',
        dutch_code => '9060',
        label      => 'Sabah'
    },
    {
        id         => '6f6b1133-7cd8-4c26-87a3-dc51388d8e32',
        dutch_code => '9061',
        label      => 'Aboe Dhabi'
    },
    {
        id         => 'fa95ef05-e436-482a-96c3-71afe37a7802',
        dutch_code => '9062',
        label      => 'Adjman'
    },
    {
        id         => '1310f6be-6968-4ba0-96c8-1d1543b9bc21',
        dutch_code => '9063',
        label      => 'Basoetoland'
    },
    {
        id         => 'd8f1e5f2-d68b-4e95-8738-d54fc4f7737a',
        dutch_code => '9064',
        label      => 'Bechuanaland'
    },
    {
        id         => '15fc93de-b739-4e2c-9eee-92cde707a80b',
        dutch_code => '9065',
        label      => 'Foedjaira'
    },
    {
        id         => '71aabd2d-a130-480c-93b3-8bbaa824b11f',
        dutch_code => '9066',
        label      => 'Frans Kameroen'
    },
    {
        id         => 'a27e0de3-9a4a-46c1-a4b0-92d332d6854b',
        dutch_code => '9067',
        label      => 'Johore'
    },
    {
        id         => '3544feec-8dba-4533-9529-a9405dd01652',
        dutch_code => '9068',
        label      => 'Korea'
    },
    {
        id         => 'cc8a06ff-1c55-43cc-b411-06902ff94b23',
        dutch_code => '9069',
        label      => 'Labuan'
    },
    {
        id         => '6ad55c27-5d2f-428b-8efb-518f5bd4f471',
        dutch_code => '9070',
        label      => 'Oem el Koewein'
    },
    {
        id         => '19c0e3d9-056c-45db-b5b2-e111a668ed27',
        dutch_code => '9071',
        label      => 'Oostenrijk-Hongarije'
    },
    {
        id         => '4f86bce1-f029-4f20-bb8f-e9b1f77f3424',
        dutch_code => '9072',
        label      => 'Portugees Oost Afrika'
    },
    {
        id         => '803fc072-c65d-4d12-a82b-d117a585b011',
        dutch_code => '9073',
        label      => 'Portugees West Afrika'
    },
    {
        id         => 'cb36a420-f08e-4817-ac71-87c72899e9d9',
        dutch_code => '9074',
        label      => 'Sjardja'
    },
    {
        id         => '37f3ecf6-2006-43b5-aa7b-79a7252e0e57',
        dutch_code => '9075',
        label      => 'Straits Settlements'
    },
    {
        id         => 'ed56eea4-addd-49a1-8d84-34b9f111d5c4',
        dutch_code => '9076',
        label      => 'Abessinië'
    },
    {
        id         => '3f8fe1b9-90dd-4f53-837e-3c7d28c3f12d',
        dutch_code => '9077',
        label      => 'Frans West Afrika'
    },
    {
        id         => 'c8256eea-a4c4-4cde-9bf6-57422fe0ef1c',
        dutch_code => '9078',
        label      => 'Frans Equatoriaal Afrika'
    },
    {
        id         => '3af27e09-3486-4aba-b7ca-c80584fce533',
        dutch_code => '9081',
        label      => 'Oeroendi'
    },
    {
        id         => '061be605-f8cb-46e6-94fa-23c8f5959fe5',
        dutch_code => '9082',
        label      => 'Roeanda-Oeroendi'
    },
    {
        id         => '2814320b-71f2-4f1e-a71f-a43f7004c134',
        dutch_code => '9084',
        label      => 'Goa'
    },
    {
        id         => '573445be-8cbc-4f35-af3d-de4cf2022cab',
        dutch_code => '9085',
        label      => 'Dantzig'
    },
    {
        id         => 'a1f4fec9-4fe7-4423-86e5-ba93fe303c17',
        dutch_code => '9086',
        label      => 'Centrafrika'
    },
    {
        id         => '8aa0feeb-3636-4214-b262-ac2008589982',
        dutch_code => '9087',
        label      => 'Djibouti'
    },
    {
        id         => 'c824d624-22d5-4ae6-a2dc-f94b6957a3b4',
        dutch_code => '9088',
        label      => 'Transjordanië'
    },
    {
        id         => '4d1d4b1a-358b-471b-9563-bb1aecb22e0b',
        dutch_code => '9089',
        label      => 'Bondsrepubliek Duitsland'
    },
    {
        id         => '2de71db7-de99-4ce0-9b3a-1f56b0d17899',
        dutch_code => '9090',
        label      => 'Vanuatu'
    },
    {
        id         => 'cde07dc7-db88-49b4-b420-adcc58e0b418',
        dutch_code => '9091',
        label      => 'Niue'
    },
    {
        id         => 'baa94555-80fe-4242-a00e-6d2e1a2c4598',
        dutch_code => '9092',
        label      => 'Spaans Noordafrika'
    },
    {
        id         => '669b1dd2-0337-4ea9-88c4-67e0a010cd6b',
        dutch_code => '9093',
        label      => 'Westelijke Sahara'
    },
    {
        id         => '68509783-7365-4d5a-af98-56dfbb3d09f3',
        dutch_code => '9094',
        label      => 'Micronesia'
    },
    {
        id         => '000d569f-6cdc-4d28-8572-5f7bd45e2cbc',
        dutch_code => '9095',
        label      => 'Svalbardeilanden'
    },
    {
        id         => '86222636-84db-4768-b339-bb6f0b1eadbe',
        dutch_code => '9999',
        label      => 'Internationaal gebied'
    }
];

=head2 MUNICIPALITY_TABLE

    [
        {
            id         => '0f44329e-ccb9-4bae-8c54-fef95b4b7328',
            dutch_code => '3',
            label      => 'Appingedam'
        },
        ...
    ],

=cut

use constant MUNICIPALITY_TABLE => [
    {
        id         => '02dbb0de-ca03-473b-9f7d-1ca80360e878',
        dutch_code => '9999',
        label      => 'Testgemeente',
    },
    {
        id         => '02dbb0de-ca03-473b-9f7d-1ca80360e878',
        dutch_code => '1332',
        label      => 'Testgemeente (historisch)',
        historical => \1,
    },
    {
        id         => 'b4edc05c-0f29-45d6-b12a-81b986a882fd',
        dutch_code => '1999',
        label      => 'RNI / Registratie Niet Ingezetenen',
    },
    {
        id         => '0f44329e-ccb9-4bae-8c54-fef95b4b7328',
        dutch_code => '3',
        label      => 'Appingedam'
    },
    {
        id         => '26f48bd5-9247-45ad-936a-363a358f40e5',
        dutch_code => '5',
        label      => 'Bedum'
    },
    {
        id         => '7bb9e5f7-b594-42c6-a4c0-5694d9df89e9',
        dutch_code => '7',
        label      => 'Bellingwedde',
        historical => \1,
    },
    {
        id         => '831b4209-8c06-412d-ae91-44a59f324f3e',
        dutch_code => '9',
        label      => 'Ten Boer'
    },
    {
        id         => '987667cd-f38f-49ad-8385-d8e56c023234',
        dutch_code => '10',
        label      => 'Delfzijl'
    },
    {
        id         => '109e16db-d984-4f4b-9a23-87fe0718636a',
        dutch_code => '14',
        label      => 'Groningen'
    },
    {
        id         => 'c812f63d-a7a9-49db-81b4-5ba00cda5d6b',
        dutch_code => '15',
        label      => 'Grootegast'
    },
    {
        id         => '38d2429a-5095-4197-aec1-697e48b1552c',
        dutch_code => '17',
        label      => 'Haren'
    },
    {
        id         => '998f2f43-c434-42e7-a72b-214f7dc83eff',
        dutch_code => '18',
        label      => 'Hoogezand-Sappemeer',
        historical => \1
    },
    {
        id         => 'c0eebf0c-ccf7-4900-9968-6b43bfeed25a',
        dutch_code => '22',
        label      => 'Leek'
    },
    {
        id         => '5039632c-3330-4acf-9b9b-4f4c44f561f8',
        dutch_code => '24',
        label      => 'Loppersum'
    },
    {
        id         => 'd84f6efe-3705-479d-bd0d-c5425466f88a',
        dutch_code => '25',
        label      => 'Marum'
    },
    {
        id         => '17ceec51-4d87-4ac0-ac8e-29eeeda9e5d9',
        dutch_code => '34',
        label      => 'Almere'
    },
    {
        id         => '7bba996b-b884-4e36-a7e6-96986416c320',
        dutch_code => '37',
        label      => 'Stadskanaal'
    },
    {
        id         => '4e444792-9fc2-4750-8e37-730011f985de',
        dutch_code => '39',
        label      => 'Scheemda',
        historical => \1,
    },
    {
        id         => '0e27c360-d2b1-4d7c-a0dc-a7a761046c46',
        dutch_code => '40',
        label      => 'Slochteren',
        historical => \1
    },
    {
        id         => '3db65516-da22-40a8-8e45-003cbc1f74bf',
        dutch_code => '47',
        label      => 'Veendam'
    },
    {
        id         => '25ed5668-3787-413c-a788-50685f69f64d',
        dutch_code => '48',
        label      => 'Vlagtwedde',
        historical => \1
    },
    {
        id         => '455865b0-a817-491a-a5f6-d9d6cb3a96e2',
        dutch_code => '50',
        label      => 'Zeewolde'
    },
    {
        id         => '4db9aa8c-4167-4bd2-b4f8-f044bffbc063',
        dutch_code => '51',
        label      => 'Skarsterlân',
        historical => \1,
    },
    {
        id         => 'e84c5221-7385-4b21-91d2-df82cbff34bf',
        dutch_code => '52',
        label      => 'Winschoten',
        historical => \1,
    },
    {
        id         => 'dacc2f85-dd19-40eb-bd30-11c7b75a3625',
        dutch_code => '53',
        label      => 'Winsum'
    },
    {
        id         => 'd36988b5-9853-4315-876a-9c5644d0b32c',
        dutch_code => '55',
        label      => 'Boarnsterhim',
        historical => \1,
    },
    {
        id         => '60cbae0c-de29-4646-b6de-ceb8be152bba',
        dutch_code => '56',
        label      => 'Zuidhorn'
    },
    {
        id         => '903a98f7-d4b4-4314-830a-0b6941a58608',
        dutch_code => '58',
        label      => 'Dongeradeel'
    },
    {
        id         => '04333305-e4c0-4ff1-b316-eb4e31569dbd',
        dutch_code => '59',
        label      => 'Achtkarspelen'
    },
    {
        id         => '38236167-f86f-4aad-aec2-5258be11729a',
        dutch_code => '60',
        label      => 'Ameland'
    },
    {
        id         => '294196e0-fb9c-436f-acd8-98d6c90f1298',
        dutch_code => '63',
        label      => 'het Bildt',
        historical => \1
    },
    {
        id         => 'a5e661ab-4eac-4221-bd35-ea8d51a3cffa',
        dutch_code => '64',
        label      => 'Bolsward',
        historical => \1,
    },
    {
        id         => '1b3d05ff-24bf-47c5-abea-7e6ae3f63284',
        dutch_code => '70',
        label      => 'Franekeradeel',
        historical => \1
    },
    {
        id         => '0dc4eb94-53ff-4a6c-9687-7eb16da1d4cb',
        dutch_code => '72',
        label      => 'Harlingen'
    },
    {
        id         => '93707cf4-10f9-4564-af90-0d0b17a7f02e',
        dutch_code => '74',
        label      => 'Heerenveen'
    },
    {
        id         => 'f44c90c4-2faf-485b-bf60-63cc4e920462',
        dutch_code => '79',
        label      => 'Kollumerland en Nieuwkruisland'
    },
    {
        id         => '61ca9ea2-43c6-409b-8828-f643534de85c',
        dutch_code => '80',
        label      => 'Leeuwarden'
    },
    {
        id         => '23e6e34e-70f4-4ee1-8b0b-263f7af79933',
        dutch_code => '81',
        label      => 'Leeuwarderadeel',
        historical => \1
    },
    {
        id         => 'fde85358-7b39-44cb-b30a-719f9ab4625b',
        dutch_code => '82',
        label      => 'Lemsterland',
        historical => \1,
    },
    {
        id         => '5c3771d9-239c-4dfc-a317-06f7acfe7933',
        dutch_code => '83',
        label      => 'Menaldumadeel',
        historical => \1,
    },
    {
        id         => '5e70afbe-b617-4ed5-89d3-f9ad9f80cd90',
        dutch_code => '85',
        label      => 'Ooststellingwerf'
    },
    {
        id         => '0429b2b6-8e4d-4102-837a-05d9b3bde6fe',
        dutch_code => '86',
        label      => 'Opsterland'
    },
    {
        id         => 'ac482447-79dd-492b-8f85-c3a497c147e3',
        dutch_code => '88',
        label      => 'Schiermonnikoog'
    },
    {
        id         => '09c3c4a4-2121-4f8b-97a8-c7bb1aa5262e',
        dutch_code => '90',
        label      => 'Smallingerland'
    },
    {
        id         => '60c0021f-ec93-414e-a7db-5267eda7e6ed',
        dutch_code => '91',
        label      => 'Sneek',
        historical => \1,
    },
    {
        id         => '6c5ebe67-e7b9-41b5-bf13-e37f43ae63e8',
        dutch_code => '93',
        label      => 'Terschelling'
    },
    {
        id         => '464ca01d-d868-4bed-a1ee-1c9b37362876',
        dutch_code => '96',
        label      => 'Vlieland'
    },
    {
        id         => '0101b3cb-2411-49c0-a282-e2e048776efe',
        dutch_code => '98',
        label      => 'Weststellingwerf'
    },
    {
        id         => '2dc89308-e768-4d80-9287-252303b793b8',
        dutch_code => '104',
        label      => 'Nijefurd',
        historical => \1,
    },
    {
        id         => 'ed386208-789a-4f1b-a0c6-9d4260f890fe',
        dutch_code => '106',
        label      => 'Assen'
    },
    {
        id         => '64abd9cb-fa26-46d8-8fc5-d61f4ea4476f',
        dutch_code => '109',
        label      => 'Coevorden'
    },
    {
        id         => 'd4c29c6f-35ca-4c35-a7de-5fda7a1adc51',
        dutch_code => '114',
        label      => 'Emmen'
    },
    {
        id         => '9258bd87-99ef-4ea0-aae9-23aad1898554',
        dutch_code => '118',
        label      => 'Hoogeveen'
    },
    {
        id         => 'b61a52c4-7c52-44c3-984d-f7fb3298e9c6',
        dutch_code => '119',
        label      => 'Meppel'
    },
    {
        id         => '71cc8847-0a46-4e99-9400-b07ab49cc850',
        dutch_code => '140',
        label      => 'Littenseradiel',
        historical => \1
    },
    {
        id         => 'd34852f9-2cec-4a3e-85c1-f27d12cd994a',
        dutch_code => '141',
        label      => 'Almelo'
    },
    {
        id         => '7434dfbb-2c2b-42b4-9ace-0b18822c350d',
        dutch_code => '147',
        label      => 'Borne'
    },
    {
        id         => 'b7c063b6-7cb4-4d0c-ae54-090bd8b85e82',
        dutch_code => '148',
        label      => 'Dalfsen'
    },
    {
        id         => 'd02401c0-475d-4dc2-ab29-a1e0cacc33ed',
        dutch_code => '150',
        label      => 'Deventer'
    },
    {
        id         => '10f1b5da-a44b-45a2-b675-80efcdae1892',
        dutch_code => '153',
        label      => 'Enschede'
    },
    {
        id         => 'b52804a0-7239-4507-9c94-27d33680bb5f',
        dutch_code => '158',
        label      => 'Haaksbergen'
    },
    {
        id         => '33bb37e6-303b-42d6-b602-b748d8e10645',
        dutch_code => '160',
        label      => 'Hardenberg'
    },
    {
        id         => '98d3a574-ff92-4f6e-ac77-f940eb2192f5',
        dutch_code => '163',
        label      => 'Hellendoorn'
    },
    {
        id         => '501912d8-d21b-4a21-bf18-f44f6bcadb24',
        dutch_code => '164',
        label      => 'Hengelo'
    },
    {
        id         => '6dcea5f9-2048-406f-94a5-3a330a347703',
        dutch_code => '166',
        label      => 'Kampen'
    },
    {
        id         => '7063ae7d-b874-40cd-b217-ba795882fd67',
        dutch_code => '168',
        label      => 'Losser'
    },
    {
        id         => '3e01f991-6560-4489-bdc4-804ef2610c59',
        dutch_code => '171',
        label      => 'Noordoostpolder'
    },
    {
        id         => '889de30f-9f6e-4afe-9615-b641fbc11a26',
        dutch_code => '173',
        label      => 'Oldenzaal'
    },
    {
        id         => '9f5ae936-33cf-4e3b-829d-1e39abcacd56',
        dutch_code => '175',
        label      => 'Ommen'
    },
    {
        id         => 'f292f45e-c732-4c26-90fc-bbb1359a722a',
        dutch_code => '177',
        label      => 'Raalte'
    },
    {
        id         => '7b7fe3e9-98b7-4259-9077-61fe1dfb100a',
        dutch_code => '180',
        label      => 'Staphorst'
    },
    {
        id         => '5a7b179b-38d5-40f9-ad19-a3d01349374a',
        dutch_code => '183',
        label      => 'Tubbergen'
    },
    {
        id         => 'a6aa39b8-e9b5-4e9f-aee2-4a582216d16a',
        dutch_code => '184',
        label      => 'Urk'
    },
    {
        id         => 'ef51400c-436a-47f3-8cac-18dc780df97a',
        dutch_code => '189',
        label      => 'Wierden'
    },
    {
        id         => '259dfcd5-3f21-417f-b2d6-51ffdf8538cd',
        dutch_code => '193',
        label      => 'Zwolle'
    },
    {
        id         => '90a40fa9-0c9d-44d2-a5b5-05a62e566913',
        dutch_code => '196',
        label      => 'Rijnwaarden',
        historical => \1
    },
    {
        id         => 'e28937f2-3c4a-4526-9175-61e16bb533d9',
        dutch_code => '197',
        label      => 'Aalten'
    },
    {
        id         => 'fe8dd89e-7145-4f13-933d-8b0ff09ef146',
        dutch_code => '200',
        label      => 'Apeldoorn'
    },
    {
        id         => 'd35b15b9-3f3f-40ef-bdcd-9634f41bff84',
        dutch_code => '202',
        label      => 'Arnhem'
    },
    {
        id         => '0fbb9c60-907d-46fa-82a5-ee36466610ff',
        dutch_code => '203',
        label      => 'Barneveld'
    },
    {
        id         => '89b378e6-e3f6-49f1-90f3-6a6d86881d86',
        dutch_code => '209',
        label      => 'Beuningen'
    },
    {
        id         => '8161d3d1-e2ed-4369-a4de-2cc00212046e',
        dutch_code => '213',
        label      => 'Brummen'
    },
    {
        id         => '3cc70992-0b36-4c64-bfdf-7321ef1a82f4',
        dutch_code => '214',
        label      => 'Buren'
    },
    {
        id         => 'db1299c8-acad-417a-bae7-a437d0470a19',
        dutch_code => '216',
        label      => 'Culemborg'
    },
    {
        id         => '822b4dd1-1699-4dcc-8624-92083280c9fb',
        dutch_code => '221',
        label      => 'Doesburg'
    },
    {
        id         => '13a6ef8f-ec49-40bd-8d56-5adb0d213f50',
        dutch_code => '222',
        label      => 'Doetinchem'
    },
    {
        id         => 'b132892d-97e2-44b1-b71c-963e3f102f25',
        dutch_code => '225',
        label      => 'Druten'
    },
    {
        id         => '875f6c86-4e38-4179-9d36-71dc68977d4a',
        dutch_code => '226',
        label      => 'Duiven'
    },
    {
        id         => '29b1cc3f-ef72-4afd-97b9-4faff745db3b',
        dutch_code => '228',
        label      => 'Ede'
    },
    {
        id         => '66e73e10-de81-4b6b-a527-0fd651644443',
        dutch_code => '230',
        label      => 'Elburg'
    },
    {
        id         => '23b7a4bb-21d4-4d58-b004-4a67fa8ed167',
        dutch_code => '232',
        label      => 'Epe'
    },
    {
        id         => 'f6b36933-7f2a-4179-95fc-3066ae3b958f',
        dutch_code => '233',
        label      => 'Ermelo'
    },
    {
        id         => '6a40ec8b-f64e-4d36-bf24-73d8a9b8b6e9',
        dutch_code => '236',
        label      => 'Geldermalsen'
    },
    {
        id         => 'abe54589-e9a4-465f-b9aa-d9e9d7891abe',
        dutch_code => '241',
        label      => 'Groesbeek',
        historical => \1,
    },
    {
        id         => '946ee42b-aa6d-4e7e-991b-2b16275f300d',
        dutch_code => '243',
        label      => 'Harderwijk'
    },
    {
        id         => '2b731ec5-e97c-4712-847b-ec575139eef8',
        dutch_code => '244',
        label      => 'Hattem'
    },
    {
        id         => '444bb582-4426-4020-8bb9-9326cd49af12',
        dutch_code => '246',
        label      => 'Heerde'
    },
    {
        id         => 'f7d01400-f3af-4bd3-996a-225a426ec29e',
        dutch_code => '252',
        label      => 'Heumen'
    },
    {
        id         => '6ecbd429-52ca-4654-a6be-a5ec18c6ba3c',
        dutch_code => '262',
        label      => 'Lochem'
    },
    {
        id         => 'b3f5e4e1-830b-4d7f-a079-e32e436802ba',
        dutch_code => '263',
        label      => 'Maasdriel'
    },
    {
        id         => 'e6feb3e1-8e82-4ac4-8756-81d6818d4451',
        dutch_code => '265',
        label      => 'Millingen aan de Rijn',
        historical => \1,
    },
    {
        id         => '973ad63a-0d66-495f-b398-9f65e80b33e2',
        dutch_code => '267',
        label      => 'Nijkerk'
    },
    {
        id         => '26f44294-2301-4521-9bc5-e1f1e4072bdb',
        dutch_code => '268',
        label      => 'Nijmegen'
    },
    {
        id         => 'fa1e9d61-804c-459d-8e45-5b6fadbe75c5',
        dutch_code => '269',
        label      => 'Oldebroek'
    },
    {
        id         => 'e4e1e25c-bc56-4d48-aa56-f37e5b1c467a',
        dutch_code => '273',
        label      => 'Putten'
    },
    {
        id         => '28f18caf-8aa1-4aa9-a097-dc88f3e51e0b',
        dutch_code => '274',
        label      => 'Renkum'
    },
    {
        id         => 'a9820528-da10-4cc1-bc5a-bfd5ad5cbeac',
        dutch_code => '275',
        label      => 'Rheden'
    },
    {
        id         => '3e509dd2-5d9a-4be7-b94c-2f0bcc309880',
        dutch_code => '277',
        label      => 'Rozendaal'
    },
    {
        id         => '901aad79-8048-4c3b-85f8-b4524483e59f',
        dutch_code => '279',
        label      => 'Scherpenzeel'
    },
    {
        id         => '2928c71f-0e67-488f-b052-72b2912dfe9c',
        dutch_code => '281',
        label      => 'Tiel'
    },
    {
        id         => 'c21f6be1-944e-4ded-aae7-5804ec748915',
        dutch_code => '282',
        label      => 'Ubbergen',
        historical => \1,
    },
    {
        id         => '0d265d53-fc98-4a6e-8584-d3d07d0f030e',
        dutch_code => '285',
        label      => 'Voorst'
    },
    {
        id         => 'e1496898-f919-4701-be2d-d67996741f67',
        dutch_code => '289',
        label      => 'Wageningen'
    },
    {
        id         => '3b6704ac-fcd5-4c40-842b-f99665a4662b',
        dutch_code => '293',
        label      => 'Westervoort'
    },
    {
        id         => 'cb11f22f-d954-4537-b7c9-88603bd4f82d',
        dutch_code => '294',
        label      => 'Winterswijk'
    },
    {
        id         => '0ad26d1d-3711-43fa-9134-38daeae48466',
        dutch_code => '296',
        label      => 'Wijchen'
    },
    {
        id         => '0f83e48e-ff21-421d-93cc-86da4e243555',
        dutch_code => '297',
        label      => 'Zaltbommel'
    },
    {
        id         => '313a3d0e-fde5-4c8c-905c-9986a746c3e6',
        dutch_code => '299',
        label      => 'Zevenaar'
    },
    {
        id         => '47c25787-89b3-41c0-a52e-103b48a874a9',
        dutch_code => '301',
        label      => 'Zutphen'
    },
    {
        id         => 'b439b4f8-7a22-4bc2-a914-3ef5c999499c',
        dutch_code => '302',
        label      => 'Nunspeet'
    },
    {
        id         => 'c3272eb6-1479-4b8f-af09-948e301f68b9',
        dutch_code => '303',
        label      => 'Dronten'
    },
    {
        id         => '3da5f7a4-27be-4219-be89-2ff428cb6bb6',
        dutch_code => '304',
        label      => 'Neerijnen'
    },
    {
        id         => '17a7cf54-fa09-4790-9290-a51ae34408c9',
        dutch_code => '305',
        label      => 'Abcoude',
        historical => \1,
    },
    {
        id         => 'e01f24c7-1eed-4fd2-b656-032bfac31f57',
        dutch_code => '307',
        label      => 'Amersfoort'
    },
    {
        id         => '8b3b81aa-7aa9-4bdb-ab1b-cdf380a9da6c',
        dutch_code => '308',
        label      => 'Baarn'
    },
    {
        id         => '51deb750-89de-4aba-9e59-bd7d45f1c254',
        dutch_code => '310',
        label      => 'De Bilt'
    },
    {
        id         => 'b974392a-b026-4753-b27c-5831bb77ab7c',
        dutch_code => '311',
        label      => 'Breukelen',
        historical => \1,
    },
    {
        id         => 'ae518c9f-4451-4781-b78f-3f3b6e822846',
        dutch_code => '312',
        label      => 'Bunnik'
    },
    {
        id         => '8cc3d752-76a6-4580-9d8c-50a9a2c31664',
        dutch_code => '313',
        label      => 'Bunschoten'
    },
    {
        id         => '6537421b-aca9-44c5-b158-b3385aaf6673',
        dutch_code => '317',
        label      => 'Eemnes'
    },
    {
        id         => 'f51f03ea-341a-447b-af55-e54cd23e5d7d',
        dutch_code => '321',
        label      => 'Houten'
    },
    {
        id         => '9b48c7e4-b3e2-4bd6-832f-b1c6d248fdfa',
        dutch_code => '327',
        label      => 'Leusden'
    },
    {
        id         => '87cc0937-5313-4183-86d1-0066c9679510',
        dutch_code => '329',
        label      => 'Loenen',
        historical => \1,
    },
    {
        id         => '8f7ee7e3-bbcd-42f8-88d4-08c4c09a0d1f',
        dutch_code => '331',
        label      => 'Lopik'
    },
    {
        id         => '7911eb6f-5bd9-4eb8-beb4-17bda9cd20d3',
        dutch_code => '333',
        label      => 'Maarssen',
        historical => \1,
    },
    {
        id         => 'e3d13bc1-d593-44b5-8cb3-ad6bc85e7d79',
        dutch_code => '335',
        label      => 'Montfoort'
    },
    {
        id         => '3087a42a-c236-43ae-bab2-f7ca216ed815',
        dutch_code => '339',
        label      => 'Renswoude'
    },
    {
        id         => '12d9210e-82c9-475b-af79-ab2b3ef92bcd',
        dutch_code => '340',
        label      => 'Rhenen'
    },
    {
        id         => '2a943b87-5bf3-4a4e-8b8a-4eb56bb7a90d',
        dutch_code => '342',
        label      => 'Soest'
    },
    {
        id         => '0425bb80-d193-4082-8b6e-9ff954a33e50',
        dutch_code => '344',
        label      => 'Utrecht'
    },
    {
        id         => '6b01d2ef-3316-44c9-8479-022a73ac366c',
        dutch_code => '345',
        label      => 'Veenendaal'
    },
    {
        id         => 'ead6ed60-3d18-45cc-914d-61c2c9203ff4',
        dutch_code => '351',
        label      => 'Woudenberg'
    },
    {
        id         => 'c1c955e1-0f90-448c-a89c-fad73d060ca6',
        dutch_code => '352',
        label      => 'Wijk bij Duurstede'
    },
    {
        id         => '2c55c36a-0785-4f3c-966f-a804df513d8d',
        dutch_code => '353',
        label      => 'IJsselstein'
    },
    {
        id         => '8a5ac601-2c81-417f-b801-0591ee57b662',
        dutch_code => '355',
        label      => 'Zeist'
    },
    {
        id         => '8e536667-446c-4ecc-bcda-00d5a98fd5cf',
        dutch_code => '356',
        label      => 'Nieuwegein'
    },
    {
        id         => '0b8187cf-7a81-4b10-98f7-f79d42f01053',
        dutch_code => '358',
        label      => 'Aalsmeer'
    },
    {
        id         => '698befda-698a-478b-98c3-e81a00db5b8c',
        dutch_code => '361',
        label      => 'Alkmaar'
    },
    {
        id         => 'ddd0ce3c-67ab-4665-b7ab-b2eab9fd0925',
        dutch_code => '362',
        label      => 'Amstelveen'
    },
    {
        id         => '0ad5aae2-a609-4a7e-b87e-3b44c7f2c663',
        dutch_code => '363',
        label      => 'Amsterdam'
    },
    {
        id         => '35dce8cd-04dc-48e8-8982-06dcd8398609',
        dutch_code => '364',
        label      => 'Andijk',
        historical => \1,
    },
    {
        id         => '151a0d57-a7ce-4e70-8448-463ffe4c0690',
        dutch_code => '365',
        label      => 'Graft-De Rijp',
        historical => \1,
    },
    {
        id         => 'f757570f-3ab0-4b78-bb1e-53e58dae3d60',
        dutch_code => '366',
        label      => 'Anna Paulowna',
        historical => \1,
    },
    {
        id         => '6c465ac6-55bd-4b9d-93f7-36a9e5e578de',
        dutch_code => '370',
        label      => 'Beemster'
    },
    {
        id         => '846067e9-05b5-4136-a8cf-2b2c8a0163a9',
        dutch_code => '373',
        label      => 'Bergen (NH.)'
    },
    {
        id         => '78446ef9-8079-4c4c-83db-8a7cd872fcf5',
        dutch_code => '375',
        label      => 'Beverwijk'
    },
    {
        id         => '723ed6d4-2f1b-4a42-bc73-67ccbb1ecce6',
        dutch_code => '376',
        label      => 'Blaricum'
    },
    {
        id         => 'c11989a3-e8ef-4472-9376-7d08e680234e',
        dutch_code => '377',
        label      => 'Bloemendaal'
    },
    {
        id         => '64c95133-d47b-47d0-b44a-645f01437331',
        dutch_code => '381',
        label      => 'Bussum',
        historical => \1,
    },
    {
        id         => 'c56c2510-6865-40b7-bfff-fe592a538f36',
        dutch_code => '383',
        label      => 'Castricum'
    },
    {
        id         => 'a9e2bf1a-2421-4c2d-b6cf-88903154851d',
        dutch_code => '384',
        label      => 'Diemen'
    },
    {
        id         => '08eb5815-d023-4733-84dc-0111c2ab42cb',
        dutch_code => '385',
        label      => 'Edam-Volendam'
    },
    {
        id         => 'e2eb572e-0cdd-47fc-8a66-34ca76157b41',
        dutch_code => '388',
        label      => 'Enkhuizen'
    },
    {
        id         => 'c834a1a8-6d2a-46f1-be2a-988eb80ca8ea',
        dutch_code => '392',
        label      => 'Haarlem'
    },
    {
        id         => 'b51e3906-2c7f-4e10-9487-db2e6fb1e3b2',
        dutch_code => '393',
        label      => 'Haarlemmerliede en Spaarnwoude'
    },
    {
        id         => '68d82b04-5159-4f81-a5be-2083475e0cc4',
        dutch_code => '394',
        label      => 'Haarlemmermeer'
    },
    {
        id         => 'd97336c4-a49f-4183-894b-da5edfd60c91',
        dutch_code => '395',
        label      => 'Harenkarspel',
        historical => \1,
    },
    {
        id         => 'dfccd608-bc72-4b43-8d6f-dfe53c5fee81',
        dutch_code => '396',
        label      => 'Heemskerk'
    },
    {
        id         => '25441e7f-b63b-43ea-aa2c-8157c1edbb3a',
        dutch_code => '397',
        label      => 'Heemstede'
    },
    {
        id         => 'f731c7d8-ff0d-4551-8851-ada4399afc8c',
        dutch_code => '398',
        label      => 'Heerhugowaard'
    },
    {
        id         => '587e1440-1c83-43d5-8107-71fa1ceacc22',
        dutch_code => '399',
        label      => 'Heiloo'
    },
    {
        id         => '29a8d264-009c-443f-b431-d74c12a08fc9',
        dutch_code => '400',
        label      => 'Den Helder'
    },
    {
        id         => '0219a31e-b399-42a7-ba4f-7734da2edb05',
        dutch_code => '402',
        label      => 'Hilversum'
    },
    {
        id         => 'e87c513b-0ed2-492c-afd2-e3407a117424',
        dutch_code => '405',
        label      => 'Hoorn'
    },
    {
        id         => '74384372-e49d-4459-99a6-830a7a5776c0',
        dutch_code => '406',
        label      => 'Huizen'
    },
    {
        id         => '25b6f43c-1685-406c-be0b-bd06cee583c5',
        dutch_code => '412',
        label      => 'Niedorp',
        historical => \1,
    },
    {
        id         => 'd091eb7e-b1e9-464f-8633-c4fa5e1032a2',
        dutch_code => '415',
        label      => 'Landsmeer'
    },
    {
        id         => 'a7038427-2adb-4f04-9c52-99c353197b27',
        dutch_code => '416',
        label      => 'Langedijk'
    },
    {
        id         => '8e9c9202-84cc-45f0-8eb3-11577152cdd6',
        dutch_code => '417',
        label      => 'Laren'
    },
    {
        id         => 'a75de19b-fb17-4d9c-9e30-7425bc4335f0',
        dutch_code => '420',
        label      => 'Medemblik'
    },
    {
        id         => 'c7c3f468-1296-4721-9d70-ac64d35188c3',
        dutch_code => '424',
        label      => 'Muiden',
        historical => \1,
    },
    {
        id         => 'ff14e4dd-610e-4c37-8199-28a0772e4f79',
        dutch_code => '425',
        label      => 'Naarden',
        historical => \1,
    },
    {
        id         => 'afbd1ec8-4087-47ca-bc97-84aef54a7a13',
        dutch_code => '431',
        label      => 'Oostzaan'
    },
    {
        id         => 'f42e388e-dfcf-4de6-bc4f-094f67f6d734',
        dutch_code => '432',
        label      => 'Opmeer'
    },
    {
        id         => 'e379661e-0c9b-45e6-aefa-52d334a25b75',
        dutch_code => '437',
        label      => 'Ouder-Amstel'
    },
    {
        id         => '8aef286a-7015-46ab-a7e7-db96435698c5',
        dutch_code => '439',
        label      => 'Purmerend'
    },
    {
        id         => 'e8c4aaf9-d2f1-40f5-948b-72d9eca1d905',
        dutch_code => '441',
        label      => 'Schagen'
    },
    {
        id         => 'a837b704-0913-4fb0-847f-ec998f362b12',
        dutch_code => '448',
        label      => 'Texel'
    },
    {
        id         => '233ca9e5-e17e-46fd-bbe3-6dce90592616',
        dutch_code => '450',
        label      => 'Uitgeest'
    },
    {
        id         => 'cc3a5dc6-c937-4c74-a0e0-387842fdc6e6',
        dutch_code => '451',
        label      => 'Uithoorn'
    },
    {
        id         => '9070e114-e08b-417c-885b-8b2599f32436',
        dutch_code => '453',
        label      => 'Velsen'
    },
    {
        id         => '6750f363-0962-4d08-9486-fb813aa458fc',
        dutch_code => '457',
        label      => 'Weesp'
    },
    {
        id         => '180ea3e0-4986-46ba-bafc-507e999c799c',
        dutch_code => '458',
        label      => 'Schermer',
        historical => \1,
    },
    {
        id         => '955f8f21-aad8-42ff-8891-f8423f5042a3',
        dutch_code => '459',
        label      => 'Wervershoof',
        historical => \1,
    },
    {
        id         => 'aeb5767e-659b-4160-b07d-d33f3e214355',
        dutch_code => '462',
        label      => 'Wieringen',
        historical => \1,
    },
    {
        id         => 'a3f4be3f-5622-4e10-b150-798ee0b5391a',
        dutch_code => '463',
        label      => 'Wieringermeer',
        historical => \1,
    },
    {
        id         => 'abfdeaf0-9b7a-40f4-9403-5502aed0565e',
        dutch_code => '473',
        label      => 'Zandvoort'
    },
    {
        id         => 'ce0e7fde-db94-401d-be43-42cc2a41bc3f',
        dutch_code => '476',
        label      => 'Zijpe',
        historical => \1,
    },
    {
        id         => 'd3c769cd-27f7-46d9-8abf-58107f93b1f2',
        dutch_code => '478',
        label      => 'Zeevang',
        historical => \1,
    },
    {
        id         => '2e4c23f0-3328-4214-930f-f2ac66db911a',
        dutch_code => '479',
        label      => 'Zaanstad'
    },
    {
        id         => '6ed63920-6a9f-46b4-bafd-0925480ac923',
        dutch_code => '482',
        label      => 'Alblasserdam'
    },
    {
        id         => 'a46ef81d-38b8-459d-902e-57187034441b',
        dutch_code => '484',
        label      => 'Alphen aan den Rijn'
    },
    {
        id         => '9a4cef81-e701-4587-9c54-bd5e4387a68e',
        dutch_code => '489',
        label      => 'Barendrecht'
    },
    {
        id         => 'f2e89f65-8a50-4587-a7b4-c62464e48d41',
        dutch_code => '491',
        label      => 'Bergambacht',
        historical => \1,
    },
    {
        id         => '8fe05876-a042-4973-b6f1-9ef7d1541cd6',
        dutch_code => '497',
        label      => 'Bodegraven',
        historical => \1,
    },
    {
        id         => '9fe8c6a0-2f2c-4526-b59b-1d7985024f61',
        dutch_code => '498',
        label      => 'Drechterland'
    },
    {
        id         => 'fb4dcf45-9279-4060-b34b-49d1cb21650d',
        dutch_code => '499',
        label      => 'Boskoop',
        historical => \1,
    },
    {
        id         => '7421af3c-880f-4418-8571-d3f39cb93c97',
        dutch_code => '501',
        label      => 'Brielle'
    },
    {
        id         => 'ce03f8d7-8c60-48a8-a67a-c278916e576e',
        dutch_code => '502',
        label      => 'Capelle aan den IJssel'
    },
    {
        id         => 'c2def0fb-f825-481a-8a02-086d9adda546',
        dutch_code => '503',
        label      => 'Delft'
    },
    {
        id         => 'ba6769eb-8379-4e4f-8210-ae08aa2628ec',
        dutch_code => '504',
        label      => 'Dirksland',
        historical => \1,
    },
    {
        id         => '6e06cf5b-14ad-4915-bb2b-924affc13685',
        dutch_code => '505',
        label      => 'Dordrecht'
    },
    {
        id         => '2f8eae40-a9f6-4cd5-8956-6a31db2a238a',
        dutch_code => '511',
        label      => 'Goedereede',
        historical => \1,
    },
    {
        id         => 'cafd931a-5200-44b2-9223-8f53908e80bb',
        dutch_code => '512',
        label      => 'Gorinchem'
    },
    {
        id         => 'cd5085d8-f50a-41f4-ad8e-65ea86762a39',
        dutch_code => '513',
        label      => 'Gouda'
    },
    {
        id               => '0c48f9ea-a528-4c61-a76e-306653334565',
        dutch_code       => '518',
        label            => "'s-Gravenhage",
        alternative_name => "Den Haag",
    },
    {
        id         => '8078a749-57bf-4f7d-9753-b60b2c80f981',
        dutch_code => '523',
        label      => 'Hardinxveld-Giessendam'
    },
    {
        id         => 'c6045df2-b3a7-4fef-a40f-70b7330b4939',
        dutch_code => '530',
        label      => 'Hellevoetsluis'
    },
    {
        id         => 'ca5ccf42-70a7-45cc-9ab1-49cd9abcea60',
        dutch_code => '531',
        label      => 'Hendrik-Ido-Ambacht',
        historical => \1,
    },
    {
        id         => '62f93422-b2dc-46de-bab0-2556e6ca7188',
        dutch_code => '532',
        label      => 'Stede Broec'
    },
    {
        id         => '3a91ff81-220f-4d46-97e1-996fac7e9c16',
        dutch_code => '534',
        label      => 'Hillegom'
    },
    {
        id         => '65163c78-06f2-4fea-a3a5-38e6db2f6ff3',
        dutch_code => '537',
        label      => 'Katwijk'
    },
    {
        id         => '6b05ef47-2b79-4d81-a0e1-c7a204957db0',
        dutch_code => '542',
        label      => 'Krimpen aan den IJssel'
    },
    {
        id         => '77f858eb-a6ca-4e15-b585-aa1655259b41',
        dutch_code => '545',
        label      => 'Leerdam'
    },
    {
        id         => 'b3b98f00-b132-4848-8ff9-20f24957f3e6',
        dutch_code => '546',
        label      => 'Leiden'
    },
    {
        id         => '5f49a31d-0682-4dc1-aecd-d8e64e4da3ef',
        dutch_code => '547',
        label      => 'Leiderdorp'
    },
    {
        id         => 'fdd42de4-e896-483d-b52e-d1297633b409',
        dutch_code => '553',
        label      => 'Lisse'
    },
    {
        id         => '25cd9bf1-1f9a-4a9b-822d-b4f8f10de74d',
        dutch_code => '556',
        label      => 'Maassluis'
    },
    {
        id         => '8fc7028b-c1e2-4cda-a01f-c3784b593010',
        dutch_code => '559',
        label      => 'Middelharnis',
        historical => \1,
    },
    {
        id         => 'ecd5191b-be80-4a93-944a-832c4ce3856f',
        dutch_code => '563',
        label      => 'Moordrecht',
        historical => \1,
    },
    {
        id         => '4063ede9-fa7c-4cc4-8bae-3d443f7e86be',
        dutch_code => '567',
        label      => 'Nieuwerkerk aan den IJssel',
        historical => \1,
    },
    {
        id         => 'd4cef9f9-7641-434c-865f-2041405b6177',
        dutch_code => '568',
        label      => 'Bernisse',
        historical => \1,
    },
    {
        id         => 'b3602387-443b-488e-921d-33920d7f2897',
        dutch_code => '569',
        label      => 'Nieuwkoop'
    },
    {
        id         => '3c68346f-7baf-480e-ae03-52ed9a824490',
        dutch_code => '571',
        label      => 'Nieuw-Lekkerland',
        historical => \1,
    },
    {
        id         => '9151ab4b-669b-4cca-9334-8b8fc10e8998',
        dutch_code => '575',
        label      => 'Noordwijk'
    },
    {
        id         => '65476ede-5654-43d7-b99f-dc9812b953d4',
        dutch_code => '576',
        label      => 'Noordwijkerhout'
    },
    {
        id         => '4fe3497d-de82-4d1b-b715-1e62cb9d63c4',
        dutch_code => '579',
        label      => 'Oegstgeest'
    },
    {
        id         => 'fb52517f-c21c-48f3-8031-9279ade94a79',
        dutch_code => '580',
        label      => 'Oostflakkee',
        historical => \1,
    },
    {
        id         => '007183f4-b7e8-49f0-a9e1-93c655d316d8',
        dutch_code => '584',
        label      => 'Oud-Beijerland'
    },
    {
        id         => 'fdb7b845-7d52-41b3-bde3-31c34ed6ff32',
        dutch_code => '585',
        label      => 'Binnenmaas'
    },
    {
        id         => '80209b34-9ba6-4b72-9e7a-e73cbe9fe211',
        dutch_code => '588',
        label      => 'Korendijk'
    },
    {
        id         => '593d1050-9b22-4933-b153-5912454f8a10',
        dutch_code => '589',
        label      => 'Oudewater'
    },
    {
        id         => 'f8996c38-6660-4ae6-b1b5-429f48b79159',
        dutch_code => '590',
        label      => 'Papendrecht'
    },
    {
        id         => 'b261c8d3-42c8-47be-8c20-6129706c8c14',
        dutch_code => '595',
        label      => 'Reeuwijk',
        historical => \1,
    },
    {
        id         => '77ad13d5-1e9e-491d-8fac-18e64ae0edd2',
        dutch_code => '597',
        label      => 'Ridderkerk'
    },
    {
        id         => '5cfba98e-69bf-4ac8-8c5f-194b935e7b0e',
        dutch_code => '599',
        label      => 'Rotterdam'
    },
    {
        id         => '009852f0-9e3e-4579-8ee0-d87a9551c8ed',
        dutch_code => '600',
        label      => 'Rozenburg',
        historical => \1,
    },
    {
        id         => '341fe6f2-f27a-4020-9bcb-e52858067909',
        dutch_code => '603',
        label      => 'Rijswijk'
    },
    {
        id         => 'd7d4bcd1-1f81-4907-9cf1-78205e7c41ff',
        dutch_code => '606',
        label      => 'Schiedam'
    },
    {
        id         => 'e1e0cb83-f905-436b-b831-3f175af0697f',
        dutch_code => '608',
        label      => 'Schoonhoven',
        historical => \1,
    },
    {
        id         => '8405e045-a005-4280-be9d-368816f7d705',
        dutch_code => '610',
        label      => 'Sliedrecht'
    },
    {
        id         => '9bb51188-f5f5-43f4-afed-f301c49a1bd9',
        dutch_code => '611',
        label      => 'Cromstrijen'
    },
    {
        id         => '8b16d64c-96c6-4987-a76b-23e2adc32694',
        dutch_code => '612',
        label      => 'Spijkenisse',
        historical => \1,
    },
    {
        id         => 'a9d775dc-9c30-4e67-80f9-c8bf3a37ecb6',
        dutch_code => '613',
        label      => 'Albrandswaard'
    },
    {
        id         => '415afef4-4f65-4ae1-8657-2706dcc6937f',
        dutch_code => '614',
        label      => 'Westvoorne'
    },
    {
        id         => '96e8dfba-b748-48e7-8b16-250fc50714bf',
        dutch_code => '617',
        label      => 'Strijen'
    },
    {
        id         => 'd62ad226-1b51-4941-b2a1-152a67c9f42b',
        dutch_code => '620',
        label      => 'Vianen'
    },
    {
        id         => '3e9c2b5d-c84a-4686-b174-15474460b953',
        dutch_code => '622',
        label      => 'Vlaardingen'
    },
    {
        id         => 'aa90d2bf-d7d1-450d-a561-ba5f3f6c7858',
        dutch_code => '623',
        label      => 'Vlist',
        historical => \1,
    },
    {
        id         => 'f5d9c20c-78b7-4f57-bb2f-aaa643b98cc5',
        dutch_code => '626',
        label      => 'Voorschoten'
    },
    {
        id         => 'd5772984-eaec-40cf-8919-4010f593e434',
        dutch_code => '627',
        label      => 'Waddinxveen'
    },
    {
        id         => '802e4785-6e0b-4d1f-b48b-930b08257dfc',
        dutch_code => '629',
        label      => 'Wassenaar'
    },
    {
        id         => 'bfdfbcc2-7740-4924-9fa8-04b4dc171bf2',
        dutch_code => '632',
        label      => 'Woerden'
    },
    {
        id         => '148f3522-3b65-492f-99a4-08d986a5221b',
        dutch_code => '637',
        label      => 'Zoetermeer'
    },
    {
        id         => '98d966ab-afd0-4ee1-b173-1f574b06a10b',
        dutch_code => '638',
        label      => 'Zoeterwoude'
    },
    {
        id         => 'b9671f2c-231c-48da-ac4e-bb92b2457810',
        dutch_code => '642',
        label      => 'Zwijndrecht'
    },
    {
        id         => 'a95c9a13-d2d1-48a3-877f-5a92e03fb8b0',
        dutch_code => '643',
        label      => 'Nederlek',
        historical => \1,
    },
    {
        id         => '4a4b2cfe-8dea-4cdf-a48d-fb3a612586c1',
        dutch_code => '644',
        label      => 'Ouderkerk',
        historical => \1,
    },
    {
        id         => '6a951aae-0027-43f5-a211-cc4013592ed8',
        dutch_code => '653',
        label      => 'Gaasterlân-Sleat',
        historical => \1,
    },
    {
        id         => '9824623a-f700-4319-aea5-c353655b23e3',
        dutch_code => '654',
        label      => 'Borsele'
    },
    {
        id         => '15592935-c639-4e53-b49f-e52bfd8fe3f4',
        dutch_code => '664',
        label      => 'Goes'
    },
    {
        id         => 'd99bbefd-d989-4b60-8ae8-87a2fc718f44',
        dutch_code => '668',
        label      => 'West Maas en Waal'
    },
    {
        id         => '3ade6e87-aab5-4418-8438-107cc160ce28',
        dutch_code => '677',
        label      => 'Hulst'
    },
    {
        id         => '3f74552d-e7b5-4ed0-9b4d-a255bdf2853e',
        dutch_code => '678',
        label      => 'Kapelle'
    },
    {
        id         => '6488d30f-4db4-4bb9-83b4-50180bfec315',
        dutch_code => '683',
        label      => 'Wymbritseradiel',
        historical => \1,
    },
    {
        id         => '80a55b1c-1dda-4565-b916-520d823cc64d',
        dutch_code => '687',
        label      => 'Middelburg'
    },
    {
        id         => '87da38ba-75f3-41de-bac2-321bae23316e',
        dutch_code => '689',
        label      => 'Giessenlanden'
    },
    {
        id         => 'cfc9b87c-4768-4a0c-bac1-c59eeb498248',
        dutch_code => '693',
        label      => 'Graafstroom',
        historical => \1,
    },
    {
        id         => '66b3556c-1787-410c-a6b2-f7aa6186b340',
        dutch_code => '694',
        label      => 'Liesveld',
        historical => \1,
    },
    {
        id         => 'd111d90b-c374-465e-b2ee-f314686b387b',
        dutch_code => '703',
        label      => 'Reimerswaal'
    },
    {
        id         => 'd422524d-72c5-4b2b-afb1-e85e433c5911',
        dutch_code => '707',
        label      => 'Zederik'
    },
    {
        id         => '819ed43e-e504-4647-9a52-c854ea3dd8b1',
        dutch_code => '710',
        label      => 'Wûnseradiel',
        historical => \1,
    },
    {
        id         => '6d2448c2-65df-481b-b69c-c8a42c6cc53f',
        dutch_code => '715',
        label      => 'Terneuzen'
    },
    {
        id         => 'e8184c8e-fcc9-4630-b11e-b787640a29cd',
        dutch_code => '716',
        label      => 'Tholen'
    },
    {
        id         => '6187d65b-e09c-4756-8f01-9c915bbd5518',
        dutch_code => '717',
        label      => 'Veere'
    },
    {
        id         => '9bea9e08-79db-40ac-86bb-e5e538ca9094',
        dutch_code => '718',
        label      => 'Vlissingen'
    },
    {
        id         => '57349338-26d8-4ec0-8243-26f1d7e1d422',
        dutch_code => '733',
        label      => 'Lingewaal'
    },
    {
        id         => 'd24f71e4-2955-4a2a-bd25-be21591c80dd',
        dutch_code => '736',
        label      => 'De Ronde Venen'
    },
    {
        id         => 'eea75c09-0fe8-417a-b66a-d98b4b005a78',
        dutch_code => '737',
        label      => 'Tytsjerksteradiel'
    },
    {
        id         => 'a9505342-0efe-480a-8935-8466eeb7c5ef',
        dutch_code => '738',
        label      => 'Aalburg'
    },
    {
        id         => 'ef45785b-0e93-49b9-9b10-21f3078f27c3',
        dutch_code => '743',
        label      => 'Asten'
    },
    {
        id         => '2df0fee1-d44b-4a48-9e53-bd236c8558a2',
        dutch_code => '744',
        label      => 'Baarle-Nassau'
    },
    {
        id         => '5d884613-3e08-460a-9ebf-09315e890817',
        dutch_code => '748',
        label      => 'Bergen op Zoom'
    },
    {
        id         => '9589e10b-2864-48ad-8f02-6b8585c88832',
        dutch_code => '753',
        label      => 'Best'
    },
    {
        id         => 'f757909a-4090-4cc6-b654-c7bcaf6be2be',
        dutch_code => '755',
        label      => 'Boekel'
    },
    {
        id         => 'ae90e90a-87dc-4c2b-bd33-cd848a08c9c3',
        dutch_code => '756',
        label      => 'Boxmeer'
    },
    {
        id         => 'fe1ffe66-7e82-4a27-bcce-7f5beaec1b13',
        dutch_code => '757',
        label      => 'Boxtel'
    },
    {
        id         => '2c29dd15-db81-4866-8a44-fea415ceb76f',
        dutch_code => '758',
        label      => 'Breda'
    },
    {
        id         => '2d3d9a83-1736-4b50-a172-cf900a39bbab',
        dutch_code => '762',
        label      => 'Deurne'
    },
    {
        id         => 'd0290a9f-f269-4322-8a42-bbfae06b0536',
        dutch_code => '765',
        label      => 'Pekela'
    },
    {
        id         => '50acfeb5-9f16-4de4-bf79-610b3570e2a5',
        dutch_code => '766',
        label      => 'Dongen'
    },
    {
        id         => '569f660a-169d-4a05-b257-c5209a3f65dd',
        dutch_code => '770',
        label      => 'Eersel'
    },
    {
        id         => 'a3efff87-36fb-4845-aeb4-a4eb81bf2158',
        dutch_code => '772',
        label      => 'Eindhoven'
    },
    {
        id         => '1655e5ac-9eb2-4f7e-8dce-60d372bf16e4',
        dutch_code => '777',
        label      => 'Etten-Leur'
    },
    {
        id         => 'dcd3a5d5-4aab-440f-b303-920fe885e4ff',
        dutch_code => '779',
        label      => 'Geertruidenberg'
    },
    {
        id         => '23ea6063-ef7f-4aa7-acfd-9f9706115a06',
        dutch_code => '784',
        label      => 'Gilze en Rijen'
    },
    {
        id         => '8bf37214-8fa4-40f2-874e-2a18cacf989d',
        dutch_code => '785',
        label      => 'Goirle'
    },
    {
        id         => 'fa6d7583-f21a-401d-980a-85c460b5f4ee',
        dutch_code => '786',
        label      => 'Grave'
    },
    {
        id         => '11208a44-9d46-4761-b721-a22b869c12d3',
        dutch_code => '788',
        label      => 'Haaren'
    },
    {
        id         => '6f611717-9d9e-442a-8ab6-53c410134c3f',
        dutch_code => '794',
        label      => 'Helmond'
    },
    {
        id               => '51f2d486-a443-4e55-b43a-09dc8f305633',
        dutch_code       => '796',
        label            => "'s-Hertogenbosch",
        alternative_name => "Den Bosch",
    },
    {
        id         => '0fb684b2-b729-4fdf-9fda-b8fc2c0abc22',
        dutch_code => '797',
        label      => 'Heusden'
    },
    {
        id         => 'df1f0d37-4cac-4e3e-96b9-6570e5efbf96',
        dutch_code => '798',
        label      => 'Hilvarenbeek'
    },
    {
        id         => '47beedaf-8132-48af-83f3-2f8301afdb1e',
        dutch_code => '808',
        label      => 'Lith',
        historical => \1,
    },
    {
        id         => '04fe984f-9629-400e-8e23-05f25f9e5402',
        dutch_code => '809',
        label      => 'Loon op Zand'
    },
    {
        id         => '790e66b5-8a8d-4934-b1ac-f3a8b48bf40c',
        dutch_code => '815',
        label      => 'Mill en Sint Hubert'
    },
    {
        id         => '4ddf4ac1-c6d3-477b-ae3d-7262a397c9a5',
        dutch_code => '820',
        label      => 'Nuenen, Gerwen en Nederwetten'
    },
    {
        id         => '31ba5cd6-53cd-45c4-8dd6-e7bb1e8286a9',
        dutch_code => '823',
        label      => 'Oirschot'
    },
    {
        id         => '7bdb09b4-4f8f-4043-a9cf-05cab828ba96',
        dutch_code => '824',
        label      => 'Oisterwijk'
    },
    {
        id         => '1165f0c4-c7fd-4627-9308-5c4fc55d0170',
        dutch_code => '826',
        label      => 'Oosterhout'
    },
    {
        id         => 'baf3a2d0-d78a-49dc-9b85-d4d7230b53ef',
        dutch_code => '828',
        label      => 'Oss'
    },
    {
        id         => 'e37ecc95-70c4-4a78-bb91-1605c689b19b',
        dutch_code => '840',
        label      => 'Rucphen'
    },
    {
        id         => 'e519e259-2200-49ac-8a7b-b8c7264b6d01',
        dutch_code => '844',
        label      => 'Schijndel',
        historical => \1,
    },
    {
        id         => 'bb896dee-15cb-4f5a-a15b-442b866a726e',
        dutch_code => '845',
        label      => 'Sint-Michielsgestel'
    },
    {
        id         => '59b5a32f-2495-4fee-aa68-fc9fa4602c0c',
        dutch_code => '846',
        label      => 'Sint-Oedenrode',
        historical => \1,
    },
    {
        id         => '2b767bf2-04cb-4894-9d5e-e8a43020e36c',
        dutch_code => '847',
        label      => 'Someren'
    },
    {
        id         => '8e6ebdf3-8525-4851-b90b-605be3be4959',
        dutch_code => '848',
        label      => 'Son en Breugel'
    },
    {
        id         => '7fdf92ad-1bed-43c6-a0f4-1aa987324132',
        dutch_code => '851',
        label      => 'Steenbergen'
    },
    {
        id         => 'd0789349-ee66-4608-8a5b-dfa47dd20ca3',
        dutch_code => '852',
        label      => 'Waterland'
    },
    {
        id         => 'b46f50a9-b3a2-4864-8b62-e857fa826f60',
        dutch_code => '855',
        label      => 'Tilburg'
    },
    {
        id         => '5f647cf2-85fa-4fb7-8f6c-a090784d8555',
        dutch_code => '856',
        label      => 'Uden'
    },
    {
        id         => '80bd5746-4499-4665-a9f0-a4c56318e825',
        dutch_code => '858',
        label      => 'Valkenswaard'
    },
    {
        id         => '6ba49c47-10cf-4737-837e-521913be92d3',
        dutch_code => '860',
        label      => 'Veghel',
        historical => \1,
    },
    {
        id         => 'c3503a95-8be2-4dae-bc14-277b212c5d87',
        dutch_code => '861',
        label      => 'Veldhoven'
    },
    {
        id         => 'd0684e7d-c051-4f90-ad63-e288a826160a',
        dutch_code => '865',
        label      => 'Vught'
    },
    {
        id         => '8515998e-a5bc-4faa-bf42-215ca6c1ceec',
        dutch_code => '866',
        label      => 'Waalre'
    },
    {
        id         => '4040f3dc-0b45-4025-a3a2-b5678f48a7b9',
        dutch_code => '867',
        label      => 'Waalwijk'
    },
    {
        id         => 'e8a32fd6-686c-4461-85e2-94672f25c2e5',
        dutch_code => '870',
        label      => 'Werkendam'
    },
    {
        id         => '3dae1ad0-366a-4960-b951-f2077d9222a9',
        dutch_code => '873',
        label      => 'Woensdrecht'
    },
    {
        id         => '5a0cea87-394e-41a5-8d52-0cecba1f787d',
        dutch_code => '874',
        label      => 'Woudrichem'
    },
    {
        id         => 'bc5fd894-0e45-442a-a331-fff56ae5b727',
        dutch_code => '879',
        label      => 'Zundert'
    },
    {
        id         => 'b7023a50-19ea-46ca-a198-9cb29b104d5a',
        dutch_code => '880',
        label      => 'Wormerland'
    },
    {
        id         => '0b9a1010-ebdb-4a88-a912-aae361fc1c31',
        dutch_code => '881',
        label      => 'Onderbanken'
    },
    {
        id         => '0a385dbc-e22a-4c1b-9d51-ffdb28325c5a',
        dutch_code => '882',
        label      => 'Landgraaf'
    },
    {
        id         => 'f1563a97-c35f-4f99-ac5d-edd7d9152dc5',
        dutch_code => '885',
        label      => 'Arcen en Velden'
    },
    {
        id         => 'f92adb54-e7f2-459a-95b3-a07f40deda2f',
        dutch_code => '888',
        label      => 'Beek'
    },
    {
        id         => '87b8bee2-3d21-412c-965c-684beb8dcb8f',
        dutch_code => '889',
        label      => 'Beesel'
    },
    {
        id         => 'b0b96a6d-28bc-432e-aea8-c4ba209b8432',
        dutch_code => '893',
        label      => 'Bergen (L.)'
    },
    {
        id         => '7052ca24-ca2f-4ff3-a899-b246080e6401',
        dutch_code => '899',
        label      => 'Brunssum'
    },
    {
        id         => 'ca65e142-fb41-4a45-9358-aa6df86581b2',
        dutch_code => '905',
        label      => 'Eijsden',
        historical => \1,
    },
    {
        id         => '4a42bcf5-cebe-4c7f-948b-e22d7ec48545',
        dutch_code => '907',
        label      => 'Gennep'
    },
    {
        id         => '385394d9-f003-472f-aa68-668336e5cb1b',
        dutch_code => '917',
        label      => 'Heerlen'
    },
    {
        id         => 'b68d92bf-06d9-47e6-a222-85e4b1cc503e',
        dutch_code => '918',
        label      => 'Helden',
        historical => \1,
    },
    {
        id         => '7adc4ec3-b3b4-4138-9aaf-e1984157be90',
        dutch_code => '928',
        label      => 'Kerkrade'
    },
    {
        id         => 'da4a8d06-f9cb-4409-aa19-ecfc5493c620',
        dutch_code => '929',
        label      => 'Kessel',
        historical => \1
    },
    {
        id         => 'eb511d2f-b386-4298-90cf-0ddf11df24d3',
        dutch_code => '934',
        label      => 'Maasbree',
        historical => \1,
    },
    {
        id         => 'c53f2ddf-c2e5-4b0a-ba8f-73efdce14836',
        dutch_code => '935',
        label      => 'Maastricht'
    },
    {
        id         => '6636f9de-1a82-48a4-984b-011cf807fdbb',
        dutch_code => '936',
        label      => 'Margraten',
        historical => \1,
    },
    {
        id         => '9e58ff19-7145-4b8e-85ed-185e7161e4c9',
        dutch_code => '938',
        label      => 'Meerssen'
    },
    {
        id         => '5f80bbf2-6a2c-4bff-b084-c8e22be843b4',
        dutch_code => '941',
        label      => 'Meijel',
        historical => \1,
    },
    {
        id         => '709b3ad6-18c5-4ad6-9073-207183b49532',
        dutch_code => '944',
        label      => 'Mook en Middelaar'
    },
    {
        id         => 'f26a2ba9-64a4-4b35-80c2-1a8f0cc2e325',
        dutch_code => '946',
        label      => 'Nederweert'
    },
    {
        id         => '5b260f6a-1ca3-480a-98ae-60cb63b2031f',
        dutch_code => '951',
        label      => 'Nuth'
    },
    {
        id         => '6b61d113-7b5c-46f5-9f9b-3027883c9fec',
        dutch_code => '957',
        label      => 'Roermond'
    },
    {
        id         => 'a22bfd75-ad84-425c-b0a4-afe83ccbf036',
        dutch_code => '962',
        label      => 'Schinnen'
    },
    {
        id         => '574e31de-3bec-474b-b4fa-e16e36beae45',
        dutch_code => '964',
        label      => 'Sevenum',
        historical => \1,
    },
    {
        id         => '02a718c6-ba48-425a-a401-e0141c2da6c9',
        dutch_code => '965',
        label      => 'Simpelveld'
    },
    {
        id         => '1259fc3d-dd12-4cac-aa59-a3c59b176db8',
        dutch_code => '971',
        label      => 'Stein'
    },
    {
        id         => '066b779b-567f-4f80-bd7a-e69d06cbdbcf',
        dutch_code => '981',
        label      => 'Vaals'
    },
    {
        id         => 'a366c12e-4d87-49c4-8f77-b97dd6a70fe4',
        dutch_code => '983',
        label      => 'Venlo'
    },
    {
        id         => '23022112-49e0-4750-9047-d23f973256c1',
        dutch_code => '984',
        label      => 'Venray'
    },
    {
        id         => 'd09dc573-04ac-4b80-8961-03a060f670b3',
        dutch_code => '986',
        label      => 'Voerendaal'
    },
    {
        id         => '3b493f4a-abfe-4f71-b279-f26a8eda791d',
        dutch_code => '988',
        label      => 'Weert'
    },
    {
        id         => 'bebbd6d2-c29b-476f-8073-c55c1d4aa4f4',
        dutch_code => '993',
        label      => 'Meerlo-Wanssum',
        historical => \1,
    },
    {
        id         => 'a6d3fa57-d49a-4f90-b847-513c7a07191c',
        dutch_code => '994',
        label      => 'Valkenburg aan de Geul'
    },
    {
        id         => '872c0f65-25a4-4a45-98b7-2bd6ca69e3e5',
        dutch_code => '995',
        label      => 'Lelystad'
    },
    {
        id         => '91a02378-7cee-42eb-9ff8-8e5241f0c4b8',
        dutch_code => '1507',
        label      => 'Horst aan de Maas'
    },
    {
        id         => 'e4e8df47-8f4b-4143-a278-142d0d62fa86',
        dutch_code => '1509',
        label      => 'Oude IJsselstreek'
    },
    {
        id         => '8d9ada86-f4c2-4ff6-8cb3-74cc46a0e98d',
        dutch_code => '1525',
        label      => 'Teylingen'
    },
    {
        id         => '749c54df-d2a3-4850-ae01-22f35c0fd249',
        dutch_code => '1581',
        label      => 'Utrechtse Heuvelrug'
    },
    {
        id         => 'c686fbf6-c777-44f7-b751-9ca0e6cb1589',
        dutch_code => '1586',
        label      => 'Oost Gelre'
    },
    {
        id         => 'd2eb46d7-73f4-417e-959a-85c1becb6550',
        dutch_code => '1598',
        label      => 'Koggenland'
    },
    {
        id         => '75a8bc60-4847-4071-bad7-6078f4b1e801',
        dutch_code => '1621',
        label      => 'Lansingerland'
    },
    {
        id         => '49a3c925-960c-4921-8817-96f4292e35b8',
        dutch_code => '1640',
        label      => 'Leudal'
    },
    {
        id         => 'b84d85b5-92c6-4f3a-858c-aaf79ccf2416',
        dutch_code => '1641',
        label      => 'Maasgouw'
    },
    {
        id         => 'a52b5eaf-4210-40c1-a098-6457e6cbb1d8',
        dutch_code => '1651',
        label      => 'Eemsmond'
    },
    {
        id         => 'ea91138b-681c-4cd9-9d7c-84c9d5c9c652',
        dutch_code => '1652',
        label      => 'Gemert-Bakel'
    },
    {
        id         => 'dad265ec-8beb-4b5f-b25a-17fe7a04c9f3',
        dutch_code => '1655',
        label      => 'Halderberge'
    },
    {
        id         => '8bdff903-ffba-438e-82e3-337feb22d183',
        dutch_code => '1658',
        label      => 'Heeze-Leende'
    },
    {
        id         => '78944c52-becd-4615-be71-0df792c77374',
        dutch_code => '1659',
        label      => 'Laarbeek'
    },
    {
        id         => '12148dba-0c0b-4ef7-8b5d-9f20989f26a5',
        dutch_code => '1661',
        label      => 'Reiderland',
        historical => \1,
    },
    {
        id         => 'e1483840-82a8-4dda-a66b-2a696f171181',
        dutch_code => '1663',
        label      => 'De Marne'
    },
    {
        id         => '42039b7e-b468-440f-9869-0704d8f72cc4',
        dutch_code => '1666',
        label      => 'Zevenhuizen-Moerkapelle',
        historical => \1,
    },
    {
        id         => '59bb51a2-7eb7-4e8f-8aa0-15e9099811c9',
        dutch_code => '1667',
        label      => 'Reusel-De Mierden'
    },
    {
        id         => '831d14b0-76b5-41d7-958b-97e2509b41dd',
        dutch_code => '1669',
        label      => 'Roerdalen'
    },
    {
        id         => 'e4bf5a34-9a61-4cc2-af9b-ba4f471e8cf4',
        dutch_code => '1671',
        label      => 'Maasdonk',
        historical => \1,
    },
    {
        id         => '2d3127ff-6f89-4db5-8fed-eb681b588010',
        dutch_code => '1672',
        label      => 'Rijnwoude',
        historical => \1,
    },
    {
        id         => 'a5a02f86-fb0e-4ede-ba7a-a7f2df13d56c',
        dutch_code => '1674',
        label      => 'Roosendaal'
    },
    {
        id         => '4ddb6441-4461-4b56-87e8-720f696c171d',
        dutch_code => '1676',
        label      => 'Schouwen-Duiveland'
    },
    {
        id         => 'b5f083d4-c9d9-4f18-a165-51bed73eb5b5',
        dutch_code => '1680',
        label      => 'Aa en Hunze'
    },
    {
        id         => 'f75be851-74c7-4ded-975d-7ab4e795d7f8',
        dutch_code => '1681',
        label      => 'Borger-Odoorn'
    },
    {
        id         => '8d2773f4-a3f3-424b-9789-54e0282abbfd',
        dutch_code => '1684',
        label      => 'Cuijk'
    },
    {
        id         => 'ce61e258-b319-4cf6-b892-8663a1870024',
        dutch_code => '1685',
        label      => 'Landerd'
    },
    {
        id         => 'a8b1c1e0-5782-4dbd-9a46-6f55c0b6a809',
        dutch_code => '1690',
        label      => 'De Wolden'
    },
    {
        id         => '70dc9728-7fd6-4f21-a00b-684da3990a40',
        dutch_code => '1695',
        label      => 'Noord-Beveland'
    },
    {
        id         => 'ae9fda03-87df-43d4-9fbb-8cce5341eaba',
        dutch_code => '1696',
        label      => 'Wijdemeren'
    },
    {
        id         => '2fcaf178-f421-4ce6-a5ba-f541c55c2a7c',
        dutch_code => '1699',
        label      => 'Noordenveld'
    },
    {
        id         => '012197bc-9dce-4d1b-a932-e7e24fc9dd58',
        dutch_code => '1700',
        label      => 'Twenterand'
    },
    {
        id         => 'b8568581-018b-48a1-9c30-871aa3b65cd0',
        dutch_code => '1701',
        label      => 'Westerveld'
    },
    {
        id         => 'bb53f60b-5ffa-4f10-b30b-e63ad02d82aa',
        dutch_code => '1702',
        label      => 'Sint Anthonis'
    },
    {
        id         => 'b5fc725f-7147-4324-b846-f7ade6ff0c60',
        dutch_code => '1705',
        label      => 'Lingewaard'
    },
    {
        id         => '42bd3c3d-055e-4487-8802-8d5cb8722cac',
        dutch_code => '1706',
        label      => 'Cranendonck'
    },
    {
        id         => 'b65b4285-c898-45f4-9cd8-b0e85f5100df',
        dutch_code => '1708',
        label      => 'Steenwijkerland'
    },
    {
        id         => '0167c404-ab3a-4239-97b5-5dd772098345',
        dutch_code => '1709',
        label      => 'Moerdijk'
    },
    {
        id         => 'c1c9a6a6-56cb-4686-9715-71b9d37c8c38',
        dutch_code => '1711',
        label      => 'Echt-Susteren'
    },
    {
        id         => '50b9ca62-ba02-47ad-b2d2-1fde068aaa32',
        dutch_code => '1714',
        label      => 'Sluis'
    },
    {
        id         => 'd4861114-e193-4a95-9d1b-028cd1540e9e',
        dutch_code => '1719',
        label      => 'Drimmelen'
    },
    {
        id         => '44158269-694d-4145-aca3-011ecd5bcc96',
        dutch_code => '1721',
        label      => 'Bernheze'
    },
    {
        id         => 'edff92ed-e242-4544-b99b-6906147a9fa9',
        dutch_code => '1722',
        label      => 'Ferwerderadiel'
    },
    {
        id         => '977708a2-d798-4974-b7dc-900a82d87f2c',
        dutch_code => '1723',
        label      => 'Alphen-Chaam'
    },
    {
        id         => 'c4dedfb1-c3c7-4427-a024-4def2091fd2d',
        dutch_code => '1724',
        label      => 'Bergeijk'
    },
    {
        id         => 'bd0831f5-bcd5-46ef-befc-7242e12753d6',
        dutch_code => '1728',
        label      => 'Bladel'
    },
    {
        id         => '22d2ad45-c29a-455c-908b-c83ba9db8310',
        dutch_code => '1729',
        label      => 'Gulpen-Wittem'
    },
    {
        id         => '302ef79e-41aa-406c-ab13-73712154a6fa',
        dutch_code => '1730',
        label      => 'Tynaarlo'
    },
    {
        id         => '8a89e60c-d874-458f-bd75-1175105ee5c8',
        dutch_code => '1731',
        label      => 'Midden-Drenthe'
    },
    {
        id         => 'debbe4de-3b3c-4c00-aa24-2963b0eb3cf3',
        dutch_code => '1734',
        label      => 'Overbetuwe'
    },
    {
        id         => '4f91209c-5974-47cb-88a5-4d93137604cc',
        dutch_code => '1735',
        label      => 'Hof van Twente'
    },
    {
        id         => '4fea7a0c-18af-4895-b2c7-d195a553b41d',
        dutch_code => '1740',
        label      => 'Neder-Betuwe'
    },
    {
        id         => '1e8efe2e-953b-427d-ae98-c5a8d58e9391',
        dutch_code => '1742',
        label      => 'Rijssen-Holten'
    },
    {
        id         => 'c96ef787-2263-4671-957e-586cf3db3db3',
        dutch_code => '1771',
        label      => 'Geldrop-Mierlo'
    },
    {
        id         => 'c7de1e8d-9eef-49c0-a1a3-5cd955fc5a8f',
        dutch_code => '1773',
        label      => 'Olst-Wijhe'
    },
    {
        id         => 'e5b51d51-b69e-4308-92b7-a8306813426a',
        dutch_code => '1774',
        label      => 'Dinkelland'
    },
    {
        id         => 'eb62c259-8700-4ed6-9d4c-53f452ad7a94',
        dutch_code => '1783',
        label      => 'Westland'
    },
    {
        id         => 'ef958472-5820-4fab-b56b-9e04abae240b',
        dutch_code => '1842',
        label      => 'Midden-Delfland'
    },
    {
        id         => 'cc3f1846-e3be-4ecc-ac85-efc7fdceb1f3',
        dutch_code => '1859',
        label      => 'Berkelland'
    },
    {
        id         => 'ef471ee9-7b22-4271-b148-1c52b4630ea8',
        dutch_code => '1876',
        label      => 'Bronckhorst'
    },
    {
        id         => '49f6715a-49cd-48c0-b3b6-73f2e9cbe327',
        dutch_code => '1883',
        label      => 'Sittard-Geleen'
    },
    {
        id         => 'dc331066-029a-4a64-be15-d513bd136876',
        dutch_code => '1884',
        label      => 'Kaag en Braassem'
    },
    {
        id         => 'f04e2d6f-e181-485d-af97-08edecca6b4d',
        dutch_code => '1891',
        label      => 'Dantumadiel'
    },
    {
        id         => '95e5f9a3-550c-4657-902c-f2f9f1fe6211',
        dutch_code => '1892',
        label      => 'Zuidplas'
    },
    {
        id         => 'de18c0b4-96bf-4abf-9fcc-a9358b0fc902',
        dutch_code => '1894',
        label      => 'Peel en Maas'
    },
    {
        id         => '380eedfc-59f0-4c37-b7e0-021b559ab0ae',
        dutch_code => '1895',
        label      => 'Oldambt'
    },
    {
        id         => '8506c310-b71b-45ed-be2f-48524593afb6',
        dutch_code => '1896',
        label      => 'Zwartewaterland'
    },
    {
        id         => '7cfdec38-b8cb-405f-9053-318fbce5ec12',
        dutch_code => '1900',
        label      => 'Súdwest-Fryslân'
    },
    {
        id         => '7fcc65f2-d98f-40e2-9d67-773abfcf9c12',
        dutch_code => '1901',
        label      => 'Bodegraven-Reeuwijk'
    },
    {
        id         => '56e7366d-8009-41cb-84d2-bf4bec7b988a',
        dutch_code => '1903',
        label      => 'Eijsden-Margraten'
    },
    {
        id         => 'a9e96779-5c02-497a-9d34-98e8df2939c4',
        dutch_code => '1904',
        label      => 'Stichtse Vecht'
    },
    {
        id         => 'e2934162-dd43-4294-80f7-6ac0e2c220c2',
        dutch_code => '1908',
        label      => 'Menameradiel',
        historical => \1
    },
    {
        id         => '57820002-5119-4bef-ab79-185fd022fad2',
        dutch_code => '1911',
        label      => 'Hollands Kroon'
    },
    {
        id         => '1ae14241-893f-4182-bbfa-7796f1011fca',
        dutch_code => '1916',
        label      => 'Leidschendam-Voorburg'
    },
    {
        id         => '6dc42793-60e2-43ac-bcdc-1f737c2087b9',
        dutch_code => '1924',
        label      => 'Goeree-Overflakkee'
    },
    {
        id         => '27469b04-c6f4-466e-a1ee-00982788e741',
        dutch_code => '1926',
        label      => 'Pijnacker-Nootdorp'
    },
    {
        id         => '7f19a8ad-f86c-4752-9df7-e545b354781a',
        dutch_code => '1927',
        label      => 'Molenwaard'
    },
    {
        id         => '3a7e8a5b-dad1-465a-9a1c-1be6a87572a5',
        dutch_code => '1930',
        label      => 'Nissewaard'
    },
    {
        id         => '06547322-c619-4d6e-8c3c-5bb41dd75107',
        dutch_code => '1931',
        label      => 'Krimpenerwaard'
    },
    {
        id         => '3ac6da20-2b5f-4698-b63a-811044478250',
        dutch_code => '1940',
        label      => 'De Fryske Marren'
    },
    {
        id         => '3726385e-6a5b-49de-ac6e-e2692e88c2f5',
        dutch_code => '1942',
        label      => 'Gooise Meren'
    },
    {
        id         => 'e857f0f5-c464-45e3-b448-4bb088b2f22d',
        dutch_code => '1945',
        label      => 'Berg en Dal'
    },
    {
        id         => '3a985cae-26c4-45d1-98ec-6910ebb310b4',
        dutch_code => '1948',
        label      => 'Meierijstad',
    },
    {
        id         => '54f983ea-c843-4491-a10e-e4f1de353271',
        dutch_code => '1949',
        label      => 'Waadhoeke'
    },
    {
        id         => '06ad4cd3-b422-44ab-8142-4f8d5beeadf8',
        dutch_code => '1950',
        label      => 'Westerwolde'
    },
    {
        id         => 'd2535d71-2571-4279-b06c-1046a7b9c0e8',
        dutch_code => '1952',
        label      => 'Midden-Groningen'
    },
    {
        id         => 'f25fe4c5-556f-443f-97ab-a541743367b6',
        dutch_code => '1955',
        label      => 'Montferland'
    },
    {
        id         => 'e055230a-8a06-4e7c-9f58-0594b3d38fe1',
        dutch_code => '1987',
        label      => 'Menterwolde',
        historical => \1
    },
];

=head2 ZAAKSYSTEEM_CONFIG_DEFINITIONS

Defines an C<ArrayRef> of C<config/definition> objects.

=cut

use constant ZAAKSYSTEEM_CONFIG_DEFINITIONS => [
    {
        id               => '8c3f8850-435d-4b64-8489-4285cdeedf7e',
        config_item_name => 'allocation_notification_template_id',
        value_type       => {
            parent_type_name => 'object_ref',
            options =>
                { object_type_name => 'email_template', type => 'legacy_id' }
        },
        value_type_name => 'object_ref',
        label           => 'E-mail template',
        category_id     => '204dea67-2001-4011-91c6-63a6a023aa5e',
        description =>
            'Deze setting wijzigt het sjabloon dat gebruikt wordt om'
            . ' e-mail-notificaties te versturen bij een toewijzingswijziging'
            . ' op een zaak'
    },

    {
        id               => '287fd068-0999-4c44-a504-a9538f19a763',
        config_item_name => 'app_enabled_meeting',
        value_type_name  => 'boolean',
        category_id      => '04c34c81-d8ed-43a3-9e2b-a24abe697d37',
        label            => 'Vergader app actief',
        description      => '',
        mutable          => 0,
    },

    {
        id               => '2c3ed588-6770-41df-8c95-2987b9bd6c61',
        config_item_name => 'app_enabled_wordapp',
        value_type_name  => 'boolean',
        category_id      => '04c34c81-d8ed-43a3-9e2b-a24abe697d37',
        label            => 'Microsoft Office Word koppeling actief',
        description      => '',
        mutable          => 0,
        deprecated       => 1,
    },
    {
        id               => '82f270d2-498e-4ce9-adcd-cb23331daf69',
        config_item_name => 'case_distributor_group',
        value_type       => {
            parent_type_name => 'object_ref',
            options => { object_type_name => 'group', type => 'legacy_id' }
        },
        category_id => 'b92f5a19-c076-43c7-82f1-897c8950f216',
        label       => 'Zaakverdeler afdeling',
        description => 'Afdeling verantwoordelijk voor zaakverdeling',
    },
    {
        id               => '7362679d-b8f6-493b-bcb1-b8d9350b443e',
        config_item_name => 'case_distributor_role',
        value_type       => {
            parent_type_name => 'object_ref',
            options => { object_type_name => 'role', type => 'legacy_id' }
        },
        category_id => 'b92f5a19-c076-43c7-82f1-897c8950f216',
        label       => 'Zaakverdeler afdeling',
        description => 'Afdeling verantwoordelijk voor zaakverdeling',
    },

    {
        id               => '33e32331-e6d6-4031-85c7-59c7eb1163bc',
        config_item_name => 'customer_info_adres',
        value_type_name  => 'text',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'Vestigingsadres',
        description      => 'Het volledige adres van uw vestiging',
    },

    {
        id               => '239bbdee-0811-42c3-8cc2-10d20b8b1e30',
        config_item_name => 'customer_info_email',
        value_type_name  => 'email',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'E-mailadres',
        description      => 'Uw algemene e-mailadres',
    },
    {
        id               => '3499c848-0903-47aa-9d86-f3bc4190e7e5',
        config_item_name => 'customer_info_faxnummer',
        value_type_name  => 'text',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'Faxnummer',
        description      => 'Het faxnummer van uw vestiging',
    },
    {
        id               => '424ff3bc-bcf6-4772-872a-b9a93a2f3c34',
        config_item_name => 'customer_info_gemeente_id_url',
        value_type       => {
            parent_type_name => 'uri',
            options          => { allowed_protocols => [qw[http https]] }
        },
        value_type_name => 'uri',
        category_id     => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label           => 'URL voor het aanvragen van een bedrijven-ID',
        description =>
            'Deze URL wordt gebruikt voor het aanvragen van een bedrijven-ID,'
            . ' zoals bijvoorbeeld het mintlabID. Deze wordt steeds minder'
            . ' gebruikt vanwege eHerkenning',
    },
    {
        id               => '11fa98a7-4fd4-4dbc-a929-c2e6ab25f49b',
        config_item_name => 'customer_info_gemeente_portal',
        value_type       => {
            parent_type_name => 'uri',
            options          => { allowed_protocols => [qw[http https]] }
        },
        category_id => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label       => 'URL van uw producten en diensten pagina',
        description =>
            'De URL van uw producten en dienstenpagina, deze wordt gebruikt'
            . ' indien een klant een aanvraag annuleerd',
    },
    {
        id               => '4bd449ae-d82d-43ed-aa84-72fe01b7462d',
        config_item_name => 'customer_info_huisnummer',
        value_type_name  => 'text',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'Vestigingsadres huisnummer',
        description      => 'Het huisnummer van uw vestiging',
    },
    {
        id               => 'd81f8648-2a36-4ea0-814e-2af227a5da8a',
        config_item_name => 'customer_info_latitude',
        value_type       => {
            parent_type_name => 'number',
            options          => { type => 'real' }
        },
        category_id => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label       => 'Gemeentecentrum breedtegraad',
        description =>
            'De breedtegraad van uw gemeentecentrum. Voor gebruik van de'
            . ' kaartfunctionaliteiten.',
    },
    {
        id               => '165a4192-8c28-4e74-904b-2317b00e2e23',
        config_item_name => 'customer_info_longitude',
        value_type       => {
            parent_type_name => 'number',
            options          => { type => 'real' }
        },
        category_id => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label       => 'Gemeentecentrum lengtegraad',
        description =>
            'De lengtegraad van uw gemeentecentrum. Voor gebruik van de'
            . ' kaartfunctionaliteiten.',
    },
    {
        id               => '5f2fef2a-4833-4847-9b1a-236039700fb0',
        config_item_name => 'customer_info_naam',
        value_type_name  => 'text',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'Naam',
        description =>
            'De naam van uw organisatie. Deze naam is ook te zien in de URL'
            . ' balk.',
    },
    {
        id               => 'fadcf16c-4bbe-4157-9771-e156ca0ae205',
        config_item_name => 'customer_info_naam_kort',
        value_type_name  => 'text',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'Korte naam',
        description =>
            'Deze naam wordt ondermeer gebruikt voor e-mailfunctionaliteiten.',
    },
    {
        id               => '945bd84a-d3cc-412f-a801-e89787deaf29',
        config_item_name => 'customer_info_naam_lang',
        value_type_name  => 'text',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'Lange naam',
        description =>
            'Deze naam wordt ondermeer gebruikt indien er een zaakregistratie'
            . ' heeft plaatsgevonden.',
    },
    {
        id               => 'eece146f-97e3-4315-a4dd-e6751360059b',
        config_item_name => 'customer_info_postbus',
        value_type_name  => 'text',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'Postbusadres nummer',
        description      => 'Het postbusnummer van uw vestiging.',
    },
    {
        id               => '02ec24a4-b511-4596-ad22-1b2a381f26ee',
        config_item_name => 'customer_info_postbus_postcode',
        value_type_name  => 'text',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'Postbusadres postcode',
        description      => 'De postcode van uw postbus van uw vestiging.',
    },
    {
        id               => 'f3e9df2b-61cc-4d38-9feb-35dda8a5d22a',
        config_item_name => 'customer_info_postcode',
        value_type_name  => 'text',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'Vestigingsadres Postcode',
        description      => 'De postcode van uw vestiging.',
    },
    {
        id               => '2e35ebfd-e6e0-4124-8e4f-7c28cfffeefd',
        config_item_name => 'customer_info_straatnaam',
        value_type_name  => 'text',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'Vestigingsadres straatnaam',
        description      => '',
    },
    {
        id               => '4eea2794-28ac-4d24-a07e-e13d75528d1e',
        config_item_name => 'customer_info_telefoonnummer',
        value_type_name  => 'text',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'Telefoonnummer',
        description      => 'Het telefoonnummer van uw vestiging.',
    },
    {
        id               => 'e9cf0c02-f64f-44a2-b7d7-ac6004218084',
        config_item_name => 'customer_info_website',
        value_type       => {
            parent_type_name => 'uri',
            options          => { allowed_protocols => [qw[http https]] }
        },
        category_id => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label       => 'Website',
        description => 'De URL van uw website.',
    },
    {
        id               => '086512fa-4b43-48c6-94da-7c9d1ccc895e',
        config_item_name => 'customer_info_woonplaats',
        value_type_name  => 'text',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'Vestigingsadres woonplaats',
        description      => 'De vestigingsplaats van uw organisatie.',
    },
    {
        id               => 'a21cbc94-b624-4a6a-8e10-3fa73c6148c8',
        config_item_name => 'customer_info_zaak_email',
        value_type_name  => 'email',
        category_id      => '6e1dfd26-a508-46fc-bd96-95d277db3812',
        label            => 'Emailadres voor mailfunctionaliteiten',
        description =>
            'Het mailadres van voor uw zaakcorrespondentie. Deze optie wordt'
            . ' uitgefaseerd, gebruik de "Uitgaande mailkoppeling in het'
            . ' koppelingsoverzicht".',
    },
    {
        id               => 'e73874c6-e81a-49cb-9b87-05deb68e1118',
        config_item_name => 'custom_relation_roles',
        value_type_name  => 'text',
        label            => 'Extra rollen voor betrokkenen',
        category_id      => '99fe5c3e-9209-45ff-a4ce-b8785addfa5f',
        description =>
            'Extra namen voor rollen van betrokkenen (bovenop de ingebouwde'
            . ' NEN-lijst).',
        mvp => 1,
    },
    {
        id               => '1c7b45b7-f101-4b7e-8514-f412063cc0c8',
        config_item_name => 'disable_dashboard_customization',
        value_type_name  => 'boolean',
        category_id      => 'b92f5a19-c076-43c7-82f1-897c8950f216',
        label            => 'Dashboard is niet door gebruikers te wijzigen',
        description =>
            'Standaard is het mogelijk om dashboard-widgets te verplaatsen,'
            . ' toe te voegen en te verwijderen. Als deze instelling aan'
            . ' staat, is deze mogelijkheid er niet.',
    },
    {
        id               => '7561d210-b9be-4e29-9b65-c4f8a2de1fd4',
        config_item_name => 'document_intake_user',
        value_type_name  => 'text',
        category_id      => 'b92f5a19-c076-43c7-82f1-897c8950f216',
        label            => 'Document intake gebruiker',
        description =>
            'Dit is de virtuele gebruiker waarnaar documenten gemaild kunnen'
            . ' worden die niet direct aan een gebruiker toegewezen moeten of'
            . ' kunnen worden. Dit mag niet matchen met een echte user in LDAP!'
    },
    {
        id               => 'e3d6909a-134b-4317-b61f-1b7dca1569b5',
        config_item_name => 'enable_mintlab_id',
        value_type_name  => 'boolean',
        category_id      => '04c34c81-d8ed-43a3-9e2b-a24abe697d37',
        label            => 'BedrijvenID',
        description      => 'Sta inloggen met BedrijvenID toe',
        mutable          => 0,
        deprecated       => 1,
    },
    {
        id               => '42f9b366-e324-4b79-abcc-a5d699290da6',
        config_item_name => 'enable_stufzkn_simulator',
        value_type_name  => 'boolean',
        category_id      => '04c34c81-d8ed-43a3-9e2b-a24abe697d37',
        label            => 'Simulatie-tool voor StUF-ZKN beschikbaar',
        description =>
            'StUF-ZKN-simulator simuleert een ZSC, en kan gebruikt worden om'
            . 'handmatig StUF-ZKN berichten te sturen naar een StUF-ZKN ZS.',
        mutable    => 0,
        deprecated => 1,
    },
    {
        id               => '1acf71e1-aab1-4951-8e7a-db7cdaaf6a2b',
        config_item_name => 'feedback_email_template_id',
        value_type_name  => 'object_ref',
        value_type       => {
            parent_type_name => 'object_ref',
            options =>
                { object_type_name => 'email_template', type => 'legacy_id' }
        },
        category_id => '204dea67-2001-4011-91c6-63a6a023aa5e',
        label       => 'PIP-feedback e-mail template',
        description =>
            'Deze setting wijzigt het sjabloon dat gebruikt wordt bij het'
            . ' verzenden van feedback uit de PIP.',
    },
    {
        id               => '92236ee6-eb05-44f3-bd0a-5bca376749b4',
        config_item_name => 'files_locally_editable',
        value_type_name  => 'boolean',
        category_id      => '04c34c81-d8ed-43a3-9e2b-a24abe697d37',
        label            => 'Lokaal bewerken van documenten',
        description =>
            'Zet de functionaliteit voor het lokaal bewerken van documenten'
            . ' aan (Zaaksysteem document watcher)',
        mutable => 0,
    },
    {
        id               => '98a664ba-e652-4ac0-8bd5-a6962389c6c6',
        config_item_name => 'file_username_seperator',
        value_type       => {
            parent_type_name => 'text',
            options          => { style => 'line' }
        },
        value_type_name => 'text',
        category_id     => 'b92f5a19-c076-43c7-82f1-897c8950f216',
        label           => 'Bestandsnaam/gebruikersnaam scheidingsteken',
        description =>
            'Dit is het scheidingsteken waarmee de gebruiker van de'
            . ' bestandsnaam onderscheiden wordt. In het geval van'
            . ' "marco-test.jpg" kan de gebruiker "marco" onderscheiden'
            . ' worden wanneer het scheidingsteken op "-" ingesteld.',
    },
    {
        id               => 'f4ebe619-933c-4ead-b664-506a9c6e1ea0',
        config_item_name => 'first_login_confirmation',
        value_type       => {
            parent_type_name => 'text',
            options          => { style => 'paragraph' }
        },
        category_id => '7ad36e82-e32d-4578-af74-2cf05696f644',
        label       => 'Bedanktekst nieuwe gebruiker',
        description =>
            'Deze tekst verschijnt bij een nieuwe gebruikers, wanneer hij/zij'
            . ' haar afdeling heeft geselecteerd',
    },
    {
        id               => 'a34e8a0c-6427-4e97-a14b-db0f12783a21',
        config_item_name => 'first_login_intro',
        value_type       => {
            parent_type_name => 'text',
            options          => { style => 'paragraph' },
        },
        category_id => '7ad36e82-e32d-4578-af74-2cf05696f644',
        label       => 'Introductietekst nieuwe gebruiker',
        description =>
            'Deze tekst verschijnt wanneer een gebruiker voor het eerst inlogt'
            . ' in zaaksysteem',
    },
    {
        id               => '6271cb81-e5fb-40fd-9385-d8544fee75f0',
        config_item_name => 'new_user_template',
        value_type       => {
            parent_type_name => 'object_ref',
            options          => {
                object_type_name => 'email_template',
                type             => 'legacy_id'
            }
        },
        category_id => '7ad36e82-e32d-4578-af74-2cf05696f644',
        label       => 'Gebruikersacceptatie sjabloon',
        description => 'Deze setting wijzigt het e-mailsjabloon welke'
            . ' verzonden wordt bij het accepteren van een nieuwe gebruiker',
    },
    {
        id               => '608f73b1-f15f-4985-8dc6-06ce2d9c257a',
        config_item_name => 'pdf_annotations_public',
        value_type_name  => 'boolean',
        category_id      => 'b92f5a19-c076-43c7-82f1-897c8950f216',
        label => 'Annotaties zijn inzichtelijk voor andere behandelaars',
        description =>
            'Deze setting bepaalt welke annotaties verschijnen in de PDF'
            . ' preview dialoog.',
        deprecated => 1,
    },
    {
        id               => '2f9d7541-8e22-4dfc-9934-1374c55175b4',
        config_item_name => 'pip_login_intro',
        value_type       => {
            parent_type_name => 'text',
            options          => {
                style  => 'paragraph',
                format => 'html',
            }
        },
        category_id => '7ad36e82-e32d-4578-af74-2cf05696f644',
        label       => 'Welkomsttekst PIP',
        description =>
            'Deze tekst verschijnt op de inlogpagina van de PIP. Indien er'
            . ' geen waarde is ingevuld wordt er geen tekst weergegeven.',
    },
    {
        id               => '2ea16761-23b7-4964-9548-2d54861cc78d',
        config_item_name => 'public_manpage',
        value_type_name  => 'boolean',
        category_id      => '0e09b22a-1639-4ee5-8f0c-c59000c4fdfb',
        label            => 'API-handleiding publiekelijk beschikbaar',
        description =>
            'Deze setting bepaalt of de /man van uw zaaksysteem publiekelijk'
            . ' beschikbaar is.',
    },
    {
        id               => '348a00a6-4163-49f4-9279-eef1ccb19ae6',
        config_item_name => 'requestor_search_extension_active',
        value_type_name  => 'boolean',
        category_id      => 'b92f5a19-c076-43c7-82f1-897c8950f216',
        label            => 'Aanvrager externe zoekfunctie - ingeschakeld',
        description =>
            'Deze setting schakelt de extra zoekknop in het aanvrager dialoog'
            . ' in.',
        deprecated => 1,
    },
    {
        id               => '7fcfb2ea-8f46-475c-8f89-e09b2c28c828',
        config_item_name => 'requestor_search_extension_href',
        value_type       => {
            parent_type_name => 'uri',
            options          => { allowed_protocols => ['https', 'http'] }
        },
        label       => 'Aanvrager externe zoekfunctie - Link',
        category_id => 'b92f5a19-c076-43c7-82f1-897c8950f216',
        description =>
            'Voer het webadres in voor een externe link die op de aanvrager'
            . ' zoek-popup dialog verschijnt.',
        deprecated => 1,
    },
    {
        id               => '836c7e4b-a896-4c25-a7a4-6f138f75d41e',
        config_item_name => 'requestor_search_extension_name',
        value_type_name  => 'text',
        label            => 'Aanvrager externe zoekfunctie - Titel',
        category_id      => 'b92f5a19-c076-43c7-82f1-897c8950f216',
        description =>
            'Voer hier de knoptitel in voor een externe link die op de'
            . ' aanvrager zoek-popup dialog verschijnt.',
        deprecated => 1,
    },
    {
        id               => '09c5e460-2fd3-4153-84db-7f9159fe4f07',
        config_item_name => 'signature_upload_role',
        value_type       => {
            parent_type_name => 'object_ref',
            options          => {
                object_type_name => 'role',
                type             => 'label',
                constraints      => {
                    system_roles => 1,
                    labels       => [qw(Behandelaar Zaaksysteembeheerder)],
                },
            }
        },
        label       => 'Rol handtekening uploaden',
        category_id => '99fe5c3e-9209-45ff-a4ce-b8785addfa5f',
        description => 'Deze setting geeft aan welke rol rechten heeft om'
            . 'handtekeningen te uploaden.',
    },
    {
        id => '2cd00952-d6a0-4096-93c9-7515108c798d',
        config_item_name =>
            'subject_pip_authorization_confirmation_template_id',
        value_type => {
            parent_type_name => 'object_ref',
            options          => {
                object_type_name => 'email_template',
                type             => 'legacy_id'
            }
        },
        category_id => '204dea67-2001-4011-91c6-63a6a023aa5e',
        label       => 'Betrokkene op zaak machtigen e-mail template',
        description => 'Deze setting wijzigt het sjabloon dat gebruikt'
            . ' wordt om e-mail notificaties te versturen'
            . ' bij een betrokkene machtiging op een zaak.',
    },
    {
        id               => '49999eee-96b4-46c4-babc-8fa6bb5978cb',
        config_item_name => 'users_can_change_password',
        value_type_name  => 'boolean',
        category_id      => '99fe5c3e-9209-45ff-a4ce-b8785addfa5f',
        label            => 'Gebruikers kunnen hun eigen wachtwoord wijzigen',
        description      => '',
        deprecated       => 0,
    },
    {
        id               => '9b0956d2-c9a6-4e84-8c49-0f53d17551a1',
        config_item_name => 'bag_spoof_mode',
        value_type_name  => 'boolean',
        category_id      => '99fe5c3e-9209-45ff-a4ce-b8785addfa5f',
        label            => 'BAG Spoofmode',
        description      => 'BAG Spoofmode',
        mutable          => 0,
    },
    {
        id               => '55482181-7e7d-4bc2-b653-356762a91ade',
        config_item_name => 'bag_priority_gemeentes',
        value_type       => {
            parent_type_name => 'object_ref',
            options          => {
                object_type_name => 'municipality_code',
                type             => 'label'
            }
        },
        category_id => '99fe5c3e-9209-45ff-a4ce-b8785addfa5f',
        label       => 'Prioriteit gemeente',
        description => '',
        mvp         => 1,
    }
];

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
