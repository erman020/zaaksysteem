package Zaaksysteem::Object::Model;

use Moose;
use namespace::autoclean;

use Moose::Util::TypeConstraints;
use Moose::Util qw/ensure_all_roles/;

use Array::Compare;
use Encode qw(encode_utf8);
use IO::Scalar;
use JSON::XS ();
use Module::Load ();
use Module::Load::Conditional ();
use Pod::Find qw(pod_where);
use Pod::Text;
use List::Util qw(first);

use Zaaksysteem::Object;
use Zaaksysteem::Search::ZQL;
use Zaaksysteem::Types qw(UUID);
use BTTW::Tools;

use Zaaksysteem::Object::Constants qw[OBJECT_TYPE_NAME_MAP];

use Zaaksysteem::Object::Environment;
use Zaaksysteem::Object::Iterator;
use Zaaksysteem::Object::Relation;
use Zaaksysteem::Object::Reference::Instance;

use Zaaksysteem::Object::SecurityIdentity;
use Zaaksysteem::Object::SecurityIdentity::Position;
use Zaaksysteem::Object::SecurityIdentity::Group;
use Zaaksysteem::Object::SecurityIdentity::Role;

use Zaaksysteem::Object::SecurityRule;

use Module::Pluggable
    search_path => 'Zaaksysteem::Object::Process',
    instantiate => 'new',
    sub_name    => 'object_process_collections';

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Object::Model - Main in-code API for L<Zaaksysteem::Object>
interactions

=head1 SUMMARY

This model is one of the major parts in Zaaksysteem's "Object" infrastructure.
The main purpose of instances of this package is to handle persistance layer
interactions. Things like retrieving, updating, and other low-level actions
that cam modify the global object state directly.

    # Retrieve an object
    my $model = Zaaksysteem::Object::Model->new(...) # or get it via $c->model()
    my $object = $model->retrieve(uuid => '...');

    # Update some data with standard Moose attribute accessors
    $object->some_attribute('foo');

    # And save it back to the database.
    $model->save($object);

Instances of this model will usually not be passed around in object instances,
which is a departure from ActiveRecord pattern found in L<DBIx::Class>, and
more closely follows the Data Mapper pattern, like L<DBIx::DataMapper> does.

This model class wraps around our database layer to provide a single wrapper
around things Zaaksysteem calls objects.

=head1 USAGE

This package represents some early glue between ordinary
L<DBIx::Class::ResultSet> objects and ZQL queryable resultsets.

When creating new objects, use this pattern:

    my $obj = $c->model('DB::ObjectData')->create(
        { object_class => 'saved_search' }
    );

    $obj->update();

Idiomatically the interaction to get some data via a ZQL query looks like this:

    my $rs = $c->model('Object')->rs;

The above example will query all types of objects there are, under
no logical constraints.

    use Zaaksysteem::Search::ZQL;

    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case');

    my $rs = $zql->apply_to_resultset($c->model('Object')->rs);

The above will query the object model for full-object hydrations of any
C<case> type object.

Complexer queries can of course be constructed, you can find more on the
syntax and usage in the L<Zaaksysteem::Search::ZQL> documentation.

=head2 Extracting data from the ResultSet

Being able to construct resultsets is all fine and dandy, but how to use the
data?

    use Zaaksysteem::Search::ZQL;
    use Data::Dumper;

    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case');

    my $rs = $zql->apply_to_resultset($c->model('Object')->rs);

    for my $row ($rs->all) {
        warn Dumper($row->TO_JSON);
    }

=head1 Audit dumping of objects

This model is also capable of dumping arbitrary objects, along with associated
files and metadata for use in audit trailing of the data.

See L<Zaaksysteem::Object::ExportFile> for a complete file format description.

=head2 Model hooks

Sometimes it is neccesary, or sane, to break the strict Data Mapper pattern.
This feature of the object model is such a break, only use on metaclass
object instances (such as type, casetype, etc).

Objects should know nothing about the persistence layer, but in Zaaksysteem
this strict seperation is not always completely true. In the case of
L<type|Zaaksysteem::Object::Types::Type> object instances it's clear the
the instance doubles in function as a meta-class instance. Metaclasses and
the (type)model all exist at the same 'level' of abstraction, so it's useful
that these parts can communicate.

The model does this by calling the C<model_hook> method on the object before,
during, or after specific persistance operations like saving, updating, and
deleting. These operations pass along the operation type, the model and any
number of additional parameters relevant to the operation.

Implementing a hook can be accomplished by overriding the C<model_hook> method
in the objecttype class.

    package My::Nice::Object;

    use Moose;

    extends 'Zaaksysteem::Object';

    override model_hook => sub {
        my ($self, $action, $model, @args) = @_;

        # Split up the logic by action type
        if ($action eq 'update') {
            my ($object) = @args;

            ...
        }
    }

=head3 Operation types

=over 4

=item create

This operation is triggered just before the call to L<save_object> is made by
the model. No arguments are passed.

=item update

This operation is triggered just before the call to L<save_object> is made by
the model. The invocant for the C<model_hook> call is an instance of the old,
already persisted object. The new version of this object is passed as an
additional argument.

=back

=head1 ATTRIBUTES

=head2 schema

This attribute holds a reference to a L<DBIx::Class::Schema> instance. For
L<Zaaksysteem> this will usually be, specifically, an instance of
L<Zaaksysteem::Schema>.

=cut

has schema => (
    is => 'ro',
    required => 1,
    isa => 'DBIx::Class::Schema'
);

=head2 user

This attribute holds a reference to a conceptual 'user' of the system. This
object is used to check the ACL tables when provided.

Although it is called 'user', it can be any object that implements a
C<security_identity> method, so it should be technically possible to 'view'
the entire object namespace through any authorizable entity.

=cut

has user => (
    is => 'ro',
    isa => duck_type([qw[security_identity]]),
    predicate => 'has_user'
);


=head2 source_name

This attribute holds the source name of the table the model instance is to
interact with. Defaults to C<ObjectData>.

=cut

has source_name => (
    is => 'ro',
    isa => 'Str',
    default => 'ObjectData'
);

=head2 prefetch_relations

Boolean attribute that configures whether or not relations of fetched objects
should be auto-inflated. Setting this attribute to C<true> will not fetch
relations of related objects.

=cut

has prefetch_relations => (
    is => 'rw',
    isa => 'Bool',
    default => 0
);

=head2 object_environment

Holds a reference to a lazily built L<Zaaksysteem::Object::Environment> object
tree.

Delegates L<Zaaksysteem::Object::Environment/process> as C<process>.

See also L</build_object_environment>.

=cut

has object_environment => (
    is => 'rw',
    isa => 'Zaaksysteem::Object::Environment',
    lazy => 1,
    builder => 'build_object_environment',
    handles => {
        process => 'process'
    }
);

=head2 json

This internal, autovivified, L<JSON> instance specifically crafted to make the
behavior of reading a JSON object serialization of Objects and instantiating
the right one inheritable. Lazy man's dependency injection >:3

=cut

has json => (
    is => 'ro',
    isa => 'JSON',
    lazy => 1,
    default => sub {
        my $self = shift;

        return JSON->new->utf8->filter_json_object(sub {
            my $args = shift;

            my $type;
            if ($args->{type}) {
                $type = $args->{type};
            }
            else {
                $type = $args->{object_class};
            };

            return $args unless $type;

            # Guard against recursive deep inflation of JSON with embedded
            # relations
            return $args if exists $args->{ multiple_values };

            # Workaround for group/role permissions on case/casetype/attribute
            # object blobs.
            return $args if exists $args->{ instance };

            my $meta = try {
                return $self->find_type_meta($type)
            };

            return {} unless $meta;

            $args->{ relations } = [ map {
                delete $_->{ related_object };

                Zaaksysteem::Object::Relation->new($_)
            } @{ $args->{ related_objects } } ];

            my $values = delete $args->{ values } || {};

            # Frontend fix, it still posts related_objects as an empty array
            delete $values->{ relations };

            my %construct_args = (%{ $args }, %{ $values });

            delete $construct_args{ $_ } for grep {
                not defined $construct_args{ $_ }
            } keys %construct_args;

            return $meta->new_object(%construct_args, update_security_rules => 0);
        });
    }
);

=head2 type_uuid_mapping

Holds a map of object type name to type object UUID.

=cut

has type_uuid_mapping => (
    is => 'rw',
    isa => 'HashRef[Str]',
    traits => [qw[Hash]],
    default => sub { return {} },
    handles => {
        has_type_uuid => 'exists',
        set_type_uuid => 'set',
        get_type_uuid => 'get'
    }
);

=head2 type_meta_mapping

Holds a map of object type name to type meta instance.

=cut

has type_meta_mapping => (
    is => 'rw',
    isa => 'HashRef[Moose::Meta::Class]',
    traits => [qw[Hash]],
    default => sub { return {} },
    handles => {
        has_type_meta => 'exists',
        set_type_meta => 'set',
        get_type_meta => 'get'
    }
);

=head1 METHODS

=head2 BUILD

Preloads type_uuid_mapping and when possible places this in a cache of the L<Zaaksysteem::Schema>
object.

=cut

sub BUILD {
    my $self = shift;

    if (
        $self->schema->cache &&
        (
            my $cache = $self->schema->cache->get({
                key => __PACKAGE__ . '|type_uuid_mapping'
            })
        )
    ) {
        return $self->type_uuid_mapping($cache);
    }

    # Prefetch all known in-storage 'type' object uuids
    my $rs = $self->new_resultset->search(
        { object_class => 'type' },
        { select => [qw[uuid index_hstore->'prefix']]
    });

    my %type_uuid_map = map {
        $_->get_column("index_hstore->'prefix'") => $_->get_column('uuid')
    } grep { defined $_->get_column("index_hstore->'prefix'") } $rs->all;

    $self->schema->cache->set({
        key     => __PACKAGE__ . '|type_uuid_mapping',
        value   => \%type_uuid_map
    }) if $self->schema->cache;

    $self->type_uuid_mapping(\%type_uuid_map);
}

=head2 find_type_meta

This method attempts to retrieve a type's L<Moose::Meta::Class> instance.

If the type isn't available in the meta cache, L<Module::Load::Conditional>
will be used to find the package defining the type in the current
source tree.

If no such package exists, the L</type_uuid_mapping> cache will be consulted
and the relevant type will be retrieved from storage.

=cut

sub find_type_meta {
    my $self = shift;
    my $type = shift;

    return $self->get_type_meta($type) if $self->has_type_meta($type);

    my $package_meta = $self->_find_type_package($type);

    if ($package_meta) {
        $self->set_type_meta($type, $package_meta);
        return $package_meta;
    }

    my $object_meta = $self->_find_type_object($type);

    if ($object_meta) {
        $self->set_type_meta($type, $object_meta);
        return $object_meta;
    }

    throw('object/type/type_not_found', sprintf(
        'Could not find type metaclass for "%s"',
        $type
    ));
}

sub _find_type_package {
    my $self = shift;
    my $type = shift;

    my $guess = $self->inflect_package_by_type($type);

    my $check = Module::Load::Conditional::can_load(
        modules => { $guess => undef },
        verbose => $ENV{ CATALYST_DEBUG }
    );

    return unless $check;

    unless(eval { $guess->can('new') }) {
        throw('object/inflate', sprintf(
            'Made a guess to load %s, but %s::new is unavailable',
            $guess, $guess
        ));
    }

    return $guess->meta;
}

sub _find_type_object {
    my $self = shift;
    my $type = shift;

    return unless $self->has_type_uuid($type);

    my $object = $self->inflate_from_row($self->new_resultset->find({
        uuid => $self->get_type_uuid($type)
    }));

    unless ($object->does('Zaaksysteem::Object::Roles::MetaType')) {
        throw('object/type/object_invalid', sprintf(
            'Found object by type (%s) is invalid',
            $type
        ));
    }

    return $object->instance_meta_class;
}

=head2 new_resultset

This method builds a fresh L<DBIx::Class::ResultSet> for the object table,
with all the bells and whistles.

    my $rs = $c->model('Object')->new_resultset;

=cut

sub new_resultset {
    my $self = shift;

    my $rs = $self->schema->resultset($self->source_name);

    ensure_all_roles($rs, 'Zaaksysteem::Backend::Object::Roles::ObjectResultSet');

    my $args = { };

    if (!$self->has_user || $self->user->is_admin) {
        $self->log->trace("Admin user: rights are implied and no capabilty checks are executed");
    } else {
        $args->{ zaaksysteem }{ user } = $self->user;
    }

    $rs->hydrate_actions(1);

    return $rs->search_rs({ invalid => 0 }, $args);
}

=head2 acl_rs

Returns a resultset with current user ACL constraints built-in.

=cut

sub acl_rs {
    my $self = shift;
    my $capability = shift;

    my $rs = $self->new_resultset;

    return $rs->search_rs(
        { -or => [ $rs->acl_items($capability) ] }
    );
}

=head2 rs

Returns a resultset that can be used to query hstore properties. The returned
object will be of type L<Zaaksysteem::Backend::Object::Roles::ObjectResultSet>.
The rows returned by the resultset are of type
L<Zaaksysteem::Backend::Object::Roles::ObjectComponent>.

Takes an optional argument which will be used as the query's C<alias>.

    my $rs = $c->model('Object')->rs;

    or

    my $rs = $c->model('Object')->rs('my_alias');

=cut

sub rs {
    my $self = shift;
    my $alias = shift;
    my $prefetch_relations = shift // 1;

    my $rs = $self->acl_rs;

    if ($alias) {
        $rs = $rs->search(undef, { alias => $alias });
    }

    return $rs unless $prefetch_relations;

    return $rs->search_rs(undef, {
        prefetch => [ 'object_relation_object_ids' ]
    })
}

=head2 inflect_package_by_type

The method tries to infer the fully qualified packagename for a given object
type string.

    my $package = $model->inflect_package_by_type('case');

It tries to be smart by converting C<_> and C</> chars into, respectively,
camelcase and sub-package packagename parts.

=head3 Examples

    case        => Zaaksysteem::Object::Types::Case
    case_meta   => Zaaksysteem::Object::Types::CaseMeta
    case/event  => Zaaksysteem::Object::Types::Case::Event

=cut

sub inflect_package_by_type {
    my $self = shift;
    my $type = shift // '';

    my @pkg_parts = qw[Zaaksysteem Object Types];

    for my $subtype (split m[/], lc($type)) {
        my @words = split m[_], $subtype;

        my $subpkg = join '', map { ucfirst } @words;

        push @pkg_parts, $subpkg;
    }

    return join '::', @pkg_parts;
}

=head2 inflate

Inflate a L<Zaaksysteem::Object> instance from a source. Instantiators
delegated by provided named arguments.

    my $object = $c->model('Object')->inflate(row => $dbix_row);

    my $maybe_object_tree = $c->model('Object')->inflate(json => $string);

    my @objects = $c->model('Object')->inflate(rs => $object_data_resultset);

=cut

define_profile inflate => (
    optional => {
        json => 'Str',
        row => 'DBIx::Class::Row',
        rs => 'DBIx::Class::ResultSet',
        relation => 'Zaaksysteem::Schema::ObjectRelation',
        hash => 'HashRef'
    },
    require_some => {
        source => [ 1, qw[json row rs relation hash] ]
    }
);

sub inflate {
    my ($self, %params) = @_;

    my $args = assert_profile(\%params)->valid;

    if(exists $args->{ json }) {
        return $self->inflate_from_json($args->{ json }, %params);
    } elsif(exists $args->{ row }) {
        return $self->inflate_from_row($args->{ row }, %params);
    } elsif(exists $args->{ rs }) {
        return $self->inflate_from_rs($args->{ rs }, %params);
    } elsif(exists $args->{ relation }) {
        return $self->inflate_from_relation($args->{ rel }, %params);
    } elsif(exists $args->{ hash }) {
        return $self->inflate_from_hash($args->{ hash }, %params);
    }

    throw('object/inflate', 'Unable to decide on inflation implementation');
}

=head2 inflate_from_json

This method takes a JSON object serialization of an Object or an array
of objects, and produces a list of Object instances. The behavior of the
JSON decoder is such that for every new C<{}>-object found in the input it
will test if it looks like an L<Object|Zaaksysteem::Object> and attempt to
instantiate it in-place.

This means that deeply nested objects will generate a large tree of objects,
which are hard to validate compared to flat lists of, and singular, objects.

    my $maybe_object_tree = $model->inflate_from_json($json_plaintext);

=cut

sub inflate_from_json {
    my $self = shift;

    return $self->json->decode(shift);
}

=head2 inflate_from_row

This method takes a L<DBIx::Class::Row> and inflates the data contained therein
to a L<Zaaksysteem::Object> instance.

    my $object = $model->inflate_from_row($object_data_row);

=cut

sig inflate_from_row => 'Zaaksysteem::Backend::Object::Data::Component';

sub inflate_from_row {
    my $self = shift;
    my $row = shift;

    my $meta_type_name = $row->can('meta_type_name') ?
        $row->meta_type_name :
        $row->object_class;

    my $meta = $self->find_type_meta($meta_type_name);

    my $args = {
        id => $row->id,
        object_id => $row->object_id,
        date_created => $row->date_created,
        date_modified => $row->date_modified
    };

    my $values = $row->properties->{ values };

    unless (ref $values eq 'HASH') {
        throw(
            'object/inflate/values_empty',
            'Unable to inflate object without value data'
        )
    }

    my @object_attributes = map { $_->name } grep {
        $_->does('Zaaksysteem::Metarole::ObjectAttribute')
    } $meta->get_all_attributes;

    for my $object_attribute (@object_attributes) {
        # Skip over things already set in the args, @object_attributes should
        # contain no collisions to begin with, and if anything thinks about
        # writing over id/object_id/date_modified/etc, we can cope.
        next if exists $args->{ $object_attribute };

        my $data = $values->{ $object_attribute };
        # Objecttypes might be hydrated with Object::Attribute insteances,
        # compensate.
        if (blessed $data) {
            $data = {
                value => $data->value
            };
        }

        next unless ref $data eq 'HASH';
        next unless defined $data->{ value };

        $args->{ $object_attribute } = $data->{ value };
    }

    my @object_relations = grep {
        $_->does('Zaaksysteem::Metarole::ObjectRelation')
    } $meta->get_all_attributes;

    for my $object_relation (@object_relations) {
        my @relations = grep {
            $_->name eq $object_relation->name
        } $row->object_relation_object_ids->all;

        next unless scalar @relations;

        my @values;

        if ($object_relation->embed) {
            @values = map {
                $self->inflate_from_json(encode_utf8($_->get_column('object_embedding')))
            } @relations;
        } else {
            for my $relation (@relations) {
                my $uuid = $relation->get_column('object_uuid');

                push @values, Zaaksysteem::Object::Reference::Instance->new(
                    id => $uuid,
                    type => $relation->object_type,
                    preview => $relation->object_preview,
                    instantiator => sub {
                        $self->retrieve(uuid => shift->id)
                    }
                );
            }
        }

        $args->{ $object_relation->name } = $object_relation->isa_set ? \@values : $values[0];
    }

    # If the column was not loaded, get_column will except, so guard against
    # it.
    if($row->has_column_loaded('authorizations')) {
        $args->{ actions } = $row->get_column('authorizations');
    }

    my $object = $meta->new_object($args);

    my @actions;

    if($object->does('Zaaksysteem::Object::Roles::Security')) {
        my @rules;

        for my $acl ($row->object_acl_entries, $row->inherited_acl_entries) {
            my $entity = Zaaksysteem::Object::SecurityIdentity->new(
                entity_type => $acl->entity_type,
                entity_id   => $acl->entity_id
            );

            push @rules, Zaaksysteem::Object::SecurityRule->new(
                entity      => $entity,
                capability  => $acl->capability,
            );
        }

        if($self->has_user) {
            my @user_rules = grep {
                $self->user->matches_security_rule(rule => $_);
            } @rules;

            # Iterate over permissions first, add all capabilities the user
            # matches on
            for my $permission (@user_rules) {
                next if grep { $_ eq $permission->capability } @actions;

                push @actions, $permission->capability;
            }
        } else {
            push @actions, $object->capabilities;
        }

        $object->security_rules(\@rules);
    } else {
        push @actions, $object->capabilities;
    }

    $object->actions(\@actions);

    if($object->does('Zaaksysteem::Object::Roles::Relation')) {
        my @relations;

        for my $rel ($row->object_relationships_object1_uuids) {
            my $args = {
                related_object_id    => $rel->get_column('object2_uuid'),
                related_object_type  => $rel->object2_type,
                relationship_name_a  => $rel->type1,
                relationship_name_b  => $rel->type2,
                relationship_title_a => $rel->title1,
                relationship_title_b => $rel->title2,
                blocks_deletion      => $rel->blocks_deletion,
            };

            if($rel->get_column('owner_object_uuid')) {
                $args->{ owner_object_id } = $rel->get_column('owner_object_uuid');
            }

            my $relation = Zaaksysteem::Object::Relation->new($args);

            push @relations, $relation;
        }

        for my $rel ($row->object_relationships_object2_uuids) {
            my $args = {
                related_object_id    => $rel->get_column('object1_uuid'),
                related_object_type  => $rel->object1_type,
                relationship_name_a  => $rel->type2,
                relationship_name_b  => $rel->type1,
                relationship_title_a => $rel->title2,
                relationship_title_b => $rel->title1,
                blocks_deletion      => $rel->blocks_deletion,
            };

            if($rel->get_column('owner_object_uuid')) {
                $args->{ owner_object_id } = $rel->get_column('owner_object_uuid');
            }

            my $relation = Zaaksysteem::Object::Relation->new($args);

            push @relations, $relation;
        }

        $object->relations(\@relations);

        if($self->prefetch_relations) {
            for my $rel ($object->all_relations, $object->all_inherited_relations) {
                next if blessed $rel->related_object;

                # Prevent relation-inflation loops
                try {
                    $self->prefetch_relations(0);
                    $rel->related_object($self->retrieve(uuid => $rel->related_object_id));
                } catch {
                    $self->log->warn($_);
                } finally {
                    $self->prefetch_relations(1);
                };
            }
        }
    }

    return $object;
}

=head2 inflate_from_rs

This method takes a L<DBIx::Class::ResultSet> and inflates the data contained
therein to a list of L<Zaaksysteem::Object> instances.

    my @objects = $model->inflate_from_rs($resultset);

=cut

# XXX TODO: Contemplate the need for a Object-specific resultset equivalent
# instead of plain list hydration, paging could be internalized, if Data::Page
# proves annoying with DBIx on the lower levels.

sub inflate_from_rs {
    my $self = shift;
    my $rs = shift;
    my @args = @_;

    my @results = map { $self->inflate_from_row($_, @args) } $rs->all;

    if($rs->describe_rows) {
        map { ensure_all_roles($_, 'Zaaksysteem::Object::Roles::Describe') } @results;

        my @requested = @{ $rs->object_requested_attributes };

        if(scalar @requested) {
            map { $_->describe_attributes(\@requested) } @results;
        }
    }

    return @results;
}

=head2 inflate_from_relation

Inflation dispatcher for L<Zaaksysteem::Schema::ObjectRelation> rows.

    my $object = $model->inflate_from_relation($object_relation_row);

=cut

sig inflate_from_relation => 'Zaaksysteem::Schema::ObjectRelation';

sub inflate_from_relation {
    my $self = shift;
    my $relation = shift;

    if ($relation->get_column('object_embedding')) {
        return $self->inflate_from_json(
            encode_utf8($relation->get_column('object_embedding'))
        );
    } else {
        return $self->inflate_from_row($relation->object_uuid);
    }
}

=head2 inflate_from_hash

Inflation dispatcher for 'plain' C<HashRef> object data structures.

    my $ref = $model->inflate_from_hash({
        type => 'my_type',
        reference => 'ef0ca8e5-b3b2-479e-98c6-5e3f3c044f0e',
    });

    or

    my $object = $model->inflate_from_hash({
        type => 'my_type',
        instance => {
            foo => 'bar'
        }
    });

=cut

sig inflate_from_hash => 'HashRef';

sub inflate_from_hash {
    my ($self, $data) = @_;

    my $reference;
    my $instance;

    my $ref_args = assert_profile($data, profile => {
        required => {
            type => 'Str',
            reference => maybe_type(UUID),
        },
        optional => {
            instance => 'HashRef',
        }
    })->valid;

    if ($ref_args->{ reference }) {
        return Zaaksysteem::Object::Reference::Instance->new(
            id => $ref_args->{ reference },
            type => $ref_args->{ type },
            instantiator => sub {
                $self->retrieve(uuid => shift->id)
            }
        );
    }

    my $meta = $self->find_type_meta($ref_args->{ type });

    return $meta->new_object(%{ $ref_args->{ instance } });
}

=head2 zql_search

This method provides an easier interface to query by a
L<ZQL|Zaaksysteem::Search::ZQL> query.

=head3 Signature

C<< Str => Zaaksysteem::Object::Iterator >>

=head3 Example

    my $iter = $c->model('Object')->zql_search('SELECT ...');

=cut

sig zql_search => 'Str => Zaaksysteem::Object::Iterator';

sub zql_search {
    my $self = shift;
    my $query = shift;

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    return Zaaksysteem::Object::Iterator->new(
        rs => $zql->apply_to_resultset($self->rs),
        inflator => sub { $self->inflate_from_row(shift) }
    );
}

=head2 search

This method implements a minimal search capability on the Object model. It
currently implements exact-value searches in the L<DBIx::Class> style.

    # Populate @objects with objects that have 'column' set to 'value'.
    my @objects = $model->search('my object class', { column => 'value' });

=cut

sub search {
    my $self = shift;

    my $iter = $self->_search_rs($self->rs, @_);

    return wantarray ? $iter->all : $iter;
}

=head2 search_rs

See L</search>. Except this doesn't return a list of objects, but a
L<DBIx::Class::ResultSet>.

=cut

sig search_rs => 'Str, ?HashRef';

sub search_rs {
    my $self = shift;

    return $self->_search_rs($self->rs, @_)->rs;
}

=head2 search_by_capability

Like L</search_rs>, but allows you to specify a what you'll be doing with the data, so
the proper ACL checks can be performed.

For example:

    my $rs = $model->search_by_capability('write', 'case', {});

will return a resultset with all case objects that the current user has at least "write"
rights for.

The first argument is an L<ACL-capability|Zaaksysteem::Types/ACLCapability> (as defined
in L<Zaaksysteem::Types>).

=cut

sig search_by_capability => 'Str, Str, ?HashRef';

sub search_by_capability {
    my $self = shift;
    my $capability = shift;

    my $rs = $self->acl_rs($capability);

    return $self->_search_rs($rs, @_)->rs;
}

=head2 _search

This method implements a generic object-attribute search capability on the
provided resultset. It is mainly used for internal queries where we want to
query the object tables without taking ACL rules into consideration.

    my @objects = $model->_search($model->new_resultset, 'product', { external_id => '1234' });

B<DO NOT> use this method with L</new_resultset> unless you are sure the
resulting objects will not leak to the frontend, via this flow unauthorized
users could inspect and modify all objects in the database.

=cut

sub _search {
    my ($self, $rs, $object_type, $args) = @_;

    my $subrs = $self->_search_rs($rs, $object_type, $args);

    return $subrs->all;
}

=head2 _search_rs

The guts of "_search". Returns the generated L<DBIx::Class::ResultSet>, instead
of calling L</inflate_from_rs> on it.

=cut

sub _search_rs {
    my ($self, $rs, $object_type, $args) = @_;

    $rs = $rs->search_rs({ object_class => $object_type });

    if(defined $args && ref $args eq 'HASH') {
        $rs = $rs->search_hstore(
            $rs->map_search_args($args)
        );
    }

    return Zaaksysteem::Object::Iterator->new(
        rs => $rs,
        inflator => sub { $self->inflate_from_row(shift) },
    );
}

=head2 query_rs

Builds a resultset using a L<Zaaksysteem::Object::Query> instance.

=cut

sig query_rs => 'Zaaksysteem::Object::Query';

sub query_rs {
    my ($self, $query) = @_;

    return $self->rs->apply_query($query);
}

=head2 count

This method implements the same logic as L</search>, but instead of returning
a list of L<Object|Zaaksysteem::Object>s, it returns the count for that list
indead.

This method exists because there is no drop-in replacement for Object
resultsets as an abstraction over L<DBIx::Class::ResultSet>s, and should be
evicted from the codebase when we have such a replacement in place.

    my $object_count = $model->count({ column => 'value' });

=cut

sub count {
    my $self = shift;
    my $rs = $self->rs->search({ object_class => shift });
    my $search_args = $rs->map_search_args(shift);

    return $rs->search_hstore($search_args)->count;
}

=head2 delete

This method takes an object or uuid argument and permanently destroys the
object, permanently. This method knows nothing about versions, so deleting
in the middle of a version-chain might result in fuzzy state.

    $model->delete(object => $ie8);

=cut

define_profile delete => (
    optional => {
        object => 'Zaaksysteem::Object',
        uuid   => UUID
    },
    require_some => {
        object_or_uuid => [ 1, qw[object uuid] ]
    }
);

sub delete {
    my ($self, %params) = @_;

    my $args = assert_profile(\%params)->valid;

    if(exists $args->{ object }) {
        return $self->delete_by_uuid($args->{ object }->id);
    } elsif(exists $args->{ uuid }) {
        return $self->delete_by_uuid($args->{ uuid });
    }

    throw('object/delete', 'Cannot delete object, unable to interpret parameters for object retrieval');
}

=head2 delete_by_uuid

This method is the hard implementation of object deletion from the database,
as such it really only finds the object in the database and calls
C<< L<DBIx::Class::Row>::delete >> on the retrieved row instance.

I<< When in doubt, use L</delete> instead of this method >>, unless you are
extending the Object model for some reason, override this so that you are
spared the logic checks done by L</delete>.

    $model->delete_by_uuid($uuid);

The returned value comes directly from C<< L<DBIx::Class::Row>::delete >>,
so it may not mean anything contextually correct.

=cut

sub delete_by_uuid {
    my $self = shift;
    my $uuid = shift;

    my $object_data = $self->rs->find($uuid);
    my $object = $self->inflate_from_row($object_data);

    if (!$object->is_deleteable) {
        throw(
            'object_model/delete_disallowed_by_relationship',
            "Cannot delete object $uuid, because other objects depend on it."
        );
    }

    $self->log->trace(sprintf('Deleting object "%s"', $object));

    $self->_create_log_entry(
        action => 'delete',
        pre_object => $object
    );

    return $object_data->delete;
}

=head2 get

Convenience method for retrieving objects by their reference. It also
implements some sanity checks, to make sure we're not accessing the database
for no good reason (calling $model->get($object) *will* work, since
$object->does(Ref), but it won't load from db).

Also, if a true reference instance was passed to us, and that reference has
an instantiator, the instantiator will be used to retrieve the object. This
is to allow flexible loading strategies that wrap the underlying
$model->retrieve() calls. Unless an instantiator calls $model->get itself this
will not introduce recursion loops.

    my $object = $model->get(ref => $my_ref);

    my $object2 = $model->get(ref => $my_object->_ref);

=cut

define_profile get => (
    required => {
        ref => role_type('Zaaksysteem::Object::Reference')
    }
);

sub get {
    my ($self, %params) = @_;

    my $args = assert_profile(\%params)->valid;

    my $ref = $args->{ ref };

    # Prevent re-loading loops, if the object is itself instantiable,
    # go do that instead of retrieving a new object from db.
    return $ref->_instance if $ref->_instantiable;

    return $self->retrieve(ref => $ref);
}

=head2 retrieve

This method allows for generic object retrieval from the database. Returns a
L<Zaaksysteem::Object> instance in case a row with the provided UUID exists.
Returns C<undef> when no row could be found.

    my $obj = $c->model('Object')->retrieve(uuid => $uuid);

=cut

define_profile retrieve => (
    optional => {
        uuid => UUID,
        ref => role_type('Zaaksysteem::Object::Reference')
    },
    require_some => {
        object_reference => [ 1, qw[uuid ref] ]
    }
);

sub retrieve {
    my ($self, %params) = @_;

    my $args = assert_profile(\%params)->valid;

    my $row = $self->rs->find($args->{ uuid } || $args->{ ref }->id);

    return unless $row;

    if ($row->invalid) {
        throw('object/model/object_invalid', 'Object is marked as invalid');
    }

    return $self->inflate_from_row($row);
}

=head2 save

This is the most generic save object. It adheres to the hashmap options calling
convention. On the highest level, this method merely auto-instantiates a JSON
argument into an L<Object|Zaaksysteem::Object> before delegating the actual work
to C<save_object>.

    # Will instantiate an $object from a JSON message, and save it to the db.
    my $object = $c->model('Object')->save(json => '{ ... }');

    # This does exactly the same, but no JSON is decoded since the object
    # is already available.
    my $object = $c->model('Object')->save(object => $object_instance);

Optimization note; only assign the return value of C<save> when actually
needed. If this call is made in a void context, the expensive re-inflation of
the updated object_data will not be executed (unless the model requires it for
post-processing).

=cut

define_profile save => (
    optional => {
        object => 'Zaaksysteem::Object',
        json => 'Str'
    },
    require_some => {
        object_or_json => [ 1, qw[object json] ]
    }
);

sub save {
    my ($self, %params) = @_;

    my $args = assert_profile(\%params)->valid;

    if(exists $args->{ json }) {
        my $res = $self->inflate_from_json(delete $args->{ json });
        if (exists $res->{object}) {
            $args = { %$args, %$res };
        }
        else {
            $args->{object} = $res;
        }
    }

    return $self->save_object(
        object         => $args->{ object },
        log            => 1,
    );
}

=head2 invalidate

=cut

sig invalidate => 'Zaaksysteem::Backend::Object::Data::ResultSet';

sub invalidate {
    my $self = shift;
    my $rs = shift;

    $rs->update({ invalid => 1 });

    return;
}

=head2 save_object

This method does a generic 'save' on an L<Object|Zaaksysteem::Object>
instance. It will check if the object has a true-ish id, which implies an
object_data row update. If the Object's id is false-like, the object_data row
will be created.

Returns a new inflation of the object_data row.

    # $object has no id, so $new_object will derive from a new object_data row
    my $new_object = $c->model('Object')->save_object(object => $object);

    # $object_new has an id, so $new_new_object will derive from an existing object_data row
    my $new_new_object = $c->model('Object')->save_object(object => $new_object);

Optimization note; only assign the return value of C<save_object> when
actually needed. If this call is made in a void context, the expensive
re-inflation of the updated object_data will not be executed (unless the model
requires it for post-processing).

=cut


define_profile save_object => (
    required => {
        object => 'Zaaksysteem::Object',
    },
    optional => {
        log            => 'Bool',
    },
);

sub save_object {
    my ($self, %params) = @_;

    my $args = assert_profile(\%params)->valid;

    my $object = $args->{object};

    my $object_data;
    my (@added_related, @deleted_related);

    $self->assert_unique_object($object);

    my ($action, $old_object);

    if($object->has_id) {
        $old_object = $self->retrieve(uuid => $object->id);
        $action = 'update';

        unless($old_object->has_action(sub { $_ eq 'write' })) {
            throw('object/save', 'Unable to save object, user does not have write access');
        }

        $old_object->model_hook($action, $self, $object);

        $object_data = $self->update_object_data($object);

        # Update object_relation table with our (possibly new) preview string
        $object_data->object_relation_object_uuids->update({
            object_preview => $object->TO_STRING
        });
    } else {
        $action = 'create';

        $object->model_hook($action, $self);

        $object_data = $self->create_object_data($object);
    }

    $self->refresh_object_relations($object, $object_data);

    if($object->does('Zaaksysteem::Object::Roles::Relation')) {
        my @old_relations = defined $old_object ? $old_object->all_relations : ();

        # We can't use the saved object yet, unless we want to take the penalty
        # of retrieving it twice (the first time with possibly incorrect
        # relationships)
        if ($action eq 'create') {
            $object->id($object_data->uuid);
        }

        for my $relation ($object->filter_relations(sub { not $_->{ is_inherited } })) {
            if (my $relcount = grep { $relation->equal_to($_) } @old_relations) {
                ### Check for duplicates, delete them when exists
                if ($relcount > 1) {
                    my $dupes = $self->schema->resultset('ObjectRelationships')->search({
                        '-or' => [
                            {
                                object1_uuid => $object_data->uuid,
                                object2_uuid => $relation->related_object_id,
                            },
                            {
                                object2_uuid => $object_data->uuid,
                                object1_uuid => $relation->related_object_id,
                            }
                        ]
                    });

                    ### Keep the first
                    $dupes->next;

                    while (my $dupe = $dupes->next) {
                        $dupe->delete;
                    }
                }

                # If the relation already exists, remove from the list (so we can
                # use the list for removals at the end), and start next iteration.
                @old_relations = grep {
                    not $relation->equal_to($_)
                } @old_relations;

                next;
            }

            my $related_object = $self->retrieve(
                uuid => $relation->related_object_id
            );

            $self->schema->resultset('ObjectRelationships')->create({
                owner_object_uuid => $relation->owner_object_id,

                object1_uuid => $object_data->uuid,
                object1_type => $object_data->object_class,
                type1        => $relation->relationship_name_a,
                title1       => $object->TO_STRING(),

                object2_uuid => $relation->related_object_id,
                object2_type => $relation->related_object_type,
                type2        => $relation->relationship_name_b,
                title2       => $related_object->TO_STRING(),

                blocks_deletion => $relation->blocks_deletion,
            });

            push @added_related, $related_object;
        }

        for my $old_relation (@old_relations) {
            unless ($old_relation->can_unrelate($object)) {
                throw('object/save/relation_not_owned', sprintf(
                    'Unable to remove relation to %s, other side owns it.',
                    $old_relation->related_object_id
                ));
            }

            my $related_object = $self->retrieve(uuid => $old_relation->related_object_id);
            push @deleted_related, $related_object;

            $object_data->object_relationships_object1_uuids->search({
                object2_uuid => $old_relation->related_object_id,
                type2 => $old_relation->relationship_name_b,
                type1 => $old_relation->relationship_name_a
            })->delete_all;

            $object_data->object_relationships_object2_uuids->search({
                object1_uuid => $old_relation->related_object_id,
                type1 => $old_relation->relationship_name_b,
                type2 => $old_relation->relationship_name_a
            })->delete_all;
        }

        # And put it back the way it was
        if ($action eq 'create') {
            $object->clear_id();
        }
    }

    # Optimization, no need to inflate the new object if it's not going to be
    # used, and we don't have to log.
    return if not ($args->{ log } || defined wantarray);

    my $new_object = $self->inflate_from_row($object_data);

    # TODO Re-build this in a new subscribe/publish framework
    if($args->{log} && $object->does('Zaaksysteem::Object::Roles::Log')) {
        $self->_create_log_entry(
            action            => $action,
            pre_object        => $old_object,
            post_object       => $new_object,
        );

        $self->_create_relation_log('add', $new_object, \@added_related)
            if (@added_related);
        $self->_create_relation_log('delete', $new_object, \@deleted_related)
            if (@deleted_related);
    }

    return $new_object;
}

=head2 refresh_object_relations

Refresh object relation rows for the given object

=cut

sig refresh_object_relations => 'Zaaksysteem::Object, Zaaksysteem::Backend::Object::Data::Component';

sub refresh_object_relations {
    my $self = shift;
    my $object = shift;
    my $object_data = shift;

    $object_data->object_relation_object_ids->delete_all;

    my @object_relations = grep {
        $_->does('Zaaksysteem::Metarole::ObjectRelation')
    } $object->meta->get_all_attributes;

    for my $object_relation (@object_relations) {
        next unless $object_relation->has_value($object);

        my $value = $object_relation->get_value($object);

        for my $ref ($object_relation->isa_set ? @{ $value } : ($value)) {
            unless ($ref->has_id || $object_relation->embed) {
                $ref = $self->save_object(object => $ref);
            }

            $object_data->object_relation_object_ids->create({
                name => $object_relation->name,
                object_type => $ref->type,
                object_preview => $ref->TO_STRING,

                ($object_relation->embed ?
                    (object_embedding => $ref->_instance) :
                    (object_uuid => $ref->id)
                ),
            });
        }
    }
}

sub _create_log_entry {
    my $self = shift;
    my %args = @_;

    my @changes;

    if ($args{post_object}) {
        my $comp = Array::Compare->new();

        # Create or update:
        @changes = map {
            my $attr_name = $_->name;
            my $result = {
                field       => $_->name,
                field_label => $_->label,
                old_value   => (defined $args{pre_object})
                    ? ($args{pre_object}->get_attribute_value($attr_name))
                    : (undef),
                new_value   => $args{post_object}->get_attribute_value($attr_name),
            };

            for my $val (qw(old_value new_value)) {
                if (blessed($result->{$val}) && $result->{$val}->isa('DateTime')) {
                    $result->{$val} = $result->{$val}->set_time_zone('Europe/Amsterdam')->dmy;
                }
                else {
                    $result->{$val} = $result->{$val};
                }
            }

            $result;
        } grep {
            my $attr_name = $_->name;
            if (defined($args{pre_object})) {
                my $old_value = $args{pre_object}->get_attribute_value($attr_name);
                my $new_value = $args{post_object}->get_attribute_value($attr_name);

                if (defined $old_value && defined $new_value) {
                    if (ref($old_value) eq 'ARRAY') {
                        # Returns true if arguments are equal
                        not $comp->compare($old_value, $new_value);
                    }
                    else {
                        $old_value ne $new_value;
                    }
                }
                else {
                    defined $old_value != defined $new_value;
                }
            }
            else {
                1;
            }
        } $args{post_object}->object_attributes;
    }

    # Determine which object to use for logging:
    # - "delete" only gets a "pre_object"
    # - "create" only gets a "post_object"
    # - "update" gets both
    my $log_object = $args{post_object} // $args{pre_object};

    my $filter = sub {
        return unless $_->related_object_type eq 'case';
        return unless ($_->relationship_name_a eq 'job_dependency' && $_->relationship_name_b eq 'scheduled_by') ||
                      ($_->relationship_name_b eq 'job_dependency' && $_->relationship_name_a eq 'scheduled_by');

        return 1;
    };

    my $case_id;
    if ($log_object->does('Zaaksysteem::Object::Roles::Relation')) {
        my @cases = $log_object->filter_relations($filter);

        if(scalar @cases > 1) {
            $self->log->warn(sprintf(
                "Multiple cases claim to have %s as a job dependency, this shouldn't happen.",
                $log_object->TO_STRING
            ));
        }

        my $rel = shift @cases;

        if($rel) {
            if($rel->related_object) {
                $case_id = $rel->related_object->case_number;
            } else {
                my $case = $self->retrieve(uuid => $rel->related_object_id);

                $case_id = $case->case_number if $case;
            }

            unless($case_id) {
                $self->log->warn('Unable to find case object for given relation, has it been deleted?');
            }
        }
    }

    if (@changes) {
        $self->schema->resultset('Logging')->trigger(
            sprintf('object/%s', $args{action}),
            {
                object_uuid => $log_object->id,
                zaak_id     => $case_id,
                data        => {
                    object_uuid      => $log_object->id,
                    object_label     => $log_object->TO_STRING,
                    object_type      => $log_object->type,
                    changes          => \@changes,
                },
            },
        );
    }

    return;
}

sub _create_relation_log {
    my ($self) = shift;
    my ($action, $object, $relations) = @_;

    for my $related_object (@$relations) {
        # Two log entries: one for each "direction" of the relationship
        $self->schema->resultset('Logging')->trigger(
            sprintf('object/relation/%s', $action),
            {
                object_uuid => $object->id,

                # Special "case" (heh)
                ($object->type eq 'case')
                    ? (zaak_id => $object->case_number)
                    : (),

                data => {
                    object_label             => "$object",
                    related_object_id        => $related_object->id,
                    related_object_type      => $related_object->type,
                    related_object_type_name => $self->_human_type_name($related_object),
                    related_object_label     => "$related_object",

                    ($related_object->type eq 'case')
                        ? (related_case_id => $related_object->case_number)
                        : ()
                },
            },
        );

        $self->schema->resultset('Logging')->trigger(
            sprintf('object/relation/%s', $action),
            {
                object_uuid => $related_object->id,

                # Special "case" (heh)
                ($related_object->type eq 'case')
                    ? (zaak_id => $related_object->case_number)
                    : (),

                data        => {
                    object_label             => "$related_object",
                    related_object_id        => $object->id,
                    related_object_type      => $object->type,
                    related_object_type_name => $self->_human_type_name($object),
                    related_object_label     => "$object",

                    ($object->type eq 'case')
                        ? (related_case_id => $object->case_number)
                        : (),
                },
            },
        );

        # Force related object update with new modification date.
        $self->touch_object_data($related_object);
    }

    return;
}

sub _human_type_name {
    my $self = shift;
    my ($object) = @_;

    my $prefix = $object->type;
    my ($type_object) = $self->_search(
        $self->new_resultset,
        'type',
        { prefix => $prefix }
    );

    if ($type_object) {
        return $type_object->name;
    }
    elsif (exists OBJECT_TYPE_NAME_MAP->{$object->type}) {
        return OBJECT_TYPE_NAME_MAP->{$object->type};
    }

    return $object->type;
}

=head2 assert_unique_object

=head3 DESCRIPTION

Asserts whether or not an object is allowed to be saved in the database. If we already have an object with a unique attribute we are not allowed to save another object which has the same value for that attribute.

=head3 SYNOPSIS

    $object_model->assert_unique_object($object);

=head3 ARGUMENTS

=over

=item object

=back

=head3 RETURNS

Dies in case of failure, returns true otherwise.

=cut

sub assert_unique_object {
    my ($self, $object) = @_;

    my @attr = grep { $_->unique && $_->has_value } $object->attribute_instances;
    return 1 if !@attr;

    my $uniq = {  map { $_->name => $_->value } @attr };
    my @objects = $self->search($object->type, $uniq);

    foreach (@objects) {
        next if ($object->has_id && $object->id eq $_->{id});
        throw(
            'object/unique/constraint',
            "Object unique constraint violation: "  . join(", ", map { "$_ => $uniq->{$_}" } keys %$uniq),
            $uniq
        );
    }
    return 1;
}

=head2 touch_object_data

Updates the L<Zaaksysteem::Schema::ObjectData/date_modified> field for the
given object.

=cut

sig touch_object_data => 'Zaaksysteem::Object';

sub touch_object_data {
    my $self = shift;
    my $object = shift;

    unless ($object->has_id) {
        throw('object/model/touch/object_not_in_storage', sprintf(
            'Attempted to touch (update date_modified) for "%s", but the object is not in storage',
            $object
        ));
    }

    # In place update of date_modified for the object, no need to
    # fetch/inflate/save
    $self->new_resultset->search({ uuid => $object->id })->update({
        date_modified => \'statement_timestamp()'
    });

    return;
}

=head2 update_object_data

This internal-ish helper method updates an existing
L<ObjectData|Zaaksysteem::Backend::Object::Data::Component> instance and syncs
the changes to the database. It returns the updated row object.

    my $object_data_row = $c->model('Object')->update_object_data($object);

=cut

sub _map_object_values {
    my ($self, $object) = @_;
    return { map { $_->name => $_ } $object->attribute_instances };
}

sub update_object_data {
    my $self = shift;
    my $object = shift;

    # If you don't get a result, please check the ACL items
    my $object_data = $self->rs->find($object->id);

    unless($object_data) {
        throw(
            'object/save/update',
            sprintf('Unable to save %s, could not find referenced object_data row %s', $object, $object->id),
            $object
        );
    }

    #$self->log->trace(sprintf('Updating object "%s"', $object // '<undefined>'));

    $object_data->object_class($object->type);

    $object_data->properties({ values => $self->_map_object_values($object) });

    $object_data->load_text_vector(
        $object->index_attributes
    );

    if($object->does('Zaaksysteem::Object::Roles::Type')) {
        $object_data->class_uuid($object->type_reference->id);
    }

    # If we're dealing with a secure object, we will need to update the ACL
    # tables as well
    if ($object->does('Zaaksysteem::Object::Roles::Security') && $object->update_security_rules) {
        # First we clean the existing ACL rows
        $object_data->object_acl_entries->delete_all;

        # Then we re-write all ACLs as the object defines them
        for my $rule ($object->all_rules) {
            my %args = %{ $rule };

            # Safeguard for ZS-4269, this is probably safe to remove by
            # the time you read this.
            delete $args{ verdict };

            my $entity = delete $args{ entity };
            my %security_identity = $entity->security_identity;

            for my $type (keys %security_identity) {
                $object_data->object_acl_entries->create({
                    object_uuid => $object_data->uuid,
                    entity_type => $type,
                    entity_id   => $security_identity{ $type },
                    %args
                });
            }
        }
    }

    return $object_data->update;
}

=head2 create_object_data

This internal-ish helper method creates a new
L<ObjectData|Zaaksysteem::Backend::Object::Data::Component> instance and syncs
it to the database. It returns the created row object.

    my $object_data_row = $c->model('Object')->create_object_data($object);

This method will also automatically create ACL entries for the current user,
if set in the model instance. It will grant the C<read>, C<write>, and
C<delete> permissions to the security identity returned by the L</user>.

Also, if the object being saved does the
L<Zaaksysteem::Object::Roles::Security>-role, this method will create ACL
entries for all L<security roles|Zaaksysteem::Object::Roles::Security/rules>
the object references. This last behavior does B<not> require the L</user>
attribute to be set.

=cut

sub create_object_data {
    my $self = shift;
    my $object = shift;

    my %data = (
        object_class => $object->type,
        text_vector => join(' ', $object->index_attributes),
        properties => { values => $self->_map_object_values($object), },
    );

    $self->log->trace(sprintf('Creating object "%s"', $object));

    if($object->does('Zaaksysteem::Object::Roles::Type')) {
        $data{ class_uuid } = $object->type_reference->id;
    }

    my $object_data = $self->new_resultset->create(\%data);

    $object_data->load_text_vector($object->index_attributes);
    $object_data = $object_data->update;

    return $object_data unless $object->does('Zaaksysteem::Object::Roles::Security');

    # Automagically grant the current user basic rights on the new object.
    if ($self->has_user) {
        $object->permit($self->user, $object->capabilities);
    }

    for my $rule ($object->all_rules) {
        my %args = %{ $rule };

        # Safeguard for ZS-4269, this is probably safe to remove by
        # the time you read this.
        delete $args{ verdict };

        my $entity = delete $args{ entity };
        my %security_identity = $entity->security_identity;

        for my $type (keys %security_identity) {
            $object_data->object_acl_entries->create({
                object_uuid => $object_data->uuid,
                entity_type => $type,
                entity_id   => $security_identity{ $type },
                %args
            });
        }
    }

    return $object_data;
}

=head2 find_position

This method returns a L<Zaaksysteem::Object::SecurityIdentity::Position>
instance after validatating the provided argument(s).

    my $entity = $model->find_position('123|987');

    # Or

    my $entity = $model->find_position('123', '987');

=cut

sub find_position {
    my $self = shift;

    # Allow both find_position('123|987') and find_position('123', '987')
    # calling styles
    my ($group_id, $role_id) = map { split m[\|] } @_;
    my $schema = $self->new_resultset->result_source->schema;

    unless(defined $group_id && defined $role_id) {
        throw('object/model/find_position', sprintf(
            'Unable to determine ou and role id from argument(s) "%s"',
            join ", ", @_
        ));
    }

    my $role_count      = $schema->resultset('Roles')->search(
        {id => $role_id}
    )->count;

    my $group_count     = $schema->resultset('Groups')->search(
        {id => $group_id}
    )->count;

    # If ou_id is provided, but is false-y (0), assume role identity
    unless ($group_id) {
        return unless $role_count;

        return Zaaksysteem::Object::SecurityIdentity::Role->new(
            role_id => $role_id
        );
    }

    # If role_id is provided, but false-y (0), assume group identity
    unless ($role_id) {
        return unless $group_count;

        return Zaaksysteem::Object::SecurityIdentity::Group->new(
            ou_id => $group_id
        );
    }

    # Default case, assume position
    return unless $role_count && $group_count;

    return Zaaksysteem::Object::SecurityIdentity::Position->new(
        ou_id => $group_id,
        role_id => $role_id
    );
}

=head2 mutate

Execute a mutation row against the model.

    my $mutation_spec = $c->model('Object')->mutate($mutation_row);

This method returns a simple hashref with information about the executed
mutation. In the case of a creation mutation, it looks like this;

    {
        mutation_type => 'create',
        object_uuid => UUID_of_new_instance,
        object_label => new_instance->TO_STRING,
        changes => [
            { field => "attr1", new_value => "new_value_attr1" },
            { field => "foo", new_value => "bar" },
            { field => "aap", new_value => "nootmies" },
        ]
    }

In the case of update mutations, the objects in the C<changes> key will also
have the C<old_value> field.

    {
        mutation_type => 'update',
        object_uuid => UUID_of_object,
        object_label => object->TO_STRING,
        changes => [
            { field => "foo", old_value => "bar", new_value => "baz" },
            { field => "qux", old_value => "quux", new_value => "corge" }
        ]
    }


And deletions, something akin to this;

    {
        mutation_type => 'delete',
        object_uuid => UUID_of_deleted_object,
        object_label => deleted_object->TO_STRING
    }

=cut

sub mutate {
    my $self = shift;
    my $mutation = shift;

    unless(blessed $mutation && $mutation->isa('Zaaksysteem::Schema::ObjectMutation')) {
        throw('object/model/mutate', 'Expecting a ObjectMutation row, got something else');
    }

    my $retval = {
        mutation_type => $mutation->type,
    };

    my %attr_name_value_map;

    for my $attr_namespaced_name (keys %{ $mutation->values }) {
        my ($namespace, $attr_name) = split m[\.], $attr_namespaced_name;

        next unless $namespace eq 'attribute';

        $attr_name_value_map{ $attr_name } = $mutation->values->{ $attr_namespaced_name };
    }

    my $lock_object = $self->inflate_from_row($mutation->lock_object_uuid);

    if($mutation->type eq 'create') {
        my %args = map {
            $_ => $attr_name_value_map{ $_ }
        } grep {
            defined $attr_name_value_map{ $_ }
        } keys %attr_name_value_map;

        my $new_instance = $self->find_type_meta($mutation->object_type)->new_object(
            \%args
        );

        $new_instance->relate($lock_object, owner_object_id => $lock_object->id);

        my $object = $self->save_object(object => $new_instance);

        my @files = map { @{ $_->value } }
                   grep { $_->attribute_type eq 'file' && $_->has_value }
                        $object->attribute_instances;

        my $case = $mutation->lock_object_uuid->get_source_object;

        for my $file (@files) {
            my $filestore = $case->result_source->schema->resultset('Filestore')->find({ uuid => $file->{ uuid } });

            $case->files->file_create({
                name => $file->{ original_name },
                db_params => {
                    filestore_id => $filestore->id,
                    case_id => $mutation->lock_object_uuid->object_id
                }
            });
        }

        my @changes;

        for my $attr_name (keys %{ $mutation->values }) {
            push @changes, {
                field => $attr_name,
                new_value => $mutation->values->{ $attr_name }
            };
        }

        $retval->{ object_uuid } = $object->id;
        $retval->{ object_label } = $object->TO_STRING;
        $retval->{ object_type } = $object->type;
        $retval->{ changes } = \@changes;
    } elsif($mutation->type eq 'update') {
        my $instance = $self->inflate_from_row($mutation->object_uuid);

        my @changes;

        my @old_files = map { @{ $_->value } }
                   grep { $_->attribute_type eq 'file' && $_->has_value }
                        $instance->attribute_instances;

        for my $attr_name (keys %attr_name_value_map) {
            my $attr = $instance->meta->find_attribute_by_name($attr_name);
            my $value = $attr_name_value_map{ $attr_name };

            push @changes, {
                field => $attr_name,
                old_value => $attr->get_value($instance),
                new_value => $value
            };

            if (defined $value) {
                $attr->set_value($instance, $value);
            } else {
                $attr->clear_value($instance);
            }
        }

        $instance->relate($lock_object, owner_object_id => $lock_object->id);

        my $object = $self->save_object(object => $instance);

        my @files = map { @{ $_->value } }
                   grep { $_->attribute_type eq 'file' && $_->has_value }
                        $object->attribute_instances;

        my $case = $mutation->lock_object_uuid->get_source_object;

        for my $file (@files) {
            if (first { $file->{uuid} eq $_->{uuid} } @old_files) {
                next;
            }

            my $id = $file->{id}
                // $case->result_source->schema->resultset('Filestore')
                ->find({ uuid => $file->{uuid} })->id;

            $case->files->file_create({
                name      => $file->{original_name} // $file->{filename},
                db_params => {
                    filestore_id => $id,
                    case_id      => $mutation->lock_object_uuid->object_id
                }
            });
        }

        $retval->{ object_uuid } = $object->id;
        $retval->{ object_label } = $object->TO_STRING;
        $retval->{ object_type } = $object->type;
        $retval->{ changes } = \@changes;
    } elsif($mutation->type eq 'delete') {
        my $instance = $self->inflate_from_row($mutation->object_uuid);

        $self->delete_by_uuid($instance->id);

        $retval->{ object_label } = $instance->TO_STRING;
        $retval->{ object_type } = $instance->type;
    } elsif($mutation->type eq 'relate') {
        my $instance = $self->inflate_from_row($mutation->object_uuid);

        $instance->relate($lock_object, owner_object_id => $lock_object->id);

        $self->save_object(object => $instance);

        $retval->{ object_uuid } = $instance->id;
        $retval->{ object_label } = $instance->TO_STRING;
        $retval->{ object_type } = $instance->type;
    }

    return $retval;
}

=head2 validate_mutation

Checks if a mutation is complete by applying the supposed mutation to an
in-memory object instance.

This method will just return true-ish if everything checks out, and the
mutations *should* apply cleanly when passed to L</mutate>. A failure is
signalled via exceptions, most likely a Moose type constraint exception.

=cut

sig validate_mutation => 'Zaaksysteem::Backend::Object::Mutation::Component';

sub validate_mutation {
    my $self = shift;
    my $mutation = shift;

    my %attr_name_value_map;

    for my $attr_namespaced_name (keys %{ $mutation->values }) {
        my ($namespace, $attr_name) = split m[\.], $attr_namespaced_name;

        next unless $namespace eq 'attribute';

        $attr_name_value_map{ $attr_name } = $mutation->values->{ $attr_namespaced_name };
    }

    if ($mutation->type eq 'create') {
        my %args = map {
            $_ => $attr_name_value_map{ $_ }
        } grep {
            defined $attr_name_value_map{ $_ }
        } keys %attr_name_value_map;

        return $self->find_type_meta($mutation->object_type)->new_object(
            \%args
        );
    }

    if ($mutation->type eq 'update') {
        my $instance = $self->inflate_from_row($mutation->object_uuid);

        for my $attr_name (keys %attr_name_value_map) {
            my $attr = $instance->meta->find_attribute_by_name($attr_name);
            my $value = $attr_name_value_map{ $attr_name };

            if (defined $value) {
                $attr->set_value($instance, $value);
            } else {
                $attr->clear_value($instance);
            }
        }

        return $instance;
    }

    return 1;
}

=head2 process

This 'after' method modifier on the delegated
L<Zaaksysteem::Object::Environment> method runs through the built up stack
after a process has finished and looks for objects to save. It modifies the
stack so that save_object actions don't percolate up the call stack.

This is a horrible way of approaching handling actions. A main action handler
should call back to this model instead, but no such code is yet in place.

=cut

after process => sub {
    my $self = shift;

    my @new_stack;
    my @subrefs;

    my %objects_to_save;

    for my $action (@{ $self->object_environment->get('stack') }) {
        if ($action->{ name } eq 'log_event') {
            next;
        }

        if ($action->{ name } eq 'save_object') {
            # Nasty *nasty* unpacking
            my $object = $action->{ args }[0]{ object };

            push @subrefs, sub {
                $self->save_object(object => $action->{ args }[0]{ object });
            };

            next;
        }

        if ($action->{ name } eq 'interface_trigger') {
            my ($module_name, $trigger, @args) = @{ $action->{ args } };

            my $interfaces = $self->schema->resultset('Interface');
            my $interface = $interfaces->find_by_module_name($module_name);

            push @subrefs, sub {
                $interface->process_trigger($trigger, @args);
            };

            next;
        }

        # Push to replacement stack by default, allowing upper levels
        # to handle relevant actions.
        push @new_stack, $action;
    }

    # Use the internal _set to prevent immutability checks.
    $self->object_environment->_set('stack', \@new_stack);

    return unless scalar @subrefs;

    # Either all updates succeed, or none do.
    $self->schema->txn_do(sub {
        $_->() for @subrefs;
    });

    return;
};

=head2 build_object_environment

Builder for the L</object_environment> attribute. This method will produce a
new L<Zaaksysteem::Object::Environment> tree of 3 levels. The root level will
be the default instantiation of an environment object. The second level will
contain symbols exported by this class, C<user>, C<save>, C<load>. The last
level is build by mapping over all classes found in the
C<Zaaksysteem::Object::Process::> namespace.

All plugins from that namespace will have their symbols imported in one common
environment, so if two collections export processes under the same name a
run-time exception is thrown by the environment class.

=cut

sub build_object_environment {
    my $self = shift;

    my $model_environment = Zaaksysteem::Object::Environment->new->new_child(
        user => $self->user,

        save => Zaaksysteem::Object::Process->new(
            description => 'Save object',
            environment_profile => {
                object => 'Zaaksysteem::Object',
                stack => 'ArrayRef'
            },
            implementation => sub {
                my ($process, $env) = @_;

                my $object = $env->get('object');

                # This will get fancier with time.
                push @{ $env->get('stack') }, $process->action('save_object', {
                    object => $object,
                    env => $env
                });

                return $object;
            }
        ),

        load => Zaaksysteem::Object::Process->new(
            description => 'Load object by reference',
            environment_profile => {
                ref => role_type('Zaaksysteem::Object::Reference')
            },
            implementation => sub {
                my ($process, $env) = @_;

                return $self->get(ref => $env->get('ref'));
            }
        )
    );

    # Early return, no process collections yet exist, and the
    # Module::Pluggable import is broken
    return $model_environment;

    return $model_environment->new_child(
        map { $_->processes } $self->object_process_collections
    );
}

=head2 export_single

Export a single object in an export file.

=cut

define_profile export_single => (
    optional => {
        tar_handle => 'Archive::Tar::Stream',
        object     => 'Zaaksysteem::Object',
        metadata   => 'HashRef',
    },
);

sub export_single {
    my $self = shift;
    my %params = @_;

    my $args = assert_profile(\%params)->valid;

    $self->_add_meta_json(
        $args->{tar_handle}, { %{$args->{metadata}}, export_type => 'single'},
    );

    $self->_add_readme_to_export($args->{tar_handle});

    $self->_add_object_to_export($args->{tar_handle}, $args->{object});

    return;
}

=head2 export_multiple

Export a ResultSet of objects to an export file.

=cut

define_profile export_multiple => (
    optional => {
        tar_handle => 'Archive::Tar::Stream',
        resultset  => 'Zaaksysteem::Backend::Object::Data::ResultSet',
        metadata   => 'HashRef',
    },
);

sub export_multiple {
    my $self = shift;
    my %params = @_;

    my $args = assert_profile(\%params)->valid;

    $self->_add_meta_json($args->{tar_handle},
        { %{ $args->{metadata} }, export_type => 'query' },
    );

    $self->_add_readme_to_export($args->{tar_handle});

    while (my $row = $args->{resultset}->next) {
        my $object = $self->inflate_from_row($row);
        $self->_add_object_to_export($args->{tar_handle}, $object);
    }

    return;
}

sub _add_meta_json {
    my $self = shift;
    my $tar  = shift;
    my $data = shift;

    my $meta_json = JSON->new->pretty->canonical->encode({
        version     => '1',
        time        => DateTime->now()->set_time_zone('UTC')->iso8601 . 'Z',
        %{$data},
    });

    $self->_add_scalar_to_export($tar, "meta.json", $meta_json);
}

=head2 export_rs_as_object

Export a ResultSet, whoms components support the C<as_object> method to
a streaming tar.

=cut

define_profile export_rs_as_object => (
    required => {
        tar_handle => 'Archive::Tar::Stream',
        resultset  => 'DBIx::Class::ResultSet',
        metadata   => 'HashRef',
    },
);

sub export_rs_as_object {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    $self->_add_meta_json(
        $args->{tar_handle}, $args->{metadata},
    );

    while(my $r = $args->{resultset}->next) {
        $self->_add_object_to_export($args->{tar_handle}, $r->as_object);
    }
    return;
}

sub _add_readme_to_export {
    my $self = shift;
    my ($tar) = @_;

    my $parser = Pod::Text->new(loose => 1);
    my $readme = pod_where({-inc => 1}, "Zaaksysteem::Object::ExportFile");

    my $text;
    $parser->output_string(\$text);
    $parser->parse_file($readme);

    $self->_add_scalar_to_export($tar, "README.txt", $text);

    return;
}

sub _add_object_to_export {
    my $self = shift;
    my ($tar, $object) = @_;

    my $object_id = $object->id;

    my $files;
    my $events;
    my $result;
    my $relations;

    if ($object->type eq 'case') {
        # Case is a special case. It's not fully "object" yet, so we need to
        # JSONify the database row, not the in-memory object.
        $object = $self->schema->resultset('ObjectData')->find($object_id);

        # Only case objects can have files attached to them for now.
        $files = $object->documents;

        $events = $self->schema->resultset('Logging')->search(
            { zaak_id => $object->object_id },
            { order_by => { -asc => 'created' } }
        );

        if ($object->has_object_attribute('case.result')) {
            $result = $self->schema->resultset('ZaaktypeResultaten')->search({
                resultaat => $object->get_object_attribute('case.result')->value,
                zaaktype_node_id => $object->get_object_attribute('case.casetype.node.id')->value
            })->first;
        }

        $relations = [ $object->relationships_data ];
    }
    elsif ($object->type eq 'event') {
        # Events are not stored in the object data table and don't have
        # an UUID as object id. Use the legacy ID instead.
        $object_id = $object->event_id;
    }

    my $object_json = do {
        my $json = JSON->new->utf8->pretty->allow_nonref->allow_blessed->convert_blessed->canonical();

        no warnings 'redefine';
        local *DateTime::TO_JSON = sub { shift->iso8601 };

        my $data = $object->TO_JSON;

        if ($events) {
            $data->{ events } = [ map { $_->TO_JSON } $events->all ];
        }

        if ($result) {
            $data->{ result } = { $result->get_columns };
        }

        if ($relations) {
            $data->{ relationships } = $relations;
        }

        $json->encode($data);
    };

    $self->_add_scalar_to_export($tar, "data/$object_id.json", $object_json);

    if ($files) {
        while(my $file = $files->next) {
            $self->_add_attachment_to_export($tar, $file, $object_id);
        }
    }

    return;
}

sub _add_scalar_to_export {
    my $self = shift;
    my ($tar, $filename, $scalar) = @_;

    my $fh = IO::Scalar->new(\$scalar);

    $tar->AddFile($filename, length($scalar), $fh, mode => 0644);

    $fh->close();

    return;
}

sub _add_attachment_to_export {
    my $self = shift;
    my ($tar, $file, $object_id) = @_;

    my $filestore = $file->filestore_id;
    my $path      = $filestore->get_path();
    my $file_uuid = $filestore->uuid;

    my $case_document_ids = $file->case_documents->search(
        {},
        {
            prefetch => {
                'case_document_id' => 'bibliotheek_kenmerken_id'
            },
        }
    );
    my $attributes = [
        map {
            "attribute." . $_->case_document_id->bibliotheek_kenmerken_id->magic_string
        } $case_document_ids->all
    ];

    my $events = $self->schema->resultset('Logging')->search({
        component => 'document',
        component_id => $file->id
    });

    my $file_json = do {
        my $json = JSON->new->utf8->pretty->allow_nonref->allow_blessed->convert_blessed->canonical();

        no warnings 'redefine';
        local *DateTime::TO_JSON = sub { shift->iso8601 };

        $json->encode({
            path       => sprintf('/%s', $file->filepath),
            attributes => $attributes,
            metadata   => $file,
            events     => [ map { $_->TO_JSON } $events->all ]
        });

    };

    $self->_add_scalar_to_export($tar, "data/$object_id/$file_uuid.json", $file_json);

    my $fh = $filestore->read_handle();
    $tar->AddFile("data/$object_id/$file_uuid.blob", -s $fh, $fh, mode => 0644);

    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
