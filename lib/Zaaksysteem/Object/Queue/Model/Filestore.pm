package Zaaksysteem::Object::Queue::Model::Filestore;
use Moose::Role;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::Queue::Model::Filestore - Filestore queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 filestore_replicate

Process the queue-item to replicate a filestore object to all configured
storage locations.

=cut

sub filestore_replicate {
    my $self = shift;
    my $item = shift;

    my $schema = $item->result_source->schema;
    my $data   = $item->data;

    my $id = $data->{filestore_id};
    $schema->txn_do(sub {
        my $filestore = $schema->resultset('Filestore')->search(
            { id => $id },
            { for => 'update' },
        );

        while (my $fs = $filestore->next) {
            $fs->replicate();
        }
    });

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
