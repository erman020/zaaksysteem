package Zaaksysteem::Object::Queue::Model::Casetype;

use Moose::Role;

use BTTW::Tools;

=head2 touch_casetype

Handler for asynchronous case touches.

=cut

define_profile touch_casetype => (
    required => {
        id => 'Int',
    }
);

sub touch_casetype {
    my $self = shift;
    my $item = shift;

    my $args = assert_profile($item->data)->valid;

    my $casetype = $self->schema->resultset('Zaaktype')->find($args->{id});
    $casetype->_sync_object($self->object_model);
    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
