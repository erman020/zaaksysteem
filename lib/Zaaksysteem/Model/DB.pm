package Zaaksysteem::Model::DB;

use Moose;
use Scalar::Util;

use Zaaksysteem::Zaken::DelayedTouch;


extends 'Catalyst::Model::DBIC::Schema';

with 'Catalyst::Component::InstancePerContext';

__PACKAGE__->config(
    schema_class => 'Zaaksysteem::Schema',
);

sub build_per_context_instance {
    my $self    = shift;
    my $c       = shift;

    my $new = (ref($self) ? $self->new(%$self) : $self->new);

    my $customer_instance   = $c->customer_instance;

    ### Database connection per request:
    unless ($c->stash->{__dbh}) {
        if (ref $ENV{ZAAKSYSTEEM_SCHEMA}) {
            $c->stash->{__dbh} = $ENV{ZAAKSYSTEEM_SCHEMA};
            $c->log->debug('Database OVERRIDE: ' . $ENV{ZAAKSYSTEEM_SCHEMA}->storage->connect_info->[0]->{dsn}) if $c->debug;
        } else {
            $c->stash->{__dbh} = $self->schema->connect(
                {
                    %{ $customer_instance->{ 'Model::DB' }{ connect_info } },

                    auto_savepoint => 1,
                }
            );
        }

        $new->schema->default_resultset_attributes->{ delayed_touch } = Zaaksysteem::Zaken::DelayedTouch->new;
    }

    $new->schema(
        $c->stash->{__dbh}
    );

    $new->schema->default_resultset_attributes->{ log } = $c->log;
    $new->schema->default_resultset_attributes->{ config } = $c->config;
    $new->schema->default_resultset_attributes->{ cache } = $c->zs_cache;
    $new->schema->default_resultset_attributes->{ queue_items } = [];

    ### Accessors on schema object
    $new->schema->catalyst_config($c->config);
    $new->schema->customer_instance($customer_instance);
    $new->schema->cache($c->zs_cache);

    # A way for the schema to get the latest BAG model, without it leading to
    # an infinite loop
    $new->schema->bag_model_builder(sub { return $c->model('BAG'); });

    my $betrokkene_stash    = {};
    $betrokkene_stash->{$_} = $c->stash->{$_} for qw/
        order
        order_direction
        paging_rows
        paging_page
        paging_total
        paging_lastpage
        paging_total
        paging_lastpage
    /;

    $new->schema->betrokkene_pager($betrokkene_stash);

    # if (ref($c)) {
    #     if ($c->user_exists) {
    #         $new->schema->current_user(
    #             $new->schema->default_resultset_attributes->{current_user} = $c->user
    #         );
    #     }
    # }

    # if (ref($c) && $c->user_exists) {
    #     $attributes->{current_user} = $c->user;
    #     $attributes->{current_user_ou_ancestry} = $self->get_current_user_ou_ancestry($c);
    # }

    foreach my $attribute (qw/log config cache/) {
        if(!Scalar::Util::isweak($new->schema->default_resultset_attributes->{$attribute})) {
            Scalar::Util::weaken($new->schema->default_resultset_attributes->{$attribute});
        }
    }

    return $new;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 build_per_context_instance

TODO: Fix the POD

=cut

