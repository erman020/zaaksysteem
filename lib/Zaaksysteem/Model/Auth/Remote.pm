package Zaaksysteem::Model::Auth::Remote;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

use BTTW::Tools;

__PACKAGE__->config(
    class => 'Zaaksysteem::Backend::Sysin::Auth::Remote',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::Auth::Remote - Catalyst model factory for
L<Zaaksysteem::Backend::Sysin::Auth::Remote>

=head1 SYNOPSIS

    my $model = $c->model('Auth::Remote');

=head1 METHODS

=head2 prepare_arguments

Prepares the arguments for L<<Zaaksysteem::Backend::Sysin::Auth::Remote->new>>.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    my $config = $c->customer_instance;

    throw(
        'model/auth/remote/not_allowed',
        'This instance does not allow sending of login requests to other instances.',
    ) unless $config->{allow_outgoing_login_requests};

    throw(
        'model/auth/remote/no_ca_cert',
        'No CA certificate configured',
    ) unless exists $config->{'services_ca_cert'};

    throw(
        'model/auth/remote/no_platform_keys',
        'No platform keys configured',
    ) unless exists $config->{'cloud_platform_keys'};

    return {
        hostname      => $c->config->{'instance_hostname'},
        server_ca     => $config->{'services_ca_cert'},
        platform_keys => $config->{'cloud_platform_keys'},
        logging_rs    => $c->model('DB::Logging'),
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
