package Zaaksysteem::Model::BAG;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';
with 'MooseX::Log::Log4perl';

use BTTW::Tools;
use Search::Elasticsearch;
use Zaaksysteem::Geo::BAG::Model;
use Zaaksysteem::Geo::BAG::Connection::ES;
use Zaaksysteem::Geo::BAG::Connection::Spoof;

=head1 NAME

Zaaksysteem::Model::BAG - Catalyst model factory for L<Zaaksysteem::Geo::BAG::Model>.

=head1 SYNOPSIS

    my $rs_model = $c->model('BAG');

=cut

__PACKAGE__->config(
    class       => 'Zaaksysteem::Geo::BAG::Model',
    constructor => 'new',
);

=head1 METHODS

=head2 prepare_arguments

Prepares the arguments to create a new L<Zaaksysteem::Geo::BAG::Model> instance.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    my $spoof_mode         = $c->model('DB::Config')->get("bag_spoof_mode");
    my $priority_gemeentes = $c->model('DB::Config')->get("bag_priority_gemeentes") || [];
    my $local_only         = $c->model('DB::Config')->get("bag_local_only");

    $priority_gemeentes = [
        grep { defined($_) && length($_) } @$priority_gemeentes
    ];

    my $search_connection;
    if ($spoof_mode) {
        $self->log->trace("Creating new BAG connection - Spoof");
        $search_connection = Zaaksysteem::Geo::BAG::Connection::Spoof->new(
            home               => $c->config->{home},
            priority_gemeentes => $priority_gemeentes,
            priority_only      => $local_only,
        );
    } else {
        $self->log->trace("Creating new BAG connection - Elasticsearch");

        $search_connection = Zaaksysteem::Geo::BAG::Connection::ES->new(
            es                 => $c->model('Elasticsearch', { cluster => 'BAG' }),
            priority_gemeentes => $priority_gemeentes,
            priority_only      => $local_only,
        );
    }

    return {
        connection         => $search_connection,
    };
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
