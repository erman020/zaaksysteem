package Zaaksysteem::Types;
use warnings;
use strict;

use Zaaksysteem::Constants qw(
    SUBJECT_TYPES
    VALID_FQDN
    ZAAKSYSTEEM_CONSTANTS
    ZAAKSYSTEEM_OPTIONS
    ZAAKSYSTEEM_CSS_TEMPLATES
    ZAAKSYSTEEM_CSS_TEMPLATES_OLD
);
use Zaaksysteem::Object::ConstantTables qw(MUNICIPALITY_TABLE COUNTRY_TABLE);

use Types::Serialiser;
use MooseX::Types -declare => [qw(
    ACLCapability
    ACLCapabilityList
    ACLEntityType
    ACLScope
    AddressNumber
    AddressNumberLetter
    AddressNumberSuffix
    ArrayRefFQDN
    ArrayRefIPs
    ArrayRefOfHashRefs
    Betrokkene
    BetrokkeneBedrijf
    Boolean
    BSN
    CaseConfidentiality
    CaseResult
    ChannelOfContact
    CompanyCocLocationNumber
    CompanyCocNumber
    CompanyCocRSIN
    CountryCode
    CustomerTemplate
    CustomerTemplateCurrent
    CustomerType
    Datestamp
    DateTimeObject
    EmailAddress
    FQDN
    HashedPassword
    Host
    IntervalStr
    IPv4
    IPv6
    JSON_XS_Boolean
    JSONBoolean
    JSONNum
    KvK
    MobileNumber
    MunicipalityCode
    NLZipcode
    NonEmptyStr
    ObjectSubjectType
    Otap
    PackageElementStr
    PersonPersonalNumber
    PersonPersonalNumberA
    PositiveNumber
    RelatedCaseType
    RelatedCaseTypes
    RemoteSearchModuleName
    SequenceNumber
    SoftwareVersions
    Store
    SubjectType
    TelephoneNumber
    Timestamp
    TransactionDirection
    URIx
    UUID
    ValueType
    Zipcode
    ZSFQDN
    ZSNetworkACL
    ZSNetworkACLs
)];

use BTTW::Tools qw(elfproef tombstone);
use DateTime::Format::ISO8601 qw[];
use Email::Valid;
use JSON::XS;
use List::Util 'any';
use MooseX::Types::Moose qw(Str Int Num Bool ArrayRef HashRef Item);
use Number::Phone;
use Zaaksysteem::BR::Subject::Constants ':remote_search_module_names';

=head1 NAME

Zaaksysteem::Types - Custom types for Zaaksysteem

=head1 SYNOPSIS

    package MyClass;
    use Moose;
    use Zaaksysteem::Types qw(TYPE1 TYPE2);

    has attr => (
        isa => TYPE1,
        is => 'ro',
    );

=head1 AVAILABLE TYPES

=head2 PositiveNumber

A positive number

=cut

subtype PositiveNumber,
    as Num,
    where { $_ > 0 },
    message { "The number you provided, $_, was not a positive number" };

=head2 SequenceNumber

C<sequence_number> type, which must be a natural/counting number.

=cut

subtype SequenceNumber,
    as Int,
    where { $_ > 0 },
    message {
        sprintf('Invalid sequence number "%s", not a natural number', $_)
    };

subtype BSN,
    as Num,
    where {
        if (length($_) < 8) {
            return 0;
        }
        elfproef(sprintf("%09d", $_), 1);
    },
    message { "The number your provided, $_, was not a BSN" };

subtype KvK,
    as Num,
    where {
        if (length($_) < 7) {
            return 0;
        }
        return 1;
    },
    message { "The number your provided, $_, is not a KvK number" };

=head2 ZSNetworkACL

A ZSNetworkACL type

=cut

subtype ZSNetworkACL, as Str,
    where {
        my $str = shift;
        # IPv4/IPv6 regexp are coming from https://github.com/waterkip/regexp-ip which is not yet on CPAN. I should probably do that one day.
        my $ipv4_re = '(?^:([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5])))';
        my $ipv6_re = '(?^:(?::(?::[0-9a-fA-F]{1,4}){0,5}(?:(?::[0-9a-fA-F]{1,4}){1,2}|:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5])))|[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}|:)|(?::(?:[0-9a-fA-F]{1,4})?|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))))|:(?:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|[0-9a-fA-F]{1,4}(?::[0-9a-fA-F]{1,4})?|))|(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|:[0-9a-fA-F]{1,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){0,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,2}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,3}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|::))';
        my $re = qr/^(?:$ipv4_re|$ipv6_re)(?:|\s+(?:.+))?$/;
        return $str =~ /$re/;
    };

=head2 ZSNetworkACLs

An array ref of L<ZSNetworkACL> types

=cut

subtype ZSNetworkACLs, as ArrayRef[ZSNetworkACL];

=head2 FQDN

A type (subtype of Str) that only allows syntactically valid fully qualified domain names.

=cut

subtype FQDN,
    as Str,
    where {
        return VALID_FQDN->($_);
    };

=head2 ZSFQDN

A type (subtype of Str) that only allows syntactically valid fully qualified domain names which do not end with zaaksysteem.<something>

=cut

subtype ZSFQDN, as FQDN, where { $_ !~ m/zaaksysteem\.[a-z]+$/ };

=head2 ArrayRefFQDN

A type subtype which defines an array reference of FQDN types.

=cut

subtype ArrayRefFQDN, as ArrayRef[FQDN];

=head2 HOST

A hostname, the ones before a 'dot'

=cut

subtype Host,
    as Str,
    where {
        return $_ =~ /^[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9]$/;
    };

=head2 CustomerType

A type (subtype of Str) that only allows syntactically valid Customer Types

=over

=item * commercial

=item * government

=item * commercieel

=item * overheid

=back

=cut

subtype CustomerType, as enum([qw(commercial government overheid commercieel)]),
    message { "'$_' is not a valid Customer Type" };

=head2 CustomerTemplate

A type (subtype of Str) that only allows syntactically valid Customer templates.

See L<Zaaksysteem::Constants/ZAAKSYSTEEM_CSS_TEMPLATES> and
L<Zaaksysteem::Constants/ZAAKSYSTEEM_CSS_TEMPLATES_OLD>.

=cut

subtype CustomerTemplate,
    as enum([
        @{ ZAAKSYSTEEM_CSS_TEMPLATES() },
        @{ ZAAKSYSTEEM_CSS_TEMPLATES_OLD() },
    ]),
    message { "'$_' is not a valid Customer Template" };

=head2 CustomerTemplateCurrent

A type (subtype of Str) that only allows syntactically valid Customer templates.

See L<Zaaksysteem::Constants/ZAAKSYSTEEM_CSS_TEMPLATES>

=cut

subtype CustomerTemplateCurrent, as enum(ZAAKSYSTEEM_CSS_TEMPLATES),
    message { "'$_' is not a valid Customer Template" };


=head2 SoftwareVersions

The controlpanel version to use for this instance

=over

=item * master

=back

=cut

enum SoftwareVersions, [qw[master]];


=head2 Betrokkene

A type (subtype of Str) that only allows syntactically valid betrokkene ID's

=cut

subtype Betrokkene, as Str,
    where {
        return $_ =~ /^betrokkene-(?:natuurlijk_persoon|medewerker|bedrijf)-\d+$/;
    },
    message { "'$_' is not a valid Betrokkene" };


=head2 BetrokkeneBedrijf

A type (subtype of Str) that only allows syntactically valid betrokkene ID's for companies

=cut

subtype BetrokkeneBedrijf, as Str,
    where {
        return $_ =~ /^betrokkene-bedrijf-\d+$/;
    },
    message { "'$_' is not a valid BetrokkeneBedrijf" };

=head2 ChannelOfContact

A type (subtype of Str) that only allows syntactically valid channel of contact (contactkanalen).

=over

=item balie

=item telefoon

=item post

=item email

=item webformulier

=item behandelaar

=item sociale media

=back

=cut

subtype ChannelOfContact, as enum(ZAAKSYSTEEM_CONSTANTS->{contactkanalen}),
    message { "'$_' is not a valid contact channel" };


=head2 Otap

A type (subtype of Str) that only allows syntactically valid OTAP environments.

=over

=item * development (D)

=item * testing (T)

=item * accept (A)

=item * production (P)

=back

=cut

subtype Otap, as enum([qw(development testing accept production)]),
    message { "'$_' is not a valid OTAP type" };

=head2 HashedPassword

A type (subtype of Str) that only allows hashed passwords, which start with their hashing algorithm in curly brackets {}

Allowed algorithms:

=over 4

=item SSHA

Salted HASH

=back

=cut

subtype HashedPassword, as Str,
    where {
        return $_ =~ /^\{SSHA\}.+$/;
    },
    message { "'$_' is not a valid hashed password" };

=head2 IPv4

A type (subtype of Str) that only allows syntactically valid IPv4 addresses.

=cut

subtype IPv4, as Str,
    where {
        my ($ip, $range) = split(/\//, shift);
        return 0 if !$ip;
        my $ipv4_re = '(?^:([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5])))';

        $ipv4_re = qr/^$ipv4_re$/;

        if ($ip =~ /$ipv4_re/ && ( !defined $range or $range =~ /^\d+/ && $range >= 1 && $range <= 32)) {
            return 1;
        }
        return 0;
    },
    message { "$_ is not a valid IPv4 CIDR address" };

=head2 IPv6

A type (subtype of Str) that only allows syntactically valid IPv6 addresses.

=cut

subtype IPv6, as Str,
    where {
        my ($ip, $range) = split(/\//, shift);
        return 0 if !$ip;
        my $ipv6_re = '(?^:(?::(?::[0-9a-fA-F]{1,4}){0,5}(?:(?::[0-9a-fA-F]{1,4}){1,2}|:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5])))|[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}|:)|(?::(?:[0-9a-fA-F]{1,4})?|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))))|:(?:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|[0-9a-fA-F]{1,4}(?::[0-9a-fA-F]{1,4})?|))|(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|:[0-9a-fA-F]{1,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){0,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,2}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,3}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|::))';

        $ipv6_re = qr/$ipv6_re/;
        if ($ip =~ /$ipv6_re/ && ( !defined $range or $range =~ /^\d+/ && $range >= 1 && $range <= 64)) {
            return 1;
        }
        return 0;
    },
    message { "$_ is not a valid IPv6 CIDR address" };

=head2 ArrayRefIPs

An array ref of IPv4 and/or IPv6 addresses

=cut

subtype ArrayRefIPs, as ArrayRef[IPv4|IPv6];

=head2 UUID

A type (subtype of Str) that only allows syntactically valid UUIDs.

=cut

subtype UUID,
    as Str,
    where {
        my $str = shift;
        return $str =~ /^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$/i;
    };

=head2 NonEmptyStr

A type (subtype of Str) that defines a label

=cut

subtype NonEmptyStr,
    as Str,
    where {
        my $str = shift;
        return $str =~ /^\p{XPosixSpace}*(?:\p{XPosixAlnum}|\p{XPosixPunct})+/;
    };

=head2 Boolean

Unfortunatly, the C<Bool> type of Moose will stringify your input, and modules like JSON will see it as a string.

B<example>

    has disabled => (
        is     => 'rw',
        isa    => 'Bool',
        default => 1,
    );

    ### $object->disabled becomes "disabled: '1'"

Except, when using Boolean, it will work as you would expect. It has the EXACT same functionality
as Bool, but will also return the right value to JSON.

    has disabled => (
        is     => 'rw',
        isa    => 'Boolean',
        default => 1,
    );

    ### $object->disabled becomes "disabled: 1"

=cut

## !ref($_) added to correctly find out JSON::XS::Boolean, because it normally stringifies to 0 or 1. But
## when send to JSON, it will inflate to "true" or "false". This could be a good implementation, but we
## chose to send back 1 of 0.

subtype Boolean, as Item,
    where { !ref($_) && (!defined($_) || $_ eq '1' || $_ eq '0' || "$_" eq '1'|| "$_" eq '0' || $_ eq "") };

class_type JSON_XS_Boolean, { class => 'Types::Serialiser::BooleanBase' };
coerce Boolean, from JSON_XS_Boolean, via { if (Types::Serialiser::is_bool($_)) { return ($_ ? 1 : 0) } };

=head2 JSONBoolean

As a bonus, we extended the C<Boolean> function. This one will send back proper true and false values. It will
coerce everything into JSON::XS::true or JSON::XS::false.

B<example>

    has disabled => (
        is     => 'rw',
        isa    => 'JSONBoolean',
        default => 1,
        coerce => 1,
    );

    ### $object->disabled becomes "disabled: true"

    has disabled => (
        is     => 'rw',
        isa    => 'JSONBoolean',
        default => { JSON::XS::true },
        coerce => 1,
    );

    ### $object->disabled becomes "disabled: true"

    has disabled => (
        is     => 'rw',
        isa    => 'JSONBoolean',
        default => 0,
        coerce => 1,
    );

    ### $object->disabled becomes "disabled: false"

    has disabled => (
        is     => 'rw',
        isa    => 'JSONBoolean',
        default => { JSON::XS::false },
        coerce => 1,
    );

    ### $object->disabled becomes "disabled: false"


=cut

subtype JSONBoolean, as Item,
    where { Types::Serialiser::is_bool($_); };

coerce JSONBoolean, from Bool, via { ($_ ? Types::Serialiser::true : Types::Serialiser::false) };

subtype JSONNum, as Num,
    where { my $num = $_; return length( do { no warnings "numeric"; $num & "" }); };

coerce JSONNum, from Num, via { my $rv = $_; $rv += 0; return $rv; };

=head2 DateTime

Class type constraint for L<DateTime> instances.

Coercions are provided for C<Str> values.

Coerced C<Str> values are parsed using
L<DateTime::Format::ISO8601/parse_datetime>, and support that module's range
of input formats.

=cut

class_type DateTimeObject, { class => 'DateTime' };

coerce DateTimeObject, from Str, via {
    eval { DateTime::Format::ISO8601->parse_datetime($_) }
};

=head2 Timestamp

The Timestamp type constraint validates date+time values. Values must be
instances of L<DateTime> using the C<UTC> L<time zone|DateTime::TimeZone>.

Coercions are provided for C<Str> and L</DateTimeObject> values.

Coerced values from C<DateTimeObject> values are reset to the C<UTC> time zone
and cloned beforehand for referential safety.

=cut

subtype Timestamp,
    as DateTimeObject,
    where { $_->time_zone->is_utc },
    message { "$_: DateTime not in UTC time zone" };

coerce Timestamp,
    from Str, via {
        return eval { DateTimeObject->coerce($_)->set_time_zone('UTC') }
    },
    from DateTimeObject, via {
        return eval { $_->clone->set_time_zone('UTC') }
    };

=head2 Datestamp

The Datestamp type constraint validates date values. Valust must be instances
of L<DateTime> using the C<floating> L<time zone|DateTime::TimeZone>.

Coercions are provided for C<Str> and L</DateTimeObject> values.

Coercions from L<DateTimeObject> values are truncated to the value's day-part
and are reset to the C<floating> timezone after being cloned for referential
security.

=cut

subtype Datestamp,
    as DateTimeObject,
    where { $_->time_zone->is_floating },
    message { "$_: DateTime not in floating time zone" };

coerce Datestamp,
    from Str, via {
        return eval {
            DateTimeObject->coerce($_)->truncate(to => 'day')->set_time_zone('floating')
        };
    },
    from DateTimeObject, via {
        return eval {
            $_->clone->truncate(to => 'day')->set_time_zone('floating')
        };
    };

=head2 IntervalStr

A string representing an interval "type".

Allowed values are:

=over

=item * years

=item * months

=item * weeks

=item * days

=item * once

=back

=cut

enum IntervalStr, [qw[years months weeks days hours minutes seconds once]];

=head2 PackageElementStr

A valid element of a Perl package name.

Currently, we only allow ASCII (even though Perl allows Unicode in its package
names).

=cut

subtype PackageElementStr, as Str,
    where { $_ =~ /^[a-zA-Z0-9_]+$/ };


=head1 Subject restrictions

=head2 SubjectType

One of three possible subject types, see constants 

=cut

subtype SubjectType,
    as enum(SUBJECT_TYPES()),
    message { "'$_' is not a valid subject type" };


=head2 ObjectSubjectType

This is awkward. We really would like to use SubjectType for this, the new english
form of a subject. But we cannot, so that is why it is called C<ObjectSubjectType>

=cut

subtype ObjectSubjectType, as enum([qw(company person employee)]),
    message { "'$_' is not a valid subject type" };

=head2 EmailAddress

Validates an e-mail address using L<Email::Valid>.

=cut

subtype EmailAddress, as NonEmptyStr,
    where {
        return Email::Valid->address(-address => $_, -allow_ip => 0) ? 1 : 0
    },
    message { "'$_' is not a valid e-mail address" };

=head2 TelephoneNumber

Validates a telephone number using L<Number::Phone>.

If a phone number is passed without explicit country code, validation assumes
Dutch locale by default (+31).

=cut

sub _reformat_number {
    my $number = $_;
    if (substr($number, 0,2) eq '00') {
        $number = "+" . substr($number, 2);
    }
    return $number;
}

subtype TelephoneNumber, as NonEmptyStr,
    where {
        my $number = _reformat_number($_);
        return 1 if defined Number::Phone->new($number);
        return 1 if defined Number::Phone->new('NL', $number);
    },
    message { "'$_' is not a valid phone number" };

=head2 TransactionDirection

Enumerated type for the 'direction' field of sysin transactions. Either
C<incoming> or C<outgoing>. The direction represents the flow of data from the
perspective of the Zaaksysteem platform.

=cut

subtype TransactionDirection, as enum([qw[incoming outgoing]]),
    message { "'$_' is not a valid transaction direction" };

=head2 MobileNumber

Validates a telephone number using L<Number::Phone>, and ensures that the
provided value represents a mobile (cellular) phone number.

If a phone number is passed without explicit country code, validation assumes
Dutch locale by default (+31).

=cut


subtype MobileNumber, as TelephoneNumber,
    where {
        my $number = _reformat_number($_);
        return 1 if eval { Number::Phone->new($number)->is_mobile };
        return 1 if eval { Number::Phone->new('NL', $number)->is_mobile };
    },
    message { "'$_' is not a valid mobile phone number" };

=head2 NLZipcode

A Dutch zipcode, starting with 1-9, 3 other digits and 2 characters

=cut

subtype NLZipcode, as Str,
    where { return $_ =~ /^[1-9][0-9]{3}[A-Z]{2}/ },
    message { "'$_' is not a valid dutch zipcode, remove space?" };

=head2 Zipcode

Please use NLZipcode when possible. Zipcode will also allow zipcodes starting with a 0.

=cut

subtype Zipcode, as Str,
    where { return $_ =~ /^[0-9]{4}[A-Z]{2}/ },
    message { "'$_' is not a valid zipcode, remove space?" };

=head1 Type "Person"

=head2 PersonPersonalNumber

Personal number, BSN in Dutch

=cut

subtype PersonPersonalNumber, as Str,
    where { return $_ =~ /^\d{7,9}$/ },
    message { "'$_' is not a valid personal number (bsn)" };

=head2 PersonPersonalNumberA

Personal number, a-nummer in Dutch

=cut

subtype PersonPersonalNumberA, as Str,
    where { return $_ =~ /^[1-9][0-9]{9}$/ },
    message { "'$_' is not a valid personal a number (a-nummer)" };


=head1 Type "Company"

=head2 CompanyCocNumber

Chamber of Commerce number, KVK-nummer in dutch

=cut

subtype CompanyCocNumber, as Str,
    where { return $_ =~ /^[0-9]{6,8}$/ }, # officially 8 digits
    message { "'$_' is not a valid coc number (kvk-nummer)" };

=head2 CompanyCocLocationNumber

Chamber of Commerce location number, Vestigingsnummer in dutch

=cut

subtype CompanyCocLocationNumber, as Str,
    where { return $_ =~ /^[0-9]{1,12}$/ }, # officially 12 digits
    message { "'$_' is not a valid coc location number (vestigingsnummer)" };

=head2 CompanyCocRSIN

Chamber of Commerce RSIN, Rechtspersonen en Samenwerkingsverbanden informatienummer in dutch

=cut

subtype CompanyCocRSIN, as Str,
    where { return $_ =~ /^[0-9]{9}$/ },
    message { "'$_' is not a valid RSIN (Rechtspersonen en Samenwerkingsverbanden informatienummer)" };

=head1 Type "Address"

=head2 AddressNumber

Number of street

=cut

subtype AddressNumber, as Str,
    where { return $_ =~ /^[0-9]{0,8}$/ },
    message { "'$_' is not a valid housenumber" };

=head2 AddressNumberLetter

Letter suffix of housenumber (huisletter in dutch)

=cut

subtype AddressNumberLetter, as Str,
    where { return $_ =~ /^[a-zA-Z]*$/ },
    message { "'$_' is not a valid housenumber letter" };

=head2 AddressNumberSuffix

Suffix of housenumber

=cut

subtype AddressNumberSuffix, as Str,
    where { return $_ =~ /^.{0,12}$/ },
    message { "'$_' is not a valid housenumber suffix" };

=head2 CaseResult

One of the possible casetype results 

=cut

subtype CaseResult, as enum(ZAAKSYSTEEM_OPTIONS()->{RESULTAATTYPEN});
    message { "'$_' is not a valid case result, see \"resultaattypen\"" };

enum CaseConfidentiality, [qw(public internal confidential)];

=head2 ACLEntityType

One of the possible ACL entity types 

=cut

subtype ACLEntityType, as enum([qw(position role user)]),
    message { "'$_' is not a valid ACL entity type, use one of position, user or role" };

=head2 ACLScope

One of the possible ACL scopes, type/instance 

=cut

subtype ACLScope, as enum([qw(type instance)]),
    message { "'$_' is not a valid ACL scope, use one of type or instance" };

=head2 ACLCapability

An ACL capability: read,write,manage or search

=cut

subtype ACLCapability, as enum([qw(read write manage search)]),
    message { "'$_' is not a valid ACL capability, use one of read, write, manage or search" };

=head2 ACLCapabilityList

A list of ACL capabilities from read,write,manage or search

=cut

subtype ACLCapabilityList, as ArrayRef[ACLCapability];

=head2 URIx

An URI type with automatic coercion from (URI-containing) string.

=cut

class_type URIx, { class => 'URI' };
coerce URIx, from Str, via { URI->new(shift) };

=head1 INTERFACE TYPES

=head2 Store

Type constraint for the L<Zaaksysteem::Interface::Store> role.

=cut

role_type Store, { role => 'Zaaksysteem::Interface::Store' };

=head2 ValueType

Type constraint for the L<Zaaksysteem::Interface::ValueType> role.

=cut

role_type ValueType, { role => 'Zaaksysteem::Interface::ValueType' };

=head2 MunicipalityCode

Type constraint for Dutch municipality codes as defined by the CPB.

=cut

subtype MunicipalityCode,
    as enum([map { sprintf("%04d", $_->{dutch_code}) } @{ MUNICIPALITY_TABLE() }]),
    message { "'$_ is not a valid Dutch Municipality code" };

coerce MunicipalityCode, from Str, via { sprintf('%04d', shift) };

=head2 CountryCode

Type constraint for Dutch country codes as defined by the Dtuch government

=cut

subtype CountryCode,
    as enum([map { sprintf("%04d", $_->{dutch_code}) } @{ COUNTRY_TABLE() }]),
    message { "'$_ is not a valid Dutch Country code" };

coerce CountryCode, from Str, via { sprintf('%04d', shift) };


=head2 RemoteSearchModuleName

Type constraint for Romete Searches

Valid types are one of the constants:

=over

=item * REMOTE_SEARCH_MODULE_NAME_KVKAPI,

=item * REMOTE_SEARCH_MODULE_NAME_OVERHEIDIO,

=item * REMOTE_SEARCH_MODULE_NAME_STUFNP,

=back

but it will allow for any string that would match those.

NOTE: currently, as we have not figured out what values are there in the wild,
we allow any string and will warn if it is not recognized as any of the above.

=cut

subtype RemoteSearchModuleName,
    as Str,
    where {
        my $remote_search = $_;
        return 1 if any { $_ eq $remote_search } (
            REMOTE_SEARCH_MODULE_NAME_KVKAPI,
            REMOTE_SEARCH_MODULE_NAME_OVERHEIDIO,
            REMOTE_SEARCH_MODULE_NAME_STUFNP,
        );
        tombstone("20180518","Theo - Undefined RemoteSearchModuleName: '$_'");
        return 1;
    },
    message { "'$_' is not one of the valid RemoteSearchModuleName enums" };
    # which will not be effective, untill we have a true Enum

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

