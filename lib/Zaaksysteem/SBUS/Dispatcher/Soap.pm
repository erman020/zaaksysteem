package Zaaksysteem::SBUS::Dispatcher::Soap;

use Moose::Role;
use Data::Dumper;
use XML::Tidy;

use Zaaksysteem::SBUS::Dispatcher::Soap::TestTransport;

use FindBin qw/$Bin/;

use XML::Compile::WSDL11;      # use WSDL version 1.1
use XML::Compile::SOAP11;      # use SOAP version 1.1
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::Translate::Reader;

has wsdl => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        my $compiler        = Zaaksysteem::SBUS::Types::StUF::XML->new(
            home    => $self->config->{home},
        );

        my $wsdl = XML::Compile::WSDL11->new(
            $self->config->{home} .  '/share/wsdl/stuf/bg0204/bg0204.wsdl',
            opts_readers  => $compiler->xml_compile_config->{READER},
            opts_writers  => $compiler->xml_compile_config->{WRITER},
        );

        for my $def (@{ $compiler->xml_definitions }) {
            $wsdl->importDefinitions(
                $def
            );
        }

        return $wsdl;
    }
);

sub _get_soap_url {
    my ($self, $type)   = @_;

    my $sbus_options    = $self->config;

    my $endpoint_url    = $sbus_options->{default_transport};
    if ($type && $type eq 'asynchroon') {
        $endpoint_url = $sbus_options->{soap_async_url} if
            (
                defined($sbus_options->{soap_async_url}) &&
                $sbus_options->{soap_async_url}
            );
    } elsif ($type && $type eq 'synchroon') {
        $endpoint_url = $sbus_options->{soap_sync_url} if
            (
                defined($sbus_options->{soap_sync_url}) &&
                $sbus_options->{soap_sync_url}
            );
    }

    die('Cannot find transport address, make sure you set it in '
        . ' customer_instance->{start_config}->{SBUS}'
        . '->{default_transport}'
    ) unless $endpoint_url;

    return $endpoint_url;
}

sub transport {
    my $self        = shift;
    my $call        = shift;
    my $port        = shift;

    my $sbus_options = $self->config;

    my ($address, $action);
    if ($port eq 'asynchroon') {
        $address = $self->_get_soap_url('asynchroon');
        $action  = $self->config->{soap_async_action};
    } else {
        $address = $self->_get_soap_url('synchroon');
        $action  = $self->config->{soap_sync_action};
    }

    $self->log->debug('Handling SOAP call: ' . $call);

    my $transport   = XML::Compile::Transport::SOAPHTTP->new(
        'address'   => $address,
        'timeout'   => 25,
    );

    if (
        defined(
            $sbus_options->{default_ssl_key}
        ) &&
        $sbus_options->{default_ssl_key}
    ) {
        $transport->userAgent->ssl_opts(
            'SSL_key_file' =>
                $sbus_options->{default_ssl_key}
        );

        $transport->userAgent->ssl_opts(
            'SSL_cert_file' =>
                $sbus_options->{default_ssl_crt}
        );

        $transport->userAgent->ssl_opts(
            'verify_hostname' => 0
        );
    }

    $transport->userAgent->default_header('SOAPAction', $action);

    return $transport;
}

sub _dispatch_soap_test_response {
    my ($self, $xml, $file) = @_;

    return unless (-d 't/inc/stuf/response');

    my $testfile    = 't/inc/stuf/response/' . lc($file);

    return unless (-f $testfile);

    my $return_xml  = '';

    open(my $fh, '<' . $testfile);
    while (<$fh>) {
        $return_xml .= $_;
    }
    close($fh);

    return $return_xml;
}

sub _dispatch_soap {
    my $self        = shift;
    my $call        = shift;
    my $params      = shift;
    my $options     = shift;

    my $operation;

    if ($options->{dispatch_port} && $options->{dispatch_port} eq 'asynchroon') {
        $operation   = $self->wsdl->operation(
            $call,
            service         => 'StUFBGAsynchroon',
            port            => 'StUFBGAsynchronePort',
        );
    } else {
        $operation   = $self->wsdl->operation(
            $call,
            service         => 'StUFBGSynchroon',
            port            => 'StUFBGSynchronePort',
        );
    }

    $self->log->debug(
        'Calling SOAP[StUF] operation: ' . $operation->name .
        ' / style: ' . $operation->style
    );

    my $client_options  = {};

    if (
        defined($self->config->{test_transport}) &&
        $self->config->{test_transport}
    ) {
        $client_options = {
            transport_hook   => sub {
                my $test_transport  = Zaaksysteem::SBUS::Dispatcher::Soap::TestTransport
                            ->new(
                                log => $self->log,
                                xml => shift,
                            );

                if ($self->fake_transport) {
                    return $self->fake_transport->($test_transport->xml)
                }

                $test_transport->process;

                return $test_transport->response;
            }
        };
    } else {
        $client_options = {
            transport   => $self->transport($call, $options->{dispatch_port}),
        };
    }

    my $client      = $operation->compileClient(%{ $client_options });

    my ($answer, $trace) = $client->($params);

    if (!$answer && $self->die_on_error) {
        die('Empty response, cannot be good, raw content: ' .
            (
                ref($trace->response)
                    ? $trace->response->content
                    : $trace->response
            )
        );
    }

    if ($answer) {
        for my $key (keys %{ $answer }) {
            next unless UNIVERSAL::isa($answer->{$key}, 'XML::LibXML::Element');

            my $value = $answer->{$key};

            ### VICREA PATCH, REMOVE ATTRS
            {
                my $nodes   = $value->getElementsByTagName('PRSIDB');

                if ($nodes->size) {
                    $_->parentNode->removeChild($_) for $nodes->get_nodelist;
                }
            }

            ### Define the reader
            my $reader         = $self->wsdl->compile(
                READER  => $key
            );

            return $reader->($value);
        }
    }

    return $answer;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 transport

TODO: Fix the POD

=cut

