package Zaaksysteem::API::v1::Serializer::Reader::Util;

use Moose;

with qw/
    Zaaksysteem::API::v1::Serializer::DispatchReaderRole
    Zaaksysteem::API::v1::Serializer::ValidationExceptionRole
/;

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Util - Misc serializer readers

=head1 SYNOPSIS

    my $reader = Zaaksysteem::API::v1::Serializer::Reader::Util->grok($object);

    my $data = $reader->($serializer, $object);

=head1 DESCRIPTION

This class implements serializer readers for a few one-off objects that are
trivially read and used all over Zaaksysteem.

This class should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastructure.

=head1 METHODS

=head2 dispatch_map

Implements sub required by
L<Zaaksysteem::API::v1::Serializer::DispatchReaderRole>.

=head3 Handlers

=over 4

=item L<DateTime>

=item L<Zaaksysteem::Object::Iterator>

=item L<BTTW::Exception>

=back

=cut

sub dispatch_map {
    return (
        DateTime => sub {
            __PACKAGE__->read_datetime(@_)
        },
        JSON     => sub {
            return @_;
        },
        'JSON::Boolean'     => sub {
            my ($serializer, $object) = @_;
            return $object ? JSON::XS::true : JSON::XS::false;
        },

        'Zaaksysteem::Types::MappedString' => sub {
            __PACKAGE__->read_mapped_string(@_)
        },

        'Zaaksysteem::API::v1::Set' => sub {
            __PACKAGE__->read_set(@_)
        },

        'Zaaksysteem::API::v1::ResultSet' => sub {
            __PACKAGE__->read_resultset(@_)
        },

        'Zaaksysteem::API::v1::ArraySet' => sub {
            __PACKAGE__->read_arrayset(@_)
        },

        'Zaaksysteem::Object::Iterator' => sub {
            __PACKAGE__->read_object_iterator(@_)
        },

        'Zaaksysteem::Object::Reference::Instance' => sub {
            __PACKAGE__->read_object_reference_instance(@_)
        },

        'BTTW::Exception::Base' => sub {
            __PACKAGE__->read_exception(@_)
        },

        'Zaaksysteem::API::v1::Status' => sub {
            __PACKAGE__->read_status(@_)
        },
    );
}

=head2 read_datetime

This reader handles L<DateTime> objects. The returned data is a valid ISO-8601
datetime string.

=cut

sub read_datetime {
    my ($class, $serializer, $datetime) = @_;

    my $dt = $datetime->clone->set_time_zone('UTC');

    return sprintf('%sZ', $dt->iso8601);
}

=head2 read_mapped_string

This reader handles L<Zaaksysteem::Types::MappedString> instances. The
returned value will be the internal 'slug' value.

=cut

sub read_mapped_string {
    my ($class, $serializer, $mapped_string) = @_;

    return $mapped_string->original;
}

=head2 read_object_iterator

This reader handles L<Zaaksysteem::Object::Iterator> objects. The returned
data is an array of serializations for every result provided by the iterator.

This reader is implemented generically, by feeding the individual results from
the iterator back into the serializer recursively.

=cut

sub read_object_iterator {
    my ($class, $serializer, $iterator) = @_;

    # Hijack the Object::Iterator inflation hook by wrapping it back onto
    # the current serializer object, and letting it figure out what to do
    # with individual objects.
    my $hijack = $iterator->clone(inflator => sub {
        return $serializer->read(shift, {
            fields => $iterator->fields
        });
    });

    return {
        type => 'set',
        reference => undef,
        instance => {
            rows => [ $hijack->all ],
        }
    };
}

=head2 read_exception

This reader handles L<BTTW::Exception::Base> objects. The returned data
is formatted like the example below.

    {
        type => 'exception',
        exception => {
            type => 'api/v1/...',
            message => 'something went wrong yo'
        }
    }

=cut

sub read_exception {
    my ($class, $serializer, $exception) = @_;

    return {
        type => 'exception',
        reference => undef,
        preview => sprintf('%s: %s', $exception->type, $exception->message),
        instance => {
            type => $exception->type,
            message => $exception->message
        }
    }
}

=head2 read_set

This reader handles L<Zaaksysteem::API::v1::Set> objects. The returned data
is an object with pager metadata and an array of set members. This reader is
a sibling of the L</read_object_iterator>, and to the outside world, this
variant only provides an additional pager key in the instance.

This reader is implemented generically, by feeding the individual results from
the iterator back into the serializer recursively.

=cut

sub read_set {
    my ($class, $serializer, $set, @opts) = @_;

    # Hijack the Object::Iterator inflation hook by wrapping it back onto
    # the current serializer object, and letting it figure out what to do
    # with individual objects.
    my $iterator = $set->build_iterator;

    my $hijack = $iterator->clone(inflator => sub {
        return $serializer->read($iterator->inflate(shift), {
            fields => $set->iterator->fields
        });
    });

    my $pager = $iterator->pager;

    return {
        type => 'set',
        reference => undef,
        instance => {
            pager => {
                page => int($pager->current_page),
                pages => int($pager->last_page),
                rows => int($pager->entries_on_this_page),
                total_rows => int($pager->total_entries),

                next => $set->mangle_uri($pager->next_page),
                prev => $set->mangle_uri($pager->previous_page)
            },

            rows => [ $hijack->all ]
        }
    };
}

=head2 read_resultset

This reader handles L<Zaaksysteem::API::v1::ResultSet> objects. The returned data
is an object with pager metadata and an array of set members. This reader is
a sibling of the L</read_object_iterator>, and to the outside world, this
variant only provides an additional pager key in the instance.

This reader is implemented generically, by feeding the individual results from
the iterator back into the serializer recursively.

=cut

sub read_resultset {
    my ($class, $serializer, $set, @opts) = @_;

    my $iterator    = $set->build_iterator;
    my $pager       = $iterator->pager;

    my @rows;
    while (my $item = $iterator->next) {
        push(
            @rows,
            $serializer->read(
                $item->can('as_object') ? $item->as_object : $item, @opts
            )
        );
    }


    return {
        type => 'set',
        reference => undef,
        instance => {
            pager => {
                page => int($pager->current_page),
                pages => int($pager->last_page),
                rows => int($pager->entries_on_this_page),
                total_rows => int($pager->total_entries),

                next => $set->mangle_uri($pager->next_page),
                prev => $set->mangle_uri($pager->previous_page)
            },

            rows => \@rows,
        }
    };
}

=head2 read_arrayset

This reader handles ArrayRef containing objects. The returned data
is an object with pager metadata and an array of set members. 

=cut

sub read_arrayset {
    my ($class, $serializer, $set, @opts) = @_;

    my $pager = $set->pager;

    return {
        type => 'set',
        reference => undef,
        instance => {
            pager => {
                page => int($pager->current_page),
                pages => int($pager->last_page),
                rows => int($pager->entries_on_this_page),
                total_rows => int($pager->total_entries),

                next => $set->mangle_uri($pager->next_page),
                prev => $set->mangle_uri($pager->previous_page)
            },

            rows => [
                map(
                    {
                        $serializer->read($_)
                    }
                    @{ $set->paged_content($pager) }
                )
            ],
        }
    };
}

=head2 read_status

This reader handles instances of L<Zaaksysteem::API::v1::Status>.

=cut

sub read_status {
    my ($class, $serializer, $status, @opts) = @_;

    return {
        type => 'status',
        reference => undef,
        instance => {
            version => $status->version
        }
    };
}

=head2 read_object_reference_instance

This reader handles object references (objects of type L<Zaaksysteem::Object::Reference::Instance>)

=cut

sub read_object_reference_instance {
    my ($class, $serializer, $object_ref, @opts) = @_;

    return {
        type      => $object_ref->type,
        reference => $object_ref->id,
        preview   => $object_ref->TO_STRING,
        # $object_ref->id,
        instance  => undef,
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
