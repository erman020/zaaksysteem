package Zaaksysteem::API::v1::Serializer::Reader::MappedString;
use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

sub class { 'Zaaksysteem::Types::MappedString' }

sub read {
    my ($class, $serializer, $item) = @_;

    return $item->original;
}

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
