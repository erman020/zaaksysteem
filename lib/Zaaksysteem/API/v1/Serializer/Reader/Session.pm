package Zaaksysteem::API::v1::Serializer::Reader::Session;
use Moose;

use Zaaksysteem::Constants qw/LDAP_DIV_MEDEWERKER/;

with 'Zaaksysteem::API::v1::Serializer::DispatchReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Session - Current session reader

=head1 DESCRIPTION

This serializer encodes the L<Zaaksysteem::API::v1::Object::Session> into a valid APIv1 JSON structure.

=head1 OBJECT TYPES

The following object types will be converted via this reader:

=over

=item * Zaaksysteem::API::v1::Object::Session

=item * Zaaksysteem::API::v1::Object::Session::Account

=back

=head1 METHODS

=head2 session

The current session

=cut

sub dispatch_map {
    return (
        'Zaaksysteem::API::v1::Object::Session' => sub { __PACKAGE__->read_session(@_) },
        'Zaaksysteem::API::v1::Object::Session::Account' => sub { __PACKAGE__->read_session_account(@_) },
    );
}

=head2 read_session

Return the contents of a session object L<Zaaksysteem::API::v1::Object::Session>

=cut

sub read_session {
    my ($class, $serializer, $object) = @_;

    my (%user);
    if ($object->logged_in_user) {
        my $subject = $object->logged_in_user;

        ### TODO: Move this to Subject::Component
        %user       = (
            id                  => ($subject->id // undef),
            surname             => ($subject->sn),
            display_name        => ($subject->displayname // undef),
            given_name          => ($subject->givenname // undef),
            initials            => ($subject->initials // undef),
            email               => $subject->mail,
            telephonenumber     => ($subject->telephonenumber // undef),
            uuid                => $subject->uuid,
        );

        $user{organizational_unit}  = $subject->primary_group->name;
        $user{parent_organizational_units} = [
            map { $_->name } @{ $subject->inherited_groups }
        ];

        $user{capabilities}     = [
            @{ $subject->system_permissions },
            @{ $object->capabilities }
        ];

        $user{settings}         = $class->_load_usersettings($subject->usersettings);

        $user{system_roles} = [
            map { $_->name } @{ $subject->system_roles }
        ];

        my @positions;
        foreach my $pos (@{$subject->position_objects}) {
            push(@positions, $serializer->read($pos));
        }

        $user{legacy}          = {
            role_ids    => [ map { $_->id } @{ $subject->roles } ],
            ou_id       => $subject->primary_group->id,
            parent_ou_ids => [ map { $_->id } @{ $subject->inherited_groups } ],
            is_div      => (grep ({ LDAP_DIV_MEDEWERKER eq $_ } @{ $user{system_roles} }) ? 1 : 0),

            # Although this are new-style objects I don't want to pin
            # ourselves to this yet, we might want a different API for
            # this.
            positions => \@positions,

        };
    }

    return {
        type      => 'session',
        reference => undef,
        instance  => {
            hostname          => $object->hostname,
            logged_in_user    => (%user ? \%user : undef),
            account           => $serializer->read($object->account),
            design_template   => $object->design_template,
            active_interfaces => $object->active_interfaces,
            configurable      => $object->configurable,
        }
    };
}

=head2 read_session_account

Return the contents of a session object L<Zaaksysteem::API::v1::Object::Session>

=cut

sub read_session_account {
    my ($class, $serializer, $object) = @_;

    return {
        type => 'session_account',
        reference => undef,
        instance => {
            map { $_ => $object->$_ } qw/
                company
                place
                address
                zipcode
                homepage
                email
                phonenumber
            /
        }
    };
}

=head2 _load_usersettings

Exposes "labs"

=cut

sub _load_usersettings {
    my ($class, $settings) = @_;

    return {
        lab    => {
            map ( { $_ => $settings->lab->$_ } qw/desktop_version case_version/),
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
