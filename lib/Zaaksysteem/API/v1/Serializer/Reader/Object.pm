package Zaaksysteem::API::v1::Serializer::Reader::Object;

use Moose;

use BTTW::Tools;

use Zaaksysteem::API::v1::ArraySet;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Object - Read L<Zaaksysteem::Object>
instances.

=head1 DESCRIPTION

=head1 METHODS

=head2 class

Returns C<Zaaksysteem::Object>. Required by
L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::Object' }

=head2 read

Returns a hashref data structure of instance data for serialization.
Required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

    {
        type => 'my_object',
        reference => '1123fg...',
        instance => {
            some_attr => 'some value',
            some_relation => { type => 'some_related_object', ... }
        }
    }

=cut

sub read {
    my ($class, $serializer, $object, $opts) = @_;

    if ($object->type eq 'casetype') {
        return $class->read_casetype($serializer, $object, $opts);
    }

    my @attrs = grep { $_->does('Zaaksysteem::Metarole::ObjectAttribute') }
                     $object->meta->get_all_attributes;

    my @rels = grep { $_->does('Zaaksysteem::Metarole::ObjectRelation') }
                    $object->meta->get_all_attributes;

    my %instance_attributes;
    my %instance_relations;

    for my $attribute (@attrs) {
        $instance_attributes{ $attribute->name }
            = $class->serialize_value($serializer, $attribute, $object);
    }

    for my $relation (@rels) {
        my $value = $relation->get_value($object);

        if (defined $value) {
            if ($relation->isa_set) {
                $value = Zaaksysteem::API::v1::ArraySet->new(
                    content => $value
                );
            }

            $value = $serializer->read($value);
        }

        $instance_relations{ $relation->name } = $value;
    }

    return {
        type => $object->type,
        reference => $object->id,
        preview => $object->TO_STRING,
        instance => { %instance_attributes, %instance_relations }
    };
}

=head2 read_casetype

Type-specific reader for casetype object instances.

=cut

# Casetype reader is explicitly non-generic, because v1 already has a casetype
# serialization we must adhere to for now
sub read_casetype {
    my ($class, $serializer, $object, $opts) = @_;

    my @phases = map {
        $class->_read_phase(
            $serializer,
            $_,
            {
                lock_registration_phase => $object->lock_registration_phase
            }
        );
    } @{$object->ordered_phases};

    my @results = map { $class->_read_result($_); }
        sort  { $a->casetype_result_id cmp $b->casetype_result_id }
        @{ $object->instance_results };

    return {
        type => 'casetype',
        reference => $object->id,
        preview => $object->TO_STRING,
        instance => {
            id => $object->id,
            offline => $object->offline ? JSON::true : JSON::false,
            title => $object->name,
            trigger => $object->trigger,
            subject_types => $object->subject_types,
            preset_client => $object->has_preset_client ? $serializer->read($object->preset_client) : undef,
            sources => $object->sources,
            results => \@results,
            phases => \@phases,
            queue_coworker_changes => ($object->queue_coworker_changes ? JSON::true : JSON::false),
            legacy => {
                zaaktype_id => $object->casetype_id,
                zaaktype_node_id => $object->casetype_node_id,
            },
            properties => {
                extension => $object->extendable ? 'Ja' : 'Nee',
                lex_silencio_positivo => $object->lex_silencio_positivo ? 'Ja' : 'Nee',
                principle_local => $object->local_basis,
                supervisor => $object->supervisor,
                supervisor_relation => $object->supervisor_relation,
                suspension => $object->suspendable ? 'Ja' : 'Nee',
                objection_and_appeal => $object->appealable ? 'Ja' : 'Nee',
                publication => $object->publishable ? 'Ja' : 'Nee',
                text_for_publication => $object->publication_text,
                designation_of_confidentiality => $object->confidentiality,
                wkpb => $object->wkpb ? 'Ja' : 'Nee',
                goal => $object->purpose,
                archive_classification_code => $object->records_classification,
                e_formulier => $object->eform,
                motivation => $object->motivation,
                penalty => $object->penalty,
                lead_time_service => $object->lead_time_service,
                lead_time_legal => $object->lead_time_legal,
            },
            settings => {
                allow_assign_on_create => $object->allow_assign_on_create ? JSON::true : JSON::false,
                allow_subjects_on_create => $object->allow_subjects_on_create ? JSON::true : JSON::false,
                allow_take_on_create => $object->allow_take_on_create ? JSON::true : JSON::false,
                allow_reuse_previous_registration => $object->allow_reuse_previous_registration ? JSON::true : JSON::false,
                contact_info_email_required => $object->contact_info_email_required ? JSON::true : JSON::false,
                contact_info_mobile_phone_required => $object->contact_info_mobile_phone_required ? JSON::true : JSON::false,
                contact_info_phone_required => $object->contact_info_phone_required ? JSON::true : JSON::false,
                intake_show_contact_info => $object->intake_show_contact_info ? JSON::true : JSON::false,
                online_payment => $object->online_payment ? JSON::true : JSON::false,
                offline_payment => $object->offline_payment ? JSON::true : JSON::false,
                public_registration_form => $object->public_registration_form ? JSON::true : JSON::false,
                show_confidentiality_on_create => $object->show_confidentiality_on_create ? JSON::true : JSON::false,
            },
            public_url_path => $object->public_url_path,
        }
    };
}

sub _read_result {
    my $self = shift;
    my $result = shift;

    return {
        resultaat_id             => $result->casetype_result_id,
        type                     => $result->generic_result_type,
        archive_procedure        => $result->retention_period_source_date,
        period_of_preservation   => $result->retention_period,
        type_of_dossier          => $result->dossier_type,
        label                    => $result->label,
        selection_list           => $result->selection_list,
        type_of_archiving        => $result->archival_type,
        comments                 => $result->explanation,
        external_reference       => $result->external_reference,
        trigger_archival         => $result->trigger_archival,
        selection_list_number    => $result->selection_list_number,
        process_type_number      => $result->process_type_number,
        process_type_name        => $result->process_type_name,
        process_type_description => $result->process_type_description,
        process_type_explanation => $result->process_type_explanation,
        process_type_generic     => $result->process_type_generic,
        process_type_object      => $result->process_type_object,
        origin                   => $result->origin,
        process_term             => $result->process_term,
    };
}

sub _read_phase {
    my $self = shift;
    my $serializer = shift;
    my $phase = shift;
    my $opts = shift;

    return {
        name => $phase->label,
        seq => $phase->sequence,
        id => $phase->casetype_phase_id,
        fields => $phase->attributes,
        locked => $opts->{ lock_registration_phase } && $phase->sequence == 1 ? \1 : \0,

        route => {
            set   => ($phase->route->{set}  ? JSON::true : JSON::false),
            role  => $phase->route->{role}  ? $serializer->read($phase->route->{role}) : undef,
            group => $phase->route->{group} ? $serializer->read($phase->route->{group}) : undef,
        },
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
