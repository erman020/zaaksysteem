package Zaaksysteem::API::v1::Serializer::Reader::Contactmoment;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::DispatchReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Contactmoment - Reader for a contactmoment

=head1 DESCRIPTION

This serializer encodes the Contactmoment Object into a valid APIv1 JSON structure.

=head1 OBJECT TYPES

The following object types will be converted via this reader:

=head1 METHODS

=head2 contactmoment

The contactmoment object

=cut

sub dispatch_map {
    return (
        'Zaaksysteem::Model::DB::Contactmoment' => sub { __PACKAGE__->read_contactmoment(@_) },
    );
}

=head2 read_contactmoment

Return the contents of a contactmoment object

=cut

sub read_contactmoment {
    my ($class, $serializer, $object) = @_;

    my $rv = {
        map ({ $_ => $object->get_column($_) } qw/subject_id case_id type medium date_created created_by/),
        message         => undef,
        email_body      => undef,
        email_subject   => undef,
        email_recipient => undef,
        email_cc        => undef,
        email_bcc       => undef,
    };


    if ($object->contactmoment_note) {
        $rv->{message} = $object->contactmoment_note->message;
    } elsif ($object->contactmoment_email) {
        $rv->{email_body} = $rv->{message} = $object->contactmoment_email->body;
        $rv->{email_subject} = $object->contactmoment_email->subject;
        $rv->{email_recipient} = $object->contactmoment_email->recipient;
        $rv->{email_cc} = $object->contactmoment_email->cc;
        $rv->{email_bcc} = $object->contactmoment_email->bcc;
    }

    return {
        type => 'contactmoment',
        reference => $object->uuid,
        instance => $rv,
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
