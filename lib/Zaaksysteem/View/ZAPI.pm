package Zaaksysteem::View::ZAPI;

use Moose;

use BTTW::Tools;
use Zaaksysteem::ZAPI::Error;
use Zaaksysteem::ZAPI::Response;

BEGIN { extends 'Catalyst::View'; }

use constant ZAPI_FORMATS   => {
    'csv'       => 'View::CSV',
    'xls'       => 'View::CSV',
    'calc'      => 'View::CSV',
};


=head2 process

Extends the default process sub

=cut

sub process {
    my $self    = shift;
    my $c       = shift;

    return if $c->stash->{ zapi_cached_response };

    unless($c->stash->{ zapi }) {
        throw('zapi/process', 'Unable to process ZAPI view, no "zapi" key in stash');
    }

    my $zapi_object = $c->stash->{zapi};
    if (
        blessed($zapi_object)
        && (   $zapi_object->isa('Zaaksysteem::ZAPI::Response')
            || $zapi_object->isa('Zaaksysteem::ZAPI::Error'))
        )
    {
        $c->stash->{zapi} = $c->stash->{zapi}->response;
        $c->res->status($c->stash->{zapi}->{status_code});
    }
    elsif (!(ref $zapi_object eq 'HASH' && $c->stash->{zapi}->{num_rows})) {
        my $zapi_no_pager = 0;

        # If the controller specifies it's a "no pager" controller, AND the
        # user specifies a zapi_no_pager parameter, the parameter wins.
        $zapi_no_pager = $c->stash->{zapi_no_pager}
            if (defined $c->stash->{zapi_no_pager});
        $zapi_no_pager = $c->req->params->{zapi_no_pager}
            if (defined $c->req->params->{zapi_no_pager});

        ### Some kind of loop?
        $zapi_object     = Zaaksysteem::ZAPI::Response->new(
            unknown         => $c->stash->{zapi},
            uri_prefix      => $c->uri_for(
                $c->action,
                $c->req->captures,
                @{ $c->req->args },
                $c->req->params
            ),
            no_pager        => $zapi_no_pager,
            page_current    => $c->req->params->{   zapi_page     } || 1,
            page_size       => $c->req->params->{   zapi_num_rows } || 10,

            order_by        => $c->req->params->{   zapi_order_by   },

            order_by_direction => $c->req->params->{zapi_order_by_direction},
        );

        $c->stash->{zapi} = $zapi_object->response;
        $c->res->status($c->stash->{zapi}->{status_code});
    }

    my $format  = $c->req->params->{zapi_format};

    if ($c->res->status ne '200') {
        $format = 'json';
    }

    $self->_process_format($c, $format, $zapi_object, @_);
};

sub _process_format {
    my $self        = shift;
    my $c           = shift;
    my $format      = shift;
    my $zapi_object = shift;


    if ($format && ZAPI_FORMATS->{$format}) {
        return $c->view('ZAPI::CSV')->process(
            $c,
            {
                zapi_object => $zapi_object,
                format      => $format
            },
            @_
        );
    }

    ### Default JSON
    return $c->view('ZAPI::JSON')->process($c, $zapi_object, @_);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAPI_FORMATS

TODO: Fix the POD

=cut

