package Zaaksysteem::Geo::BAG::Connection::Spoof;
use Moose;
use namespace::autoclean;

with 'MooseX::Log::Log4perl', 'Zaaksysteem::Geo::BAG::Connection';

use autodie;
use BTTW::Tools;
use File::Spec::Functions qw(catfile);
use Hash::Flatten qw(flatten unflatten);
use JSON::XS;
use List::Util qw(first);
use Math::Trig qw(:pi :great_circle deg2rad);

=head1 NAME

Zaaksysteem::Geo::BAG::Connection::Spoof - Search the included (small) BAG extract

=head1 SYNOPSIS

    my $connection = Zaaksysteem::Geo::BAG::Connection::Spoof->new(
        home => '/opt/zaaksysteem',
    );

    my $result = $connection->search("1234AA 94");

=head1 ATTRIBUTES

=head2 home

Home/installation directory of Zaaksysteem. Will be used to locate the BAG
extract to search through.

=cut

has home => (
    is => 'ro',
    isa => 'Str',
    required => 1,
);

my %SPOOF_BAG;

=head1 METHODS

=head2 search

Query the in-memory BAG (1000 random entries).

=cut

define_profile search => (
    required => {
        type  => 'Str',
        query => 'Str',
    }
);

sub search {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    $self->_ensure_spoof_bag_loaded();
    return if not exists $SPOOF_BAG{ $args->{type} };

    my $type = $args->{type};
    my $query = $args->{query};

    my %query_parts;

    if ($query =~ s/(?<postcode_numbers>[0-9]{4})\s*(?<postcode_letters>[A-Za-z]{2})//) {
        $query_parts{postcode} = uc("$+{postcode_numbers}$+{postcode_letters}");
    }

    $query_parts{words} = [ split /\W+/, $query ];

    my @result;
    for my $entry (@{ $SPOOF_BAG{ $args->{type} } }) {
        my $score = 0;

        if ($query_parts{postcode} && $entry->{postcode} eq $query_parts{postcode}) {
            $score += 2;
        }

        for my $word (@{ $query_parts{words} }) {
            for my $field (qw(huisnummer plaats straatnaam)) {
                if ($entry->{$field} =~ /^\Q$word\E/i) {
                    $score++;
                }
            }
        }

        if (@{ $self->priority_gemeentes }) {
            for my $gemeente (@{ $self->priority_gemeentes }) {
                if ($entry->{gemeente} eq $gemeente) {
                    $score++ if $score;
                    last;
                } elsif($self->priority_only) {
                    $score = 0;
                }
            }
        }
        if ($score) {
            push @result, {
                score  => $score,
                source => $entry,
            };
        }
    }

    return [
        map  { unflatten($_->{source}) }
        sort { $b->{score} <=> $a->{score} }
        @result,
    ];
}

=head2 get_exact

Search for entries matching the specified exactly (no prefix searches etc.).

=cut

define_profile get_exact => (
    required => {
        type   => 'Str',
        fields => 'HashRef',
    },
);

sub get_exact {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    $self->_ensure_spoof_bag_loaded();
    return if not exists $SPOOF_BAG{ $args->{type} };

    my @matching;

ENTRY:
    for my $entry (@{ $SPOOF_BAG{ $args->{type} } }) {
        for my $field (keys %{ $args->{fields} }) {
            next ENTRY if $entry->{$field} ne $args->{fields}{$field};
        }

        push @matching, unflatten($entry);
    }

    return \@matching;
}

=head2 get

Retrieve a single BAG item by type.

=cut

my %key_location_map = (
    'openbareruimte'   => 'woonplaats.openbareruimte.id',
    'nummeraanduiding' => 'woonplaats.openbareruimte.nummeraanduiding.id',
);

define_profile get => (
    required => {
        type => 'Str',
        id   => 'Num',
    },
);

sub get {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    $self->_ensure_spoof_bag_loaded();
    return if not exists $SPOOF_BAG{ $args->{type} };

    my $location = $key_location_map{ $args->{type} };

    my $item = first {
        $_->{ $location } eq $args->{id}
    } @{ $SPOOF_BAG{ $args->{type} } } ;

    return unflatten($item) if defined $item;
    return;
}

=head2 find_nearest

Find the BAG object of the specified type that's nearest to the specified location

=cut

define_profile find_nearest => (
    required => {
        type      => 'Str',
        latitude  => "Num",
        longitude => "Num",
    },
);

sub find_nearest {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    $self->_ensure_spoof_bag_loaded();
    return if not exists $SPOOF_BAG{ $args->{type} };

    # Math::Trig assumes that φ=0 at the poles, while "GPS" coordinates
    # have φ=0 at the equator. Subtracting latitude from 90 fixes this.
    my $lat = deg2rad(90 - $args->{latitude});
    my $lon = deg2rad($args->{longitude});

    my $matching_entry;
    my $matching_distance = 1e4 * pi;

    for my $entry (@{ $SPOOF_BAG{ $args->{type} } }) {
        my ($entry_lat, $entry_lon) = split /,/, $entry->{geo_lat_lon};
        $entry_lat = deg2rad(90 - $entry_lat);
        $entry_lon = deg2rad($entry_lon);

        my $distance = great_circle_distance(
            $lon, pip2 - $lat,
            $entry_lon, pip2 - $entry_lat,
            1e4
        );

        if ($distance < $matching_distance) {
            $self->log->trace("Found a new shortest distance: $distance");
            $self->log->trace("Straat/plaats: $entry->{straatnaam} $entry->{plaats}");
            $matching_entry = $entry;
            $matching_distance = $distance;
        }
    }

    if ($matching_entry) {
        $self->log->trace("Matching entry straat/plaats: $matching_entry->{straatnaam} $matching_entry->{plaats}");
        return [unflatten($matching_entry)];
    }
    return;
}

=head2 _ensure_spoof_bag_loaded

Ensure the (partial) BAG extract is loaded and ready for searching.

=cut

sub _ensure_spoof_bag_loaded {
    my $self = shift;

    return if keys %SPOOF_BAG;

    $SPOOF_BAG{nummeraanduiding} = $self->_load_bag_part('nummeraanduiding');
    $SPOOF_BAG{openbareruimte}   = $self->_load_bag_part('openbareruimte');

    return;
}

=head2 _load_bag_part

Loads a (partial) BAG dump file for use in spoof mode.

=cut

my %type_part_map = (
    nummeraanduiding => 'nummeraanduiding.txt',
    openbareruimte   => 'openbareruimte.txt',
);

sub _load_bag_part {
    my $self = shift;
    my $type = shift;

    my $bag_file = catfile($self->home, "share", "bag", $type_part_map{$type});

    open my $bag_fh, "<:raw", $bag_file;

    my @rv;
    while (my $line = readline($bag_fh)) {
        my $parsed = flatten( JSON::XS->new->utf8->decode($line) );
        push @rv, $parsed;
    }

    close $bag_fh;

    return \@rv;
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
