package Zaaksysteem::Geo::BAG::Model;
use Moose;

with 'MooseX::Log::Log4perl';

use BTTW::Tools;
use Moose::Util::TypeConstraints qw(enum);
use Zaaksysteem::Constants qw(BAG_TYPES);
use Zaaksysteem::Geo::BAG::Object;
use Zaaksysteem::Types qw(NonEmptyStr);

=head1 NAME

Zaaksysteem::Geo::BAG::Model - Model for the BAG, allows searching etc.

=head1 SYNOPSIS

    my $bag = Zaaksysteem::Geo::BAG::Model->new(
        connection => $connection,
        priority_gemeentes => ['Amsterdam'],
    );

    my $results1 = $bag->search(query => "1114AD 90");
    my $results2 = $bag->search(query => "Kerkstr 50");

=head1 ATTRIBUTES

=head2 connection

An object that implements L<Zaaksysteem::Geo::BAG::Connection>, used to search
the BAG.

=cut

has connection => (
    is       => 'ro',
    does     => 'Zaaksysteem::Geo::BAG::Connection',
    required => 1,
);

=head2 parse_search_term

Parse a "Google Maps" style attribute value into its component parts (if
possible), so the caller can decide whether call L</get_exact> or L</search>.

=cut

sub parse_search_term {
    my $self = shift;
    my $term = shift;

    # The Google Maps attribute saves the *human readable* address instead of the identification.
    # We need to parse it and do a BAG search to get it back.
    my ($huisnummer, $huisletter, $huisnummertoevoeging, $postcode) =
        $term =~ /([0-9]+)([a-zA-Z])?(?:-([^,]+))?, ([0-9]{4}\ ?[A-Z]{2})/;

    if (!$postcode || !$huisnummer) {
        # Possibly an old-style value
        # (Google doesn't know the difference between huisnummertoevoeging and huisletter)
        ($huisnummer, $postcode) =
            $term =~ /([0-9]+)(?:[^,]*), ([0-9]{4}\ ?[A-Z]{2})/;
    }

    return unless ($postcode && $huisnummer);

    return {
        postcode   => $postcode,
        huisnummer => $huisnummer,
        (defined $huisletter)
            ? (huisletter => $huisletter)
            : (),
        (defined $huisnummertoevoeging)
            ? (huisnummer_toevoeging => $huisnummertoevoeging)
            : (),
    }
}

=head2 search

Search the BAG for the given text string.

The string will be parsed and passed on to the L</connection>.

Accepts one named argument:

=over

=item * query [Str]

The text to search for.

=back

=cut

define_profile search => (
    required => {
        type => enum(BAG_TYPES),
        query => NonEmptyStr,
    },
);

sub search {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    $self->log->trace("Performing full-text BAG search, term = '$args->{query}'");

    my $results = $self->connection->search(
        type  => $args->{type},
        query => $args->{query},
    );

    return [
        map { Zaaksysteem::Geo::BAG::Object->new(bag_object => $_) } @$results
    ];
}

=head2 get_exact

Search the BAG for a "nummeraanduiding" object that matches the given fields
(that have to match completely).

The values will be transformed and passed on to the L</connection>.

Requires the following named arguments:

=over

=item * postcode

=item * huisnummer

=item * huisletter (optional)

=item * huisnummer_toevoeging (optional)

=back

=cut

define_profile get_exact => (
    required => {
        postcode              => "Str",
        huisnummer            => "Num",
    },
    optional => {
        huisletter            => "Str",
        huisnummer_toevoeging => "Str",
    },
    field_filters => {
        # Normalize the postal code to no spaces, uppercase
        postcode => sub {
            my $pc = shift;
            $pc =~ s/ //g;
            return uc($pc);
        }
    }
);

sub get_exact {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $results = $self->connection->get_exact(
        type  => 'nummeraanduiding',
        fields => {
            postcode              => $args->{postcode},
            huisnummer            => $args->{huisnummer},
            huisletter            => $args->{huisletter} // '',
            huisnummer_toevoeging => $args->{huisnummer_toevoeging} // '',
        }
    );

    if (@$results == 0) {
        throw(
            'geo/bag/model/no_results',
            sprintf(
                'No results for exact BAG search for (postcode: %s, huisnummer: %s, huisletter: %s, toevoeging: %s)',
                $args->{postcode}              // '(none)',
                $args->{huisnummer}            // '(none)',
                $args->{huisletter}            // '(none)',
                $args->{huisnummer_toevoeging} // '(none)',
            ),
        );
    } elsif (@$results > 1) {
        $self->log->info(
            sprintf('Multiple (%d) results for exact BAG search; returning the first result', scalar @$results),
        );
    }

    return Zaaksysteem::Geo::BAG::Object->new(bag_object => $results->[0]);
}

=head2 find_nearest

Find the nearest BAG object of the specified type (with a maximum of 200 meters).

=cut

define_profile find_nearest => (
    required => {
        type      => enum(BAG_TYPES),
        latitude  => "Num",
        longitude => "Num",
    },
);

sub find_nearest {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    $self->log->trace(sprintf(
        "Performing nearest search, type = %s, lat = %s, lon = %s",
        $args->{type},
        $args->{latitude},
        $args->{longitude}
    ));

    my $result = $self->connection->find_nearest(
        type      => $args->{type},
        latitude  => $args->{latitude},
        longitude => $args->{longitude},
    );

    return unless $result;
    return unless @$result > 0;
    return Zaaksysteem::Geo::BAG::Object->new(bag_object => $result->[0]);
}

=head2 get

Retrieve a specific BAG object, specified by type and identifier.

=cut

sub get {
    my $self = shift;
    my $object_type = shift;
    my $object_id = shift;

    my $result = $self->connection->get(
        type => $object_type,

        # Get rid of "0" prefixes
        id   => int($object_id),
    );

    if ($result) {
        return Zaaksysteem::Geo::BAG::Object->new(bag_object => $result);
    }
    return;
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
