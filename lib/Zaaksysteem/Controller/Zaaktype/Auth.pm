package Zaaksysteem::Controller::Zaaktype::Auth;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Zaaksysteem::Controller::Zaaktype::Auth in Zaaktype::Auth.');
}

{
    Params::Profile->register_profile(
        method  => 'edit',
        profile => {
            required => [ qw/
            /],
            optional            => [ qw/
            /],
            constraint_methods  => {
            },
        }
    );

    sub edit : Chained('/zaaktype/base'): PathPart('auth/edit'): Args(0) {
        my ($self, $c) = @_;

        $c->stash->{params} = $c->session->{zaaktype_edit};
        $c->stash->{template} = 'zaaktype/auth/edit.tt';

        if ($c->req->params->{update}) {
            ### LOAD parameters
            $c->forward('load_parameters');

            $c->response->redirect($c->uri_for('/zaaktype/finish'));
            $c->detach;
        }
    }
}

sub load_parameters : Private {
    my ($self, $c) = @_;
    my (%role_args);

    ### Drop zaaktype_edit auth
    $c->session->{zaaktype_edit}->{auth} = {};


    $role_args{ $_ } = $c->req->params->{ $_ } for
        grep(/ou_.*?_\d+$/, keys %{ $c->req->params });

    ### Loop over variables
    for my $group (grep(
            /ou_id_(\d+)$/,
            keys %role_args
    )) {
        my $count   = $group;
        $count      =~ s/.*?(\d+)$/$1/g;

        my (@rechten, %recht);

        #if (!defined($c->req->params->{'ou_id_' . $count})) { next; }

        if (
            UNIVERSAL::isa($c->req->params->{'role_recht_' . $count}, 'ARRAY')
        ) {
            @rechten = @{
                $c->req->params->{'role_recht_' . $count}
            };
        } elsif ($c->req->params->{'role_recht_' . $count}) {
            push(
                @rechten,
                $c->req->params->{'role_recht_' . $count}
            );
        } else {
            next;
        }

        $recht{
            $c->req->params->{'ou_id_' . $count}
        } = {};

        my %selected;
        for my $auth (@rechten) {
            $selected{$auth} = 1;
        }

        # Update hash:
        $c->session->{zaaktype_edit}->{auth}->{$count}
            = {
                id      => $c->req->params->{'ou_id_' . $count},
                ou_id   => $c->req->params->{'ou_id_' . $count},
                role_id => $c->req->params->{'role_id_' . $count},
                rechten => \%selected,
            };
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 edit

TODO: Fix the POD

=cut

=head2 load_parameters

TODO: Fix the POD

=head2 index

TODO: Fix the POD

=cut

