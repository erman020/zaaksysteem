package Zaaksysteem::Controller::Beheer::Bibliotheek::Sjablonen;

use Moose;

use File::stat;

use Data::Dumper;
use BTTW::Tools;

use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS LOGGING_COMPONENT_SJABLOON/;

use constant SJABLONEN              => 'sjablonen';
use constant SJABLONEN_MODEL        => 'Bibliotheek::Sjablonen';
use constant SJABLONEN_DB           => 'DB::BibliotheekSjablonen';
use constant CATEGORIES_DB          => 'DB::BibliotheekCategorie';

BEGIN { extends 'Zaaksysteem::Controller' }

with 'MooseX::Log::Log4perl';

sub base : Chained('/') : PathPart('beheer/bibliotheek/sjablonen'): CaptureArgs(2) {
    my ( $self, $c, $bibliotheek_categorie_id, $id ) = @_;

    if($bibliotheek_categorie_id && $bibliotheek_categorie_id =~ m|^\d+$|) {
        $c->stash->{categorie} = $c->model('DB::BibliotheekCategorie')->find($bibliotheek_categorie_id);
    }

    $c->assert_any_user_permission(qw[beheer beheer_zaaktype_admin]);

    $c->stash->{bib_type}   = SJABLONEN;

    $self->_get_external_template_generators($c);

    if ($id) {
        my $entry = $c->model(SJABLONEN_DB)->find($id);

        my $base_uri = "/beheer/bibliotheek/$bibliotheek_categorie_id";
        unless ($entry) {
            $c->log->warn("Unable to find template with id '$id', redirecting to catalogue");
            $c->res->redirect($c->uri_for($base_uri));
            $c->detach;
        }

        $c->stash->{bib_entry} = $entry;

        if ($c->stash->{categorie}) {
            $c->add_trail({
                uri     => $c->uri_for($base_uri),
                label   => 'Categorie: ' . $c->stash->{categorie}->naam,
            });
        }

        $c->add_trail( {
            uri   => $c->uri_for("$base_uri/$id"),
            label => 'Sjabloon: ' . $c->stash->{bib_entry}->naam,
        });

    } else {
        $c->stash->{bib_new}    = 1;
    }
}

sub _get_external_template_generators {
    my ($self, $c) = @_;

    my $rs = $c->model('DB::Interface')->search_active({
        module => { '-in' => ['xential', 'stuf_dcr'] }
    });
    my %external;
    while (my $x = $rs->next) {
        $external{$x->name} = $x;
    }
    $c->stash->{external_template_engines} = \%external;
}



sub download : Chained('base') : PathPart('download') : Args(1) {
    my ($self, $c, $filestore_id) = @_;

    # Allow the download?
    $c->assert_any_user_permission(qw[beheer beheer_zaaktype_admin]);

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my ($filestore)    = $c->model('DB::Filestore')->search({id => $filestore_id});
    if (!$filestore) {
        $c->log->debug("Filestore entry with id $filestore_id not found.");
        return;
    }

    $c->serve_filestore($filestore);

    my $name = $filestore->original_name;
    $c->res->headers->header('Content-Disposition', "attachment; filename=\"$name\"");

    Zaaksysteem::StatsD->statsd->end('serve_file.sjablonen_download.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('serve_file.sjablonen_download', 1);
}


sub view : Chained('base'): PathPart(''): Args(0) {
    my ( $self, $c) = @_;

    $c->stash->{template} = 'beheer/bibliotheek/sjablonen/view.tt';
}

{
    Params::Profile->register_profile(
        method  => 'bewerken',
        profile =>
            'Zaaksysteem::Model::Bibliotheek::Sjablonen::bewerken',
    );

    sub bewerken : Chained('base'): PathPart('bewerken'): Args() {
        my ( $self, $c ) = @_;
        my ($dv);

        if ($c->stash->{bib_new}) {
            $c->stash->{bib_id} = 0;
        } else {
            $c->stash->{bib_id} = $c->stash->{bib_entry}->id;
        }

        if ($c->stash->{categorie}) {
            $c->stash->{categorie_id} =
                $c->stash->{categorie}->id;
        }

        $c->stash->{bib_cat}        = $c->model(CATEGORIES_DB)->search(
            {
                'system'    => undef,
            },
            {
                order_by    => ['pid','naam']
            }
        );

        my $requested_with = $c->req->header("x-requested-with") || '';

        ### Validation, PROUDLEVEL=7
        if ($c->req->params->{update}) {
            $c->stash->{categorie_id} =
                $c->req->params->{bibliotheek_categorie_id};
            my $validated = 0;


            ### Default validation
            if ($dv = $c->zvalidate) {
                my $name = $c->req->params->{naam};
                if (
                    $c->stash->{bib_new} &&
                    $c->model(SJABLONEN_MODEL)->sjabloon_exists(
                        'naam'  => $name,
                    )
                ) {
                    $c->log->info("Template with '$name' already exists");
                    $c->zcvalidate({ invalid => ['naam']});
                } else {
                    $validated = 1;
                }
            }

            if (
                !$validated || $c->req->is_xhr &&
                exists($c->req->params->{do_validation})
            ) {
                $c->detach;
            }

            ### Let's work our magic on the bibliotheek
            my $options = $dv->valid;

            if ($self->log->trace) {
                $self->log->trace("Creating template from controller with options %s", dump_terse($options));
            }

            if (
                my $sjabloon = $c->model(SJABLONEN_MODEL)->bewerken(
                    $options
                )
            ) {
                $c->model('DB::Logging')->trigger($c->stash->{ bib_new } ? 'template/create' : 'template/update', {
                    component => LOGGING_COMPONENT_SJABLOON,
                    component_id => $sjabloon->id,
                    data => {
                        template_id => $sjabloon->id,
                        reason => $options->{ commit_message }
                    }
                });

                if ($c->req->params->{json_response}) {
                    $c->stash->{json} = {
                        'id'    => $sjabloon->id
                    };
                    $c->detach('Zaaksysteem::View::JSONlegacy');
                }

                $c->push_flash_message('Sjabloon succesvol opgeslagen');
            }
        }

        if ($c->req->is_xhr) {
            if ($c->stash->{bib_id}) {
                $c->stash->{bib_entry} = $c->model(SJABLONEN_MODEL)->retrieve(
                    id  => $c->stash->{bib_id}
                );
            }

            $c->stash->{template} =
                'beheer/bibliotheek/sjablonen/edit.tt';

            $c->detach;
        }

        $c->res->redirect(
            $c->uri_for(
                '/beheer/bibliotheek/'
                . $c->stash->{categorie_id}
            )
        );
        $c->detach;
    }
}

sub verwijderen : Chained('base'): PathPart('verwijderen'): Args() {
    my ( $self, $c )    = @_;
    my $entry           = $c->stash->{bib_entry};

    return unless $entry;

    ### Confirmed
    my $flag_only = 0;
    if (
        $entry->zaaktype_sjablonens->count
    ) {
        ### in depth search
        my $used_in_zaaktype_sjablonen = $entry->zaaktype_sjablonens->search(undef, {
            prefetch => { 'zaaktype_node_id' => 'zaaktype_id' }
        });

        my $notused = 1;
        while (
            $notused &&
            (my $zt_sjabloon = $used_in_zaaktype_sjablonen->next)
        ) {
            my $node = $zt_sjabloon->zaaktype_node_id;
            if ($node->is_huidige_versie && !$node->zaaktype_id->deleted) {
                $c->stash->{confirmation}->{message} =
                    'Helaas, dit sjabloon is in gebruik door een of meerdere actieve zaaktypen.';
                $notused=0;
                next;
            }

            my $non_deleted_cases = $node->zaaks->search({
                status => { '!=' => 'deleted' }
            });

            ### Er is minstens 1 zaak _niet_ vernietigd
            if ($non_deleted_cases->count) {
                $c->log->debug('BibliotheekSjablonen can_remove checks: active case(s) found');

                $c->stash->{confirmation}->{message} =
                    'Helaas, dit sjabloon is in gebruik door een of meerdere zaken.';
                $notused = 0;
                next;
            } else {
                $flag_only = 1;
            }

            ### Ok, looks like it is not used, er is geen actieve zaaktype
            ### en de inactieve zaaktypen hebben allemaal geen zaken gekoppeld
            ### gehad... Free to wipe, notused=1
        }
        if (!$notused) {
            $c->stash->{confirmation}->{msgonly}    = '1';

            ### Msg
            $c->detach('/page/confirmation');
        }
    }

    ### Post
    if ( $c->req->params->{confirmed}) {
        $c->model('DB::Logging')->trigger('template/remove', {
            component => LOGGING_COMPONENT_SJABLOON,
            component_id => $entry->id,
            data => {
                template_id => $entry->id,
                reason => $c->req->param('commit_message')
            }
        });

        if ($flag_only) {
            $entry->deleted(DateTime->now());
            $entry->update;

            $c->log->debug(
                'Sjabloon ' . $entry->id . ' verwijderd dmv flag'
            );
        } else {
            ### Do not forget to delete magic strings
            $entry->bibliotheek_sjablonen_magic_strings->delete;
            $entry->delete;
            $c->log->debug(
                'Sjabloon ' . $entry->id . ' verwijderd'
            );
        }

        ### Msg
        $c->push_flash_message('Sjabloon succesvol verwijderd.');
        $c->res->redirect(
            $c->uri_for(
                '/beheer/bibliotheek/'
                . $entry->get_column('bibliotheek_categorie_id')
            )
        );

        $c->detach;
        return;
    }


    $c->stash->{confirmation}->{message}    =
        'Weet u zeker dat u dit sjabloon wilt verwijderen?'
        . ' Deze actie kan niet ongedaan gemaakt worden. Maar geen zorgen, dit
        sjabloon is niet in gebruik door een zaaktype';


    $c->stash->{confirmation}->{commit_message} = 1;
    $c->stash->{confirmation}->{type}       = 'yesno';

    $c->stash->{confirmation}->{uri}     = $c->uri_for(
                            '/beheer/bibliotheek/' . $c->stash->{bib_type} . '/'
                            . $entry->get_column('bibliotheek_categorie_id') . '/'
                            . $entry->id . '/verwijderen'
        );
    $c->forward('/page/confirmation');
    $c->detach;
}



sub search
    : Chained('/beheer/bibliotheek/base')
    : PathPart('sjablonen/search')
    : Args()
{
    my ( $self, $c )        = @_;

    return unless ($c->req->is_xhr);

    $c->stash->{bib_type}   = SJABLONEN;

    $c->stash->{bib_cat}        = $c->model(CATEGORIES_DB)->search(
        {
            'pid'       => undef,
        },
        {
            order_by    => 'naam'
        }
    );

    if ($c->req->params->{search}) {
        ### Return json response with results
        my $json = [];

        my %search_query    = ();
        $search_query{'lower(naam)'} = {
            'like' => '%' .  lc($c->req->params->{naam}) . '%'
        } if $c->req->params->{naam};
        $search_query{bibliotheek_categorie_id} =
            $c->req->params->{bibliotheek_categorie_id}
                if $c->req->params->{bibliotheek_categorie_id};

        $search_query{'deleted'}    = undef;

        my $kenmerken = $c->model(SJABLONEN_DB)->search(
            \%search_query,
            {
                order_by    => 'naam'
            }
        );

        while (my $kenmerk = $kenmerken->next) {
            push(@{ $json },
                {
                    'naam'                  => $kenmerk->naam,
                    'categorie'             => $kenmerk->bibliotheek_categorie_id->naam,
                    'id'                    => $kenmerk->id,
                }
            );
        }

        $c->stash->{json} = $json;
        $c->detach('Zaaksysteem::View::JSONlegacy');
    }

    $c->stash->{template} = 'widgets/beheer/bibliotheek/search.tt';
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CATEGORIES_DB

TODO: Fix the POD

=cut

=head2 LOGGING_COMPONENT_SJABLOON

TODO: Fix the POD

=cut

=head2 SJABLONEN

TODO: Fix the POD

=cut

=head2 SJABLONEN_DB

TODO: Fix the POD

=cut

=head2 SJABLONEN_MODEL

TODO: Fix the POD

=cut

=head2 bewerken

TODO: Fix the POD

=cut

=head2 download

TODO: Fix the POD

=cut

=head2 search

TODO: Fix the POD

=cut

=head2 verwijderen

TODO: Fix the POD

=cut

=head2 view

TODO: Fix the POD

=head2 base

TODO: Fix the POD

=cut

