package Zaaksysteem::Controller::Beheer::Bibliotheek::Kenmerken;

use Moose;

use Zaaksysteem::Backend::Tools::FilestoreMetadata qw(get_document_categories);

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_CONSTANTS
    LOGGING_COMPONENT_KENMERK
/;
use Zaaksysteem::Attributes;

use constant KENMERKEN              => 'kenmerken';
use constant KENMERKEN_MODEL        => 'Bibliotheek::Kenmerken';
use constant KENMERKEN_DB           => 'DB::BibliotheekKenmerken';
use constant MAGIC_STRING_DEFAULT   => 'doc_variable';
use constant CATEGORIES_DB          => 'DB::BibliotheekCategorie';

BEGIN { extends 'Zaaksysteem::Controller' }

=head2 base

Base controller for the /beheer/bibliotheek/kenmerken tree.

Mostly puts some frequently-used things on the stack for other controllers to use.

=cut

sub base : Chained('/') : PathPart('beheer/bibliotheek/kenmerken') : CaptureArgs(2) {
    my ( $self, $c, $bibliotheek_categorie_id, $id ) = @_;

    if($bibliotheek_categorie_id && $bibliotheek_categorie_id =~ m|^\d+$|) {
        $c->stash->{categorie} = $c->model('DB::BibliotheekCategorie')->find($bibliotheek_categorie_id);
    }
    $c->assert_any_user_permission(qw[beheer beheer_zaaktype_admin]);

    $c->stash->{bib_type}   = KENMERKEN;
    if ($id) {
        my $entry = $c->model(KENMERKEN_DB)->find($id);

        unless($entry) {
            $c->res->redirect($c->uri_for('/beheer/bibliotheek/' . $bibliotheek_categorie_id));
            $c->detach;
        }

        $c->stash->{ bib_entry } = $entry;

        $c->stash->{versions} = $c->model('DB::Logging')->search({
            event_type => ['attribute/update', 'attribute/create'],
            component_id => $entry->id
        }, {
            order_by => { '-desc' => 'id' }
        });

        $c->add_trail(
            {
                uri     => $c->uri_for('/beheer/bibliotheek/'
                    . $c->stash->{bib_type} . '/'
                    . $bibliotheek_categorie_id . '/'
                    . $id
                ),
                label   =>  'Kenmerk: ' . $c->stash->{bib_entry}->naam,
            }
        );

    } else {
        $c->stash->{bib_new}    = 1;
    }
}



{
    Params::Profile->register_profile(
        method  => 'bewerken',
        profile =>
            'Zaaksysteem::Model::Bibliotheek::Kenmerken::bewerken',
    );

    sub bewerken : Chained('base'): PathPart('bewerken'): Args() {
        my ( $self, $c ) = @_;
        my ($dv);

        my $params = $c->req->params;

        if ($c->stash->{bib_new}) {
            $c->stash->{bib_id} = 0;
        } else {
            if ( $c->stash->{bib_entry}->system ) {
                $c->push_flash_message(
                    'Standaard kenmerken kunnen niet '
                    . 'gewijzigd worden.'
                );
                $c->res->redirect(
                    $c->uri_for(
                        '/beheer/bibliotheek/'
                        . $c->stash->{categorie_id}
                    )
                );
                $c->detach;
            }
            $c->stash->{bib_id} = $c->stash->{bib_entry}->id;
        }

        if ($c->stash->{categorie}) {
            $c->stash->{categorie_id} =
                $c->stash->{categorie}->id;
        }

        $c->stash->{bib_cat}        = $c->model(CATEGORIES_DB)->search(
            {
                'system'    => [ undef, 0 ],
            },
            {
                order_by    => ['pid','naam']
            }
        );


        if ($params->{update}) {
            $c->stash->{categorie_id} = $params->{bibliotheek_categorie_id};

            my $validated = 1;
            ### Default validation
            unless ($dv = $c->zvalidate) {
                $validated = 0;
            }

            ### Make sure this name does not exist yet
            my $existing = $c->model(KENMERKEN_DB)->find({naam => $params->{kenmerk_naam}});
            if ($c->stash->{bib_new} && $existing) {

                $c->zcvalidate({
                    invalid => ['kenmerk_naam'],
                    msgs    => {
                        'kenmerk_naam'  => 'Kenmerk bestaat al in de categorie: ' .
                            ($existing->bibliotheek_categorie_id ? $existing->bibliotheek_categorie_id->naam : '')
                    },
                });

            } else {
                unless ($c->req->params->{kenmerk_type} eq 'file') {
                    ### Extended validation (magic string given and free?
                    if (
                        $c->stash->{bib_new} &&
                        (
                            !$c->req->params->{kenmerk_magic_string} ||
                            $c->req->params->{kenmerk_magic_string} ne
                            $c->model(KENMERKEN_DB)
                                ->generate_magic_string(
                                    $c->req->params->{kenmerk_magic_string}
                                )
                        )
                    ) {
                        $c->zcvalidate({ invalid => ['kenmerk_magic_string']});
                        my $validated = 0;
                    }
                }
            }

            if (
                !$validated || $c->req->is_xhr &&
                exists($c->req->params->{do_validation})
            ) {
                $c->detach;
            }


            ### Let's work our magic on the bibliotheek
            my $options = $dv->valid;

            ### Checkboxes
            $options->{kenmerk_besluit} = $dv->valid('kenmerk_besluit');

            if (my $kenmerk = $c->model(KENMERKEN_MODEL)->bewerken($options)) {

                if ($c->req->params->{json_response}) {
                    $c->stash->{json} = {id => $kenmerk->id};
                    $c->forward('Zaaksysteem::View::JSONlegacy');
                    $c->detach;
                }

                $c->push_flash_message('Kenmerk succesvol opgeslagen');
            } else {
                $c->push_flash_message('Fout bij aanmaken kenmerk');
            }
        }

        if ($c->req->is_xhr) {
            if ($c->stash->{bib_id}) {
                $c->stash->{bib_entry} = $c->model(KENMERKEN_MODEL)->get(
                    id  => $c->stash->{bib_id}
                );

                # need to work around this model mayhem
                # to get to my precious little sub
                $c->stash->{kenmerk_component} = $c->model(KENMERKEN_DB)->find($c->stash->{bib_id});
            }

            my @categories = get_document_categories;
            $c->stash->{document_categories} = \@categories;

            # Look if an "appointments" module is active. This will make the
            # input type appear accordingly.
            $c->stash->{appointment_interface} = $c->model('DB::Interface')->search_active({
                module => 'appointment'
            })->first;

            $c->stash->{template} =
                'beheer/bibliotheek/kenmerken/edit.tt';

            $c->detach;
        }

        $c->res->redirect(
            $c->uri_for(
                '/beheer/bibliotheek/'
                . $c->stash->{categorie_id}
            )
        );
        $c->detach;
    }
}

sub verwijderen : Chained('base'): PathPart('verwijderen'): Args() {
    my ( $self, $c )    = @_;
    my $entry           = $c->stash->{bib_entry};

    return unless $entry;

    if ( $entry->system ) {
        $c->push_flash_message(
            'Standaard kenmerken kunnen niet '
            . 'gewijzigd worden.'
        );
        $c->res->redirect(
            $c->uri_for(
                '/beheer/bibliotheek/'
                . $entry->get_column('bibliotheek_categorie_id')
            )
        );
        $c->detach;
    }

    ### Confirmed
    my $flag_only = 0;
    if (
        $entry->zaaktype_kenmerkens->count
    ) {
        ### in depth search
        my $used_in_zaaktype_kenmerken = $entry->zaaktype_kenmerkens->search(undef, {
            prefetch => { zaaktype_node_id => 'zaaktype_id' }
        });

        my $notused = 1;
        while (
            $notused &&
            (my $zt_kenmerk = $used_in_zaaktype_kenmerken->next)
        ) {
            my $node = $zt_kenmerk->zaaktype_node_id;

            if ($node->is_huidige_versie && !$node->zaaktype_id->deleted) {
                $c->stash->{confirmation}->{message} =
                    'Helaas, dit kenmerk is in gebruik door een of meerdere actieve zaaktypen.';
                $notused=0;
                next;
            }

            my $non_deleted_cases = $node->zaaks->search({
                status => { '!=' => 'deleted' }
            });

            ### Er is minstens 1 zaak _niet_ vernietigd
            if ($non_deleted_cases->count) {
                $c->log->debug('BibliotheekKenmerken can_remove checks: active case(s) found');

                $c->stash->{confirmation}->{message} =
                    'Helaas, dit kenmerk is in gebruik door een of meerdere zaken.';
                $notused = 0;
                next;
            } else {
                $flag_only = 1;
            }

            ### Ok, looks like it is not used, er is geen actieve zaaktype
            ### en de inactieve zaaktypen hebben allemaal geen zaken gekoppeld
            ### gehad... Free to wipe, notused=1
        }

        if (!$notused) {
            $c->stash->{confirmation}->{msgonly}    = '1';

            ### Msg
            $c->detach('/page/confirmation');
        }
    }

    ### Post
    if ( $c->req->params->{confirmed}) {
        $c->model('DB::Logging')->trigger('attribute/remove', {
            component => LOGGING_COMPONENT_KENMERK,
            component_id => $entry->id,
            data => {
                attribute_id => $entry->id,
                reason => $c->req->param('commit_message')
            }
        });

        if ($flag_only) {
            $entry->deleted(DateTime->now());
            $entry->update;

            $c->log->debug(
                'Kenmerk ' . $entry->id . ' verwijderd dmv flag'
            );
        } else {
            if ($entry->zaaktype_kenmerkens->count) {
                $entry->zaaktype_kenmerkens->delete;
            }

            $entry->delete;
            $c->log->debug(
                'Kenmerk ' . $entry->id . ' verwijderd'
            );
        }

        ### Msg
        $c->push_flash_message('Kenmerk succesvol verwijderd.');
        $c->res->redirect(
            $c->uri_for(
                '/beheer/bibliotheek/'
                . $entry->get_column('bibliotheek_categorie_id')
            )
        );

        $c->detach;
        return;
    }


    $c->stash->{confirmation}->{message}    =
        'Weet u zeker dat u dit kenmerk wilt verwijderen?'
        . ' Deze actie kan niet ongedaan gemaakt worden. Maar geen zorgen, dit
        kenmerk is niet in gebruik door actieve zaaktypen.';

    $c->stash->{confirmation}->{type}       = 'yesno';
    $c->stash->{confirmation}->{commit_message}       = 1;

    $c->stash->{confirmation}->{uri}     = $c->uri_for(
                            '/beheer/bibliotheek/' . $c->stash->{bib_type} . '/'
                            . $entry->get_column('bibliotheek_categorie_id') . '/'
                            . $entry->id . '/verwijderen'
        );

    $c->forward('/page/confirmation');
    $c->detach;
}

sub search
    : Chained('/')
    : PathPart('beheer/bibliotheek/kenmerken/search')
    : Args()
{
    my ( $self, $c )        = @_;

    return unless $c->req->is_xhr($c);

    $c->stash->{bib_type}   = KENMERKEN;

    $c->stash->{bib_cat}        = $c->model(CATEGORIES_DB)->search(
        {
            'system'    => [ undef, 0 ],
            'pid'       => undef,
        },
        {
            order_by    => ['pid','naam']
        }
    );


    my $params = $c->req->params();

    my $hide_veld_opties = {map {$_, 1} qw/bag_adressen bag_openbareruimtes bag_straat_adressen googlemaps file/};
    $c->stash->{hide_veld_opties} = $hide_veld_opties;
    $c->stash->{nowrapper} = 1;

    if ($params->{search}) {
        ### Return json response with results
        my $results = [];

        my $search_query = {};


        if ($params->{naam}) {
            $search_query->{'lower(naam)'} = {
                'like' => '%' .  lc($params->{naam}) . '%'
            };
        }

        if ($params->{kenmerk_type}) {
            $search_query->{value_type} = $params->{kenmerk_type};
        }

        if($params->{bibliotheek_categorie_id}) {
            $search_query->{bibliotheek_categorie_id} = $params->{bibliotheek_categorie_id};
        }

        $search_query->{system} = [ undef, 0 ];


# search_filter_post means this search is to setup search filters. (TRAC 168)
# not every type is useful for searching
# add a where clause to get rid of those
        if($params->{search_filter_post} && !exists $search_query->{value_type}) {
            $search_query->{value_type} = {
                '-not_in' => [keys %$hide_veld_opties]
            };
        }

        $search_query->{deleted}    = undef;


        my $kenmerken = $c->model(KENMERKEN_DB)->search(
            $search_query,
            {
                order_by    => 'naam'
            }
        );

        while (my $kenmerk = $kenmerken->next) {

            next unless $kenmerk->bibliotheek_categorie_id;
            push(@{ $results },
                {
                    'naam'          => $kenmerk->naam,
                    'invoertype'    =>
                        ZAAKSYSTEEM_CONSTANTS->{
                            'veld_opties'
                        }->{$kenmerk->value_type}->{label},
                    'categorie'     => $kenmerk->bibliotheek_categorie_id->naam,
                    'id'            => $kenmerk->id,
                }
            );
        }
        $c->stash->{results} = $results;
        $c->stash->{template} = 'widgets/beheer/bibliotheek/kenmerken/results.tt';

    } else {

        $c->stash->{search_filter_post} = $params->{search_filter_post};
        $c->stash->{template} = 'widgets/beheer/bibliotheek/search.tt';
    }
}


sub get_magic_string
    : Chained('/beheer/bibliotheek/base')
    : PathPart('kenmerken/get_magic_string')
    : Args()
{
    my ( $self, $c )        = @_;

    my $suggestion = $c->model(KENMERKEN_DB)
       ->generate_magic_string($c->req->params->{naam});
    $c->res->body($suggestion);
}


sub setup : Local {
    my ($self, $c) = @_;

    ### Sets up standaard kenmerken wanneer deze nog niet zijn aangemaakt
    my $zaaknummer = $c->model('DB::BibliotheekKenmerken')->search({
        'magic_string'  => 'zaaknummer',
        system  => 1,
    });

    my $cat;
    if ($zaaknummer->count) {
        $c->res->body(
            ($c->res->body||'') .
            '<br />OK (Systeemkenmerken bestaan al)'
        );

        $cat            = $c->model('DB::BibliotheekCategorie')->search(
            {
                naam    => 'Systeemkenmerken',
                system  => 1,
            }
        );

        $c->detach unless $cat->count;

        $cat = $cat->first;

    } else {
        $cat         = $c->model('DB::BibliotheekCategorie')->create(
            {
                naam    => 'Systeemkenmerken',
                label   => 'Systeemkenmerken',
                system  => 1,
            }
        );
    }

    my @kenmerken = ZAAKSYSTEEM_MAGIC_STRINGS;

    for my $kenmerk (@kenmerken) {
        my $old = $c->model('DB::BibliotheekKenmerken')->search(
            {
                naam        => $kenmerk,
                system      => 1,
            }
        );

        next if $old->count;

        $c->model('DB::BibliotheekKenmerken')->create(
            {
                naam        => $kenmerk,
                value_type  => 'text',
                label       => $kenmerk,
                description => $kenmerk,
                magic_string => $kenmerk,
                system      => 1,
                bibliotheek_categorie_id    => $cat->id
            }
        );
        $c->res->body(
            $c->res->body . '<br/>Added ' . $kenmerk
        );
    }

    $c->res->body(
        $c->res->body . '<br/>DONE'
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CATEGORIES_DB

TODO: Fix the POD

=cut

=head2 KENMERKEN

TODO: Fix the POD

=cut

=head2 KENMERKEN_DB

TODO: Fix the POD

=cut

=head2 KENMERKEN_MODEL

TODO: Fix the POD

=cut

=head2 LOGGING_COMPONENT_KENMERK

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 bewerken

TODO: Fix the POD

=cut

=head2 get_magic_string

TODO: Fix the POD

=cut

=head2 search

TODO: Fix the POD

=cut

=head2 setup

TODO: Fix the POD

=cut

=head2 verwijderen

TODO: Fix the POD

=cut

