package Zaaksysteem::Controller::Beheer::Object::Import;

use Moose;

use Moose::Util::TypeConstraints qw[enum];
use Params::Profile;

use JSON qw[encode_json];

use BTTW::Tools;
use Zaaksysteem::Object::Importer;
use Zaaksysteem::Object::ImportState;

BEGIN { extends 'Zaaksysteem::Controller' }

use constant OBJECT_TYPES => [
    {
        type => 'casetype',
        label => 'Zaaktypen',
        file_formats => [
            { type => 'zs', label => 'Zaaksysteem.nl' },
            { type => 'inav', label => 'I-Navigator (versie 1.5.02)' },
            { type => 'icon', label => 'I-Controller' },
            { type => 'ztc2', label => 'KING ZTC2.0 (Referentiezaaktypen)' }
        ]
    },

    {
        type => 'case',
        label => 'Zaken',
        file_formats => [
            { type => 'zs', label => 'Zaaksysteem.nl' }
        ]
    },

    {
        type => 'product',
        label => 'Producten en diensten',
        file_formats => [
            { type => 'zs', label => 'Zaaksysteem.nl' },
            { type => 'sim', label => 'Kluwer (SIM)' },
            { type => 'Sdu::Product', label => 'SDU' }
        ]
    },

    {
        type => 'qa',
        label => 'Vragen en antwoorden',
        file_formats => [
            { type => 'zs', label => 'Zaaksysteem.nl' },
            { type => 'sim', label => 'Kluwer (SIM)' },
            { type => 'Sdu::Faq', label => 'SDU' }
        ]
    }
];

sub base : Chained('/') : PathPart('beheer/object/import') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{ page_title } = 'Objecten importeren';
    $c->stash->{ object_types } = OBJECT_TYPES;
    $c->stash->{ state } = Zaaksysteem::Object::ImportState->new(
        %{ $c->session->{ _import_state } }
    );
}

sub index : Chained('base') : PathPart('') : Args(1) {
    my ($self, $c, $library_id) = @_;

    if (defined $library_id && $library_id =~ m[^\d+$]) {
        $c->stash->{ library_id } = $library_id;
    }

    if($c->stash->{ state }->import_fileref) {
        $c->stash->{ fileref } = $c->model('DB::Filestore')->find_by_uuid(
            $c->stash->{ state }->import_fileref
        );
    }

    $c->stash->{ nowrapper } = $c->req->is_xhr;

    $c->stash->{ template } = $c->req->is_xhr ?
        'beheer/object/import_config_form.tt' :
        'beheer/object/import.tt';
}

Params::Profile->register_profile(
    method => 'validate',
    profile => {
        required => [qw[object_type file_format filestore_uuid library_id]]
    }
);

sub validate : Chained('base') : PathPart('validate') : Args(0) {
    my ($self, $c) = @_;

    unless($c->req->method eq 'POST') {
        $c->res->redirect($c->uri_for('/beheer/object/import/' . $c->req->param('library_id')));
        $c->detach;
    }

    my $params = $c->req->params();
    my $filestore_uuid = $params->{ filestore_uuid };

    unless($filestore_uuid) {
        $c->forward('/upload/upload');
        $params->{filestore_uuid} = $c->stash->{uuid};
    }


    my $dv = Params::Profile->check(params => $params);

    if($dv->has_missing) {
        throw(
            'object/import/validate/missing_fields',
            'Error validating profile: missing field(s): ' . join(", ", $dv->missing)
        );
    }

    if($dv->has_invalid) {
        throw(
            'object/import/validate/invalid_fields',
            'Error validating profile: invalid field(s): ' . join(", ", $dv->invalid)
        );
    }

    my $fileref = $c->model('DB::Filestore')->find_by_uuid($dv->valid->{ filestore_uuid });

    unless($fileref) {
        die('Filestore did not contain referenced file.');
    }

    my $importer;
    my $state = Zaaksysteem::Object::ImportState->new(
        library_id => $dv->valid->{ library_id }
    );

    eval {
        $importer = Zaaksysteem::Object::Importer->new(
            format => $dv->valid->{ file_format },
            object_type => $dv->valid->{ object_type },
            schema => $fileref->result_source->schema,
            state => $state,
            model => $c->model('Object'),
        );

        $importer->hydrate_from_files($fileref);
    };

    if($@) {
        $c->log->debug('Unable to initialize importer, ' . $@);

        $c->push_flash_message('Importbestand niet geldig');

        $c->res->redirect($c->uri_for('/beheer/object/import/' . $dv->valid->{ library_id }));
        $c->detach;
    }

    $c->session->{ _import_state } = $state->get_state;

    $c->forward($importer->redirect) if($importer->redirect);

    $c->res->redirect($c->uri_for('/beheer/object/import/configure_items'));
    $c->detach;
}

=head2 configure_items

=head3 URL

C</beheer/object/import/configure_items>

=cut

sub configure_items : Chained('base') : PathPart('configure_items') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ fileref } = $c->model('DB::Filestore')->find_by_uuid($c->stash->{ state }->import_fileref);

    $c->stash->{ bib_cat } = $c->model('DB::BibliotheekCategorie')->search(
        { system => undef, pid => undef },
        { order_by => ['pid', 'naam'] }
    );

    $c->stash->{ template } = 'beheer/object/import_items_options.tt';
}

=head2 run

=head3 URL

C</beheer/object/import/run>

=cut

sub run : Chained('base') : PathPart('run') : Args(0) {
    my ($self, $c) = @_;

    my @unfinished = $c->stash->{ state }->find_by('selected_option', '');

    if(scalar(@unfinished)) {
        $c->push_flash_message('Er zijn nog items zonder geconfigureerde actie');
        $c->res->redirect($c->uri_for('/beheer/object/import/configure_items'));
        $c->detach;
    }

    my $importer = Zaaksysteem::Object::Importer->new(
        schema => $c->model('DB::Filestore')->search({})->result_source->schema,
        state => $c->stash->{ state },
        format => $c->stash->{ state }->format,
        object_type => $c->stash->{ state }->object_type,
        model => $c->model('Object'),
    );

    $importer->execute;

    $c->session->{ _import_state } = $c->stash->{ state }->get_state;
    $c->res->redirect($c->uri_for('/beheer/object/import/finish'));
    $c->detach;
}

=head2 set_library_id

Update import state in session with selected bibliotheek_categorie. Opted
not to make this into a proper ZAPI controller because it would make the
whole module inconsistent.

library_id is an alias for bibliotheek_categorie_id and will be used to
position the imported objects in the casetype catalogue.

=cut

define_profile set_library_id => (
    required => { library_id => 'Int' }
);

sub set_library_id : Chained('base') : PathPart('set_library_id') : Args(0) {
    my ($self, $c) = @_;

    my $import_state = $c->stash->{ state };
    my $params = assert_profile($c->req->params)->valid;

    $import_state->library_id($params->{ library_id });

    $c->session->{ _import_state } = $import_state->get_state;

    $c->stash->{json} = {success => 1};
    $c->forward('Zaaksysteem::View::JSONlegacy');
}

=head2 save_option

=head3 URL

C</beheer/object/oimport/save_option>

=head3 Parameters

=over 4

=item item_id

This required parameter is the unique identifier for the import item. It can
exclusively be interpreted as a string, no further type-checking is done.

=item option

This required parameter indicates the currently selected option for the item.

=item process_item

This optional parameter indicates if the item should be processed at all. This
replaces the C<ignore> option.

=back

=cut

define_profile save_option => (
    required => {
        item_id => 'Str',
        option => 'Str',
    },
    optional => {
        process_item => enum([qw[on]])
    }
);

sub save_option : Chained('base') : PathPart('save_option') : Args(0) {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    unless($c->req->method eq 'POST') {
        $c->detach;
    }

    my $item = $c->stash->{ state }->get_by('id', $opts->{ item_id });

    my $selected = 'ignore';

    if($opts->{ process_item }) {
        $selected = $opts->{ option } eq 'ignore' ? $item->default_option : $opts->{ option };
    }

    my %config;

    for my $key (grep { $_ =~ m[^${selected}\.] } keys %{ $opts }) {
        my ($config_key) = $key =~ m|^${selected}\.(.*)$|;

        $config{ $config_key } = $opts->{ $key };
    }

    $item->select_option($selected, \%config);

    # Save modified state back to the session
    $c->session->{ _import_state } = $c->stash->{ state }->get_state;

    $c->res->content_type('application/json');
    $c->res->body(encode_json($item->selected));
}

=head2 finish

=head3 URL

C</beheer/object/import/finish>

=cut

sub finish : Chained('base') : PathPart('finish') : Args(0) {
    my ($self, $c) = @_;

    if($c->stash->{ state }->import_fileref) {
        $c->stash->{ fileref } = $c->model('DB::Filestore')->find_by_uuid(
            $c->stash->{ state }->import_fileref
        );
    }

    $c->session->{ _import_state } = { };
    $c->stash->{ template } = 'beheer/object/import_finish.tt';
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 OBJECT_TYPES

TODO: Fix the POD

=cut

=head2 base

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 validate

TODO: Fix the POD

=cut

