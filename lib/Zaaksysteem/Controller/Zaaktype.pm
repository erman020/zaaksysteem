package Zaaksysteem::Controller::Zaaktype;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

=head2 base

Base controller for the /zaaktype tree.

=cut

sub base : Chained('/') : PathPart('zaaktype'): CaptureArgs(0) {
    my ($self, $c) = @_;

    if ($c->req->path =~ /zaaktype\/categorie/) {
        delete($c->session->{zaaktype_edit});
    }

    $self->_load_trails($c);
}

sub _load_trails {
    my ($self, $c) = @_;

    if ($c->session->{zaaktype_edit}->{category}) {
        my $cat = $c->model('DB::ZaaktypeCategorie')->find(
                $c->session->{zaaktype_edit}->{category}
            );

        if ($cat) {
            $c->add_trail(
                {
                    uri     => $c->uri_for('/zaaktype/categorie/' . $cat->id),
                    label   => 'Categorie: ' . $cat->categorie,
                }
            );
        }
    }

    if ($c->session->{zaaktype_edit}->{edit}) {
        if (my $zn = $c->model('DB::ZaaktypeNode')->find(
                $c->session->{zaaktype_edit}->{edit}
            )
        ) {
            $c->add_trail(
                {
                    uri     => $c->uri_for('/zaaktype/edit/' . $zn->id),
                    label   => 'Zaaktype: ' . $zn->titel,
                }
            );
        }
    }


}

sub begin : Private {
    my ($self, $c, $id) = @_;


    #$c->assert_user_role(qw/admin/);

    $c->forward('/begin');

    if ($c->session->{zaaktype_edit}->{category}) {
        $c->stash->{categorie}
            = $c->model('DB::ZaaktypeCategorie')->find(
                $c->session->{zaaktype_edit}->{category}
            );
    }

    $c->add_trail(
        {
            uri     => $c->uri_for('/zaaktype'),
            label   => 'Zaaktypebeheer',
        }
    );

}

sub view : Chained('base'): PathPart(''): Args(1) {
    my ($self, $c, $id) = @_;

    ### Forward to first section
    # Initiate zaaktype
    delete($c->session->{zaaktype_edit});
    $c->session->{zaaktype_edit} = $c->model('Zaaktype')->find($id);
    $c->stash->{zaaktype_view} = 1;

    $c->stash->{template}   = 'zaaktype/finish.tt';

    $c->stash->{categorie}
        = $c->model('DB::ZaaktypeCategorie')->find(
            $c->session->{zaaktype_edit}->{category}
        );

    $c->add_trail(
        {
            uri     => $c->uri_for('/zaaktype/categorie/' . $c->stash->{categorie}->id),
            label   => $c->stash->{categorie}->categorie
        }
    );

    $c->add_trail(
        {
            uri     => $c->uri_for('/zaaktype/' . $id),
            label   => 'Zaaktype overzicht'
        }
    );
}

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->forward('/zaaktype/category/index');
}

sub add : Chained('base'): PathPart('add'): Args(1) {
    my ($self, $c, $catid) = @_;

    ### Forward to first section
    # Initiate zaaktype
    delete($c->session->{zaaktype_edit});
    $c->session->{zaaktype_edit}->{category}    = $catid;
    $c->stash->{categorie}  = $c->model('DB::ZaaktypeCategorie')->find($catid);

    $c->session->{zaaktype_edit}->{create}      = 1;

    $c->forward('/zaaktype/algemeen/edit');
}

{
    Params::Profile->register_profile(
        method  => 'verplaats',
        profile => {
            required => [ qw/
                zaaktype_id
                categorie_id
            /],
            constraint_methods => {
                zaaktype_id     => qr/^\d+$/,
                categorie_id     => qr/^\d+$/,
            }
        }
    );

    sub verplaats : Chained('base'): PathPart('verplaats'): Args(1) {
        my ($self, $c, $zaaktype_id) = @_;

        my $zaaktype = $c->model('Zaaktype')->retrieve(
            id  => $zaaktype_id
        ) or return;

        ### VAlidation
        if ($c->req->is_xhr &&
            $c->req->params->{do_validation}
        ) {
            $c->zvalidate;
            $c->detach;
        }

        ### Post
        if (
            %{ $c->req->params } &&
            $c->req->params->{categorie_id}
        ) {
            $c->res->redirect(
                $c->uri_for('/zaaktype')
            );

            ### Confirmed
            my $dv;
            return unless $dv = $c->zvalidate;

            my $zaaktype_edit = $c->model('Zaaktype')->get($zaaktype->id);

            $zaaktype_edit->{category} = $c->req->params->{categorie_id};

            $c->model('Zaaktype')->create($zaaktype_edit);

            my $db_zt = $zaaktype->ztno->zaaktype_id;
            $db_zt->zaaktype_categorie_id(
                $c->req->params->{categorie_id}
            );

            $db_zt->update;


            ### Msg
            $c->push_flash_message('Zaaktype succesvol verplaatst.');

            (my $categorie_id = $c->req->params->{categorie_id}) =~ s/[^0-9]//g;
            $c->res->redirect($c->uri_for('/zaaktype/categorie/' .  $c->req->params->{categorie_id}));

            $c->detach;
        }

       $c->stash->{zaaktype_categorien} =
            $c->model('DB::ZaaktypeCategorie')->search({});

        $c->stash->{zaaktype_id} = $zaaktype->id;
        $c->stash->{zaaktype} = $zaaktype;
        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = 'zaaktype/widgets/verplaats.tt';
    }
}

sub edit : Chained('base'): PathPart('edit'): Args() {
    my ($self, $c, $id, $zaaktype_component) = @_;

    $c->session->{zaaktype_edit} = $c->model('Zaaktype')->find($id);
    $c->stash->{categorie}
        = $c->model('DB::ZaaktypeCategorie')->find(
            $c->session->{zaaktype_edit}->{category}
        );

    $self->_load_trails($c);

    if ($zaaktype_component && $zaaktype_component eq 'auth') {
        $c->forward('/zaaktype/auth/edit');
        $c->session->{zaaktype_auth_only} = 1;
    } else {
        $c->forward('/zaaktype/algemeen/edit');
    }
}

sub clone : Chained('base'): PathPart('clone'): Args(1) {
    my ($self, $c, $id) = @_;

    $c->session->{zaaktype_edit} = $c->model('Zaaktype')->duplicate($id);
    $c->stash->{categorie}
        = $c->model('DB::ZaaktypeCategorie')->find(
            $c->session->{zaaktype_edit}->{category}
        );

    $c->forward('/zaaktype/algemeen/edit');
}

=head2 delete

Remove a case type.

=cut

{
    Params::Profile->register_profile(
        method  => 'delete',
        profile => {
            required => [ qw/
                zaaktype_id
            /],
            constraint_methods => {
                zaaktype_id     => qr/^\d+$/,
            }
        }
    );

    sub delete : Chained('base'): PathPart('delete'): Args() {
        my ($self, $c, $id) = @_;

        ### VAlidation
        if ($c->req->is_xhr &&
            $c->req->params->{do_validation}
        ) {
            $c->zvalidate;
            $c->detach;
        }

        ### Post
        if (
            %{ $c->req->params } &&
            $c->req->params->{confirmed}
        ) {
            $c->res->redirect(
                $c->uri_for('/zaak/intake', { scope => 'documents' })
            );

            ### Confirmed
            my $dv;
            return unless $dv = $c->zvalidate;

            $c->model('Zaaktype')->delete($dv->valid('zaaktype_id'));

            ### Msg
            $c->push_flash_message('Zaaktype succesvol verwijderd.');

            $c->res->redirect('/zaaktype');
            $c->detach;
            return;
        }


        $c->stash->{confirmation}->{message}    =
            'Weet u zeker dat u dit zaaktype wilt verwijderen?'
            . ' Deze actie kan niet ongedaan gemaakt worden. Lopende zaken'
            . ' onder dit zaaktype kunnen worden afgerond, echter, er kunnen'
            . ' geen nieuwe zaken meer worden aangemaakt.';

        $c->stash->{confirmation}->{type}       = 'yesno';

        $c->stash->{confirmation}->{params}     = {
            'zaaktype_id'   => $id
        };

        $c->forward('/page/confirmation');
        $c->detach;
    }
}


sub searchOBSOLETE : Chained('/'): PathPart('zaaktype/searchOBSOLETE'): Args(0) {
    my ($self, $c) = @_;

    unless ($c->req->is_xhr) {
        $c->response->redirect('/');
        $c->detach;
    }

   $c->stash->{zaaktype_categorien} =
        $c->model('DB::BibliotheekCategorie')->search(
            {   pid => undef },
            {
                order_by    => 'naam'
            }
        );

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'widgets/zaaktype/search_popup.tt';
    $c->stash->{search_filter_post} = $c->req->params->{'search_filter_post'};

    ### Internal post, give results

    if (%{ $c->req->params } && $c->req->params->{search}) {
        my %opts;

        $opts{bibliotheek_categorie_id} = $c->req->params->{zaaktype_categorie}
            if ($c->req->params->{zaaktype_categorie});

        $opts{zaaktype_titel} = $c->req->params->{zaaktype_naam}
            if ($c->req->params->{zaaktype_naam});


        $opts{zaaktype_trefwoorden} = $c->req->params->{zaaktype_trefwoorden}
            if ($c->req->params->{zaaktype_trefwoorden});

        $opts{zaaktype_omschrijving} = $c->req->params->{zaaktype_omschrijving}
            if ($c->req->params->{zaaktype_omschrijving});


        $opts{zaaktype_betrokkene_type} = $c->req->params->{jsbetrokkene_type}
            if (
                $c->req->params->{jsbetrokkene_type} &&
                $c->req->params->{jsbetrokkene_type} ne 'undefined'
            );

        $opts{zaaktype_trigger} = $c->req->params->{jstrigger}
            if (
                $c->req->params->{jstrigger} &&
                $c->req->params->{jstrigger} ne 'undefined'
            );

        $opts{show_inactive} = $c->req->params->{show_inactive};

        $c->stash->{zaaktypen} = $c->model('Zaaktype')->list(\%opts);

        if ($c->req->params->{json_response}) {
            my (@zaaktypen);
            while (my $zt = $c->stash->{zaaktypen}->next) {
                push(@zaaktypen,
                    {
                        naam    => $zt->zaaktype_node_id->titel,
                        nid      => $zt->get_column('zaaktype_node_id'),
                        id       => $zt->id
                    }
                );
            }
            $c->stash->{json} = {
                'zaaktypen'    => \@zaaktypen,
            };
            $c->forward('Zaaksysteem::View::JSONlegacy');
            $c->detach;
        }

        $c->stash->{template} = 'widgets/zaaktype/search_resultrows.tt';
    }
}


sub search : Chained('/'): PathPart('zaaktype/search'): Args(0) {
    my ($self, $c) = @_;

    die "only xmlhttprequests" unless($c->req->is_xhr);

    my $params = $c->req->params();

    $c->stash->{nowrapper} = 1;

    if($params->{search}) {

        my $entries = $c->model('DB::Zaaktype')->search_with_options({
            trigger => $params->{trigger},
            betrokkene_type => $params->{betrokkene_type},
            term => $params->{term},
        });

        unless($params->{show_offline}) {
            $entries = $entries->search({'me.active' => 1});
        }

        $c->stash->{total}        = $entries->count;

        my $limited_entries = $entries->search({},{ rows => 100 });
        $c->stash->{count}         = $limited_entries->count;

        $c->stash->{template} = 'zaaktype/search/result.tt';
        $c->stash->{zaaktypen} = $limited_entries;
    } else {

        my $options = {};

        foreach my $option (qw/trigger betrokkene_type show_offline/) {
            if (exists $params->{$option}) {
                $options->{$option} = $params->{$option};
            }
        }

        $c->stash->{options} = $options;
        $c->stash->{template} = 'zaaktype/search/index.tt';
    }
}

sub finish : Chained('base'): PathPart('finish'): Args(0) {
    my ($self, $c) = @_;

    unless (%{ $c->req->params } && $c->req->params->{confirm}) {
        $c->stash->{template} = 'zaaktype/finish.tt';
        $c->detach;
    }

    ### Voer deze info aan ons almachtige sexy modelletje, fat model wel te
    ### verstaan, niet zo'n slanke den
    ###### Classic
    $c->model('Zaaktype')->create($c->session->{zaaktype_edit});

    $c->res->redirect($c->uri_for('/zaaktype'));

    delete($c->session->{zaaktype_edit});
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 add

TODO: Fix the POD

=cut

=head2 begin

TODO: Fix the POD

=cut

=head2 clone

TODO: Fix the POD

=cut

=head2 delete

TODO: Fix the POD

=cut

=head2 edit

TODO: Fix the POD

=cut

=head2 finish

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 search

TODO: Fix the POD

=cut

=head2 searchOBSOLETE

TODO: Fix the POD

=cut

=head2 verplaats

TODO: Fix the POD

=cut

=head2 view

TODO: Fix the POD

=cut

