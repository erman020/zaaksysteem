package Zaaksysteem::Controller::API::Case;

use Moose;

use List::MoreUtils qw[none];

use BTTW::Tools;
use Zaaksysteem::ZTT;

use JSON;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Case - ZAPI Case controller

=head1 DESCRIPTION

Case API. Based on "old" case backend.

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/base') : PathPart('case') : CaptureArgs(1) {
    my ($self, $c, $case_id) = @_;

    $c->stash->{ zaak } = try {
        return $c->model('DB::Zaak')->find($case_id)
    } catch {
        throw('api/case/retrieval_fault', sprintf(
            'Internal error while retrieving case by id "%s"',
            $case_id
        ));
    };

    if (!defined $c->stash->{zaak} || $c->stash->{zaak}->is_deleted) {
        throw('api/case/find_case', sprintf(
            'Unable to find case by id "%s"',
            $case_id
        ));
    }

    unless($c->check_any_zaak_permission(qw[zaak_read zaak_beheer zaak_edit])) {
        throw('api/case/authorization', 'Access denied');
    }

    $c->stash->{ zapi } = [];
    $c->stash->{ case } = $c->stash->{ zaak }->object_data;

    # We're special, this controller has a seperate implementation, we don't
    # want the Object model to get in our way.
    $c->model('Object')->prefetch_relations(0);
}

=head2 caseless_base

This base action exists such that APIs that are considered part of the Case
domain can chain off the API like L<Zaaksysteem::Controller::API::Case::File>
does, but without a case number.

=cut

sub caseless_base : Chained('/api/base') : PathPart('case') : CaptureArgs(0) { }

=head2 get

=head3 URL

C</api/case/[CASE_ID]>

=cut

sub get: Chained('base') : PathPart('') : Args(0): ZAPI {
    my ($self, $c) = @_;

    my $output = $c->stash->{ case }->TO_JSON;

    $output->{ case } = {
        checklist       => $self->_retrieve_checklist($c),
        case_actions    => $self->_retrieve_actions($c),
        relations       => $self->_retrieve_relations($c),
        case_documents  => $self->_retrieve_documents($c),
        pending_changes => $self->_retrieve_pending_changes($c),
        class_uuid      => $c->stash->{case}->class_uuid->id,
        permissions     => $c->retrieve_list_of_zaak_permissions($c->stash->{zaak}),
    };

    $output->{related_objects} = $self->_retrieve_related_objects($c, $c->stash->{case});
    $output->{type} = 'case';

    $output->{ values } = {
        %{ $output->{ values } },
        $self->_retrieve_mutations($c)
    };

    $c->stash->{zaak}->log_view();
    $c->stash->{zapi} = [ $output ];

    return;
}

=head2 confidentiality

Set a new confidentiality category for the referenced case.

=head3 URL

C</api/case/[CASE_ID]/confidentiality>

=head3 Parameters

=over 4

=item confidentiality

=back

=head3 Response

Return value: ZAPI on success

    {
        results: {
            message: 'Confidentiality updated',
            value: $params->{confidentiality}
        }
    }

=cut

### TODO: Parameter checking
sub confidentiality: Chained('base') : PathPart('confidentiality') : ZAPI {
    my ($self, $c) = @_;

    throw ('api/case/confidentiality', 'access violation')
        unless $c->check_any_zaak_permission('zaak_read','zaak_beheer','zaak_edit');

    throw ('api/case/confidentiality/method_not_post', 'only POST requests allowed')
        unless $c->req->method eq 'POST';

    my $new_value = $c->req->params->{confidentiality};

    $c->stash->{zaak}->set_confidentiality($new_value);
    $c->stash->{zaak}->update;

    $c->stash->{zapi} = [{
        message => 'Confidentiality updated',
        value   => $c->stash->{zaak}->confidentiality
    }];
}

=head2 request_attribute_update

request update to a field. the citizens are not allowed to change fields
without an official reviewing first, this sends a request to the official.

=head3 URL

C</api/case/[CASE_ID]/request_attribute_update>

=cut

sub request_attribute_update : Chained('base') : PathPart('request_attribute_update') : ZAPI {
    my ($self, $c, $bibliotheek_kenmerken_id) = @_;

    my $params = $c->req->params;

    throw('api/case/request_attribute_update', "need bibliotheek_kenmerken_id")
        unless $bibliotheek_kenmerken_id;

    my $kenmerk = $c->stash->{zaak}->zaaktype_node_id->zaaktype_kenmerken->search_rs(
        {
            bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id
        },
        {
            prefetch => 'bibliotheek_kenmerken_id'
        },
    )->first;

    if (!$kenmerk) {
        throw('pip/update_field', "Couldn't find requested attribute.");
    }

    # get it, put on the stash where we need it
    $c->forward('stash_submitted_field_value', [ $kenmerk, $params ]);

    my $subject_identifier = $c->stash->{ subject_identifier } // ($c->user_exists ? $c->user->betrokkene_identifier : undef);

    if ($self->_can_skip_change_approval($c, $kenmerk)) {
        $c->stash->{ zaak }->zaak_kenmerken->update_fields({
            new_values => { $bibliotheek_kenmerken_id => $c->stash->{ veldoptie_value } },
            zaak       => $c->stash->{zaak},
        });
    }
    else {
        my $scheduled_job = $c->model('DB')->resultset('ScheduledJobs')->create_task({
            task                        => 'case/update_kenmerk',
            bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
            value                       => $c->stash->{ veldoptie_value },

            # as soon as the subjects table /user management system
            # include the citizens we could add them here
            created_by                  => $subject_identifier,
            reason                      => $params->{ toelichting },
            case                        => $c->stash->{ zaak },
        });
        # Update num_unaccepted_XXXX fields in the case object
        $c->stash->{zaak}->touch();

        $self->_log_update_field($c, {
            subject_name => $scheduled_job->format_created_by,
            reason       => $params->{toelichting},
            new_values   => $c->stash->{ veldoptie_value },
            value_type   => $kenmerk->bibliotheek_kenmerken_id->value_type,
            kenmerk      => $kenmerk->label || $kenmerk->bibliotheek_kenmerken_id->naam,

            # If coming from PIP, other event type
            $c->session->{pip} ? (event_type => 'case/pip/updatefield')  : (),
        });
    }

    # this functionality will quickly become viable, so let's keep it in commented form
    #$c->forward('update_field_message', [$scheduled_job->apply_roles->description]);
    $c->stash->{ template } = 'widgets/general/veldoptie_view.tt';
    $c->stash->{ nowrapper } = 1;

    $c->stash->{ veldoptie_multiple } = $kenmerk->bibliotheek_kenmerken_id->type_multiple;
    $c->stash->{ veldoptie_type } = $kenmerk->type;
    # veldoptie_value is put on the stash by sub-action

    my $html = $c->view('TT')->render($c, 'widgets/general/veldoptie_view.tt');
    $c->stash->{zapi} = [{ attribute_value_as_html => $html }];
}

sub _can_skip_change_approval {
    my $self = shift;
    my ($c, $kenmerk) = @_;

    # Appointment updates don't require approval.
    return 1 if ($kenmerk->bibliotheek_kenmerken_id->value_type eq 'appointment');
    return 1 if ($kenmerk->bibliotheek_kenmerken_id->value_type eq 'calendar_supersaas');

    # In other cases: only logged-in users with special permission can skip the
    # approval queue, and the only for specially configured attributes.
    return if not $c->user_exists;
    return if not $kenmerk->properties->{skip_change_approval};

    return 1 if $c->check_field_permission($kenmerk);

    return;
}

=head2 stash_submitted_field_value

B<PRIVATE>

=cut

sub stash_submitted_field_value : Private {
    my ($self, $c, $kenmerk, $params) = @_;

    my $key = 'kenmerk_id_' . $kenmerk->get_column('bibliotheek_kenmerken_id');
    my $value = $params->{ $key };

    # if no checkboxes are checked, we will receive nothing, which would be OK
    if ($params->{ $key . '_checkbox' }) {
        $value //= [];
    }

    unless (exists $params->{$key} || exists $params->{"${key}_checkbox"}) {
        throw('pip/field_value', 'Unable to get value for attribute "%s"', $kenmerk->bibliotheek_kenmerken_id->naam);
    }

    $c->stash->{ veldoptie_value } = $value;
}

=head2 end

Implements logic to be executed after every case api request.

=cut

sub end {
    my ($self, $c) = @_;

    $c->forward('/broadcast_queued_items');

    return;
}

=head1 HELPER METHODS

=head2 _retrieve_documents

=cut

sub _retrieve_documents {
    my ($self, $c)  = @_;

    my $case        = $c->stash->{ zaak };

    my $case_type_properties = $case->zaaktype_node_id->zaaktype_kenmerken;

    my @case_documents  = $case_type_properties->search(
        {
            'bibliotheek_kenmerken_id.value_type' => 'file',
        },
        {
            join => [
                'bibliotheek_kenmerken_id',
            ],
            prefetch => [
                'zaaktype_node_id',
                'bibliotheek_kenmerken_id'
            ]
        }
    )->all;

    return \@case_documents;
}

=head2 _retrieve_related_objects

=cut

sub _retrieve_related_objects {
    my ($self, $c, $row) = @_;

    # This spot right here is out of reach of the automated deep_relations
    # behaviour found in ZS::Object::Model, so it's explicit use is warrented
    # here.
    my $deep = $c->req->param('deep_relations') ? 1 : 0;

    my @relations;

    my $object1_rels_opts;
    my $object2_rels_opts;

    # Use deep-prefetching so we minimize queries when applicable
    if ($deep) {
        $object1_rels_opts = {
            prefetch => { object2_uuid => 'object_relation_object_ids' }
        };

        $object2_rels_opts = {
            prefetch => { object1_uuid => 'object_relation_object_ids' }
        };
    }

    my $object1_rels = $row->object_relationships_object1_uuids->search(
        undef,
        $object1_rels_opts
    );

    my $object2_rels = $row->object_relationships_object2_uuids->search(
        undef,
        $object2_rels_opts
    );

    for my $rel ($object1_rels->all) {
        my $relation = Zaaksysteem::Object::Relation->new(
            related_object_id   => $rel->get_column('object2_uuid'),
            related_object_type => $rel->object2_type,
            relationship_name_a => $rel->type1,
            relationship_name_b => $rel->type2,
            blocks_deletion     => $rel->blocks_deletion,
        );

        if($deep) {
            my $related = $rel->object2_uuid;

            # Special-snowflake case objects are not guaranteed to have
            # initialized casetype relations
            if ($related->object_class eq 'case') {
                my @object_relations = $related->object_relation_object_ids->all;

                if (none { $_->object_type eq 'casetype' } @object_relations) {
                    $related->_init_casetype_relation;
                    $related->discard_changes;
                }
            }

            $relation->related_object(
                $c->model('Object')->inflate_from_row($related)
            );
        }

        push @relations, $relation;
    }

    for my $rel ($object2_rels->all) {
        my $relation = Zaaksysteem::Object::Relation->new(
            related_object_id   => $rel->get_column('object1_uuid'),
            related_object_type => $rel->object1_type,
            relationship_name_a => $rel->type2,
            relationship_name_b => $rel->type1,
            blocks_deletion     => $rel->blocks_deletion,
        );

        if($deep) {
            my $related = $rel->object1_uuid;

            # Special-snowflake case objects are not guaranteed to have
            # initialized casetype relations
            if ($related->object_class eq 'case') {
                my @object_relations = $related->object_relation_object_ids->all;

                if (none { $_->object_type eq 'casetype' } @object_relations) {
                    $related->_init_casetype_relation;
                    $related->discard_changes;
                }
            }

            $relation->related_object(
                $c->model('Object')->inflate_from_row($related)
            );
        }

        push @relations, $relation;
    }

    return \@relations;
}

=head2 _retrieve_relations

=cut

sub _retrieve_relations {
    my ($self, $c) = @_;

    my @relations;

    my $rels1 = $c->stash->{ case }->object_relationships_object1_uuids->search({ type1 => 'related' });
    my $rels2 = $c->stash->{ case }->object_relationships_object2_uuids->search({ type2 => 'related' });
    my $model = $c->model('Object');

    for my $rel1 ($rels1->all) {
        my $object_data = $rel1->object2_uuid;

        my $label = try {
            return $c->model('Object')->inflate_from_row($object_data)->TO_STRING;
        } catch {
            return 'INVALIDE';
        };

        push @relations, {
            object_type => $object_data->object_class,
            object_label => $label,
            object_uuid => $object_data->uuid,
        };
    }

    for my $rel2 ($rels2->all) {
        my $object_data = $rel2->object1_uuid;

        my $label = try {
            return $c->model('Object')->inflate_from_row($object_data)->TO_STRING;
        } catch {
            return 'INVALDE';
        };

        push @relations, {
            object_type => $object_data->object_class,
            object_label => $label,
            object_uuid => $object_data->uuid,
        };
    }

    return \@relations;
};

=head2 _retrieve_mutations

=cut

sub _retrieve_mutations {
    my ($self, $c) = @_;

    my %retval;
    my $ztt_cache = {};

    for my $attr ($c->stash->{ zaak }->zaaktype_node_id->zaaktype_kenmerken->all) {
        next unless $attr->get_column('object_id');

        my $type = $c->model('Object')->inflate_from_row($attr->object_id);

        unless($type->type eq 'type') {
            throw('api/case/get/object_type', sprintf(
                'Expected a Type object to be linked via ZaaktypeKenmerken, found a %s',
                $type->type
            ));
        }

        my $key = sprintf('object.%s', $type->prefix);

        my $mutations_rs = $c->stash->{ case }->object_mutation_lock_object_uuids->search(
            { object_type => $type->prefix },
            { order_by => 'date_created' }
        );

        my @mutations;

        for my $mutation ($mutations_rs->all) {
            push @mutations, $mutation;

            my $object;

            # Build an in-memory object from the mutation values we receive, so we can TO_STRING
            my %kvp;
            if ($mutation->type eq 'create') {
                my $values = $mutation->values;

                for my $attr (keys %{ $values }) {
                    my ($namespace, $attr_name) = split m[\.], $attr;

                    next unless $namespace eq 'attribute';
                    next unless defined $values->{ $attr };

                    $kvp{ $attr_name } = $values->{ $attr };
                }

                $object = eval {
                    $c->model('Object')->find_type_meta($mutation->object_type)->new_object(\%kvp);
                };
            } else {
                $object = eval { $c->model('Object')->inflate_from_row($mutation->object_uuid) };
            }

            if (defined $object) {
                $mutation->complete(1);
                $mutation->label($object->TO_STRING);
            } else {
                my $type = $c->model('Object')->search('type', { prefix => $mutation->object_type })->next;
                my $name;

                if ($type->has_title_template) {
                    my $ztt = Zaaksysteem::ZTT->new(cache => $ztt_cache)->add_context(\%kvp);

                    $name = $ztt->process_template($type->title_template)->string;
                } else {
                    $name = $type->name;
                }

                $mutation->complete(0);
                $mutation->label($name);
            }
        }

        my $events = $c->stash->{ zaak }->loggings->search(
            { event_type => { 'ILIKE' => 'case/object/%' } },
            { order_by => { 'desc' => 'created_on' } }
        );

        for my $event ($c->stash->{ zaak }->loggings->search({ event_type => { 'ILIKE' => 'case/object/%' }})) {
            next unless $event->event_data;

            my $event_data = JSON->new->decode($event->event_data);

            next unless $event_data->{ object_type } eq $type->prefix;

            my ($mutation_type) = reverse split m[/], $event->event_type;
            my ($subject_id) = $event->created_by =~ m[betrokkene\-\w+\-(\d+)];

            my $mutation = $mutations_rs->new_result({
                object_uuid => $event->get_column('object_uuid'),
                lock_object_uuid => undef,
                object_type => $type->prefix,
                type => $mutation_type,
                executed => 1,
                values => {
                    map { $_->{ field } => $_->{ new_value } } @{ $event_data->{ changes } }
                }
            });

            # Set volatile fields
            $mutation->read_only(1);
            $mutation->complete(1);
            $mutation->label($event_data->{ object_label });

            push @mutations, $mutation;
        }

        $retval{ $key } = \@mutations;
    }

    return %retval;
}

=head2 _retrieve_checklists

=cut

sub _retrieve_checklist {
    my ($self, $c) = @_;

    my $checklists = $c->stash->{ zaak }->checklists->search(undef, {
        order_by => 'case_milestone'
    });

    # Index all lists by milestone
    my %checklists = map {
        $_->case_milestone => [
            $_->checklist_items(undef, { order_by => ['sequence', 'id'] })->all
        ]
    } $checklists->all;

    return {
        by_milestone => \%checklists
    };
}

=head2 _retrieve_checklists

=cut

sub _retrieve_pending_changes {
    my ($self, $c) = @_;

    my @list_changes = $c->model('DB::ScheduledJobs')->search_update_field_tasks(
        {
            case_id     => $c->stash->{zaak}->id
        }
    )->only_most_recent_field_update;

    ### Retrieve pending names
    ### We would like to use 1 query to get all the names belonging to all the update tasks. Below
    ### a function which retrieves the names per id efficiently.
    my %name_by_id;
    for my $change (@list_changes) {
        next unless $change->parameters->{created_by};

        $name_by_id{$change->parameters->{created_by}} = undef;
    }

    my $id_and_name = $self->_collect_names_by_identifiers($c, [keys %name_by_id]);

    # Index all lists by milestone

    my %changes;
    for my $change (@list_changes) {
        $changes{ $change->parameters->{bibliotheek_kenmerken_id} } = {
            (map { $_ => $change->parameters->{$_} } qw/value reason case_id bibliotheek_kenmerken_id created_by/),
            created_by_name => ($id_and_name->{ $change->parameters->{created_by} } || undef),
        }

    }

    return \%changes;
}

=head2 _collect_names_by_identifiers

    $id_and_name = $self->_collect_names_by_identifiers($c, [qw/betrokkene-natuurlijk_persoon-1 betrokkene-medewerker-77/]);

=cut

sub _collect_names_by_identifiers {
    my ($self, $c, $list_of_ids) = @_;
    my %id_and_name;

    my $map = {
        natuurlijk_persoon  => 'NatuurlijkPersoon',
        bedrijf             => 'Bedrijf',
        medewerker          => 'Subject',
    };

    my %ids_per_type;
    for my $id (@$list_of_ids) {
        my ($type, $bid)        = $id =~ /^betrokkene-(.*?)-(\d+)$/;
        $ids_per_type{$type}    //= [];

        push(@{ $ids_per_type{$type} }, $bid);
    }

    for my $type (keys %ids_per_type) {
        my $table = $map->{$type};
        next unless $table;

        my $prefix = "betrokkene-$type-";

        my $rows  = $c->model('DB::' . $table)->search({
            id => { -in => $ids_per_type{$type} }
        });

        while (my $row = $rows->next) {
            $id_and_name{$prefix . $row->id} = $row->display_name;
        }
    }

    return \%id_and_name;
}

=head2 _retrieve_actions

=cut

sub _retrieve_actions {
    my ($self, $c) = @_;

    my $milestones = $c->stash->{ zaak }->zaaktype_node_id->zaaktype_statussen->search(undef, {
        order_by => 'status'
    });

    # Collect all case actions, create ad-hoc if missing.
    my $action_rs = $c->stash->{ zaak }->case_actions_cine;

    # Index all actions by milestone
    my %actions = map {
        $_->status => [ $action_rs->milestone($_->status)->sorted ]
    } $milestones->all;

    return {
        by_milestone => \%actions
    };
}

=head2 _log_update_field

=cut

define_profile _log_update_field => (
    required => [qw[kenmerk value_type subject_name]],
    optional => [qw[new_values reason event_type]],
    defaults => { event_type => 'case/update/field' },
);

sub _log_update_field {
    my ($self, $c, $opts) = @_;

    my $valid_opts = assert_profile($opts)->valid;

    my $new_values = $valid_opts->{value_type} =~ m|^bag| ?
        $c->model('Gegevens::Bag')->humanize($valid_opts->{new_values}) :
        $valid_opts->{ new_values };

    if (ref $new_values eq 'ARRAY') {
        ($new_values) = @{ $new_values } if scalar @{ $new_values } == 1;
    }

    my $log = $c->model('DB::Logging')->trigger($valid_opts->{event_type}, {
        component => 'zaak',
        zaak_id   => $c->stash->{zaak}->id,
        data => {
            subject_name => $valid_opts->{ subject_name },
            kenmerk      => $valid_opts->{ kenmerk },
            toelichting  => $valid_opts->{reason} // '',
            new_values   => $new_values
        }
    });

    $c->stash->{zaak}->create_message_for_behandelaar(
        message    => $valid_opts->{reason} // "Geen toelichting gegeven",
        event_type => $valid_opts->{event_type},
        log        => $log,
    );
    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
