package Zaaksysteem::Controller::API::v1::EventLog;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::EventLog - APIv1 controller for event logs

=head1 DESCRIPTION

Show the event log for the whole of Zaaksysteem, or just for an object or view.

=cut

use Archive::Tar::Stream;
use BTTW::Tools;
use Zaaksysteem::API::v1::ResultSet;
use Zaaksysteem::Tie::CallingHandle;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('intern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/eventlog> routing namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('eventlog') : CaptureArgs(0) { }

=head2 list

List all the events found in Zaaksysteem. This is basicaly the "Logboek"
in ZS.  When used with ES Query params you can look for specific entries:

=head3 URL Path

C</api/v1/eventlog>

=head3 Parameters

=over 4

=item match:timeline=UUID

Get the timeline related to object, eg the timeline of a user. Which
entries were made on its behalf and which changes in which cases were
made.

=item match:object=UUID

Get the timeline of the object. Only show items related to the actual
object itself.

=back

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    my $rs = $self->_get_objects($c);

    my $set = Zaaksysteem::API::v1::ResultSet->new(iterator => $rs);

    $c->stash->{set} = $set;

    $self->list_set($c);
}

=head2 download

Similar to the list call, but instead of returning JSON objects you get
to download all the events. When used with ES Query params you can look
for specific entries:

=head3 URL Path

C</api/v1/eventlog/download>

=head3 Parameters

=over 4

=item match:timeline=UUID

Get the timeline related to object, eg the timeline of a user. Which
entries were made on its behalf and which changes in which cases were
made.

=item match:object=UUID

Get the timeline of the object. Only show items related to the actual
object itself.

=back

=cut

sub download : Chained('base') : PathPart('download') : Args(0) : RO {
    my ($self, $c) = @_;

    my $rs = $self->_get_objects($c);

    my $object_model = $c->model('Object');

    my $filename = sprintf(
        "zs-%s.tar",
        DateTime->now()->iso8601,
    );

    $c->res->content_type('application/x-tar');
    $c->res->header('Content-Disposition', qq[attachment; filename="$filename"]);

    my $handle = Zaaksysteem::Tie::CallingHandle->create(
        write_cb => sub { $c->res->write($_[0]); },
    );

    my $tar = Archive::Tar::Stream->new(outfh => $handle);

    $object_model->export_rs_as_object(
        resultset  => $rs,
        tar_handle => $tar,
        metadata  => {
            zs_version  => $c->config->{ZS_VERSION},
            environment => $c->config->{gemeente_id},
            user        => $c->user->username,
        }
    );

    $tar->FinishTar();

    $c->stash->{file_download}  = 1;
}

sub _get_objects {
    my ($self, $c) = @_;

    my $rs = $c->model("DB::Logging")->search_rs(
        {
            deleted_on => undef,
            restricted => 0,

            # Do nothing, these types are legacy and need be migrated.
            # This falls out of scope for the contactdossier epic
            event_type => { '!=' => undef },
        },
        { order_by => { -desc => 'id' } },
    );

    my $params = $c->req->params();
    delete $params->{page};

    my $query = {};

    if (keys %$params) {
        my $es = $c->parse_es_query_params->{query};

        if (defined $es->{match}{event_type}) {
            $query->{event_type} = [-in => $es->{match}{event_type}];
        }

        if (defined $es->{match}{keyword}) {
            my $like = "%$es->{match}{keyword}%";

            $query->{'-and'} = { -or => [
                event_data => { ilike => $like },
                onderwerp => { ilike => $like },
            ]},
        }


        if (defined $es->{match}{timeline}) {
            my $br     = $c->model('BR::Subject');
            my $object = $br->find($es->{match}{timeline});
            if ($object) {
                my $bid = $object->old_subject_identifier;

                my (undef, $type, $id) = split(/\-/, $bid);
                my $rs_related_cases
                    = $c->model('DB::ZaakBetrokkenen')->search_rs(
                    {
                        betrokkene_type      => $type,
                        gegevens_magazijn_id => $id,
                        deleted              => undef
                    },
                    );

                $query->{'-or'} = [
                    { created_by  => $object->old_subject_identifier },
                    { created_for => $object->old_subject_identifier },
                    { modified_by => $object->old_subject_identifier },
                    {
                        zaak_id => {
                            -in => $rs_related_cases->get_column('zaak_id')
                                ->as_query
                        }
                    },
                ];
            }
            else {
                my $object = $c->model('Object')->retrieve(uuid => $es->{match}{timeline});
                if ($object->type eq 'case') {
                    $query = { zaak_id => $object->case_number },
                }
                else {
                    $query = { object_uuid => $es->{match}{object} };
                }

            }
        }
        elsif (defined $es->{match}{object}) {
            my $br     = $c->model('BR::Subject');
            my $object = $br->find($es->{match}{object});
            if ($object) {
                throw(
                    'api/v1/event_log/subject_object_not_yet_supported',
                    'Unable to construct query for subject object instances'
                );
            }
            else {
                my $object = $c->model('Object')->retrieve(uuid => $es->{match}{object});
                if ($object->type eq 'case') {
                    $query = { zaak_id => $object->case_number },
                }
                else {
                    $query = { object_uuid => $es->{match}{object} };
                }

            }
        }
    }

    return $rs->search_rs($query);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
