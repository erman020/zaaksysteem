package Zaaksysteem::Controller::API::v1::Subject::Role;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Subject::Role - API v1 controller for retrieving subject roles

=head1 DESCRIPTION

This controller returns the set of subject role instances for the current Zaaksysteem instance.

=cut

use BTTW::Tools;
use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::Constants qw(BASE_RELATION_ROLES);
use Zaaksysteem::Object::Types::Subject::Role;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('extern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/subject/role> routing namespace.

=cut

sub base : Chained('/api/v1/subject/base') : PathPart('role') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{set} = $self->get_subject_roles($c);
}

=head2 list

Return a list of all subject roles.

=head3 URL Path

C</api/v1/subject/role>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;
    $self->list_set($c);
}

=head1 METHODS

=head2 get_subject_roles

Make L<Zaaksysteem::Object::Types::Subject::Role> objects for every available
subject role, and put them on the stash.

=cut

sub get_subject_roles {
    my ($self, $c) = @_;

    my @roles;

    for my $role (@{ BASE_RELATION_ROLES() }) {
        push @roles, Zaaksysteem::Object::Types::Subject::Role->new(
            label   => $role,
            is_builtin => 1,
        );
    }

    for my $role (@{ $c->model('DB::Config')->get_value('custom_relation_roles') || [] }) {
        push @roles, Zaaksysteem::Object::Types::Subject::Role->new(
            label   => $role,
            is_builtin => 0,
        );
    }

    return Zaaksysteem::API::v1::ArraySet->new(
        content => \@roles,
        rows_per_page => 250,
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
