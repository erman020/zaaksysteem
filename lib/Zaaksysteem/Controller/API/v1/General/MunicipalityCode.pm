package Zaaksysteem::Controller::API::v1::General::MunicipalityCode;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::General::MunicipalityCode - API v1
controller for municipality codes

=head1 DESCRIPTION

This controller returns the municipality codes in use by Dutch government.
These codes are maintained by the CBS.

=cut

use BTTW::Tools;
use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::Constants qw(RGBZ_GEMEENTECODES);
use Zaaksysteem::Object::Types::MunicipalityCode;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('extern', 'intern', 'allow_pip');
}

has '+namespace' => (
    default => 'municipality_codes'
);

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/general/municipality_code> routing namespace.

=cut

sub base : Chained('/api/v1/general/base') : PathPart('municipality_code') : CaptureArgs(0) {
    my ($self, $c)      = @_;
}

=head2 instance_base

Reserves the C</api/v1/general/municipality_code/[MUNICIPALITY_CODE_UUID]> routing
namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    try {
        $c->stash->{$self->namespace} =
            Zaaksysteem::Object::Types::MunicipalityCode->new_from_uuid($uuid);
    }
    catch {
        throw('api/v1/general/municipality_codes/not_found', sprintf(
            'Unable to find municipality code "%s"',
            $uuid,
        ));
    };
}

=head2 list

=head3 URL Path

C</api/v1/general/municipality_code>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $self->get_all_municipality_codes($c);
    $self->list_set($c);

}

=head2 get

=head3 URL Path

C</api/v1/general/municipality_code/[MUNICIPALITY_CODE_UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;
    $self->get_object($c);
}

=head1 METHODS

=head2 get_all_municipality_codes

Get all the legal entitities from Zaaksysteem

=cut

sub get_all_municipality_codes {
    my ($self, $c)      = @_;

    my $entities = RGBZ_GEMEENTECODES();

    my @objects = sort { $a->label cmp $b->label } map {
        Zaaksysteem::Object::Types::MunicipalityCode->new_from_dutch_code($_);
    } keys %$entities;

    my $params = $c->req->params;
    delete $params->{$_} for qw(rows_per_page page);

    if (keys %$params) {
        my $es = $c->parse_es_query_params->{query};

        if (defined $es->{match}{code}) {
            my $code = $es->{match}{code};
            @objects = grep { $_->dutch_code =~ /$code/ } @objects;
        }

        if (defined $es->{match}{name}) {
            my $name = $es->{match}{name};
            my $qr = qr/\Q$name\E/i;

            @objects =  grep {
                $_->label =~ /$qr/
                    or
                $_->has_alternative_name
                    ? $_->alternative_name =~ /$qr/
                    : (),
            } @objects;
        }
    }

    my $set = Zaaksysteem::API::v1::ArraySet->new(
        content             => \@objects,
        allow_rows_per_page => 500,
    );

    $c->stash->{set} = $set;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, 2018 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
