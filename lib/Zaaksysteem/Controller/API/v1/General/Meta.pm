package Zaaksysteem::Controller::API::v1::General::Meta;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::General::Config - API v1 controller for configuration entries

=head1 DESCRIPTION

This controller returns a set of configuration items for the current Zaaksysteem instance.

=cut

use BTTW::Tools;
use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::Object::Types::Meta;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('intern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/general/meta> routing namespace.

=cut

sub base : Chained('/api/v1/general/base') : PathPart('meta') : CaptureArgs(0) {
    my ($self, $c)      = @_;
    $self->get_config_items($c);
}

=head2 list

Return a list of all configuration items

=head3 URL Path

C</api/v1/general/meta>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $self->list_set($c);
}

=head1 METHODS

=head2 get_config_items

Aggregates several data about the Zaaksysteem instance. Returns an
L<Zaaksysteem::API::v1::ArraySet> of L<Zaaksysteem::Object::Types::Meta>
instances.

=cut

sub get_config_items {
    my ($self, $c) = @_;

    my $version = Zaaksysteem::Object::Types::Meta->new(
        label => 'Version',
        value => $c->config->{ZS_VERSION},
    );
    my $nen2081_version = Zaaksysteem::Object::Types::Meta->new(
        label => 'NEN Version',
        value => $c->config->{NEN2081_VERSION},
    );
    my $app_server = Zaaksysteem::Object::Types::Meta->new(
        label => 'Appserver',
        value => $c->req->hostname,
    );
    my $customer_info = Zaaksysteem::Object::Types::Meta->new(
        label => 'Customer information',
        value => $c->get_customer_info,
    );
    my $database = Zaaksysteem::Object::Types::Meta->new(
        label => 'Database',
        value => $c->model('DB')->schema->storage->connect_info->[0]{dsn},
    );

    $c->stash->{set} = Zaaksysteem::API::v1::ArraySet->new(
        content => [ $version, $nen2081_version, $app_server, $customer_info, $database ],
    );
}

has '+namespace' => (
    default => 'meta'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
