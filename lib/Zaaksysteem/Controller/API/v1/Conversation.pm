package Zaaksysteem::Controller::API::v1::Conversation;

use Moose;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Conversation - APIv1 controller for conversation objects (contactmomenten)

=head1 DESCRIPTION

This is the controller API class for C<api/v1/conversation>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Conversation>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Conversation>

=cut

use BTTW::Tools;
use Moose::Util::TypeConstraints;
use Zaaksysteem::Types qw/Betrokkene ChannelOfContact/;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('intern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/conversation> routing namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('conversation') : CaptureArgs(0) {}

=head2 create

=head3 URL Path

C</api/v1/conversation/create>

=cut

define_profile create => (
    required    => {
        type        => subtype('Str' => where { $_ =~ m/^(?:email|note)$/ }),
        subject_id  => Betrokkene,
        medium      => ChannelOfContact,
        message     => 'Str',
    },
    optional    => {
        case_id     => 'Num',
    }
);

sub create : Chained('base') : PathPart('create') : Args(0) : RO {
    my ($self, $c)  = @_;

    my $params      = assert_profile($c->req->params)->valid;

    $c->stash->{result} = $c->model('DB::Contactmoment')->contactmoment_create({
        %$params,
        created_by => $c->user->betrokkene_identifier,
    });

    $c->model('DB::Logging')->trigger('subject/contactmoment/create', {
        component => $params->{ case_id } ? 'zaak' : 'betrokkene',
        zaak_id => $params->{ case_id } || undef,
        created_for => $params->{ subject_id },
        data => {
            case_id => $params->{ case_id } || undef,
            content => $params->{ message },
            subject_id => $params->{ subject_id },
            contact_channel => $params->{ medium }
        }
    });
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
