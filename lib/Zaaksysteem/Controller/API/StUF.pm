package Zaaksysteem::Controller::API::StUF;

use Moose;

use Zaaksysteem::Backend::Sysin::Modules;
use BTTW::Tools;
use MIME::Base64 qw(encode_base64 decode_base64);

use XML::LibXML;

BEGIN { extends 'Zaaksysteem::General::SOAPController' }

use constant COMPONENT_KENNISGEVING => 'kennisgeving';

sub _get_config_interface {
    my ($self, $c) = @_;

    my %args = (
        module       => 'stufconfig',
    );

    if (my $config_id = $c->req->arguments->[0]) {
        if ($config_id !~ /^[0-9]+$/) {
           $c->stash->{soap}->fault(
               {
                   code => '500',
                   reason => 'Verkeerde endpoint-URL aangeroepen',
                   detail => sprintf(
                       'StUF-koppeling-identifier mag alleen uit cijfers bestaan (niet "%s")',
                       $config_id,
                   ),
               }
           );
           return;
        }
        $args{id} = $config_id;
    }

    my $rs = $c->model('DB::Interface')->search_active(\%args);
    my $interface = $rs->next;

    if (!$interface) {
        my %fault = (
            code   => '500',
            reason => 'Geen StUF-configuratie gevonden',
            detail => $args{id}
                ? "StUF-koppeling met ID $args{id} is niet geconfigureerd"
                : 'StUF-koppeling is niet geconfigureerd',
        );
        $c->stash->{soap}->fault(\%fault);
        return;
    }
    elsif ($rs->next) {
        $c->stash->{soap}->fault(
            {
                code => '500',
                reason => 'Verkeerde endpoint-URL aangeroepen',
                detail => 'Meerdere StUF-configuraties gevonden. Gebruik de configuratie-specifieke URL.',
            }
        );
        return;
    }

    return $interface;
}

sub process_soap {
    my ($self, $c) = @_;

   $c->stash->{stuf_config_interface} = $self->_get_config_interface($c);

    my $xml    = $c->stash->{soap}->envelope();
    my $answer = $c->forward('handle_kennisgeving', [$xml]);
    $c->stash->{soap}->literal_return($answer) if $answer;
}

### Here for backwards compatible reasons
sub bg0204 : Local SOAP('DocumentLiteral') {
    my ($self, $c) = @_;
    $self->process_soap($c);
}

sub stuf0204 : Local SOAP('DocumentLiteral') {
    my ($self, $c) = @_;
    $self->process_soap($c);
}

sub stuf0301 : Chained('/') : PathPart('api/stuf/stuf0301') : Args(0) : SOAP('DocumentLiteral') {
    my ($self, $c) = @_;
    $self->process_soap($c);
}

sub stuf0301_specific : Chained('/') : PathPart('api/stuf/stuf0301') : Args(1) : SOAP('DocumentLiteral') {
   my ($self, $c) = @_;
    $self->process_soap($c);
}


sub endpoint : Private {
    my ($self, $c) = @_;
    $self->process_soap($c);
}

sub handle_kennisgeving : Private {
    my ($self, $c, $xml)    = @_;

    ### Detect entiteittype
    my ($entiteittype)      = $xml =~ /entiteittype.*?>(.*?)<\/(\w+:)?entiteit/si;

    unless ($entiteittype) {
        $c->log->error('Entiteittype not found');
        $c->stash->{soap}->fault(
            {
                code     => '500',
                reason   => 'Entiteittype not found',
                detail   => 'Entiteittype could not be recognized'
            }
        );

        return;
    }

    my $module_name = 'stuf' . lc($entiteittype);

    my $rs = $c->model('DB::Interface')->search_active({ module => $module_name });
    my $interface = $rs->next;
    if (!$interface) {
        $c->log->info('Interface not active: ' . $module_name);
        $c->stash->{soap}->fault(
            {
                code     => '500',
                reason   => 'StUF module not active',
                detail   => 'StUF module for ' . $entiteittype . ' not active, be sure to configure it',
            }
        );
        return;
    }
    elsif ($rs->next) {
        $c->log->info(sprintf 'Too many interfaces for "%s" configured, only one allowed', $module_name);
        $c->stash->{soap}->fault(
            {
                code     => '500',
                reason   => 'StUF module not found',
                detail   => 'StUF module for ' . $entiteittype . ' not found, be sure to configure it',
            }
        );
        return;
    }


    my $dn                 = $c->engine->env->{SSL_CLIENT_S_DN} || $c->req->header('x-client-ssl-s-dn') // '';
    my $client_cert        = $c->engine->env->{SSL_CLIENT_CERT} || $c->req->header('x-client-ssl-cert') // '';

    my $certificate_match = try {
        my $module = $interface->module_object;

        $module->assert_client_certificate(
            dn                 => $dn,
            client_cert        => (decode_base64($client_cert) || ''),

            get_certificate_cb => sub {
                my $config_interface = $c->stash->{stuf_config_interface};
                my $certificate = $c->model('DB::Filestore')->find($config_interface->jpath('$.mk_cert[0].id'));

                return $certificate;
            },
        );
    } catch {
        $c->log->error("Error validating client certificate: $_");

        $interface->process_generic_error("$_", {
            short_error => 'Client-certificaat validatie fout'
        });

        return;
    };

    if (!$certificate_match) {
        $c->stash->{soap}->fault(
            {
                code     => '403',
                reason   => 'Forbidden',
                detail   => 'AUTHORIZATION FAILURE: Invalid SSL Certificate'
            }
        );

        return;
    }

    my ($response, $transaction);
    eval {
        $transaction = $interface->process(
            {
                input_data                  => $xml,
                config_interface_id         => $c->stash->{stuf_config_interface}->id,
                external_transaction_id     => 'unknown',       # Will be replaced later
            }
        );

        if ($transaction->transaction_records->count == 1) {
            my $record      = $transaction->transaction_records->first;

            $response       = $record->output;
        }
    };

    if ($@ || !$response) {
        my $message = $@;

        if (!$message) {
            # Make sure isn't undefined
            $message = '';

            if ($transaction) {
                $message = join(", ", sprintf('transaction id: %s', $transaction->id));
            }
        }

        $c->stash->{soap}->fault(
            {
                code     => '500',
                reason   => 'Transaction could not be processed',
                detail   => "Transaction could not be processed: $message",
            }
        );

        return;
    }


    if (
        $response &&
        (
            $response =~ /^Error: / ||
            $response =~ /xml version/
        )
    ) {
        $response =~ s/^Error: //;
        $response =~ s/^.*?<\?xml versio/<?xml versio/;
        $response =~ s/> at .* line \d+$/>/;

        return XML::LibXML->load_xml(string => $response)->documentElement();
    }

    $c->stash->{soap}->fault(
        {
            code     => '500',
            reason   => 'No response given',
            detail   => 'Unknown error occured'
        }
    );

    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 COMPONENT_KENNISGEVING

TODO: Fix the POD

=head2 bg0204

TODO: Fix the POD

=cut

=head2 endpoint

TODO: Fix the POD

=cut

=head2 handle_kennisgeving

TODO: Fix the POD

=cut

=head2 stuf0204

TODO: Fix the POD

=cut

=head2 stuf0301

TODO: Fix the POD

=cut

=head2 verify_authorization

TODO: Fix the POD

=cut

