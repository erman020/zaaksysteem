package Zaaksysteem::Controller::Plugins::Twofactor;
use Moose;

BEGIN { extends 'Zaaksysteem::Controller'; }

use Zaaksysteem::Constants qw(
    PARAMS_PROFILE_DEFAULT_MSGS
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_TWOFACTOR
);
use BTTW::Tools;
use Zaaksysteem::Types qw(MobileNumber BSN NonEmptyStr EmailAddress);

my $SECOND_FACTOR_RE = qr/^[A-Za-z0-9]+$/;
my $USERNAME_RE      = qr/^[^\s]+$/;

my %error_mapping = (
    'auth/alternative/invalid/bsn' => 'U heeft een ongeldig BSN ingevoerd',
    'auth/alternative/username/exists' => 'Deze gebruikersnaam bestaat al',
    'auth/alternative/user/not_found'  => 'Deze gebruikersnaam bestaat niet',
    'auth/alternative/password/invalid' => 'Uw wachtwoord is niet gelijk',
    'auth/alternative/password/username' => 'Uw wachtwoord bevat uw gebruikersnaam',
    'auth/alternative/password_reset/inactive' => 'Uw account is inactief, er kan geen wachtwoord reset uitgevoerd worden',
    'auth/alternative/password_reset/timeout' => 'U kan uw wachtwoord momenteel niet resetten, probeer het over enkele minuten opnieuw',
    'auth/alternative/account/exists' => 'Er bestaat al een account voor deze BSN',
);

=head1 NAME

Zaaksysteem::Controller::Plugins::Twofactor - Two-factor authentication controller

=head1 ACTIONS

=head2 base

Base for all two-factor authentication controllers. Retrieves the active
"auth_twofactor" interface and puts it on the stash.

Reserves the C</auth/twofactor> URI namespace.

=cut

sub base : Chained('/') : PathPart('auth/twofactor') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{twofactor_interface} = $self->_get_interface($c)
        or throw("twofactor/unconfigured", "Two-factor authentication is not configured");

    return;
}

=head2 login

Show the "Login using two-factor auth" (or register a new account) screen

=head3 URL Path

C</auth/twofactor>

=cut

sub login : Chained('base') : PathPart('') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;

    if ($c->user_exists) {
        $self->log->debug("Found existing user (behandelaar) session, removing it.");
        $c->delete_session;
    }

    # In case of an XHR, and we get here... tell the frontend that we are not logged in
    if ($c->req->is_xhr) {
        $c->res->status('401');
    }

    $c->session->{_twofactor}{success_endpoint} = $c->req->params->{success_endpoint}
        if(exists $c->req->params->{success_endpoint});

    $c->stash->{twofactor_intro_text} = $c->stash->{twofactor_interface}->jpath('$.login_message');

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }

    # Show username & password page
    $c->stash->{template} = 'plugins/twofactor/login.tt';
}

=head2 logout

Logs out the user, and redirects to the login page.

=head3 URL Path

C</auth/twofactor/logout>

=cut

sub logout : Chained('base') : PathPart('logout') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    my $success_endpoint = $c->session->{_twofactor}{success_endpoint};

    # The success endpoint is necessary to make "log in again" work.
    delete $c->session->{_twofactor};
    $c->session->{_twofactor}{success_endpoint} = $success_endpoint;

    $c->flash->{logged_out} = 1;

    # Success endpoint should still be in session at this point.
    $c->res->redirect($c->uri_for('/auth/twofactor'));
}


define_profile process_activation => (
    required => {
        activation_code => NonEmptyStr,
        username        => NonEmptyStr,
        password        => NonEmptyStr,
        password_check  => NonEmptyStr,
        email           => EmailAddress,
        phone           => MobileNumber,
        subject_type    => 'Str',
    },
    optional => {
        bsn              => BSN,
        kvknummer        => 'Num',
        vestigingsnummer => 'Num'
    }
);

sub process_activation : Chained('base') : PathPart('process_activation') : Args(0) : Access(*) {
    my ($self, $c, $activation_code) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    my $args = assert_profile($c->req->params)->valid;

    my $model = $c->model('Auth::Alternative');

    try {
        my $subject = $model->get_activation_subject($args->{activation_code});

        $model->assert_password(%$args);

        $model->update_subject($subject, $args);

        $model->update_password(
            subject     => $subject,
            password    => $args->{password},
        );

        $self->_process_login($c,
            {
                subject  => $subject,
            }
        );

        $model->delete_activation_link($args->{activation_code});

        my $ue = $subject->user_entities->search(
            { source_interface_id => $model->interface->id })->first;

        $model->activate_account($ue);

        $c->session->{_twofactor}{is_activation}    = 1;
        $c->session->{_twofactor}{register_success} = 1;
        $c->session->{_twofactor}{success_endpoint} = $c->uri_for('/pip/login');

        $c->res->redirect($c->uri_for('/auth/twofactor/registration_factor'));
    }
    catch {
        $c->log->error($_);

        if ($_->can('type')) {
            if (my $human_text = $error_mapping{ $_->type }) {
                $c->flash->{twofactor_error} = $human_text;
            }
        }


        $c->flash->{twofactor_error} //= sprintf(
            "Er is een onbekende fout opgetreden. Probeer het opnieuw of meldt het probleem bij uw leverancier: %s",
            $c->get_zs_session_id
        );

        $c->res->redirect(
            $c->uri_for('/auth/twofactor/activate/' . $args->{activation_code})
        );
    }
}

=head2 process_login

Perform the first step of the login using the specified username and password.

If the login is correct, an SMS will be sent and the user will be redirected to
the "second factor" page.

If the login is incorrect, the user will be redirected back to the login page,
and see a message.

=head3 URL Path

C</auth/twofactor/process_login>

=cut

Params::Profile->register_profile(
    method => 'process_login',
    profile => {
        required => [qw(username password)],
        constraint_methods => {
            username => $USERNAME_RE,
        },
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    },
);

# Log in to existing account
sub process_login : Chained('base') : PathPart('process_login') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    my $args = assert_profile($c->req->params)->valid;

    my $ue;
    try {

        my $model = $c->model('Auth::Alternative');
        $ue = $model->get_user_entity($args->{username});

        if (!$ue->subject_id->is_active) {
            $c->log->warn(sprintf("User %s is not active", $args->{username}));
            $c->flash->{twofactor_error} = "Uw account is niet actief, neem contact op met uw leverancier";
            $c->res->redirect($c->uri_for('/auth/twofactor'));
            return;
        }

        my $subject = $model->check_password($ue, $args->{password});
        $self->_process_login($c,
            {
                subject  => $ue->subject_id,
            }
        );
       $c->res->redirect($c->uri_for('/auth/twofactor/second_factor'));
    }
    catch {
        if (eval { $_->isa('Throwable::Error') } && $_->can('type') && $_->type eq 'alt/auth/invalid/subject_type') {
            my $human_subject = $ue->subject_id->subject_type eq 'person' ? 'natuurlijk persoon' : 'bedrijf';
            my $human_zaaktype = $c->session->{_zaak_create}{ztc_aanvrager_type} eq 'natuurlijk_persoon' ? 'natuurlijk persoon' : 'bedrijf';
            $c->flash->{twofactor_error} = sprintf("U probeert een zaak voor een %s als een %s", $human_zaaktype, $human_subject);
            $c->res->redirect($c->uri_for('/auth/twofactor'));
            return;
        }
        else {
            $c->log->warn($_);
            $c->flash->{twofactor_error} = "Onjuiste gebruikersnaam of wachtwoord";
            $c->res->redirect($c->uri_for('/auth/twofactor'));
        }
    }
}

=head2 second_factor

Show the "Enter the code you received by SMS" page.

=head3 URL Path

C</auth/twofactor/second_factor>

=cut

sub second_factor : Chained('base') : PathPart('second_factor') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;
    if (!$c->session->{_twofactor}{second_factor}) {
        throw("twofactor/sequence_error", "Sequence error: can't do second factor without first logging in");
    }

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }

    $c->stash->{template} = 'plugins/twofactor/second_factor.tt';
}

=head2 verify_second_factor

Verify that the code entered by the user is correct.

Then forwards to the "success_endpoint" to continue the action that requires logging in.

=head3 URL Path

C</auth/twofactor/verify_second_factor>

=cut

Params::Profile->register_profile(
    method => 'verify_second_factor',
    profile => {
        required => [qw(factor)],
        constraint_methods => {
            factor => $SECOND_FACTOR_RE,
        },
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    }
);

sub verify_second_factor : Chained('base') : PathPart('verify_second_factor') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    my $args = assert_profile($c->req->params)->valid;

    if (!$self->_verify_second_factor($c, $args->{factor})) {
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/second_factor')
        );
        return;
    }

    $c->res->redirect(
        $c->session->{_twofactor}{success_endpoint}
    );
    return 1;

}

=head2 register

Show the "Register a new account" page.

=head3 URL Path

C</auth/twofactor/register>

=cut

# Register a new account
sub register : Chained('base') : PathPart('register') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;

    $c->stash->{aanvrager_type} = $c->session->{_zaak_create}{ztc_aanvrager_type};

    $c->stash->{twofactor_intro_text} = $c->stash->{twofactor_interface}->jpath('$.registration_message');

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }

    # Show "Register account" page
    $c->stash->{template} = 'plugins/twofactor/register.tt';
}

sub activate : Chained('base') : PathPart('activate') : Access(*) : Method('GET') {
    my ($self, $c, $activation_code) = @_;

    my $model = $c->model('Auth::Alternative');
    try {

        my $subject = $model->get_activation_subject($activation_code);

        $c->stash->{aanvrager_type}  = $subject->subject_type;
        $c->stash->{subject}         = $subject;
        $c->stash->{activation_code} = $activation_code;

        $c->stash->{twofactor_intro_text} = $c->stash->{twofactor_interface}->jpath('$.registration_message');
    }
    catch {
        $c->log->warn($_);
        $c->stash->{no_activation_code} = 1;
        $c->flash->{twofactor_error}    = "De activatiecode is verlopen of bestaat niet. Neem contact op met uw leverancier";
    };

    # Show "Activate account" page
    $c->stash->{template} = 'plugins/twofactor/activate.tt';

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }



}

=head2 process_registration

Process the initial registration of the user account.

Compares the supplied passwords, checks if the email address is already in use,
etc.

=head3 URL Path

C</auth/twofactor/process_registration>

=cut

Params::Profile->register_profile(
    method => 'process_registration',
    profile => {
        required => [qw( username password password_check email phone) ],
        optional => [qw( bsn kvk vestigingsnummer )] ,
        constraint_methods => {
            username       => $USERNAME_RE,
            password       => qr/^.+$/,
            password_check => qr/^.+$/,
            email          => qr/^.+?\@.+\.[a-z0-9]{2,}$/,
            phone          => sub { my $val = pop; return MobileNumber->check($val) },
            bsn              => qr/^[0-9]{8,9}$/,
            kvknummer        => qr/^[0-9]{8}$/,
            vestigingsnummer => qr/^[0-9]{12}$/,
        },
        dependencies => {
            aanvrager_type => {
                'natuurlijk_persoon' => [ 'bsn' ],
                'niet_natuurlijk_persoon' => [ 'kvknummer' ],
            }
        },
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    },
);

sub process_registration : Chained('base') : PathPart('process_registration') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    my $args = assert_profile($c->req->params)->valid;

    $args->{subject_type} = $c->session->{_zaak_create}{ztc_aanvrager_type} eq 'natuurlijk_persoon'
        ? 'person'
        : 'company';

    my $model = $c->model('Auth::Alternative');

    try {

        # Check on valid BSN this way so we can show a correct error
        # message
        if ($args->{subject_type} eq 'person' && !BSN->check($args->{bsn})) {
            throw("auth/alternative/invalid/bsn", "Invalid BSN");
        }

        my $subject = $model->register_account(%$args);
        $self->_process_login($c,
            {
                subject  => $subject,
            }
        );
        $c->session->{_twofactor}{register_success} = 1;
        $c->res->redirect($c->uri_for('/auth/twofactor/registration_factor'));
    }
    catch {
        $c->log->warn($_);

        if ($_->can('type')) {
            if (my $human_text = $error_mapping{ $_->type }) {
                $c->flash->{twofactor_error} = $human_text;
            }
        }


        $c->flash->{twofactor_error} //= sprintf(
            "Er is een onbekende fout opgetreden. Probeer het opnieuw of meld het probleem bij uw leverancier: %s",
            $c->get_zs_session_id
        );

        # TODO: Stop variabelen in session
        $c->stash->{_twofactor} = $args;
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/register')
        );
    }
}

=head2 registration_factor

Shows the "Please enter the code you received" page in the new account registration flow.

=head3 URL Path

C</auth/twofactor/registration_factor>

=cut

sub registration_factor : Chained('base') : PathPart('registration_factor') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;
    if (!$c->session->{_twofactor}{second_factor}) {
        throw("twofactor/sequence_error", "Sequence error: can't do second factor without first registering");
    }

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }

    $c->stash->{is_activation} = $c->session->{_twofactor}{is_activation};

    # Show "second factor" page.
    $c->stash->{template} = 'plugins/twofactor/register_second_factor.tt';
}

=head2 verify_registration_factor

Verifies that the code entered by the user is correct, and enables the account
if that's the case.

Then forwards to the "success_endpoint" to continue the action that requires logging in.

=head3 URL Path

C</auth/twofactor/verify_registration_factor>

=cut

Params::Profile->register_profile(
    method => 'verify_registration_factor',
    profile => {
        required => [qw(factor)],
        constraint_methods => {
            factor => $SECOND_FACTOR_RE,
        },
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    },
);

sub verify_registration_factor : Chained('base') : PathPart('verify_registration_factor') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    my $args = assert_profile($c->req->params)->valid;

    my $model = $c->model('Auth::Alternative');

    my $ue = $model->get_user_entity($c->session->{_twofactor}{username});

    if (!$self->_verify_second_factor($c, $args->{factor})) {
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/registration_factor')
        );
        return;
    }

    if ($model->activate_account($ue)) {
        $c->log->debug("Er is een mail verstuurd naar de gebruiker");
    }
    else {
        $c->log->debug("Er is geen mail verstuurd naar de gebruiker");
    }

    $c->res->redirect(
        $c->session->{_twofactor}{success_endpoint}
    );
    return;

}

=head2 send_token_per_sms

Send the SMS token via a transaction

=cut

sub send_token_per_sms {
    my ($self, $c) = @_;
    $c->stash->{twofactor_interface}->process_trigger(
        "send_sms",
        {
            phonenumber => $c->session->{_twofactor}{phone},
            message     => "Uw inlogcode is " . $c->session->{_twofactor}{second_factor},
        },
    );
}


=head2 lost_password

Show the "Lost password" page, where a user who forgot their password can start
the process of setting a new one.

=head3 URL Path

C</auth/twofactor/lost_password>

=cut

sub lost_password : Chained('base') : PathPart('lost_password') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }
    $c->stash->{twofactor_password_text} = $c->stash->{twofactor_interface}->jpath('$.password_lost_message');

    # Show "second factor" page.
    $c->stash->{template} = 'plugins/twofactor/lost_password.tt';
}

=head2 process_lost_password

Process the form submission by the "Lost password" page. This sends an email to
the user (if one hasn't been sent in the past 5 minutes), which contains a link
that will allow the user to reset their password.

=head3 URL Path

C</auth/twofactor/process_lost_password>

=cut

Params::Profile->register_profile(
    method => 'process_lost_password',
    profile => {
        required => [qw(username)],
        constraint_methods => {
            username => $USERNAME_RE,
        },
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    }
);

sub process_lost_password : Chained('base') : PathPart('process_lost_password') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    my $args = assert_profile($c->req->params)->valid;

    my $model = $c->model('Auth::Alternative');

    my $ue = $model->find_user_entity($args->{username});

    if (!$ue) {
        $c->stash->{twofactor_error} = "Gebruiker bestaat niet";
        $c->forward('lost_password');
        return;
    }

    try {
        $model->assert_password_reset($ue);
        my $token = $model->create_auth_challenge();
        $c->session->{_twofactor}{second_factor} = $token;

        my $subject = $ue->subject_id;
        my $properties = $subject->properties;

        $c->session->{_twofactor}{phone} = $properties->{phone_number} // $properties->{initial_phone_number};

        $self->send_token_per_sms($c);

        $c->session->{_twofactor}{recovery_username} = $subject->username;

        $c->res->redirect($c->uri_for('/auth/twofactor/lost_password_verify_second_factor_page'));
    } catch {
        $c->log->warn($_);
        if ($_->can('type')) {
            if (my $human_text = $error_mapping{ $_->type }) {
                $c->flash->{twofactor_error} = $human_text;
            }
        }

        $c->flash->{twofactor_error} //= sprintf(
            "Er is een onbekende fout opgetreden. Probeer het opnieuw of meldt het probleem bij uw leverancier: %s",
            $c->get_zs_session_id
        );
        $c->forward('lost_password');
    }

}

=head2 lost_password_verify_second_factor_page

Landing page for two-factor verification of password reset process.

=head3 URL Path

C</auth/twofactor/lost_password_verify_second_factor_page>

=cut

# Show "SMS has been sent; please type code here" page
sub lost_password_verify_second_factor_page : Chained('base') : PathPart('lost_password_verify_second_factor_page') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }

    # Show "second factor" page.
    $c->stash->{template} = 'plugins/twofactor/lost_password_second_factor.tt';
}

=head2 lost_password_verify_second_factor

Verify if the "second factor" was correct while doing the password reset process.

=head3 URL Path

C</auth/twofactor/lost_password_verify_second_factor>

=cut

Params::Profile->register_profile(
    method => 'lost_password_verify_second_factor',
    profile => {
        required => [qw(factor)],
        constraint_methods => {
            factor => $SECOND_FACTOR_RE,
        },
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    }
);

# Verify the SMS, redirect to "change password" page
sub lost_password_verify_second_factor : Chained('base') : PathPart('lost_password_verify_second_factor') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    my $args = assert_profile($c->req->params)->valid;

    if (!$self->_verify_second_factor($c, $args->{factor})) {
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/lost_password_verify_second_factor_page')
        );
        return;
    }
    else {
        delete $c->session->{_twofactor}{authenticated};
        $c->session->{_twofactor}{reset_password} = 1;
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/lost_password_new_password')
        );

    }

}

=head2 lost_password_new_password

Show the "Pick a new password" page after verifying the second factor (SMS code)

=head3 URL Path

C</auth/twofactor/lost_password_new_password>

=cut

# Show "type a new password" page
sub lost_password_new_password : Chained('base') : PathPart('lost_password_new_password') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;
    if (!$c->session->{_twofactor}{reset_password}) {
        throw("twofactor/sequence_error", "Sequence error: can't reset the password without first verifying");
    }

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }

    # Show "Type a new password twice" page.
    $c->stash->{template} = 'plugins/twofactor/lost_password_new_password.tt';
}

=head2 lost_password_update_password

Re-set the password of the user to the newly entered one.

=cut

Params::Profile->register_profile(
    method => 'lost_password_update_password',
    profile => {
        required => [qw(password password_check)],
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    },
);

sub lost_password_update_password : Chained('base') : PathPart('lost_password_update_password') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    if (!$c->session->{_twofactor}{reset_password}) {
        throw("twofactor/sequence_error", "Sequence error: can't reset the password without first verifying");
    }

    my $args = assert_profile($c->req->params)->valid;

    my $model = $c->model('Auth::Alternative');

    my $ue = $model->get_user_entity($c->session->{_twofactor}{recovery_username});

    try {
        $model->assert_password(
            username       => $ue->subject_id->username,
            password       => $args->{password},
            password_check => $args->{password_check},
        );

        $model->update_password(
            subject     => $ue->subject_id,
            user_entity => $ue,
            password    => $args->{password},
        );

        $model->trigger_logging(
            type => 'password',
            data => { },
            subject => $ue->subject_id,
        );

        $c->session->{_twofactor}{password_reset_done} = 1;
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/lost_password_password_changed')
        );

    }
    catch {
        $self->log->warn($_);
        $c->flash->{twofactor_error} = "Wachtwoorden komen niet overeen";
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/lost_password_new_password')
        );
    }
}

=head2 lost_password_password_changed

Landing page for succesfully reset password.

=head3 URL Path

C</auth/twofactor/lost_password_password_changed>

=cut

# Commit changed password, redirect to login page
sub lost_password_password_changed  : Chained('base') : PathPart('lost_password_password_changed') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;
    if (!$c->session->{_twofactor}{password_reset_done}) {
        throw("twofactor/sequence_error", "Sequence error: can't reset the password without first verifying");
    }

    $c->stash->{template} = 'plugins/twofactor/lost_password_password_changed.tt';
}

=head2 _zaak_create_security

Called by /form to get all authentication related data into the right location
in the session, OR forward to the login page if authentication wasn't
successful yet.

=cut

sub _zaak_create_security : Private {
    my ($self, $c) = @_;

    if (
        ($c->req->params->{authenticatie_methode} // '') eq 'twofactor' ||
        ($c->session->{_zaak_create}{extern}{verified} // '')  eq 'twofactor'
    ) {
        my $interface = $self->_get_interface($c);

        $self->log->trace("Twofactor authentication requested");
        if ($c->session->{_twofactor}{authenticated}) {
            $self->log->trace(sprintf(
                "Twofactor authentication successful for user '%s' (type: %s)",
                $c->session->{_twofactor}{username},
                $c->session->{_zaak_create}{ztc_aanvrager_type},
            ));
            my %extern;

            # Check if we are allowed to create this zaaktype
            $extern{aanvrager_type} = $c->session->{_zaak_create}{ztc_aanvrager_type};
            $extern{verified}       = 'twofactor';
            $extern{id}             = $c->session->{_twofactor}{authenticated_id};

            $c->session->{_zaak_create}{extern} = \%extern;

            $c->stash->{aanvrager_type} = $extern{aanvrager_type};
            $c->stash->{aanvrager}{email}  = $c->session->{_twofactor}{email};
            $c->stash->{aanvrager}{mobiel} = $c->session->{_twofactor}{phone};
        }
        else {
            $self->log->trace("Twofactor authentication not (yet) successful - forwarding to login page");

            my %arguments;

            $arguments{'authenticatie_methode'} = 'twofactor'
                if ($c->req->params->{authenticatie_methode});
            $arguments{'sessreset'} = 1
                if ($c->req->params->{sessreset});

            if (   $c->req->params->{ztc_aanvrager_type}
                && $c->req->params->{ztc_aanvrager_type} =~ /^((?:niet_)?natuurlijk_persoon)$/
            ) {
                $arguments{'ztc_aanvrager_type'} = $1;
            }

            if (   $c->req->params->{zaaktype_id}
                && $c->req->params->{zaaktype_id} =~ /^\d+$/
            ) {
                $arguments{'zaaktype_id'} = $c->req->params->{zaaktype_id};
            }

            if ($interface->jpath('$.allow_new_accounts')) {
                $c->session->{pip_login} = 0;
            }
            else {
                # This hides the "New account" link
                $c->session->{pip_login} = 1;
            }
            $c->res->redirect(
                $c->uri_for(
                    '/auth/twofactor',
                    { success_endpoint => $c->uri_for('/zaak/create/webformulier/', \%arguments) }
                )
            );

            if (
                $c->session->{_zaak_create}{extern} &&
                $c->session->{_zaak_create}{verified} eq 'twofactor'
            ) {
                delete($c->session->{_zaak_create}{extern});
            }

            $c->detach;
        }
    }
    else {
        # Different authentication method requested. Passthrough.
        return;
    }

    if ( $c->session->{_zaak_create}{ztc_aanvrager_type} eq 'natuurlijk_persoon' ) {
        $c->forward('Zaaksysteem::Controller::Plugins::Digid', '_zaak_create_aanvrager');
    }
    else {
        $c->forward('Zaaksysteem::Controller::Plugins::Bedrijfid', '_zaak_create_aanvrager');
    }
}

sub _zaak_create_load_externe_data : Private {
    my ($self, $c) = @_;

    return unless $c->session->{_zaak_create}{extern}{verified} eq 'twofactor' &&
        $c->session->{_zaak_create}{aanvrager_update};

    if($c->req->params->{aanvrager_update}) {
        my $type = ($c->session->{_zaak_create}{ztc_aanvrager_type} eq 'natuurlijk_persoon')
            ? 'natuurlijk_persoon'
            : 'bedrijf';

        my $id = $c->model('Betrokkene')->create(
            $type,
            {
                %{ $c->session->{_zaak_create}{aanvrager_update} },
                'np-authenticated'   => 0,
                'np-authenticatedby' => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_TWOFACTOR,
            }
        );

        $c->session->{_zaak_create}{ztc_aanvrager_id} = "betrokkene-$type-" .  $id;
    }
}

sub _get_interface {
    my ($self, $c) = @_;

    return $c->model('DB::Interface')->search_active({module => 'auth_twofactor'})->first
}

sub _get_authenticated_id {
    my $self = shift;
    my $subject = shift;

    if ($subject->subject_type eq 'person') {
        $self->log->trace("BSN found: '" . $subject->properties->{bsn} . "'");
        return $subject->properties->{bsn};
    }
    else {
        my $rv = $subject->properties->{kvknummer};
        if (length($subject->properties->{vestigingsnummer})) {
            $rv .= sprintf(
                "%012d",
                $subject->properties->{vestigingsnummer}
            );
        }
        $self->log->trace("KVK/VN found: '" . $rv . "'");
        return $rv;
    }
}

=head2 _process_login

Generates the token and sets two factor data in the session.

    $self->_process_login($c, {
        subject    => $subject,
        betrokkene => $betrokkene, # optional
    });

=cut

sub _process_login {
    my ($self, $c, $params) = @_;

    my $model = $c->model('Auth::Alternative');

    $c->session->{_twofactor}{username} = $params->{subject}->username;

    if (my $type = $c->session->{_zaak_create}{ztc_aanvrager_type}) {
        if (   $params->{subject}->subject_type eq 'person'
            && $type ne 'natuurlijk_persoon')
        {
            $c->log->error(
                sprintf(
                    "Invalid subject type %s vs %s",
                    $params->{subject}->subject_type, $type
                )
            );
            throw(
                'alt/auth/invalid/subject_type',
                "Unable to login, not a valid subject"
            );
        }
    }

    my $properties = $params->{subject}->properties;

    $c->session->{_twofactor}{authenticated_id} = $self->_get_authenticated_id($params->{subject});
    $c->session->{_twofactor}{subject_id} = $params->{subject}->id;

    my ($phone, $email);
    if ($params->{betrokkene}) {
        $phone = $params->{betrokkene}->mobiel;
        $email = $params->{betrokkene}->email;
    }
    $phone //= $properties->{phone_number}  // $properties->{initial_phone_number};
    $email //= $properties->{email_address} // $properties->{initial_email_address};

    $c->session->{_twofactor}{email} = $email;
    $c->session->{_twofactor}{phone} = $phone;

    $c->session->{_twofactor}{subject_type} = $params->{subject}->subject_type;

    my $token = $model->create_auth_challenge();
    $c->session->{_twofactor}{second_factor} = $token;

    $self->send_token_per_sms($c);

    return 1;

}

sub _verify_second_factor {
    my $self = shift;
    my $c = shift;
    my $user_token = shift;

    my $token = $c->session->{_twofactor}{second_factor};
    if (!$token) {
        throw("twofactor/sequence_error", "Sequence error: can't do second factor without first registering");
    }

    my $model = $c->model('Auth::Alternative');
    my $ok = $model->verify_second_factor($user_token, $token);

    if ($ok) {
        $c->session->{_twofactor}{authenticated} = 1;
    }
    else {
        $self->log->trace("Second factor: failure");
        $c->flash->{twofactor_error} = "Onjuiste verificatiecode";
    }
    return $ok;

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
