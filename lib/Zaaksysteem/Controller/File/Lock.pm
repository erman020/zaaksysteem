package Zaaksysteem::Controller::File::Lock;

use Moose;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::File::Lock

=head1 DESCRIPTION

File API endpoints for lock interactions on a file

=head1 ACTIONS

=head2 base

Reserves the C</file/[id]/lock> namespace

=cut

sub base : Chained('/file/instance_base') : PathPart('lock') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{ subject } = $c->user->as_object;
}

=head2 update_base

Base action that acquires a preliminary SELECT FOR UPDATE lock on the file

=cut

sub update_base : Chained('base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c) = @_;

    unless ($c->req->method eq 'POST' || 1) {
        throw('file/lock/update_requires_post_request', sprintf(
            'Refusing to update a file lock for non-POST request'
        ));
    }

    # Re-select file with update lock
    $c->stash->{ file } = $c->model('DB::File')->search(
        { id => $c->stash->{ file_id } },
        { for => 'update' }
    )->first;

    $c->stash->{ file }->assert_no_lock($c->stash->{ subject });
}

=head2 get

=head3 URL Endpoint

C</file/[id]/lock/acquire>.

=cut

sub get : Chained('base') : PathPart('') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    $c->stash->{ zapi } = [ $c->stash->{ file } ];
}

=head2 acquire

=head3 URL Endpoint

C</file/[id]/lock/acquire>.

=cut

sub acquire : Chained('update_base') : PathPart('acquire') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    # Acquire lock only if there isn't one yet.
    unless ($c->stash->{ file }->is_locked) {
        $c->log->info(sprintf(
            'Acquiring lock for user "%s" on file "%s"',
            $c->stash->{ subject }->display_name,
            $c->stash->{ file }->filename
        ));

        $c->stash->{ file }->acquire_lock($c->stash->{ subject });
        $c->stash->{ file }->discard_changes;
    }

    $c->detach('get');
}

=head2 release

=head3 URL Endpoint

C</file/[id]/lock/acquire>.

=cut

sub release : Chained('update_base') : PathPart('release') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    # If this conditional is true, combined with the check in update_base(),
    # the current user is the lock owner and is permitted to release the lock
    if ($c->stash->{ file }->is_locked) {
        $c->log->info(sprintf(
            'Releasing lock on file "%s" (was locked by "%s" until "%s")',
            $c->stash->{ file }->filename,
            $c->stash->{ subject }->display_name,
            $c->stash->{ file }->lock_timestamp->iso8601
        ));

        $c->stash->{ file }->release_lock($c->stash->{ subject });
        $c->stash->{ file }->discard_changes;
    }

    $c->detach('get');
}

=head3 extend

=head3 URL Endpoint

C</file/[id]/lock/extend>.

=cut

sub extend : Chained('update_base') : PathPart('extend') : Args(0) : ZAPI {
    my ($self, $c) = @_; 

    # Only extend lock if one exists.
    if ($c->stash->{ file }->is_locked) {
        $c->log->info(sprintf(
            'Extending lock for user "%s" on file "%s"',
            $c->stash->{ subject }->display_name,
            $c->stash->{ file }->filename
        ));

        $c->stash->{ file }->extend_lock($c->stash->{ subject });
        $c->stash->{ file }->discard_changes;
    }

    $c->detach('get');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
