package Zaaksysteem::Controller::Betrokkene;

use Moose;

use Clone qw(clone);
use Email::Valid;
use JSON;

use 5.010;

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR
    VALIDATION_CONTACT_DATA
    ZAAKSYSTEEM_CONSTANTS
    LOGGING_COMPONENT_BETROKKENE
    DOCUMENTS_STORE_TYPE_NOTITIE

    RGBZ_LANDCODES
/;

use BTTW::Tools;
use Zaaksysteem::Types qw(BSN EmailAddress NonEmptyStr MobileNumber);


BEGIN { extends 'Zaaksysteem::Controller' }

my $landcodes = [];
{
    my %remap = reverse %{ RGBZ_LANDCODES() };
    for my $country (sort keys %remap) {
        push(@{ $landcodes },
            {
                value   => $remap{$country},
                label   => $country,
            }
        );
    }
}

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Zaaksysteem::Controller::Betrokkene in Betrokkene.');
}

sub base : Chained('/') : PathPart('betrokkene'): CaptureArgs(0) {
    my ($self, $c) = @_;

    ## Zaakid?
    if ($c->req->params->{'zaak'}) {
        $c->stash->{zaak} = $c->model('DB::Zaak')->find($c->req->params->{'zaak'});
    }
}

{
    define_profile create => (
        required => [ qw/
            betrokkene_type
            np-geslachtsnaam
        /],
        optional => [ qw/
            create
            np-voornamen
            np-burgerservicenummer
            np-huisnummertoevoeging
            np-voorvoegsel
            np-adellijke_titel
            np-geboortedatum
            np-geslachtsaanduiding
            np-in_gemeente
            np-landcode
            np-huisnummer
            np-postcode
            np-straatnaam
            np-woonplaats
            np-adres_buitenland1
            np-adres_buitenland2
            np-adres_buitenland3
            npc-telefoonnummer
            npc-email
            npc-mobiel

            np-correspondentie_huisnummer
            np-correspondentie_huisnummertoevoeging
            np-correspondentie_straatnaam
            np-correspondentie_postcode
            np-correspondentie_woonplaats
        /],
        constraint_methods  => {
            'np-burgerservicenummer'    => sub {
                my $val = pop;
                return BSN->check($val);
            },
            'np-geboortedatum'          => qr/[\d-]+/,
            'np-geslachtsnaam'          => qr/.+/,
            'np-huisnummer'             => qr/^\d+$/,
            'np-huisnummertoevoeging'   => qr/^[\w\d\s-]{1,4}$/,
            'np-postcode'               => qr/^\d{4}[a-zA-Z]{2}$/,
            'np-woonplaats'             => qr/.+/,
            'np-straatnaam'             => qr/.+/,
            'np-correspondentie_huisnummer' => qr/^\d+$/,
            'np-correspondentie_huisnummertoevoeging' => qr/^[\w\d\s-]{1,4}$/,
            'np-correspondentie_postcode'   => qr/^\d{4}[a-zA-Z]{2}$/,
            'np-correspondentie_woonplaats' => qr/.+/,
            'np-correspondentie_straatnaam' => qr/.+/,
            'np-voorletters'            => qr/[\w.]+/,
            'np-voornamen'              => qr/.+/,
            'np-landcode'               => qr/^\d+$/,
            'npc-email'                 => qr/^.+?\@.+\.[a-z0-9]{2,}$/,
            'npc-telefoonnummer'        => qr/^[\d\+]{6,15}$/,
            'npc-mobiel'                => qr/^[\d\+]{6,15}$/,
        },
        dependencies => {
            'np-landcode' => sub {
                my $dfv     = shift;
                my $code    = shift;

                if ($code eq '6030') {
                    if ($dfv->get_input_data->{briefadres}) {
                        return ['np-correspondentie_postcode','np-correspondentie_huisnummer','np-correspondentie_straatnaam','np-correspondentie_woonplaats'];
                    } else {
                        return ['np-postcode','np-huisnummer','np-straatnaam','np-woonplaats'];
                    }
                } else {
                    return ['np-adres_buitenland1'];
                }
            }
        },
        msgs => {
            'format'    => '%s',
            'missing'   => 'Veld is verplicht.',
            'invalid'   => 'Veld is niet correct ingevuld.',
            'constraints' => {
                'np-postcode'       => 'Postcode zonder spatie (1000AA)',
                'np-correspondentie_postcode' => 'Postcode zonder spatie (1000AA)',
                'np-telefoonnummer' => 'Nummer zonder spatie (e.g: +312012345678)',
                'np-mobiel'         => 'Nummer zonder spatie (e.g: +316123456789)',
            }
        },
    );

    sub create : Chained('/') : PathPart('betrokkene/create'): Args(0) {
        my ($self, $c) = @_;

        if ($c->req->is_xhr) {
            $c->zvalidate;
            $c->detach;
        }

        ### Default: view
        $c->stash->{template}   = 'betrokkene/create.tt';

        if ($c->req->method eq 'POST') {
            # Validate information
            my $params = $c->req->params;
            return unless $c->zvalidate && $params->{create};

            ### Create person

            # Convert postcode
            $params->{'np-postcode'} = uc($params->{'np-postcode'});

            my $id = $c->model('Betrokkene')->create(
                'natuurlijk_persoon',
                {
                    %$params,
                    authenticatedby =>
                        ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR,
                }
            );

            $c->model('DB::Logging')->trigger('subject/create', {
                component => LOGGING_COMPONENT_BETROKKENE,
                component_id => $id,
                created_for => $id,
                data => {
                    subject_id => $id
                }
            });

            if ($id) {
                $c->push_flash_message('Natuurlijk persoon aangemaakt');
                $c->res->redirect(
                    $c->uri_for(
                        '/betrokkene/' . $id,
                        { gm => 1, type => 'natuurlijk_persoon' }
                    )
                );
            }
        }

    }
}

=head2 generate_alternative_authentication

=head3 URI

    /betrokkene/generate_alternative_authentication/[ID]

Generate alternative authentication subject for a betrokkene

=cut

define_profile generate_alternative_authentication => (
    required => {
        username     => NonEmptyStr,
        subject_type => NonEmptyStr,
        email        => EmailAddress,
        phone        => MobileNumber,
    },
    optional => {
        bsn              => BSN,
        kvk              => 'Num',
        vestigingsnummer => 'Num',
        active           => 'Bool',
    }
);

sub generate_alternative_authentication : Chained('base') : PathPart('generate_alternative_authentication') : Args(1) {
    my ($self, $c, $gmid) = @_;

    my $args = assert_profile($c->req->params)->valid;
    $self->_alt_auth_try_catch($c,
        sub {

            $c->assert_any_user_permission(
                qw[beheer contact_nieuw contact_edit_subset]);

            my $model = $c->model('Auth::Alternative');

            my $betrokkene = $c->model('Betrokkene')->get(
                {
                    type   => $args->{subject_type},
                    intern => 0,
                },
                $gmid
            );

            # TODO: Lookup existing subjec for betrokkene
            my $object  = $betrokkene->gm_object;
            my $subject = $model->find_subject_by_betrokkene($betrokkene);
            my $ue;

            my $subject_type
                = $args->{subject_type} eq 'natuurlijk_persoon'
                ? 'person'
                : 'company';

            if ($subject_type eq 'person') {
                if (!BSN->check($object->bsn)) {
                    throw("auth/alternative/invalid/bsn", "Invalid BSN");
                }
            }

            my $email = Email::Valid->address($args->{email});

            if (!$subject) {
                $ue = $model->create_account(
                    username     => $args->{username},
                    email        => $email,
                    phone        => $args->{phone},
                    subject_type => $subject_type,
                    $subject_type eq 'person'
                    ? (bsn => $object->bsn)
                    : (
                        kvknummer        => $object->dossiernummer,
                        vestigingsnummer => $object->vestigingsnummer
                    ),
                );
                $subject = $ue->subject_id;
                $model->set_subject_link($subject, $object);
            }
            else {
                $model->update_subject($subject, $args);
                $ue = $subject->user_entities->search_rs(
                    { source_interface_id => $model->interface->id })->first;
            }

            if ($args->{active} && !$subject->is_active) {
                $model->activate_account($ue);
            }
            elsif (!$args->{active} && $subject->is_active) {
                $model->disable_account($ue);
            }
        }
    );
}

=head2 send_activation_link

=head3 URI

    /betrokkene/send_activation_link/[ID]

Send activation link for a betrokkene

=cut

define_profile send_activation_link => (
    required => {
        subject_type => NonEmptyStr,
    },
);


sub send_activation_link : Chained('base') : PathPart('send_activation_link') : Args(1) {
    my ($self, $c, $gmid) = @_;

    my $args    = assert_profile($c->req->params)->valid;
    $self->_alt_auth_try_catch($c,
        sub {
            my $subject = $self->get_alt_auth_account($c, $gmid, $args->{subject_type});
            my $model = $c->model('Auth::Alternative');

            my $link = $model->create_activation_link($subject);
            if (!$subject->is_active) {
                my $ue = $subject->user_entities->search_rs(
                    { source_interface_id => $model->interface->id })->first;
                $model->activate_account($ue);
            }

            $model->send_activation_link($subject,
                $c->uri_for('/auth/twofactor/activate/' . $link->token));
        }
    );

}

sub _alt_auth_try_catch {
    my ($self, $c, $sub) = @_;

    $c->stash->{zapi} = try {
        $sub->();
        return [ { success => 1 } ];
    }
    catch {
        $self->log->info("$_");
        return Zaaksysteem::ZAPI::Error->new_from_error(
            $_,
            'auth/alternative/unknown_error'
        );
    };

    $c->detach($c->view('ZAPI'));
}

=head2 get_alt_auth_account

Get the alternative authentication account for the given betrokkene

=cut

sig get_alt_auth_account => 'Defined,Int,Str';

sub get_alt_auth_account {
    my ($self, $c, $id, $type) = @_;

    my $betrokkene = $c->model('Betrokkene')->get(
        {
            type   => $type,
            intern => 0,
        },
        $id,
    );

    if (!$betrokkene) {
        throw('auth/alternative/no_betrokkene',
            "No betrokkene found with id $id as type $type");
    }

    my $subject = $c->model('Auth::Alternative')->find_subject_by_betrokkene($betrokkene);
    return $subject if $subject;

    throw(
        'auth/alternative/no_account',
        "No subject found for betrokkene"
    );
}


sub view_base : Chained('base'): PathPart('') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    die "view_base: id not set, nothing to do here" unless $id =~ /^\d+$/;

    my $betrokkene_type = $c->req->params->{type};

    $c->stash->{requested_bid} = $id;

    if ($c->req->params->{gm}) {
        $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
            {
                type    => $betrokkene_type,
                intern  => 0,
            },
            $id
        );
        if (
            !$c->stash->{betrokkene} ||
            (
                $c->stash->{betrokkene}->can('gm_extern_np') &&
                $c->stash->{betrokkene}->gm_extern_np &&
                $c->stash->{betrokkene}->gm_extern_np->can('deleted_on') &&
                $c->stash->{betrokkene}->gm_extern_np->deleted_on
            )
        ) {
            my %query_params = ( flash_message_action => 'subject_not_found' );
            $c->res->redirect($c->uri_for('/intern/', \%query_params));
            $c->detach;
        }

        ### A dirty place to implement, because of the lack of possibilities
        ### in this old Betrokkene object.
        if ($c->stash->{betrokkene} && !$c->stash->{betrokkene}->has_valid_address) {
            my %query_params = ( flash_message_action => 'subject_no_address' );
            $c->res->redirect($c->uri_for('/intern/', \%query_params));
            $c->detach;
        }

        # Enable edit-mode form for current subject if requested
        if ($c->req->params->{ edit }) {
            # Double check user's permissions, even though they shouldn't end
            # up here anyway.
            $c->assert_any_user_permission(qw[beheer contact_nieuw]);

            # Deny edit-mode for autentic subjects (via StUF vector etc)
            if ($c->stash->{ betrokkene }->authenticated) {
                throw('subject/update/not_permitted', sprintf(
                    'Subject "%s" is authentic, cannot edit',
                    $c->stash->{ betrokkene }->display_name
                ));
            }

            $c->stash->{ betrokkene_edit } = 1;
        }
    } else {
        $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
            {},
            $id
        );
    }

    if($c->stash->{betrokkene}->can('active') && !$c->stash->{betrokkene}->active) {
        $c->push_flash_message( 'Deze persoon wordt niet meer gevolgd '
            . 'door het Zaaksysteem en bevat mogelijk onjuiste '
            . 'persoonsgegevens. De persoon wordt opnieuw geactiveerd bij '
            . 'het registreren van een zaak.'
        );
    }

    if($c->stash->{betrokkene}->can('note') && $c->stash->{betrokkene}->note) {
        $c->push_flash_message('Interne notitie: ' . $c->stash->{betrokkene}->note);
    }

    $c->forward('include_woz_tab');
    $c->forward('signature_settings');

    $c->detach unless $c->stash->{betrokkene};

    if ($betrokkene_type eq 'medewerker') {
        $c->stash->{ user_can_change_password } =
            $c->model('DB::Config')->get('users_can_change_password') &&
            $self->looking_at_self($c);

        if ($c->user->login_entity->source_interface_id->module ne 'authldap') {
            ### User not logged in via username/password
            delete $c->stash->{ user_can_change_password };
        }

        # No use in changing your extension if there is no KCC module active.
        if ($c->model('DB::Interface')->search_active({ module => 'kcc' })->first) {
            $c->stash->{ user_can_change_extension } = $self->looking_at_self($c);
        }
    }

    $c->forward('handle_betrokkene_session');
}


=head2 signature_settings

Determines wether the signature settings need to be shown.

There are two options:
1) Behandelaars can change their own signature (default)
This means that a behandelaar will see the settings on their own page.

2) Zaaksysteembeheerders and Administrators can change signatures for behandelaars
This means that the role must be checked.

Signature are always applied to the user that's currently being viewed.

=cut

sub signature_settings : Private {
    my ($self, $c) = @_;

    my $role = $c->model('DB::Config')->get('signature_upload_role') || '';

    if ($role eq 'behandelaar') {
        # see if behandelaar is looking at own page
        $c->stash->{show_signature_settings} = $self->looking_at_self($c);

    } elsif ($role eq 'zaaksysteembeheerder') {

        $c->stash->{show_signature_settings} = $c->check_any_user_permission('owner_signatures');
    }
}


=head2 looking_at_self

See if we have an employee looking at own page. Compare the uuid of the
logged in user to the user on the stash.

ldap_rs is a legacy misnomer, it refers to the subject table row.
$c->user also maps to a row in the subject table.

=cut

sub looking_at_self {
    my ($self, $c) = @_;

    return $c->stash->{betrokkene} &&
        $c->stash->{betrokkene}->type eq 'medewerker' &&
        $c->stash->{betrokkene}->can('ldap_rs') &&
        $c->stash->{betrokkene}->ldap_rs &&
        $c->user &&
        $c->stash->{betrokkene}->ldap_rs->uuid eq $c->user->uuid;
}


sub include_woz_tab : Private {
    my ($self, $c) = @_;

    return if !$c->show_woz;

    my $b = $c->stash->{betrokkene};
    foreach (qw(burgerservicenummer dossiernummer)) {
        next unless $b->can($_);
        $c->forward('/beheer/woz/view', [ $b->btype . '-' . int($b->$_()) , $c->req->params->{woz_id} ]);
        last;
    }
}

sub woz_object : Chained('view_base'): PathPart('woz_object') {
    my ($self, $c) = @_;
}


sub handle_betrokkene_session : Private {
    my ($self, $c) = @_;

    if ($c->req->params->{enable_betrokkene_session}) {
        $c->betrokkene_session_enable($c->stash->{betrokkene});

        # Re-initialize betrokkene_session in stash
        $self->prepare_page($c);

        if ($c->req->is_xhr) {
            $c->stash->{json} = {
                succes  => 1,
                naam    => $c->betrokkene_session->naam,
                url     => $c->uri_for(
                    '/betrokkene/' . $c->betrokkene_session->ex_id,
                    {
                        gm  => 1,
                        type => $c->betrokkene_session->btype

                    }
                )->as_string
            };

            $c->detach('Zaaksysteem::View::JSONlegacy');
        }
    }
}

=head2 get_betrokkene_session

ZAPI (GET) callable for lookup of the current active subject session, if any.

=head2 URL Construction

B</betrokkene/get_session>

=cut

sub get_betrokkene_session : GET : Chained('/') : PathPart('betrokkene/get_session') : Args(0) {
    my ($self, $c) = @_;

    my $betrokkene = $c->betrokkene_session;

    if ($betrokkene) {
        $c->stash->{zapi} = [$betrokkene->as_hashref];
    } else {
        $c->stash->{zapi} = [ ];
    }

    return $c->detach('Zaaksysteem::View::ZAPI');
}


=head1 Disable subject session

ZAPI (POST) callable for removing / disabling the current active subject session, if any.

=head2 URL Construction

B</betrokkene/disable_session>

=cut

sub disable_betrokkene_session : POST : Chained('/') : PathPart('betrokkene/disable_session') : Args(0) {
    my ($self, $c) = @_;

    if(!$c->betrokkene_session_disable) {
        throw('betrokkene/session_disable_failed', 'Kon actieve betrokkene niet uitzetten.');
    }

    $c->stash->{zapi} = [];
    $c->detach('Zaaksysteem::View::ZAPI');
}

=head1 Enable a subject session

ZAPI (POST) callable for enabling the subject session for a specific subject.

=head2 URL Construction

B</betrokkene/enable_session>

=head2 POST variables

=over

=item * identifier

The identifier of the subject to set in the session.

=back

=cut

define_profile enable_betrokkene_session => (
    required => [qw(identifier)],
);

sub enable_betrokkene_session : Chained('/') : JSON : PathPart('betrokkene/enable_session') : Args(0) {
    my ($self, $c) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $betrokkene = $c->model('Betrokkene')->get({}, $opts->{identifier})
        or throw("betrokkene/not_found", "Betrokkene niet gevonden.");

    $c->betrokkene_session_enable($betrokkene);

    # Re-initialize betrokkene_session in stash
    $self->prepare_page($c);

    $c->stash->{zapi} = [$betrokkene->as_hashref];

    return $c->detach('Zaaksysteem::View::ZAPI');
}

=head2 prepare_page

Retrieves the currently active subject session details, if any are set, and
puts them in the stash for the template to pick up.

=cut

sub prepare_page {
    my ($self, $c) = @_;

    if (!$c->user_exists) {
        return;
    }

    my $subject = $c->betrokkene_session;
    my $subject_data;
    if($subject) {
        $subject_data = $subject->as_hashref();
    }

    $c->stash->{betrokkene_session} = JSON->new->utf8(0)->encode({subject => $subject_data});

    return;
}

sub _betrokkene_zaken : Private {
    my ($self, $c, $opts)   = @_;

    $c->stash->{results_per_page} = ( $c->req->params->{results_per_page} || $opts->{rows} || 10 );

    my $resultset = $c->model('Zaken')->zaken_pip(
        {
            page => ($opts->{page} || $c->req->params->{'page'} || 1),
            rows                      => $c->stash->{results_per_page},
            betrokkene_type           => $opts->{betrokkene}->btype,
            gegevens_magazijn_id      => $opts->{betrokkene}->ex_id,
            type_zaken                => $opts->{type_zaken},
            sort_direction            => $c->req->params->{sort_direction},
            sort_field                => $c->req->params->{sort_field},
            show_all_betrokkene_cases => 1,
            as_aanvrager              => $opts->{as_aanvrager},
            as_betrokkene             => $opts->{as_betrokkene},
            as_gemachtigde            => $opts->{as_gemachtigde},
        }
    );

    return $c->model('Zaken')->filter({
        resultset      => $resultset,
        textfilter     => $opts->{textfilter},
        dropdown       => (
            $opts->{statusfilter} ||
            $c->req->params->{'statusfilter'},
        )
    });
}

sub view : Chained('view_base'): PathPart('') : Args() {
    my ($self, $c) = @_;

    $c->stash->{landcodes}  = $landcodes;
    $c->stash->{template}   = 'betrokkene/view.tt';

    if ($c->check_any_user_permission(qw/contact_nieuw contact_search/)) {
        $c->stash->{can_betrokkene_edit} = 1;
    }

    # Check if the interface exists, show the omgevingen tab
    my $cp = $c->model('DB::Interface')->search_active({ module => 'controlpanel' })->first;
    if ($cp) {
        $c->stash->{'show_controlpanel'}   = 1;
        $c->stash->{'controlpanel_domain'} = $cp->get_interface_config->{domain};
    }
    else {
        $c->stash->{'show_controlpanel'} = 0;
    }

    if ($c->user_exists && $c->stash->{'betrokkene'}) {
        $c->stash->{'betrokkene'}->log_view(
            'betrokkene-medewerker-' . $c->user->uidnumber
        );
    }

    $c->stash->{force_result_finish} = 1;

    $c->stash->{zaken}  = $c->forward('_betrokkene_zaken', [
        {
            rows                    => 10,
            betrokkene              => $c->stash->{betrokkene},
            as_aanvrager            => 1,
            type_zaken              => ['resolved', 'new', 'open', 'stalled'],
        }
    ]);
    $c->stash->{'zaken_display_fields'} = $c->model('SearchQuery')->get_display_fields();

    $c->stash->{open_zaken}  = $c->forward('_betrokkene_zaken', [
        {
            rows                    => 10,
            betrokkene              => $c->stash->{betrokkene},
            type_zaken              => ['new', 'open', 'stalled'],
            as_aanvrager            => 1,
            textfilter              => $c->req->params->{open_textfilter},
        }
    ]);

    # status
    {
        my $open_display_fields = {
            map { $_->{fieldname} => $_ }
            @{ $c->model('SearchQuery')->get_display_fields() }
        };

        $open_display_fields->{'days_left'}->{label} = 'Dagen';

        $c->stash->{'open_display_fields'} = [];
        push(
            @{ $c->stash->{'open_display_fields'} },
            $open_display_fields->{ $_ }
        ) for qw/status me.id zaaktype_node_id.titel
        me.onderwerp days_left/;
    }

    if (my $bag_object = $c->stash->{betrokkene}->bag_object) {
        $c->stash->{adres_zaken}        = $c->model('Zaken')->adres_zaken(
            {
                page                    => ($c->req->params->{'page'} || 1),
                rows                    => 10,
                nummeraanduiding_id     => $bag_object->nummeraanduiding->{identification},
                'sort_direction'        => $c->req->params->{sort_direction},
                'sort_field'            => $c->req->params->{sort_field},
            }
        );

        $c->stash->{'adres_display_fields'} = $c->model('SearchQuery')->get_display_fields();
    }

    $c->stash->{gemachtigde_zaken} = $c->forward(
        '_betrokkene_zaken',
        [
            {
                rows       => 10,
                betrokkene => $c->stash->{betrokkene},
                type_zaken => ['resolved', 'new', 'open', 'stalled'],
                as_betrokkene => 1,
            }
        ]
    );

    $c->stash->{betrokken_bij_zaken} = $c->forward(
        '_betrokkene_zaken',
        [
            {
                rows       => 10,
                betrokkene => $c->stash->{betrokkene},
                type_zaken => ['resolved', 'new', 'open', 'stalled'],
                as_betrokkene  => 1,
                as_gemachtigde => 0,
            }
        ]
    );


    my $betrokkene_obj = $c->stash->{betrokkene};
    if (
        $betrokkene_obj &&
        $betrokkene_obj->can('messages') &&
        $betrokkene_obj->messages &&
        scalar(keys %{ $betrokkene_obj->messages })
    ) {
        $c->push_flash_message(
            'Let op: '
                . join(', ',
                    map(
                        { ucfirst($_) }
                        values %{ $betrokkene_obj->messages }
                    )
                ),
        );
    }

    my $sort_field = $c->req->params->{ sort_field } || '';
    my $sort_direction = lc($c->req->params->{ sort_direction } || '');

    $sort_direction = 'asc' unless $sort_direction eq 'desc';
    $sort_field =~ s/[^a-zA-Z\d]//g;

    $c->stash->{ sort_field } = $sort_field;
    $c->stash->{ sort_direction } = $sort_direction;
}

sub search : Chained('base'): PathPart('search') {
    my ($self, $c) = @_;

    if ( $c->req->params->{betrokkene_type} eq 'natuurlijk_persoon' and $c->req->params->{do_validation} ) {
        $c->register_profile(
            'method'    => 'search',
            profile     => _np_search_validator()
        );
        $c->zvalidate;
        $c->detach;
    }

    $c->stash->{ $_ } = $c->req->params->{ $_ } for (
        keys (%{ $c->req->params })
    );

    my $stufconfig = try {
        return $c->model('DB::Interface')->find_by_module_name('stufconfig');
    }
    catch {
        $self->log->warn($_);
        return;
    };

    my $stuf_np_interface;

    if ($stufconfig && $stufconfig->active && $c->stash->{betrokkene_type} eq 'natuurlijk_persoon') {
        my $stuf_params = $stufconfig->get_interface_config;
        my $module      = $stufconfig->module_object;

        ### Prevent external search when disabled
        unless ($stuf_params->{search_extern_webform_only}) {
            if ($stuf_np_interface = $module->can_search_sbus($stufconfig)) {
                $c->stash->{stuf_sbus_search} = 1;
            }

            if (my $stuf_np_gbav_interface = $module->can_search_gbav($stufconfig)) {
                $c->stash->{stuf_gbav_search} = 1;
                $stuf_np_interface      = $stuf_np_gbav_interface;
            }
        }
    }

    if ($c->req->is_xhr) {
        $c->stash->{nowrapper} = 1;

        $c->stash->{betrokkene_type} = $c->req->params->{betrokkene_type} ||
            $c->req->params->{jstype};

        if (exists($c->req->params->{search})) {
            my %sparams = ();

            for my $key (keys %{ $c->req->params }) {
                if ($c->req->params->{$key}) {
                    my $rawkey = $key;
                    $key =~ s/np-//g;
                    $sparams{$key} = $c->req->params->{$rawkey};
                }
            }

            # Geboortedatum...
            if ($sparams{'geboortedatum-dag'}) {
                $sparams{'geboortedatum'} =
                    sprintf('%02d', $sparams{'geboortedatum-jaar'}) . '-'
                    . sprintf('%02d', $sparams{'geboortedatum-maand'}) . '-'
                    .$sparams{'geboortedatum-dag'};
            } elsif ($sparams{'geboortedatum'}) {
                $sparams{'geboortedatum'} =~ s/^(\d{2})-(\d{2})-(\d{4})$/$3-$2-$1/;
            }

            if(exists $sparams{ burgerservicenummer }) {
                $sparams{ burgerservicenummer } = int($sparams{ burgerservicenummer });
            }

            my $rows_per_page = $c->req->param('rows_per_page') || 40;
            delete($sparams{$_}) for qw/import_datum url method jscontext jsversion jsfill submit search jstype rows_per_page/;

            my $type    = $c->req->params->{jstype};
            if ($c->req->params->{jsversion} == 3) {
                delete($sparams{$_}) for grep { /^ezra_client_info/ } keys %{
                    $c->req->params
                };
                $type   = $c->req->params->{betrokkene_type};
            }

            $c->stash->{betrokkene_type} = $type;

            delete($sparams{betrokkene_type});

            $c->stash->{template} = 'betrokkene/popup/search_resultrows.tt';

            if ($c->req->params->{inactive_search}) {
                $sparams{inactive_search} = 1;
            }

            my $betrokkenen = $c->model('Betrokkene')->search(
                {
                    type    => $type,
                    intern  => 0,
                    rows_per_page => $rows_per_page,
                },
                \%sparams
            );

            $c->stash->{results} = [];

            if ($betrokkenen) {
                while (my $bet = $betrokkenen->next) {
                    push (@{ $c->stash->{results} }, $bet);
                }
            }

            $c->detach;
        }

        $c->stash->{template} = 'betrokkene/popup/search.tt';
    } else {
        $c->stash->{template} = 'betrokkene/search.tt';

        ## Paging
        $c->stash->{ $_ } = $c->req->params->{ $_ }
            for grep {
                $c->req->params->{ $_ } &&
                $c->req->params->{ $_ } =~ /^\d+/
            } qw/paging_page paging_rows/;

        my %sparams = ();
        my ($startsearch, $betrokkene_type);

        if (exists($c->req->params->{search})) {
            for my $key (keys %{ $c->req->params }) {
                if ($c->req->params->{betrokkene_type} eq 'natuurlijk_persoon') {
                    if ($c->req->params->{$key} && $key =~ /^np-/) {
                        my $rawkey = $key;
                        $key =~ s/np-//g;
                        $sparams{$key} = $c->req->params->{$rawkey};
                    }
                } elsif ($c->req->params->{betrokkene_type} eq 'bedrijf') {
                    my $rawkey = $key;
                    next if (
                        lc($rawkey) eq 'search' ||
                        lc($rawkey) eq 'betrokkene_type'
                    );
                    $sparams{$key} = $c->req->params->{$rawkey};
                } elsif ($c->req->params->{betrokkene_type} eq 'medewerker') {
                    my $rawkey = $key;
                    next if (
                        lc($rawkey) eq 'search' ||
                        lc($rawkey) eq 'betrokkene_type'
                    );
                    $sparams{$key} = $c->req->params->{$rawkey};
                }

            }
            $betrokkene_type = $c->req->params->{'betrokkene_type'};

            $startsearch++;
        } elsif (
            (
                $c->stash->{paging_page} ||
                $c->req->params->{order}
            ) && $c->session->{betrokkene_search_data}
        ) {
            %sparams            = %{ $c->session->{betrokkene_search_data} };
            $betrokkene_type    = $c->session->{betrokkene_type};
            $startsearch++;
        } else {
            delete($c->session->{betrokkene_search_data});
        }

        if ($startsearch) {
            $c->session->{betrokkene_search_data} = \%sparams;
            $c->session->{betrokkene_type} = $betrokkene_type;

            $c->stash->{template} = 'betrokkene/search_results.tt';

            # Geboortedatum...
            if ($sparams{'geboortedatum-dag'}) {
                $sparams{'geboortedatum'} =
                    sprintf('%02d', $sparams{'geboortedatum-jaar'}) . '-'
                    . sprintf('%02d', $sparams{'geboortedatum-maand'}) . '-'
                    .$sparams{'geboortedatum-dag'};
            } elsif ($sparams{'geboortedatum'}) {
                $sparams{'geboortedatum'} =~ s/^(\d{2})-(\d{2})-(\d{4})$/$3-$2-$1/;
            }

            if(exists $sparams{ burgerservicenummer }) {
                $sparams{ burgerservicenummer } = int($sparams{ burgerservicenummer });
            }

            $c->stash->{betrokkenen} = $c->model('Betrokkene')->search(
                {
                    type    => $betrokkene_type,
                    intern  => 0,
                },
                \%sparams
            );

            $c->stash->{betrokkene_type} = $betrokkene_type;
        }

    }
}

sub external_import : Chained('/'): PathPart('betrokkene/external_import'): Args(0) {
    my ($self, $c)  = @_;

    if ($c->req->params->{external_transaction_id}) {
        my $stufconfig  = $c->model('DB::Interface')
                        ->find_by_module_name('stufconfig');

        my $np;

        if ($c->req->params->{external_transaction_id} =~ /bsn/) {
            my ($bsn)   = $c->req->params->{external_transaction_id} =~ /bsn-(\d+)$/;

            if ($bsn) {
                ($np) = grep (
                    {
                        $_->{burgerservicenummer} eq $bsn
                    }
                    @{ $c->session->{last_sbus_results} }
                );
            }
        } elsif ($c->req->params->{external_transaction_id} =~ /external_id/) {
            my ($external_id)   = $c->req->params->{external_transaction_id} =~ /external_id-(\d+)$/;

            if ($external_id) {
                ($np) = grep (
                    {
                        $_->{external_id} eq $external_id
                    }
                    @{ $c->session->{last_sbus_results} }
                );
            }
        }

        my $stuf_np_interface = $stufconfig->module_object->get_natuurlijkpersoon_interface(
            $stufconfig
        );

        my $result;
        eval {
            $result = $stuf_np_interface->process_trigger('import', $np);
        };

        if ($@) {
            $c->stash->{json} = $c->return_error($@);
        } else {
            $c->stash->{json} = $c->view('JSON')->prepare_json_row(
                {
                    betrokkene_identifier   => $result->{betrokkene_identifier}
                }
            );
        }
    }

    $c->forward('View::JSON');
}



sub snapshot : Chained('base'): PathPart('snapshot'): Args(2) {
    my ($self, $c, $betrokkene_type, $id) = @_;


    $c->stash->{'betrokkene'} = $c->model('Betrokkene')->get(
        {
            intern  => 1,
            type    => $betrokkene_type,
        },
        $id
    );

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'betrokkene/popup/get.tt';
}


sub get : Chained('base'): PathPart('get'): Args(1) {
    my ($self, $c, $id) = @_;

    die "betrokkene_id not set" unless($id);

    my $params = $c->req->params;

    if ($params->{betrokkene_type}) {
        $c->stash->{'betrokkene'} = $c->model('Betrokkene')->get(
            {
                intern  => 0,
                type    => $params->{betrokkene_type},
            },
            $id
        ) or return;
    } else {
        $c->stash->{'betrokkene'} = $c->model('Betrokkene')->get({}, $id)
            or return;
    }

    if ($c->user_exists && $c->stash->{'betrokkene'}) {
        $c->stash->{'betrokkene'}->log_view(
            'betrokkene-medewerker-' . $c->user->uidnumber
        );
    }

    if ($params->{actueel} && $params->{actueel} =~ /^\d+$/) {
        if ($c->stash->{'betrokkene'}->gm_extern_np) {
            my $gegevens_magazijn_id =
                $c->stash->{'betrokkene'}->gm_extern_np->id;

            $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
                {
                    intern  => 0,
                    type    => $c->stash->{betrokkene}->btype,
                },
                $gegevens_magazijn_id
            );
        }
    }

    if ($c->req->is_xhr) {
        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = 'betrokkene/popup/get.tt';
    }
}



{
    sub _load_update_profile {
        my ($self, $c, $gmid) = @_;

        my $profile;

        if ($c->req->params->{betrokkene_type} eq 'bedrijf') {
            ### Get profile from Model
            $profile = $c->get_profile(
                'method'=> 'create',
                'caller' => 'Zaaksysteem::Betrokkene::Object::Bedrijf'
            ) or return;

            my @required_fields = grep {
                $_ ne 'vestiging_postcodewoonplaats' ||
                $_ ne 'vestiging_adres'
            } @{ $profile->{required} };

            push(@required_fields, 'rechtsvorm');

            $profile->{required} = \@required_fields;
        } else {
            $profile = $c->get_profile(
                'method' => 'create',
                'caller' => 'Zaaksysteem::Controller::Betrokkene'
            ) or return;

        }

        $c->register_profile(
            method  => 'update',
            profile => _add_profile_checks(
                context         => $c,
                betrokkene_type => $c->req->params->{betrokkene_type},
                profile         => clone($profile),
                contact_id      => $gmid,
            ),
        );
    }

    my $BETROKKENE_MAP = {
        bedrijf => 2,
        natuurlijk_persoon => 1,
    };

    define_profile(update => %{ VALIDATION_CONTACT_DATA() });

    sub update : Chained('base'): PathPart('info/update'): Args(1) {
        my ($self, $c, $gmid) = @_;

        $c->assert_any_user_permission(qw[beheer contact_nieuw contact_edit_subset]);

        my $user_can_edit = $c->check_any_user_permission(qw[beheer contact_nieuw]);

        my $params = $c->req->params();

        ### Betrokkene edit only
        if ($params->{betrokkene_edit}) {           
            $self->_load_update_profile($c, $gmid)
        } else {
            $c->register_profile(
                'method'    => 'update',
                profile     => VALIDATION_CONTACT_DATA,
            );
        }

        if ($c->req->is_xhr) {
            $c->zvalidate;
            $c->detach;
        }

        ### END Betrokkene edit only

        my $contact_data = $c->model('DB::ContactData')->search({
            gegevens_magazijn_id  => $gmid,
            betrokkene_type         => $BETROKKENE_MAP->{
                $params->{betrokkene_type}
            },
        });

        my $betrokkene_ident = sprintf(
            'betrokkene-%s-%d',
            $params->{ betrokkene_type },
            $gmid
        );

        if ($contact_data->count) {
            $contact_data = $contact_data->first;
        } else {
            $contact_data = $c->model('DB::ContactData')->create({
                    gegevens_magazijn_id    => $gmid,
                    betrokkene_type         => $BETROKKENE_MAP->{
                        $params->{betrokkene_type}
                    },
            });
        }

        # Update niet authentieke gegevens
        if ($params->{betrokkene_edit} && (my $dv = $c->zvalidate) && $user_can_edit) {
            my $gmbetrokkene = $c->model('Betrokkene')->get(
                {
                    type    => $params->{betrokkene_type},
                    intern  => 0,
                },
                $gmid
            );

            unless ($gmbetrokkene->authenticated) {
                if ($params->{betrokkene_type} eq 'bedrijf') {
                    my $params = $dv->valid;

                    ### Depending on the country, remove params
                    if (
                        $params->{vestiging_landcode} &&
                        $params->{vestiging_landcode} != '6030'
                    ) {
                        $params->{$_} = undef for qw/
                            vestiging_postcode
                            vestiging_adres
                            vestiging_straatnaam
                            vestiging_huisnummer
                            vestiging_huisletter
                            vestiging_huisnummertoevoeging
                            vestiging_postcodewoonplaats
                            vestiging_postcode
                            vestiging_woonplaats
                        /;
                    } else {
                        $params->{$_} = undef for qw/
                            vestiging_adres_buitenland1
                            vestiging_adres_buitenland2
                            vestiging_adres_buitenland3
                        /;
                    }

                    for my $dbkey (keys %{ $params }) {
                        $gmbetrokkene->$dbkey($params->{$dbkey})
                            if $gmbetrokkene->can($dbkey);
                    }
                } else { # natuurlijk_persoon or medewerker
                    my %clean_params = map(
                        {
                            my $key = $_;
                            $key =~ s/np-//;
                            $key => $params->{$_}
                        }
                        keys %$params
                    );

                    if (
                        $clean_params{landcode} &&
                        $clean_params{landcode} != '6030'
                    ) {
                        $clean_params{$_} = undef for qw/
                            postcode
                            adres
                            straatnaam
                            huisnummer
                            huisletter
                            huisnummertoevoeging
                            woonplaats

                            correspondentie_postcode
                            correspondentie_adres
                            correspondentie_straatnaam
                            correspondentie_huisnummer
                            correspondentie_huisletter
                            correspondentie_huisnummertoevoeging
                            correspondentie_woonplaats
                        /;
                    } else {
                        $clean_params{$_} = undef for qw/
                            adres_buitenland1
                            adres_buitenland2
                            adres_buitenland3
                        /;
                    }

                    ### First, add correspondentieadres when requested
                    $c->model('DB')->txn_do(sub {
                        if ($clean_params{landcode} eq '6030' && $params->{briefadres}) {
                            my %correspondentie_params = map {
                                my $dbkey = $_;
                                $dbkey =~ s/^correspondentie_//;

                                ($dbkey => $clean_params{$_});
                            } grep {
                                /^correspondentie_/
                            } keys %clean_params;

                            $gmbetrokkene->add_address(
                                {
                                    functie_adres   => 'B',
                                    %correspondentie_params
                                }
                            );
                        } elsif ($gmbetrokkene->correspondentieadres) {
                            $gmbetrokkene->delete_address_by_function('B');
                        }

                        ### Delete verblijfsaddress when straatnaam not set
                        if ($clean_params{straatnaam} || $clean_params{adres_buitenland1}) {
                            $gmbetrokkene->add_address(
                                {
                                    functie_adres   => 'W',
                                    %clean_params
                                }
                            );
                        } elsif (!$clean_params{straatnaam} && ($clean_params{landcode} ne '6030' || $params->{briefadres})) {
                            $gmbetrokkene->delete_address_by_function('W');
                        }

                        # Only update actual columns in the natuurlijk_persoon table, skip the
                        # (proxied) "adres" ones, or they'll overwrite some of the work we did above.
                        for my $dbkey ($gmbetrokkene->gm_np->columns()) {
                            if (   exists($clean_params{$dbkey})
                                && $gmbetrokkene->can($dbkey)
                            ) {
                                $gmbetrokkene->$dbkey($clean_params{$dbkey});
                            }
                        }
                    });
                }
            }
        }

        # Update contactgegevens
        if ($c->zvalidate) {
            for my $param (keys %{ $params }) {
                my ($prefix, $field) = $param =~ m[^(npc)\-(.*)$];

                next unless defined $prefix and $prefix eq 'npc';

                $contact_data->set_column($field, $params->{ $param } || undef);
            }

            $contact_data->exec_and_log_update(sub {
                return $c->model('DB::Logging')->trigger(@_);
            });
        }

        if ($c->stash->{zaak}) {
            $c->res->redirect($c->uri_for('/zaak/' . $c->stash->{zaak}->nr));
        } else {
            # Remove edit on post
            my $uri             = URI->new($c->req->referer);
            my %query_params    = $uri->query_form;
            delete($query_params{edit});

            $c->res->redirect($c->uri_for($uri->path, \%query_params));
        }
    }
}

{
    sub verwijder : Chained('base'): PathPart('verwijder'): Args(2) {
        my ($self, $c, $betrokkene_type, $gmid) = @_;

        return unless $c->check_any_user_permission(qw/contact_nieuw contact_search/);

        return unless $gmid;

        my $gmbetrokkene = $c->model('Betrokkene')->get(
            {
                type    => $betrokkene_type,
                intern  => 0,
            },
            $gmid
        );

        my $params = $c->req->params();

        # Update niet authentieke gegevens
        if (%$params && $params->{confirmed}) {
            $c->response->redirect(
                $c->uri_for(
                    '/betrokkene/search'
                )
            );

            do {
                $c->push_flash_message('Deze betrokkene kan niet'
                    . ' worden verwijderd');
                $c->detach;
            } unless $gmbetrokkene->can_verwijderen;

            if ($gmbetrokkene->verwijder) {
                my $event = $c->model('DB::Logging')->trigger('subject/remove', {
                    component => LOGGING_COMPONENT_BETROKKENE,
                    component_id => $gmid,
                    data => {
                        subject_name => $gmbetrokkene->naam,
                        subject_id => $gmid
                    }
                });

                $c->push_flash_message($event->onderwerp);
            }
        }

        $c->stash->{confirmation}->{message}    =
            'Weet u zeker dat u betrokkene "'
            . $gmbetrokkene->naam . '" wilt verwijderen?';

        $c->stash->{confirmation}->{type}       = 'yesno';
        $c->stash->{confirmation}->{uri}        =
            $c->uri_for(
                '/betrokkene/verwijder/' . $betrokkene_type . '/' . $gmid
            );


        $c->forward('/page/confirmation');
        $c->detach;
    }
}

sub betrokkene : Chained('/') : PathPart('betrokkene'): CaptureArgs(1) {
    my ($self, $c, $betrokkene_identifier) = @_;

    if ($betrokkene_identifier) {
        my ($betrokkene_type, $betrokkene_id)
            = $betrokkene_identifier =~ /^betrokkene-(.*?)-(\d+)$/;

        unless ($betrokkene_type && $betrokkene_id) {
            $c->res->redirect($c->uri_for('/'));
            $c->detach;
        }

        $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
            {
                'type'      => $betrokkene_type,
                'intern'    => 0,
            },
            $betrokkene_id
        );
    }
}

my $rechtsvormen = [];
{
    my $kvkrechtsvormen_enabled = ZAAKSYSTEEM_CONSTANTS
                                    ->{kvk_rechtsvormen_enabled};

    for my $code (@{ $kvkrechtsvormen_enabled }) {
        if (ZAAKSYSTEEM_CONSTANTS->{kvk_rechtsvormen}->{ $code }) {
            push(@{ $rechtsvormen },
                {
                    value   => $code,
                    label   => ZAAKSYSTEEM_CONSTANTS
                        ->{kvk_rechtsvormen}
                        ->{ $code }
                }
            );
        }
    }
}

my $BETROKKENE_TEMPLATE = {
    natuurlijk_persoon  => [
        {
            label   => 'BSN',
            name    => 'np-burgerservicenummer',
            classes => ['input_large'],
        },
        {
            label   => 'Voornamen',
            name    => 'np-voornamen',
            classes => ['input_large'],
        },
        {
            label   => 'Tussenvoegsel',
            name    => 'np-voorvoegsel',
            classes => ['input_mini'],
        },
        {
            label   => 'Achternaam',
            name    => 'np-geslachtsnaam',
            classes => ['input_large'],
        },
        {
            label   => 'Adellijke titel',
            name    => 'np-adellijke_titel',
            classes => ['input_large'],
        },
        {
            label       => 'Geslacht',
            name        => 'np-geslachtsaanduiding',
            type        => 'radio',
            options     => [
                {
                    label   => 'Man',
                    name    => 'np-geslachtsaanduiding',
                    value   => 'M',
                },
                {
                    label   => 'Vrouw',
                    name    => 'np-geslachtsaanduiding',
                    value   => 'V',
                }
            ],
        },
        {
            label   => 'Land',
            name    => 'np-landcode',
            type    => 'select',
            options => $landcodes,
            selected => 6030,
            classes => [qw/
                zsaction
                zsaction-when-6030-show-binnenland
                zsaction-when-6030-hide-buitenland
                zsaction-whennot-6030-show-buitenland
                zsaction-whennot-6030-hide-binnenland
            /],
        },
        {
            label   => 'Adresregel 1',
            name    => 'np-adres_buitenland1',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-buitenland'],
        },
        {
            label   => 'Adresregel 2',
            name    => 'np-adres_buitenland2',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-buitenland'],
        },
        {
            label   => 'Adresregel 3',
            name    => 'np-adres_buitenland3',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-buitenland'],
        },
        {
            label   => 'Briefadres',
            name    => 'briefadres',
            type    => 'select',
            options => [{ label => 'Ja', value => '1'}, { label => 'Nee', value => '0'}], 
            selected => 'nee',
            rowclasses => [qw/
                zsaction-dest-binnenland
            /],
            classes => [qw/
                zsaction
                zsaction-when-ja-show-correspondentieadres
                zsaction-whennot-ja-hide-correspondentieadres
            /],
        },
        {
            label   => 'Straat',
            name    => 'np-straatnaam',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-binnenland','zsaction-dest-verblijfsadres'],
        },
        {
            label   => 'Huisnummer',
            name    => 'np-huisnummer',
            classes => ['input_mini'],
            rowclasses => ['zsaction-dest-binnenland','zsaction-dest-verblijfsadres'],
        },
        {
            label   => 'Huisnummer toevoeging',
            name    => 'np-huisnummertoevoeging',
            classes => ['input_mini'],
            rowclasses => ['zsaction-dest-binnenland','zsaction-dest-verblijfsadres'],
        },
        {
            label       => 'Postcode',
            name        => 'np-postcode',
            classes     => ['input_medium'],
            post_label  => '1234AZ',
            rowclasses => ['zsaction-dest-binnenland','zsaction-dest-verblijfsadres'],
        },
        {
            label   => 'Woonplaats',
            name    => 'np-woonplaats',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-binnenland','zsaction-dest-verblijfsadres'],
        },
        {
            label   => 'Correspondentie straat',
            name    => 'np-correspondentie_straatnaam',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-binnenland','zsaction-dest-correspondentieadres'],
        },
        {
            label   => 'Correspondentie huisnummer',
            name    => 'np-correspondentie_huisnummer',
            classes => ['input_mini'],
            rowclasses => ['zsaction-dest-binnenland','zsaction-dest-correspondentieadres'],
        },
        {
            label   => 'Correspondentie huisnummer toevoeging',
            name    => 'np-correspondentie_huisnummertoevoeging',
            classes => ['input_mini'],
            rowclasses => ['zsaction-dest-binnenland','zsaction-dest-correspondentieadres'],
        },
        {
            label       => 'Correspondentie postcode',
            name        => 'np-correspondentie_postcode',
            classes     => ['input_medium'],
            post_label  => '1234AZ',
            rowclasses => ['zsaction-dest-binnenland','zsaction-dest-correspondentieadres'],
        },
        {
            label   => 'Correspondentie woonplaats',
            name    => 'np-correspondentie_woonplaats',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-binnenland','zsaction-dest-correspondentieadres'],
        },
        {
            label   => 'Binnengemeentelijk',
            name    => 'np-in_gemeente',
            type    => 'checkbox',
            rowclasses => ['zsaction-dest-binnenland'],
        },
    ],
    bedrijf     => [
        {
            label   => 'Vestiging land',
            name    => 'vestiging_landcode',
            type    => 'select',
            options => $landcodes,
            selected => 6030,
            classes => [qw/
                zsaction
                zsaction-when-6030-show-binnenland
                zsaction-when-6030-hide-buitenland
                zsaction-whennot-6030-show-buitenland
                zsaction-whennot-6030-hide-binnenland
            /],
        },
        {
            label   => 'Rechtsvorm',
            name    => 'rechtsvorm',
            type    => 'select',
            options => $rechtsvormen,
        },
        {
            label   => 'KVK-nummer',
            name    => 'dossiernummer',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-binnenland'],
        },
        {
            label   => 'Vestigingsnummer',
            name    => 'vestigingsnummer',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-binnenland'],
        },
        {
            label   => 'Handelsnaam',
            name    => 'handelsnaam',
            classes => ['input_large'],
        },
        {
            label   => 'Adresregel 1',
            name    => 'vestiging_adres_buitenland1',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-buitenland'],

        },
        {
            label   => 'Adresregel 2',
            name    => 'vestiging_adres_buitenland2',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-buitenland'],

        },
        {
            label   => 'Adresregel 3',
            name    => 'vestiging_adres_buitenland3',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-buitenland'],

        },
        {
            label   => 'Vestiging straat',
            name    => 'vestiging_straatnaam',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-binnenland'],

        },
        {
            label   => 'Vestiging huisnummer',
            name    => 'vestiging_huisnummer',
            classes => ['input_mini'],
            rowclasses => ['zsaction-dest-binnenland'],

        },
        {
            label   => 'Vestiging huisletter',
            name    => 'vestiging_huisletter',
            classes => ['input_mini'],
            rowclasses => ['zsaction-dest-binnenland'],

        },
        {
            label   => 'Vestiging toevoeging',
            name    => 'vestiging_huisnummertoevoeging',
            classes => ['input_mini'],
            rowclasses => ['zsaction-dest-binnenland'],

        },
        {
            label   => 'Vestiging postcode',
            name    => 'vestiging_postcode',
            classes => ['input_medium'],
            rowclasses => ['zsaction-dest-binnenland'],

        },
        {
            label   => 'Vestiging woonplaats',
            name    => 'vestiging_woonplaats',
            classes => ['input_large'],
            rowclasses => ['zsaction-dest-binnenland'],

        },
    ],
};

sub bewerken : Chained('betrokkene') : PathPart('bewerken'): Args(0) {
    my ($self, $c) = @_;

    if ($c->req->method ne 'POST') {
        $c->log->warn(sprintf(
            "/betrokkene/X/bewerken requested with non-POST method '%s'",
            $c->req->method,
        ));
    }

    $c->stash->{betrokkene_type}    = (
        $c->req->params->{betrokkene_type}
        || 'natuurlijk_persoon'
    );

    $c->stash->{betrokkene_type}    = 'natuurlijk_persoon'
        unless defined($BETROKKENE_TEMPLATE->{
                $c->stash->{betrokkene_type}
        });

    $c->stash->{betrokkene_template} = $BETROKKENE_TEMPLATE->{
        $c->stash->{betrokkene_type}
    };

    my $profile;
    if ($c->stash->{betrokkene_type} eq 'bedrijf') {
        $profile                    = $c->forward(
            '/betrokkene/bedrijf/bedrijven_profile'
        );
    } else {
        $profile                    = Zaaksysteem->get_profile(
            method  => __PACKAGE__ . '::create'
        );
    }

    # Don't mess around in the original profile - it's also used in other places.
    $profile = _add_profile_checks(
        context         => $c,
        betrokkene_type => $c->stash->{betrokkene_type},
        profile         => clone($profile),
    );

    if (
        my $dv = $c->forward('/page/dialog', [{
            validatie           => $profile,
            user_permissions    => [qw/contact_nieuw/],
            template            => 'widgets/betrokkene/bewerken.tt',
        }])
    ) {
        my $params  = $dv->valid;

        if (
            my $id = $c->forward(
                '_create_betrokkene',
                [
                    $c->stash->{betrokkene_type},
                    $params
                ],
            )
        ) {
            my $betrokkene = $c->model('Betrokkene')->get(
                {},
                'betrokkene-' . $c->stash->{betrokkene_type} . '-' . $id
            );

            # Add logging
            my $logging_description = join ", ", map { $_ . ': ' . $params->{$_} } sort keys %$params;

            $c->model('DB::Logging')->trigger('subject/create', {
                component => LOGGING_COMPONENT_BETROKKENE,
                component_id => $id,
                data => {
                    subject_id => $betrokkene->betrokkene_identifier,
                    parameters => $params
                }
            });

            # TODO what the actual fuck guys...
            $c->stash->{json} = {
                'succes'    => 1,
                'bericht'   => 'Betrokkene aangemaakt: '
                    .'<a href="' . $c->uri_for(
                        '/betrokkene/' . $id,
                        {
                            gm      => 1,
                            type    => $c->stash->{betrokkene_type}
                        }
                    ) . '">' .  $betrokkene->display_name . '</a>'
                    . ' (<a href="' . $c->uri_for(
                        '/zaak/create',
                        {
                            aanvraag_trigger    => 'extern',
                            betrokkene_naam     => $betrokkene->display_name,
                            betrokkene_id       =>
                                $betrokkene->betrokkene_identifier,
                            betrokkene_type     => $betrokkene->btype,
                        },
                    ) . '" class="ezra_nieuwe_zaak_tooltip-show">'
                    . 'Zaak aanmaken</a>)'
            }
        } else {
            $c->stash->{json} = {
                'succes'    => 0,
                'bericht'   => 'Fout bij aanmaken betrokkene',
            }
        }

        $c->forward('Zaaksysteem::View::JSONlegacy');
    }
}

=head2 burgerservicenummer

This will return the BSN as plain text, or '-' if it's unknown.

=head3 ULR path

C</betrokkene/[betrokkene-id]/burgerservicenummer>

=cut

sub burgerservicenummer : Chained('betrokkene') : PathPart('burgerservicenummer') : Args(0) : RO {
    my ($self, $c) = @_;
    $c->view(undef); # we return plain text here, at all times

    unless ($c->check_user_role('persoonsverwerker')) {
        $c->res->code(403);
        $c->res->body('onvoldoende rechten om het Burgerservicenummer in te zien');
        $c->detach
    }

    my $betrokkene = $c->stash->{betrokkene};
    # Chained('betrokkene') doesn't catch ;'NOT FOUND' as probably expected,
    # but dare not fix that, for `bewerken` is chained to it and creates ... ?
    unless ($betrokkene) {
        $c->res->code(404);
        $c->res->body('contact kan niet worden gevonden');
        $c->detach
    }

    unless ($betrokkene->type eq 'natuurlijk_persoon') {
        $c->res->code(400);
        $c->res->body('contact is geen natuurlijke persoon');
        $c->detach
    }

    my $bsn = $betrokkene->bsn;
    unless ($bsn) {
        $c->res->code(200);
        $c->res->body('-');
        $c->detach
    }

    $c->model('DB::Logging')->trigger(
        'subject/inspect' => {
            component => 'betrokkene',
            component_id => $betrokkene->id,
            created_for => $betrokkene->betrokkene_identifier,
            data => {
                _betrokkene_identifier => $betrokkene->betrokkene_identifier,
                name => $betrokkene->display_name,
                field => "bsn",
            }
        }
    );

    $c->res->body($bsn);
}

sub _create_betrokkene : Private {
    my ($self, $c, $betrokkene_type, $opts)  = @_;

    $opts->{'np-postcode'} = uc($opts->{'np-postcode'})
        if defined($opts->{'np-postcode'});

    my $id = $c->model('Betrokkene')->create(
        $betrokkene_type,
        {
            %{ $opts },
            authenticatedby =>
                ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR,
        }
    );

    return $id;
}

=head2 _add_profile_checks

Alters a L<Data::FormValidator> profile for a betrokkene to make sure no
duplicate entries can be created in the database (poor man's UNIQUE check - we
can't add the constraint in the database)

=cut

sub _add_profile_checks {
    my %args = @_;

    # Fix up profile -- "natuurlijk_persoon" & "bedrijf" cases -> db check
    my %invalid_reason;
    if ($args{betrokkene_type} eq 'natuurlijk_persoon') {
        $args{profile}->{constraint_methods}{"np-burgerservicenummer"} = sub {
            my $self = shift;
            my ($bsn) = @_;

            return 0 unless BSN->check($bsn);

            my $existing = $args{context}->model('DB::NatuurlijkPersoon')->search(
                {
                    -and => [
                        { 'me.deleted_on'                   => undef },
                        { 'NULLIF(me.burgerservicenummer,\'\')::integer' => int($bsn) },

                        exists $args{contact_id}
                            ? { 'me.id' => { '!=' => $args{contact_id} } }
                            : ()
                    ],
                },
            )->count;

            return 1 if ($existing == 0);

            $invalid_reason{"np-burgerservicenummer"} = "Persoon met dit burgerservicenummer reeds geregistreerd";
            return 0;
        };
    }
    elsif ($args{betrokkene_type} eq 'bedrijf') {
        my %bedrijf_search = (deleted_on => undef);

        if (exists $args{contact_id}) {
            $bedrijf_search{id} = { '!=' => $args{contact_id} };
        }

        my $handler = sub {
            my $key       = shift;
            my $old_check = shift;

            return sub {
                my $dfv = shift;
                my ($value) = @_;

                return 0 if ($value !~ $old_check);

                my %additional_search;
                if ($dfv->get_filtered_data->{vestigingsnummer}) {
                    %additional_search = (
                        "NULLIF(dossiernummer,'')::bigint"  => $dfv->get_filtered_data->{dossiernummer},
                        "vestigingsnummer"                  => $dfv->get_filtered_data->{vestigingsnummer}
                    )
                } else {
                    %additional_search = (
                        "NULLIF(dossiernummer,'')::bigint" => $dfv->get_filtered_data->{dossiernummer},
                        vestigingsnummer => undef
                    );
                }

                ### Possible memory leak...
                my $existing = $args{context}->model('DB::Bedrijf')->search(
                    {
                        %bedrijf_search,
                        %additional_search
                    }
                )->count;

                return 1 if ($existing == 0);

                $invalid_reason{dossiernummer}      = "Organisatie met dit KvK-nummer en vestigingsnummer reeds geregistreerd";

                return 0;
            };
        };

        $args{profile}->{constraint_methods}{"dossiernummer"} = $handler->(
            "dossiernummer",
            $args{profile}->{constraint_methods}{"dossiernummer"},
        );
    }

    $args{profile}->{msgs} = sub {
        my $self = shift;

        $self->{msgs} ||= {};

        my %msgs;
        if ($self->has_invalid) {
            for my $i (keys %{ $self->invalid }) {
                if (exists $invalid_reason{$i}) {
                    $msgs{$i} = $invalid_reason{$i};
                }
                else {
                    $msgs{$i} = "Veld is niet correct ingevuld";
                }
            }
        }
        if ($self->has_missing) {
            for my $i (@{ $self->missing }) {
                $msgs{$i} = "Veld is verplicht";
            }
        }

        return \%msgs;
    };

    return $args{profile};
}

sub _np_search_validator {
    return {
        required => [
            'betrokkene_type',
            'search',
        ],
        optional => [
            'np-burgerservicenummer',
            'np-postcode',
            'np-huisnummer',
            'np-geboortedatum',
            'np-voorvoegsel',
            'np-geslachtsnaam',
        ],
        require_some => {
            # normaly, you would create an entry like `foo_or_bar_or_tux` and
            # that would cause the name of the `require_some` key to show up in
            # the `missing` list.
            # by adding the restraint three times, we get all three keys in the
            # list off missing fields ... and thus mark all three fields missing
            #
            # other feedback scenarios have been worked on but failed because
            # off `$c->register_profile`
            'np-burgerservicenummer' => [ 1 =>
                'np-burgerservicenummer',
                'np-postcode',
                'np-geboortedatum',
            ],
            'np-postcode' => [ 1 =>
                'np-burgerservicenummer',
                'np-postcode',
                'np-geboortedatum',
            ],
            'np-geboortedatum' => [ 1 =>
                'np-burgerservicenummer',
                'np-postcode',
                'np-geboortedatum',
            ],
        },
        dependency_groups => {
            postcode__en__huisnummer => [
                'np-postcode',
                'np-huisnummer',
            ],
            geboortedatum__en__achternaam => [
                'np-geboortedatum',
                'np-geslachtsnaam',
            ],
        },
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 LOGGING_COMPONENT_BETROKKENE

TODO: Fix the POD

=cut

=head2 RGBZ_LANDCODES

TODO: Fix the POD

=cut

=head2 VALIDATION_CONTACT_DATA

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR

TODO: Fix the POD

=cut

=head2 base

TODO: Fix the POD

=cut

=head2 betrokkene

TODO: Fix the POD

=cut

=head2 bewerken

TODO: Fix the POD

=cut

=head2 create

TODO: Fix the POD

=cut

=head2 disable_betrokkene_session

TODO: Fix the POD

=cut

=head2 enable_betrokkene_session

TODO: Fix the POD

=cut

=head2 external_import

TODO: Fix the POD

=cut

=head2 get

TODO: Fix the POD

=cut

=head2 handle_betrokkene_session

TODO: Fix the POD

=cut

=head2 include_woz_tab

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 search

TODO: Fix the POD

=cut

=head2 snapshot

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

=head2 verwijder

TODO: Fix the POD

=cut

=head2 view

TODO: Fix the POD

=cut

=head2 view_base

TODO: Fix the POD

=cut

=head2 woz_object

TODO: Fix the POD

=cut

