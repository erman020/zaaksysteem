package Zaaksysteem::Backend::Object::Data::Roles::Type;

use Moose::Role;
use namespace::autoclean;

use BTTW::Tools;
use List::Util qw[uniq];

=head1 NAME

Zaaksysteem::Backend::Object::Data::Roles::Type - Implement behavior specific
to L<Object|Zaaksysteem::Backend::Object::Data::Component>s.

=head1 DESCRIPTION

This role implements some additional logic specific to the 'type' Object. It's
mostly a very thin shim, nothing to see here.

=head1 METHODS

=head2 bibliotheek_entry_args

Constructs a HashRef containing the key-value pairs to be injected in the
L<BibliotheekEntry|Zaaksysteem::Backend::Object::BibliotheekEntry::ResultSet>
resultset.

    $bibliotheek_entry->update(
        $object_data_type_row->bibliotheek_entry_args
    );

=cut

sub bibliotheek_entry_args {
    my $self = shift;

    my $attr = $self->get_object_attribute('category_id');

    return unless $attr->has_value;

    return {
        bibliotheek_categorie_id => $attr->value,
        search_term => $self->get_object_attribute('name')->human_value,
        object_type => $self->object_class,
        object_uuid => $self->uuid
    };
}

=head2 reload_index

This method hooks into the save/update infrastructure of type objects
and makes sure the defined C<instance_authorizations> are in place in the
ACL tables as a 'type'-scoped rule.

=cut

after reload_index => sub {
    my $self = shift;

    $self->object_acl_entries->delete_all;

    my $acls = $self->object_acl_entries;

    my $instance_auths = $self->get_object_attribute('instance_authorizations');

    unless($instance_auths->has_value && ref $instance_auths->value eq 'ARRAY') {
        return;
    }

    my %map = (
        zaak_beheer => [qw(manage write read search)],
        zaak_edit   => [qw(       write read search)],
        zaak_read   => [qw(             read search)],
    );

    for my $acl (@{ $instance_auths->value }) {
        # Make a copy, so we don't inadvertently mess with the original value.
        my %acl_rules = %$acl;
        my $ou = delete $acl_rules{ org_unit_id };
        my $role = delete $acl_rules{ role_id };

        my @capabilities = uniq map { @{ $map{ $_ } } } grep { exists $map{ $_ } } keys %acl_rules;

        my $entity = {};

        if ($ou && $role) {
            $entity->{ position } = sprintf('%s|%s', $ou, $role);
        } elsif ($ou) {
            $entity->{ group } = $ou;
        } elsif ($role) {
            $entity->{ role } = $role;
        } else {
            throw('object/type/acl', sprintf(
                'Unable to determine security entity for ou=%s,role=%s',
                $ou,
                $role
            ));
        }

        $self->grant($entity,
            scope => 'type',
            capabilities => \@capabilities
        );
    }

    $self->grant({ role => '20002' }, capabilities => ['read']);
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

