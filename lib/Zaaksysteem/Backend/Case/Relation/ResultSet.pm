package Zaaksysteem::Backend::Case::Relation::ResultSet;

use Moose;

BEGIN { extends 'DBIx::Class::ResultSet'; }

use BTTW::Tools;
use Zaaksysteem::Zaken::RelationView;

=head2 get

Get a single relationship between two cases

=cut

sub get {
    my $self = shift;
    my $case_id_a = shift;
    my $case_id_b = shift;

    return $self->search({ -and => [
        { -or => { case_id_a => $case_id_a, case_id_b => $case_id_a } },
        { -or => { case_id_a => $case_id_b, case_id_b => $case_id_b } }
    ]})->first;
}

=head2 get_all

Get all relations for a known case_id

=cut

sub get_all {
    my $self = shift;
    my $case_id = shift;

    return map
        { Zaaksysteem::Zaken::RelationView->new($_, $case_id) }
        $self->search({ case_id => $case_id });
}

# Play intercept with the usual way of accessing relation objects
# This is so $rs->search({ case_id => 666 }) will return what the
# called intends, not what the caller says (see what I did there? :p)
around search => sub {
    my $orig = shift;
    my $self = shift;
    my $search = shift;

    # We've got a live one!
    if(exists $search->{ case_id }) {
        my $case_id = delete $search->{ case_id };

        $search->{ -or } = {
            case_id_a => $case_id,
            case_id_b => $case_id
        };
    }

    $self->$orig($search, @_);
};

sub get_sorted {
    my $self = shift;
    my $case_id = shift;

    return sort { $a->order_seq <=> $b->order_seq } $self->get_all($case_id);
}

sub recalculate_order {
    my $self = shift;
    my $case_id = shift;

    my $iter = 1;

    for my $rel ($self->get_sorted($case_id)) {
        $rel->order_seq($iter);
        $rel->update;

        $iter++;
    }
}

sub add_relation {
    my $self = shift;
    my $case_id = shift;
    my $related_case_id = shift;

    # Pretend we created a relation, return the relation
    my $rel = $self->get($case_id, $related_case_id);

    throw(
        'case_relation/duplicate_add',
        'Relation ' . $rel->to_string . ' already exists'
    ) if defined $rel;

    # In order to calculate the correct sequence numbers
    # we'll need both relation listings for left and right
    my @left_rels = $self->get_sorted($case_id);
    my @right_rels = $self->get_sorted($related_case_id);

    my $relation = $self->create({
        case_id_a => $case_id,
        case_id_b => $related_case_id,
        order_seq_a => scalar(@right_rels) + 1,
        order_seq_b => scalar(@left_rels) + 1,
        type_a => 'plain',
        type_b => 'plain'
    });

    my $cases = $self->result_source->schema->resultset('Zaak')->search({
        'me.id' => { -in => [ $case_id, $related_case_id ] }
    });

    for my $case ($cases->all) {
        $case->logging->trigger('case/update/relation', { component => 'zaak', data => {
            case_id => $case_id eq $case->id ? $case_id : $related_case_id,
            relation_id => $case_id ne $case->id ? $case_id : $related_case_id
        }});
    }

    return $relation;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 add_relation

TODO: Fix the POD

=cut

=head2 get_sorted

TODO: Fix the POD

=cut

=head2 recalculate_order

TODO: Fix the POD

=cut

