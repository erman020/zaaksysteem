package Zaaksysteem::Backend::Roles::Component;

use Moose;

use BTTW::Tools;
use Zaaksysteem::Constants qw[ZAAKSYSTEEM_AUTHORIZATION_ROLES];

use Zaaksysteem::Object::Types::Role;

use List::Util qw[first];

extends 'DBIx::Class';

=head1 NAME

Zaaksysteem::Backend::Roles::Component - Implement specific behaviors for
Roles rows.

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 parent_id

The parent_id of this role.

=cut

has parent_id => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        return shift->parent_group_id;
    }
);

=head2 object

Holds a reference to a L<Zaaksysteem::Object::Types::Role> instance, which
encodes this row in the L<Zaaksysteem::Object> style.

=cut

has object => (
    is => 'ro',
    isa => 'Zaaksysteem::Object::Types::Role',
    lazy => 1,
    builder => 'as_object'
);

=head2 parent_groups

Holds a list of parent groups for this role

=cut

has parent_groups => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        my $self            = shift;

        my $parent_group    = $self->parent_group_id;

        ### Query all parent groups
        return [ $self->result_source->schema->resultset('Groups')->search(
            {
                id      => { in     => $parent_group->path }
            },
            {
                order_by    => { '-asc' => [\"array_length(me.path, 1)"] },
            }
        ) ];
    }
);

=head2 subjects

Arguments: none

    my $subjects_rs = $groups->subjects;

Will retrieve subjects who have this role.

=cut

has subjects => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        my $self        = shift;

        $self->result_source->schema->resultset('Subject')->search(
            {
                $self->id => \'= ANY (role_ids)',
            },
        );
    }
);

=head1 METHODS

=head2 update_role

Argumants: \%params

Return value: $UPDATED_ROW

    $newrole   = $newrole->update_role(
        {
            name            => 'Bestandelaar',
            description     => 'Bestandelaar',
        }
    );

=cut

define_profile 'update_role' => (
    required            => [],
    optional            => [qw/name description change_systemrole/],
    constraint_methods  => {
        name                => qr/^[\w\s_-]+$/,
        description         => qr/^[\w\s_-]+$/,
    }
);

sub update_role {
    my $self            = shift;
    my $params          = assert_profile(shift || {})->valid;

    throw(
        'roles/update_role/cannot_change_system_role',
        'Not authorized to alter systemrole, unless change_systemrole=1'
    ) unless ($params->{change_systemrole} || !$self->system_role);

    for my $key (qw/name description/) {
        $self->$key($params->{$key});
    }

    $self->update;
    return $self->discard_changes();
}

=head2 implies_legacy_permission

This method takes an old-style global permission name (admin, gebruiker,
dashboard, etc), and checks if the role implies that permission. Returns the
assertion as a boolean value.

    my $role = ...;

    if ($role->implies_legacy_permission('admin')) {
        do_authorized_stuff();
    } else {
        warn "that's naughty!";
    }

=cut

sig implies_legacy_permission => 'Str';

sub implies_legacy_permission {
    my $self = shift;
    my $permission = shift;

    my $authorizations = first {
        $_->{ ldapname } eq $self->name
    } values %{ ZAAKSYSTEEM_AUTHORIZATION_ROLES() };

    return unless $authorizations;

    return $authorizations->{ rechten }{ global }{ $permission };
}

=head2 security_identity

Implements a duck-typed L<Zaaksysteem::Object::SecurityIdentity> for the role
instance. Returns a hashable list C<role, $id>.

=cut

sub security_identity {
    return (role => shift->id);
}

=head2 set_deleted

Arguments: none

    $role->set_deleted;

    ### Not needed:
    # $role->update

Only way to proper delete a role

=cut

sub set_deleted {
    my $self        = shift;

    ### Loop over subjects to remove role_ids
    $self->result_source->schema->txn_do(
        sub {
            for my $subject ($self->subjects->all) {
                next unless grep({ $_ == $self->id } @{ $subject->role_ids });

                my @role_ids = grep({ $_ != $self->id } @{ $subject->role_ids });
                $subject->role_ids(\@role_ids);
                $subject->update;
            }

            $self->delete;
        }
    );
}

=head2 as_object

Returns the DB object as a Zaaksysteem object

=cut

sub as_object {
    my $self = shift;

    return Zaaksysteem::Object::Types::Role->new(
        role_id     => $self->id,
        name        => $self->name,
        description => $self->description // '',
        system_role => $self->system_role ? 1 : 0,
        id          => $self->uuid,
    );
}


=head2 TO_JSON

Implements the automagic serialization to JSON via L<JSON/encode>.

=cut

sub TO_JSON {
    my $self    = shift;

    my $parent_id = $self->get_column('parent_group_id');

    return {
        $self->get_columns,
        table     => 'Roles',
        $parent_id ? (parent_id => $parent_id) : ()
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

