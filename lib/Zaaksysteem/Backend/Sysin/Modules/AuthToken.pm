package Zaaksysteem::Backend::Sysin::Modules::AuthToken;
use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

extends 'Zaaksysteem::Backend::Sysin::Modules';

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::AuthToken - Token-based authentication

=head1 DESCRIPTION

This interface module calls itself C<authtoken>, and handles authentication using a
generated token.

=cut

my $INTERFACE_ID = 'authtoken';
my @INTERFACE_CONFIG_FIELDS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_session_invitation_version',
        type        => 'select',
        required    => 1,
        default     => 'v2',
        label       => 'Versie authenticatie',
        description => q{
                Versie 1: Geschikt voor alle versies van de Document Watcher*<br>
                Versie 2: wordt geadviseerd voor Document Watcher versie 1.6 of hoger.<br>
                <br>
                * Versie 1 wordt vanaf 2019 niet meer ondersteund
        },
        data => {
            options => [
                { label => 'Versie 2', value => 'v2' },
                { label => 'Versie 1', value => 'v1' },
            ]
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_session_invitation_version_deprecated',
        type        => 'display',
        required    => 0,
        label       => 'Let op:',
        data => {
            template => qq{
                Ondersteuning voor versie 1 van sessiemachtigingen is verouderd en wordt in 2019 uitgefaseerd.<br>
            }
        },
        when => 'interface_session_invitation_version == "v1"',
    ),
);

my $INTERFACE_DESCRIPTION = qq{
<p>Het Zaaksysteem kent koppelingen waarbij het authenticatieproces automatisch
wordt geregeld in het berichtenverkeer. Dit gebeurt via een gegenereerde
inlogtoken met een beperkte geldigheidsduur. Momenteel geldt dit alleen voor de
koppeling Document watcher.</p>
<p>Via dit koppelvlak wordt dit authenticatieproces geactiveerd.</p>
};

my $MODULE_SETTINGS = {
    name                          => $INTERFACE_ID,
    label                         => 'Authenticatie voor externe applicaties',
    description                   => $INTERFACE_DESCRIPTION,
    interface_config              => \@INTERFACE_CONFIG_FIELDS,
    direction                     => 'outgoing',
    manual_type                   => ['text'],
    essential                     => 1,
    is_multiple                   => 0,
    is_manual                     => 0,
    retry_on_error                => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    test_interface                => 0,
    trigger_definition            => {},
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    $class->$orig(%{$MODULE_SETTINGS});
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
