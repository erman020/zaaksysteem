package Zaaksysteem::Backend::Sysin::Modules::OpenCage;

use Moose;
use namespace::autoclean;

use BTTW::Tools;

use LWP::UserAgent;
use HTTP::Request;
use URI;
use JSON;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw[
    Zaaksysteem::Geo::Provider
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
];

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::OpenCage - OpenCage.com integration

=head1 DESCRIPTION

This integration handles interactions with OpenCage compliant services,
which is used for (reverse) geo location queries.

=head1 CONSTANTS

=head2 INTERFACE_ID

Hardcoded to C<opencage>. Instances of this module can be found by this slug.

=cut

use constant INTERFACE_ID => 'opencage';

=head2 INTERFACE_CONFIG_FIELDS

Contains a list of L<Zaaksysteem::ZAPI::Form::Field> instances which represent
the module's configuration UI.

=cut

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_opencage_url',
        type        => 'text',
        label       => 'OpenCage Service URL',
        default     => 'http://api.opencagedata.com/geocode/v1/json',
        data        => {
            placeholder => 'http://api.opencagedata.com/geocode/v1/json'
        },
        description => <<'EOD'
Geef hier de URL van de OpenCage service op. De standaard waarde voor dit
veld wijst naar de algemene API van OpenCage en hoeft niet aangepast te
worden.
EOD
    ),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_opencage_use_platform_key',
        type        => 'checkbox',
        label       => 'Gebruik de Zaaksysteem OpenCage API Licentie',
        default     => 1,
        description => <<'EOD'
Zaaksysteem.nl bied aan alle gebruikers en omgevingen een standaard API
sleutel aan, welke opgenomen is in het Zaaksysteem platform. Indien uw
omgeving meer OpenCage API requests per dag doet dan de Zaaksysteem licentie
toestaat kunt u dit veld aanvinken, waarna er een eigen sleutel opgegeven kan
worden.
EOD
    ),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_opencage_api_key',
        type        => 'text',
        label       => 'OpenCage API sleutel',
        required    => 1,
        when        => '(!interface_opencage_use_platform_key)',
        data        => {
            placeholder => '6860f38ae2b0e9833076a9cbf57f55e0'
        },
        description => <<'EOD'
Geef hier de sleutel (API key) op van uw OpenCage licentie (te vinden op het
<a href="https://developer.opencagedata.com/">dashboard> van OpenCage).
EOD
    )
];

=head2 INTERFACE_DESCRIPTION

Description for the module, presented to admins when configuring the
integration.

=cut

use constant INTERFACE_DESCRIPTION => <<'EOD';
<p>
    Deze koppeling configureert uw omgeving om gebruik te maken van de
    <a href="https://geocoder.opencagedata.com">OpenCage Geocoder</a> service.
</p>
<p>
    OpenCage Geocoder is een internet service waarmee informatie over publieke
    adressen en locaties kan worden opgehaald. Zaaksysteem gebruikt deze
    service onder andere om op basis van een exacte kaartlocatie een bijhorend
    adres te koppelen.
</p>
<p>
    Voor meer informatie over het gebruik dit koppelprofiel en samenhangende
    functionaliteiten van Zaaksysteem kunt u terecht op de
    <a href="https://wiki.zaaksysteem.nl/wiki/Redirect_koppelprofiel_openCage">
        Zaaksysteem Wiki
    </a>.
</p>
EOD

=head2 INTERFACE_TEST_DESCRIPTION

Description for all module related tests.

=cut

use constant INTERFACE_TEST_DESCRIPTION => <<'EOD';
Om te controleren of de koppeling correct is geconfigureerd, kunt u hieronder
een aantal tests uitvoeren.
EOD

=head2 MODULE_SETTINGS

Main configuration for this module, within the ZAPI/Sysin context of
Zaaksysteem.

=cut

use constant MODULE_SETTINGS => {
    name => INTERFACE_ID,
    label => 'OpenCage Geocoder',
    interface_config => INTERFACE_CONFIG_FIELDS,
    direction => 'outgoing',
    is_multiple => 0,
    is_manual => 0,
    retry_on_error => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface => 0,
    description => INTERFACE_DESCRIPTION,
    sensitive_config_fields => [qw[opencage_api_key]],
    test_interface => 1,
    test_definition => {
        description => INTERFACE_TEST_DESCRIPTION,
        tests => { },
    },
    trigger_definition => {
        geocode => {
            method => 'geocode',
            api => 1
        }
    },
    trigger_definition => {
        reverse_geocode => {
            method => 'reverse_geocode',
            api => 1
        }
    }
};

=head1 ATTRIBUTES

=head2 user_agent

Autovivified L<LWP::UserAgent> for querying OpenCage.

Can be overridden via L</build_user_agent>.

=cut

has user_agent => (
    is => 'rw',
    isa => 'LWP::UserAgent',
    lazy => 1,
    builder => 'build_user_agent'
);

=head2 json

Autovivified L<JSON> instance for decoding responses from OpenCage.

Can be overridden via L</build_json>

=cut

has json => (
    is => 'rw',
    isa => 'JSON',
    lazy => 1,
    builder => 'build_json'
);

=head1 METHODS

=cut

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    return $class->$orig(%{ MODULE_SETTINGS() });
};

=head2 geocode

=cut

define_profile geocode => (
    required => {
        address => 'Str|Zaaksysteem::Object::Types::Address'
    },
    optional => {
        api_key => 'Str'
    }
);

sub geocode {
    my ($self, $options, $interface) = @_;

    my $params = assert_profile($options->{ request_params })->valid;
}

=head2 reverse_geocode

=cut

define_profile reverse_geocode => (
    required => {
        location => 'Zaaksysteem::Object::Types::Location'
    },
    optional => {
        api_key => 'Str'
    }
);

sub reverse_geocode {
    my ($self, $options, $interface) = @_;

    my $params = assert_profile($options)->valid;
    my $location = $options->{ location };

    my $request = $self->build_query_request($interface, {
        query => $location->coordinates,
        api_key => $options->{ api_key }
    });

    my $response = $self->user_agent->request($request);

    unless ($response->is_success) {
        throw('syin/opencage/response_failure', sprintf(
            'Failed to query OpenCage: %s',
            $response->status_line
        ));
    }

    my $txn = $interface->process({
        input_data => sprintf('Location: %s', $location->coordinates),
        direction => 'outgoing',
        processor_params => {
            processor => '_process_response',
            request => $request->as_string,
            response => $response->decoded_content,
            preview => sprintf('Locatie "%s" opgezocht', $location->coordinates)
        }
    });

    my $data = $self->json->decode($response->decoded_content);

    unless (defined $data && ref $data eq 'HASH') {
        throw('sysin/opencage/response_invalid', sprintf(
            'Unable to parse OpenCage response'
        ));
    }

    my @results = @{ $data->{ results } };

    return unless scalar @results;

    my $result = $results[0]->{ components };

    # Parses streetnumbers "4", "4L", "4L-XYZ", "4-XYZ"
    my $regex = qr[
        (\d+) # street number, required
        (?:   # street letter, only first char, optional
            ([[:alpha:]])[[:alpha:]]*
        )?
        (?:   # street number addition, 1-4 chars, optional
            \-([[:alnum:]]{1,4})
        )?
    ]x;

    my ($number, $letter, $extra) = $result->{ house_number } =~ $regex;

    my %addr_args = (
        street => $result->{ road },
        zipcode => $result->{ postcode },
        city => $result->{ suburb } // $result->{ city }
    );

    $addr_args{ street_number        } = $number if $number;
    $addr_args{ street_letter        } = $letter if $letter;
    $addr_args{ street_number_suffix } = $extra  if $extra;

    return Zaaksysteem::Object::Types::Address->new(%addr_args);
}

=head2 build_query_request

=cut

define_profile build_query_request => (
    required => {
        query => 'Str'
    },
    optional => {
        api_key => 'Str'
    }
);

sig build_query_request => 'Zaaksysteem::Backend::Sysin::Interface::Component, HashRef';

sub build_query_request {
    my $self = shift;
    my $interface = shift;
    my $params = assert_profile(shift)->valid;

    my $uri = URI->new($interface->jpath('$.opencage_url'));

    my $key = $interface->jpath('$.opencage_api_key');

    if ($interface->jpath('$.opencage_use_platform_key')) {
        unless ($params->{ api_key }) {
            throw('sysin/opencage/platform_key_required', sprintf(
                'OpenCage is configured to use the platform-bound key, but none supplied'
            ));
        }

        $key = $params->{ api_key };
    }

    $uri->query_form(
        # OpenCage indicates this saves them significant resources,
        # and we don't need annotations
        no_annotations => 1,

        pretty => 1,
        key => $key,
        q => $params->{ query }
    );

    return HTTP::Request->new('GET', $uri->as_string);
}

=head2 build_user_agent

Builds a L<LWP::UserAgent> instance.

=cut

sub build_user_agent {
    return LWP::UserAgent->new(
        agent => sprintf('Zaaksysteem/%s', $Zaaksysteem::VERSION),
    );
}

=head2 build_json

Builds a L<JSON> instance.

=cut

sub build_json {
    return JSON->new;
}

sub _process_response {
    my $self = shift;
    my $record = shift;

    my $txn = $self->process_stash->{ transaction };

    my $params = $txn->get_processor_params;
    my $interface = $txn->interface;

    $record->input($params->{ request });
    $record->output($params->{ response });
    $record->preview_string($params->{ preview });

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
