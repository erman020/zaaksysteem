package Zaaksysteem::Backend::Sysin::Modules::STUFNPS;

use Moose;
use Encode qw(encode_utf8);

use Zaaksysteem::BR::Subject;
use Zaaksysteem::SOAP::Client;
use Zaaksysteem::StUF::0301::Processor::BG0310;
use BTTW::Tools;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUFPRSNPS
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFNPS - StUF NPS module (BG0310) for zaaksysteem.nl

=head1 DESCRIPTION

This module will handle StUF 0301 - BG 0310 traffic from and to zaaksysteem.nl. It supports
"questioning" the other side, as well as receiving mutations. It also supports searching
through GBA-V-Translators based on StUF. This module is currently tested on Centric.

=head1 SYNOPIS

    # Most of this module is used by third party modules, like L<Zaaksysteem::BR::Subject>

=head1 FILE LAYOUT

StUF is rather complex, below a list of modules which are somehow related to this package.

B<Questioning>

    # Business logic
    Zaaksysteem::BR::Subject                            # Main subject manager for retrieving and saving subjects
      Zaaksysteem::BR::Subject::StUF::NP                # Role including the logic for remote import/search
      Zaaksysteem::Backend::Sysin::Modules::STUFNPS     # This module
      
    # XML Reading and generating
    Zaaksysteem::XML::Generator::StUF0301::BG0310       # Interface to raw xml templates
      Zaaksysteem::StUF::0301::Processor::BG0310        # Businesslogic to BG0310 messages: parsing and generating through XML generator

    # Templates: share/xml-templates/stuf0301/bg0310

    # Tests
    TestFor::General::Backend::Sysin::Modules::STUFNPS
    TestFor::General::StUF::0301::Processor::BG0310


=head1 TESTS

Most of the tests can be found in L<TestFor::General::Backend::Sysin::Modules::STUFNPS>

=head1 CONSTANTS

=cut

###
### Interface Properties
###
### Below a list of interface properties, see
### L<Zaaksysteem::Backend::Sysin::Modules> for details.

=head2 INTERFACE_ID

Contains the interface id: C<stufnps>

=cut

use constant INTERFACE_ID               => 'stufnps';

=head2 INTERFACE_CONFIG_FIELDS

Config fields for this interface, empty for now

=cut

use constant INTERFACE_CONFIG_FIELDS    => [];

=head2 MODULE_SETTINGS

Configuration for this module

=cut

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'StUF Koppeling NPS',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text', 'file'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 1,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    trigger_definition              => {
        disable_subscription   => {
            method  => 'disable_subscription',
        },
        enable_object_subscription   => {
            method  => 'enable_object_subscription',
        },
        search_nps      => {
            method  => 'search_nps',
        },
        import_nps      => {
            method  => 'import_nps',
        },
    },
};

=head1 ATTRIBUTES

=head2 stuf_object_type

B<Value>: NPS

The object type of this StUF interface

=cut

has 'stuf_object_type' => (
    'is'        => 'ro',
    'default'   => 'NPS'
);

=head2 stuf_subscription_table

B<Value>: NatuurlijkPersoon

The DBIx::Class schema for this StUF entity.

=cut

has 'stuf_subscription_table' => (
    'is'        => 'ro',
    'default'   => 'NatuurlijkPersoon'
);

=head2 stuf_version

B<Value>: 0310

The StUF version for this entity. Or better spoken: the StUF-BG version.

=cut

has 'stuf_version'      => (
    'is'        => 'ro',
    'default'   => '0310'
);

###
### BUILDARGS
###

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 QUESTION RELATED FUNCTIONS

=head2 search_nps

    my $transaction = $interface->process_trigger('search_nps', { personal_number => '123456789' });

    my @result      = @{ $transaction->get_processor_params->{result} }

Searches for NP entities matching the given params in remote systems

=cut

define_profile search_nps => (
    required => {},
    constraint_methods  => {
        personal_number     => sub {
            my $val     = pop;

            return eval { elfproef($val, $val) };
        },
        'address_residence.zipcode' => sub {
            my $val     = pop;

            return 1 if $val =~ /^[1-9][0-9]{3}[A-Z]{2}$/;
            return;
        },
        'address_residence.street_number' => sub {
            my $val     = pop;

            return 1 if $val =~ /^[0-9]+$/;
            return;
        }
    },
    field_filters   => {
        personal_number     => sub {
            my $val = shift;

            if ($val =~ /^\d{8}$/) {
                $val = sprintf("%09s", $val);
            }

            return $val;
        },
        'address_residence.zipcode' => sub {
            my $val = shift;

            $val =~ s/\s+//g;

            return uc($val);
        },
        date_of_birth               => sub {
            my $val = shift;

            my $converted = date_filter($val);

            return $converted;
        },
    },
    require_some    => {
        bsn_or_zipcode_or_birthdate => [1, qw/personal_number address_residence.zipcode date_of_birth/],
    },
    dependency_groups => {
        zipcode_group       => [qw/address_residence.zipcode address_residence.street_number/],
    }
);

sub search_nps {
    my $self            = shift;
    my $params          = shift;
    my $interface       = shift;

    ### Error when required params are missing, but do not override $params
    my $valid_params    = assert_profile($params)->valid;
    $params             = { %$params, %$valid_params };

    my $config_interface_id = delete $params->{config_interface_id};

    my $schema = $interface->result_source->schema;

    my $config_iface = $schema->resultset('Interface')->find($config_interface_id);
    $self->config_interface($config_iface);

    my $info            = "Gezocht op: ";
    $info .= "\n $_ : " . $params->{$_} for grep { length($params->{$_}) } keys %$params;

    unless ($config_iface->module_object->can_search_sbus($config_iface)) {
        throw(
            'sysin/modules/stuf' . lc($self->stuf_object_type) . '/search_nps/no_permission',
            'Searching via SBUS not setup'
        )
    }

    my $transaction     = $interface->process(
        {
            processor_params        => {
                processor       => '_process_search_nps',
                request_params  => $params,
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => $info,
            config_interface_id     => $config_iface->id,
        }
    );

    unless (
        $transaction->processor_params &&
        $transaction->processor_params->{result}
    ) {
        throw(
            'sysin/modules/stuf' . lc($self->stuf_object_type) . '/search_nps/no_processed_result',
            'Failure in getting results',
        );
    }

    return $transaction;
}

sub soap_error {
    my ($self, $record) = @_;
    return $record->output;
}

=head2 _process_search_nps

    $interface->process(
        {
            processor_params        => {
                processor       => '_process_search_nps',
                request_params  => { personal_number => 12345678 }
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => Data::Dumper::Dumper($params),
        }
    );

Actual processor for searching in other party for NPS

=cut

sub _process_search_nps {
    my $self            = shift;
    my $record          = shift;
    my $transaction     = $self->process_stash->{transaction};

    ### Every error is by default fatal (do not retry)
    $self->process_stash->{error_fatal} = 1;

    my $processor_params    = $transaction->get_processor_params();
    my $params              = $processor_params->{request_params};

    my $xml_processor       = $self->_get_xml_processor(transaction => $transaction);
    my $xml                 = $xml_processor->search_np($params);

    $record->preview_string("Zoek: " . $xml_processor->search_to_string($params));

    my $config_module   = $self->config_interface;
    my $module_cfg      = $self->get_config_from_config_iface;

    my $soapaction      = 'http://www.egem.nl/StUF/sector/bg/0310/npsLv01';

    if ($module_cfg->{stuf_supplier} eq 'pinkv3' && $config_module->module_object->can_search_gbav($config_module)) {
        $soapaction     = 'http://www.egem.nl/StUF/sector/bg/0310/npsLv01Integraal';
    }

    my ($error, $persons);
    my $response_xml = try {
        $self->call_other_party(
            record          => $record,
            xml             => $xml,
            action          => $soapaction,
            xml_processor   => $xml_processor
        );
    } catch {
        $error = {
            message     => $_,
        };
    };

    if ($error || ($error = $self->_get_xml_error($xml_processor, $response_xml, 'bg:npsLa01'))) {
        $record->output('Failure in getting results: ' . $error->{message} . ". XML:\n" . substr($response_xml, 0,3096) );
        $record->is_error(1);
        $transaction->error_fatal(1);
        $transaction->error_count(1);
    } else {
        $persons             = $xml_processor->parse_multiple_np($response_xml);
    }

    $transaction->processor_params(
        {
            %{ $transaction->processor_params },
            result  => ($persons || []),
            ($error ? (error => $error) : ()),
        }
    );

    return $record->output();
}

=head2 import_nps

    my $result          = $interface->process_trigger(
        'import_nps',
        {
            external_subscription   => { external_identifier    => '82923234' },
            subject                 => { personal_number        => '987654321' },
        }
    );

=cut


define_profile import_nps => (
    required            => [qw/subject id/],
    optional            => [qw/external_subscription/],
    constraint_methods  => {
        subject     => sub {
            my $val     = pop;

            return unless ref $val eq 'HASH';
            return unless $val->{personal_number};

            return 1;
        },
        external_subscription     => sub {
            my $val     = pop;

            return unless ref $val eq 'HASH';
            return unless $val->{external_identifier};

            return 1;
        }
    }
);

sub import_nps {
    my $self            = shift;
    my $params          = shift;
    my $interface       = shift;

    assert_profile($params || {});

    if(!$self->may_search_brp($self->config_interface)) {
        throw(
            'sysin/modules/stuf' . lc($self->stuf_object_type) . '/import_nps/no_permission',
            'Importing via SBUS not setup'
        );
    }

    my $config_iface    = $self->config_interface;
    my $transaction     = $interface->process(
        {
            processor_params        => {
                processor       => '_process_import_nps',
                request_params  => $params,
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => Data::Dumper::Dumper($params),
            config_interface_id     => $config_iface->id,
        }
    );

    unless (
        $transaction->processor_params &&
        $transaction->processor_params->{result}
    ) {
        throw(
            'stuf' . lc($self->stuf_object_type) . '/import_nps/no_processed_result',
            'Failure in getting results',
        );
    }

    return $transaction;
}

=head2 _process_import_nps

    $interface->process(
        {
            processor_params        => {
                processor       => '_process_import_nps',
                request_params  => {
                    external_subscription   => { external_identifier    => '82923234' },
                    subject                 => { personal_number        => '987654321' },
                }
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => Data::Dumper::Dumper($params),
        }
    );

Actual processor for importing data from another party

=cut

sub _process_import_nps {
    my $self            = shift;
    my $record          = shift;
    my $transaction     = $self->process_stash->{transaction};

    ### Every error is by default fatal (do not retry)
    $self->process_stash->{error_fatal} = 1;

    my $processor_params    = $transaction->get_processor_params();
    my $params              = $processor_params->{request_params};

    my $external_identifier = $params->{external_identifier}
        // $params->{external_subscription}{external_identifier} // undef;

    my $schema = $transaction->result_source->schema;

    my $br = Zaaksysteem::BR::Subject->new(
        schema => $schema
    );

    my $subject = $br->find($params->{id});

    my $config_interface_id = $self->process_stash->{config_interface_id};

    my $config_interface = $schema->resultset('Interface')->find($config_interface_id);
    $self->config_interface($config_interface);
    my $config_interface_uuid = $config_interface->uuid;

    if (!$subject->has_external_subscription) {
        # This is almost impossible due to the save logic of the import
        # module, but we keep it for now.
        $subject->external_subscription(
            Zaaksysteem::Object::Types::ExternalSubscription->new(
                external_identifier => 'IN_PROGRESS',
                config_interface_uuid => $config_interface_uuid,
                interface_uuid => $transaction->interface_id->uuid,
        ));
        $br->save($subject);
        $subject->discard_changes(schema => $schema);
    }

    my $subscription = $self->_get_object_subscription_by_id(
        $schema,
        $subject->external_subscription->internal_identifier
    );

    my $response_xml = $self->_send_kennisgevings_bericht(
        subscription => $subscription,
        transaction  => $transaction,
        record       => $record,
        subject      => $params->{subject},
        async        => 1,
    );
    $self->log->debug($response_xml);

    my $np = $subject->subject;

    $record->preview_string(
        "Toegevoegd: " .
        join(', ', ($np->personal_number, $np->initials, $np->family_name))
    );

    my $info = "Toegevoegd: \n";
    $info .= "\n $_ : " . $np->$_
        for grep { length($np->$_) }
        qw/personal_number initials first_names family_name/;

    $transaction->input_data($info);

    if ($response_xml) {
        my $interface       = $transaction->interface_id;
        my $config_iface    = $self->config_interface($interface);

        if ($config_iface->jpath('$.mk_spoof')) {
            my $xml_processor = $self->_get_xml_processor(transaction => $transaction);

            $self->_spoof_ack_from_other_party(
                $transaction,
                $xml_processor,
                $params->{id}
            );
        }
    }

    return $record->output();
}

sub _send_kennisgevings_bericht {
    my $self = shift;
    my $options = {@_};

    my $transaction = $options->{transaction};
    my $record     = $options->{record};

    my $external_id
        = $options->{subscription}->external_id eq 'IN_PROGRESS'
        ? undef
        : $options->{subscription}->external_id;

    $external_id = undef;

    my $xml_processor = $self->_get_xml_processor(transaction => $transaction);

    my $xml = $xml_processor->import_np(
        {
            internal_identifier => $options->{subscription}->id,
            ($external_id
                ? (external_identifier => $external_id)
                : (subject => $options->{subject})),
        }
    );

    my $error;
    my $response_xml = try {
        return $self->call_other_party(
            record          => $record,
            xml             => $xml,
            action          => 'http://www.egem.nl/StUF/sector/bg/0310/npsLk01',
            xml_processor   => $xml_processor,
            async           => $options->{async} // 0,
        );
    }
    catch {
        $error = { message => $_ };
    };
    $error //= $self->_get_xml_error($xml_processor, $response_xml);

    if ($error) {
        $self->log->error("Error while retreiving XML from StUF: " . dump_terse($error));

        $record->output('Failure in getting results: ' . $error->{message});
        $record->is_error(1);
        $transaction->error_fatal(1);
        $transaction->error_count(1);

        $transaction->processor_params(
            {
                %{ $transaction->processor_params },
                result  => 0,
                error => $error,
            }
        );
        return undef;
    }
    $transaction->processor_params(
        {
            %{ $transaction->processor_params },
            result  => 1,
        }
    );

    my $bsn = $options->{subject}{personal_number};
    $record->preview_string("Afnemerindicatie: $bsn");

    my $via_gbav = $self->_via_gbav($options->{subscription});
    if ($via_gbav) {
        my $transaction = $transaction->interface_id->process(
            {
                processor_params => {
                    processor => '_process_set_pink_voa',
                    params    => { burgerservicenummer => $bsn }
                },
                external_transaction_id => 'unknown',
                input_data              => "Burgerservicenummer: $bsn",
                direction               => 'outgoing',
            }
        );
    }

    return $response_xml;

}

=head2 _process_disable_subscription

    $interface->process(
        {
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => 'Disable subscription: 44',
            processor_params        => {
                processor                       => '_process_disable_subscription',
                params                          => {
                    subscription_id         => 5654321
                    external_id             => 'MK9239023',
                }
            },
        }
    );

The actual processor for disabling a C<NPS> subscription

First delete the record in our DB, when our call below fails,
we can rollback this. It will not work the other way around

=cut

sub _process_disable_subscription {
    my $self                        = shift;
    my ($record,$object)            = @_;

    my $p_params = $record->transaction_id->get_processor_params->{params};

    my $interface = $record->transaction_id->interface_id;
    my $schema    = $interface->result_source->schema;

    my $subscription = $self->_get_object_subscription_by_id(
        $schema,
        $p_params->{subscription_id}
    );

    my $config_module = $self->config_interface;

    my $module_cfg = $self->get_config_from_config_iface;

    my $return_output = '';

    if (!$config_module->module_object->can_connect_to_other_sbus($config_module)) {

        $self->_disable_np($schema, $subscription);

        $return_output = 'Subscription removed, but no interaction started with servicebus'
                .': synchronization-mode is not "hybrid" or "question". '
                . $p_params->{external_id} . '" must be manually removed';

        $record->output($return_output);
        return $return_output;
    }

    my $transaction = $record->transaction_id;

    my $xml_processor       = $self->_get_xml_processor(transaction => $transaction);
    my $xml                 = $xml_processor->disable_entity(
        {
            internal_identifier     => $subscription->id,
            (
                ($subscription->external_id && $subscription->external_id ne 'IN_PROGRESS')
                    ? (external_identifier     => $subscription->external_id)
                    : ()
            ),
        }
    );

    my ($error, $result);
    my $response_xml = try {
        $self->call_other_party(
            record        => $record,
            xml           => $xml,
            action        => 'http://www.egem.nl/StUF/sector/bg/0310/npsLk01',
            xml_processor => $xml_processor,
            async         => 1,
        );
    }
    catch {
        $error = { message => $_ };
    };

    if ($error
        || ($error = $self->_get_xml_error($xml_processor, $response_xml)))
    {
        $record->output('Failure in getting results: ' . $error->{message});
        $record->is_error(1);
        $transaction->error_fatal(1);
        $transaction->error_count(1);
    }
    else {
        $return_output = $xml;
        $self->_disable_np($schema, $subscription);

    }

    $record->output($return_output);
    return $return_output;
}

sub _disable_np {
    my ($self, $schema, $subscription) = @_;

    my $np = $schema->resultset($subscription->local_table)->find($subscription->local_id);
    $np->disable_natuurlijk_persoon;
    $subscription->update({date_deleted => DateTime->now() });
    return 1;
}

=head2 call_other_party

    $self->call_other_party(
        record          => $record,                             # Record
        xml             => $xml,                                # Raw XML (string)
        action          => '{http://localhost/service}Test',    # SOAPAction
        xml_processor   => $xml_processor   # instance of Zaaksysteem::StUF::0301::Processor::BG0310
    );

Initiates a SOAP call to the "other party". Sending the given xml. Will return the raw
response XML

=cut

sub call_other_party {
    my $self        = shift;
    my %opts        = @_;
    my $record      = $opts{record};
    my $transaction = $record->transaction_id;
    my $interface   = $transaction->interface_id;

    ### Load configuration
    my $config_iface    = $self->config_interface;
    my %certs           = $self->_get_certificates($config_iface);

    my $spoof;
    if ($config_iface->jpath('$.mk_spoof')) {
        $spoof = $self->_spoof_response($transaction, $opts{xml_processor}, $opts{xml});

        $spoof ||= '<?xml version="1.0"?>';
    }

    ### Load SOAP CLient
    my $soap_client     = Zaaksysteem::SOAP::Client->new(
        endpoint    => ($opts{async} ? $config_iface->jpath('$.mk_async_url') : $config_iface->jpath('$.mk_sync_url')),
        ($spoof ? (spoof_response => $spoof) : ()),

        # TODO: Can we please please please please get rid of the verify
        # hostname and the http allowed?
        ua => LWP::UserAgent->new(
            agent   => "Zaaksysteem/" . ($Zaaksysteem::VERSION // 'CLI-X_X'),
            timeout => 10,
            ssl_opts => {
                verify_hostname => 0,
                %certs,
            },
            protocols_allowed => [qw(https http)],
        ),
    );

    ### Do the actual call
    my $soap_data = $soap_client->call(
        $opts{action},
        $opts{xml},
    );

    $record->input($soap_data->{request}->decoded_content);
    $record->output($soap_data->{response}->decoded_content);

    return $soap_data->{response}->decoded_content;
}

=head1 MUTATION RELATED FUNCTIONS

=head2 $module->_binnen_gemeente($object)

    $self->_binnen_gemeente($nps_object);

B<Return value>: $TRUE_OR_FALSE

Returns whether this person has moved to another city or not. In case of a 0301 xml message,
this would mean the system will check the configured gemeente_code in the StUF configuration.

=cut

sub _binnen_gemeente {
    my $self                = shift;
    my ($record, $object, $adres)   = @_;

    if ($adres && $adres->landcode && $adres->landcode != 6030) {
        return 0;
    }

    my $gemeentecode = $self->get_config_from_config_iface()->{gemeentecode};

    return ($object->binnen_gemeente($gemeentecode));
}

=head1 PRIVATE METHODS

=head2 _spoof_ack_from_other_party

    $self->_spoof_ack_from_other_party($transaction, $xml_processor, '89a98-da8f98-adf89a89-8af98');

Spoofs a lk01 to our system according to the given UUID.

=cut

sub _spoof_ack_from_other_party {
    my ($self, $transaction, $xml_processor, $uuid) = @_;
    my $interface       = $transaction->interface_id;
    my $config_iface    = $self->config_interface;
    my $schema          = $transaction->result_source->schema;

    # No spoof initiated
    return 1 unless $config_iface->jpath('$.mk_spoof');

    my $np              = $schema->resultset('NatuurlijkPersoon')->search({uuid => $uuid})->first;

    throw(
        'sysin/modules/stufnps/spoof/no_np_found_by_uuid',
        'Cannot find NP by given uuid'
    ) unless $np;

    throw(
        'sysin/modules/stufnps/spoof/no_subscription_found',
        'Cannot find a subscription with given NP'
    ) unless $np->subscription_id;

    # Get person and object_subscription from uuid
    my $xml = $xml_processor->spoof_ack_from_other_party(
        {
            external_id     => $np->subscription_id->external_id,
            local_object_id => $np->get_column('subscription_id'),
            personal_number => $np->burgerservicenummer,
            config_interface_id => $config_iface->id,
        }
    );

    # Send kennisgeving into system with verzendend and s
    my $new_transaction     = $interface->process(
        {
            direction               => 'incoming',
            external_transaction_id => 'unknown',
            input_data              => $xml,
            config_interface_id     => $config_iface->id,
        }
    );

}

=head2 _register_object_subscription

    $self->_register_object_subscription(
        {
            transaction     => $transaction,
            uuid            => $params->{id},
            external_id     => 'IN_PROGRESS',
        }
    );

Registers an object subscription

=cut

define_profile _register_object_subscription => (
    required => [qw/transaction uuid external_id config_interface_id/],
);

sub _register_object_subscription {
    my $self        = shift;
    my $params      = assert_profile(shift || {})->valid;
    my $np          = shift;

    my $transaction = $params->{transaction};

    my $schema      = $transaction->result_source->schema;

    $np //= $schema->resultset('NatuurlijkPersoon')
        ->search({ uuid => $params->{uuid} })->first;

    throw(
        'sysin/modules/stufprs/object_subscription/no_np_found_by_uuid',
        "No NP is found by UUID $params->{uuid}"
    ) unless $np;

    my $subscription = $np->add_object_subscription(
        config_interface_id => $self->config_interface->id,
        external_id     => $params->{external_id},
        interface_id    => $transaction->get_column('interface_id'),
        authenticatedby => 'gba',
    );

    return $subscription;
}

=head2 _get_xml_error

    print $self->_get_xml_error($xml_processor, $xml)

    ### Returns undef when there is no problem, otherwise
    ### {
    ###     is_stuf_error   => 1,
    ###     code            => 244,
    ###     message         => "Authorizatieprobleem"
    ### }

Will return a fault when there is one

=cut

sub _get_xml_error {
    my $self            = shift;
    my $xml_processor   = shift;

    return $xml_processor->parse_stuf_error(@_);
}


=head2 _spoof_response

    my $res_xml = $self->_spoof_response($transaction, $xml_processor, $string_xml);

Returns a response XML with spoofed data

=cut

sub _spoof_response {
    my ($self, $transaction, $xml_processor, $xml)   = @_;

    my $SOAP_ENVELOPE = q{<?xml version="1.0"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
<SOAP-ENV:Body>%s</SOAP-ENV:Body>
</SOAP-ENV:Envelope>};

    my $params  = $transaction->get_processor_params->{request_params};

    my $resxml;
    if ($xml =~ /npsLv01/) {
        $resxml = $xml_processor->answer_question_np($params);
        # $resxml = $xml_processor->error_message({errortype => 'Fo01', code =>'StUF0001', message => "Testerror"});
    }

    if ($xml =~ /npsLk01/) {
        $resxml = $xml_processor->confirm_message();
    }

    return sprintf($SOAP_ENVELOPE, encode_utf8($resxml));
}

=head2 _get_stuf_config

    my $config = $self->_get_stuf_config($interface);

    # $config contains processor config

=cut

sub _get_stuf_config {
    my ($self, $interface) = @_;

    my $config_iface    = $self->config_interface;
    return $self->get_config_from_config_iface;
}


=head2 _get_xml_processor

    my $processor = $self->_get_xml_processor(transaction => $transaction);

=cut

sub _get_xml_processor {
    my $self        = shift;
    my %opts        = @_;
    my $transaction = $opts{transaction};

    my $call_options    = $self->_load_call_options_from_config(
        $transaction->interface_id,
        {
            reference       => $transaction->id,
            datetime        => DateTime->now(),
        }
    );

    my $config = $self->get_config_from_config_iface();
    my $schema = $transaction->result_source->schema;

    my %sender_id = (
        applicatie  => $call_options->{sender},
    );

    if ($config->{stuf_supplier} eq 'centric') {
        if (defined $schema->current_user) {
            $sender_id{gebruiker} = $schema->current_user->username;
        } else {
            $sender_id{gebruiker} = 'unknown';
        }
    }

    return Zaaksysteem::StUF::0301::Processor::BG0310->new(
        interface_uuid  => $transaction->interface_id->uuid,
        schema          => $schema,
        reference_id    => $call_options->{reference},
        our_party       => \%sender_id,
        other_party     => {
            applicatie => $call_options->{receiver},
        },
        other_party_primary_key => 'sleutelVerzendend',
        municipality_code       => $config->{gemeentecode},
    );
}

=head2 _process_enable_object_subscription

Process the object subcription and send out all the data of the subject.
The receiving party needs all the subject data because when they send a
confirmation they use the data we provided to them and we save that data.

=cut

sub _process_enable_object_subscription {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    try {
        $self->log_params($params);

        my $schema = $interface->result_source->schema;

        my $subscription = $self->_get_object_subscription_by_id($schema, $params->{subscription_id});

        my $np = $subscription->get_local_entry;

        if (!$np) {
            # Probably only on a developement environment
            $self->log->info("Unable to enable an object subscription for an non authenticated person");
            return 0;
        }

        my $object  = $np->as_object;
        my $br = Zaaksysteem::BR::Subject->new(
            schema => $schema,
        );

        my $subject = $br->object_as_params($object->subject);

        my $response_xml = $self->_send_kennisgevings_bericht(
            subscription => $subscription,
            transaction  => $transaction,
            record       => $record,
            subject      => $subject,
            async        => 1,
        );
    }
    catch {
        $self->log->error($_);
        $self->set_record($record, input => $params);
        $self->catch_error($record, $_);
    };
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::StUF::0301::Processor::BG0310> L<Zaaksysteem::Backend::Sysin::Modules::STUFCONFIG>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

