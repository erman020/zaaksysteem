package Zaaksysteem::Backend::Sysin::Modules::STUFCONFIG;

use Moose;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFCONFIG - StUF configuration

=head1 DESCRIPTION

STUF Configuration for StUF related queries

=head1 SYNOPSIS

=cut

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use JSON;
use Crypt::OpenSSL::X509;
use LWP::UserAgent;
use IO::Socket::SSL;
use HTTP::Request;
use DateTime;

use BTTW::Tools;
use Zaaksysteem::Constants qw/RGBZ_GEMEENTECODES/;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUF
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
    Zaaksysteem::Backend::Sysin::Modules::Roles::MultiTenant
);


use constant INTERFACE_ID => 'stufconfig';

sub build_config_fields {
    my $self  = shift;
    my @fields = (
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_stuf_supplier',
            type  => 'select',
            label => 'Aanbieder van ServiceBus',
            data  => {
                options => [
                    {
                        value => 'centric',
                        label => 'Centric (0301)',
                    },
                    {
                        value => 'pink',
                        label => 'Pink (0204)',
                    },
                    {
                        value => 'pinkv3',
                        label => 'Pink (0301)',
                    },
                    {
                        value => 'vicrea',
                        label => 'Vicrea (0204)',
                    }
                ],
            },
            required => 1,
            description =>
                'Elke leverancier heeft haar eigen manier van interpretatie '
                . 'van het StUF protocol. Om deze smaken uit elkaar te houden, vragen wij '
                . 'u een keuze te maken voor een leverancier.'
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_synchronization_type',
            type  => 'select',
            label => 'Type koppeling',
            data  => {
                options => [
                    {
                        value => 'mutation',
                        label => 'Mutatieberichten',
                    },
                    {
                        value => 'question',
                        label => 'Vraagberichten',
                    },
                    {
                        value => 'hybrid',
                        label => 'Hybride (vraag + mutatieberichten)',
                    },
                ],
            },
            required    => 1,
            description => <<'EOD'
Het type van de koppeling bepaalt of er gebruik gemaakt kan worden van
vraagberichten en/of mutatieberichten.

De keuzes 'Vraagberichten' en 'Hybride' zorgen er voor dat bij
intergemeentelijke verhuizingen de contacten niet worden verwijderd.

Uiteraard is het niet mogelijk GBA-V (integrale zoekvraag BRP) bevragen te
doen indien enkel het type 'Mutatieberichten' geselecteerd is.
EOD
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_gemeentecode',
            type     => 'select',
            label    => 'Gemeente code',
            required => 1,
            data     => {
                options => [
                    (
                        sort { $a->{label} cmp $b->{label} } map {
                            {
                                value => $_,
                                label => sprintf("%s (%s)",
                                    RGBZ_GEMEENTECODES()->{$_}, $_,)
                            }
                        } keys %{ RGBZ_GEMEENTECODES() }
                    ),

                    # Special non-standard values
                    { value => 0,    label => 'Ongedefinieerd (0)' },
                    { value => 9999, label => 'Test (9999)' }
                ],
            },
            description =>
                'Selecteer de gemeente voor welke deze koppeling van toepassing is',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_search_form_title',
            type     => 'text',
            label    => 'Naam op zoekformulier',
            required => 1,
            data     => { placeholder => 'Vul hier een titel in' },
            description =>
                'Naam voor deze configuratie, zoals deze op het formulier voor contacten zoeken getoond wordt.',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_ca_cert',
            type        => 'file',
            label       => 'Makelaar CA-certificaat',
            required    => 0,
            description => <<'EOD'
<p>
    Upload hier de
    <abbr title="Certificate Authority">CA</abbr>-certificaat-keten dat
    Zaaksysteem moet gebruiken om de makelaar te authenticeren.
</p>
<p>
    Indien er geen keten geüpload is, valt Zaaksysteem terug op de ingebouwde
    set aan vertrouwde CA's.
</p>
EOD
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_client_cert',
            type        => 'file',
            label       => 'Zaaksysteem client certificaat',
            required    => 0,
            description => <<'EOD'
<p>
    Upload hier het gecombineerde certifcaat+key bestand, waarmee Zaaksysteem
    vraagbericht communicatie beveiligt.
</p>
<p>
    Dit bestand kan alleen door Mintlab geupload worden in verband met het
    beveiligingsbeleid.
</p>
EOD
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_mk_cert',
            type        => 'file',
            label       => 'Makelaar client certificaat',
            description => <<'EOD'
<p>
    Upload hier het publieke deel van het client-certificaat van de makelaar.
</p>
<p>
    Wanneer er berichten van de makelaar naar Zaaksysteem verstuurd worden,
    én de makelaar maakt gebruik van een client certificaat, wordt dit
    certificaat gebruikt om de identiteit van de makelaar te authenticeren.
</p>
EOD
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_mk_sender',
            type        => 'text',
            label       => 'Identificatie verzender',
            default     => 'ZSNL',
            required    => 1,
            data        => { placeholder => 'ZSNL' },
            description => <<'EOD'
Vul hier de naam in die door Zaaksysteem gebruikt moet worden om de ontvanger
en verzender van StUF berichten te identificeren.
EOD
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_mk_ontvanger',
            type        => 'text',
            label       => 'Identificatie ontvanger',
            required    => 1,
            data        => { placeholder => 'CGM' },
            description => <<'EOD'
Vul hier de naam in die door de makelaar gebruikt wordt om de ontvanger en
verzender van StUF berichten te identificeren.
EOD
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_gbav_applicatie',
            type  => 'text',
            label => 'Identificatie GBA-V ontvanger',
            when =>
                'interface_synchronization_type === "hybrid" || interface_synchronization_type === "question"',
            required    => 1,
            data        => { placeholder => 'CML' },
            description => <<'EOD'
Vul hier de naam in die door de makelaar verwacht wordt wanneer er
vraagberichten (GBA-V) verstuurd worden door Zaaksysteem.
EOD
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_mk_ontvanger_afnemer',
            type        => 'text',
            label       => 'Identificatie afnemer ontvanger',
            required    => 1,
            data        => { placeholder => 'CMODIS' },
            description => <<'EOD'
Vul hier de naam in die door de makelaar verwacht wordt wanneer er berichten
verstuurd worden met betrekking tot afnemersindicaties.
EOD
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name => 'interface_gbav_search',
            type => 'checkbox',
            when =>
                'interface_synchronization_type === "hybrid" || interface_synchronization_type === "question"',
            label       => 'Vraagberichten naar GBA-V toestaan',
            description => <<'EOD'
Geef hier aan of het gebruik van integrale zoekvragen via de BRP (GBA-V) is
toegestaan in het gebruik van Zaaksysteem.
EOD
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            when =>
                '(interface_synchronization_type === "hybrid" || interface_synchronization_type === "question") && interface_gbav_search === true',
            name => 'interface_gbav_search_role_restriction',
            type => 'checkbox',
            label =>
                "Vraagberichten alleen toestaan voor 'BRP extern bevrager'-rol",
            description =>
                "Vraagberichten alleen toestaan voor 'BRP extern bevrager'-rol"
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_search_extern_webform_only',
            type  => 'checkbox',
            label => 'Vraagberichten alleen toestaan via webformulier',
            when =>
                '(interface_synchronization_type === "hybrid" || interface_synchronization_type === "question") && interface_gbav_search === true',
            description => <<'EOD'
Vink deze optie aan indien het niet wenselijk is dat medewerkers binnen
Zaaksysteem integrale zoekvragen kunnen doen. In dat geval worden GBA-V
bevragingen enkel uitgevoerd via publieke webformulieren voor contacten die
nog niet bekend zijn in Zaaksysteem.
EOD
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_mk_sync_url',
            type     => 'text',
            label    => 'Makelaar endpoint (synchroon)',
            required => 0,
            data     => {
                placeholder =>
                    'https://makelaar.domein.nl/pad/naar/synchroon/endpoint'
            },
            description => <<'EOF',
<p>
    Om te communiceren met een makelaar moet de StUF koppeling geconfigureerd
    zijn met een endpoint om berichten te kunnen uitwisslen met de makelaar.
</p>
<p>
    De in te vullen waarde dient geleverd te worden door de leverancier van de
    makelaar, waarbij aangegeven moet zijn dat het gaat om de endpoint voor
    <em>synchroon</em> berichtenverkeer.
<p>
EOF
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_mk_async_url',
            type     => 'text',
            label    => 'Makelaar endpoint (asynchroon)',
            required => 0,
            data     => {
                placeholder =>
                    'https://makelaar.domein.nl/pad/naar/asynchroon/endpoint'
            },
            description => <<'EOF',
<p>
    Om te communiceren met een makelaar moet de StUF koppeling geconfigureerd
    zijn met een endpoint om berichten te kunnen uitwisslen met de makelaar.
</p>
<p>
    De in te vullen waarde dient geleverd te worden door de leverancier van de
    makelaar, waarbij aangegeven moet zijn dat het gaat om de endpoint voor
    <em>asynchroon</em> berichtenverkeer.
<p>
EOF
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name  => 'interface_gbav_pink_url',
            type  => 'text',
            label => 'PinkRoccade endpoint integrale BRP-bevraging',
            when =>
                '(interface_synchronization_type === "hybrid" || interface_synchronization_type === "question") && (interface_stuf_supplier === "pink" || interface_stuf_supplier === "pinkv3")',
            data => {
                placeholder =>
                    'https://makelaar.domein.nl/pad/naar/gba-v/voa/endpoint'
            },
            description => <<'EOF',
<p>
    Wanneer u gebruik maakt van de makelaarsuite van
    <a href="http://www.pinkroccadelocalgovernment.nl/">PinkRoccade</a> en
    gebruik wilt maken van GBA-V vraagberichten is het noodzakelijk een andere
    endpoint te gebruiken voor dit specifieke berichtenverkeer.
</p>
<p>
    De in te vullen waarde dient geleverd te worden door PinkRoccade, waarbij
    aangegeven moet zijn dat het gaat om de endpoint voor de <em>GBA-V</em>
    service.
</p>
EOF
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_mk_spoof',
            type        => 'checkbox',
            label       => 'Laat zaaksysteem een makelaar simuleren',
            description => <<'EOD'
<p>
    Met onze zogenaamde "Spoof"-modus kan een servicebus gesimuleerd worden,
    zodat getest kan worden of verschillende scenario's van de koppeling
    werken.
</p>
<p>
    <em>LET OP</em>, deze optie is exclusief voor testscenario's bedoeld, en
    zal op een live omgeving de StUF koppeling functioneel onbruikbaar maken
    zolang als de optie geactiveerd is.
</p>
EOD
        )
    );
    return \@fields;
}

# Directly presented in sysin-ui.
use constant INTERFACE_DESCRIPTION => <<'EOD';
<p>
    Deze koppeling configureert de algemene
    <a href="http://gemmaonline.nl/index.php/StUF_Berichtenstandaard">StUF</a>
    aansluiting op uw Zaaksysteem.
</p>
<p>
    Om deze koppeling te configureren heeft u specifieke informatie van de
    StUF makelaar leverancier nodig, en dient de makelaar zelf ook
    geconfigureerd te worden zodat Zaaksysteem er mee mag en kan communiceren.
</p>
<p>
    Voor meer informatie kunt u terecht op de
    <a href="http://wiki.zaaksysteem.nl/wiki/Koppelprofiel_StUF_configuratie">
        Zaaksysteem Wiki
    </a>
</p>
EOD

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'StUF Koppeling Configuratie',
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_multiple                   => 0,
    is_manual                     => 1,
    retry_on_error                => 1,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    description                   => INTERFACE_DESCRIPTION,
    sensitive_config_fields       => [
        qw(
            mk_cert
            ca_cert
            client_cert
            mk_ontvanger
            gbav_applicatie
            mk_ontvanger_afnemer
            mk_sync_url
            mk_async_url
            gbav_pink_url
            )
    ],
    test_interface  => 1,
    test_definition => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u uw verbinding
            met uw servicebus.
        },
        tests => [
            {
                id     => 1,
                label  => 'Test CA certificaat',
                name   => 'ca_certificate_test',
                method => 'stuf_test_ca_certificate',
                description =>
                    'Test of de CA-certificaatketen correct en compleet is.'
            },
            {
                id     => 2,
                label  => 'Test client-certificaat',
                name   => 'client_certificate_test',
                method => 'stuf_test_client_certificate',
                description =>
                    'Test of het client certificate correct en compleet is.'
            },
            {
                id     => 3,
                label  => 'Test firewall',
                name   => 'connection_test',
                method => 'stuf_test_connection',
                description =>
                    'Kan verbinding maken met server en poort van datadistributiesysteem'
            },
            {
                id     => 4,
                label  => 'Test TLS-verbinding',
                name   => 'secure_connection_test',
                method => 'stuf_test_ssl_connection',
                description =>
                    'Verbinding met datadistributiesysteem mbv. geconfigureerd CA certificaat en client-certificaat'
            },
            {
                id          => 5,
                label       => 'Test berichtuitwisseling',
                name        => 'message_test',
                method      => 'stuf_test_message_exchange',
                description => 'Berichtenverkeer met datadistributiesysteem'
            }
        ],
    },

};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{ MODULE_SETTINGS() });
};


=head2 stuf_test_connection

Runs a basic tcp connectability test

=cut

sub stuf_test_connection {
    my $self      = shift;
    my $interface = shift;

    $self->test_host_port($_) for $self->_get_test_endpoints($interface);

    return;
}

=head2 stuf_test_ca_certificate

Runs a series of checks on the CA certificate chain, to see if it's usable.

=cut

sub stuf_test_ca_certificate {
    my $self      = shift;
    my $interface = shift;

    my $ca = $self->_get_cert_path($interface, 'ca_cert');
    return if not $ca;

    $self->test_ca_chain($ca);

    return;
}

=head2 stuf_test_client_certificate

Runs a series of checks on the CA certificate chain, to see if it's usable.

=cut

sub stuf_test_client_certificate {
    my $self      = shift;
    my $interface = shift;

    my $cert = $self->_get_cert_path($interface, 'client_cert');
    $self->test_client_certificate($cert);

    return;
}

=head2 stuf_test_ssl_connection

Runs a secure tcp connectability test.

=cut

sub stuf_test_ssl_connection {
    my $self      = shift;
    my $interface = shift;

    my $ca          = $self->_get_cert_path($interface, 'ca_cert');
    my $client_cert = $self->_get_cert_path($interface, 'client_cert');

    for my $endpoint ($self->_get_test_endpoints($interface)) {
        $self->test_host_port_ssl($endpoint, $ca, $client_cert);
    }

    return;
}

=head2 stuf_test_message_exchange

Higher-level test of the message exchange between the Zaaksysteem instance and
configured DDS. This test throws some invalid data to the DDS, and if a
SOAP/StUF error is thrown, the exchange is 'succesful'.

=cut

sub stuf_test_message_exchange {
    my $self      = shift;
    my $interface = shift;

    # Prevent superfluous tests if the SSL connection aint even right yet
    $self->stuf_test_ssl_connection($interface);

    my $ca          = $self->_get_cert_path($interface, 'ca_cert');
    my $client_cert = $self->_get_cert_path($interface, 'client_cert');

    my $template = <<'EOM';
<?xml version="1.0"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
  <SOAP-ENV:Body>
    <bg:npsLv01 xmlns:bg="http://www.egem.nl/StUF/sector/bg/0310" xmlns:st="http://www.egem.nl/StUF/StUF0301" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <bg:stuurgegevens>
        <st:berichtcode>Lv01</st:berichtcode>
        <st:zender>
          <st:applicatie>%s</st:applicatie>
        </st:zender>
        <st:ontvanger>
          <st:applicatie>%s</st:applicatie>
        </st:ontvanger>
        <st:referentienummer>%s</st:referentienummer>
        <st:tijdstipBericht>%s</st:tijdstipBericht>
        <st:entiteittype>NPS</st:entiteittype>
      </bg:stuurgegevens>
      <bg:parameters>
        <st:sortering>1</st:sortering>
        <st:indicatorVervolgvraag>false</st:indicatorVervolgvraag>
        <st:maximumAantal>3</st:maximumAantal>
        <st:indicatorAantal>true</st:indicatorAantal>
      </bg:parameters>
      <bg:gelijk st:entiteittype="NPS">
        <bg:verblijfsadres>
          <bg:aoa.postcode st:exact="true">1333LH</bg:aoa.postcode>
          <bg:aoa.huisnummer xsi:nil="true"/>
        </bg:verblijfsadres>
      </bg:gelijk>
      <bg:scope>
        <bg:object st:entiteittype="NPS">
          <bg:inp.bsn xsi:nil="true"/>
        </bg:object>
      </bg:scope>
    </bg:npsLv01>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
EOM

    my $ua = LWP::UserAgent->new(
        ssl_opts => {
            SSL_cert_file       => $client_cert,
            SSL_key_file        => $client_cert,
            SSL_verify_callback => sub { 1 }
        }
    );

    my $message = sprintf($template,
        $interface->jpath('$.mk_sender'),
        $interface->jpath('$.mk_ontvanger'),
        int(rand(32768)), DateTime->now->strftime('%Y%m%d%H%I%S000'));

    for my $endpoint ($self->_get_test_endpoints($interface)) {
        my $request = HTTP::Request->new(POST => $endpoint);

        $request->header('Content-Type' => 'text/xml;charset=UTF-8');
        $request->header(
            'SOAPAction' => 'http://www.egem.nl/StUF/sector/bg/0310/npsLv01');

        $request->content($message);

        my $response = $ua->request($request);

        my $data = $response->decoded_content;

        unless ($data =~ m[Envelope]g && $data =~ m[StUF]gi) {
            throw('sysin/modules/stufconfig/test/message_exchange_failed',
                <<'EOM');
Berichtuitwisseling met de geconfigureerde DDS mislukt,
mogelijk is de DDS of Zaaksysteem nog niet correct
geconfigureerd. Mogelijke oorzaken:
 * Verzend/ontvang identificatie
 * Zaaksysteem/DDS client certificaten niet uitgewisseld
 * Aanbieder incorrect ingesteld
EOM
        }
    }
}

=head2 can_search_sbus

Arguments: $ROW_CONFIG_INTERFACE

Return value: $ROW_PRS_OR_NPS_INTERFACE or FALSE

    my $prs_or_nps_interface = $stufconfig_module->can_search_sbus($stufconfig);

Returns the Natuurlijk Persoon interface according to the settings of the stufconfig
module.

Will return a NPS or PRS interface, Depending on the stuf_supplier (pink,vicrea,centric) and
the setting: interface_sbus_search and interface_bidirectional

=cut


sub can_search_sbus {
    my $self             = shift;
    my $config_interface = shift;

    throw(
        'modules/stufconfig/invalid_config_interface',
        'Please supply the config interface as first parameter'
        )
        unless ($config_interface
        && $config_interface->module eq 'stufconfig');

    return unless $config_interface->active;

    my $prsnps_interface
        = $self->get_natuurlijkpersoon_interface($config_interface);

    return unless ($prsnps_interface && $prsnps_interface->active);

    if ($config_interface->get_interface_config->{synchronization_type}
        =~ /^(?:question|hybrid)$/)
    {
        return $prsnps_interface;
    }

    return;

    #$interface->result_source
}

=head2 can_connect_to_other_sbus

Arguments: $ROW_CONFIG_INTERFACE

Return value: $ROW_PRS_OR_NPS_INTERFACE or FALSE

    my $prs_or_nps_interface = $stufconfig_module->can_search_sbus($stufconfig);

Returns the Natuurlijk Persoon interface according to the settings of the stufconfig
module.

Will return a NPS or PRS interface, Depending on the stuf_supplier (pink,vicrea,centric) and
if the C<synchronization_type> equals "hybrid" or "question"

=cut


sub can_connect_to_other_sbus {
    my $self             = shift;
    my $config_interface = shift;

    throw(
        'modules/stufconfig/invalid_config_interface',
        'Please supply the config interface as first parameter'
        )
        unless ($config_interface
        && $config_interface->module eq 'stufconfig');

    return unless $config_interface->active;

    my $prsnps_interface
        = $self->get_natuurlijkpersoon_interface($config_interface);

    return unless ($prsnps_interface && $prsnps_interface->active);


    if (   $config_interface->get_interface_config->{synchronization_type}
        && $config_interface->get_interface_config->{synchronization_type}
        =~ /^(?:question|hybrid)$/)
    {
        return $prsnps_interface;
    }

    return;

    #$interface->result_source
}

=head2 can_search_gbav

Arguments: $ROW_CONFIG_INTERFACE

Return value: $ROW_PRS_OR_NPS_INTERFACE or FALSE

    my $prs_or_nps_interface = $stufconfig_module->can_search_sbus($stufconfig);

Returns the Natuurlijk Persoon interface according to the settings of the stufconfig
module. Only when we are allowed to search the GBAV

Will return a NPS or PRS interface, Depending on the stuf_supplier (pink,vicrea,centric) and
the setting: interface_gbav_search and interface_bidirectional

=cut


sub can_search_gbav {
    my $self             = shift;
    my $config_interface = shift;

    throw(
        'modules/stufconfig/invalid_config_interface',
        'Please supply the config interface as first parameter'
        )
        unless ($config_interface
        && $config_interface->module eq 'stufconfig');

    return unless $config_interface->active;

    my $prsnps_interface
        = $self->get_natuurlijkpersoon_interface($config_interface);

    return unless ($prsnps_interface && $prsnps_interface->active);


    if ($config_interface->get_interface_config->{synchronization_type}
        =~ /^(?:question|hybrid)$/
        && $config_interface->get_interface_config->{gbav_search})
    {
        return $prsnps_interface;
    }

    return;

    #$interface->result_source
}

=head2 get_natuurlijkpersoon_interface

Arguments: $ROW_CONFIG_INTERFACE

Return value: $ROW_PRS_OR_NPS_INTERFACE or FALSE

    my $prs_or_nps_interface = $stufconfig_module->can_search_sbus($stufconfig);

Returns the active Natuurlijk Persoon interface according to the settings of the stufconfig
module "stuf_supplier".

=cut

sub get_natuurlijkpersoon_interface {
    my $self             = shift;
    my $config_interface = shift;

    throw(
        'modules/stufconfig/invalid_config_interface',
        'Please supply the config interface as first parameter'
        )
        unless ($config_interface
        && $config_interface->module eq 'stufconfig');

    my $type = 'prs';

    my $supplier
        = lc($config_interface->get_interface_config->{stuf_supplier} // '');

    if (!$supplier) {
        $self->log->warn("StUF-Config interface without supplier: "
                . $config_interface->id);
    }

    if ($supplier eq 'centric' || $supplier eq 'pinkv3') {
        $type = 'nps';
    }

    return $config_interface->result_source->schema->resultset('Interface')
        ->search_active({ module => 'stuf' . $type })->first;
}

=head2 get_primary_key

Arguments: $ROW_CONFIG_INTERFACE

Return value: $STRING_SLEUTEL_IDENTIFIER

    print $module_cfg->get_primary_key;

    # Prints either:
    #   sleutelGegevensbeheer
    # OR
    #   sleutelVerzendend

Depending on the manufacturer..pink or others

=cut

sub get_primary_key {
    my $self             = shift;
    my $config_interface = shift;

    if ($config_interface->get_interface_config->{stuf_supplier}
        && lc($config_interface->get_interface_config->{stuf_supplier}) eq
        'centric')
    {
        return 'sleutelVerzendend';
    }

    return 'sleutelGegevensbeheer';
}

sub _get_test_endpoints {
    my $self      = shift;
    my $interface = shift;

    return grep { $_ } map { $interface->jpath(sprintf('$.%s', $_)) } qw[
        mk_async_url
        mk_sync_url
        gbav_pink_url
    ];
}

sub _get_cert_path {
    my $self      = shift;
    my $interface = shift;
    my $key       = shift;

    my $id = $interface->jpath(sprintf('$.%s[0].id', $key));

    return unless $id;

    my $filestore = $interface->result_source->schema->resultset('Filestore');

    return $filestore->find($id)->get_path;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
