package Zaaksysteem::Backend::Sysin::Modules::STUFZKN;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::BaseURI
    Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate
    Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPGeneric
/;

use XML::LibXML;
use BTTW::Tools;
use Zaaksysteem::StUF::0310::Processor;
use Zaaksysteem::StUF::ZKN::0102::Model;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::Tools::SysinModules qw(select_municipality_code);

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFZKN - STUF DMS/STUF ZKN interface

=head1 DESCRIPTION

Interface module that implements the SOAP calls described in the STUF-DMS/STUF-ZKN.

This module implements the L<Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPGeneric>
role, making the STUF-ZKN calls available through a SOAP interface.

=cut

my $INTERFACE_ID = 'stuf_zkn';

my @INTERFACE_CONFIG_FIELDS = (

    select_municipality_code(),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_allow_new_cases',
        type        => 'checkbox',
        required    => 0,
        label       => 'Registreren van nieuwe zaken accepteren',
        description => '',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_allow_phase_update',
        type        => 'checkbox',
        required    => 0,
        label       => 'Afronden fase accepteren',
        description => '',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_client_certificate',
        type        => 'file',
        label       => 'Certificaat (public key)',
        description => 'Upload hier het certificaat van de software die de SOAP-interface van deze koppeling zal aanroepen',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_casetypes',
        type        => 'multiple',
        label       => 'Zaaktypes',
        required    => 0,
        data => {
            fields => [
                {
                    name => 'casetype',
                    type => 'spot-enlighter',
                    label => 'Zaaktype',
                    description => 'Let op! Zorg dat het geselecteerde zaaktype een unieke waarde in het veld "identificatie" heeft. Deze identificatiecode wordt door StUF-ZKN gebruikt om het zaaktype te selecteren.',
                    data => {
                        restrict => 'casetype/object',
                        placeholder => 'Type uw zoekterm',
                        label => 'values.name'
                    }
                },
            ],
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_url',
        type        => 'display',
        label       => 'Endpoint URL (voor andere systemen)',
        description => '',
        required    => 0,
        data => { template => '<[field.value]>/sysin/interface/<[activeLink.id]>/soap' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_test_url',
        type        => 'display',
        label       => 'Simulatietool',
        description => '',
        required    => 0,
        default     => '/stuf-zkn/simulator',
        data => { template => '<a href="<[field.value]>"><[field.value]></a>' },
    ),
);

my %MODULE_SETTINGS = (
    name                          => $INTERFACE_ID,
    label                         => 'StUF-ZKN',
    interface_config              => \@INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_multiple                   => 1,
    is_manual                     => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    attribute_list                => [],
    retry_on_error                => 0,
    trigger_definition            => {},
);

has uri_values => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    default => sub {
        {
            interface_test_url => '/stuf/simulator',
        }
    },
);

has services_uri_values => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    default => sub {
        {
            interface_api_url  => '',
        }
    },
);

=head2 BUILDARGS

Configures this interface module (configuration form fields, etc.).

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%MODULE_SETTINGS);
};

my $STUF_ZKN_NS = 'http://www.egem.nl/StUF/sector/zkn/0310';

my %DISPATCH_TABLE = (
    # Case calls
    "{$STUF_ZKN_NS}genereerZaakIdentificatie_Di02"     => 'generate_case_id',
    "{$STUF_ZKN_NS}zakLv01"                            => 'get_case_details',
    "{$STUF_ZKN_NS}zakLk01"                            => 'write_case',

    # Document calls
    "{$STUF_ZKN_NS}genereerDocumentIdentificatie_Di02" => 'generate_document_id',
    "{$STUF_ZKN_NS}edcLv01"                            => 'get_case_document',
    "{$STUF_ZKN_NS}edcLk01"                            => 'write_case_document',

    # Document calls, "locking"
    "{$STUF_ZKN_NS}geefZaakdocumentbewerken_Di02"      => 'get_case_document_lock',
    "{$STUF_ZKN_NS}updateZaakdocument_Di02"            => 'write_case_document_lock',
    "{$STUF_ZKN_NS}cancelCheckout_Di02"                => 'unlock_case_document',
);

my %DISPATCH_ERROR_TABLE = (
    "{$STUF_ZKN_NS}zakLv01" => 'Fo02',
);

sub _get_model {
    my ($self, $opts) = @_;

    my $interface = $opts->{interface};
    my $schema    = $opts->{schema};

    return Zaaksysteem::StUF::ZKN::0102::Model->new(
        schema    => $schema,
        interface => $interface,
        municipality_code =>
            $interface->get_interface_config->{municipality_code},
    );
}

sub _process_row_stufzkn_version_1_2 {
    my ($self, $soap_action, $record, $row, $transaction, $interface) = @_;

    my $model = $interface->model;

    if ($model->supports_soap_action($soap_action)) {
        try {
            $model->process_soap($soap_action, $record->input);
        }
        catch {
            $self->log->debug("$_");
        };
    }
    return;
}

sub _process_row {
    my $self = shift;
    my ($record, $row) = @_;

    my $transaction = $record->transaction_id;
    my $interface   = $transaction->interface_id;

    my $processor = Zaaksysteem::StUF::0310::Processor->new(
        record           => $record,
        schema           => $record->result_source->schema,
        betrokkene_model => $record->result_source->schema->betrokkene_model,
        object_model     => Zaaksysteem::Object::Model->new(
            schema => $record->result_source->schema,
        ),
        interface => $interface,
        municipality_code =>
            $interface->get_interface_config->{municipality_code},
    );

    my ($xc, @node_names) = $processor->get_xpath_and_soap_action($record->input);
    my $soap_action = $node_names[0];

    my $response_xml;
    my ($code, $exception);
    my $func  = $DISPATCH_TABLE{$soap_action};

    if (!$func) {
        my $rv = $self->_process_row_stufzkn_version_1_2(
            $soap_action,
            $record,
            $row,
            $transaction,
            $interface
        );

        return if $rv;

        # Fall back to an unknown stuf zaken method error
        try{
            throw('stufzkn/action/unknown', "Unsupported SOAP action called '$soap_action'");
        }
        catch {
            $exception = $_;
        };
        $code  = 'StUF037';
    }
    else {
        try {
            $response_xml = $processor->$func($xc);
        }
        catch {
            $exception = $_;
            $self->log->info("$_");
        };
    };

    my $msg = $exception ? "Fout opgetreden bij verwerken van verzoek $soap_action" : "Verzoek $soap_action afgehandeld";

    if ($exception) {
        my $type = $DISPATCH_ERROR_TABLE{ $soap_action };
        my %stuurgegevens = $processor->parse_stuurgegevens($xc, $xc->findnodes('ZKN:stuurgegevens'));

        $response_xml = $processor->generate_error(
            type          => $type // 'Fo03',
            exception     => $exception,
            stuurgegevens => \%stuurgegevens,
            reference_id  => $record->id,
            $code ? (code => $code) : (),
        );
        $record->update({is_error => 1});
        $transaction->update({error_fatal => 1, error_count => 1});
    };

    $self->set_record_output($record, $response_xml, $msg);
    return 1;
}


=head2 soap_error

Throw a correct SOAP error

=cut

sub soap_error {
    my ($self, $record) = @_;
    return $record->output;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, 2016, 2018 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
