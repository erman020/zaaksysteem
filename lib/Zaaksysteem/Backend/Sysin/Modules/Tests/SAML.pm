package Zaaksysteem::Backend::Sysin::Modules::Tests::SAML;

use Moose::Role;

use BTTW::Tools;
use Zaaksysteem::SAML2::SP;

sub test_sp {
    my $self = shift;
    my $interface = shift;

    # SP package must itself be sufficiently robust to handle
    # misconfigured interfaces right off the bat. This isn't a
    # unit testing suite ya'know
    my $sp = Zaaksysteem::SAML2::SP->new_from_interface(
        interface => $interface
    );
}

sub test_idp {
    my $self = shift;
    my $interface = shift;

    my $idp = Zaaksysteem::SAML2::IdP->new_from_interface(
        interface => $interface
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 test_idp

TODO: Fix the POD

=cut

=head2 test_sp

TODO: Fix the POD

=cut

