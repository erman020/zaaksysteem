package Zaaksysteem::Backend::Sysin::Modules::Roles::StUFPRSNPS;
use Moose::Role;

use Zaaksysteem::StUF;
use DateTime::Format::Strptime;
use BTTW::Tools;

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUF
    Zaaksysteem::BR::Subject::Queue::Person
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::STUFPRSNPS - STUFPRSNPS engine for StUF PRS/NPS related queries

=head1 SYNOPSIS

    # See testfile:
    # t/170-sysin-modules-stufprs.t

=head1 DESCRIPTION

STUFPRS engine for StUF PRS related queries

=head1 ATTRIBUTES

=head2 pink

L<Zaaksysteem::XML::Compile::Instance> for Pink-specific SOAP calls. Should be
a L<Zaaksysteem::XML::Pink::Instance> object.

This

=cut

has pink => (
    'is'        => 'rw',
    'isa'       => 'Zaaksysteem::XML::Pink::Instance',
    'lazy'      => 1,
    'default'   => sub {
        return Zaaksysteem::XML::Compile->xml_compile->add_class('Zaaksysteem::XML::Pink::Instance')->pink;
    }
);

=head1 TRIGGERS

=head2 search($params)

=cut

sub may_search_brp {
    my $self = shift;
    my $interface = shift;

    if ($interface && $interface->name eq 'STUFCONFIG') {
        return $interface->module_object->can_search_sbus($interface);
    }
    return $self->config_interface->module_object->can_search_sbus($self->config_interface);
}

sub search_prs {
    my $self            = shift;
    my $params          = shift || {};
    my $interface       = shift;

    throw(
        'sysin/modules/stuf' . lc($self->stuf_object_type) . '/search_prs/no_params',
        'Cannot search with empty query parameters'
    ) unless scalar keys %{ $params };

    my $module_cfg      = $self->get_config_from_config_iface();
    my $config_module   = $self->config_interface;

    my %certs           = $self->_get_certificates($config_module);


    # Translate the name/key of any given search parameter to their internal equivalent
    my $converted_params;
    my @param_keys = keys %$params;

    for my $attribute (@{ $self->attribute_list }) {
        if (grep {$attribute->{internal_name}} @param_keys) {
            $params->{$attribute->{external_name}} = $params->{$attribute->{internal_name}};
            delete $params->{$attribute->{internal_name}};
        }
    }

    my $stufmsg     = Zaaksysteem::StUF->new(
        entiteittype    => $self->stuf_object_type,
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie => $module_cfg->{mk_ontvanger},
            },
        ),
        soap_ssl_crt => $certs{SSL_cert_file},
        soap_ssl_key => $certs{SSL_key_file},
    )->search(
        {
            reference_id            => 'T' . time(),
            date                    => DateTime->now(),
            %{ $params }
        }
    );

    $stufmsg->load_from_interface($self->config_interface);

    my $reply   = $stufmsg->dispatch;

    my @subjects;
    for (my $i = 0; $i < scalar(@{ $reply->body }); $i++) {
        my $params      = $reply->get_params_for_natuurlijk_persoon($i);
        my $adr_params  = $reply->get_params_for_natuurlijk_persoon_adres($i);

        my %params      = (%$params, %$adr_params);

        $params{$config_module->module_object->get_primary_key($config_module)} = $reply->as_params->[$i]->{$config_module->module_object->get_primary_key($config_module)};

        push(@subjects, \%params);
    }

    return \@subjects;
}

sub import_prs {
    my $self            = shift;
    my $params          = shift || {};
    my $interface       = shift;

    my $config_module                       = $self->config_interface;

    throw(
        'sysin/modules/stuf' . lc($self->stuf_object_type) . '/import_prs/no_params',
        'Cannot import with no param: sleutelGegevensbeheer'
    ) unless $params->{$config_module->get_primary_key};

    my $module_cfg                  = $self->get_config_from_config_iface;

    my %certs  = $self->_get_certificates($config_module);

    my $stufmsg     = Zaaksysteem::StUF->new(
        entiteittype    => $self->stuf_object_type,
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie          => $module_cfg->{mk_ontvanger},
            },
        ),
        soap_ssl_crt => $certs{SSL_cert_file},
        soap_ssl_key => $certs{SSL_key_file},
    )->search(
        {
            reference_id            => 'T' . time(),
            date                    => DateTime->now(),
            $config_module->module_object->get_primary_key($config_module)   => $params->{$config_module->module_object->get_primary_key($config_module)},
        }
    );

    $stufmsg->load_from_interface($self->config_interface);

    my $reply       = $stufmsg->dispatch;

    $interface->process(
        {
            external_transaction_id => 'unknown',
            input_data              => $reply->parser->xml,
            processor_params        => {
                processor               => '_process_import_entry',
                $config_module->module_object->get_primary_key($config_module)   => $params->{$config_module->module_object->get_primary_key($config_module)}
            },
            direct                  => 1,
        }
    );
}

sub _process_import_entry {
    my $self                        = shift;
    my ($record,$object)            = @_;

    my $params                      = $record
                                    ->transaction_id
                                    ->get_processor_params();

    my $module_cfg                  = $self->get_config_from_config_iface;
    my $config_module               = $self->config_interface;

    my %certs           = $self->_get_certificates($config_module);

    my $stufmsg                     = Zaaksysteem::StUF->new(
        entiteittype    => $self->stuf_object_type,
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie          => $module_cfg->{mk_ontvanger},
            },
            kennisgeving => {
                mutatiesoort      => 'T',
                indicatorOvername => 'I',
            }
        ),
        soap_ssl_crt => $certs{SSL_cert_file},
        soap_ssl_key => $certs{SSL_key_file},
    )->set_afnemerindicatie(
        {
            reference_id            => $record->get_column('transaction_id'),
            date                    => $record->transaction_id->date_created,
            $config_module->module_object->get_primary_key($config_module)   => $params->{$config_module->module_object->get_primary_key($config_module)}
        }
    );

    $stufmsg->load_from_interface( $self->config_interface );


    eval {
        my $reply_object = $stufmsg->dispatch;
    };

    if ($@) {
        if (eval { $@->type eq 'stuf/soap/no_answer' }) {
            throw(
                $@->type,
                "\nResponse:\n" .
                $@->object->response->content
            );
        }
        die ($@);
    }

    my $stuf_prs    = Zaaksysteem::StUF->from_xml(
        $record->transaction_id->input_data,
        {
            cache => ($record->result_source->schema->default_resultset_attributes->{'stuf_cache'} ||= {})
        }
    );

    my $entry       = $self->stuf_create_entry(
        $record, $stuf_prs
    );

    $self->_add_subscription_for_entry(
        $record, $stufmsg, $entry
    );
}



=head1 PROCESSORS

=head2 CREATE SUBJECT

=head2 $module->stuf_create_entry($transaction_record, $rowobject)

Return value: $ROW_NATUURLIJK_PERSOON

Creates a new L<Zaaksysteem::DB::Component::NatuurlijkPersoon> into our database,
and sets a subscription between our data record and theirs via C<ObjectSubscription>

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=item $rowobject

The source of the data, in case of a CSV this would proba

=back

=cut

sub _assert_gemeentecode {
    my $self                        = shift;
    my $record                      = shift;
    my $object                      = shift;
    my $gemeentecode                = shift;

    my $stufversion                 = $self->_get_stuf_version($record->transaction_id->interface_id);
    if ($stufversion eq '0204') {
        if (!$self->get_config_from_config_iface()->{gemeentecode}) {
            $self->stuf_throw(
                $object,
                'sysin/modules/stufprs/stuf_create_entry/no_gemeente_code_set',
                'No gemeentecode found in configuration, please set it'
            );
        }

        if (!$gemeentecode) {
            $self->stuf_throw(
                $object,
                'sysin/modules/stufprs/stuf_create_entry/no_gemeente_code',
                'No gemeentecode found in StUF message, invalid XML'
            );
        }
    }
}

sub stuf_create_entry {
    my $self                = shift;
    my ($record, $object)   = @_;  

    my $natuurlijk_persoon  = $self->_create_subject_natuurlijk_persoon(@_);

    if ($self->got_address(@_)) {
        my $adres               = $self->_create_subject_adres(@_, $natuurlijk_persoon);

        $self->_assert_gemeentecode($record, $object, $adres->gemeente_code)
            if (!$adres->landcode || $adres->landcode == 6030);

        if ($self->_binnen_gemeente($record, $object, $adres)) {
            $natuurlijk_persoon->in_gemeente(1);
        }

        $natuurlijk_persoon->adres_id($adres->id);
        $natuurlijk_persoon->update;
    }

    $record->preview_string(
        "Toegevoegd: " .
        join(', ', grep {defined $_} ($natuurlijk_persoon->burgerservicenummer, $natuurlijk_persoon->voornamen, $natuurlijk_persoon->naamgebruik))
    );

    return $natuurlijk_persoon;
}

=head2 got_address

    if ($stuf->got_address($record, $object)) {
        print "Got a valid address!";
   
    }

B<Return value>: true on success

Returns a true value when subject got an address (either vbl or cor)

=cut

sub got_address {
    my $self                = shift;
    my ($record, $object, $natuurlijk_persoon)   = @_;

    my ($coradres, $vbladres) = $self->_get_cor_vbl_adres(@_, $natuurlijk_persoon);

    if ($self->_has_address($coradres) || $self->_has_address($vbladres)) {
        return 1;
    }

    return;
}

sub _create_subject_natuurlijk_persoon {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $subject_params      = $object->get_params_for_natuurlijk_persoon;

    my $np_rs               = $record
                            ->result_source
                            ->schema
                            ->resultset('NatuurlijkPersoon');

    $subject_params      = {
        map(
            { $_ => $subject_params->{$_} }
            grep (
                { exists $subject_params->{ $_ } }
                $record->result_source->schema->resultset('NatuurlijkPersoon')->result_source->columns
            )
        )
    };

    $self->_update_subject_params($subject_params);

    ### BACKWARDS compatability
    my $entry               = $np_rs->create(
        {
            %{ $subject_params },
            authenticated       => 1,
            authenticatedby     => 'gba'
        }
    );

    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => 'NatuurlijkPersoon',
                                table_id    => $entry->id,
                                create      => 1
                            );

    $mutation_record->from_dbix($entry);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    return $entry;
}

sub _create_subject_adres {
    my $self                = shift;
    my ($record, $object, $natuurlijk_persoon)   = @_;

    my $address_params      = $object->get_params_for_natuurlijk_persoon_adres;

    ### We need to fix subject in ZS real soon...
    my ($vbladres, $coradres);

    $coradres = { map({ my $adreskey = $_; $adreskey =~ s/^correspondentie_//; $adreskey => $address_params->{$_} } grep ( { $_ =~ /^correspondentie_/ } keys %{ $address_params })) };
    $coradres->{functie_adres} = 'B';

    $vbladres = { map({ $_ => $address_params->{$_} } grep ( { $_ !~ /^correspondentie_/ } keys %{ $address_params })) };
    $vbladres->{functie_adres} = 'W';

    if ($vbladres->{straatnaam} || $vbladres->{adres_buitenland1}) {
        my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                                ->new(
                                    table       => 'Adres',
                                    create      => 1
                                );

        $natuurlijk_persoon->add_address($vbladres, {mutation_record => $mutation_record});

        push(
            @{ $self->process_stash->{row}->{mutations} },
            $mutation_record
        );
    }

    if ($coradres->{straatnaam} || $coradres->{adres_buitenland1}) {
        my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                                ->new(
                                    table       => 'Adres',
                                    create      => 1
                                );

        $natuurlijk_persoon->add_address($coradres, {mutation_record => $mutation_record});

        push(
            @{ $self->process_stash->{row}->{mutations} },
            $mutation_record
        );
    }

    return $natuurlijk_persoon->adres_id;
}

=head2 UPDATE SUBJECT

=head2 $module->stuf_update_entry($record, $object)

Updates a PRS entry in our database

=cut


sub stuf_update_entry {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $natuurlijk_persoon  = $self->get_entry_from_subscription(@_, 'NatuurlijkPersoon');

    $self->_update_subject_natuurlijk_persoon(
        @_, $natuurlijk_persoon
    ) or return $natuurlijk_persoon;        # Return on undef, empty person probably

    my ($coradres, $vbladres) = $self->_get_cor_vbl_adres(@_, $natuurlijk_persoon);

    my $updated_components  = $object->updated_components();

    my ($has_moved);
    if (
        (
            $updated_components->{verblijfsadres} ||            ### There are updates given for these components, else
            $updated_components->{correspondentie}              ### we should not even look at the address
        ) &&
        (
            !$self->_has_address($coradres) && !$self->_has_address($vbladres)
        )
    ) {
        ### No addresses. Maybe this person has moved to the "outside"

        if (
            $natuurlijk_persoon->verblijfsadres &&
            $natuurlijk_persoon->verblijfsadres->gemeente_code
        ) {
            if ($vbladres && $vbladres->{gemeente_code} && $vbladres->{gemeente_code} ne $natuurlijk_persoon->verblijfsadres->gemeente_code) {
                $has_moved = 1;
            }
        }
    }

    ### Update address
    $self->_update_subject_adres(@_, $natuurlijk_persoon);

    $record->preview_string(
        "Bijgewerkt: " .
        join(', ', ($natuurlijk_persoon->burgerservicenummer, $natuurlijk_persoon->voornamen, $natuurlijk_persoon->naamgebruik))
    );

    my $subject_params      = $object->get_params_for_natuurlijk_persoon;

    my $module_cfg                  = $self->get_config_from_config_iface();
    my $config_module               = $self->config_interface;

    my $object_params = $object->as_params;
    my $object_subscription = $natuurlijk_persoon->subscription_id;

    if ($natuurlijk_persoon->deleted_on || !$natuurlijk_persoon->active) {

        if (my $adres = $natuurlijk_persoon->adres_id) {
            $adres->update({deleted_on => undef});
        }
        else {
            $self->log->warn(sprintf("NatuurlijkPersoon %s heeft geen adres!", $natuurlijk_persoon->id));
        }

        if ($object_subscription) {
            $object_subscription->update({date_deleted => undef});
        }
        else {
            $self->log->warn(sprintf("NatuurlijkPersoon %s heeft geen object subscription!", $natuurlijk_persoon->id));
        }

        $natuurlijk_persoon->update({deleted_on => undef, active => 1});
    }

    if ($has_moved) {
        # Send out a disable subscription queue item, picked up by
        # another process
        if ($object_subscription) {
            $object_subscription->object_subscription_delete;
        }
        else {
            $self->log->warn(sprintf("NatuurlijkPersoon %s heeft geen object subscription!", $natuurlijk_persoon->id));
        }
    }

    ### Special CASE: Verhuizen
    if ($natuurlijk_persoon->verblijfsadres) {
        if ($self->_binnen_gemeente($record, $object, $natuurlijk_persoon->verblijfsadres)) {
            if (!$natuurlijk_persoon->in_gemeente) {
                $natuurlijk_persoon->in_gemeente(1);
                $natuurlijk_persoon->update;
            }
        } else {
            $natuurlijk_persoon->in_gemeente(0);
            $natuurlijk_persoon->update;
        }
    } else {
        $natuurlijk_persoon->in_gemeente(0);
        $natuurlijk_persoon->update;
    }


    $self->run_post_triggers($natuurlijk_persoon);

    return $natuurlijk_persoon;
}

sub _fix_date {
    my $self = shift;
    my $date = shift;

    my $dtf = DateTime::Format::Strptime->new(pattern => '%Y%m%d');
    return $dtf->parse_datetime($date);
}

sub _update_subject_params {
    my ($self, $subject) = @_;

    $subject->{burgerservicenummer} = int($subject->{burgerservicenummer}) if exists $subject->{burgerservicenummer};

    if (my $end = $subject->{datum_overlijden}) {
        if (ref $end eq '') {
            $subject->{datum_overlijden} = $self->_fix_date($end);
        }
    }
}

sub _update_subject_natuurlijk_persoon {
    my $self                                = shift;
    my ($record, $object, $entry)           = @_;

    my $subject_params      = $object->get_params_for_natuurlijk_persoon;
    my %old_values          = $entry->get_columns;

    $self->_update_subject_params($subject_params);

    eval { $entry->update($subject_params)->discard_changes };
    if ($@)  {
        warn $@;
        $self->stuf_throw(
            $object,
            'sysin/modules/stufprs/process/np_update_error',
            "Impossible to update NatuurlijkPersoon in our database: $@"
        );
    }

    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => 'NatuurlijkPersoon',
                                table_id    => $entry->id,
                                update      => 1
                            );

    ### Log mutation
    $mutation_record->from_dbix($entry, \%old_values);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    return $entry;
}

sub _get_cor_vbl_adres {
    my $self                                        = shift;
    my ($record, $object, $natuurlijk_persoon)      = @_;

    my $address_params      = $object->get_params_for_natuurlijk_persoon_adres;

    my $coradres = { map({ my $adreskey = $_; $adreskey =~ s/^correspondentie_//; $adreskey => $address_params->{$_} } grep ( { $_ =~ /^correspondentie_/ } keys %{ $address_params })) };
    $coradres->{functie_adres} = 'B';

    my $vbladres = { map({ $_ => $address_params->{$_} } grep ( { $_ !~ /^correspondentie_/ } keys %{ $address_params })) };
    $vbladres->{functie_adres} = 'W';

    return ($coradres, $vbladres);
}

sub _has_address {
    my $self                                = shift;
    my $address                             = shift;

    if ($address->{straatnaam} || $address->{adres_buitenland1}) {
        return 1;
    }

    return;
}

sub _update_subject_adres {
    my $self                                = shift;
    my ($record, $object, $natuurlijk_persoon)    = @_;

    my $updated_components      = $object->updated_components();

    return unless (
        $updated_components->{verblijfsadres} ||
        $updated_components->{correspondentie} ||
        $updated_components->{buitenland} 
    );

    my ($coradres, $vbladres)   = $self->_get_cor_vbl_adres(@_);
    if ($updated_components->{correspondentie}) {
        my ($coradres, $vbladres) = $self->_get_cor_vbl_adres(@_);

        if ($self->_has_address($coradres)) {
            $self->_update_subject_adres_by_type($natuurlijk_persoon,$coradres,$object);
        } else {
            $natuurlijk_persoon->delete_address_by_function('B');
        }
    }

    if ($updated_components->{verblijfsadres} || $updated_components->{buitenland}) {
        if ($self->_has_address($vbladres)) {
            $self->_update_subject_adres_by_type($natuurlijk_persoon,$vbladres,$object);

            $self->_assert_gemeentecode($record, $object, $vbladres->{gemeente_code})
                if (!$vbladres->{landcode} || $vbladres->{landcode} == 6030);
        } else {
            $natuurlijk_persoon->delete_address_by_function('W');
        }
    }

    $natuurlijk_persoon->update_address_links;

    return $natuurlijk_persoon->adres_id;
}

sub _update_subject_adres_by_type {
    my $self        = shift;
    my $np          = shift;
    my $params      = shift;
    my $object      = shift;
    my ($current,$mutation_record);

    ### Get old address
    if (uc($params->{functie_adres}) eq 'B') {
        $current = $np->correspondentieadres;
    } else {
        $current = $np->verblijfsadres;
    }

    if ($current) {
        my %old_values          = $current->get_columns;

        unless ($current->update($params)->discard_changes) {
            $self->stuf_throw(
                $object,
                'sysin/modules/stufprs/process/adres_update_error',
                'Impossible to update Adres in our database, unknown error'
            );
        }

        $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation->new(
            table       => 'Adres',
            table_id    => $current->id,
            update      => 1
        );

        $mutation_record->from_dbix($current, \%old_values);
    } else {
        $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                                ->new(
                                    table       => 'Adres',
                                    create      => 1
                                );

        $np->add_address($params, {mutation_record => $mutation_record});
    }

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );
}

=head2 DELETE SUBJECT

=head2 stuf_delete_entry

=cut

sub stuf_delete_entry {
    my $self                                = shift;
    my ($record, $object, $subscription)    = @_;


    my $natuurlijk_persoon                  = $self->get_entry_from_subscription(
                                                $record,
                                                $object,
                                                'NatuurlijkPersoon',
                                                $subscription
                                            );

    $record->preview_string(
        "Verwijderd: " .
        join(', ', ($natuurlijk_persoon->burgerservicenummer, $natuurlijk_persoon->voornamen, $natuurlijk_persoon->naamgebruik))
    );

    $natuurlijk_persoon->disable_natuurlijk_persoon;

    $self->_remove_subscription_from_entry($record, $object, $natuurlijk_persoon);
}

=head2 enable_object_subscription

    $module->enable_object_subscription(
        { subscription_id => $id },
        $interface
    );

=cut

sub enable_object_subscription {
    my $self            = shift;
    my $params          = shift;
    my $interface       = shift;

    my $subscription = $self->_get_object_subscription_by_id(
        $interface->result_source->schema,
        $params->{subscription_id}
    );

    $interface->process(
        {
            direction               => 'outgoing',
            external_transaction_id => $subscription->external_id,
            input_data              => sprintf(
                'Enable subscription for bsn %s [%d / %s]',
                $params->{bsn} // '',
                $subscription->local_id, $subscription->external_id
            ),
            processor_params => {
                processor       => '_process_enable_object_subscription',
                subscription_id => $subscription->id,
                external_id     => $subscription->external_id,
                bsn             => $params->{bsn} // '',
                a_nummer        => $params->{a_nummer} // '',
            },
        }
    );
}

=head2 _via_gbav

Send via GBA-V. Business logic where the request should be send to a seperate GBA-V box. Must meet the condition:

If StUF supplier is C<pink> and C<search in GBA-V is allowed> and C<the subscription ID is not set> this function returns a true-ish value, otherwise 0.

Have a look at c<import_nps> in L<Zaaksysteem::Backend::Sysin::Modules::STUFPRS>

=cut

sub _via_gbav {
    my ($self, $subscription) = @_;

    if (   $subscription->external_id eq ''
            || $subscription->external_id eq 'IN_PROGRESS'
    ) {
        my $config_iface = $self->config_interface;
        my $module_cfg   = $config_iface->get_interface_config;

        if (   $module_cfg->{stuf_supplier} =~ /^pink(v3)?$/
            && $config_iface->module_object->can_search_gbav($config_iface)
        ) {
            return 1;
        }
    }

    return 0;
}

sub _process_set_pink_voa {
    my $self                = shift;
    my $record              = shift;

    my $transaction         = $self->process_stash->{transaction};
    my $interface           = $transaction->interface_id;

    my $processor_params    = $transaction->get_processor_params();
    my $params              = $processor_params->{params};

    my $call_options        = $self->_load_call_options_from_config(
        $transaction->interface_id,
        {
            reference           => $transaction->id,
            datetime            => DateTime->now(),
            follow_subscription => 1,
            calltype            => 'pink_gbav'
        }
    );

    ### GBA-V Handling
    my ($rv, $trace) = $self->pink->set_pink_voa(
        $params,
        $call_options
    );

    if (!$rv) {
        throw(
            'sysin/modules/stuf' . lc($self->stuf_object_type) . '/set_pink_voa/invalid_response',
            sprintf(
                "Failure in calling Pink VOA service '%s': %s\nrequest: %s\nresponse:\n%s",
                $call_options->{dispatch}{endpoint},
                ($trace && $trace->request)
                    ? $trace->request->decoded_content
                    : '(no request)',
                ($trace && $trace->response)
                    ? $trace->response->decoded_content
                    : '(no response)',
            ),
            { fatal => 1 }
        );
    }

    $transaction->external_transaction_id($rv->{referentie});

    $record->input($trace->request->content);
    $record->output($trace->response->content);
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 import_prs

TODO: Fix the POD

=cut

=head2 search_prs

TODO: Fix the POD

=cut

