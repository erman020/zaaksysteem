package Zaaksysteem::Backend::Sysin::Modules::Zorginstituut;
use Moose;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Zorginstituut - Interface for Zorginstituut iWMO/iJW messages

=head1 DESCRIPTION

An interface module that handles the parts of the logic for sending and receiving iWMO/iJW XML messages
from and to external providers such as Acknownledge and C2GO.

Uses the following plugin structure for sending and receiving message:

    L<Zaaksysteem::Zorginstituut::Plugins>

=cut

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw(
    Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPAPI
    Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPGeneric
    Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate
);

use List::Util qw(first);
use Module::Pluggable::Object;
use IO::All;

use Zaaksysteem::Constants qw(RGBZ_GEMEENTECODES LOGGING_COMPONENT_ZAAK);
use Zaaksysteem::Constants::Users qw(:all);
use BTTW::Tools;
use Zaaksysteem::Types qw/UUID/;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;
use MIME::Base64 qw(decode_base64);

my @MODELS = Module::Pluggable::Object->new(
    search_path => 'Zaaksysteem::Zorginstituut::Model',
    min_depth   => 1,
    require     => 1,
    except      => qr/^Zaaksysteem::Zorginstituut::Model::[0-9]+$/,
)->plugins;

my @PROVIDERS = Module::Pluggable::Object->new(
    search_path => 'Zaaksysteem::Zorginstituut::Provider',
    require     => 1.
)->plugins;


=head1 interface properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

# Directly presented in sysin-ui.
my $interface_description = q{
<p>
  Via deze koppeling wordt de gemeente in staat gesteld om <a
  href="https://www.istandaarden.nl/istandaarden/iwmo">WMO</a> en <a
  href="https://www.istandaarden.nl/istandaarden/ijw">JW</a> berichten
  te versturen. Deze koppeling maakt gebruik van de volgende
  standaarden: <a href="https://modellen.istandaarden.nl/wmo/iwmo2_1/index.php/Berichten">
  WMO 2.1 </a>, <a href="https://modellen.istandaarden.nl/jw/ijw2_1/index.php/Berichten">
  JW 2.1 </a> en <a href="http://www.gemmaonline.nl/index.php/Documentatie_StUF-koppelvlak_iWmo-iJw">
  StUF </a>
</p>
<p>
  Om deze koppeling te configureren heeft u specifieke informatie van
  een aanbieder nodig.  Het Di01 endpoint is de URI waarbij zogenaamde
  "heenberichten" opgestuurd dienen te worden, denk hierbij aan de
  WMO301 en JW301 berichten.
  <!-- Deze gaat van belang zijn bij WMO305/307 berichten
  Het Du01 endpoint veld is de URI waarbij genaamde
  "retourberichten" opgestuurd dienen te worden, denk hierbij aan de
  WMO302 en JW302 berichten. -->
  <br/>
  <br/>
  Voor meer informatie kunt u terecht op de
  <a href="http://wiki.zaaksysteem.nl/wiki/Redirect_Zorginstituut">
    Zaaksysteem Wiki
  </a>
</p>
};

my $MODULE_SETTINGS = {
    name             => 'zorginstituut',
    label            => 'iWMO en iJW',
    description      => $interface_description,
    interface_config => [
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_gemeentecode',
            type     => 'select',
            label    => 'Gemeentecode',
            required => 1,
            data     => {
                options => [
                    sort(
                        { $a->{label} cmp $b->{label} } map({ { value => $_, label => RGBZ_GEMEENTECODES()->{$_} } } keys %{ RGBZ_GEMEENTECODES() })),
                    { value => 0,    label => 'Ongedefinieerd' },
                    { value => 9999, label => 'Test' }
                ],
            },
        ),

        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_berichtcode',
            type        => 'select',
            label       => 'Type bericht',
            required    => 1,
            data => {
                options => [
                    sort {
                        $a->{code} cmp $b->{code}
                    } map {
                        {
                            value => $_->shortname,
                            label => $_->label,
                            code  => $_->code,
                        }
                    } @MODELS
                ],
            },
        ),

        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_provider',
            type        => 'select',
            label       => 'Aanbieder',
            required    => 1,
            data => {
                options => [
                    sort {
                        $a->{value} cmp $b->{value}
                    } map {
                        {
                            value => $_->shortname,
                            label => $_->label,
                        }
                    } @PROVIDERS
                ],
            },
        ),
        (
            # Gather together the configuration items for each available
            # appointment provider plugin.
            map {
                my @config_items = $_->configuration_items;

                # Only show these config items if the plugin they're from is selected.
                for my $ci (@config_items) {
                    if ($ci->when) {
                        $ci->when(sprintf('interface_provider == "%s" && (%s)', $_->shortname, $ci->when));
                    } else {
                        $ci->when(sprintf('interface_provider == "%s"', $_->shortname));
                    }
                }
                @config_items;
            } @PROVIDERS
        ),

        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_timeout',
            type        => 'text',
            label       => 'Timeout',
            default     => '15',
            required    => 1,
            description => 'Timeout',
            data        => { pattern => '^\d+$' },
        ),
    ],
    direction                     => 'outgoing',
    retry_on_error                => 0,
    manual_type                   => ['text'],
    module_type                   => [ 'soapapi' ],
    is_multiple                   => 1,
    is_manual                     => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 1,
    attribute_list                => [
        map { { external_name => $_, attribute_type => 'magic_string', all_casetypes => 1} }
            qw(
            aanbieder

            beschikkingsnummer
            beschikkingsingangsdatum
            beschikkingsafgiftedatum
            beschikkingseinddatum
            stop_beschikkingseinddatum
            stop_reden_intrekking_algemeen

            toegewezen_product_code
            toegewezen_product_categorie
            toegewezen_product_omvang_eenheid
            toegewezen_product_omvang_frequentie
            toegewezen_product_omvang_volume
            toegewezen_product_toewijzingsdatum
            toegewezen_product_ingangsdatum
            toegewezen_product_einddatum
            stop_toegewezen_product_einddatum
            toegewezen_product_reden_intrekking
            stop_toegewezen_product_reden_intrekking

            toegewezen_product_code_1
            toegewezen_product_categorie_1
            toegewezen_product_omvang_eenheid_1
            toegewezen_product_omvang_frequentie_1
            toegewezen_product_omvang_volume_1
            toegewezen_product_toewijzingsdatum_1
            toegewezen_product_ingangsdatum_1
            toegewezen_product_einddatum_1
            stop_toegewezen_product_einddatum_1
            toegewezen_product_reden_intrekking_1
            stop_toegewezen_product_reden_intrekking_1

            toegewezen_product_code_2
            toegewezen_product_categorie_2
            toegewezen_product_omvang_eenheid_2
            toegewezen_product_omvang_frequentie_2
            toegewezen_product_omvang_volume_2
            toegewezen_product_toewijzingsdatum_2
            toegewezen_product_ingangsdatum_2
            toegewezen_product_einddatum_2
            stop_toegewezen_product_einddatum_2
            toegewezen_product_reden_intrekking_2
            stop_toegewezen_product_reden_intrekking_2

            toegewezen_product_code_3
            toegewezen_product_categorie_3
            toegewezen_product_omvang_eenheid_3
            toegewezen_product_omvang_frequentie_3
            toegewezen_product_omvang_volume_3
            toegewezen_product_toewijzingsdatum_3
            toegewezen_product_ingangsdatum_3
            toegewezen_product_einddatum_3
            stop_toegewezen_product_einddatum_3
            toegewezen_product_reden_intrekking_3
            stop_toegewezen_product_reden_intrekking_3
        ),
    ],
    trigger_definition => {
        PostStatusUpdate => { method => 'post_status_update', },
    },
};

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{$MODULE_SETTINGS});
};

=head1 METHODS

=head2 is_valid_soap_api

Returns true when this interface is a valid interface for this soap action.

=cut

define_profile is_valid_soap_api => (
    required    => {
        xml         => 'Str',
        soapaction  => 'Str',
        interface   => 'Zaaksysteem::Backend::Sysin::Interface::Component',
    },
);

sub is_valid_soap_api {
    my $self   = shift;
    my $params = assert_profile({@_})->valid;

    my $model = $params->{interface}->model;

    return unless $model->supports_soap_action($params->{soapaction});
    return $model->can_process_du01($params->{xml});
}

define_profile check_client_certificate => (
    required => {
        interface   => 'Defined',
    },
    optional => {
        client_s_dn => 'Str',
        client_cert => 'Str',
    },
    defaults => {
        client_s_dn => sub {''},
        client_cert => sub {''},
    },
);

sub check_client_certificate {
    my $self   = shift;
    my $params = assert_profile({@_})->valid;
    my $interface = $params->{interface};

    my $model = $params->{interface}->model;
    my $provider = $model->provider;

    if ($provider->has_client_public_key) {

        if (!$params->{client_s_dn} && !$params->{client_cert}) {
            $self->log->info("Unable to check client certificate, none provided");
            return 0;
        }

        my $client_cert = io->catfile($provider->client_public_key)->slurp();

        return try {
            $interface->module_object->assert_client_certificate(
                dn          => $params->{client_s_dn},
                client_cert => decode_base64($params->{client_cert}),
                stored_cert => $client_cert,
            );
        } catch {
            # We don't log the error because this function is called
            # from the /api/soap endpoint and what we call logs more
            # than enough.
            return 0;
        };
    }
    elsif ($params->{client_s_dn} || $params->{client_cert}) {
        # If someone sends us a certificate and we don't have anything
        # configured we assume that there is another interface that may
        # have clients certs configured. Therefore the check will fail.
        # This logic may fail if we ever are going to support checking
        # a client cert against a CA.. But we might want to check
        # against a DN of some sort.
        $self->log->info("Client certificate provided but interface not configured");
        return 0;
    }
    return 1;
}

sub soap_error {
    my ($self, $record) = @_;

    my $transaction = $record->transaction_id;
    my $interface   = $transaction->interface;
    my $model       = $interface->model;

    my $du01 = $model->get_stuf_du01($record->input);

    return $model->stuf_envelope(
        'ggk0210-fo01',

        gemeentecode    => $model->municipality_code,
        agb_code        => $du01->findvalue('//s:zender/s:organisatie'),
        date            => DateTime->now(),
        our_reference   => $transaction->uuid,
        their_reference => $du01->findvalue('//s:referentienummer'),
        error_code      => 'IB216',
        error_side      => 'client',
        error           => $record->output,
    );
}

sub _get_filestore_from_config_item {
    my ($self, $schema, $item) = @_;

    return unless $item;
    my $file = $schema->resultset('Filestore')->find($item->[0]{id});
    return unless $file;
    return $file->get_path;
}

sub _get_model {
    my ($self, $opts) = @_;

    my $interface = $opts->{interface};
    my $schema    = $opts->{schema};
    my $config    = $interface->get_interface_config;

    my $code      = $config->{berichtcode};
    my $aanbieder = $config->{provider};

    my $plugin          = first { $code eq $_->shortname } @MODELS;
    my $provider_plugin = first { $aanbieder eq $_->shortname } @PROVIDERS;

    my $qr = qr/^${aanbieder}_(.+)$/;

    my %provider;

    foreach my $item (keys %{$config}) {
        if ($item =~ $qr) {
            $provider{$1} = $config->{$item} if $config->{$item};
        }
    }
    if (!$provider{ca_certificate_use_system}) {
        $provider{ca_certificate} = $self->_get_filestore_from_config_item($schema, $provider{ca_certificate});
        delete $provider{ca_certificate} unless $provider{ca_certificate};
    }

    my @cert_keys = qw(client_public_key client_private_key);
    if (!$provider{use_client_certificates}) {
        delete $provider{$_} foreach (@cert_keys);
    }
    else {
        foreach (@cert_keys) {
            $provider{$_} = $self->_get_filestore_from_config_item($schema, $provider{$_});
            unless ($provider{$_}) {
                throw('zorginstituut/client_certificates/configuration_error',
                    "No $_ configured while client certificates are selected"
                );
            }
        }
        delete $provider{use_client_certificates};
    }

    my $provider = $provider_plugin->new(%provider, timeout => $config->{timeout});

    my $model = $plugin->new(
        schema            => $schema,
        interface         => $interface,
        municipality_code => $config->{gemeentecode},
        provider          => $provider,
    );

    return $model;
}

sub _set_record_output {
    my ($self, $record, $output) = @_;

    if (ref $output eq 'HASH') {
        my $msg = $output->{errors} ? "Fout opgetreden bij %s (%s) voor zaak %s" : "%s (%s) is succesvol verstuurd voor zaak %s";
        $self->set_record_output($record, $output, sprintf($msg, uc($output->{type}), $output->{code}, $output->{case_id}));
    }
    else {
        $self->set_record_output($record, $output, "Fout opgetreden: $output",);
    }

}

=head2 _process_row

Process an incoming SOAP message

=cut

sub _process_row {
    my $self = shift;
    my ($record, $row) = @_;

    my $transaction = $record->transaction_id;
    my $interface   = $transaction->interface;
    my $schema      = $interface->result_source->schema;

    my $model = $interface->model;

    $transaction->direction('incoming');
    $transaction->update;

    try {

        my $is_valid = $model->process_soap($record->input);

        my $du01 = $model->get_stuf_du01($record->input);
        my $case = $model->get_case($du01);

        my $xml = $model->stuf_envelope(
            'ggk0210-bv03',

            agb_code        => $du01->findvalue('//s:zender/s:organisatie'),
            date            => DateTime->now(),
            our_reference   => $transaction->uuid,
            their_reference => $du01->findvalue('//s:referentienummer'),
            gemeentecode    => $model->municipality_code,
        );

        $record->output($xml);
        my $msg = $is_valid ? sprintf("%s succesvol verwerkt voor zaak %d", $model->answer_type, $case->id) :
            sprintf("%s succesvol verwerkt (met fouten) voor zaak %d", $model->answer_type, $case->id);
        $record->preview_string($msg);

    }
    catch {
        $self->catch_error($record, $_);
    };
    return 1;

}

sub _post_status_update {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();

    my $interface = $transaction->interface;

    $self->log_params($params);

    foreach (qw(base_url message processor statusCode statusText kenmerken)) {
        delete $params->{$_} if exists $params->{$_};
    }

    my $schema = $interface->result_source->schema;
    my $case   = $schema->resultset('Zaak')->find($params->{case_id});
    my $model  = $interface->model;

    if ($model->type eq 'WMO301' || $model->type eq 'JW301') {
        try {
            my $rv = $model->send_301(
                case   => $case,
                record => $record,
            );

            $self->set_record_output($record, $rv, sprintf('%s bericht(en) verstuurd voor zaak %d', $model->type, $case->id));
        }
        catch {
            $self->catch_error(
                $record, $_,
                sprintf(
                    '%s bericht(en) niet verstuurd voor zaak %d',
                    $model->type, $case->id
                )
            );
        };
    }

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
