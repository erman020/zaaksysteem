package Zaaksysteem::Backend::Sysin::Modules::EmailIntake;
use Moose;

use Mail::Track;
use Try::Tiny;
use BTTW::Tools;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;
use IO::All;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Roles::Email
/;


=head1 INTERFACE CONSTANTS

=head2 DOCUMENT_INTAKE

=head2 OLO

=head2 GENERIC

=head2 INTERFACE_ID

=head2 INTERFACE_CONFIG_FIELDS

=head2 MODULE_SETTINGS

=cut

use constant DOCUMENT_INTAKE => 100;
use constant OLO             => 200;
use constant GENERIC         => 300;

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID               => 'emailintake';
use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_user',
        type        => 'text',
        label       => 'E-mailadres',
        required    => 1,
        description => 'E-mailadres waar de mail naar verstuurd moet worden om geaccepteerd te worden',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_type',
        type        => 'select',
        label       => 'Type interface',
        data        => {
            options     => [
                {
                    value    => OLO,
                    label    => 'Online Omgevingsloket',
                },
                {
                    value    => DOCUMENT_INTAKE,
                    label    => 'Documentintake',
                },
                {
                    value    => GENERIC,
                    label    => 'Zaaktype',
                },
            ],
        },
        required    => 1,
        description => 'Het type e-mail interface: OLO, generiek, etc',
    ),
];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'Inkomende mailafhandeling',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text'],
    is_multiple                     => 1,
    is_manual                       => 0,
    allow_multiple_configurations   => 1,
    is_casetype_interface           => 1,
    has_attributes                  => 1,
    attribute_list                  => [
        {
            external_name   => 'subject',
            optional        => 1,
            attribute_type  => 'magic_string',
            from_casetype   => 1,
            GENERIC()       => 1,
        },
        {
            external_name   => 'from',
            attribute_type  => 'magic_string',
            optional        => 1,
            from_casetype   => 1,
            GENERIC()       => 1,
        },
        {
            external_name   => 'body',
            attribute_type  => 'magic_string',
            optional        => 1,
            from_casetype   => 1,
            GENERIC()       => 1,
        },
    ],
    retry_on_error                  => 1,
    trigger_definition  => {
        process_mail   => {
            method  => 'process_mail',
        },
    },
};

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 ATTRIBUTES

=head2 interface

=head2 exception

=head2 schema

=cut

has interface => (is => 'rw');
has exception => (is => 'rw');
has schema => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return if !$self->interface;
        return $self->interface->result_source->schema;
    },
    weak_ref => 1,
);

=head1 METHODS

=head2 process_mail

=cut

sub process_mail {
    my ($self, $params, $interface) = @_;

    $self->interface($interface);

    my $transaction = $interface->process({
            external_transaction_id => 'unknown',
            input_data              => 'mail',
            models                  => $self->get_process_stash('models'),
            processor_params        => {
                processor => '_process_mail',
                %$params,
            },
        },
    );

    if ($self->exception) {
        if (eval { $self->exception->isa('Throwable::Error') } ) {
            $self->exception->throw();
        }
        else {
            throw 'EmailIntake/process_mail', $self->exception . "";
        }
    }
    return $transaction;
}

=head2 get_attribute_mapping

Overwritten sub to get the correct attributes depending on the interface type (OLO, GENERIC, DOCUMENT_INTAKE)

=cut

sub get_attribute_mapping {
    my $self        = shift;
    my ($interface) = @_;
    my $config      = $self->next::method(@_);

    my $type = $interface->get_interface_config->{type};

    $config->{attributes} = [
        grep { exists($_->{$type}) && $_->{$type} }
        @{ $config->{attributes} }
    ];

    return $config;
}

sub _process_mail {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;
    my $config      = $interface->get_interface_config;

    my $model_stash = $self->get_process_stash('models') || {};

    my $models = assert_profile($model_stash, profile => {
        required => { zaak => 'Zaaksysteem::Zaken::Model' }
    })->valid;

    my $zaak_model  = $models->{ zaak };

    $self->interface($interface);

    try {

        my $mt = Mail::Track->new();

        my $file = $self->assert_file_from_id($interface, $params->{message});
        my $path = $self->assert_path($file);

        my $mime = io->catfile($path)->slurp();

        if ($self->log->is_trace) {
            $self->log->trace(sprintf("MIME found: %s", $mime // "<undefined>"));
        }

        my $message = $mt->parse($mime);
        my $mail = Zaaksysteem::Email->new(
            message => $message,
            schema  => $self->schema
        );

        if ($config->{type} eq DOCUMENT_INTAKE) {
            $mail->add_attachments();
        }
        elsif ($config->{type} eq OLO) {
            $mail->add_to_omgevingsloket({ models => $model_stash });
        }
        elsif ($config->{type} eq GENERIC) {
            my $casetype = $interface->case_type_id;
            my %options = (
                from    => $message->from,
                body    => $mail->get_message_body,
                subject => $message->subject,
            );
            my $case = $self->_create_case(%options);
            $mail->add_to_case($case);

            $zaak_model->execute_phase_actions($case, { phase => 1 });
        }
        else {
            throw("EmailIntake/Invalid/config/type", "Invalid configuration type");
        }

        $file->delete();
        $file->filestore->delete();

        my $msg = sprintf("Email van '%s' naar '%s' met onderwerp '%s' is verwerkt", $message->from, $message->to, $message->subject);
        $record->preview_string(substr($msg, 0, 200));
        $record->output($msg);
    }
    catch {
        my $err;
        if (eval {$_->isa('Throwable::Error')}) {
            $err = $_->as_string;
        }
        # ClamAv::Error::Client errors
        elsif (eval {$_->isa('Error::Simple')}) {
            $err = $_->stringify;
        }
        else {
            $err = "$_";
        }
        $record->output($err);

        my $msg = sprintf("Email met onderwerp '%s' is niet verwerkt: %s", $params->{subject}, $err);
        $record->preview_string(substr($msg, 0, 200));

        $self->exception($_);

        die $_;
    };
}

sub _assert_preset_client {
    my $self = shift;
    my $preset_client = $self->interface->case_type_id->zaaktype_node_id->zaaktype_definitie_id->preset_client;
    return $preset_client if $preset_client;
    throw('mailintake/no_preset_client', 'Vooringevulde aanvrager niet ingesteld');
}

sub _create_case {
    my ($self, %options) = @_;

    my $aanvrager = {
        'betrokkene'  => $self->_assert_preset_client,
        'verificatie' => 'medewerker'
    };

    my %kenmerken = % { $self->interface->case_type_id->attributes_by_magic_string() };
    my @kenmerken;

    my @attributes = @{ $self->interface->get_interface_config->{attribute_mapping} || []};
    foreach (@attributes) {
        if ($kenmerken{$_->{internal_name}{searchable_object_id}}) {
            push(@kenmerken, { $kenmerken{$_->{internal_name}{searchable_object_id}}->id => $options{$_->{external_name}} });
        }
    }

    my $opts = {
        aanvraag_trigger    => 'extern',
        contactkanaal       => 'email',
        onderwerp           => $options{subject},
        zaaktype_id         => $self->interface->get_column('case_type_id'),
        aanvragers          => [$aanvrager],
        kenmerken           => \@kenmerken,
        registratiedatum    => DateTime->now(),
    };

    return $self->interface->result_source->schema->resultset('Zaak')->create_zaak($opts);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
