package Zaaksysteem::Backend::Sysin::Interface::Component;

use Moose;

use Data::Compare;
use JSON::Path qw[];
use List::Util qw[first];

use BTTW::Tools;
use BTTW::Tools::DDiff qw[ddiff ddiff_stringify];
use Zaaksysteem::Backend::Sysin::Modules;
use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS/;

with 'MooseX::Log::Log4perl';

extends 'DBIx::Class';

=head1 NAME

Zaaksysteem::Backend::Sysin::Interface::Component - An interface component

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 module_object

An L<Zaaksysteem::Backend::Sysin::Modules> module which is related to this interface

=cut

has module_object => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return Zaaksysteem::Backend::Sysin::Modules->find_module_by_id(
            $self->module, $self->result_source->schema);
    }
);

=head2 model

A model which is related to the interface that you are using. When you are using this your
L<Zaaksysteem::Backend::Sysin::Modules::XXX> module should implement the function C<_get_model>.

=cut

has model => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->module_object->_get_model(
            {
                interface => $self,
                schema    => $self->result_source->schema,
            }
        );
    }
);

=head2 $interface->interface_update

Update the properties of an existing Sysin::Interface.

This method will call the active_state_callback on L</module_object> if the
previous active state of the instance is different from the new one.

=head3 Arguments

=over

=item name

=item case_type_id

=item max_retries

Defines the number of automated retries on the transactions belonging to this interface before the process gives up.

=item interface_config

The JSON hash containing all relevant configuration details.

=item multiple

Defines whether or not this interface will always receive a single mutation or multiple mutations.

=item active

Defines whether or not the Interface should be considered as 'in production'.

=back

=head3 Returns

An updated Sysin::Interface::Component object. The object used to call interface_update should also be updated.

=cut

use Zaaksysteem::Types qw(NonEmptyStr);

define_profile interface_update => (
    missing_optional_valid => 1,
    required               => [],
    optional               => [
        qw/
            case_type_id
            objecttype_id
            interface_config
            max_retries
            multiple
            active
            name
            /
    ],
    constraint_methods => {
        case_type_id => qr/^\d+$/,
        max_retries  => qr/^\d+$/,
        multiple     => qr/^(0|1){0,1}$/,
        active       => qr/^(0|1){0,1}$/,
        module       => sub {
            my ($dfv, $val) = @_;

            my $schema = $dfv->get_input_data->{schema};

            return 1
                if (grep { $_->name eq $val }
                Zaaksysteem::Backend::Sysin::Modules->list_of_modules($schema));

            return;
        },
        case_type_id => sub {
            my ($dfv, $val) = @_;
            my ($entry);

            return unless ref($val) || $val =~ /^\d+/;

            my $schema = $dfv->get_input_data->{schema};

            my $zt_id = (ref($val) ? $val->id : $val);

            return 1
                if (($entry = $schema->resultset('Zaaktype')->find($zt_id))
                && $entry->active
                && !$entry->deleted);

            return;
            }
    },
    typed => { name => NonEmptyStr, },
    defaults => {
        active   => 0,
        multiple => 0,
    }
);

sub interface_update {
    my $self        = shift;
    my $opts        = assert_profile(
        {
            %{ shift() },
            schema  => $self->result_source->schema
        }
    )->valid;

    my $model = shift;

    my @changes = $self->_merge_interface_config($opts);

    my $rs = $self->result_source->resultset;

    $opts = $rs->_prepare_options({
        %{ $opts },
        module => $self->module,
        schema => $self->result_source->schema
    });

    delete $opts->{schema};

    # Cleanup if the objecttype is being unset.
    if (exists $opts->{ objecttype_id } && !defined $opts->{ objecttype_id }) {
        $self->cleanup_objecttype_links($model);
    }

    # Cleanup if the objecttype is being set to something else.
    if (defined $opts->{ objecttype_id } && $opts->{ objecttype_id } ne $self->get_column('objecttype_id')) {
        $self->cleanup_objecttype_links($model);
    }

    my $active = $self->active;
    my $retval = $self->update($opts);

    if ($opts->{ active } != $active) {
        my $type = $opts->{ active } ? 'activate' : 'deactivate';

        $self->trigger_log($type);
    } elsif (scalar @changes) {
        $self->trigger_log(update => {
            changes => \@changes,
        });
    }

    $self->install_objecttype_links($model);

    # Post-update hooks
    if ($self->module_object->has_active_state_callback) {
        if ($opts->{ active } != $active) {
            $self->module_object->do_active_state_callback($retval, $model, $opts);
        }
    }
    if ($self->module_object->has_interface_update_callback) {
        $self->module_object->do_interface_update_callback($retval, $model, $opts);
    }

    return $retval;
}

=head2 get_objecttype

Convenience method for retrieving the interface's associated
L<Zaaksysteem::Object::Types::Type> instance, if set.

    my $type = $interface->get_objecttype($object_model);

This method also does some checks on the object to see if it is in an expected
state, and may throw C<sysin/interface/objecttype_inflation_failed>, and
C<sysin/interface/invalid_objecttype> exceptions when those checks fail.

=cut

sig get_objecttype => 'Zaaksysteem::Object::Model';

# Because of the fluctuating nature of interfaces when this thing is usually
# called (during updates), making this an attribute would introduce a host of
# state related bugs.
sub get_objecttype {
    my $self = shift;
    my $model = shift;

    my $object_data = $self->objecttype_id;

    return unless defined $object_data;

    my $type = $model->inflate_from_row($object_data);

    unless (defined $type) {
        throw('sysin/interface/objecttype_inflation_failed', sprintf(
            'Could not inflate the type object "%s" associated with interface "%s"',
            $self->get_column('objecttype_id'),
            $self->name
        ));
    }

    unless ($type->does('Zaaksysteem::Object::Roles::Interfaceable')) {
        throw('sysin/interface/invalid_objecttype', sprintf(
            'The object type associated with interface "%s" does not implement Zaaksysteem::Object::Roles::Interfaceable',
            $self->name
        ));
    }

    return $type;
}

=head2 cleanup_objecttype_links

This method cleans up any associated objecttypes of links to this object.

    $interface->cleanup_objecttype_links;

=cut

sig cleanup_objecttype_links => 'Zaaksysteem::Object::Model';

sub cleanup_objecttype_links {
    my $self = shift;
    my $model = shift;

    my $type = $self->get_objecttype($model);

    # Clean exit if there's nothing to cleanup.
    return unless $type;

    $type->remove_interface($self->id);

    $model->save(object => $type);

    return;
}

=head2 install_objecttype_links

This method installs this interface's link into a configured objecttype
instance.

    $interface->install_objecttype_links($object_model);

=cut

sig install_objecttype_links => 'Zaaksysteem::Object::Model';

sub install_objecttype_links {
    my $self = shift;
    my $model = shift;

    my $type = $self->get_objecttype($model);

    return unless $type;

    # Return if we're already set in the registry
    return if grep { $_->{ id } eq $self->id } $type->all_interfaces;

    $type->add_interface({
        id => $self->id,
        label => $self->name
    });

    $model->save(object => $type);

    return;
}

=head2 trigger_log

Convenience method for triggering a log entry for this component instance.

=cut

sig trigger_log => 'Str, ?HashRef';

sub trigger_log {
    my $self = shift;
    my $type = shift;
    my $data = shift || {};

    $data->{ interface_name } = $self->name;

    return $self->result_source->schema->resultset('Logging')->trigger(
        sprintf('sysin/interface/%s', $type),
        {
            component => 'interface',
            component_id => $self->id,
            data => $data
        }
    );
}

=head2 $interface->_merge_interface_config(\%OPTIONS)

Merges the current interface config with the ones given in update, prevents
the interface config from beeing overwritten

B<OPTIONS>

=over 4

=item interface_config

The interface_config to merge with the values already in the database

=back

=cut

sub _merge_interface_config {
    my $self            = shift;
    my $opts            = shift;

    return unless (
        $opts &&
        exists($opts->{interface_config}) &&
        UNIVERSAL::isa($opts->{interface_config}, 'HASH') &&
        $self->interface_config
    );

    my %given_config    = %{ $opts->{interface_config} };

    my $db_json         = $self->interface_config;
    my %db_config       = %{
        $self
            ->result_source
            ->resultset
            ->_decode_interface_config(
                $self->interface_config
            )
    };

    $opts->{interface_config} = { (%db_config, %given_config) };

    return ddiff(\%db_config, \%given_config);
}

=head2 $interface->interface_update

Delete the interface from the system

=cut

sub interface_delete {
    my $self    = shift;

    $self->update({date_deleted => DateTime->now});

    $self->trigger_log('delete');

    return 1;
}

=head2 get_mapped_attribute

Convenience method that finds and dereferences the internal attribute
definition in an attribute mapping, indexed by slug.

    # In an attribute mapping listing
    ...
    {
        slug => 'id',
        external_name => 'My fancy label',
        attribute_type => ...
    }

    # Somewhere with a component instance
    my $attr = $interface->get_mapped_attribute('id')

    # Which will contain whatever the spotenlighter for the attribute_type
    # has saved

=cut

sub get_mapped_attribute {
    my $self = shift;
    my $slug = shift;

    my $attribute = first {
        defined $_->{ slug } && $_->{ slug } eq $slug
    } @{ $self->jpath('$.attribute_mapping') || [] };

    unless (defined $attribute) {
        throw('sysin/interface/attribute_mapping_not_found', sprintf(
            'Could not find attribute mapping by slug "%s"',
            $slug
        ));
    }

    my $internal = $attribute->{ internal_name };

    unless (defined $internal && ref $internal eq 'HASH') {
        throw('sysin/interface/attribute_mapping_not_set', sprintf(
            'Attribute mapping for slug "%s" is not set',
            $slug
        ));
    }

    my $attribute_name = $internal->{ column_name };

    if ($attribute_name =~ m[^attribute\.(.*)$]) {
        $attribute_name = $1;
    }

    return $attribute_name;
}

=head2 $interface->get_attribute_mapping

Returns a list of attributes, which can be configured, and an instruction how
to fill this attribute list

=cut

sub get_attribute_mapping {
    my $self    = shift;

    return $self->module_object->get_attribute_mapping($self);
}

=head2 $interface->set_attribute_mapping

Sets a list of attributes, which can be configured, and an instruction how
to fill this attribute list

=cut

sub set_attribute_mapping {
    my $self    = shift;
    my $params  = shift;

    return $self->module_object->set_attribute_mapping($self, $params);
}

=head2 get_mapped_attributes_from_case

=cut

sig get_mapped_attributes_from_case => 'Zaaksysteem::Schema::Zaak';

sub get_mapped_attributes_from_case {
    my ($self, $case) = @_;

    my $mapping = $self->get_attribute_mapping->{attributes};

    if (!$mapping) {
        $self->log->warn("Attribute mapping requested, none found!");
        return;
    }


    my %attr = map { $_->bibliotheek_kenmerken_id->magic_string => $_->value } $case->zaak_kenmerken;
    my %result;
    foreach my $m (@$mapping) {
        my $internal = $m->{internal_name}{searchable_object_id};
        my $external = $m->{external_name};

        if (defined $internal && exists $attr{$internal}) {
            $result{$external} = $attr{$internal};
        }
        elsif ($self->log->is_trace) {
            $self->log->trace(
                sprintf("Skipping %s to %s, not present in case",
                    $internal // '<no internal name defined>',
                    $external // '<no external name defined>'
                )
            );
        }
    }
    return \%result;

}

=head2 $interface->get_trigger_definition($action)

Convenience method for L<Zaaksysteem::Backend::Sysin::Modules#get_trigger_definition>

=cut

sub get_trigger_definition {
    my ($self, $action)    = @_;

    return $self->module_object->get_trigger_definition($action);
}

=head2 $interface->process_trigger($action, \%params, \%models)

Convenience method for L<Zaaksysteem::Backend::Sysin::Modules#process_trigger>

=cut

sub process_trigger {
    my ($self, $action, $params, $models)    = @_;
    $params ||= {};
    $models ||= {};

    return $self->module_object->process_trigger({
        interface   => $self,
        params      => $params,
        action      => $action,
        models      => $models
    });
}

=head2 $interface->process_api_trigger($action, \%params)

Convenience method for L<Zaaksysteem::Backend::Sysin::Modules#process_api_trigger>

=cut

sub process_api_trigger {
    my ($self, $action, $params)    = @_;
    $params                         ||= {};

    return $self->module_object->process_api_trigger({
        interface   => $self,
        params      => $params,
        action      => $action
    });
}

=head2 process_generic_error

This method can be used to create a generic error transaction. The use case
for this specific method is that not all fault states we would like to have a
transaction for are caught while processing some input (think StUF client
certificate failures).

=cut

define_profile process_generic_error => (
    optional => {
        input_data => 'Str',
        short_error => 'Str',
        external_transaction_id => 'Str'
    },
    defaults => {
        input_data => 'n/a',
        external_transaction_id => 'n/a'
    }
);

sig process_generic_error => 'Str, ?HashRef';

sub process_generic_error {
    my $self = shift;
    my $message = shift;
    my $args = assert_profile(shift || {})->valid;

    $args->{ interface_id } = $self->id;

    my $tx = $self->transactions->transaction_create($args);

    if ($args->{ short_error }) {
        $message = sprintf('%s: %s', $args->{ short_error }, $message);

        $tx->preview_data({
            transaction_id => $tx->id,
            preview_string => $args->{ short_error }
        });
    }

    $tx->error_count(1);
    $tx->success_count(0);
    $tx->error_fatal(1);
    $tx->error_message($message);

    return $tx->update
}

=head2 $interface->get_interface_form

Convenience method dispatching to attached interface module for generating
the interface form.

=cut

sub get_interface_form {
    my ($self, %params) = @_;

    return $self->module_object->generate_interface_form(%params, entry => $self);
}

=head2 $interface->process

Return value: $ROW_TRANSACTION

Convenient method to L<Zaaksysteem::Backend::Sysin::Modules#process>, see this
man page for more information.

=cut

sub process {
    my $self        = shift;
    my $options     = shift || {};

    return $self->module_object->process(
        {
            %{ $options },
            interface   => $self,
        }
    );
}

sub run_test {
    my $self        = shift;
    my $test_id     = shift;


    return $self->module_object->run_test(
        $self,
        $test_id
    );
}

=head2 $interface->TO_JSON

=cut

sub TO_JSON {
    my $self                    = shift;
    my $opts                    = shift || {};

    my $values                  = { $self->get_columns };

    ### Decode interface config to perl, so JSON can re-encode it...
    $values->{interface_config} = $self
        ->result_source
        ->resultset
        ->_decode_interface_config($values->{interface_config});

    unless ($opts->{ignore_errors}) {
        my $errors_rs = $self->transactions->search(
            {'me.error_count' => { '>' =>  0 } },
            { rows => 1}
        );
        $values->{transaction_errors} = $errors_rs->first ? 1 : 0
    }

    return $values;
}

=head2 $interface->get_properties

=cut

sub get_interface_config {
    my $self    = shift;

    $self->result_source
        ->resultset
        ->_decode_interface_config($self->interface_config);
}

sub update_interface_config {
    my $self    = shift;
    my $config  = shift;

    $self->interface_config(
        $self   ->result_source
                ->resultset
                ->_encode_interface_config($config)
    );

    $self->update;

}

sub jpath {
    my $self = shift;

    return JSON::Path->new(shift)->value($self->get_interface_config);
}

sub jpath_all {
    my $self = shift;

    return JSON::Path->new(shift)->values($self->get_interface_config);
}

=head2 get_casetype_id

Retrieves the C<UUID> of the associated casetype. Returns undef if no casetype
is associated with the interface.

=cut

sub get_casetype_id {
    my $self = shift;

    my $zaaktype_id = $self->get_column('case_type_id');

    return unless $zaaktype_id;

    my $zt = $self->result_source->schema->resultset('Zaaktype')->find($zaaktype_id);

    unless (defined $zt) {
        throw('sysin/casetype_corrupt', sprintf(
            'Attempted to retrieve casetype_id, but zaaktype row "%s" was not found',
            $zaaktype_id
        ));
    }

    return $zt->uuid;
}

=head2 create_case

For requestor, a preset client is used. A hashref without attribute values
is expected. For an explanation of the attribute values, see 'map_attributes'.

=cut

define_profile create_case => (
    required => [qw/values requestor/],
    optional => [qw/record/],
    typed => {
        values => 'HashRef',
    },
    constraint_methods => {
        requestor => sub {
            my ($dfv, $value) = @_;

            # betrokkene-natuurlijk_persoon-344343
            # betrokkene-medewerker-23
            # betrokkene-bedrijf-1232
            return $value =~ m/^betrokkene-(?:natuurlijk_persoon|medewerker|bedrijf)-\d+$/;
        }
    }
);

sub create_case {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $aanvrager = {
        betrokkene => $params->{requestor},
        verificatie => 'medewerker'
    };

    my @attributes = $self->_map_attributes($params->{values});

    my @type_warnings = $self->_get_type_warnings(@attributes);

    if ($params->{record}) {
        $params->{record}->output(join "\n", @type_warnings);
    }

    my $kenmerken = [
        map {{
            $_->{bibliotheek_kenmerk}->id => $_->{value}
        }} @attributes
    ];

    my $opts = {
        aanvraag_trigger    => 'extern',
        contactkanaal       => 'email',
        onderwerp           => 'Omgevingsloket',
        zaaktype_id         => $self->get_column('case_type_id'),
        aanvragers          => [$aanvrager],
        kenmerken           => $kenmerken,
        registratiedatum    => DateTime->now,
    };

    return $self->result_source->schema->resultset('Zaak')->create_zaak($opts);
}

=head2 _get_type_warnings

When an interface create a new case and populates the zaaktype_kenmerken,
this verifies wether anything unhandy has been done like putting a
string 'abc' into a numeric field. At the time of this writing, this would
yield weird behaviour, the value would show in the case but as soon
as you would attempt to edit it the string value will disappear. This
function predicts these problems so they can be logged.

=cut

sub _get_type_warnings {
    my ($self, @attributes) = @_;

    my @warnings;

    foreach my $attribute (@attributes) {

        my $value = $attribute->{ value };
        my $bibliotheek_kenmerk = $attribute->{bibliotheek_kenmerk};
        my $filtered = $bibliotheek_kenmerk->filter($value);

        if ($value && !Compare($value, $filtered)) {
            my $value_display = ref $value eq 'ARRAY' ? join(", ", @$value) : $value;
            my $filtered_display = ref $filtered eq 'ARRAY' ? join(", ", @$filtered) : $filtered;
            $value_display ||= '';
            $filtered_display ||= '';

            # try to get the label from the config
            my $type = ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{
                $bibliotheek_kenmerk->value_type
            }->{label} || $bibliotheek_kenmerk->value_type;

            my $kenmerk_naam = $bibliotheek_kenmerk->naam;

            push @warnings, "Mogelijk dataverlies: De waarde '$value_display' " .
                "is bij het importeren gefilterd tot '$filtered_display' " .
                "om in het gekozen kenmerk '$kenmerk_naam' met type '$type' te passen.";
        }
    }

    return @warnings;
}

=head2 map_attributes

There's two steps. First there's matching the supplied raw input data
to the configured magic_strings. The interface configuration maps
'external_name' - name is it's known in the outside world, to:
'internal_name' - in this scenario that's a magic string,
(a tag that has been configured to represent a custom field)

So input data could be:
    {
        attribute1 => 'value 1',
        attribute2 => 'value 2'
    }

Interface configuration could be:
    [{
        external_name => 'attribute_1',
        internal_name => 'magic_string_1' # in reality one level deeper,
                                          # but doesn't matter here
    },
    {
        external_name => 'attribute_2',
        internal_name => 'magic_string_2'
    }]

Which is shortened to:

$attribute_mapping = {
    magic_string1 => 'attribute1',
    magic_string2 => 'attribute2'
}

And finally the casetype configuration:
{
    magic_string_1 => 123, # bibliotheek_kenmerken_id
    magic_string_2 => 124
}

The desired end result of this transmogrification looks like:
    [{
        123 => 'value 1'
    },
    {
        124 => 'value 2'
    }]

=cut

sub _map_attributes {
    my ($self, $values) = @_;

    throw('sysin/interface/no_values', 'Geen waarden ontvangen')
        unless $values && ref $values eq 'HASH';

    throw('sysin/interface/case_type_id_undefined', 'Geen zaaktype ingesteld')
        unless $self->case_type_id;

    my $lookup = $self->case_type_id->attributes_by_magic_string;

    my %attribute_mapping = map {
        # magic_string => external_name
        $_->{internal_name}{searchable_object_id} => $_->{external_name}
    } grep {
        # double exists construct to prevent auto-vivification
        exists $_->{internal_name} &&
        exists $_->{internal_name}{searchable_object_id}
    } @{ $self->get_interface_config->{attribute_mapping} };

    return map {
        $self->_format_attribute($lookup, \%attribute_mapping, $values, $_)
    } grep {
        my $external_name = $attribute_mapping{$_};
        exists $lookup->{$_} && exists $values->{$external_name}
    } keys %attribute_mapping;
}

=head2 _format_attribute

Helper function for map_attributes. Returns a hashref containing
one key and value pair, as desired by case creation logic.

=cut

sub _format_attribute {
    my ($self, $lookup, $attribute_mapping, $values, $magic_string) = @_;

    my $external_name = $attribute_mapping->{$magic_string};
    my $bibliotheek_kenmerk = $lookup->{$magic_string};

    return {
        bibliotheek_kenmerk => $bibliotheek_kenmerk,
        value => $values->{$external_name}
    };
}

=head2 register_case_attributes_as_mutations

Adds a mutation row for every attribute that has been added to the newly created case.

=cut

sub register_case_attributes_as_mutations {
    my ($self, $case) = @_;

    my $mutation = Zaaksysteem::Backend::Sysin::Transaction::Mutation->new(
        create              => 1,
        table               => 'Zaak',
        table_id            => $case->id
    );

    my $kenmerken = $case->field_values;

    foreach my $key (keys %$kenmerken) {
        $mutation->add_mutation({
            old_value   => '',
            new_value   => $kenmerken->{$key},
            column      => 'kenmerk.' . $key,
        });
    }

    return $mutation;
}

=head2 is_active

Tells you if the interface is active or not.
It does this by looking at the active boolean and/or checks
if the interface has a date deleted set.

=cut

sub is_active {
    my $self = shift;
    if (!$self->active || defined $self->date_deleted) {
        return 0;
    }
    return 1;
}

=head2 check_client_certificate

Check if the client certificate matches

=cut

sub check_client_certificate {
    my $self = shift;
    return $self->module_object->check_client_certificate(
        interface => $self,
        @_,
    );
}

=head2 is_valid_soap_api

Check if the soap call is supported by this interface

=cut


sub is_valid_soap_api {
    my $self = shift;
    return $self->module_object->is_valid_soap_api(
        interface => $self,
        @_,
    );
}

=head2 is_multi_tenant

Check if the interface supports multi-tenant

=cut

sub is_multi_tenant {
    my $self = shift;

    return 0 if ! $self->module_object->does("Zaaksysteem::Backend::Sysin::Modules::Roles::MultiTenant");
    return $self->result_source->schema->is_multi_tenant;
}

=head2 supports_hostname

Check if the interface supports the given hostname.

    # Check if the interface can be used for the hostname
    $self->supports_hostname("foo.zs.nl");

    # The interface must be specificly set for the hostname
    $self->supports_hostname("foo.zs.nl", 1);

=cut

sig supports_hostname => 'Str, ?Bool';

sub supports_hostname {
    my ($self, $hostname, $exact) = @_;

    if (!$self->is_multi_tenant) {
        return 1;
    }

    my $config = $self->get_interface_config;
    if (!exists $config->{is_multi_tenant} || !$config->{is_multi_tenant}) {
        if ($exact) {
            return 0;
        }
        return 1;
    }
    elsif ($config->{multi_tenant_host} eq $hostname) {
        return 1;
    }
    else {
        return 0;
    }
}

1;

__END__

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 get_interface_config

TODO: Fix the POD

=cut

=head2 interface_delete

TODO: Fix the POD

=cut

=head2 jpath

TODO: Fix the POD

=cut

=head2 run_test

TODO: Fix the POD

=cut

=head2 update_interface_config

TODO: Fix the POD

=cut

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

