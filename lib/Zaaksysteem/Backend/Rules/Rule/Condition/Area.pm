package Zaaksysteem::Backend::Rules::Rule::Condition::Area;

use Moose::Role;
use BTTW::Tools;

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Condition::Area - Handles area rules (wijk)

=head1 SYNOPSIS

=head1 DESCRIPTION

=cut

# has 'wijk'  =>

has 'is_area' => (
    is  => 'rw',
);

after 'BUILD' => sub {
    my $self        = shift;

    my $requestor_areas;
    if ($self->attribute eq 'aanvrager_wijk') {
        # warn('Found aanvrager_wijk' . Data::Dumper::Dumper($self->rules_params));
        $self->validates_true(0);
        $self->validation_type('fixed');

        if (
            $self->rules_params->{'case.requestor.house_number'} &&
            $self->rules_params->{'case.requestor.zipcode'}
        ) {
            $requestor_areas = $self->parkeergebied({
                huisnummer  => $self->rules_params->{'case.requestor.house_number'},
                postcode    => $self->rules_params->{'case.requestor.zipcode'},
            });
        }
    } elsif ($self->is_area || $self->attribute =~ /(\d+)-wijk/) {
        if ($self->attribute =~ /(\d+)-wijk/) {
            my $magic_string = $self->attribute_map->{$1};
            $self->_old_kenmerk_id($1);

            $self->attribute('attribute.' . $magic_string);
            $self->is_area(1);
        }

        if ($self->is_area) {
            $self->validates_true(0);
            $self->validation_type('revalidate');

            ### Got attribute!
            if ($self->rules_params->{$self->attribute}) {
                $requestor_areas = $self->wijk({
                    bag_value => $self->rules_params->{$self->attribute}
                });
            }
        }
    } else {
        return;
    }

    if ($requestor_areas) {
        for my $requestor_area (@{ $requestor_areas }) {
            if (grep { $requestor_area && $_ && $requestor_area eq $_ } @{ $self->values }) {
                $self->validates_true(1);
            }
        }
    }

    if (!$requestor_areas || ! @{ $requestor_areas }) {
        ### Special case: no values, which means: when nothing is selected, return true
        if (!length join('', grep ({ defined $_ } @{ $self->values }))) {
            $self->validates_true(1);
        }
    }

};

=head2 conditions

=cut

sub parkeergebied  {
    my ($self, $options) = @_;

    # aanvrager may not be postcode/huisnummer enabled
    my $parkeergebied = [];
    eval {
        $parkeergebied = $self->_schema->resultset('Parkeergebied')->find_parkeergebied($options);
    };
    if($@) {
        warn "error during parkeergebied lookup: " . $@;
    }
    return $parkeergebied;
}

sub wijk {
    my ($self, $options) = @_;

    my $bag_value = $options->{bag_value}
        or return [];

    my $bag_model = $self->_schema->bag_model;

    my $bag_object;
    if( $bag_value =~ m/^(openbareruimte|nummeraanduiding)-(\d+)$/) {
        $bag_object = $bag_model->get($1 => $2);
    } else {
        my $bag_object;
        if (my $parsed_search_term = $bag_model->parse_search_term($bag_value)) {
            $bag_object = $bag_model->search_exact(%$parsed_search_term);
        } else {
            my $bag_objects = $bag_model->search(
                type => 'nummeraanduiding',
                query => $bag_value,
            );

            $bag_object = $bag_objects->[0] if @$bag_objects;
        }

        unless ($bag_object) {
            $self->log->warn("Could not find bag object for search term '$bag_value'");
            return [];
        }
    }

    my $address_data = $bag_object->nummeraanduiding || $bag_object->openbareruimte;
    return $self->parkeergebied({
        postcode                => $address_data->{postcode},
        huisnummer              => $address_data->{huisnummer},
        huisnummertoevoeging    => $address_data->{huisnummertoevoeging},
        huisletter              => $address_data->{huisletter},
        woonplaats              => $address_data->{woonplaats},
        straatnaam              => $address_data->{straat}, # different naming alert
    });
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 parkeergebied

TODO: Fix the POD

=cut

=head2 wijk

TODO: Fix the POD

=cut

