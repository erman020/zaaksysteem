package Zaaksysteem::Backend::Rules::Rule::AttributesValidation;
use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Rule::AttributesValidation - Validate attributes

=head1 SYNOPSIS

    with 'Zaaksysteem::Backend::Rules::Rule::AttributesValidation';

    $self->validate_price($price);

=head1 DESCRIPTION

Moose role which validates attributes. Currently only implements one
function: C<validate_price>

=head1 METHODS

=head2 validate_price

Validates the price, if the price is valid returns 1 and sets the
attribute name to 'case.price'

=cut

sub validate_price {
    my $self  = shift;
    my $value = shift;

    if ($self->attribute_name =~ /^(?:case\.)?price$/) {
        # A price via ZTT is 3.25 and not 3,25
        if (defined $value && $value =~ m|^\d+([,.]\d+)?$|) {
            $self->attribute_name('case.price');
            return 1;
        }
        else {
            $self->log->warn(sprintf('Incorrect price in rule "%s": %s', $self->label, $value));
        }
        return 0;
    }
    return 0;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
