package Zaaksysteem::Zaaktypen;

use Moose;
use namespace::autoclean;

use Moose::Util qw/apply_all_roles/;

use BTTW::Tools;
use Zaaksysteem::Constants;

use Params::Profile;
use Data::Dumper;

with qw/
    Zaaksysteem::Zaaktypen::Children
    Zaaksysteem::Zaaktypen::INavigator
    MooseX::Log::Log4perl
/;

=head1 NAME

Zaaksysteem::Zaaktypen - Zaaktype interaction model

=head1 DESCRIPTION

=head1 CONSTANTS

=head2 ZAAKTYPEN_PREFIX

=cut

use constant ZAAKTYPEN_PREFIX   => 'zaaktype_';

=head2 ZAAKTYPEN_RELATIES

=cut

use constant ZAAKTYPEN_RELATIES => [qw/
    zaaktype_authorisaties
    zaaktype_betrokkenen
    zaaktype_statussen
/];

=head1 ATTRIBUTES

=head2 prod

=head2 dbic

=cut

has [qw/prod dbic/] => (
    'is'    => 'rw',
);

=head2 object_model

A reference to a L<Zaaksysteem::Object::Model> instance. This is used by the
object sync hooks.

=cut

has object_model => (
    is => 'rw',
    isa => 'Zaaksysteem::Object::Model',
    required => 1
);

=head1 METHODS

=head2 session_template

=cut

{
    my $SESSION_TEMPLATE    = {
        'zaaktype'      => 'Zaaktype',
        'definitie'     => 'ZaaktypeDefinitie',
        'node'          => 'ZaaktypeNode',
        'statussen'     => 'ZaaktypeStatus',
    };

    sub session_template {
        my ($self) = @_;

        my $template    = {};
        while (my ($key, $table) = each %{ $SESSION_TEMPLATE }) {
            $template->{$key} = $self->dbic->resultset($table)->_get_session_template;
        }

        return $template;
    }
}

=head2 retrieve_session

=cut

sub retrieve_session {
    my ($self, $casetype_id) = @_;

    return $self->retrieve(id => $casetype_id, as_session => 1);
}

=head2 retrieve

=cut

{
    Params::Profile->register_profile(
        method  => 'retrieve',
        profile => {
            required        => [ qw/
            /],
            'optional'      => [ qw/
                as_session
                as_clone
            /],
            'require_some'  => {
                'zaaktype_id_or_node_id' => [
                    1,
                    'id',
                    'nid'
                ],
            },
            'constraint_methods'    => {
                'id'    => qr/^\d+$/,
                'nid'   => qr/^\d+$/,
            },
        }
    );

    sub retrieve {
        my ($self, %opts) = @_;
        my ($nid);

        my $dv = Params::Profile->check(
            params  => \%opts
        );

        do {
            $self->log->warn(
                'Zaaktype->retrieve: invalid options' . Dumper $dv
            );
            return;
        } unless $dv->success;

        ### Retrieve resultset
        my ($zt_node);
        if ($opts{id}) {
            $zt_node = $self->_retrieve_node_by_zaaktype_id($opts{id});
        } else {
            $zt_node = $self->_retrieve_node_by_zaaktype_node_id($opts{nid});
        }

        ### Retrieve as session when asked
        if ($opts{as_session}) {
            my $session = $self->_retrieve_as_session($zt_node);
            return unless $session;

            if ($opts{as_clone}) {
                ### Delete id and version
                delete($session->{zaaktype}->{id});
                $session->{node}->{version} = 1;
                delete $session->{zaaktype}->{zaaktype_node_id};
            }

            return $session;
        }

        return $zt_node;
    }

    sub _retrieve_as_session {
        my ($self, $node)   = @_;
        my $rv              = {};

        return unless ref($node);

        ### Retrieve all relaties
        my $relaties        = ZAAKTYPEN_RELATIES;
        my $relatieprefix   = ZAAKTYPEN_PREFIX;

        ### Retrieve zaaktype and zaaktypenode
        {
            ### ZAAKTYPE
            $rv->{zaaktype}     = {};
            my @columns         = $node->zaaktype_id->result_source->columns;

            for my $column (@columns) {
                ### When this is a reference to another table, just
                ### retrieve the id
                if (ref($node->zaaktype_id->$column) && $node->zaaktype_id->$column->can('id')) {
                    $rv->{zaaktype}->{$column}  = $node->zaaktype_id->$column->id;
                } elsif (!ref($node->zaaktype_id->$column)) {
                    $rv->{zaaktype}->{$column} = $node->zaaktype_id->$column;
                }
            }


            ### DEFINITIE XXX TEMPORARILY IF
            $rv->{definitie}     =
                $self->_retrieve_as_session_definitie($node);

            ### NODE
            $rv->{node}         = {};
            @columns            = $node->result_source->columns;

            for my $column (@columns) {
                ### When this is a reference to another table, just
                ### retrieve the id
                if(ref($node->$column) && $column eq 'properties') {
                    $rv->{node}->{$column} = $node->$column;
                } elsif(ref($node->$column) && $node->$column->can('id')) {
                    $rv->{node}->{$column}  = $node->$column->id;
                } elsif (!ref($node->$column)) {
                    $rv->{node}->{$column} = $node->$column;
                }
            }
        }

        for my $relatie (@{ $relaties }) {
            ### Remove prefix,
            ### eg: $rv->{kenmerken} ipv $rv->{zaaktype_kenmerken}
            my $key         = $relatie;
            $key            =~ s/^$relatieprefix//;

            ### XXX Generic bitte
            if ($relatie eq 'zaaktype_authorisaties') {
                next unless $node->zaaktype_id->$relatie->can('_retrieve_as_session');
                $rv->{$key}     =
                    $node->zaaktype_id->$relatie->_retrieve_as_session({ node => $node });
            } else {
                next unless $node->$relatie->can('_retrieve_as_session');
                $rv->{$key}     = $node->$relatie->_retrieve_as_session({ node => $node });
            }

        }

        return $rv;
    }


    sub _retrieve_as_session_definitie {
        my ($self, $node)   = @_;
        my $rv              = {};

        if ($node->zaaktype_definitie_id) {
            my @columns         = $node->zaaktype_definitie_id->result_source->columns;
            for my $column (@columns) {
                ### When this is a reference to another table, just
                ### retrieve the id
                if (
                    ref($node->zaaktype_definitie_id->$column) &&
                    $node->zaaktype_definitie_id->$column->can('id')
                ) {
                    $rv->{$column} =
                        $node->zaaktype_definitie_id->$column->id;
                } elsif (!ref($node->zaaktype_definitie_id->$column)) {
                    $rv->{$column}
                        = $node->zaaktype_definitie_id->$column;
                }
            }

        } else {
            ### WATCH IT, THIS IS AN OLD DEFINITION
            $rv->{oud_zaaktype} = 1;

            my $attributen  = $node->zaaktype_attributen->search();
            while (my $attr = $attributen->next) {
                my $values  = $attr->zaaktype_values;
                if ($values->count == 1) {
                    my $value = $values->first;

                    my $key = $attr->key;
                    $key    =~ s/^ztc_//g;

                    $rv->{$key} = $value->value;
                }
            }

            ### BACKWARDS COMPATIBILITY
            $rv->{'extra_informatie'} = $node->toelichting;
        }

        return $rv;
    }


    sub _retrieve_node_by_zaaktype_node_id {
        my ($self, $nid) = @_;
        my ($zt_node);

        unless (
            $zt_node = $self->dbic->resultset('ZaaktypeNode')->find($nid)
        ) {
            $self->log->warn(
                'Zaaktype->retrieve: Cannot find zaaktype with node_id: '
                . $nid
            );

            return;
        }

        return $zt_node;
    }


    sub _retrieve_node_by_zaaktype_id {
        my ($self, $id) = @_;

        my $zaaktype = $self->dbic->resultset('Zaaktype')->find($id, {
            prefetch => 'zaaktype_node_id'
        });

        unless (defined $zaaktype) {
            $self->log->warn(sprintf(
                'Zaaktype retrieval: cannot find zaaktype by id "%s"',
                $id
            ));

            return;
        }

        return $zaaktype->zaaktype_node_id;
    }
}

=head2 validate_session

=cut

{
    Params::Profile->register_profile(
        method  => 'validate_session',
        profile => {
            required        => [ qw/
                session
            /],
            'optional'      => [ qw/
                zs_fields
            /],
            'constraint_methods'    => {
            },
        }
    );

    sub validate_session {
        my ($self, %opts) = @_;
        my ($rv);

        my $dv = Params::Profile->check(
            params  => \%opts
        );

        do {
            $self->log->warn(
                'Zaaktype->validate_session: invalid options' . Dumper $dv
            );
            return;
        } unless $dv->success;

        my $relaties        = ZAAKTYPEN_RELATIES;
        my $relatieprefix   = ZAAKTYPEN_PREFIX;

        $rv->{node}         =
            $self->dbic->resultset('ZaaktypeNode')->_validate_session(
                $dv->valid('session')->{node}
            );

        $rv->{zaaktype}     =
            $self->dbic->resultset('Zaaktype')->_validate_session(
                $dv->valid('session')->{zaaktype}
            );

        $rv->{definitie}    =
            $self->dbic->resultset('ZaaktypeDefinitie')->_validate_session(
                $dv->valid('session')->{definitie}
            );


        for my $relatie (@{ $relaties }) {
            my $relatie_info    = $self->dbic->resultset('ZaaktypeNode')
                                    ->result_source->relationship_info($relatie);
            next unless $relatie_info && $relatie_info->{source};

            my $relatie_object  = $self->dbic->resultset($relatie_info->{source});

            next unless $relatie_object->can('_validate_session');

            ### Remove prefix,
            ### eg: $rv->{kenmerken} ipv $rv->{zaaktype_kenmerken}
            my $key         = $relatie;
            $key            =~ s/^$relatieprefix//;

            $rv->{$key}     = $relatie_object->_validate_session($dv->valid('session')->{statussen});
        }

        ### Everything validated, now make a validation profile when params are
        ### given
        $rv->{validation_profile}   = $self->_make_validation_profile(
            $rv,
            [ $dv->valid('zs_fields') ]
        ) if $dv->valid('zs_fields');

        return $rv;
    }

    sub _make_validation_profile {
        my ($self, $validated, $fields) = @_;

        my $validation_profile = {
            success     => 1,
            missing     => [],
            invalid     => [],
            unknown     => [],
            valid       => [],
            msgs        => {},
        };

        for my $param (@{ $fields }) {
            # Security, only test.bla[.bla.bla]
            next unless $param  =~ /^[\w\d\_]+\.[\w\d\_\.]+$/;

            my @tree            = split(/\./, $param);
            my $method          = pop(@tree);

            my $eval = '$validated->{';
            $eval   .= join('}->{', @tree) . '}';

            if (eval($eval . '->valid(\'' . $method . '\');')) {
                push(
                    @{ $validation_profile->{valid} },
                    $param
                );
            } elsif (eval($eval . '->invalid(\'' . $method . '\');')) {
                push(
                    @{ $validation_profile->{invalid} },
                    $param
                );
                $validation_profile->{success} = 0;
            } elsif (eval($eval . '->missing(\'' . $method . '\');')) {
                push(
                    @{ $validation_profile->{missing} },
                    $param
                );
                $validation_profile->{success} = 0;
            }

            my $msgs    = eval($eval . '->msgs');

            #warn Dumper($msgs);
            $validation_profile->{msgs}->{$param} = $msgs->{$method}
                if $msgs->{$method};

        }

        return $validation_profile;
    }
}

=head1 commit_session

=cut

{
    Params::Profile->register_profile(
        method  => 'commit_session',
        profile => {
            required        => [ qw/
                session
            /],
            'optional'      => [ qw/
                commit_components
                commit_message
            /],
            'constraint_methods'    => {
            },
        }
    );

    sub commit_session {
        my ($self, %opts) = @_;
        my ($rv);

        my $dv = Params::Profile->check(
            params  => \%opts,
        );

        if (!$dv->success) {
            die 'Zaaktype->commit_session: invalid options' . Dumper $dv;
        }

        my $session = $dv->valid('session');

        my $relaties        = ZAAKTYPEN_RELATIES;
        my $relatieprefix   = ZAAKTYPEN_PREFIX;

        ### Validate session
        $self->assert_valid_session($session);

        ### Transaction
        my ($zaaktype_node);
        $self->dbic->txn_do(sub {
            ### Create zaaktype, unless it's an edit ;)
            my $zaaktype = $self->_commit_zaaktype($session);
            if (!$zaaktype) {
                $self->log->error("No zaaktype found in session");
                return;
            }
            $zaaktype_node = $self->_commit_zaaktype_node($session, $zaaktype);
            if (!$zaaktype_node) {
                $self->log->error("No zaaktype node commited");
                return;
            }

            my $zaaktype_definitie = $self->_commit_zaaktype_definitie($session, $zaaktype_node);
            if (!$zaaktype_definitie) {
                $self->log->error("No zaaktype definitie commited");
                return;
            }

            my $rel_rs = $self->dbic->resultset('ZaaktypeNode');
            for my $relatie (@{ $relaties }) {

                $self->log->debug("Trying to commit $relatie to session");

                my $relatie_info = $rel_rs->result_source->relationship_info($relatie);

                unless ($relatie_info && $relatie_info->{source}) {
                    $self->log->warn("Unable to get relatie_info source for $relatie");
                }
                my $relatie_object  = $self->dbic->resultset($relatie_info->{source});

                unless ($relatie_object->can('_commit_session')) {
                    $self->log->warn("Unable _commit_session on relatie_object of $relatie");
                }

                ### Remove prefix,
                ### eg: $rv->{kenmerken} ipv $rv->{zaaktype_kenmerken}
                my $key         = $relatie;
                $key            =~ s/^$relatieprefix//;

                $rv->{$key} = $relatie_object->_commit_session(
                    $zaaktype_node,
                    $session->{$key}
                );
            }

            ### Fase update
            if ($session->{definitie}->{oud_zaaktype}) {
                ### Oud zaaktype, duplicate definitie
                if (
                    $session->{definitie}->{oud_zaaktype} &&
                    $zaaktype_node->zaaktype_id
                ) {
                    my $old_nodes = $self->dbic->resultset('ZaaktypeNode')->search(
                        {
                            zaaktype_id => $zaaktype->id
                        }
                    );

                    my $current_fases = $self->dbic->resultset('ZaaktypeStatus')->search(
                        {
                                zaaktype_node_id   => $zaaktype_node->id,
                        }
                    );

                    my %definitie = $zaaktype_node->zaaktype_definitie_id->get_columns;
                    delete($definitie{id});

                    while (my $old_node = $old_nodes->next) {
                        ### UPDATE DEFINITIE
                        my $copy = $self->dbic->resultset('ZaaktypeDefinitie')->create(
                            \%definitie
                        );

                        $old_node->zaaktype_definitie_id($copy->id);
                        $old_node->update;

                        ### UPDATE FASES
                        $current_fases->reset;
                        if ($current_fases->count) {
                            while (my $current_fase = $current_fases->next) {
                                my $old_fase = $old_node->zaaktype_statussen->search(
                                    {
                                        status  => $current_fase->status
                                    }
                                );

                                next unless $old_fase->count == 1;
                                $old_fase = $old_fase->first;

                                $old_fase->naam( $current_fase->naam );
                                $old_fase->fase( $current_fase->fase );

                                $old_fase->update;
                            }

                        }
                    }
                }
            }

            if ($@) {
                my $msg = "Rolling back $@";
                $self->log->fatal($msg);
                die($msg);
            }

            my $components = $opts{commit_components};
            my $message    = $opts{commit_message};

            my $event = $self->dbic->resultset('Logging')->trigger(
                'casetype/mutation',
                {
                    component    => LOGGING_COMPONENT_ZAAKTYPE,
                    component_id => $zaaktype->id,
                    data         => {
                        case_type => $zaaktype->id,
                        title     => $zaaktype->zaaktype_node_id->titel,
                        components     => $components,
                        commit_message => $message,
                    },
                },
            );

            $zaaktype->discard_changes->_sync_object($self->object_model);
            $zaaktype->zaaktype_node_id->update({logging_id => $event->id});
        });

        return $zaaktype_node;
    }

    sub _commit_zaaktype {
        my ($self, $session)        = @_;
        my $params = {};

        my $rs = $self->dbic->resultset('Zaaktype');
        for my $col ($rs->result_source->columns) {
            next unless defined $session->{zaaktype}->{ $col };
            $params->{ $col } = $session->{zaaktype}->{ $col };
        }

        my $zaaktype = $params->{id} ? $rs->find($params->{id}) : undef;

        if ($zaaktype) {
            if ($session->{zaaktype}->{bibliotheek_categorie_id}) {
                $zaaktype->bibliotheek_categorie_id(
                    $session->{zaaktype}->{bibliotheek_categorie_id}
                );
                $zaaktype->update;
            }
            return $zaaktype;
        }
        return $rs->create($params);
    }

    sub _commit_zaaktype_node {
        my ($self, $session, $zaaktype)        = @_;
        my $params = {};

        $params->{ $_ } = $session->{node}->{ $_ }
            for $self->dbic->resultset('ZaaktypeNode')->result_source
                ->columns;

        if ($params->{id}) {
            ### Bump version
            $params->{version}++;
        }

        ### Make sure we delete id
        delete($params->{id});

        ### Add zaaktype id
        $params->{zaaktype_id} = $zaaktype->id;

        my $node    = $self->dbic->resultset('ZaaktypeNode')->create(
            $params
        );

        ### Mark old one as deleted
        if ($zaaktype->zaaktype_node_id) {
            $zaaktype->zaaktype_node_id->deleted(DateTime->now());
            $zaaktype->zaaktype_node_id->update;
        }

        $zaaktype->zaaktype_node_id($node->id);
        $zaaktype->update;

        return $node;
    }

    sub _commit_zaaktype_definitie {
        my ($self, $session, $node)   = @_;
        my ($rv, $params) = (undef, {});

        $params->{ $_ } = $session->{definitie}->{ $_ }
            for $self->dbic->resultset('ZaaktypeDefinitie')->result_source
                ->columns;

        delete($params->{id});
        $rv = $self->dbic->resultset('ZaaktypeDefinitie')->create(
            $params
        );

        if ($rv) {
            $node->zaaktype_definitie_id($rv->id);
            $node->update;

        }

        return $rv;
    }
}

=head2 assert_valid_session

Determine wether this session can be committed.

Check if there is no other active casetype with the same name.
When updating an existing casetype, obviously exclude that one.

=cut

sub assert_valid_session {
    my ($self, $session) = @_;

    my $existing = $self->dbic->resultset('Zaaktype')->search({
        'me.active' => 1,
        'zaaktype_node_id.titel' => $session->{node}->{titel},
    }, {
        join => 'zaaktype_node_id'
    });

    if (my $casetype_id = $session->{zaaktype}->{id}) {
        my $count = $existing->search({
            'me.id' => { '!=' => $casetype_id }
        })->count;
        if ($count) {
            $self->log->info('assert_valid_session: Found existing zaaktype');
            # throw("casetype/commit_session/invalid_session", "Zaaktype id already in use.")
        }
    }
}

=head2 verwijder

=cut

{
    Params::Profile->register_profile(
        method  => 'verwijder',
        profile => {
            required        => [ qw/
            /],
            'optional'      => [ qw/
                as_session
                as_clone
                commit_message
            /],
            'require_some'  => {
                'zaaktype_id_or_node_id' => [
                    1,
                    'id',
                    'nid'
                ],
            },
            'constraint_methods'    => {
                'id'    => qr/^\d+$/,
                'nid'   => qr/^\d+$/,
            },
        }
    );

    sub verwijder {
        my ($self, %opts) = @_;
        my ($nid);

        my $dv = Params::Profile->check(
            params  => \%opts
        );

        do {
            $self->log->warn(
                'verwijder: invalid options' . Dumper $dv
            );
            return;
        } unless $dv->success;

        ### Retrieve resultset
        my ($zt_node);
        if ($opts{id}) {
            $zt_node = $self->_retrieve_node_by_zaaktype_id($opts{id});
        } else {
            $zt_node = $self->_retrieve_node_by_zaaktype_node_id($opts{nid});
        }

        my $zt = $zt_node->zaaktype_id;

        $zt->assert_delete;

        $zt_node->active(undef);
        $zt_node->deleted(DateTime->now);
        $zt_node->update;

        $zt->set_deleted;
        $zt->_sync_object($self->object_model);

        my $event = $self->dbic->resultset('Logging')->trigger(
            'casetype/remove',
            {
                component    => LOGGING_COMPONENT_ZAAKTYPE,
                component_id => $zt->id,
                data         => {
                    casetype_id => $zt->id,
                    reason      => $opts{'commit_message'} // 'Geen reden opgegeven',
                }
            }
        );

        return $event;

    }
}

=head2 export

=cut

{
    Params::Profile->register_profile(
        method  => 'export',
        profile => {
            required        => [ qw/
                id
            /],
            'constraint_methods'    => {
                'id'    => qr/^\d+$/,
            },
        }
    );

    sub export {
        my ($self, %opts) = @_;

        my $dv = Params::Profile->check(
            params  => \%opts
        );

        do {
            $self->log->warn(
                'Zaaktype->export: invalid options'
            );
            return;
        } unless $dv->success;

        my $zt_node = $self->_retrieve_node_by_zaaktype_id($opts{id});
        my $session = $self->_retrieve_as_session($zt_node);

        delete($session->{zaaktype}->{id});
        $session->{node}->{version} = 1;

        return $session;
    }
}

=head2 process_bibliotheek_notificaties

notificaties (emails) can have references the fields (bibliotheek_kenmerken)
that will be used for attachments

these need to be included in the export. they are stored using a link
table. add the attachment-field to the notification.

for now i will not add fields if they are not present in the casetype to begin
with because the target system would not benefit since the field is not included.
the goal is to replicate the exact behaviour that the casetype has, which is
met this way.

=cut

sub process_bibliotheek_notificaties {
    my ($self, $hash, $db_row) = @_;

    # these are fields that can be attached to an e-mail.
    $hash->{attachment_bibliotheek_kenmerken_ids} = [
        map { $_->get_column('bibliotheek_kenmerken_id') }
        $db_row->bibliotheek_notificatie_kenmerks->all
    ];
}

=head2 process_bibliotheek_kenmerken

=cut

sub process_bibliotheek_kenmerken {
    my ($self, $hash, $db_row) = @_;

    # from januari 2014, bibliotheek_kenmerken_values have sort_order and active
    # properties, allowing for more elaborate maintenance. older zaaksysteem exports
    # don't know about these fields. therefore we present both options. newer
    # version will use "extended_options".
    my $options = $db_row->options();

    $hash->{options}            = [map { $_->{value} } @$options];
    $hash->{extended_options}   = $options;
    $hash->{properties}         = $db_row->properties;

    delete $hash->{file_metadata_id};

    $hash->{metadata} = { $db_row->file_metadata_id->get_columns }
        if $db_row->file_metadata_id;

}

=head2 list_child_casetypes

Return a list, enabling the caller to update one by one
to allow progress monitoring.

=cut

define_profile list_child_casetypes => (
    required => {
        mother => 'HashRef',
    },
);

sub list_child_casetypes {
    my $self      = shift;
    my $arguments = assert_profile(shift)->valid;

    my $mother = $arguments->{mother};
    my $child_casetypes = $mother->{node}->{properties}->{child_casetypes} // [];

    return grep {$_->{import}} @{$child_casetypes};
}

=head2 update_child_casetype

Apply settings of a mother casetype to linked child casetype.

=cut

define_profile update_child_casetype => (
    required => {
        mother           => 'HashRef',
        child_settings   => 'HashRef',
    },
    optional => {
        commit_message   => 'Any',
        commit_component => 'Any',
    },
);

sub update_child_casetype {
    my $self      = shift;
    my $arguments = assert_profile(shift)->valid;

    my $mother    = $arguments->{mother};
    my $settings  = $arguments->{child_settings};
    my $child     = $self->retrieve(id => $settings->{casetype}{id}, as_session => 1);


    apply_all_roles($self, 'Zaaksysteem::Zaaktypen::Children');
    $self->apply_mother_settings($mother, $child, $settings);

    my $commit_message = ' via moederzaaktype' . $arguments->{commit_message} . $self->child_messages($settings) . '.';

    $self->commit_session(
        session          => $child,
        commit_message   => $commit_message,
        commit_component => $arguments->{commit_component},
    );
}

=head2 list_abandoned_casetypes

Return a list, enabling the caller to update one by one
to allow progress monitoring.

=cut

define_profile list_abandoned_casetypes => (
    required => {
        mother => 'HashRef',
    },
);

sub list_abandoned_casetypes {
    my $self      = shift;
    my $arguments = assert_profile(shift)->valid;

    my $mother = $arguments->{mother};
    my $child_casetypes = $mother->{node}->{abandoned_casetypes} // [];

    return @{$child_casetypes};
}

=head2 abandon_child_casetype

Remove a mother casetype to linked child casetype. We do not considder this as a
new version off the casetype, we only remove informational data. Also, this will
be applied insantly, rather then a session that needs to be commited for reason
descibed above.
If a mother has been given, it will only be possible if the child-casetype has
the same mother-casetype-id as that the mother has; Only true mother can abandon
child-casetypes.

=cut

define_profile abandon_child_casetype => (
    required => {
        child_settings   => 'HashRef',
    },
    optional => {
        mother           => 'HashRef',
        commit_message   => 'Any',
        commit_component => 'Any',
    },
);

sub abandon_child_casetype {
    my $self      = shift;
    my $arguments = assert_profile(shift)->valid;

    my $mother    = $arguments->{mother};
    my $settings  = $arguments->{child_settings};

    my $child     = $self->retrieve(id => $settings->{casetype}{id}, as_session => 0);

    if ( $mother and $mother->{zaaktype}->{id} ne $child->moeder_zaaktype_id->id ) {
        $self->log->warn(
            sprintf(
                "abandon_child_casetype: Can not abandon child-casetype '%d'," .
                " current mother-casetype-id '%d' and this mother-casetype-id '%d' differ",
                $settings->{casetype}{id},
                $mother->{zaaktype}->{id},
                $child->moeder_zaaktype_id->id,
            )
        );
        return
    }

    $child->moeder_zaaktype_id(undef);
    return $child->update
}

=head2 child_messages

=cut

sub child_messages {
    my ($self, $settings) = @_;

    my $options = ZAAKSYSTEEM_CONSTANTS->{CHILD_CASETYPE_OPTIONS};

    # the angular frontend benefits from separation, we're being a
    # good sport here.
    my $middles_phases = {
        "value" => "middle_phases",
        "label" => "Overige fasen (inclusief toewijzing)"
    };

    my @selected = grep {
        my $value = $_->{value};
        $settings->{$value}
    } (@$options, $middles_phases);

    my $selected = join ", ", map { $_->{label} } @selected;
    return ". Onderdelen overgenomen: " . ($selected || '-');
}

=head2 import_inavigator_casetype

Update a casetype using settings from I-Navigator import

The process is: Import XML file with settings,
Edit settings in the GUI, then Commit the changes


This function implements the changes per casetype.

=cut

define_profile import_inavigator_casetype => (
    required => [qw/
        action
        casetype_id
        settings
    /],
    optional => [qw/
        commit_message
    /],
    constraint_methods => {
        action => qr/^(create|update)$/,
    }
);

sub import_inavigator_casetype {
    my $self      = shift;
    my $arguments = assert_profile(shift)->valid;

    my $casetype = $self->retrieve(
        id         => $arguments->{casetype_id},
        as_clone   => $arguments->{action} eq 'create',
        as_session => 1,
    ) or throw (
        'zaaktypen/import_inavigator_casetype',
        'Unable to retrieve casetype with id ' . $arguments->{casetype_id}
    );

    apply_all_roles($self, 'Zaaksysteem::Zaaktypen::INavigator');

    $self->apply_inavigator_settings({
        casetype => $casetype,
        settings => $arguments->{settings}
    });

    my $commit_message = ' via I-Navigator';# . $arguments->{commit_message};
    # to figure out: what kind of messages do we need here?
    # or log in transaction table?
    # . $self->child_messages($settings) . '.';

    my $zaaktype_node = $self->commit_session(
        session => $casetype,
        commit_message => $commit_message
    );

    return $zaaktype_node->get_column('zaaktype_id');
}

=head2 casetype_actions

=cut

sub casetype_actions {
    return [
        { head => 'Formulier' },
        { field => 'node.aanvrager_hergebruik', label => 'Zaakgegevens hergebruiken'},
        { field => 'node.webform_toegang', label => 'Aanvragen met publiek webformulier'},
        { field => 'node.online_betaling', label => 'Internetkassa gebruiken'},
        { field => 'node.properties.offline_betaling', label => 'Anders betalen toestaan'},
        { field => 'node.contact_info_email_required', label =>'E-mailadres verplicht bij webformulier'},
        { field => 'node.contact_info_phone_required', label =>'Telefoonnummer verplicht bij webformulier'},
        { field => 'node.contact_info_mobile_phone_required', label =>'Mobiel telefoonnummer verplicht bij webformulier'},
        { field => 'node.properties.no_captcha', label => 'Captcha uitschakelen bij vooringevulde aanvrager'},
        { head => 'Intern formulier' },
        { field => 'node.properties.confidentiality', label => 'Toon vertrouwelijkheid'},
        { field => 'node.contact_info_intake', label =>'Contactgegevens tonen bij intake'},
        { field => 'node.extra_relaties_in_aanvraag', label => 'Mogelijkheid om relaties toe te voegen bij zaakintake'},
        { head => 'Toewijzing aanpassing bij interne intake' },
        { field => 'node.automatisch_behandelen', label => 'Zaak automatisch in behandeling nemen'},
        { field => 'node.toewijzing_zaakintake', label => 'Toewijzing tonen bij zaakintake'},
        { head => 'Zaakdossier' },
        { field => 'node.prevent_pip', label => 'Zaak niet op aanvrager PIP publiceren'},
        { field => 'node.properties.lock_registration_phase', label => 'Registratiefase vergrendelen'},
        { field => 'node.properties.queue_coworker_changes', label => "Kenmerk- en documentwijzigingen van collega's in wachtrij plaatsen"},
        { field => 'node.properties.check_permissions_for_assignee', label => 'Rechten valideren bij toewijzing wijzigen' },
        { head => 'API instellingen' },
        { field => 'node.properties.api_can_transition', label => 'Zaaksysteem API mag fase afronden' },
        { field => 'node.is_public', label => 'Zaakgegevens zijn publiekelijk beschikbaar'}
    ];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
