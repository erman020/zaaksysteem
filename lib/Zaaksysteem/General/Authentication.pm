package Zaaksysteem::General::Authentication;

use strict;
use warnings;

use Data::Dumper;

use Scalar::Util qw/blessed/;

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_AUTHORIZATION_ROLES
    ZAAKSYSTEEM_AUTHORIZATION_PERMISSIONS
    ZAAKSYSTEEM_CONSTANTS
/;
use Zaaksysteem::Constants::Users qw(:all);

use BTTW::Tools qw(throw);

=head2 assert_user

Asserts if the user is a C<PIP>, C<API> or C<REGULAR> user. You can
import these values from L<Zaaksysteem::Constants::Users>

    $c->assert_user(); # Defaults to a REGULAR user

    $c->assert_user(PIP) # user must be a PIP user

    $c->assert_user(PIP|REGULAR) user must be PIP or REGULAR

=cut

sub assert_user {
    my ($c, $scope) = @_;

    unless ($scope) {
        $scope = REGULAR;
    }

    return if $c->check_user_mask($scope);

    throw(
        'zaaksysteem/unauthorized_user',
        'Unauthorized: authentication required',
        { http_code => 401 }
    );
}

=head2 check_user_mask

Checks if the user, if existant and logged in, has specific authorization
properties.

    if ($c->check_user_mask(API)) {
        # User is an API delegate
    }

    # Bail out unless user is any kind of admin
    return unless $c->check_user_mask(ADMIN);

=cut

sub check_user_mask {
    my ($c, $scope) = @_;

    return unless defined $scope;

    # Ensure proper bit context
    $scope |= 0;

    if ($scope & FORM) {
        return 1 if !$c->user_exists && $c->session->{form};
    }

    if ($scope & PIP) {
        return 1 if !$c->user_exists && $c->session->{pip};
    }

    if ($scope & API) {
        return 1 if $c->user_exists && $c->user->is_external_api;
    }

    if ($scope & REGULAR) {
        return 1 if $c->user_exists && !$c->user->is_external_api;
    }

    if ($scope & ADMIN) {
        return 1 if $c->user_exists && $c->check_any_user_permission('admin');
    }

    return;
}

sub assert_zaak_permission {
    my $c = shift;

    return 1 if $c->check_zaak_permission(@_);

    $c->log->debug("assert_zaak_permission: denied");
    $c->forward('/forbidden');
    $c->detach();

    return;
}

sub betrokkene_object {
    my $c     = shift;

    my $bid   = $c->user->uidnumber or return;

    return $c->model('Betrokkene')->get(
        {
            extern  => 1,
            type    => 'medewerker',
        },
        $bid
    );
}

sub check_zaak_permission {
    my ($c, $permission) = @_;

    return unless $c->stash->{zaak};
    return unless $c->user_exists;

    my $bid     = $c->user->uidnumber or return;


    my $bo      = $c->model('Betrokkene')->get(
        {
            extern  => 1,
            type    => 'medewerker',
        },
        $bid
    );

    return unless $bo->org_eenheid;

    my $orgid = $bo->org_eenheid->id;

    if (
        $c->stash->{zaak}->kenmerk->behandelaar &&
        $c->stash->{zaak}->kenmerk->behandelaar->id eq $bo->id
    ) {
        return 1;
    }

    if (
        $c->stash->{zaak}->kenmerk->zaakeigenaar &&
        $c->stash->{zaak}->kenmerk->zaakeigenaar->id eq $bo->id
    ) {
        return 1;
    }

#    if (
#        $c->stash->{zaak}->kenmerk->aanvrager &&
#        $c->stash->{zaak}->kenmerk->aanvrager->id eq $bo->id
#    ) {
#        return 1;
#    }
#
    if (
        $c->check_user_role('manager') &&
        $c->stash->{zaak}->kenmerk->aanvrager &&
        $c->stash->{zaak}->kenmerk->aanvrager->id eq $bo->id
    ) {
        return 1;
    }

    if (
        $c->stash->{zaak}->kenmerk->org_eenheid &&
        $c->stash->{zaak}->kenmerk->org_eenheid->id eq $bo->org_eenheid->id
    ) {
        return 1;
    }


    $c->forward('/forbidden');
    $c->detach();

    return;
}

sub assert_permission {
    my $c = shift;

    return 1 if $c->check_permission(@_);

    $c->log->debug("assert_permission: denied");
    $c->forward('/forbidden');
    $c->detach();

    return;
}

sub check_permission {
    my ($c, @roles)  = @_;


    if ($c->check_user_role(qw/admin/)) {
        return 1;
    }

    return unless $c->stash->{zaak};

    if (
        blessed($c->stash->{zaak}->kenmerk->zaakeigenaar) &&
        $c->stash->{zaak}->kenmerk->zaakeigenaar->ex_id == $c->user->uidnumber
    ) {
        return 1;
    }

    my $success;
    for my $role (@roles) {
        return unless ZAAKSYSTEEM_CONSTANTS->{'authorisation'}
            ->{rechten}->{$role};

        my $auths = $c->stash->{zaak}->zaaktype_id->zaaktype_authorisations
            ->search(
                {
                    recht   => $role,
                }
            );

        next unless $auths->count;

        while (my $auth = $auths->next) {
            $success = $auth->recht if $c->user->member_of_group_id($auth->group_id)
        }
    }

    return unless $success;

    return 1;
}

### XXX BELOW IS NEW STYLE
sub _get_ldap_role {
    my ($self, $role) = @_;

    return ZAAKSYSTEEM_AUTHORIZATION_ROLES->{ $role }->{'ldapname'};
}


=head2 check_any_user_permission

Given a list of permissions, find out if the user
has at least one of these, hence 'any'

=cut

sub check_any_user_permission {
    my ($c, @check_permissions) = @_;

    return unless $c->user_exists;

    my $user_permissions = $c->get_user_permissions;

    # admin always gets access
    return grep { $user_permissions->{$_} } ('admin', @check_permissions);
}


sub assert_any_user_permission {
    my ($c, @permissions) = @_;

    return 1 if $c->check_any_user_permission(@permissions);

    $c->log->debug("assert_any_user_permission: denied");

    $c->detach('/forbidden');
}

=head2 check_any_zaak_permission

cached version, to lessen traffic to LDAP server.

alternative caching strategy is to get all permissions and then consult
the cached lookup. the penalty is for smaller json requests that only call
this function once.

=cut

sub check_any_zaak_permission {
    my ($c, @check_permissions) = @_;

    return $c->check_any_given_zaak_permission($c->stash->{zaak}, @check_permissions);
}


=head2 check_any_given_zaak_permission

need to be able to call this from lists of cases. so per case the whole has to
executed.

the historic function signature makes it a bit hard to overload, we could look
if @_[1] is a hashref or something but i rather fix it with a different function
name - much less debugging nightmares.

'given' - indicates the zaak is specifically passed to the function instead
being taken from the stash, so it refers to the case rather than to the permission.
anybody got a catchier name?

=cut

sub check_any_given_zaak_permission {
    my ($c, $zaak, @check_permissions) = @_;

    # stash is cleared after each request
    my $cache_key = '_cache_check_any_zaak_permission - zaak: ' . $zaak->id . ' - ' . join ",", sort @check_permissions;

    return $c->stash->{$cache_key} if exists $c->stash->{$cache_key};
    return $c->stash->{$cache_key} = $c->retrieve_any_zaak_permission($zaak, @check_permissions);
}


=head2 retrieve_any_zaak_permission

do the actual check for case permissions. this means check
if the currently logged in user has certain rights on a
given case. these are criteria:

- actual logged in user with at least one defined role
- admins always have access to anything
- case staff have edit and read access
- others users must be in an organizational unit to have access
- after that only users that have explicit rights through case type
  management have access.

=cut

sub retrieve_any_zaak_permission {
    my ($c, $zaak, @check_permissions) = @_;

    return unless $zaak && $c->user_exists;

    ### First, check if user is allowed to view zaken
    unless ($c->check_any_user_permission('gebruiker')) {
        $c->push_flash_message('Geen toegang: u heeft geen rechten om zaken te bekijken [!gebruiker]');
        return;
    }

    return 1 if $c->check_any_user_permission('admin');

    my $betrokkene = $c->betrokkene_object or return;

    # staff related to the case have access
    return 1 if
        (grep { $_ eq 'zaak_read' } @check_permissions) &&
        $c->is_case_staff($zaak, $betrokkene);

    # Behandelaar can edit always
    return 1 if
        (grep { $_ eq 'zaak_edit' } @check_permissions) &&
        ($c->is_behandelaar($zaak, $betrokkene));

    # Coordinator can edit when behandelaar is set
    return 1 if (
        (grep { $_ eq 'zaak_edit' } @check_permissions) &&
        ($zaak->behandelaar && $c->is_coordinator($zaak, $betrokkene))
    );

    my $authorisations  = $c->get_cached_zaaktype_authorisations($zaak->get_column('zaaktype_id'));

    my $user_role_ids   = [ map { $_->id } @{ $c->user->roles } ];

    my $parent_ou_ids   = [ map { $_->id } @{ $c->user->primary_groups }, @{ $c->user->inherited_groups } ];

    ### Casetype authorisations
    foreach my $authorisation (@$authorisations) {

        # make distinction for authorisations for confidential cases
        next unless ($zaak->confidentiality eq 'confidential') == $authorisation->{confidential};

        # if any of the parent_ou_ids (which includes mine) has rights,
        # i'm able to access this.
        if ($authorisation->{ou_id} && !grep {$authorisation->{ou_id} eq $_ } @$parent_ou_ids) {
            next;
        }

        # if this is an authorisation that applies to us,
        # then see if it is a right that we were wondering about.
        my $role_permission = grep { $authorisation->{role_id} eq $_ } @$user_role_ids;
        my $right = grep { $authorisation->{recht} eq $_ } @check_permissions;

        return 1 if $role_permission && $right;
    }

    my %permission_map = (
        'zaak_read'     => 'read',
        'zaak_search'   => 'search',
        'zaak_edit'     => 'write',
        'zaak_beheer'   => 'manage'
    );

    ### Case authorisations
    my @case_authorisations = $zaak->zaak_authorisations->search->all;
    foreach my $authorisation (@case_authorisations) {
        next unless ($authorisation->scope eq 'instance' and $authorisation->entity_type eq 'position');

        my ($ou_id, $role_id) = split('\|', $authorisation->entity_id);

        # if any of the parent_ou_ids (which includes mine) has rights,
        # i'm able to access this.
        if (!grep { $ou_id eq $_ } @$parent_ou_ids) {
            next;
        }

        # if this is an authorisation that applies to us,
        # then see if it is a right that we were wondering about.
        my $role_permission = grep { $role_id eq $_ } @$user_role_ids;
        my $right = grep { $authorisation->capability eq $permission_map{$_} } @check_permissions;

        return 1 if $role_permission && $right;
    }
}

=head2 retrieve_list_of_zaak_permissions

Will return a list of permissions for the given user

=cut

sub retrieve_list_of_zaak_permissions {
    my ($c, $zaak) = @_;
    my %rv;

    my $map = {
        admin   => {
            zaak_read   => 1,
            zaak_edit   => 1,
            zaak_beheer => 1,
            zaak_search => 1,

            case_allocate => 1,
        },
        edit   => {
            zaak_read   => 1,
            zaak_edit   => 1,
            zaak_search => 1,

            case_allocate => 1,
        },
        read   => {
            zaak_read   => 1,
            zaak_search => 1,
        },
        search   => {
            zaak_search => 1,
        },
        allocate => {
            case_allocate => 1,
        }
    };

    return [] unless ($zaak && $c->user_exists);

    ### First, check if user is allowed to view zaken
    unless ($c->check_any_user_permission('gebruiker')) {
        $c->push_flash_message('Geen toegang: u heeft geen rechten om zaken te bekijken [!gebruiker]');
        return [];
    }

    return [ keys %{ $map->{admin} } ] if $c->check_any_user_permission('admin');

    my $betrokkene = $c->betrokkene_object or return;

    # staff related to the case have access
    if ($c->is_case_staff($zaak, $betrokkene)) {
        %rv = (%rv, %{ $map->{read} });
    }

    if ($c->is_behandelaar($zaak, $betrokkene)) {
        %rv = (%rv, %{ $map->{edit} });
    }

    if ($c->is_coordinator($zaak, $betrokkene)) {
        %rv = (%rv, %{ $map->{edit} });
    }

    my $authorisations  = $c->get_cached_zaaktype_authorisations($zaak->get_column('zaaktype_id'));

    my $user_role_ids   = [ map { $_->id } @{ $c->user->roles } ];

    my $parent_ou_ids   = [ map { $_->id } @{ $c->user->primary_groups }, @{ $c->user->inherited_groups } ];

    foreach my $authorisation (@$authorisations) {

        # make distinction for authorisations for confidential cases
        next unless ($zaak->confidentiality eq 'confidential') == $authorisation->{confidential};

        # if any of the parent_ou_ids (which includes mine) has rights,
        # i'm able to access this.
        if ($authorisation->{ou_id} && !grep {$authorisation->{ou_id} eq $_ } @$parent_ou_ids) {
            next;
        }

        # if this is an authorisation that applies to us,
        # then see if it is a right that we were wondering about.
        my $role_permission = grep { $authorisation->{role_id} eq $_ } @$user_role_ids;

        $rv{ $authorisation->{recht} } = 1 if ($role_permission);
    }

    my %permission_map = (
        'read'      => 'zaak_read',
        'search'    => 'zaak_search',
        'write'     => 'zaak_edit',
        'manage'    => 'zaak_beheer',
    );

    ### Case authorisations
    my @case_authorisations = $zaak->zaak_authorisations->search->all;
    foreach my $authorisation (@case_authorisations) {
        next unless ($authorisation->scope eq 'instance' and $authorisation->entity_type eq 'position');

        my ($ou_id, $role_id) = split('\|', $authorisation->entity_id);

        # if any of the parent_ou_ids (which includes mine) has rights,
        # i'm able to access this.
        if (!grep { $ou_id eq $_ } @$parent_ou_ids) {
            next;
        }

        # if this is an authorisation that applies to us,
        # then see if it is a right that we were wondering about.
        my $role_permission = grep { $role_id eq $_ } @$user_role_ids;

        $rv{ $permission_map{$authorisation->capability} } = 1 if $role_permission;
    }

    ### Edits only allowed when you are a behandelaar or when behandelaar is set and you are coordinator
    if (
        !$rv{zaak_beheer} && !$zaak->behandelaar && $rv{zaak_edit}
    ) {
        delete($rv{zaak_edit});
        %rv = (%rv, %{ $map->{allocate} });
    }

    return [ keys %rv ];
}


sub get_cached_zaaktype_authorisations {
    my ($c, $zaaktype_id) = @_;

    my $cache_key = '_zaaktype_authorisations_cache - zaaktype_id: ' . $zaaktype_id;

    return $c->stash->{$cache_key} ||= $c->retrieve_zaaktype_authorisations($zaaktype_id);
}

sub retrieve_zaaktype_authorisations {
    my ($c, $zaaktype_id) = @_;

    my $rs = $c->model('DB::ZaaktypeAuthorisation');
    my @authorisations = $rs->search({zaaktype_id => $zaaktype_id});

    return [map { {$_->get_columns} } @authorisations];
}

=head2 get_user_role_ids

User roles are identified by numbers (e.g. 20002)
Catalyst gives us names. We contact the ldap server for the complete
config to obtain the matching names.

=cut

sub get_user_role_ids {
    my ($c) = @_;

    my $cache_key = '__cache_user_role_ids';

    return exists $c->stash->{$cache_key} ?
        $c->stash->{$cache_key} :
        $c->stash->{$cache_key} = $c->fetch_user_role_ids;
}


sub fetch_user_role_ids {
    my ($c)         = @_;
    my @roles;

    push(
        @roles,
        {
            role_id     => $_->id,
            name        => $_->name,
        }
    ) for @{ $c->user->roles };

    return \@roles;
}

=head2 is_case_staff

behandelaars, coordinators and aanvragers who are medewerker always have
read/write access to their cases.

=cut

sub is_case_staff {
    my ($c, $zaak, $betrokkene) = @_;

    return (
        $zaak->behandelaar &&
        $zaak->behandelaar->gegevens_magazijn_id eq $betrokkene->ldapid
    ) || (
        $zaak->coordinator &&
        $zaak->coordinator->gegevens_magazijn_id eq $betrokkene->ldapid
    ) || (
        $zaak->aanvrager &&
        $zaak->aanvrager->gegevens_magazijn_id eq $betrokkene->ldapid &&
        $zaak->aanvrager->betrokkene_type eq 'medewerker'
    );
}

=head2 is_behandelaar

Returns true if the specified user is "behandelaar" of the specified case.

=cut

sub is_behandelaar {
    my ($c, $zaak, $betrokkene) = @_;

    return (
        $zaak->behandelaar &&
        $zaak->behandelaar->gegevens_magazijn_id eq $betrokkene->ldapid
    );
}

=head2 is_coordinator

Returns true if the specified user is "behandelaar" of the specified case.

=cut

sub is_coordinator {
    my ($c, $zaak, $betrokkene) = @_;

    return (
        $zaak->coordinator &&
        $zaak->coordinator->gegevens_magazijn_id eq $betrokkene->ldapid
    );
}

sub assert_any_zaak_permission {
    my ($c, @permissions) = @_;

    return 1 if $c->check_any_zaak_permission(@permissions);

    $c->log->debug("assert_any_zaak_permission: denied");
    $c->forward('/forbidden');
    $c->detach();

    return;
}


=head2 get_user_permissions

In Constants.pm, there lives a huge structure that maps specific permissions
to user roles. Say Bob is a 'Behandelaar', then the structure tells us he
has certain rights. Then if Bob also happens to be 'Zaaktypebeheerder', that
also buys him rights. All these rights are added up, allowing
us to see if Bob has a specific right.

=cut

sub get_user_permissions {
    my ($c) = @_;

    return {} unless $c->user_exists;

    my @user_roles          = @{ $c->user->roles };
    my $roles_configuration = ZAAKSYSTEEM_AUTHORIZATION_ROLES;

    my $user_permissions = {};

    for my $group (values %{ $roles_configuration }) {
        # filter the groups that Bob is part of
        next unless grep { $_->name eq $group->{ldapname} } @user_roles;

        # add the group's rights
        for my $right (keys %{$group->{rechten}->{global}}) {
            $user_permissions->{$right} = 1;
        }
    }

    return $user_permissions;
}

sub check_user_role {
    my ($c, @roles) = @_;

    return unless $c->user_exists;

    my @ldaproles = @{ $c->user->roles };

#    $c->log->debug(
#        'AUTH: Requesting check for roles: '
#        . join(', ', @roles)
#    );

    my $zs_auth_roles   = ZAAKSYSTEEM_AUTHORIZATION_ROLES;

    my @permissions;
    for my $check_role (@roles) {
        next unless $zs_auth_roles->{$check_role};

        if (grep {
                $_->name eq $zs_auth_roles->{$check_role}->{ldapname}
            } @ldaproles
        ) {
#            $c->log->debug('AUTH: Found role: '
#                . $check_role . ' in LDAP store'
#            );

            return 1;
        }
    }

#    $c->log->debug(
#        'AUTH: Did not find requested roles'
#        . ' in LDAP store'
#    );

    return;
}

sub list_available_permissions {
    my ($c) = @_;

    return ZAAKSYSTEEM_AUTHORIZATION_PERMISSIONS;
}

sub assert_user_role {
    my ($c, @roles) = @_;

    return 1 if $c->check_user_role(@roles);

    $c->log->debug("assert_user_role: denied");
    $c->forward('/forbidden');
    $c->detach();

    return;
}

sub user_betrokkene {
    my ($c) = @_;

    return unless (ref($c) && $c->user_exists && $c->user);

    my $betrokkene_id   = $c->user->uidnumber;

    my $betrokkene_obj  = $c->model('Betrokkene')->get(
        {
            extern  => 1,
            type    => 'medewerker',
        },
        $betrokkene_id
    );

    return unless $betrokkene_obj;
    return $betrokkene_obj;
}

sub user_ou_id {
    my ($c) = @_;

    my $betrokkene_obj  = $c->user_betrokkene;

    return unless $betrokkene_obj;

    return $betrokkene_obj->org_eenheid->id
        if $betrokkene_obj->org_eenheid;

    return;
}

sub user_roles {
    my ($c) = @_;

    return unless (ref($c) && $c->user_exists && $c->user);

    return map({ $_->name } @{ $c->user->roles });
}

sub user_roles_ids {
    my ($c) = @_;

    return map { $_->id } @{ $c->user->roles };
}


=head2 check_field_permission

Check if the user has permission to work this field. algorithm:

- Check case permissions (we have those, otherwise we wouldn't have landed here)
- Check if this kenmerk is guarded by an additional role that we need to have.
- If so, check if we play this role. (do we wear this hat?)

=cut

sub check_field_permission {
    my ($c, $zaaktype_kenmerk) = @_;

    my $required_permissions = $zaaktype_kenmerk->required_permissions_decoded;

    return 1 unless @$required_permissions;

    my $user_role_ids   = $c->get_user_role_ids;
    my $parent_ou_ids   = $c->get_user_parent_ou_ids;
    my $all_groups      = $c->model('DB::Groups')->get_all_cached($c->stash);
    my $all_roles       = $c->model('DB::Roles')->get_all_cached($c->stash);

    # populate this with labels
    foreach my $required (@$required_permissions) {
        # cache in stash so we only have to do it once per request
        # we could do better by doing this in the get_ou_by_id sub
        my $ou_id   = $required->{org_unit_id};
        my $role_id = $required->{role_id};

        next unless $ou_id && $role_id;

        my ($group) = grep { $_->id == $ou_id } @$all_groups;
        my ($role)  = grep { $_->id == $role_id } @$all_roles;

        unless ($group && $role) {
            next;
        }

        $zaaktype_kenmerk->{ou_names}->{$ou_id} = $group->name;
        $zaaktype_kenmerk->{role_names}->{$role_id} = $role->name;
    }

    return 1 if $c->check_any_user_permission('admin');

     # any of these will suffice
    foreach my $required (@$required_permissions) {

        return 1 if
            (grep { $required->{org_unit_id} eq $_->id } @$parent_ou_ids) &&
            (grep { $required->{role_id}     eq $_->{role_id} } @$user_role_ids);
    }

    # explicit return to refuse access
    return;
}

=head2 assert_field_permission

Performs L</check_field_permission> and throws up a "Forbidden" reply if the
check fails.

=cut

sub assert_field_permission {
    my $c = shift;

    return 1 if $c->check_field_permission(@_);

    $c->log->debug("assert_field_permission: denied");
    $c->forward('/forbidden');
    $c->detach();

    return;
}

sub get_user_parent_ou_ids {
    my ($c) = @_;

    my $cache_key = '__cache_user_parent_ou_ids';

    return exists $c->stash->{$cache_key} ?
        $c->stash->{$cache_key} :
        $c->stash->{$cache_key} = $c->fetch_parent_ou_ids;
}


sub fetch_parent_ou_ids {
    my ($c) = @_;

    return [ @{ $c->user->primary_groups }, @{ $c->user->inherited_groups } ];
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_AUTHORIZATION_PERMISSIONS

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_AUTHORIZATION_ROLES

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 assert_any_user_permission

TODO: Fix the POD

=cut

=head2 assert_any_zaak_permission

TODO: Fix the POD

=cut

=head2 assert_permission

TODO: Fix the POD

=cut

=head2 assert_user_role

TODO: Fix the POD

=cut

=head2 assert_zaak_permission

TODO: Fix the POD

=cut

=head2 betrokkene_object

TODO: Fix the POD

=cut

=head2 check_permission

TODO: Fix the POD

=cut

=head2 check_user_role

TODO: Fix the POD

=cut

=head2 check_zaak_permission

TODO: Fix the POD

=cut

=head2 fetch_parent_ou_ids

TODO: Fix the POD

=cut

=head2 fetch_user_role_ids

TODO: Fix the POD

=cut

=head2 get_cached_zaaktype_authorisations

TODO: Fix the POD

=cut

=head2 get_user_parent_ou_ids

TODO: Fix the POD

=cut

=head2 list_available_permissions

TODO: Fix the POD

=cut

=head2 retrieve_zaaktype_authorisations

TODO: Fix the POD

=cut

=head2 user_betrokkene

TODO: Fix the POD

=cut

=head2 user_ou_id

TODO: Fix the POD

=cut

=head2 user_roles

TODO: Fix the POD

=cut

=head2 user_roles_ids

TODO: Fix the POD

=cut

=head2 user_roles_ldap

TODO: Fix the POD

=cut

