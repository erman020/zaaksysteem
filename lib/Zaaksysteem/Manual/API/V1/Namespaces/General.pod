=head1 NAME

Zaaksysteem::Manual::API::V1::Namespaces::General - General API reference
documentation

=head1 NAMESPACE URL

    /api/v1/general

=head1 DESCRIPTION

This page documents the endpoints within the C<general> API namespace.

=head1 ENDPOINTS

=head2 Config endpoints

=head3 C<GET /config>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<config|Zaaksysteem::Manual::API::V1::Types::Config> instances.

=head3 C<GET /config/[config:parameter]>

Returns a L<config|Zaaksysteem::Manual::API::V1::Types::Config> instance.

=head3 C<POST /config/create>

Returns an L<exception|Zaaksysteem::Manual::API::V1::Types::Exception>
instance.

=head3 C<POST /config/[config:parameter]/delete>

Returns an L<exception|Zaaksysteem::Manual::API::V1::Types::Exception>
instance.

=head2 Country code endpoints

=head3 C<GET /country_codes>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<country_code|Zaaksysteem::Manual::API::V1::Types::CountryCode> instances.

=head3 C<GET /country_codes/[country_code:id]>

Returns a L<country_code|Zaaksysteem::Manual::API::V1::Types::CountryCode>
instance.

=head3 C<POST /country_codes/create>

Returns an L<exception|Zaaksysteem::Manual::API::V1::Types::Exception>
instance.

=head3 C<POST /country_codes/[country_code:id]/delete>

Returns an L<exception|Zaaksysteem::Manual::API::V1::Types::Exception>
instance.

=head2 Legal entity type endpoints

=head3 C<GET /legal_entity_types>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<legal_entity_type|Zaaksysteem::Manual::API::V1::Types::LegalEntityType>
instances.

=head3 C<GET /legal_entity_types/[legal_entity_type:code]>

Returns a
L<legal_entity_type|Zaaksysteem::Manual::API::V1::Types::LegalEntityType>
instance.

=head3 C<POST /legal_entity_types/create>

Returns an L<exception|Zaaksysteem::Manual::API::V1::Types::Exception>
instance.

=head3 C<POST /legal_entity_types/[legal_entity_type:code]/delete>

Returns an L<exception|Zaaksysteem::Manual::API::V1::Types::Exception>
instance.

=head2 Meta endpoints

=head3 C<GET /meta>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<meta|Zaaksysteem::Manual::API::V1::Types::Meta> instances.

=head3 C<GET /meta/[meta:id]>

Returns an L<exception|Zaaksysteem::Manual::API::V1::Types::Exception>
instance.

=head3 C<POST /meta/create>

Returns an L<exception|Zaaksysteem::Manual::API::V1::Types::Exception>
instance.

=head3 C<POST /meta/[meta:id]/delete>

Returns an L<exception|Zaaksysteem::Manual::API::V1::Types::Exception>
instance.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
