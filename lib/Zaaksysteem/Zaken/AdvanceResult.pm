package Zaaksysteem::Zaken::AdvanceResult;

use Moose;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Zaken::AdvanceResult - Data wrapper for case transition checks

=head1 SYNOPSIS

    my $res = Zaaksysteem::Zaken::AdvanceResult->new;

    if ($something_is_not_ok) {
        $res->fail('something', 'not okay because ...');
    } else {
        $res->ok('something');
    }

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 transition_states

This attribute wraps a map of statenames and associated boolean values.

=head3 Proxied methods

=over 4

=item set_state

=item get_state

=item state_values

=item states

=back

=cut

has transition_states => (
    is => 'rw',
    isa => 'HashRef[Bool]',
    traits => [qw[Hash]],
    default => sub { {} },
    handles => {
        set_state => 'set',
        get_state => 'get',
        state_values => 'values',
        states => 'kv'
    }
);

=head2 transition_state_reasons

This attribute wraps a map of statenames and associated check-failure reasons

Even though a check may fail
(C<$advance_result->transition_states->{ check } == 0), a reason need not be
specified.

=over 4

=item set_reason

=item get_reason

=item clear_reason

=item reasons

=back

=cut

has transition_state_reasons => (
    is => 'rw',
    isa => 'HashRef[Str]',
    traits => [qw[Hash]],
    default => sub { {} },
    handles => {
        set_reason => 'set',
        get_reason => 'get',
        clear_reason => 'delete',
        reasons => 'values'
    }
);

=head2 missing

An array reference (optional) containing a list of magic strings for missing attributes

=cut

has missing => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    predicate => 'has_missing',
);

=head1 METHODS

=head2 fail

This method will set a failure state for the provided checkname.

    $advance_result->fail('checkname', 'reason');

=cut

sig fail => 'Str, ?Str';

sub fail {
    my ($self, $check, $reason) = @_;

    $self->transition_states->{ $check } = 0;

    if($reason) {
        $self->transition_state_reasons->{ $check } = $reason;
    }

    return $self;
}

=head2 ok

This method will set a success state for the provided checkname. It also
clears the L</transition_state_reason> map of reasons for the check.

=cut

sig ok => 'Str';

sub ok {
    my ($self, $check) = @_;

    $self->set_state($check, 1);
    $self->clear_reason($check);

    return $self;
}

=head2 can_advance

This method will return true-ish if there are no false-ish values in the
L</transition_states> mapping.

=cut

sub can_advance {
    my $self = shift;

    my $fault_count = scalar grep { not $_ } $self->state_values;

    return 0 if $fault_count;
    return 1;
}

=head2 TO_JSON

Implements the automagic serialization interface for L<JSON/encode>.

=cut

sub TO_JSON {
    my $self = shift;

    my %retval = (
        (
            map {
                $_->[0] => $_->[1] ? 1 : 0
            } $self->states
        ),

        ($self->has_missing) ? (missing_fields => $self->missing) : ()
    );

    return \%retval;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
