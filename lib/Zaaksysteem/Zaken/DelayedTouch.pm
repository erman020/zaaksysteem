package Zaaksysteem::Zaken::DelayedTouch;
use Moose;

use BTTW::Tools;
with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Zaken::DelayedTouch - Defines a touch object, for delayed touching
of cases from an external location, like Catalyst

=head1 SYNOPSIS

    my $touch_object = Zaaksysteem::Zaken::DelayedTouch->new;

    $touch_object->add_case(4434);

    ### Later
    my $db_schema = $c->model('DB');

    $touch_object->execute($db_schema);

=head1 DESCRIPTION

Delayes touching and indexing of cases by collecting all case numbers and touching at
the end of the execution cycle.

=head1 ATTRIBUTES

=head2 cases

Return value: $ARRAY_CASE_NUMBERS

    print join(',', @{ $touch_object->cases });

    # 424,343,2424,13

Returns a list of case numbers.

=cut

has cases => (
    is      => 'rw',
    isa     => 'ArrayRef[Zaaksysteem::Zaken::ComponentZaak]',
    traits  => [qw[Array]],
    default => sub { [] },
    handles => {
        all_cases => 'elements',
        map_cases => 'map',
        push_case => 'push'
    }
);

=head2 METHODS

=head2 add_case($CASE_NUMBER)

Return value: $COUNT_CASES

    print $touch_object->add_case(44);

    # 1

Adds a case to the delayed object for later touching.

=cut

sig add_case => 'Zaaksysteem::Zaken::ComponentZaak';

sub add_case {
    my $self = shift;
    my $case = shift;

    unless (grep { $_->id eq $case->id } $self->all_cases) {
        $self->push_case($case);
    }

    return map { $_->id } $self->all_cases;
}

=head2 execute($SCHEMA)

Return value: $BOOL_SUCCESS

    $touch_object->execute($BOOL_SUCCESS);

Retrieves every single case in C<< $touch_object->cases >> from the database, and runs
C<< $row->_touch >> on it.

=cut

sig execute => 'Zaaksysteem::Schema';

sub execute {
    my $self   = shift;
    my $schema = shift;

    my @cases;
    $self->map_cases(
        sub {
            my $case = $schema->resultset('Zaak')->find($_->id);
            if ($case) {
                $case->_touch;
                push(@cases, $case->id);
                return $case->id;
            }
        }
    );

    $self->cases([]);

    if ($self->log->is_trace && @cases) {
        $self->log->trace("Touched cases: " . join(', ', @cases));
    }

    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
