package Zaaksysteem::Zaken::ComponentZaak;

use Moose;

use Data::UUID;
use DateTime;
use List::MoreUtils qw[uniq any];
use List::Util qw(first);

use Zaaksysteem::ZTT;
use BTTW::Tools;
use Zaaksysteem::Constants;

use Zaaksysteem::Backend::Tools::Term qw/calculate_term/;
use Zaaksysteem::Backend::Rules;
use Zaaksysteem::Backend::Email;

extends 'DBIx::Class';

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Zaken::Roles::MetaObjecten
    Zaaksysteem::Zaken::Roles::BetrokkenenObjecten
    Zaaksysteem::Zaken::Roles::FaseObjecten
    Zaaksysteem::Zaken::Roles::DocumentenObjecten
    Zaaksysteem::Zaken::Roles::DeelzaakObjecten
    Zaaksysteem::Zaken::Roles::KenmerkenObjecten
    Zaaksysteem::Zaken::Roles::RouteObjecten
    Zaaksysteem::Zaken::Roles::ChecklistObjecten
    Zaaksysteem::Zaken::Roles::Acties
    Zaaksysteem::Zaken::Roles::Publish
    Zaaksysteem::Zaken::Roles::Export
    Zaaksysteem::Zaken::Roles::Fields
    Zaaksysteem::Zaken::Roles::Schedule
    Zaaksysteem::Zaken::Roles::ZTT
    Zaaksysteem::Zaken::Roles::HStore
    Zaaksysteem::Roles::Timer
);

=head1 NAME

Zaaksysteem::Zaken::ComponentZaak - A row representing a case.

=head1 SYNOPSIS

=head1 CONSTANTS

=head2 RELATED_OBJECTEN

This constant map has the internal names for related objects tied to a
'human-friendly' string.

=cut

use constant RELATED_OBJECTEN => {
    fasen     => 'Fasen',
    voortgang => 'Voortgang',
    sjablonen => 'Sjablonen',
    locaties  => 'Locaties',
    acties    => 'Acties'
};

=head1 ATTRIBUTES

=head2 te_vernietigen

This lazy attribute contains C<1> if the L</vernietigingsdatum> has passed.
It is indicative of the case having reached end-of-life and is OK to destruct.

=cut

has 'te_vernietigen'    => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;

        return unless $self->vernietigingsdatum;

        return 1 if $self->vernietigingsdatum < DateTime->now();

        return;
    }
);

=head2 object_data

This attribute holds the current
L<Zaaksysteem::Backend::Object::Data::Component>. It has several potential
side-effects, aside from returning an instance of the object data row.

It may 1) create a new object data row if none exist, or it may 2) delete
all object data rows that reference the case instance if the case
L</is_deleted>.

=cut

has object_data => (
    is      => 'rw',
    lazy    => 1,
    clearer => '_clear_object_data',

    default => sub {
        my $self = shift;

        my $schema = $self->result_source->schema;
        my $object_data = $schema->resultset('ObjectData');

        # We don't want deleted objects to enter the hstore/object_data table
        # also, we don't care for object_data object triggers for deletions,
        # so we use the more efficient delete method on the resultset instead
        # of delete_all.
        if($self->is_deleted) {
            my $objects = $object_data->search({
                object_class => 'case',
                object_id => $self->id
            });

            my @ids = $objects->get_column('uuid')->all;

            $objects->delete;

            my $qrs = $schema->resultset('Queue');

            for my $id (@ids) {
                $qrs->queue_item($qrs->create_item('delete_object', {
                    label => sprintf('Delete object index for zaak %d', $self->id),
                    metadata => { target => 'backend', object_model => 1 },
                    data => {
                        object_reference => {
                            id => $id,
                            type => 'case'
                        }
                    }
                }));
            }

            return;
        }

        return $object_data->find_or_create_by_object_id({
            object_class => 'case',
            object_id    => $self->id,
            class_uuid   => $self->zaaktype_id->uuid,
        });
    }
);

=head2 relationships_by_case_number

    my $rv = $zaak->relationships_by_case_number

    # Returns: 
    {
        parent      => [2323],
        children    => [2425,5525],
        relations   => [8935,3489,8924]
    }

=cut

has 'relationships_by_case_number' => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self    = shift;
        my $rv      = {
            parent      => ($self->get_column('pid') ? [$self->get_column('pid')] : []),
            children    => [],
            relations   => [],
        };

        $rv->{children} = [ $self->zaak_children->search->get_column('id')->all ];

        push @{ $rv->{relations} }, map { $_->case_id }
            $self->result_source->schema->resultset('CaseRelation')->get_sorted($self->id);

        return $rv;
    }
);

=head2 relationships_by_object_uuid

    my $rv = $zaak->relationships_by_object_uuid

    # Returns: 
    {
        parent      => ['565185ee-fd8f-4c2f-8729-0f8a0c249f3f'],
        children    => ['7ba53f43-a45a-48e4-9b0f-b6d09381d6b2','907a53f8-a45a-48e4-9b0f-b6d09381d6d4'],
        relations   => ['4727a53f-a45a-48e4-9b0f-b6d09381d6c1']
    }

=cut

has 'relationships_by_object_uuid' => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self    = shift;
        my @numbers;

        my $rv      = {
            parent      => [],
            children    => [],
            relations   => [],
        };

        my $n_relations = $self->relationships_by_case_number;

        push(@numbers, @{ $n_relations->{$_} }) for keys %$n_relations;

        my @objects = $self->result_source->schema->resultset('ObjectData')->search(
            {
                object_class => 'case',
                object_id => \@numbers
            }
        )->all;

        for my $key (keys %$n_relations) {
            for my $number (@{ $n_relations->{$key} }) {
                my ($object) = grep { $number == $_->object_id } @objects;

                next unless $object;

                push (@{ $rv->{$key} }, $object->id);
            }
        }

        return $rv;
    }
);

sub nr  {
    my $self    = shift;

    $self->id( @_ );
}

=head2 set_result_by_id

Arguments: \%PARAMS

Return value: $TRUE_ON_SUCCESS

    $success = $zaak->set_result_by_id({ id => 2424 });

Sets the result of the case by the given zaaktype_resultaten->id

=cut

sub set_result_by_id {
    my ($self, $args) = @_;

    my $result = $self->zaaktype_node_id->zaaktype_resultaten->find($args->{id});

    return unless $result;

    $self->set_resultaat($result->resultaat);

    return 1;
}

sig set_resultaat => 'Str';

sub set_resultaat {
    my $self = shift;
    my $res  = lc(shift);

    my $oud_resultaat = $self->resultaat // '';

    if (lc($oud_resultaat) eq $res) {
        return;
    }

    if (!$self->zaaktype_node_id->zaaktype_resultaten->search_rs({ resultaat => $res})->first) {
        throw("case/set_result/result_name/invalid", "Unable to set the result to '$res'");
    }

    $self->resultaat($res);


    my $event_type = 'case/update/result';
    my $data = {
        case_id     => $self->id,
        result      => $self->resultaat,
        old_result  => $oud_resultaat
    };

    $self->trigger_logging($event_type, {
        component   => 'zaak',
        zaak_id     => $self->id,
        data        => $data,
    });

    $self->update;
    return;
}


sub zaaktype_definitie {
    my $self    = shift;

    return $self->zaaktype_node_id->zaaktype_definitie_id;
}

=head2 rules

Arguments: \%OPTIONS

    ### Get rules engine for current case
    my $rules = $case->rules({ case.number_status => 1, reload => 1});

    ### Validation profile for current case
    my $validation = $rules->validate_from_case($case);

    ### Replacement for visible_fields
    $validation->active_attributes

Will return the new rules engine for the given status. Will cache the results in
this case object for performance purposes. When given the optional reload parameter,
it will reload the cache so you will get a fresh result. The only time this would be usefull
is when native case attributes change, such as the payment_status, aanvrager, confidentiality
or channel_of_contact.

=cut


has '_rules' => (
    'is'        => 'rw',
    'isa'       => 'HashRef',
    'lazy'      => 1,
    'default'   => sub { {}; }
);

has '_all_rules' => (
    'is'        => 'rw',
    'isa'       => 'Zaaksysteem::Backend::Rules',
);

define_profile 'rules' => (
    required    => [],
    optional    => ['reload', 'case.number_status']
);

sub rules {
    my $self            = shift;
    my $options         = assert_profile(shift || {})->valid;

    if (!$options->{reload}) {
        if ($options->{'case.number_status'}) {
            if ($self->_rules->{ $options->{'case.number_status'} }) {
                return $self->_rules->{ $options->{'case.number_status'} };
            }
        } elsif ($self->_all_rules) {
            return $self->_all_rules;
        }
    }

    my $params          = Zaaksysteem::Backend::Rules->generate_object_params(
        {
            'case'                      => $self,
            ($options->{'case.number_status'} ? ('case.number_status'        => $options->{'case.number_status'}) : ()),
        },
        {
            engine                      => 1,
        }
    );

    if ($options->{'case.number_status'}) {
        return ($self->_rules->{ $options->{'case.number_status'} } = $params->{rules});
    } else {
        return $self->_all_rules($params->{rules});
    }
}

=head2 is_late

Every case has an expiry date, calculated by the registration date +
the given expiry time, which is set in casetype management. If the expiry
date has passed and the case has not been closed yet, the case is considered
to be late.

Calculations happen on whole days. So first the timestamps are reduced to
dates, from which they are compared.

=cut

sub is_late {
    my $self = shift;

    my $reference = $self->is_afgehandeld ? $self->afhandeldatum : DateTime->now;

    # display a warning even when the case is already closed
    return $reference->truncate(to => 'day')->epoch >
        $self->streefafhandeldatum->truncate(to => 'day')->epoch;
}

=head2 case_documents

Convenience method to get case documents from a case

=cut

sub case_documents {
    my $self = shift;
    return $self->active_files->search_rs(
        { 'case_documents.case_document_id' => { '!=' => undef } },
        { join => { case_documents => 'file_id' }, });
}

# zaak->zaaktype_node_id->properties->{$property}
sub zaaktype_property {
    my ($zaak, $property) = @_;

    die "need zaak"     unless $zaak;
    die "need property" unless $property;

    my $zaaktype_node = $zaak->zaaktype_node_id or die "need zaaktype_node_id";

    my $properties = $zaaktype_node->properties or return; # older zaaktypen don't have this field. the return value is undef.

    return $properties->{$property};
}


=head2 zaaktype_resultaat

Returns the selected result object for this case. Used for systeemkenmerken.

=cut

sub zaaktype_resultaat {
    my ($zaak) = @_;

    return unless $zaak->resultaat;

    return $zaak->zaaktype_node_id->zaaktype_resultaten->search({ resultaat => $zaak->resultaat })->first;
}



sub status_perc {
    my $self            = shift;

    my $numstatussen    = $self->zaaktype_node_id->zaaktype_statussen->count;

    return 0 unless $numstatussen;

    # Force it down to an integer.
    return 0 + sprintf("%.0f", ($self->milestone / $numstatussen) * 100);
}

sig open_zaak => '?Zaaksysteem::Betrokkene::Object::Medewerker|Zaaksysteem::Object::Types::Subject';

sub open_zaak {
    my $self = shift;
    my $current_user = shift || $self->get_current_user;

    unless (defined $current_user) {
        throw(
            'case/open/current_user_undefined',
            'Unable to open case, current user could not be resolved (API bug?)'
        );
    }

    my $former_state = {
        status      => $self->status,
        behandelaar => $self->behandelaar ? $self->behandelaar->id : undef,
        coordinator => $self->coordinator ? $self->coordinator->id : undef,
    };

    $self->status('open');

    my ($id, $naam);
    if ($current_user->can("betrokkene_identifier")) {
        $id = $current_user->betrokkene_identifier;
        $naam = $current_user->naam;
    }
    elsif ($current_user->subject_type eq 'employee') {
        $id = $current_user->old_subject_identifier;
        $naam = $current_user->display_name;
    }
    else {
        throw('case/open/subject_type', "Unsupported subject type");
    }

    unless ($self->behandelaar) {
        $self->set_behandelaar($id);
    }

    unless ($self->coordinator) {
        $self->set_coordinator($id);
    }

    $self->update;

    my $new_state = {
        status      => $self->status,
        behandelaar => $self->behandelaar->id,
        coordinator => $self->coordinator->id,
    };

    my $logging_row = $self->trigger_logging('case/accept', { component => 'zaak', data => {
        case_id         => $self->id,
        acceptee_name   => $naam,
        former_state    => $former_state,
        new_state       => $new_state,
    }});

    return $logging_row->id;
}

sig reject_zaak => '?Zaaksysteem::Betrokkene::Object::Medewerker,?Str';

sub reject_zaak {
    my $self         = shift;
    my $current_user = shift || $self->get_current_user;
    my $comment      = shift;

    unless (defined $current_user) {
        throw(
            'case/reject/current_user_undefined',
            'Unable to reject case, current user could not be resolved (API bug?)'
        );
    }

    if ($self->status ne 'new') {
        throw(
            'case/reject/not_new',
            "Unable to 'reject' case, case is not in 'intake' state",
        );
    }

    if ($self->behandelaar) {
        $self->behandelaar_gm_id(undef);
        $self->behandelaar(undef);
    }

    my @configs = $self->result_source->schema->resultset('Config')->search(
        {
            parameter => ['case_distributor_group', 'case_distributor_role']
        }
    )->all;

    my $cache = {};
    my %calls = ();
    for my $config (@configs) {
        if ($config->parameter eq 'case_distributor_group') {
            my $group = $self->result_source->schema->resultset('Groups')->find($config->value);

            $calls{route_ou} = $config->value if $group;
        }

        if ($config->parameter eq 'case_distributor_role') {
            my $role = $self->result_source->schema->resultset('Roles')->find($config->value);

            $calls{route_role} = $config->value if $role;
        }
    }

    throw(
        'case/reject/incomplete_distributors',
        "Case distributors not set or not complete, make sure distributors are configured",
    ) unless scalar(keys %calls) == 2;

    $self->$_($calls{$_}) for sort keys %calls;

    $self->trigger_logging(
        'case/reject',
        {
            component => 'zaak',
            zaak_id   => $self->id,
            data      => {
                case_id => $self->id,
                defined $comment ? (comment => $comment) : (),
                current_user =>
                    { display_name => $current_user->display_name, }
            }
        }
    );

    $self->update;
}


sub unrelate {
    my ($self, $related_id) = @_;

    if($self->relates_to && $self->relates_to->id == $related_id) {
        $self->relates_to(undef);
        $self->update;
    } else {
        my $related_case = $self->result_source->schema->resultset('Zaak')->find($related_id);

        $related_case->relates_to(undef);
        $related_case->update;
    }
}

sub set_verlenging {
    my $self    = shift;
    my $dt      = shift;

    $self->streefafhandeldatum($dt);
    $self->set_vernietigingsdatum;
    $self->update;
}


sub _bootstrap {
    my ($self, $opts)   = @_;

    $self->_bootstrap_datums($opts);
    $self->_bootstrap_route($opts);
    $self->update;
}


sub _bootstrap_datums {
    my ($self, $opts)   = @_;

    if ($opts->{registratiedatum}) {
        $self->registratiedatum($opts->{registratiedatum});
    } elsif (!$self->registratiedatum) {
        $self->registratiedatum(DateTime->now());
    }

    ### Streefbare afhandeling
    if ($opts->{streefafhandeldatum}) {
        $self->streefafhandeldatum($opts->{streefafhandeldatum});
    } else {
        my ($norm, $type);

        if ($opts->{streefafhandeldatum_data}) {
            $norm = $opts->{streefafhandeldatum_data}->{termijn};
            $type = $opts->{streefafhandeldatum_data}->{type};
        }
        else {
            $norm = $self->zaaktype_node_id->zaaktype_definitie_id->servicenorm,
            $type = $self->zaaktype_node_id->zaaktype_definitie_id->servicenorm_type
        }

        my $calculated = calculate_term({
            start  => $self->registratiedatum,
            amount => $norm,
            type   => $type
        });

        $self->streefafhandeldatum($calculated);
    }
}

sub insert {
    my $self    = shift;

    # we need the database to generate the new searchable_id param.
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};
    delete $self->{_column_data}->{object_type};

    $self->{_column_data}->{uuid} = $self->result_source->resultset->_generate_uuid
        unless (
            exists $self->{_column_data}->{uuid} &&
            $self->{_column_data}->{uuid}
        );

    $self->{_column_data}->{duplicate_prevention_token} = $self->result_source->resultset->_generate_uuid
        unless (
            exists $self->{_column_data}->{duplicate_prevention_token} &&
            $self->{_column_data}->{duplicate_prevention_token}
        );

    $self->_handle_changes({insert => 1});
    $self->next::method(@_);
}

sub update {
    my $self    = shift;
    my $columns = shift;

    $self->set_inflated_columns($columns) if $columns;
    $self->_handle_changes;
    $self->next::method(@_);
}

sub _handle_changes {
    my $self    = shift;
    my $opt     = shift;
    my $changes = {};

    if ($opt && $opt->{insert}) {
        $changes = { $self->get_columns };
        $changes->{_is_insert} = 1;
    } else {
        $changes = { $self->get_dirty_columns };
    }

    if (not $changes->{ _is_insert }) {
        my %map = (
            behandelaar => 'assignee',
            coordinator => 'coordinator',
            aanvrager => 'requestor',
            route_ou => 'route',
            route_role => 'route',
            resultaat => 'outcome'
        );

        my @names = uniq grep { $_ } map {
            $map{ $_ } if exists $changes->{ $_ }
        } keys %map;

        if (scalar @names) {
            if ($self->log->is_trace) {
                $self->log->trace(sprintf(
                    'Cleaning up %s relations for case %d',
                    join(', ', map { "'$_'" } @names),
                    $self->id
                ));
            }

            $self->object_data->object_relation_object_ids->search({
                name => \@names
            })->delete;
        }
    }

    $self->{_get_latest_changes} = $changes;

    $self->_set_search_string();

    return 1;
}

=head2 trigger_logging

Add a logging entry for the case.

If the case confidentiality level is not "public" at the time, the "restricted"
flag is set, so the log lines aren't shown on pages.

=cut

sub trigger_logging {
    my $self = shift;
    my ($type, $fields) = @_;

    $fields->{restricted} = ($self->confidentiality eq 'public') ? 0 : 1;

    return $self->logging->trigger($type, $fields);
}

=head2 log_view

Log a "view"/single retrieval of this case.

=cut

sub log_view {
    my $self    = shift;

    my $existing = $self->logging->find_recent({
        event_type => 'case/view',
        component  => 'zaak',
        zaak_id    => $self->id,
    });

    if ($existing) {
        $self->log->trace("Found existing log item. Updating creation time.");
        $existing->update({ created => DateTime->now() });
        return;
    }

    $self->trigger_logging(
        'case/view',
        {
            component => 'zaak',
            zaak_id   => $self->id,
        }
    );

    return;
}

sub _set_search_string {
    my ($self) = @_;

    my @terms = $self->zaaktype_node_id->titel;

    # Case may not exist yet.
    if($self->id) {
        push @terms, $self->id;
    }

    if($self->aanvrager) {
        my $gmnp = $self->aanvrager->natuurlijk_persoon;

        if (defined $gmnp) {
            push @terms, $gmnp->voornamen, $gmnp->geslachtsnaam;
        } else {
            push @terms, $self->aanvrager->naam;
        }
    }

    if($self->behandelaar) {
        push @terms, $self->behandelaar->naam;
    }

    push @terms, (
        $self->registratiedatum,
        $self->onderwerp,
        $self->zaaktype_node_id->zaaktype_trefwoorden
    );

    my $search_term = join ' ', grep { defined && length } @terms;

    $self->search_term($search_term);
    $self->search_order($search_term);
}

after insert => sub {
    my $self = shift;

    $self->_handle_logging;
    $self->touch();
};

after update => sub {
    my $self = shift;

    $self->_handle_logging;
    $self->touch();
};

=head2 touch(\%opts)

 $case->touch();

Touches a cases, and updates magic strings in onderwerp accordingly,
updates the search index etc.

=cut

has 'touch_in_progress' => (
    is  => 'rw'
);

sub touch {
    my $self = shift;

    return if $self->touch_in_progress;

    my $delayed = $self->result_source->schema->default_resultset_attributes->{delayed_touch};

    if ($delayed) {
        return $delayed->add_case($self);
    }
    else {
        return $self->_touch;
    }
}

=head2 _touch

All case updates should be combined in one transaction, so
the overall state is consistent. Also the eval() has been
removed so we will get a nice clean ZSOD when something goes wrong.
When we're sick we want to feel bad so we can go see a doctor.
Also this way Arne and his robo disciple can spot problems
in an early stage.

=cut

sub _touch {
    my $self = shift;

    $self->log_start("_touch started");

    $self->touch_in_progress(1);

    my $schema = $self->result_source->schema;
    my $object;
    $schema->txn_do(sub {

        # Database rows are retrieved so two parallel 'touch' calls on different backends
        # don't interfere with each other.
        $schema->resultset('Zaak')->search_rs(
            { 'me.id' => $self->id },
            { for     => \'no key update' }
        )->first;

        $self->log_stop("searching for case");

        $object = $schema->resultset('ObjectData')->search_rs(
            {
                'me.object_class' => 'case',
                'me.object_id'    => $self->id,
            },
            { for => \'no key update' }
        )->first;

        $self->log_stop("searching for object_data");

        $self->_update_supersaas_appointments();

        if ($self->_refresh_onderwerp) {
            $self->last_modified(DateTime->now);
            $self->update;

            # If onderwerp or onderwerp_extern was (re)written during touch,
            # retrieving it here will return bytes. If it wasn't changed, it
            # will be characters. Obviously, this is bad and confusing.
            #
            # discard_changes forces a new SELECT, which forces the pg_enable_utf8
            # flag to do its work, so we always get characters.
            $self->discard_changes();
            $self->log_stop("refreshing data");
        }
        $self->update_hstore;
        $self->log_stop("updating hstore");

    });

    my $qrs = $schema->resultset('Queue');

    $qrs->queue_item($qrs->create_item('index_object', {
        label => sprintf('Update search index for zaak %d', $self->id),
        metadata => { target => 'backend', object_model => 1 },
        data => {
            object_reference => {
                id => $object->id,
                type => 'case',
                preview => sprintf(
                    '%d %s',
                    $self->id,
                    $self->zaaktype_node->titel
                )
            },
            internal_reference => {
                id => $self->id,
                table => 'zaak'
            },
            content => $self->search_term
        }
    }));

    $self->update_child_relations($object);
    $self->log_stop("updating child relations");

    $self->touch_in_progress(0);

    return 1;
}

=head2 update_child_relations

Update all the childs of a case. This will make sure that apiv1 will return correct object data.

=cut

sub update_child_relations {
    my ($self, $object) = @_;

    # Sometimes the object data row is not created, or the case is
    # deleted. No need to update the child relations in that case.
    return unless $object;

    my $schema = $self->result_source->schema;
    my $queue = $schema->resultset('Queue');

    my $rs = $schema->resultset('ObjectRelationships')->search(
        {
            object1_type => 'case',
            object2_type => 'case',
            '-or'        => [
                { object1_uuid => $object->id, type1 => 'parent' },
                { object2_uuid => $object->id, type2 => 'parent' },
            ]
        },
    );

    my @child_cases;

    while (my $rel = $rs->next) {
        if ($rel->get_column('object1_uuid') eq $object->id) {
            push @child_cases, $rel->get_column('object2_uuid');
        } else {
            push @child_cases, $rel->get_column('object1_uuid');
        }
    }

    for my $child (@child_cases) {
        $queue->queue_item(
            $queue->create_item(
                'touch_case',
                {
                    object_id => $object->uuid,
                    label     => 'Deelzaak synchronisatie',
                    metadata  => {
                        disable_acl => 1,
                        target      => 'backend',
                    },
                    data => { case_object_id => $child }
                }
            )
        );
    }

    return 1;
}

sub _refresh_onderwerp {
    my $self        = shift;

    my $node = $self->zaaktype_node_id;

    return unless $node;

    my $definitie = $node->zaaktype_definitie_id;

    return unless $definitie;

    my $ztt = Zaaksysteem::ZTT->new(cache => $self->_ztt_cache);

    $ztt->add_context($self);

    my $updated = 0;
    my $extra_info = $definitie->extra_informatie;

    if ($extra_info) {
        my $toelichting = $ztt->process_template($extra_info)->string // '';
        my $onderwerp = $self->onderwerp // '';

        if ($toelichting ne $onderwerp) {
            $self->onderwerp($toelichting);
            $updated = 1;
        }
    }

    my $extra_extern = $definitie->extra_informatie_extern;
    if ($extra_extern) {
        my $toelichting = $ztt->process_template($extra_extern)->string // '';
        my $onderwerp = $self->onderwerp_extern // '';

        if ($toelichting ne $onderwerp) {
            $self->onderwerp_extern($toelichting);
            $updated = 1;
        }
    }
    return $updated;
}

sub _assert_kenmerk_ok {
    my ($self, $id) = @_;

    my $item = $self->zaaktype_node_id->zaaktype_kenmerken->find(
        { bibliotheek_kenmerken_id => $id });

    return 1 if $item;

    throw("case/bibliotheek_kenmerk/not_exists",
        "Attribute $id does not exist for zaak " . $self->id);
}

=head2 update_attribute

    $self->update_attribute(
        bibliotheek_kenmerk => $library_attribute,
        value               => 'Defined',
    );

B<Arguments>: L<bibliotheek_kenmerk|Zaaksysteem::Model::DB::BibliotheekKenmerken>, A defined value
B<Returns value>: A true-ish value.

=cut

define_profile update_attribute => (
    required => {
        bibliotheek_kenmerk => 'Zaaksysteem::Model::DB::BibliotheekKenmerken',
        value               => 'Defined'
    },
);

sub update_attribute {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    $self->_assert_kenmerk_ok($opts->{bibliotheek_kenmerk}->id);
    $self->zaak_kenmerken->replace_kenmerk(zaak_id => $self->id, %$opts) ;
    return 1;
}

=head2 update_location

Updates the case location using the given
L<Zaaksysteem::Object::Types::Address>.

    $case->update_location(Zaaksysteem::Object::Types::Address->new(...));

=cut

sig update_location => 'Zaaksysteem::Object::Types::Location';

sub update_location {
    my $self = shift;
    my $location = shift;

    unless ($location->has_address) {
        throw('case/update_location/no_address', sprintf(
            'Refusing to update case location without an address'
        ));
    }

    my $schema = $self->result_source->schema;

    my $bag_rs = $schema->resultset('BagNummeraanduiding')->search_address(
        $location->address
    );

    my $item = $bag_rs->next;

    unless (defined $item && $item->get_column('score') >= 1100) {
        $self->log->info('No BAG item found for given address with a score > 1100');

        return;
    }

    $schema->txn_do(sub {
        my $zaak_bag = $self->zaak_bags->create_bag({
            bag_type => 'nummeraanduiding',
            bag_id => $item->identificatie,
            bag_nummeraanduiding_id => $item->identificatie,
            bag_coordinates_wsg => sprintf('(%s)', $location->coordinates)
        });

        return $self->update({
            locatie_zaak => $zaak_bag->id
        });
    });
}

=head2 _update_supersaas_appointments

Updates the appointment on the SuperSaaS side

=cut

sub _update_supersaas_appointments {
    my ($self) = @_;

    my $schema = $self->result_source->schema;
    my $interface = $schema->resultset('Interface')->search_active({module => 'supersaas'})->first;
    return unless $interface;

    my $saas_kenmerken = $self->zaak_kenmerken->search(
        { 'bibliotheek_kenmerken_id.value_type' => 'calendar_supersaas' },
        { prefetch => 'bibliotheek_kenmerken_id' }
    );
    return unless $saas_kenmerken->count();

    my $description = $self->onderwerp_extern;

    while (my $kenmerk = $saas_kenmerken->next) {
        my $magic_string = $kenmerk->bibliotheek_kenmerken_id->magic_string;

        my $product = $interface->process_trigger('getProduct', { magic_string => $magic_string });

        if ($product) {
            my $appointment_id = (split(/;/, $kenmerk->value))[-1];

            $self->log->debug(
                sprintf(
                    "Found SuperSaaS kenmerk: '%s' with appointment id '%s'. Going to update SuperSaaS",
                    $magic_string,
                    $product->{schedule_id}
                )
            );

            $interface->process_trigger(
                'updateAppointment',
                {
                    appointmentId => $appointment_id,
                    productLinkID => $product->{schedule_id},
                    description   => $description,
                }
            );
        }
        else {
            $self->log->warn(sprintf(
                "Cannot update SuperSaaS kenmerk '%s' in case '%d', because it's not linked to a SuperSaaS calendar",
                $magic_string,
                $self->get_column('id'),
            ));
        }
    }

    return;
}

=head2 can_delete

A case can be deleted when a set of rules is matched. This set
continues to evolve over time.

Status on Januari 2014:
- case is already deleted - necessary for recursive behaviour, we're gonna ask
  all relations if they're also cool with deletion, if they're already deleted
  they will be okay.
- Case must be closed
- vernietigingsdatum must have passed
- No unclosed ancestors, typically a deelzaak cannot be closed unless its hoofdzaak
  has been closed.
- No unclosed vervolgzaken

For several scenarios warnings have been required, so the mechanism has been split
up into errors and warnings. Warnings need to be confirmed by the GUI, since the
backend has no way on knowing about that, it just reports the warnings and carries
on with its business.

=cut

has 'can_delete'   => (
    'is'    => 'ro',
    'lazy'  => 1,
    'default'   => sub {
        my ($self) = @_;

        return $self->is_deleted ||
            !@{ $self->deletion_errors };
    }
);

=head2 deletion_errors

Returns a list with strings pointing out why this case can't be deleted.

=cut

has 'deletion_errors'   => (
    'is'    => 'ro',
    'lazy'  => 1,
    'default'   => sub {
        my ($self) = @_;

        my @errors = ();

        push @errors, "Zaak is nog niet afgehandeld"
            unless $self->is_afgehandeld;

        push @errors, "Bewaartermijn is niet verstreken"
            unless $self->te_vernietigen;

        push @errors, map {
            my $err;
            if ($_->is_afgehandeld) {
                $err = "Bewaartermijn van hoofdzaak " . $_->id . " niet verstreken";
            }
            else {
                $err = 'Hoofdzaak ' . $_->id . ' is nog niet afgehandeld';
            }
            $err;
        } grep { !$_->can_delete } $self->ancestors;

        push @errors, map {
            my $err;
            if($_->is_afgehandeld) {
                $err = "Bewaartermijn van deelzaak " . $_->id . " niet verstreken";
            }
            else {
                $err = "Deelzaak " . $_->id . " is nog niet afgehandeld";
            }
            $err;
        } $self->active_children;

        return \@errors;
    }
);

=head2 dependent_objects

Return a list of scheduled jobs that depend on this case.

=cut

sub dependent_objects {
    my $self = shift;

    my $object_data = $self->object_data;

    my $scheduled1 = $object_data->object_relationships_object1_uuids;
    my $scheduled2 = $object_data->object_relationships_object2_uuids;

    return ($scheduled1->all, $scheduled2->all);
}

=head2 deletion_warnings

Returns a list with warnings that need to be read and confirmed by the user
before this case may be deleted.

=cut

has 'deletion_warnings'   => (
    'is'    => 'ro',
    'lazy'  => 1,
    'default'   => sub {
        my ($self) = @_;

        my @warnings = ();

        my @relations = $self->result_source->schema->resultset('CaseRelation')->get_all($self->id);

        # warn for related cases that are still active
        push @warnings, map {
            my $msg;
            if ($_->case->is_afgehandeld) {
                $msg = 'Bewaartermijn voor gerelateerde zaak ' . $_->case->id . ' is nog niet verstreken';
            }
            else {
                $msg = 'Gerelateerde zaak ' . $_->case->id . ' is nog niet afgehandeld';
            }
            $msg;
        } grep { !$_->case->can_delete } @relations;

        push @warnings, grep { defined $_ } map {
            my $object;
            if ($_->get_column('object1_uuid') eq  $self->object_data->uuid) {
                $object = $_->object2_uuid;
            }
            else {
                $object = $_->object1_uuid;
            }

            my $rv;
            # Special case, ZS-3896
            if ($object->object_class eq 'scheduled_job') {
                $rv = sprintf("Taak '%s' is nog niet voltooid", $object->TO_STRING);
            }
            elsif($object->object_class eq 'case') {
                # Handled by the specific "case" code higher up
                $rv = undef;
            }
            else {
                $rv = sprintf("Object '%s' is nog aan deze zaak gekoppeld", $object->TO_STRING);
            }

            $rv;
        } $self->dependent_objects;

        return \@warnings;
    }
);


=head2 ancestors

Return a list of ancestors (parent, grandparent, etc.) of the current case.

=cut

sub ancestors {
    my ($self) = @_;

    my $current = $self;

    my @visited;
    while ($current = $current->pid) {
        # Infinite loop protection.
        if (grep { $_->id == $current->id } @visited) {
            warn "Case " . $self->id . " has a loop in its parent hierarchy.\n";
            last;
        }

        push @visited, $current;
    }

    return @visited;
}

=head2 active_children

Return a list of active child cases.

=cut

sub active_children {
    my ($self, @children) = @_;

    # recursion protection, the db model allows my grandpa to be my son.
    # if this loop happens, no delete possible, because there is a living parent - me!
    if (any { $self->id == $_ } @children) {
        warn "glitch in the matrix, space-time continuum breached. case " . $self->id . " is own parent";
        return ($self); # if i am my active child, i'll return myself
    }

    grep { !$_->ready_for_destruction || $_->active_children(@children, $self->id) }
        $self->zaak_children->search->all;
}


# we can't use can_delete because of potential infinite recursion problems
sub ready_for_destruction {
    my $self = shift;

    return $self->is_deleted || ($self->is_afgehandeld && $self->te_vernietigen);
}


=head2 _delete_object_subscriptions

TODO: Ask Michiel to fix this code.

=cut

sub _delete_object_subscriptions {
    my $self = shift;

    # When the subject of a case no longer has any active cases pointing to it,
    # any object subscriptions that exist should be closed.
    # TODO: move this to subject code after dropout merge.

    ### Temporarily disabled: remove when new implementation is implemented. This version
    ### bugs in some situations
    return if ($self->aanvrager->betrokkene_type ne 'natuurlijk_persoon');

    my $schema = $self->result_source->schema;
    my $os = $schema->resultset('ObjectSubscription')->search_rs(
        {
            date_deleted => undef,
            local_table  => 'NatuurlijkPersoon',
            local_id     => $self->aanvrager_object->gmid,
        }
    );
    return if $os->count == 0;
    throw("zaak/delete/objectsubscription/multiple", "Multiple object subscriptions found: " . $os->count) if $os->count > 1;

    my $object_subscription = $os->first;

    my $has_active_case;
    my $np_case_subject = $schema->resultset('ZaakBetrokkenen')->search(
        {
            betrokkene_type => 'natuurlijk_persoon',
            gegevens_magazijn_id => $self->aanvrager->gegevens_magazijn_id,
            deleted => undef,
        }
        );

    while (my $case_subject = $np_case_subject->next()) {
        for my $case ($case_subject->zaak_aanvragers) {

            # Case needs to have status deleted, unless it's the one we are
            # currently trying to delete.
            if (!$case->is_deleted && $case->id != $self->id) {
                $has_active_case = 1;
                last;
            }
        }
    }
    if (!$has_active_case) {
        $object_subscription->object_subscription_delete;
    }
}

=head2 set_deleted

Delete the case.

=head3 ARGUMENTS

=over

=item * process

Sometimes a case is not deleted by a human, but rather by a process. Eg. a failed Ogone payment, or something else. You can overwrite the current user with this

=item * no_checks

In certain cases we don't want a case to be checked against the regular rules. Failed payments, for example. Disables the checks.

=back

=cut

sub set_deleted {
    my $self = shift;
    my $opts = {@_};

    if (!$opts->{no_checks} && !$self->can_delete) {
        throw('case/set_deleted/cant_delete',
            sprintf("Zaak %d kan niet verwijderd worden", $self->id));
    }

    my $schema = $self->result_source->schema;

    # TODO children, relaties. cascade?

    $schema->txn_do(
        sub {
            my $case_id = $self->id;

            # Allow a process to be "named" as the current user. This is because
            # with Ogone payments I want to be able to log that the Ogone
            # process deleted the case and not some employee.
            my $current_user;
            if ($opts->{process}) {
                $current_user = $opts->{process};
            }
            else {
                $current_user = $schema->resultset('Zaak')->current_user->naam;
            }

            $self->_delete_zaak;
            #$self->_delete_object_subscriptions;

            $self->trigger_logging(
                'case/delete',
                {
                    component => 'zaak',
                    data      => {
                        case_id       => $case_id,
                        acceptee_name => $current_user,
                    }
                }
            );
        }
    );

    return 1;
}

sub is_deleted {
    my $self = shift;

    return $self->deleted && $self->deleted <= DateTime->now();
}

sub _delete_zaak {
    my $self = shift;

    my @relation_views = $self->zaak_relaties();
    for my $x (@relation_views) {
        $x->relation->delete();
    }

    $self->logging->delete_all;

    $self->status('deleted');
    $self->deleted(DateTime->now());

    my $retval = $self->update;

    $self->_clear_object_data;

    if(defined $self->object_data) {
        # ObjectData needs to be very much dead after deleting a Zaak.
        throw('case/delete', 'ObjectData object is defined after deletion of Zaak');
    }

    return $retval;
}

sub zaak_relaties {
    my $self = shift;

    return $self->result_source->schema->resultset('CaseRelation')->get_sorted(
        $self->get_column('id')
    );
}

sub _get_latest_changes {
    my $self    = shift;

    return $self->{_get_latest_changes};
}

sub _handle_logging {}

sub duplicate {
    my $self    = shift;
    $self->result_source->schema->resultset('Zaak')->duplicate( $self, @_ );
}

sub wijzig_zaaktype {
    my $self    = shift;
    $self->result_source->schema->resultset('Zaak')->wijzig_zaaktype($self, @_);
}

# CINE for create-if-not-exists
sub case_actions_cine {
    my $self = shift;

    # Don't apply rules to unsorted action resultsets, results go all screwy
    # as the actions may not be retrieved in order.
    my $rs = $self->case_actions(@_)->sorted;

    $rs->apply_rules({ case => $self });

    return $rs if $rs->count;

    $rs->create_from_case($self);

    return $rs->reset;
}

sub format_payment_status {
    my $self = shift;

    my $payment_status = $self->payment_status;

    if($payment_status) {
        return ZAAKSYSTEEM_CONSTANTS->{payment_statuses}->{$payment_status} || $payment_status;
    }
}

sub format_payment_amount {
    my $self = shift;

    my $payment_amount = $self->payment_amount;
    my $formatted = $payment_amount ? sprintf("%0.2f", $payment_amount) : '-';
    $formatted =~ s/\./,/g;

    return $formatted;
}

sub set_payment_status {
    my $self = shift;

    my $status = shift;
    my $amount = shift;

    $self->payment_status($status);
    $self->update();

    $self->trigger_logging('case/payment/status', {
        component => 'zaak',
        zaak_id => $self->id,
        data => {
            case_id => $self->id,
            status_code => $self->payment_status,
            status => $self->format_payment_status,
            amount => $amount
        }
    });
}

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        uuid => $self->uuid,
        pid => $self->pid,

        afhandeldatum => $self->afhandeldatum,
        contactkanaal => $self->contactkanaal,
        created => $self->created,
        deleted => $self->deleted,
        last_modified => $self->last_modified,
        milestone => $self->milestone,
        object_type => $self->object_type,
        onderwerp => $self->onderwerp,
        registratiedatum => $self->registratiedatum,
        streefafhandeldatum => $self->streefafhandeldatum,
        vernietigingsdatum => $self->vernietigingsdatum,
        zaaktype_id => $self->zaaktype_id,
        zaaktype_node_id => $self->zaaktype_node_id,
        days_perc => $self->status_perc,
        resultaat => $self->resultaat,

        route_ou => $self->route_ou,
        route_role => $self->route_role,

        locatie_correspondentie => $self->locatie_correspondentie,
        locatie_zaak => $self->locatie_zaak,

        aanvraag_trigger => $self->aanvraag_trigger,
        aanvrager => $self->aanvrager_object,
        behandelaar => $self->behandelaar_object,
        coordinator => $self->coordinator_object,
    };
}

=head2 check_queue_coworker_changes

Determine wether a given user should pass their changes as requests.

=cut

sub check_queue_coworker_changes {
    my ($self, $user_id) = @_;

    # avoid warnings on 'ne' operator
    my $behandelaar_id = $self->behandelaar && $self->behandelaar->gegevens_magazijn_id || '';

    return $self->zaaktype_property('queue_coworker_changes') &&
        $behandelaar_id ne $user_id;
}

=head2 object_type

Return the object_type (used while zaken are "dual" zaak/object_data)

XXX Temporary, remove once "zaak" is in object_data!

=cut

sub object_type { return 'case' }

sub set_confidentiality {
    my ($self, $value) = @_;

    my $old = $self->confidentiality;
    my $new = $self->confidentiality($value);

    $self->trigger_logging('case/update/confidentiality', {
        component => 'zaak',
        zaak_id   => $self->id,
        data      => {
            new => $new,
            old => $old
        }
    });

    $self->update;
}


=head2 display_flash_messages

On display of a case some flash messages need to be shown to the user.
This generates a list with the message, view layer can relay.


=cut

sub display_flash_messages {
    my $self = shift;

    my @messages;

    if (
        $self->aanvrager_object &&
        $self->aanvrager_object->can('messages_as_flash_messages')
    ) {
        push (
            @messages,
            @{ $self->aanvrager_object->messages_as_flash_messages }
        );
    }

    my $payment_status = $self->payment_status || '';
    if ($payment_status eq CASE_PAYMENT_STATUS_FAILED) {
        push @messages, {
            message     => 'Let op, betaling niet succesvol',
            type        => 'error'
        };
    }

    if ($payment_status eq CASE_PAYMENT_STATUS_PENDING) {
        push @messages, {
            message => 'Let op, betaling (nog) niet afgerond',
            type => 'error'
        };
    }

    return @messages;
}

=head2 create_message_for_behandelaar

Create a message for a behandelaar of a case

=head3 SYNOPSIS

    my $log = $schema->resultset('Logging')->trigger(
        'some/trigger',
        {
            component => 'kenmerk',
            zaak_id   => $zaak->id,
            data      => $data,
        }
    );

    $case->create_message_for_behandelaar(
        message    => 'This is a message',
        event_type => 'foo/bar',
        log        => $log,
        subject    => $user_entity->subject,
    );

=head3 ARGUMENTS

=over

=item * message

The actual message, required.

=item * event_type

An event type, required.

=item * log

An log object, optional.

=item * subject

Sets the user wich will create the message.
An L<Zaaksysteem::Backend::Subject::Component>, optional.

=back

=cut

sub create_message_for_behandelaar {
    my ($self, %params) = @_;

    if (my $behandelaar = $self->behandelaar) {
        my $current_user = $params{subject} // $self->get_current_user;
        if (!$current_user || $current_user->betrokkene_identifier ne $behandelaar->betrokkene_identifier) {
            try {
                my $message = $self->result_source->schema->resultset('Message')->message_create(
                    {
                        case_id    => $self->id,
                        subject_id => $behandelaar->betrokkene_identifier,
                        event_type => $params{event_type},
                        message    => $params{message},
                        $params{log} ? (log => $params{log}) : (),
                    }
                );
            }
            catch {
                $self->log->error("Unable to create message: " . $_);
                throw("case/message/create", "Notificatie voor behandelaar is mislukt");
            }
        }
        else {
            $self->log->trace(
                sprintf(
                    "Not creating message: current user matches behandelaar '%s'",
                    $current_user->betrokkene_identifier,
                )
            );
        }
    }
}

=head2 assert_assignee

Asserts if you can assign a case to a behandelaar

=head3 SYNOPSIS

    my $case->assert_assignee(
        betrokkene_id => foo,
        subject       => $subject,
    );

=head3 ARGUMENTS

=over

=item * betrokkene_id

The betrokken_id

=item * subject

A subject object

=back

=cut

define_profile assert_assignee => (
    optional => {
        betrokkene_id => 'Str',
        subject       => 'Zaaksysteem::Schema::Subject',
    },
    require_some =>
        { subject_or_betrokkene_id => [1, qw/betrokkene_id subject/], },
);

sub assert_assignee {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    my $properties = $self->zaaktype_node_id->properties;

    unless ($properties->{ check_permissions_for_assignee }) {
        $self->log->warn(sprintf(
            "DEPRECATION: Case %d has no limitations on who the assignee is",
            $self->id
        ));
        return 1;
    }

    my $subject = $opts->{ subject };

    unless ($subject) {
        my ($id) = $opts->{betrokkene_id} =~ m[^betrokkene\-medewerker\-(\d+)$];

        unless ($id) {
            throw(
                'case/assignee/betrokkene_id/unknown',
                'Unknown betrokkene given'
            );
        }

        my $schema = $self->result_source->schema;

        $subject = $schema->resultset('Subject')->find($id);

        unless ($subject) {
            throw('case/assignee/subject/unknown', sprintf(
                'Unknown subject for betrokkene with %d',
                $id
            ));
        }
    }

    $self->assert_write_permission($subject);

    return 1;
}

sub assert_search_permission {
    my ($self, $subject) = @_;

    # We need to force touch the case in order to set all the relations
    # and so forth before we go looking in the database. Otherwise the
    # relations, rights, etc aren't in the DB and we cannot check if the
    # case can be assigned to $subject.
    $self->_touch() unless $self->object_data;

    my $schema = $self->result_source->schema;

    my $model = Zaaksysteem::Object::Model->new(
        schema => $schema,
        user   => $subject
    );

    my $rs = $model->rs->search({
        'me.object_class'   => 'case',
        'me.object_id'      => $self->id
    });

    my $first = $rs->first;
    return $first if $first;

    $self->log->info(sprintf(
        "Unable to find case %d with subject %d",
        $self->id,
        $subject->id
    ));

    throw(
        'case/permissions/search/unauthorized',
        'Unauthorized to search case',
    );
}

sig assert_read_permission => 'Zaaksysteem::Schema::Subject';

sub assert_read_permission {
    my ($self, $subject) = @_;

    return 1 if $subject->is_admin;
    my $case = $self->assert_search_permission($subject);
    return 1 if $case->has_permission('read');

    throw(
        'case/permissions/read/unauthorized',
        'Unauthorized to read case',
    );
}

=head2 assert_write_permission

Assert if the given L<Zaaksysteem::Schema::Subject> object can modify the case

=cut

sig assert_write_permission => 'Zaaksysteem::Schema::Subject';

sub assert_write_permission {
    my ($self, $subject) = @_;

    return 1 if $subject->is_admin;

    my $case = $self->assert_search_permission($subject);
    return 1 if $case->has_permission('write');

    throw(
        'case/permissions/read/unauthorized',
        'Unauthorized to write or manage case',
    );
}


=head2 get_zaaktype_result

Retrieves the C<zaaktype_resultaat> row that belongs to the result set on this
case/casetype.

If there's no result set, the undefined value is returned.

If the result isn't found in the C<zaaktype_resultaat> table, an exception is
thrown.

=cut

sub get_zaaktype_result {
    my $self = shift;

    return if not $self->resultaat;

    my $result = $self->zaaktype_node_id->zaaktype_resultaten->search(
        {resultaat => $self->resultaat}
    )->first;

    if (!$result) {
        throw(
            'case/result_not_found',
            sprintf("Zaak %d: Unable to find zaaktype_resultaat '%s' while it is defined", $self->id, $self->resultaat),
        );
    }

    return $result;
}

=head2 get_current_user

Convenience method to get the current user object from the schema.

=cut

sub get_current_user {
    my $self = shift;
    return $self->result_source->schema->resultset('Zaak')->current_user;
}


=head2 active_files

Retrieve a consistent selection of active files.

=cut

sub active_files { shift->files->active_rs }

=head2 search_active_files

Retrieve a (sub)set of active files for the case.

=cut

sub search_active_files {
    my $self = shift;
    my $ids  = shift;
    return $self->files->search_active($ids);
}

has mailer => (
    is => 'rw',
    isa => 'Zaaksysteem::Backend::Email',
    lazy => 1,
    builder => '_build_mailer',
);

sub _build_mailer {
    my $self = shift;
    return Zaaksysteem::Backend::Email->new(case => $self);
}

1;

__END__

=head1 COMPONENTS

=head2 Magic Strings

=over 4

=item L<Zaaksysteem::Zaken::Roles::ZTT> - Magic string handling

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CASE_PAYMENT_STATUS_FAILED

TODO: Fix the POD

=cut

=head2 CASE_PAYMENT_STATUS_PENDING

TODO: Fix the POD

=cut

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 case_actions_cine

TODO: Fix the POD

=cut

=head2 duplicate

TODO: Fix the POD

=cut

=head2 format_payment_amount

TODO: Fix the POD

=cut

=head2 format_payment_status

TODO: Fix the POD

=cut

=head2 is_deleted

TODO: Fix the POD

=cut

=head2 mailer

TODO: Fix the POD

=cut

=head2 nr

TODO: Fix the POD

=cut

=head2 open_zaak

TODO: Fix the POD

=cut

=head2 ready_for_destruction

TODO: Fix the POD

=cut

=head2 set_confidentiality

TODO: Fix the POD

=cut

=head2 set_payment_status

TODO: Fix the POD

=cut

=head2 set_resultaat

TODO: Fix the POD

=cut

=head2 set_verlenging

TODO: Fix the POD

=cut

=head2 status_perc

TODO: Fix the POD

=cut

=head2 unrelate

TODO: Fix the POD

=cut

=head2 wijzig_zaaktype

TODO: Fix the POD

=cut

=head2 zaak_relaties

TODO: Fix the POD

=cut

=head2 zaaktype_definitie

TODO: Fix the POD

=cut

=head2 zaaktype_property

TODO: Fix the POD

=cut
