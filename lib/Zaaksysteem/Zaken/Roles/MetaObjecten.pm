package Zaaksysteem::Zaken::Roles::MetaObjecten;

use Moose::Role;
use Data::Dumper;


sub reden_opschorten {
    my $self    = shift;

    return $self->_meta_method(@_);
}

sub stalled_since {
    my $self    = shift;
    my $dt      = shift;

    die('Need DateTime object as first parameter')
        unless (!$dt || UNIVERSAL::isa($dt, 'DateTime'));

    return $self->_meta_method($dt, 'stalled_since');
}

sub reden_verlenging {
    my $self    = shift;

    return $self->_meta_method(@_);
}

sub reden_afhandeling {
    my $self    = shift;

    return $self->_meta_method(@_);
}

sub reden_deel {
    my $self    = shift;

    return $self->_meta_method(@_);
}

sub reden_vervolg {
    my $self    = shift;

    return $self->_meta_method(@_);
}

sub reden_gerelateerd {
    my $self    = shift;

    return $self->_meta_method(@_);
}

sub _meta_method {
    my $self        = shift;
    my $value       = shift;
    my $method      = shift;

    if (!$method) {
        ($method)    = [ caller(1) ]->[3] =~ /reden_(.*)/;
    }

    my $meta        = $self->zaak_meta->first;

    unless (length($value)) {
        return $meta->$method if $meta;
        return;
    }

    if (!$meta) {
        $meta   = $self->zaak_meta->create({});
    }

    $meta->$method($value);

    if ( $meta->update ) {
        return $value;
    }

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 reden_afhandeling

TODO: Fix the POD

=cut

=head2 reden_deel

TODO: Fix the POD

=cut

=head2 reden_gerelateerd

TODO: Fix the POD

=cut

=head2 reden_opschorten

TODO: Fix the POD

=cut

=head2 reden_verlenging

TODO: Fix the POD

=cut

=head2 reden_vervolg

TODO: Fix the POD

=cut

=head2 stalled_since

TODO: Fix the POD

=cut

