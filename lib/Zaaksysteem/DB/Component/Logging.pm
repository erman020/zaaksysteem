package Zaaksysteem::DB::Component::Logging;
use base qw/DBIx::Class::Row/;
use Moose;

use Moose::Util qw/apply_all_roles/;
use JSON;
use DateTime::Format::ISO8601;

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_LOGGING_LEVELS
/;
use BTTW::Tools;

with 'MooseX::Log::Log4perl';

sub new {
    shift->next::method(@_)->_apply_roles;
}

sub inflate_result {
    shift->next::method(@_)->_apply_roles;
}

has _skip_roles => (
    is      => 'ro',
    isa     => 'HashRef',
    builder => '_build_skip_roles',
);

sub _build_skip_roles {
    return {
        map { $_ => 1 } qw(
            Zaaksysteem::DB::Component::Logging::Attribute
            Zaaksysteem::DB::Component::Logging::Auth
            Zaaksysteem::DB::Component::Logging::Auth
            Zaaksysteem::DB::Component::Logging::Auth::Alternative
            Zaaksysteem::DB::Component::Logging::Case
            Zaaksysteem::DB::Component::Logging::Case::Attribute
            Zaaksysteem::DB::Component::Logging::Case::Document::Accept
            Zaaksysteem::DB::Component::Logging::Case::Document::Metadata
            Zaaksysteem::DB::Component::Logging::Case::Note
            Zaaksysteem::DB::Component::Logging::Case::Note
            Zaaksysteem::DB::Component::Logging::Case::Payment
            Zaaksysteem::DB::Component::Logging::Case::Pip
            Zaaksysteem::DB::Component::Logging::Case::Relation
            Zaaksysteem::DB::Component::Logging::Case::Subject
            Zaaksysteem::DB::Component::Logging::Case::Update
            Zaaksysteem::DB::Component::Logging::Casetype
            Zaaksysteem::DB::Component::Logging::Contactmoment
            Zaaksysteem::DB::Component::Logging::Email
            Zaaksysteem::DB::Component::Logging::InternalNote
            Zaaksysteem::DB::Component::Logging::NatuurlijkPersoon
            Zaaksysteem::DB::Component::Logging::Ou
            Zaaksysteem::DB::Component::Logging::Stuf::RemoteSearch
            Zaaksysteem::DB::Component::Logging::Subject
            Zaaksysteem::DB::Component::Logging::Subject::Contactmoment
            Zaaksysteem::DB::Component::Logging::Subject::Note
            Zaaksysteem::DB::Component::Logging::Sysin
            Zaaksysteem::DB::Component::Logging::Sysin::Interface
            Zaaksysteem::DB::Component::Logging::Template
            Zaaksysteem::DB::Component::Logging::Template::Email
            Zaaksysteem::DB::Component::Logging::User
            Zaaksysteem::DB::Component::Logging::User::Update
            Zaaksysteem::DB::Component::Logging::Xential
        )
    };
}

sub _apply_roles {
    my ($self) = @_;

    unless ($self->event_type) {
        my $data = $self->infer_event_type;

        if ($data) {
            $self->event_data(JSON->new->canonical->encode($data));
        }
    }

    if ($self->event_type) {
        apply_all_roles($self, 'Zaaksysteem::DB::Component::Logging::Event');

        # Apply all possible roles to this event that we can find
        # Unroll the event_type, append event_type parts to the module we're looking for
        # Example: case/checklist/item/update

        # First iteration: try to apply ZS::DB::C::Logging::Case
        # Second iteration:             ZS::DB::C::Logging::Case::Checklist
        # Third iteration:              ZS::DB::C::Logging::Case::Checklist::Item
        # Final iteration:              ZS::DB::C::Logging::Case::Checklist::Item::Update

        # The reason for this is so we get some inheritence-like behavior in our roles.
        my @type = split('/', $self->event_type);
        my @subtype_iterator;

        while(scalar(@type)) {
            push(@subtype_iterator, shift @type);

            my $role = $self->inflect_event_type(join('/', @subtype_iterator));

            try {
                # Skip roles which we know do not exists
                return if $self->_skip_roles->{$role};

                apply_all_roles($self, $role);
            }
            catch {
                $self->log->warn("Error applying role $role: $_");
            };
        }

        $self->_add_magic_attributes;
    }

    return $self;
}

sub _set_effective_loglevel {
    my $self    = shift;

    my $levels  = ZAAKSYSTEEM_LOGGING_LEVELS;

    if ($self->loglevel) {
        $self->loglevel(
            $self->loglevel || 1
        );

        if ($self->loglevel !~ /^\d+$/) {
            $self->loglevel(
                $levels->{$self->loglevel} || 1
            );
        }
    }
}

sub infer_event_type {
    my $self = shift;

    if (!defined $self->onderwerp) {
        $self->log->warn("No logging possible for event type: " . ($self->event_type // "No event type"));
        return;
    }

    if($self->onderwerp =~ 'Zaak \((\d+)\) aangemaakt') {
        $self->event_type('case/create');

        return {
            case_id => $self->get_column('zaak_id'),
            casetype_id => $self->zaak_id->get_column('zaaktype_id')
        };
    }

    if($self->onderwerp =~ 'Zaak gesloten op (.*)') {
        $self->event_type('case/close');

        return {
            case_id => $self->get_column('zaak_id'),
            close_date => $1
        }
    }

    if($self->onderwerp =~ 'Kenmerk "(.*)" gewijzigd naar: "(.*)"') {
        $self->event_type('case/attribute/update');

        my $attribute = $self
                        ->result_source
                        ->schema
                        ->resultset('ZaaktypeKenmerken')
                        ->search({
                            'bibliotheek_kenmerken_id.naam' => { 'ilike' => $1 },
                            'me.zaaktype_node_id' => $self->zaak_id->get_column('zaaktype_node_id')
                        },
                        {
                            join    => 'bibliotheek_kenmerken_id'
                        }
                    )->first;

        return {
            case_id => $self->get_column('zaak_id'),
            attribute_id => $attribute ? $attribute->get_column('bibliotheek_kenmerken_id') : undef,
            attribute_value => $2
        };
    }

    if($self->onderwerp =~ 'Document "(.*)" \[(\d+)\] succesvol aangemaakt') {
        $self->event_type('case/document/create');

        return {
            case_id => $self->get_column('zaak_id'),
            document_filename => $1,
            document_id => $self->component_id
        };
    }

    if($self->onderwerp =~ 'Vernietigingsdatum gewijzigd: (.*)') {
        $self->event_type('case/update/purge_date');

        return {
            case_id => $self->get_column('zaak_id'),
            purge_date => $1
        };
    }

    if($self->onderwerp =~ 'Streefafhandeldatum voor zaak: "(.*)" gewijzigd naar: (.*)') {
        $self->event_type('case/update/target_date');

        return {
            case_id => $self->get_column('zaak_id'),
            target_date => $2
        };
    }

    if($self->onderwerp =~ 'registratiedatum voor zaak: "(.*)" gewijzigd naar: (.*)') {
        $self->event_type('case/update/registration_date');

        return {
            case_id => $self->get_column('zaak_id'),
            registration_date => $2
        };
    }

    if($self->onderwerp =~ 'Status gewijzigd naar: (.*)') {
        $self->event_type('case/update/status');

        return {
            case_id => $self->get_column('zaak_id'),
            status => $1
        };
    }

    if($self->onderwerp =~ 'Betrokkene "(.*)" gewijzigd naar: "(.*)"') {
        $self->event_type('case/relation/update');

        return {
            case_id => $self->get_column('zaak_id'),
            subject_relation => $1,
            subject_name => $2
        };
    }

    if($self->onderwerp =~ 'Betrokkene: "(.*)" toegevoegd aan zaak, relatie: (.*)') {
        $self->event_type('case/relation/update');

        return {
            case_id => $self->get_column('zaak_id'),
            subject_relation => $1,
            subject_name => $2
        };
    }

    if($self->onderwerp =~ 'Kenmerk "(.*)" verwijderd ivm verbergen door regels.') {
        $self->event_type('case/attribute/remove');

        return {
            case_id => $self->get_column('zaak_id'),
            attribute_id => $self->component_id
        };
    }

    if($self->onderwerp =~ 'Sjabloon \((.*)\) toegevoegd') {
        $self->event_type('case/template/add');

        return {
            case_id => $self->get_column('zaak_id'),
            template_name => $1
        };
    }

    if($self->onderwerp =~ 'Sjabloon "(.*)" \[(\d+)\] succesvol gegenereerd') {
        $self->event_type('case/template/render');

        return {
            case_id => $self->get_column('zaak_id'),
            document_filename => $1,
            document_id => $2
        };
    }

    if($self->onderwerp =~ 'Antwoord voor vraag: "(.*)" gewijzigd naar "(.*)"') {
        $self->event_type('case/checklist/item/update');

        return {
            case_id => $self->get_column('zaak_id'),
            checklist_item_name => $1,
            checklist_item_value => $2
        }
    }

    if($self->onderwerp =~ 'Notitie toegevoegd') {
        $self->event_type('case/note/create');

        return {
            content => $self->bericht
        };
    }

    $self->log->trace(sprintf("The subject '%s' does not match anything, requires an extension", $self->onderwerp));

    return;
}

sub insert {
    my $self    = shift;

    $self->_set_effective_loglevel;

    $self->next::method( @_ );
}

sub update {
    my $self    = shift;

    $self->_set_effective_loglevel;

    # Only attempt setting event data
    if(UNIVERSAL::can($self, 'data')) {
        # UTF-8 encoding is done by DBD::Pg
        $self->event_data(JSON->new->canonical->utf8(0)->encode($self->data // {}));
    } else {
        $self->event_data('{}');
    }

    $self->next::method( @_ );
}

sub loglevel    {
    my $self    = shift;

    my $loglevel    = $self->next::method(@_);

    my $levels      = ZAAKSYSTEEM_LOGGING_LEVELS;

    return $levels->{ $loglevel };
}

sub creator {
    my $self = shift;

    # Always return if there is no creator at all
    return '' unless($self->created_by);

    # Return the cached name if present
    if ($self->created_by_name_cache) {
        return $self->created_by_name_cache;
    }

    # Fall back on an LDAP lookup if there is no cached name
    my $creator = $self->result_source->schema->betrokkene_model->get({}, $self->created_by);

    if ($creator) {
        # Make sure we cache it for future queries
        $self->update({created_by_name_cache => $creator->display_name});
        return $creator->display_name;
    }

    # User no longer present in LDAP, give up
    return "<Onbekende medewerker>";
}

sub TO_JSON {
    my $self = shift;

    return {
        id             => $self->id,
        event_type     => $self->event_type,
        event_category => $self->event_category,
        case_id        => $self->get_column('zaak_id'),
        timestamp      => $self->created_localized->datetime,
        description    => $self->onderwerp,
        created_by     => $self->creator
    };

}

# Default category is always 'event'
sub event_category { 'event' }

# Convenience getter for arbitrairy resultsets
sub rs { shift->result_source->schema->resultset(shift); }

# Nor should we be filtered
sub _should_filter { return 1; }

=head2 created_localized

Creates a localized timestamp from the created timestamp.
If the event data contains a timestamp, that timestamp is also localized.

=cut

sub created_localized {
    my $self = shift;

    if ($self->can('data') && ref $self->data eq 'HASH' && $self->data->{timestamp}) {
        my $dt = DateTime::Format::ISO8601->parse_datetime($self->data->{timestamp});
        $self->data->{timestamp} = $self->_create_localized($dt)->datetime;
    }

    return $self->_create_localized($self->created);
}

=head2 _create_localized

Private helper function to create localized timezones.

=head3 TODO

Perhaps the timezone should be in a configuration, so you can pick and choose, and/or based on the timezone of the client.
In any case it has feature request written all over it.

=cut

sub _create_localized {
    my ($self, $dt) = @_;
    return $dt->set_time_zone('UTC')->set_time_zone('Europe/Amsterdam');
}


=head2 delete_messages

When cleaning up cases we need to get rid of the logging as well. When a
message is created it references the logging. So we need to clean that up
first.

=cut

sub delete_messages {
    my $self = shift;

    $self->messages->delete_all;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 creator

TODO: Fix the POD

=cut

=head2 event_category

TODO: Fix the POD

=cut

=head2 infer_event_type

TODO: Fix the POD

=cut

=head2 inflate_result

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 loglevel

TODO: Fix the POD

=cut

=head2 new

TODO: Fix the POD

=cut

=head2 rs

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

