package Zaaksysteem::DB::Component::GenericNatuurlijkPersoon;

use strict;
use warnings;
use BTTW::Tools;

use base qw/DBIx::Class/;

use Zaaksysteem::Backend::Subject::Naamgebruik qw/naamgebruik/;

use Moose;


=head1 ATTRIBUTES

=head2 display_name

=cut

has 'display_name' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        ### Depends on aanduiding naamgebruik.
        my $voorletters = $self->voorletters;
        if (!$voorletters) {
            $voorletters = $self->_build_voorletters;
        }

        my @name;
        if ($voorletters) {
            push(@name, $voorletters);
        }
        if ($self->achternaam) {
            push(@name, $self->achternaam);
        }

        return '' unless @name;
        return join(" ", @name);
    },
);

=head2 voorletters

=cut

has 'voorletters'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'builder'   => '_build_voorletters',
);

sub _build_voorletters {
    my $self        = shift;
    my $val         = shift;

    my $voornamen   = defined($val) ? $val : $self->voornamen;

    if (!$voornamen) {
        return '';
    }

    my ($firstchar) = $voornamen =~ m/^(\w{1})/;
    my @other_chars = $voornamen =~ m/ (\w{1})/g;

    return join(".", $firstchar, @other_chars) . (
        ($firstchar || @other_chars) ?
        '.' : ''
    );
}

=head2 achternaam

=cut

has 'achternaam'  => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return naamgebruik({
            aanduiding => $self->aanduiding_naamgebruik || '',
            partner_voorvoegsel => $self->partner_voorvoegsel,
            partner_geslachtsnaam => $self->partner_geslachtsnaam,
            voorvoegsel => $self->voorvoegsel,
            geslachtsnaam => $self->geslachtsnaam,
        });
    }
);

=head2 cached_addresses

Returns: ARRAYREF of Zaaksysteem::Schema::Adres or Zaaksysteem::Schema::Adres

Return an arrayref of addresses linked to this NP

=cut

has 'cached_addresses' => (
    is      => 'rw',
    lazy    => 1,
    isa     => 'ArrayRef',
    builder => '_build_cached_addresses',
    clearer => '_clear_cached_addresses',
);

sub _build_cached_addresses {
    my $self = shift;

    my $table = 'Adres';
    if ($self->result_source->name =~ /^gm_/) {
        $table = 'GmAdres';
    }

    my $rv  = [];

    push(@$rv, $self->adres_id) if $self->adres_id;

    ### Return all addresses
    return [ @$rv, $self->result_source->schema->resultset($table)->search({natuurlijk_persoon_id => $self->id, deleted_on => undef})->all ];
}

sub TO_JSON {
    my $self                = shift;

    my $json                = $self->next::method(@_);

    if (my $os = $self->subscription_id) {
        if (!$os->date_deleted) {
            $json->{object_subscription} = {
                'id' => $os->id,
                'external_id' => $os->external_id,
            };
        }
    }

    $json->{burgerservicenummer} = $self->bsn;

    return $json;
}

sub in_onderzoek {
    my $self    = shift;

    my @checks = grep( { $_ =~ /^onderzoek_.*?/ } $self->columns);

    my @failures;
    for my $check (@checks) {
        my $categorie   = $check;
        $categorie      =~ s/onderzoek_(.*?)/$1/;

        if ($self->$check) {
            push(@failures, $categorie);
        }
    }

    return \@failures if scalar(@failures);
    return;
}

sub is_overleden {
    my $self    = shift;

    return 1 if $self->datum_overlijden;
    return;
}

=head2 correspondentieadres

Return: L<Zaaksysteem::Schema::Adres>

=cut

has 'correspondentieadres'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'builder'   => '_build_correspondentieadres',
    'clearer'   => '_clear_correspondentieadres',
);

sub _build_correspondentieadres {
    my $self        = shift;

    my ($adres)     = grep ( { $_->functie_adres eq 'B' } @{ $self->cached_addresses });

    return $adres;
}

=head2 verblijfsadres

Return: L<Zaaksysteem::Schema::Adres>

=cut

has 'verblijfsadres'        => (
    'is'        => 'rw',
    'lazy'      => 1,
    'builder'   => '_build_verblijfsadres',
    'clearer'   => '_clear_verblijfsadres',
);

sub _build_verblijfsadres {
    my $self        = shift;

    my ($adres)     = grep ( { $_->functie_adres eq 'W' } @{ $self->cached_addresses });

    return $adres;
}


=head2 add_address

Arguments: \%ADDRESS_PARAMS

Checks whether there is already an address of this type, add a new one and remove the old one.

The adres_id gets set to the "verblijfsadres" when there is no correspondentieadres. When no verblijfsadres
available, set it to correspondentieadres.

=cut

define_profile add_address => (
    optional    => {
        straatnaam => 'Str',
        huisnummer => 'Num',
        postcode => 'Str',
        woonplaats => 'Str',
        huisletter => 'Str',
        huisnummertoevoeging => 'Str',
        landcode   => 'Num',
        gemeente_code => 'Num',
        adres_buitenland1 => 'Str',
        adres_buitenland2 => 'Str',
        adres_buitenland3 => 'Str',
        functie_adres => 'Str',
    },
);

sub add_address {
    my $self        = shift;
    my $params      = assert_profile(shift || {})->valid;
    my $options     = shift || {};

    die('Add address can only be called on an external betrokkene object') unless $self->result_source->name eq 'natuurlijk_persoon';
    $params->{functie_adres} ||= 'W';

    $self->result_source->schema->txn_do(sub {
        my $address = $self->addresses->create(
            {
                %$params,
                natuurlijk_persoon_id => $self->id,
            }
        );

        if ($options->{mutation_record}) {
            $options->{mutation_record}->table_id($address->id);
            $options->{mutation_record}->from_dbix($address);
        }

        my @old_addresses = $self->addresses->search()->all;

        ### Delete old record(s)
        my ($cor_adres, $vbl_adres);
        for my $old_address (@old_addresses) {
            if ($address->id != $old_address->id && $old_address->functie_adres eq $address->functie_adres) {
                ### Unset link first
                if ($self->adres_id && $old_address->id == $self->get_column('adres_id')) {
                    $self->adres_id(undef);
                    $self->update;
                }

                $old_address->delete;
                next;
            }

            if ($old_address->functie_adres eq 'B') {
                $cor_adres = $old_address;
            } elsif($old_address->functie_adres eq 'W') {
                $vbl_adres = $old_address;
            }
        }

        ### Correct the link
        $self->update_address_links;
    });

    return $self->adres_id;
}

=head2 update_address_links

Arguments: none

Return value: $TRUE_ON_SUCCESS

    $success = $np->update_address_links();

Updates the link from a natuurlijk_persoon to the active address row. Correspondentieaddress when it is
set, otherwise the verblijfsadres

=cut

sub update_address_links {
    my $self        = shift;

    $self->_clear_cached_addresses;
    $self->_clear_correspondentieadres;
    $self->_clear_verblijfsadres;

    $self->adres_id($self->correspondentieadres || $self->verblijfsadres);

    $self->update;
}

=head2 delete_address_by_function

Arguments: $STRING_FUNCTIE_LETTER (enum: B or W)

$np->delete_address_by_function('B');

Removes an address by function

=cut

sub delete_address_by_function {
    my $self        = shift;
    my $code        = uc(shift);

    throw('betrokkene/np/invalide_functie_adress', 'Functie adres must be one of B or W') unless $code =~ /^[WB]$/;

    $self->result_source->schema->txn_do(sub {
        my @addresses = $self->addresses->search(
            {
                deleted_on      => undef,
            }
        )->all;

        ### Unset adres_id (foreign key problems)
        $self->adres_id(undef);
        $self->update;

        ### Delete entry
        my $adres_id;
        for my $address (@addresses) {
            if ($address->functie_adres eq $code) {
                $address->delete;
                next
            }

            $adres_id       = $address->id;
        }

        ### Correct the link
        $self->update_address_links;
    });

    return 1;
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 in_onderzoek

TODO: Fix the POD

=cut

=head2 is_overleden

TODO: Fix the POD

=cut

