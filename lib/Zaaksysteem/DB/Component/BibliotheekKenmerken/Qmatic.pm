package Zaaksysteem::DB::Component::BibliotheekKenmerken::Qmatic;
use Moose::Role;

with 'Zaaksysteem::DB::Component::BibliotheekKenmerken::Calendar';

=head2 format_as_string

A case field is updated, and this must be recorded in the logs and displayed as a file.
A QMatic value looks like "day;time;id", where day and time are ISO8601 datetimes.

=cut

sub format_as_string {
    my ($self, $values) = @_;

    return map { $self->format_qmatic($_) } @$values;
}


sub format_qmatic {
    my ($self, $value) = @_;

    my $qmatic_value = $self->parse_qmatic_value($value);

    return "Afspraak geboekt op: ". format_date($qmatic_value->{appDate}) .
        ' om ' . format_time($qmatic_value->{appTime});
}


sub parse_qmatic_value {
    my ($self, $value) = @_;

    my ($appDate, $appTime, $appointmentId) = split /;/, $value;

    return {
        appDate         => $appDate,
        appTime         => $appTime,
        appointmentId     => $appointmentId
    };
}

=head2 reject_pip_change_request

From the QMatic documentation ('Q-MATIC Calendar Web Service API - Documenation V3.0.PDF'):
(misspelling of documentation is accurate)

Delete an existing appointment through the appointment ID
Input
    appointmentId Integer Unique appointment Id
Output
    An AppointmentStatusType element containing;
    appointmentId Integer This will always be '0'
    appointmentStatus Integer
        Integer representing an appointment status code. Status codes:
        1 = Appointment Deleted
        5 = An unknown error occurred.

=cut

sub reject_pip_change_request {
    my ($self, $parameters) = @_;

    my $qmatic_value = $self->parse_qmatic_value($parameters->{value});

    if ($qmatic_value->{appointmentId}) {

        my $schema = $self->result_source->schema;

        my $qmatic_interface = $schema->resultset('Interface')->search_active({ module => 'qmatic' })->first
                or die "need qmatic interface";

        my $result = $qmatic_interface->process_trigger('deleteAppointment', {
            appointmentId => $qmatic_value->{appointmentId}
        });

        $schema->resultset('Logging')->trigger('case/pip/rejectupdate', {
            component => 'case',
            component_id  => $parameters->{case_id},
            zaak_id       => $parameters->{case_id},
            betrokkene_id => undef,
            data => {
                reason => 'QMatic afspraak nummer ' . $qmatic_value->{appointmentId} . ' verwijderd'
            }
        });

        return $result && @$result && $result->[0]->{appointmentStatus} eq '1';
    }
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 format_qmatic

TODO: Fix the POD

=cut

=head2 parse_qmatic_value

TODO: Fix the POD

=cut

