package Zaaksysteem::DB::Component::Logging::Auth::Alternative::Account;
use Moose::Role;

=head2 subject

An L<Zaaksysteem::Model::DB::Subject> object

=cut

has subject => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;
        $self->rs('Subject')->find($self->data->{subject_id});
    }
);

=head2 onderwerp

Pretty print the subject

=cut

sub onderwerp {
    my $self = shift;

    my $data = $self->data;

    my $id;
    if ($data->{subject_type} eq 'person') {
        $id = "BSN: $data->{bsn}";
    }
    else {
        if ($data->{vestigingsnummer}) {
            $id = "KVK: $data->{kvknummer}/ Vestigingsnummer: $data->{vestigingsnummer}"
        }
        else {
            $id = "KVK: $data->{kvknummer}";
        }
    }

    my $msg;

    if ($data->{action} eq 'create') {
        $msg = "Gebruiker '%s' (%s) aangemaakt met email '%s' en telefoonnummer '%s'";
    }
    else {
        $msg = "Gebruiker '%s' (%s) verwijderd";
    }
    return sprintf($msg, $data->{username}, $id, $data->{email}, $data->{phone});

}

sub event_category { 'system'; }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
