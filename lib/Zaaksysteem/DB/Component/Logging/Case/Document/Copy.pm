package Zaaksysteem::DB::Component::Logging::Case::Document::Copy;
use Moose::Role;
use HTML::Entities qw(encode_entities);

=head1 NAME

Zaaksysteem::DB::Component::Logging::Case::Document::Copy - Event message
handler for case/document/copy events.

=head1 DESCRIPTION

See L<Zaaksysteem::DB::Component::Logging::Event>.

=head1 METHODS

=head2 onderwerp

Defines the logline for this event. Assumes C<< $self->data >> to have this
structure:

=cut

sub onderwerp {
    my $self = shift;

    my $msg = sprintf(
        "Document '%s' (versie %d) gekopieerd naar document '%s' (versie %d)",
        encode_entities($self->data->{original_filename}),
        $self->data->{original_version},
        encode_entities($self->data->{copy_filename}),
        $self->data->{copy_version}
    );

    if ($self->data->{case_id}) {
        $msg .= " voor zaak " . $self->data->{case_id};
    }
    return $msg;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
