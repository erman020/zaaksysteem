package Zaaksysteem::DB::Component::Logging::Case::Document::Label;

use Moose::Role;
use HTML::Entities qw(encode_entities);

=head2 onderwerp

Subject of ths log entry

=cut

sub onderwerp {
    my $self = shift;

    sprintf(
        'Bestand "%s" is %s als zaakdocument "%s"',
        encode_entities($self->data->{ file_name }),
        $self->data->{action} eq 'remove' ? 'verwijderd' : 'toegevoegd',
        encode_entities($self->data->{ label }),
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
