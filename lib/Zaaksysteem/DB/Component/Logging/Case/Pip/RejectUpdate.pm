package Zaaksysteem::DB::Component::Logging::Case::PIP::RejectUpdate;

use Moose::Role;

sub onderwerp {
    sprintf('Kenmerk aanpassingen "%s" afgewezen', shift->data->{ reason });
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

