package Zaaksysteem::DB::Component::Logging::Subject::Internalnote::Create;

use Moose::Role;

# THIS CLASS COVERS HISTORIC DATA AND SHOULD NOT BE MODIFIED FURTHER.
# See ZS::DB::Component::Logging::Subject::UpdateContactData for replacement
# implementation.

=head2 onderwerp

Example logline: 'Interne notitie gewijzigd: "Deze meneer is boos" voor MK Ootjers'

=cut

sub onderwerp {
    my $self = shift;

    sprintf('Interne notitie gewijzigd voor %s (%s): "%s"', $self->data->{name}, $self->data->{ident}, $self->data->{note});
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, 2018 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
