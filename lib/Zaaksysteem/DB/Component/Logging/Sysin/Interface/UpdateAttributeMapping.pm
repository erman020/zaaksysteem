package Zaaksysteem::DB::Component::Logging::Sysin::Interface::UpdateAttributeMapping;

use Moose::Role;

use BTTW::Tools::DDiff;

=head1 NAME

Zaaksysteem::DB::Commponent::Logging::Sysin::Interface::Update - Handles
C</sysin/interface/update_attribute_mapping> event logs

=head1 METHODS

=head2 onderwerp

Overrides default event subject line generator.

=cut

sub onderwerp {
    my $self = shift;

    return sprintf(
        "Gekoppelde kenmerken voor koppeling \"%s\" aangepast:\n%s",
        $self->data->{ interface_name },
        join("\n", $self->body_lines)
    );
}

=head2 body_lines

Returns a list of changes to a sysin interface in human readable format.

=cut

sub body_lines {
    my $self = shift;

    return ddiff_stringify(@{ $self->data->{ changes } });
}

=head2 TO_JSON

Extends the default TO_JSON method by adding the C<expanded> and C<content>
keys.

=cut

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    $data->{ expanded } = JSON::false;
    $data->{ content  } = join("\n", $self->body_lines);

    return $data;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
