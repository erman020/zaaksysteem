package Zaaksysteem::DB::Component::ScheduledJobs::Case::UpdateField;
use Moose::Role;


sub approve {
    my ($self) = @_;

    my $parameters = $self->parameters;
    my $case_id    = $parameters->{case_id};
    my $case       = $self->result_source->schema->resultset('Zaak')->find($case_id);

    # Stored differently by the PIP thing
    my $value = ref $parameters->{value} eq 'ARRAY' ? $parameters->{value} : [ $parameters->{value} ];

    my $update_fields_params = {
        new_values  => {
            $parameters->{bibliotheek_kenmerken_id} => $value,
        },
        zaak    => $case,
        message => 'Wijziging goedgekeurd door behandelaar'
    };

    $case->zaak_kenmerken->update_fields($update_fields_params);

    # reject soft-deletes the scheduled job, we likey.
    $self->reject();
}


=head2 description

A case field is updated, and this must be recorded in the logs and displayed as a file.
Some fields have different view angles.

=cut

sub description {
    my ($self, $values) = @_;

    return $self->get_bibliotheek_kenmerk->apply_roles->format_as_string($values);
}

sub get_bibliotheek_kenmerk {
    my ($self) = @_;

    my $bibliotheek_kenmerken_id = $self->parameters->{bibliotheek_kenmerken_id};

    my $schema = $self->result_source->schema;

    return $schema->resultset('BibliotheekKenmerken')->find($bibliotheek_kenmerken_id);
}


sub handle_rejection {
    my ($self) = @_;

    return $self->get_bibliotheek_kenmerk->apply_roles->reject_pip_change_request($self->parameters);
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 approve

TODO: Fix the POD

=cut

=head2 get_bibliotheek_kenmerk

TODO: Fix the POD

=cut

=head2 handle_rejection

TODO: Fix the POD

=cut

