package Zaaksysteem::DB::Storage;
use warnings;
use strict;

use base 'DBIx::Class::Storage::DBI::Pg';

use Time::HiRes qw(time);
use Carp qw[longmess];
use Log::Log4perl;

=head1 NAME

Zaaksysteem::DB::Storage - Statistics logging on top of DBIx::Class::Storage::DBI::Pg

=head1 ATTRIBUTES

=head2 _query_start_time

The time the measure was started

=head2 query_list

A store with a list of queries, for later reference

=cut

__PACKAGE__->mk_group_accessors(simple => qw/_query_start_time query_list/);

=head1 METHODS

=head2 log

Interface to log4perl. This is no Moose class, so manually added

=cut

my $logger;
sub log {
    my $self    = shift;
    my $cat     = shift;

    if ($cat && $cat =~ m/^(\.|::)/) {
        return Log::Log4perl->get_logger(ref($self) . $cat);
    } elsif($cat)  {
        return Log::Log4perl->get_logger($cat);
    } else {
        return $logger if $logger;
        return ($logger = Log::Log4perl->get_logger(ref($self)));
    }
}


=head2 _query_start

Saves the start time of a query.

=cut

sub _query_start {
    my $self = shift;
    my $sql = shift;

    $self->_query_start_time(time);

    return $self->next::method($sql, @_);
}

=head2 _query_end

Logs the duration of a query (based on a start time logged earlier).

=cut

sub _query_end {
    my $self = shift;
    my $sql  = shift;
    my $bind = shift;

    my $diff    = time() - $self->_query_start_time;
    my $elapsed = sprintf("%0.5f", $diff);

    if ($ENV{DBIC_TRACE}) {
        my $caller;
        if ($ENV{DBIC_FULL_TRACE}) {
            $caller = longmess('Trace :');
        } else {
            my $iter = 10;
            my $caller_package = "";
            my $caller_line;

            while (defined $caller_package && $caller_package !~ m[^Zaaksysteem]) {
                $iter++;

                ($caller_package, $caller_line) = (caller $iter)[0, 2];
            }

            $caller = "$caller_package at line $caller_line";
        }

        my $milliseconds = ($diff * 1000);

        $self->log->trace(
            sprintf(
                "SQL: %6.2f ms | %s: %s | Caller: %s",
                $milliseconds,
                $sql,
                join(',', $self->_format_for_trace($bind)),
                $caller
            )
        );
    }

    my $query_list = $self->query_list;
    push @$query_list, {
        took  => $elapsed,
        query => $sql,
    };
    $self->query_list($query_list);

    return $self->next::method($sql, @_);
}

=head2 reset_query_counters

Resets the query list, start time counter.

=cut

sub reset_query_counters {
    my $self    = shift;

    $self->query_list([]);
    $self->_query_start_time(0);

    return;
}

### Disable standard debugging, this storage class has all we need
sub new {
    my $self  = shift->next::method(@_);

    $self->debug(0);

    return $self;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
