package Zaaksysteem::Auth::User;

use Moose;

use BTTW::Tools;

BEGIN { extends 'Catalyst::Authentication::User' }

has person => (
    is => 'ro',
    isa => 'Zaaksysteem::Model::DB::Person',
    required => 1
);

has entity => (
    is => 'ro',
    isa => 'Zaaksysteem::Model::DB::UserEntity',
    required => 1
);

sub supported_features {
    return {
        session => 1
    };
}

sub get_object {
    return shift->person;
}

sub uidnumber {
    return shift->entity->source_identifier;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get_object

TODO: Fix the POD

=cut

=head2 supported_features

TODO: Fix the POD

=cut

=head2 uidnumber

TODO: Fix the POD

=cut

