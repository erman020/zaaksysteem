#! /bin/bash

copy_conf() {
    local src=$1
    local dst=$2

    if [ ! -e "$dst" ]; then
        cp -v "$src" "$dst"
    else
        echo "Not creating $dst - file exists."
    fi
}

copy_conf etc/virus_scanner-service.conf.dist etc/virus_scanner-service.conf
copy_conf etc/zaaksysteem.conf.dist etc/zaaksysteem.conf
copy_conf etc/log4perl.conf.dist etc/log4perl.conf
copy_conf etc/default_customer.conf.dist etc/customer.d/default.conf

echo "Done!"
