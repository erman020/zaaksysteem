#!/usr/bin/env bash

set -e

DIR=$(dirname $0);
cd $DIR/../

version_file=lib/Zaaksysteem/Version.pm

version=$(grep '^our $VERSION' $version_file | \
    awk '{print $NF}' | sed -e "s/'//g" -e 's/;//g')

db_version=$(echo $version | sed -e 's/^v//')
db_version_dir=db/upgrade/$db_version
mkdir -p $db_version_dir

db_items=0

if compgen -G "db/upgrade/NEXT/*" > /dev/null; then
    for i in db/upgrade/NEXT/*
    do
        echo $i;
        file=$db_version_dir/$(basename $i .sql).sql
        git mv $i $file
        db_items=$(($db_items + 1))
    done

    git commit db/upgrade/NEXT $db_version_dir \
        -m "Move files from db/upgrade/NEXT to $db_version_dir"
fi

git tag -a $version -m "Tag release of $version"

point_version=$(echo $version | cut -d\. -f3)
major_version=$(echo $version | cut -d\. -f1,2)

new_version=$(echo $major_version.$(( $point_version + 1)))

sed -i -e "s/$version/$new_version/" $version_file
git commit -m "Bump release version to $new_version" $version_file
