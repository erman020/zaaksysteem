#!/bin/bash

set -x
set -e

db=zaaksysteem
hostname=vagrant.zaaksysteem.nl

usage() {
    cat <<OEF

    $(basename $0) OPTIONS

    h       This help
    u       Hostname
    d       Database name
    f       File to upload
    n       New file to upload
    l       Use localhost for DB queries
OEF
    exit 0

}

while getopts "hd:f:u:n:l" name
do
    case $name in
        h) usage;;
        d) db=$OPTARG;;
        f) file=$OPTARG;;
        n) new_file=$OPTARG;;
        u) hostname=$OPTARG;;
        l) PSQL_OPTS='-h localhost -U zaaksysteem';;
    esac
done
shift $((OPTIND - 1))

do_sql() {
    psql -t $PSQL_OPTS $db "$*"
}


INTERFACE=$(echo $(do_sql -c "SELECT uuid FROM interface WHERE module='xential' order by id desc limit 1"))
INTERFACE_ID=$(do_sql -c "SELECT id FROM interface WHERE uuid = '$INTERFACE'")


get_data() {

    local processor=$1
    TID=$(echo $(do_sql -c "SELECT uuid, processor_params from transaction WHERE interface_id = $INTERFACE_ID order by id desc" | grep $processor | awk '{print $1}'| head -1 ))

    PROC_PARAMS=$(do_sql -c "SELECT processor_params from transaction WHERE interface_id = $INTERFACE_ID and uuid = '$TID'")

    CID=$(echo $PROC_PARAMS | grep '"case":' | perl -pe 's/.*"case":(\d+).*/$1/');

    # Empty CID means we processed the transaction already, for testing
    # purposes we override the $CID

    if [ -z "$CID" ]
    then
        CASEUUID=$(echo $PROC_PARAMS | grep case_uuid | perl -pe 's/.*"case_uuid":"([a-z0-9-]+)".*/$1/')

    else
        CASEUUID=$(echo $(do_sql -c "SELECT uuid from object_data where object_id = $CID and object_class = 'case'"))
    fi

    FILE_UUID=$(echo $PROC_PARAMS | grep '"filestore_uuid":' | perl -pe 's/.*"filestore_uuid":"([a-z0-9-]+)".*/$1/');

    BASE_URL="https://$hostname/api/v1/sysin/interface/$INTERFACE/trigger/api_post_file?transaction_uuid=$TID&case_uuid=$CASEUUID"
}

if [ -n "$file" ]
then

    get_data _create_file_from_template

    curl -k --form "upload=@$file"  $BASE_URL

    FILE_UUID=$(echo $(do_sql -c "SELECT uuid FROM filestore fs JOIN file f on f.filestore_id = fs.id and f.generator = 'xential' and f.active_version = true order by f.id desc limit 1"));
    echo "Added file $FILE_UUID";
fi

if [ -n "$new_file" ]
then
    get_data _process_request_edit_file
    curl -k --form "upload=@$new_file" "$BASE_URL&file_uuid=$FILE_UUID"
fi
