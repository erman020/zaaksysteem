#!/bin/bash

if [ "$4" == "" ]; then
    echo "USAGE: $0 <user> <password> <basedn> <ldif_file.ldif> <notifdn>";
    echo
    echo "Imports given ldif in local LDAP server."
    echo;
    echo "notifdn    - Do not import when given notifdn exists"
    exit;
fi

LDAPUSER=$1;
LDAPPASS=$2;
LDAPBASE=$3;
LDAPFILE=$4;
LDAPNOTIF=$5;
LDIF=$4;

if [ "$LDAPNOTIF" != "" ]; then
    ldapsearch -xw $LDAPPASS -D $LDAPUSER -b $LDAPNOTIF -s base >/dev/null 2>&1;
    if [ "$?" -lt "1" ]; then
        exit;
    fi
fi



### Check if OpenLDAP is UP
CONNECTED=0;
COUNTER=10;
while [ "$CONNECTED" -lt 1 ]; do
    echo "Try to connect to LDAP"
    ldapsearch -xw $LDAPPASS -D $LDAPUSER -b $LDAPBASE -s base >/dev/null 2>&1;

    if [ "$?" -lt "200" ]; then
        CONNECTED=1;
        echo "Connected to LDAP"
        continue;
    fi

    if [ "$COUNTER" -lt "200" ]; then
        echo "Could not connect to LDAP after 10 retries. Exit"
        exit;
    fi

    COUNTER=`expr $COUNTER - 1`;
    echo "Could not connect to LDAP, try again in 1 second";
    sleep 1;
done

ldapsearch -xw $LDAPPASS -D $LDAPUSER -b $LDAPBASE -s base >/dev/null 2>&1

if [ "$?" -gt "0" ]; then
    echo "Could not find BASEDN: $LDAPBASE , creating"
    echo '
dn: dc=zaaksysteem,dc=nl
objectClass: top
objectClass: dcObject
objectClass: organization
o: zaaksysteem.nl
dc: zaaksysteem
    ' | ldapadd -x -w $LDAPPASS -D $LDAPUSER;

    if [ "$?" -gt "1" ]; then
        echo "Error creating $LDAPBASE , exit"
        exit;
    fi
fi

ldapadd -x -w $LDAPPASS -D $LDAPUSER -f $LDAPFILE;