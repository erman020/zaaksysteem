#! python3
"""
Simple SMTP daemon that drops all email in a location (configurable using the
environment)
"""
import asyncore
import logging
import signal
import sys
from email import message_from_bytes
from mailbox import Maildir, MaildirMessage
from os import environ
from smtpd import SMTPServer

class TestingSMTPServer(SMTPServer):
    """
    A simple sandbox SMTP server that delivers all incoming mail to an maildir
    file specified on the command line.
    """

    def set_maildir(self, mailbox):
        self.maildir = mailbox

    def process_message(self, peer, mailfrom, rcpttos, data, **kwargs):
        logging.info( 'Message received from "{}" to "{}"'.format(mailfrom, rcpttos) )

        msg = message_from_bytes(data, _class=MaildirMessage)
        self.maildir.add(msg)

def get_config():
    cfg = {
        'port': 8025,
        'maildir': '/tmp/maildir',
    }

    if 'SMTPD_PORT' in environ:
        cfg['port'] = int(environ['SMTPD_PORT'])
    if 'SMTPD_MAILDIR' in environ:
        cfg['maildir'] = environ['SMTPD_MAILDIR']

    return cfg

def signal_handler(signum, stackframe):
    print("Signal {} received. Exiting gracefully.".format(signum))
    sys.exit(0)

logging.basicConfig(level=logging.DEBUG)

config = get_config();

s = TestingSMTPServer(('0.0.0.0', config['port']), None, decode_data=False)
mailbox = Maildir(config['maildir'])
s.set_maildir(mailbox)

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

asyncore.loop()
