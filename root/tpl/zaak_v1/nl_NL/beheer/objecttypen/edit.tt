<div class="object-type-edit" data-ng-controller="nl.mintlab.admin.objecttype.ObjectTypeEditController" data-ng-init="categoryId='[% category_id %]';objectTypeId='[% objecttype_id %]';init()" data-zs-object-type-edit>
	
	

	<div class="block block-progress">
		<div class="block-header block-header-border clearfix">
			<div class="block-header-title"><[activePhase.label]></div>
		</div>
		
		<div class="block-content" data-ng-cloak data-ng-if="isMetadataLoaded()">
			
			<div class="object-type-form object-type-form-<[phase.id]>" data-ng-repeat="phase in phases" data-zs-form-template-parser data-config="getForm(phase.id)" data-ng-show="phase===activePhase">
			</div>
			
			<div class="form-actions-sticky object-type-edit-actions clearfix">
				<div class="object-type-edit-actions-cancel">
					<a href="/beheer/bibliotheek" data-ng-show="isFirstPhase()" class="button button-secondary left">
						Annuleren
					</a>
					<button data-ng-click="prevPhase()" data-ng-show="!isFirstPhase()" class="button button-secondary left">
						Vorige
					</button>
				</div>
				<ul data-zs-breadcrumb="activePhase.id" data-zs-breadcrumb-select="handlePhaseSelect($value)" class="voortgang">
					
					<li data-ng-repeat="phase in phases" data-zs-breadcrumb-item="phase.id">
						<div><[phase.label]></div>			
					</li>
					
				</ul>
				<div class="object-type-edit-actions-continue">
					<button type="button" data-ng-click="nextPhase()" data-ng-show="!isLastPhase()" data-ng-disabled="!isActiveFormValid()"class="button button-primary right">
						Volgende
					</button>
					<button type="button" data-ng-click="handleFinishClick()" data-ng-disabled="isLastPhase()&&!isActiveFormValid()" class="button right" data-ng-class="{'button-secondary': !isLastPhase(), 'button-primary': isLastPhase() }" data-ng-show="isLastPhase()||!isNewObject()">
						<[isNewObject()?'Afronden':'Opslaan']>
					</button>
				</div>
			</div>
		</div>
		
	</div>
	
	

	<script type="text/zs-scope-data">
		{
			"phases": [
				{
					"id": "general",
					"label": "Algemeen",
					"form": {
						"name": "form_general",
						"fields": [
							{
								"name": "name",
								"type": "text",
								"label": "Naam objecttype",
								"required": true
							},
                            {
                                "name": "title_template",
                                "type": "text",
                                "label": "Objecttitel",
                                "required": true
                            },
                            {
                                "name": "category_id",
                                "type": "select",
                                "label": "Categorie",
                                "required": true,
                                "data": {
                                    "options": [% category_options %]
                                }
                            },
							{
								"name": "attributes",
								"type": "object-type-attribute-list",
								"label": "Kenmerken",
								"data": {
									"multi": 5,
									"params": {
										"exclude_system_attributes": true
									}
								}
							}
						]
					}
				},
				{
					"id": "relations",
					"label": "Relaties",
					"form": {
						"name": "form_relations",
						"fields": [
							{
								"name": "related_casetypes",
								"type": "object-type-casetype-list",
								"label": "Gerelateerde zaaktypen",
								"default": [],
                                "data": {
                                    "params": { "object_type": "casetype" }
                                }
							},
							{
								"name": "related_objecttypes",
								"type": "spot-enlighter",
								"label": "Gerelateerde objecttypen",
								"data": {
									"label": "label",
									"restrict": "objecttypes",
									"multi": true
								},
								"default": []
							}
						]
					}
				},
				{
					"id": "rights",
					"label": "Rechten",
					"form": {
						"name": "form_rights",
						"fields": [
							{
								"name": "instance_authorizations",
								"type": "org-unit-rights",
								"label": "Openbaar/intern",
								"data": {
									"availableRights": [ "zaak_beheer", "zaak_read", "zaak_edit" ]
								},
								"value": [
								]
							}
						]
					}
				},
				{
					"id": "finish",
					"label": "Afronden",
					"form": {
						"name": "form_finish",
						"fields": [
							{
								"name": "objecttype_name_display",
								"label": "Naam objecttype",
								"type": "display",
								"data": {
									"template": "<[getObjectName()]>"
								}
							},
							{
								"name": "modified_sections",
								"label": "Componenten gewijzigd",
								"type": "checkbox-list",
								"data": {
									"options": [
										{
											"value": "general",
											"label": "Algemeen"
										},
										{
											"value": "relations",
											"label": "Relaties"
										},
										{
											"value": "rights",
											"label": "Rechten"
										}
									]
								},
								"required": true,
								"value": []
							},
							{
								"name": "modification_rationale",
								"label": "Wijzigingsomschrijving",
								"type": "textarea",
								"required": true
							},
							{
								"name": "object_validation_results",
								"template": "/html/admin/objecttype/validate.html"
							}
						]
					}
				}
			]
		}
	</script>
	
</div>

<script type="text/ng-template" id="/html/form/form-field-type-object-type-attribute-list.html">
	
	<div class="form-field-spot-enlighter-component" data-ng-model="scope[field.name]" data-zs-object-type-attribute-form-field data-zs-spot-enlighter-form-field data-zs-spot-enlighter-form-field-multiple="5" data-zs-form-field data-zs-spot-enlighter-form-field-transform="objectTypeAttributeFormField.transform($object)" data-zs-spot-enlighter-resolve="attribute_id">
		<div class="object-type-attribute-list table table-fixed form-field-spot-enlighter-component-list" data-ng-show="scope[field.name].length" data-zs-sort>
			<div class="object-type-attribute-list-item object-type-attribute-list-item-header table-header">
				<div class="object-type-attribute-list-item-cell object-type-attribute-list-item-cell-sort object-type-attribute-list-item-header-cell table-cell">
				</div>
				<div class="object-type-attribute-list-item-cell object-type-attribute-list-item-cell-index object-type-attribute-list-item-header-cell table-cell">
					Index
				</div>
				<div class="object-type-attribute-list-item-cell object-type-attribute-list-item-cell-attribute object-type-attribute-list-item-header-cell table-cell">
					Kenmerk
				</div>
				<div class="object-type-attribute-list-item-cell object-type-attribute-list-item-cell-title object-type-attribute-list-item-header-cell table-cell">
					Titel
				</div>
				<div class="object-type-attribute-list-item-cell object-type-attribute-list-item-cell-required object-type-attribute-list-item-header-cell table-cell">
					Verplicht
				</div>
				<div class="object-type-attribute-list-item-cell object-type-attribute-list-item-cell-options object-type-attribute-list-item-header-cell table-cell">
					
				</div>
			</div>
			<div class="object-type-attribute-list-item object-type-attribute-list-item-value table-body form-field-spot-enlighter-component-list-item" data-ng-repeat="object in scope[field.name]" data-zs-popup="'/html/admin/objecttype/edit-attribute.html'" data-ng-controller="nl.mintlab.admin.objecttype.ObjectTypeAttributeController">
				<div class="object-type-attribute-list-item-cell object-type-attribute-list-item-cell-sort table-cell" data-zs-sortable="object" data-ng-draggable>
					<div class="draggable-component-drag"></div>
				</div>
				<div class="object-type-attribute-list-item-cell object-type-attribute-list-item-cell-index table-cell">
					<input type="checkbox" data-ng-model="object.index" data-ng-change="objectTypeAttributeFormField.handleIndexChange(object)"/>
				</div>
				<div class="object-type-attribute-list-item-cell object-type-attribute-list-item-cell-attribute table-cell">
					<i class="icon icon-graphic icon-kenmerken"></i> <[object.attribute_label]>
				</div>
				<div class="object-type-attribute-list-item-cell object-type-attribute-list-item-cell-title table-cell">
					<input type="text" data-ng-model="object.label" data-ng-required="true"/>
				</div>
				<div class="object-type-attribute-list-item-cell object-type-attribute-list-item-cell-required table-cell">
					<input type="checkbox" data-ng-model="object.required"/>
				</div>
				<div class="object-type-attribute-list-item-cell object-type-attribute-list-item-cell-options table-cell">
					<div class="row-actions">
						<button type="button" data-ng-click="openPopup()">
							<i class="icon-font-awesome icon-pencil"></i>
						</button>
						<button type="button" data-ng-click="remove(object)">
							<i class="icon-font-awesome icon-remove"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="spot-enlighter-wrapper">
			<input type="text" data-ng-model="obj" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="'attributes'" data-zs-spot-enlighter-params="field.data.params" data-zs-placeholder="field.data.placeholder||'Begin te typen&hellip;'" />
		</div>
	</div>
	

</script>

<script type="text/ng-template" id="/html/admin/objecttype/edit-attribute.html">
	<div data-zs-modal data-ng-init="title='Kenmerk instellen'">
	
		<div data-zs-form-template-parser="<[form]>">
		</div>
		
		<script type="text/zs-scope-data">
			{
				"form": {
					"name": "attribute_edit_form",
					"fields": [
						{
							"name": "internal_description",
							"label": "Toelichting (intern)",
							"type": "textarea"
						},
						{
							"name": "external_description",
							"label": "Toelichting (extern)",
							"type": "textarea"
						},
						{
							"name": "publication",
							"label": "Publiceren",
							"type": "checkbox-list",
							"data": {
								"options": [
									{
										"value": "pip",
										"label": "Persoonlijke Internet Pagina (PIP)"
									},
									{
										"value": "public",
										"label": "Publieke website"
									}
								]
							}
						},
						{
							"name": "pip_can_edit",
							"label": "Aanvrager kan kenmerk wijzigen in de PIP",
							"type": "checkbox"
						},
						{
							"name": "use_as_system_attribute",
							"label": "Gebruik als systeemkenmerk",
							"type": "checkbox"
						},
						{
							"name": "rights",
							"label": "Specifieke behandelrechten",
							"type": "org-unit"
						}
					]
				}
			}
		</script>
	</div>

</script>

<script type="text/ng-template" id="/html/form/form-field-type-object-type-casetype-list.html">
	<div class="form-field-spot-enlighter-component" data-ng-model="scope[field.name]" data-zs-spot-enlighter-form-field data-zs-spot-enlighter-form-field-multiple="5" data-zs-form-field data-zs-spot-enlighter-resolve="related_object_type + '/' + related_object_id" data-zs-spot-enlighter-form-field-transform="objectTypeCasetypeFormField.transform($object)" data-zs-object-type-casetype-form-field>
	
		<ul class="object-type-casetype-list form-field-spot-enlighter-component-list" data-ng-show="scope[field.name].length" data-zs-sort>
			<li class="form-field-spot-enlighter-list-item ng-scope" data-ng-repeat="object in scope[field.name]">
				<span class="object-type-casetype-list-item-title">
					<[object.related_object_title || '&nbsp;']>
				</span>
				
				<span class="form-field-spot-enlighter-list-item-options">
					<button type="button" class="form-field-spot-enlighter-list-item-remove icon-font-awesome icon-remove" data-ng-click="remove(object)">
					</button>
				</span>

			</li>
		</ul>
		
		<div class="spot-enlighter-wrapper">
			<input type="text" data-ng-model="obj" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="'objects'" data-zs-spot-enlighter-params="field.data.params" data-zs-placeholder="field.data.placeholder||'Begin te typen&hellip;'" />
		</div>
	</div>

</script>

<script type="text/ng-template" id="/html/form/form-field-type-org-unit-rights.html">

	<div class="form-field-casetype-rights-component" 
		data-ng-model="scope[field.name]"
		data-zs-org-unit-rights-form-field 
		data-available-rights="field.data.availableRights"
		data-zs-array-model
	>
	
		<div class="casetype-rights">
		    <div class="casetype-rights-title">
		        <[field.data.title]>
		    </div>
		    <table class="casetype-rights-title-table">
		        <thead class="casetype-rights-title-table-header">
		            <tr class="casetype-rights-title-table-row-heading">
		                <th class="casetype-rights-title-table-cell casetype-rights-title-table-cell-role">
		                    Rol
		                </th>
		                <th class="casetype-rights-title-table-cell casetype-rights-title-table-cell-rights">
		                    Rechten
		                </th>
		                <th class="casetype-rights-title-table-cell casetype-rights-title-table-cell-options">
		                </th>
		            </tr>
		        </thead>
		        <tbody class="casetype-rights-title-table-header-body">
		            <tr class="casetype-rights-title-table-row" data-ng-repeat="right in orgUnitRightsFormField.getRights()">
		                <td class="casetype-rights-title-table-cell casetype-rights-title-table-cell-role" data-zs-position-picker data-zs-position-picker-role="right.role_id" data-zs-position-picker-org-unit="right.org_unit_id" data-zs-position-picker-change="orgUnitRightsFormField.handlePositionChange(right, $orgUnitId, $roleId)">
		                </td>
		                <td class="casetype-rights-title-table-cell casetype-rights-title-table-cell-rights">
		                    <ul class="casetype-rights-type-list">
		                        <li class="casetype-rights-type-list-item" data-ng-repeat="rightType in orgUnitRightsFormField.getRightTypes()">
		                            <label>
		                                <input type="checkbox" data-ng-model="right[rightType.id]" data-ng-change="orgUnitRightsFormField.handleRightTypeChange(right)">
		                                <[rightType.label]>
		                            </label>
		                        </li>
		                    </ul>
		                </td>
		                <td class="casetype-rights-title-table-cell casetype-rights-title-table-cell-options row-actions">
		                    <button type="button" data-ng-click="orgUnitRightsFormField.removeRight(right)" class="row-action">
		                        <i class="icon-remove icon-font-awesome icon"></i>
		                    </button>
		                </td>
		            </tr>
		        </tbody>
		    </table>
		    <button type="button" class="button button-secondary button-small" data-ng-click="orgUnitRightsFormField.addRight()">
		        <i class="icon-font-awesome icon-plus"></i>
		        Rol toevoegen
		    </button>
		    
		</div>
		
		
	
	</div>
	

</script>

<script type="text/ng-template" id="/html/admin/objecttype/validate.html">
	[% PROCESS beheer/objecttypen/validate.tt %] 
</script>
