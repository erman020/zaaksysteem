<div id="relatie_definitie" class="form">
    [% UNLESS formless %]
    <form[% IF class %] class="[% class %]"[% END %]>
    [% END %]

    [% hidden_id_field %]
    <input type="hidden" name="uniqueidr" value="[% c.req.params.uniqueidr %]"/>
    <input type="hidden" name="destination" value="[% destination %]"/>
    <input type="hidden" name="update" value="1"/>
    <input type="hidden" name="rownumber" value="[% rownumber %]"/>
    <input type="hidden" name="milestone_last" value="[% milestone_last %]" />

    <table id="relatie_template" class="table-fixed">
        <tbody class="basic">
            <tr>
                <td class="td120">Toewijzing</td>
                <td class="td200">
                    [% PROCESS widgets/general/auth_select.tt
                        AUTH_SELECT_OU_NAME   = 'relaties_ou_id'
                        AUTH_SELECT_OU_VALUE  = params.ou_id
                        AUTH_SELECT_ROLE_NAME = 'relaties_role_id'
                        AUTH_SELECT_ROLE_VALUE = params.role_id
                        AUTH_SELECT_NO_GLOBAL = 1
                    %]
                </td>
            </tr>
            <tr>
                <td>Aanvrager deel-/vervolgzaak</td>
                <td>
                    <select name="relaties_eigenaar_type" id="antwoord_invoertype">
                        [% FOR eigenaar IN constants.zaaktype.deelvervolg_eigenaar %]
                            [% label = eigenaar.value.label %]

                            [% IF zaak %]
                                [% IF eigenaar.key == 'aanvrager' %]
                                    [% label = label _ ' (' _ zaak.aanvrager_object.naam _ ')' %]
                                [% END %]

                                [% IF eigenaar.key == 'behandelaar' && zaak.behandelaar_object %]
                                    [% label = label _ ' (' _ zaak.behandelaar_object.naam _ ')' %]
                                [% END %]

                                [% IF eigenaar.key == 'ontvanger' && zaak.ontvanger_object %]
                                    [% label = label _ ' (' _ zaak.ontvanger_object.naam _ ')' %]
                                [% END %]
                            [% END %]
                            <option
                                value="[% eigenaar.key %]"
                                [% (params.eigenaar_type == eigenaar.key) ? ' selected' : '' %]
                            >
                                [% label %]
                            </option>
                        [% END %]
                    </select>
                </td>
            </tr>
            [% IF params.eigenaar_id %]
                [% aanvrager = c.model('Betrokkene').get({}, params.eigenaar_id) %]
            [% END %]

            <tr class="ezra_relatie_rcpt_betrokkene">
                <td>Rol betrokkene:</td>
                <td>
                    <select name="relaties_eigenaar_role">
                    [% FOREACH role IN rollen %]
                      [% role = role | html_entity %]
                      <option value="[% role %]"[% IF params.eigenaar_role == role %] selected[% END %]>[% role %]</option>
                    [% END %]
                    </select>
                </td>
            </tr>

            [% INCLUDE widgets/search_contact.tt
                label = "Andere aanvrager"
                type_label = "Andere aanvrager type"
                ezra_id = "ezra_id_aanvrager"
                my_display = "display: none;"
                input_name = "relaties_eigenaar_id"
                selector_input = "relaties_eigenaar_searchtype"
                contact_betrokkene_naam = aanvrager.naam
                contact_betrokkene_id = params.eigenaar_id
                skip_validator = 1
            %]
        </tbody>
        <tbody class="advanced table-hide">
            <tr>
                <td>Soort:</td>
                <td class="tdvar">
                    <select class="ezra_milestone_zaak_type" name="relaties_relatie_type[% (mrelatie ? '.' _ mrelatiei : '') %]">
                    [% IF milestone_last %]
                        <option
                            [% params.relatie_type == 'vervolgzaak_datum' ? 'selected="selected"' : '' %]
                        value="vervolgzaak_datum"
                        class="vervolgzaak_datum has_start_delay">
                            Vervolgzaak (datum)
                        </option>
                        <option
                            [% params.relatie_type == 'vervolgzaak'  ? 'selected="selected"' : '' %]
                        value="vervolgzaak"
                        class="has_start_delay">
                            Vervolgzaak (periode)
                        </option>
                    [% END %]
                        <option
                        [% params.relatie_type == ZCONSTANTS.subzaken_deelzaak ? 'selected="selected"' : '' %]
                        value="[% ZCONSTANTS.subzaken_deelzaak %]">
                            Deelzaak
                        </option>
                        <option
                        [% params.relatie_type == ZCONSTANTS.subzaken_gerelateerd ? 'selected="selected"' : '' %]
                        value="[% ZCONSTANTS.subzaken_gerelateerd %]">
                            Gerelateerde zaak
                        </option>
                    </select>
                </td>
            </tr>

            [% options = [] %]
            [% IF zaak %]
                [% fasen = zaak.scalar.fasen.scalar.search({},{order_by => 'me.status'}) %]
                [% WHILE (fase = fasen.next) %]
                    [% options.push({
                        id => fase.status,
                        title => fase.fase,
                        status = fase.status
                    }) %]
                [% END %]
            [% ELSE %]
                [% FOREACH status = zaaktype.statussen %]
                    [% options.push({
                        id      => status.value.definitie.status,
                        title   => status.value.definitie.fase,
                        status  => status.value.definitie.status
                    }) %]
                [% END %]
            [% END %]

            <tr class="ezra_milestone_zaak_deelzaak_only" style="display:none;">
                <td>Afhandelen in fase</td>
                <td>
                    <div>
                        <select name="relaties_required">
                                [% FOREACH option = options.reverse %]
                                    <option
                                        [% (params.required && params.required == option.status
                                            ? 'selected="selected"'
                                            : ''
                                        ) %]
                                    value="[% option.status %]">[% option.title %]</option>
                                [% END %]
                                [% IF zaak && zaak.is_afgehandeld %]
                                    <option value=""
                                    selected="selected">[Afgehandeld]</option>
                                [% END %]
                                [% IF !zaak && !milestone_number %] 
                                    <small>(Registratiefase is niet beschikbaar)</small>
                                [% END %]
                        </select>
                    </div>
                </td>
            </tr>

            <tr>
                <td>Kenmerken kopieren</td>
                <td>
                    <div>
                        <input type="hidden" name="dialog_checkboxes" value="relaties_kopieren_kenmerken" />
                        <input type="checkbox" value="1" name="relaties_kopieren_kenmerken"[% (params.kopieren_kenmerken ? ' checked="checked"' : '') %]>
                    </div>
                </td>
            </tr>

            [% UNLESS zaak %]
            <tr class="ezra_milestone_case_relation_automatic" style="display: none;">
                <td>Starten bij afronden fase</td>
                <td>
                    <div>
                        <input type="hidden" name="dialog_checkboxes" value="relaties_status" />
                        <input type="checkbox" value="1" name="relaties_status"[% (params.status ? ' checked="checked"' : '') %]>
                    </div>
                </td>
            </tr>
            [% END %]

            <tr>
                <td>Tonen in PIP</td>
                <td>
                    <input
                      type="hidden"
                      name="dialog_checkboxes"
                      value="relaties_show_in_pip"
                    />
                    <input
                      type="checkbox"
                      value="1"
                      name="relaties_show_in_pip"[% (params.show_in_pip ? ' checked="checked"' : '') %]
                      class="show_in_pip"
                    >
                </td>
            </tr>
            <tr class="ezra_milestone_zaak_in_pip in_pip">
                <td>Label in PIP</td>
                <td>
                    <input
                      type="text"
                      class="show_in_pip_label"
                      name="relaties_pip_label"
                      value="[% params.pip_label ?  params.pip_label : ''%]"/>
                </td>
            </tr>

            <tr>
                <td>Zaak automatisch in behandeling nemen</td>
                <td>
                    <div>
                        <input type="hidden" name="dialog_checkboxes" value="relaties_automatisch_behandelen" />
                        <input type="checkbox" value="1" name="relaties_automatisch_behandelen"[% (params.automatisch_behandelen ? ' checked="checked"' : '') %]>
                    </div>
                </td>
            </tr>


            <tr class="ezra_milestone_zaak_type_starten" style="display: none;">
                <td>Starten na:</td>
                <td>
                    <input type="text" name="relaties_start_delay" value="[% params.start_delay || 0 %]" size="4" />
                    <span class="label_dagen">dagen</span>
                </td>
            </tr>

            <tr>
                <td>Betrokkene toevoegen</td>
                <td>
                    <input type="checkbox" class="show_add_subject" />
                </td>
            </tr>

            [% IF params.betrokkene_id %]
                [% betrokkene = c.model('Betrokkene').get({}, params.betrokkene_id) %]
            [% END %]

            [% INCLUDE widgets/search_contact.tt
                label = "Betrokkene"
                type_label = "Betrokkene type"
                tr_classes = ["add_subject"]
                ezra_id = "ezra_id_betrokkene"
                input_name = "relaties_betrokkene_id"
                selector_input = "relaties_betrokkene_type"
                contact_betrokkene_naam = betrokkene.naam
                contact_betrokkene_id = params.betrokkene_id
                skip_validator = 1
            %]

            [% rollen = [
                "Advocaat",
                "Auditor",
                "Aannemer",
                "Bewindvoerder",
                "Familielid",
                "Gemachtigde",
                "Mantelzorger",
                "Ouder",
                "Verzorger",
                "Ontvanger",
                "Anders"
            ] %]

            <tr class="ezra_milestone_zaak_betrokkene add_subject">
                <td>Rol betrokkene</td>
                <td>
                    <select name="relaties_betrokkene_role_set">
                        [% FOREACH role IN rollen %]
                        <option value="[% role %]"[% IF params.betrokkene_role_set == role %] selected[% END %]>[% role %]</option>
                        [% END %]
                    </select>
                </td>
            </tr>

            <tr class="ezra_milestone_zaak_betrokkene ezra_milestone_zaak_betrokkene_custom_role" style="display: none;">
                <td>Andere rol</td>
                <td>
                    <input type="text" name="relaties_betrokkene_role" value="[% params.betrokkene_role %]" />
                </td>
            </tr>

            <tr class="ezra_milestone_zaak_betrokkene add_subject">
                <td>Magic string prefix</td>
                <td>
                    <input type="text" name="relaties_betrokkene_prefix" value="[% params.betrokkene_prefix %]" readonly />
                </td>
            </tr>

            <tr class="ezra_milestone_zaak_betrokkene add_subject">
                <td>Gemachtigd voor deze zaak</td>
                <td>
                    <input type="hidden" name="dialog_checkboxes" value="relaties_betrokkene_authorized" />
                    <input type="checkbox" name="relaties_betrokkene_authorized" value="1" [% IF params.betrokkene_authorized %]checked [% END %]/>
                </td>
            </tr>

            <tr class="ezra_milestone_zaak_betrokkene add_subject">
                <td>Verstuur bevestiging per e-mail</td>
                <td>
                    <input type="hidden" name="dialog_checkboxes" value="relaties_betrokkene_notify" />
                    <input type="checkbox" name="relaties_betrokkene_notify" value="1" [% IF params.betrokkene_notify %]checked [% END %]disabled />
                </td>
            </tr>
        </tbody>
    </table>

    <a class="ezra_show_advanced_option show-more knop-text">
        <i class="icon icon-show-more"></i>
        <span>Toon geavanceerde opties</span>
    </a>

    [% UNLESS formless %]
    <div class="form-actions clearfix">
        <input type="submit" value="[% IF submit_button %][% submit_button %][% ELSE %]Opslaan[% END %]" class="button button-primary">
    </div>
    </form>
    [% END %]
</div>
