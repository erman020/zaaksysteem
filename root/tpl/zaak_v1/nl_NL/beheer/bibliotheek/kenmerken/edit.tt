[% USE JSON %]
[% USE Dumper %]

[% BLOCK block_categorie %]
    [% WHILE (categorie = bib_cat.next) %]
    [% class_definition = [];
        IF invoertype.value.multiple;
            class_definition.push("has_options");
        END;
        IF invoertype.key == 'file' || invoertype.key == 'date' ;
            class_definition.push(invoertype.key);
        END;
    %]
        <option
            value="[% categorie.id %]"
            [% IF class_definition.size %]
            class="[% class_definition.join(' ') %]"
            [% END %]
            [% (categorie_id == categorie.id) ? ' selected' : '' %]
        >[% '-' | repeat(CAT_STR_DEPTH) %] [% categorie.naam %]</option>
        [% IF categorie.scalar.bibliotheek_categories.count %]
        [% SET CAT_STR_DEPTH = CAT_STR_DEPTH + 1 %]
            [% inner_cat = categorie.scalar.bibliotheek_categories %]
            [% save_categorie = bib_cat %]
            [% INCLUDE block_categorie
                bib_cat = inner_cat
                CAT_STR_DEPTH = CAT_STR_DEPTH
            %]
            [% categorie = save_categorie %]
            [% SET CAT_STR_DEPTH = CAT_STR_DEPTH - 1 %]
        [% END %]
    [% END %]
[% END %]


[% BLOCK version_history %]
<div class="kenmerk-version-history">
        <a class="ezra_show_advanced_option show-more knop-text" rel="show_text:Toon historie;hide_text:Verberg historie">
            <i class="icon icon-show-more"></i> <span>Toon historie</span>
        </a>
        <table class="kenmerk-version-history-inner table-hide">
        <tr>
            <th>Versie</th>
            <th>Datum</th>
            <th>Gebruiker</th>
            <th>Mogelijkheden</th>
            <th></th>
        </tr>
        [% WHILE (version = versions.next) %]
            <tr>
                <td>[% version.data.version || '' %]</td>
                <td>[% version.last_modified.set_time_zone('UTC').set_time_zone('Europe/Amsterdam').strftime('%d-%m-%Y %H:%M') %]</td>
                <td>[% c.model('Betrokkene').get({ intern => 0 }, version.created_by).naam  %]</td>
                <td>
                    <ul>
                    [% UNLESS version.data.options.size %]&ndash;[% END %]
                    [% FOREACH option = version.data.options %]
                        <li [% UNLESS option.active %]style="color: #999"[% END %]>[% option.value %]</li>
                    [% END %]
                    </ul>
                </td>
                <td>
                    [% IF version.data.reason %]
                        [% PROCESS widgets/general/dropdown.tt
                            dropdown_content = version.data.reason
                        %]
                    [% END %]
                </td>
            </tr>
        [% END %]
    </table>
</div>
[% END %]

[% USE Scalar %]
[% nowrapper = 1 %]
<div id="kenmerk_definitie">
    <form action="[%
        formaction || c.uri_for(
            '/beheer/bibliotheek/kenmerken/skip/' _ bib_id _ '/bewerken'
        )
    %]" method="POST"[% (c.req.params.row_id ? '' : ' class="zvalidate"') %]>
    <input type="hidden" name="id" value="[% bib_id %]" />
    <input type="hidden" name="update" value="1" />
    <input type="hidden" name="row_id" value="[% c.req.params.row_id %]" />
    <div>
        <table class="form popup-form" id="kenmerk_template">
            <tbody>
                <tr>
                    <td class="td-label td220"><label>Naam [% c.loc(bib_type _ '1') %]</label></td>
                    <td>
                        <input type="text" name="kenmerk_naam" value="[% bib_entry.kenmerk_naam %]" class="infinite ezra_kenmerk_naam">
                    </td>
                    <td class="td250">
                        <div class="validator rounded">
                            <div class="validate-tip"></div>
                            <div class="validate-content rounded-right">
                                <span></span>
                                Deze naam bestaat al in een andere categorie.
                            </div>
                        </div>
                        <div class="tooltip-test-wrap">
                            <div class="tooltip-test rounded">
                                Typ hier de naam van het kenmerk</div>
                            <div class="tooltip-test-tip"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="td-label td220"><label>Publieke naam [% c.loc(bib_type _ '1') %]</label></td>
                    <td>
                        <input type="text" name="kenmerk_naam_public" value="[% bib_entry.kenmerk_naam_public %]" class="infinite ezra_kenmerk_naam">
                    </td>
                    <td class="td250">
                        <div class="validator rounded">
                            <div class="validate-tip"></div>
                            <div class="validate-content rounded-right">
                                <span></span>
                                Deze naam bestaat al in een andere categorie.
                            </div>
                        </div>
                        <div class="tooltip-test-wrap">
                            <div class="tooltip-test rounded">
                                Typ hier de naam van het kenmerk, zoals deze publiek getoond wordt</div>
                            <div class="tooltip-test-tip"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="td-label"><label>Gevoelig gegeven</label></td>
                    <td>
                      <input type="checkbox"
                             name="kenmerk_properties.sensitive_field"
                             [%- IF bib_entry.kenmerk_properties.sensitive_field -%]
                             checked="checked"
                             [%- END -%]
                             id="kenmerk_sensitive_field"
                      />
                    <div class="tooltip-test-wrap">
                      <div class="tooltip-test rounded">
                          Geef hier aan of het kenmerk gevoelige
                          gegevens gaat bevatten.
                      </div>
                      <divclass="tooltip-test-tip"></div>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td class="td-label"><label>Invoertype</label></td>
                    <td>
                    [% IF bib_id %]
                        [% FOR invoertype IN constants.veld_opties %]
                            [% NEXT UNLESS bib_entry.kenmerk_type == invoertype.key %]
                            [% invoertype.value.label %]

                            [% class_definition = [];
                                IF invoertype.value.multiple;
                                    class_definition.push("has_options");
                                END;
                                IF invoertype.key == 'file' || invoertype.key == 'date' || invoertype.key == 'appointment';
                                    class_definition.push(invoertype.key);
                                END;
                            %]

                            <input type="hidden" name="kenmerk_type" value="[% bib_entry.kenmerk_type %]"
                                [% IF class_definition.size %]
                                class="[% class_definition.join(' ') %]"
                                [% END %]
                                id="kenmerk_invoertype"
                            /> [% IF bib_entry.kenmerk_type_multiple %](meervoudig)[% END %]
                        [% END %]
                        [% ELSE %]
                        <select name="kenmerk_type" id="kenmerk_invoertype" class="select350">
                            <option value="">Selecteer</option>
                            [% FOR invoertype IN constants.veld_opties %]
                                [% NEXT IF invoertype.key == 'appointment' && !appointment_interface %]
                                [% class_definition = [];
                                    IF invoertype.value.multiple;
                                        class_definition.push("has_options");
                                    END;
                                    IF invoertype.value.allow_multiple_instances;
                                        class_definition.push("allow_multiple_instances");
                                    END;
                                    IF invoertype.value.allow_default_value;
                                        class_definition.push("allow_default_value");
                                    END;
                                    IF invoertype.key == 'file' || invoertype.key == 'date' || invoertype.key == 'appointment';
                                        class_definition.push(invoertype.key);
                                    END;
                                %]
                                <option value="[% invoertype.key %]" [% (bib_entry.kenmerk_type == invoertype.key) ? ' selected="selected"' : '' %]
                                [% IF class_definition.size %]
                                    class="[% class_definition.join(' ') %]"
                                [% END %]
                                >[% invoertype.value.label %]</option>

                            [% END %]
                            </select>
                    [% END %]
                    <div class="edit_kenmerk_multiple">
                [% IF bib_id %]
                        <input type="hidden" name="kenmerk_type_multiple" value="[% bib_entry.kenmerk_type_multiple %]" id="kenmerk_invoertype_multiple"/>
                [% ELSE %]
                            <input type="checkbox" name="kenmerk_type_multiple" [% IF bib_entry.kenmerk_type_multiple %]checked[% END %]
                                id="kenmerk_invoertype_multiple"/> Meerdere waarden toegestaan
                    </div>
                [% END %]
                    </td>
                    <td>
                        <div class="validator rounded">
                            <div class="validate-tip"></div>
                            <div class="validate-content rounded-right">
                                <span></span>
                                Invoertype is verplicht
                            </div>
                        </div>
                        <div class="tooltip-test-wrap">
                            <div class="tooltip-test rounded">
                                Selecteer hier een invoertype, maar let op:
                                deze kan niet meer gewijzigd worden.</div>
                            <div class="tooltip-test-tip"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="td-label"><label>Categorie (verplaatsen)</label></td>
                    <td>
                        <select name="bibliotheek_categorie_id" class="select350">
                            [%
                                INCLUDE block_categorie
                                CAT_STR_DEPTH = 1
                                bib_cat = bib_cat.scalar.search()
                            %]
                        </select>
                    </td>
                    <td>
                        <div class="validator rounded">
                            <div class="validate-tip"></div>
                            <div class="validate-content rounded-right">
                                <span></span>
                                Categorie is verplicht
                            </div>
                        </div>
                        <div class="tooltip-test-wrap">
                            <div class="tooltip-test rounded">
                                Selecteer hier een categorie</div>
                            <div class="tooltip-test-tip"></div>
                        </div>
                    </td>
                </tr>
                <tr class="multiple-options" data-ng-controller="nl.mintlab.admin.EditKenmerkController">
                    <td class="td-label"><label>Mogelijkheden</label></td>
                    <td>
                         <script type="text/zs-scope-data">
                        {
                            "options": [% bib_entry.kenmerk_options ? JSON.encode(bib_entry.kenmerk_options) : '[]' %]
                        }
                        </script>
                        <div class="kenmerk-option-table">
                            <!-- <thead class="kenmerk-option-table-header">
                                <tr>
                                    <td>
                                        Actief
                                    </td>
                                    <td>
                                        Mogelijkheid
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </thead> -->
                            <ul class="kenmerk-option-table-body" data-zs-sort>
                                <li class="kenmerk-option" data-ng-repeat="option in options | filter:isOptionVisible:option" data-ng-draggable data-ng-drag-mimetype="zs/kenmerk-option" data-zs-sortable="option">
                                    <div class="kenmerk-option-value">
                                        <input class="kenmerk-option-active-input" type="checkbox" data-ng-model="option.active" name="kenmerk_active" />
                                         <[option.value]>
                                    </div>
                                    <div class="kenmerk-option-actions">
                                        <button type="button" data-ng-click="deleteOption(option)" data-ng-show="!option.id">
                                            <i class="icon icon-font-awesome icon-remove"></i>
                                        </button>
                                    </div>
                                </li>
                            </ul>
                            <button class="kenmerk-option-visibility-button show-more" type="button" data-ng-click="toggleInactiveOptionsVisibility()" data-ng-show="hasInactiveOptions()">
                                <span data-ng-show="showInactiveOptions">
                                    <i class="icon icon-show-more icon-show-less"></i> Verberg inactieve mogelijkheden
                                </span>
                                <span data-ng-show="!showInactiveOptions">
                                    <i class="icon icon-show-more"></i> Toon inactieve mogelijkheden
                                </span>
                            </button>
                        </div>
                        <div data-ng-form name="addopt" class="kenmerk-option-add">
                            <textarea name="newOption" data-ng-model="newOption" data-zs-placeholder="'Voeg een mogelijkheid toe'" data-zs-autogrow="1">
                            </textarea>
                            <button type="button" data-ng-click="addOption(newOption)" data-ng-disabled="!newOption" class="button button-small button-secondary">
                                <i class="icon icon-font-awesome icon-plus"></i>
                            </button>
                        </div>
                        <textarea cols="60" rows="5" name="kenmerk_options" class="infinite" style="display: none"><[getJson()]></textarea>
                    </td>
                    <td>
                        <div class="validator rounded">
                            <div class="validate-tip"></div>
                            <div class="validate-content rounded-right">
                                <span></span>
                                Minimaal 1 mogelijkheid
                            </div>
                        </div>
                        <div class="tooltip-test-wrap">
                            <div class="tooltip-test rounded">
                                Voer een nieuwe mogelijkheid en klik op de "plus"-knop</div>
                            <div class="tooltip-test-tip"></div>
                        </div>
                    </td>
                </tr>
                [% IF versions.count %]
                <tr>
                    <td class="td-label"><label>Versiehistorie</label></td>
                    <td colspan="2">[% PROCESS version_history %]</td>
                </tr>
                [% END %]
                <tr>
                    <td class="td-label"><label>Toelichting</label></td>
                    <td>
                        <textarea cols="40" rows="10" name="kenmerk_help" class="infinite">[% bib_entry.kenmerk_help %]</textarea>
                    </td>
                    <td>
                        <div class="validator rounded">
                            <div class="validate-tip"></div>
                            <div class="validate-content rounded-right">
                                <span></span>
                                Toelichting
                            </div>
                        </div>
                        <div class="tooltip-test-wrap">
                            <div class="tooltip-test rounded">
                                Vul hier een tekstuele omschrijving van dit
                                kenmerk
                            </div>
                            <div class="tooltip-test-tip"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="td-label"><label>Voorinvulling</label></td>
                    <td>
                        <textarea cols="40" rows="10" name="kenmerk_value_default" class="infinite">[% bib_entry.kenmerk_value_default %]</textarea>
                    </td>
                    <td>
                        <div class="validator rounded">
                            <div class="validate-tip"></div>
                            <div class="validate-content rounded-right">
                                <span></span>
                                Toelichting
                            </div>
                        </div>
                        <div class="tooltip-test-wrap">
                            <div class="tooltip-test rounded">
                                Vul hier een tekstuele voorinvulling in voor dit
                                kenmerk
                            </div>
                            <div class="tooltip-test-tip"></div>
                        </div>
                    </td>
                </tr>
                <tr class="ezra_is_for_document">
                    <td class="td-label"><label>Documentcategorie</label></td>
                    <td>
                        <select name="metadata_document_category" class="select350">
                            [% FOR d = document_categories %]
                                <option value="[% d %]" [% IF bib_entry.file_metadata_id.document_category == d %]SELECTED[%END%]>[% d %]</option>
                            [% END %]
                        </select>
                    </td>
                </tr>
                <tr class="ezra_is_for_document">
                    <td class="td-label"><label>Vertrouwelijkheid</label></td>
                    <td>
                        <select name="metadata_trust_level"  class="select350">
                        [% SET trust_levels = ['Openbaar','Beperkt openbaar','Intern','Zaakvertrouwelijk','Vertrouwelijk','Confidentieel','Geheim','Zeer geheim'] %]

                        [% FOR t = trust_levels %]
                            <option value="[%t%]" [% IF t == bib_entry.file_metadata_id.trust_level %]selected[% END %]>[%t%]</option>
                        [% END %]
                        </select>
                    </td>
                </tr>
               <tr class="ezra_is_for_document">
                    <td class="td-label"><label>Richting</label></td>
                    <td>
                        <select name="metadata_origin"  class="select350">
                        <option value='' SELECTED></option>
                        [% SET origins = ['Inkomend', 'Uitgaand', 'Intern'] %]
                        [% FOR o = origins %]
                            <option value="[%o%]" [% IF o == bib_entry.file_metadata_id.origin %]selected[% END %]>[%o%]</option>
                        [% END %]
                        </select>
                    </td>
                </tr>
                [% IF appointment_interface %]
                <tbody class="appointment-settings"
                       data-ng-controller="nl.mintlab.admin.EditAppointmentKenmerkController"
                       data-ng-init="init('[% appointment_interface.uuid %]')">
                    <script type="text/zs-scope-data">
                        { "properties": [% bib_entry.kenmerk_properties ? JSON.encode(bib_entry.kenmerk_properties) : '{}' %] }
                    </script>
                    <tr>
                        <td class="td-label"><label>Locatie</label></td>
                        <td>
                            <select name="kenmerk_properties.location_id"
                                    class="select350"
                                    data-ng-model="current_location"
                                    data-ng-options="location as location.label for location in locations track by location.id">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="td-label"><label>Product</label></td>
                        <td>
                            <input type="hidden" name="kenmerk_properties.appointment_interface_uuid" value="[% appointment_interface.uuid %]">
                            <select name="kenmerk_properties.product_id"
                                    class="select350"
                                    data-ng-show="current_location"
                                    data-ng-model="current_product"
                                    data-ng-options="product as product.label for product in products track by product.id">
                            </select>
                        </td>
                    </tr>
                </tbody>
                [% END %]
                <tr class="ezra_is_not_for_document">
                    <td class="td-label"><label>Magic String</label></td>
                    <td>
                        <input
                            [% (bib_id ? 'disabled="disabled"' : '') %]
                            type="text"
                            name="kenmerk_magic_string"
                            value="[% bib_entry.kenmerk_magic_string %]"
                            class="infinite ezra_kenmerk_magic_string"
                        >
                        [% IF bib_id %]
                            <input
                                type="hidden"
                                name="kenmerk_magic_string"
                                value="[% bib_entry.kenmerk_magic_string %]"
                                class="infinite ezra_kenmerk_magic_string"
                            >
                        [% END %]
                    </td>
                    <td>
                        <div class="validator rounded">
                            <div class="validate-tip"></div>
                            <div class="validate-content rounded-right">
                                <span></span>
                                magic string
                            </div>
                        </div>
                        <div class="tooltip-test-wrap">
                            <div class="tooltip-test rounded">
                                Vul hier een unieke naam in van dit kenmerk
                            </div>
                            <div class="tooltip-test-tip"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="td-label">Opmerking</td>
                    <td>
                        <input type="text" name="commit_message" value="[% bib_entry ? 'Wijziging ' : 'Nieuw aangemaakt ' %]" class="infinite ezra_sjabloon_naam">
                    </td>
                    <td>
                        <div class="validator rounded">
                            <div class="validate-tip"></div>
                            <div class="validate-content rounded-right">
                                <span></span>
                                Opmerking is verplicht ivm. NEN-2082 norm.
                            </div>
                        </div>
                        <div class="tooltip-test-wrap">
                            <div class="tooltip-test rounded">
                            Korte omschrijving van de reden van aanmaak/wijziging
                            </div>
                            <div class="tooltip-test-tip"></div>
                        </div>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
    <div class="form-actions">
    <input type="submit" value="Opslaan" class="button button-primary">
    </div>
    </form>
</div>
