[% USE JSON %]

<div class="block-fullscreen widget-search-wrap ng-cloak">
	<div class="block-header clearfix">
		<div class="block-header-title">Zoeken</div>
	</div>
	<div
		data-ng-controller="nl.mintlab.widget.search.SearchWidgetController"
		data-ng-init="objectType='[% object_type | html_entity %]';ownerId='[% c.user.id %]';public=[% public ? 'true': 'false' %];interfaceId='[% interface_id | html_entity %]';init()"
	>
		
		<div class="widget-search">
			<div class="widget-body" data-ng-class=" { 'widget-search-filter-locked': filtersLocked, 'widget-search-filter-unlocked': !filtersLocked, 'widget-search-loading': isLoading() }" >
				<div class="widget-search-filter-component" data-ng-show="!isPublic()">
					
					<div class="widget-search-filter-component-header">
						<button type="button" class="widget-search-filter-collapse-button btn-floating btn-floating-small" data-ng-click="handleLockClick()" data-zs-title="<[filtersLocked?'Klap filtermenu in':'Klap filtermenu uit']>">
							<i class="mdi" data-ng-class="{'mdi-chevron-left': filtersLocked, 'mdi-chevron-right': !filtersLocked }"></i>
						</button>
					</div>
				
					<div class="widget-search-filter-body">
						
						<div class="widget-search-filter-save forced-scrollbar" data-ng-controller="nl.mintlab.widget.search.SearchWidgetSaveController" data-zs-before-unload="handleUnload($event)" data-ng-init="searchId='[% search_id | html_entity %]';">
							<div class="forced-scrollbar">
								<div class="widget-search-filter-save-header">
									<span>Opgeslagen zoekopdrachten</span>
									<button class="btn btn-secondary btn-small btn-round widget-search-filter-save-add-button" data-ng-click="search=createSearch();" data-zs-popup="'/html/widget/search/settings.html'" data-zs-title="Nieuwe zoekopdracht">
										<i class="mdi mdi-plus"></i>
									</button>
								</div>
								<div class="widget-search-filter-save-body form-field" data-ng-show="!collapsed">
									<ul class="widget-search-filter-save-list">
										<li class="widget-search-filter-save-list-item" data-ng-repeat="filter in getFilters() | limitTo: getLimit()" data-ng-class="{'widget-search-filter-save-list-item-active': filter===activeSearch, 'widget-search-filter-save-list-item-loading': isFilterLoading(filter), 'widget-search-filter-save-list-item-expanded': expanded, 'widget-search-filter-save-list-item-edit': editMode }" data-ng-controller="nl.mintlab.widget.search.FilterController">
											<button class="widget-search-save-list-item-label" data-zs-confirm="handleFilterClick(filter, $event)" data-zs-confirm data-zs-confirm-enabled="isChanged()" data-zs-confirm-label="U heeft onopgeslagen wijzigingen. Deze gaan verloren als u van zoekopdracht verandert." data-zs-confirm-verb="Verander zoekopdracht" data-ng-disabled="filter===activeSearch">
												<span><[filter.values.label]></span>
											</button>
											<div class="widget-search-save-list-item-form-name">
												<input type="text" data-ng-model="$parent.newName" data-ng-required="true" data-ng-keyup="onKeyUp($event)" data-ng-if="editMode" data-ng-blur="saveName()" data-zs-init-focus/>
											</div>
											<div class="filter-menu" data-zs-popup-menu data-ng-show="isEditable()||isSharable()" data-zs-popup-menu-positioning="smart">
												<button type="button" class="filter-menu-open btn btn-flat" data-zs-popup-menu-position-reference>
													<i class="mdi mdi-dots-vertical"></i>
												</button>
												<ul class="filter-menu popup-menu zs-popup-menu-list zs-popup-menu-positioned-right">
													<li class="filter-menu-item" data-ng-show="isEditable()">
														<button type="button" class="filter-menu-item-button" data-ng-click="setEditMode()">
															Bewerk titel
														</button>
													</li>
													<li class="filter-menu-item" data-ng-show="isSharable()">
														<button type="button" class="filter-menu-item-button" data-ng-click="openShareSettings()">
															Delen
														</button>
													</li>
													<li class="filter-menu-item" data-ng-show="isRemovable()">
														<button type="button" class="filter-menu-item-button" data-zs-confirm="removeSearch(filter)" data-zs-confirm-verb="Verwijderen" data-zs-confirm-label="Weet u zeker dat u deze zoekopdracht wil verwijderen?">
															Verwijderen
														</button>
													</li>
												</ul>
											</div>
											<div class="filter-settings" data-ng-controller="nl.mintlab.widget.search.FilterSettingsController" data-ng-if="isShareSettingsVisible()">
												<div class="filter-settings-label">Zoekopdracht delen</div>
												<div class="filter-settings-internal">
													<ul class="filter-share-position-list">
														<li class="filter-share-position" data-ng-repeat="position in positions track by position.tracking_id" data-ng-controller="nl.mintlab.widget.search.FilterPositionController">
															<div class="filter-share-position-org filter-share-position-type">
																<div class="filter-share-position-type-label">Afdeling:</div> <[getOrgLabel()]>
															</div>
															<div class="filter-share-position-role filter-share-position-type">
																<div class="filter-share-position-type-label">Rol:</div> <[getRoleLabel()]>
															</div>
															<div class="filter-share-position-type-label">Rechten:</div>
															<div class="filter-share-position-right-list">
																<label class="filter-share-position-capability" data-ng-repeat="capability in getCapabilities()">
																	<input class="filter-share-position-capability-checkbox" type="checkbox" data-ng-checked="hasCapability(capability.capability)" data-ng-click="toggleCapability(capability.capability)"><[capability.label]>
																</label>
															</div>
															<button type="button" class="filter-share-position-remove mdi mdi-close" data-ng-click="removePosition()">
															</button>
														</li>
													</ul>
													<div class="filter-share-permission-add-wrap"  data-ng-controller="nl.mintlab.widget.search.FilterPositionAddController">
														<button type="button" class="filter-share-permission-add-button btn btn-secondary" data-ng-click="showControl()" data-ng-show="!isControlVisible()">Rol toevoegen</button>
														<div class="filter-share-permission-add" data-ng-if="isControlVisible()">
															<div class="filter-share-permission-add-control">
																<div class="filter-share-permission-add-control-label">Configureer een nieuwe rol</div>
																<div class="filter-share-permission-add-control-picker" data-zs-position-picker data-zs-position-picker-change="handlePositionChange($orgUnitId, $roleId)" data-zs-position-picker-org-unit="getOrgUnitId()" data-zs-position-picker-role="getRoleId()">
																</div>
																<button type="button" class="filter-share-permission-add-control-confirm btn btn-flat" data-ng-click="confirm()">
																	Opslaan
																</button>
																<button type="button" class="filter-share-permission-add-control-cancel mdi mdi-close" data-ng-click="hideControl()">
																	
																</button>
															</div>
														</div>
													</div>
												</div>
												<div class="filter-settings-public">
													<label>
														<input type="checkbox" data-ng-checked="filter.values.public" data-ng-click="handlePublicClick()"/>
														Beschikbaar voor externe systemen
													</label>
												</div>
												<div class="filter-settings-confirm">
													<button type="button" class="filter-settings-close btn btn-flat" data-ng-click="closeShareSettings()">
														<i class="mdi mdi-check"></i> Klaar
													</button>
												</div>
											</div>
										</li>
									</ul>
									<button class="btn-show-more" data-ng-click="toggleSearches()" data-ng-if="getFilters().length && getFilters().length > 8">
										<span data-ng-if="getLimit()">
											<i class="mdi mdi-chevron-down"></i>
											Toon alle
										</span>
										<span data-ng-if="!getLimit()">
											<i class="mdi mdi-chevron-up"></i>
											Toon minder
										</span>
									</button>
									
								</div>
								
								<script type="text/zs-scope-data">
									{
										"filters": [
											{
												"id": "mine",
												"values": {
													"label": "Mijn openstaande zaken",
													"objectType": {
														"object_type": "case",
														"label": "Zaak"
													},
													"values": {
														"case.assignee.id": [
															{
																"id": [% c.user.id || '""' %],
																"object_type": "medewerker",
																"naam": [% JSON.encode(c.user.naam) %]
															}
														],
														"case.status": [ "open" ]
													}
												}
											},
											{
												"id": "my-department",
												"values": {
													"label": "Mijn afdeling",
													"objectType": {
														"object_type": "case",
														"label": "Zaak"
													},
													"values": {
														"case.route_ou$case.route_role": [
															{
																"orgUnit": {
																	"org_unit_id": "[% c.user_ou_id | html_entity %]",
																	"role_id": null
																}
															}
														]
													}
												}
											},
											{
												"id": "all",
												"values": {
													"label": "Alle zaken",
													"objectType": {
														"object_type": "case",
														"label": "Zaak"
													}
												}
												
											}
											[% IF search_query %]
											,
											[% JSON.encode(search_query) %]

											[% END %]
										]
									}
								</script>
							</div>
						</div>
						
						<div class="widget-search-filter-form-objecttype form-fieldset form-fieldset-name-objecttype" data-zs-scope data-ng-init="collapsed=false" data-ng-class="{ 'form-fieldset-changed': !!options.objectType, 'widget-search-filter-form-objecttype-collapsed': collapsed }">
							<div class="form-fieldset-meta">
								<div class="form-fieldset-header clearfix" data-ng-click="collapsed=!collapsed">
									<button type="button" class="form-fieldset-collapse mdi"  data-ng-class="{'mdi-chevron-down': collapsed, 'mdi-chevron-up': !collapsed }">
									</button>
									<span class="form-fieldset-title ng-binding">Objecttype</span>
								</div>
							</div>
								
							<div class="form-fieldset-children ng-scope" data-ng-show="!collapsed">
								<div class="form-field form-field-name-objecttype">
									<div class="form-field-input spot-enlighter-wrapper"> 
										<input type="text" name="spotenlighter_objecttype" data-ng-model="options.objectType" data-ng-change="handleObjectTypeChange($object)" data-zs-placeholder="'Kies een objecttype'" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="'objecttypes'" data-zs-spot-enlighter-label="label">
									</div>
								</div>
							</div>
						</div>
						
						
						<div class="widget-search-filter-form" data-zs-form-template-parser="<[filters]>">
						</div>
					</div>
					
				</div>
				
				<div class="widget-search-result" data-ng-controller="nl.mintlab.widget.search.SearchWidgetResultController as searchWidgetResultCtrl" data-ng-class="{'widget-search-result-all': !activeGrouping.by, 'widget-search-result-map-visible': isLocationVisible() }">
					
					<div class="widget-search-result-loader">
						<div class="widget-search-result-loader-spinner-wrapper">
							<div class="widget-search-result-loader-spinner"></div>
						</div>
					</div>

					<div class="widget-search-result-header-wrapper">
					
						<div class="widget-search-result-header" 
							data-ng-controller="nl.mintlab.widget.search.SearchWidgetSaveOptionsController"
							data-ng-class="{'widget-search-result-header-edited': isChanged() }"
							data-ng-init="searchTitle='[% search_title | html_entity %]'"
						>
							<div class="widget-search-result-header-left">
								<button class="widget-search-result-header-label" data-ng-click="!isPublic()&&handleTitleClick()" data-zs-title="<[isPublic()?'':'Klik om de resultaten te verversen']>">
									<span class="widget-search-result-header-label-title"><[searchTitle || activeSearch.values.label]></span>
								</button>
								
								<button data-ng-show="hasResults()&&hasObjectType()" class="widget-search-result-header-label-counter" data-zs-popup="'/html/widget/search/search-stats.html'" data-ng-click="!isPublic()&&openPopup()" data-zs-title="<[isPublic()?'': 'Klik hier voor uitgebreidere statistieken']>">(<[getNumResults()]>)</button>
								
								<ul class="widget-search-result-header-operations" data-ng-class="{'widget-search-result-header-operations-show': isChanged}" ng-show="!isPublic()">
									<li class="widget-search-result-header-has-changed" data-ng-show="isChanged()">gewijzigd</li>
									<li class="widget-search-result-header-save-options">
										<button data-ng-click="saveAsCopy()" data-ng-show="isChanged()&&!isEditable()" class="btn btn-flat btn-round btn-small widget-search-result-header-save-options-button-save-as" data-zs-title="Opslaan als nieuwe zoekopdracht">
											<i class="mdi mdi-content-save-all"></i>
										</button>
										<button data-ng-click="saveChanges()" data-ng-show="isChanged()&&isEditable()" class="btn btn-flat btn-round btn-small" data-zs-title="Opslaan">
											<i class="mdi mdi-content-save"></i>
										</button>
										<!--button data-ng-click="saveSearch()" data-ng-show="isChanged()&&!isEditable()">
											Opslaan als
										</button-->
										<button data-ng-click="revertChanges()" data-ng-show="isChanged()" class="btn btn-flat btn-round btn-small" data-zs-title="Maak wijzigingen ongedaan">
											<i class="mdi mdi-undo"></i>
										</button>
									</li>
								</ul>
							</div>
							<div class="widget-search-result-header-display" data-ng-show="hasObjectType()">
								<div class="widget-search-result-header-select" data-ng-show="groupings.length&&(displayMode==='list'||displayMode==='map')">
			                    	<select data-ng-model="$parent.$parent.activeGrouping" data-ng-options="grouping as grouping.label for grouping in groupings">
			                    	</select>
			                    </div>
								<div role="navigation" class="btn-group" data-ng-show="hasChart()||hasMap()">
									<button class="btn btn-flat btn-round btn-small" data-ng-class="{'btn-active': displayMode == 'list'}" data-ng-click="setDisplayMode('list'); setLocationVisibility(false)" data-zs-title="Lijstweergave">
			                            <i class="mdi mdi-view-list"></i>
			                        </button>
			                        <button class="btn btn-flat btn-round btn-small" data-ng-class="{'btn-active': displayMode == 'chart'}" data-ng-click="setDisplayMode('chart')" data-zs-title="Grafiekweergave">
			                            <i class="mdi mdi-chart-line"></i>
			                        </button>
			                        <button class="btn btn-flat btn-round btn-small" data-ng-class="{'btn-active': displayMode == 'map'}" data-ng-click="setDisplayMode('map'); setLocationVisibility(true)" data-zs-title="Kaartweergave">
			                            <i class="mdi mdi-map"></i>
			                        </button>
			                    </div>
			                    <div class="widget-search-result-header-column-management" data-zs-popup-menu>
			                    	<button class="widget-search-result-header-column-management-button btn btn-flat btn-dropdown" data-zs-title="Kolommenbeheer">
			                    		<span>
			                    			<i class="mdi mdi-table"></i>
			                    			<i class="mdi mdi-menu-down"></i>
			                    		</span>
			                    	</button>
			                    	<div class="crud-column-popup-menu popup-menu" data-zs-popup-menu-list data-ng-if="isPopupMenuOpen()" data-ng-include="'/html/widget/search/column-management.html'" data-ng-controller="nl.mintlab.widget.search.SearchWidgetColumnController" data-ng-class="{'crud-column-popup-menu-loading': column.loading }">
			                    	</div>
			                    </div>
							</div>
							
						</div>

					</div>
					
					<div class="widget-search-result-display" data-ng-switch data-on="(displayMode==='list')||(displayMode==='map')" data-ng-show="hasObjectType()">
						<ul class="widget-search-result-list accordion" data-ng-switch-when="true">
							<li class="widget-search-result-list-item accordion-item" data-ng-repeat="group in groups track by group.value" data-ng-class="{'accordion-item-open': group.value === openGroup }" >
								<button class="widget-search-result-list-item-header accordion-item-header" data-ng-click="onGroupClick(group)" data-ng-if="activeGrouping.by">
									<span class="widget-search-result-list-item-header-label"><[group.label]></span>
									<span class="widget-search-result-list-item-header-count" data-ng-show="group.count">(<[group.count]>)</span>
								</button>
								<div class="accordion-panel" data-ng-if="group.value==openGroup" data-ng-class="{'accordion-panel-single': activeGrouping.by}">
									<div class="widget-search-result-map" data-zs-ezra-map data-zs-ezra-map-search data-ng-if="isLocationVisible()">
										[% PROCESS widgets/general/map.tt
										    MAP_TEMPLATE    = (c.user_exists ? 'case' : 'public')
										    MAP_CSSPROFILE  = 'smallauto'
										    MAP_READONLY    = 1
										    MAP_MULTIPLE    = 1
										    MAP_CENTER      = c.get_customer_info.latitude _ ' ' _ c.get_customer_info.longitude
										%]
									</div>
									<div class="widget-search-result-crud" data-zs-crud-template-parser="<[getCrud(group)]>" data-zs-crud-column-manager="getSearchColumns($query)" data-ng-controller="nl.mintlab.widget.search.SearchWidgetCrudController" data-zs-crud-data-change="handleCrudDataChange($response)">
									</div>
								</div>
							</li>
							<li class="widget-search-result-list-item widget-search-result-list-item-empty no-results" data-ng-if="groups.length===0">
								Geen resultaten
							</li>
						</ul>
						<div class="widget-search-result-chart" 
							data-ng-switch-when="false" 
							data-zs-line-chart 
							data-zs-line-chart-zql="getZql()" 
							data-zs-line-chart-profile="options.chartProfile" 
							data-zs-line-chart-message="message" 
							data-zs-line-chart-loading="loading.linechart"
							data-exceeds-limit="searchWidgetResultCtrl.exceedsLimit()"
						>
							<select data-ng-model="options.chartProfile" data-ng-options="option.label for option in chartOptions">
								
							</select>
							<div class="message no-results" ng-show="message"><[message]></div>
                            <div id="search_query_chart_container"></div>
						</div>
					</div>
				</div>
				
				
				
			</div>
			
		</div>
	
	</div>
	
</div>

<script type="text/ng-template" id="/html/widget/search/form-field-attribute.html">

	<div class="form-field-attribute" data-zs-attribute-form-field data-zs-form-field data-ng-model="scope[field.name]"data-zs-form-template-parser="<[getAttributeForm()]>">
	</div>
	
</script>

<script type="text/ng-template" id="/html/widget/search/attribute-spot-enlighter.html">
	<div class="form-field-spot-enlighter-component" data-ng-model="scope[field.name]" data-zs-spot-enlighter-form-field data-zs-spot-enlighter-form-field-multiple="<[field.data.multi]>" data-zs-spot-enlighter-label="<[field.data.label]>" data-zs-form-field data-zs-spot-enlighter-resolve="_id">
		<ul class="form-field-spot-enlighter-list" data-ng-show="field.data.multi">
			<li class="form-field-spot-enlighter-list-item form-field-spot-enlighter-list-item-multiple-options" data-ng-repeat="object in scope[field.name] track by object._id" data-ng-controller="nl.mintlab.widget.search.AttributeItemController" data-ng-class="{'form-field-spot-enlighter-list-item-expanded': isOpen() } ">
				<div class="form-field-spot-enlighter-list-item-header">
					<div class="form-field-spot-enlighter-list-item-label" data-ng-click="toggleOpen()">
						<[getObjLabel(object)]>
					</div>
					<div class="form-field-spot-enlighter-list-item-options">
						<button class="form-field-spot-enlighter-list-item-remove mdi mdi-close" data-ng-click="remove(object);handleRemoveAttrClick(object);">
						</button>
					</div>
				</div>
				<div class="form-field-spot-enlighter-list-item-form" data-ng-if="isOpen()" data-ng-include="'/html/widget/search/attribute-edit.html'">
				</div>
			</li>
		</ul>
		<div class="spot-enlighter-wrapper">
			<input type="text" data-ng-model="obj" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="getRestrict(field)" data-zs-spot-enlighter-label="<[field.data.label]>" data-zs-placeholder="field.data.placeholder" />
		</div>
	</div>
</script>

<script type="text/ng-template" id="/html/widget/search/attribute-edit.html">
	<div data-zs-form-template-parser="<[getAttributeConfigForm()]>" zs-attribute-edit-form>
	</div>
</script>

<script type="text/ng-template" id="/html/widget/search/column-notifications.html">
	<span class="fase-counter counter counter-in-results" data-ng-show="getVal()>0" data-ng-controller="nl.mintlab.widget.search.CaseNotificationController" data-zs-title="U heeft <[getVal()]> notificatie<[getVal() > 1 ? 's' : '']> op deze zaak">
		<[getVal()]>
	</span>
</script>

<script type="text/ng-template" id="/html/widget/search/column-destruction-blocked.html">
	<span class="destruction-blocked" data-ng-show="item.values['case.destructable']&&item.values['case.destruction_blocked']">
		Geblokkeerd
	</span>
</script>

<script type="text/ng-template" id="/html/widget/search/column-type-calendar.html">
	<div>
		<span ng-show="item.values[column.id]">
			<[item.values[column.id].split(';')[0] | date:'dd-MM-yyyy']>  
			<[item.values[column.id].split(';')[1] | date:'HH:mm']>
		</span>
	</div>
</script>

<script type="text/ng-template" id="/html/widget/search/column-type-calendar-supersaas.html">
	<div>
		<span ng-show="item.values[column.id]">
			<[item.values[column.id].split(';')[0] | date:'dd-MM-yyyy HH:mm']>  
		</span>
	</div>
</script>

<script type="text/ng-template" id="/html/widget/search/column-type-list.html">
	<ul class="search-column-type-list-list" data-zs-search-column-list data-list="item.values[column.id]">
		<li class="search-column-type-list-list-item" data-ng-repeat="listItem in searchColumnList.getItems() track by $index">
			<[listItem]>
		</li>
	</ul>
</script>

<script type="text/ng-template" id="/html/widget/search/column-type-addr-mult.html">
	<ul class="search-column-type-list-list">
		<li class="search-column-type-list-list-item" data-ng-repeat="addr in item.values[column.id]">
			<[addr.human_identifier]>
		</li>
	</ul>
</script>

<script type="text/ng-template" id="/html/widget/search/column-type-file.html">
	<ul class="search-column-type-list-list">
		<li class="search-column-type-list-list-item" data-ng-repeat="file in item.values[column.id]">
			<a data-ng-href="/api/object/<[item.id]>/file/<[file.uuid]>"><[file.filename||file.original_name]></a>
		</li>
	</ul>
</script>

<script type="text/ng-template" id="/html/widget/search/action-menu.html">
	<div class="crud-item-action-menu clearfix" data-zs-popup-menu data-zs-popup-menu-positioning="smart" data-ng-controller="nl.mintlab.core.crud.CrudItemActionMenuController">
		<button class="crud-item-action-menu-open-button btn btn-flat btn-round btn-small btn-secondary" data-zs-popup-menu-position-reference data-ng-click="handleOpenMenuClick($event)">
			<i class="mdi mdi-dots-vertical"></i>
		</button>
		<ul class="crud-item-action-menu-list popup-menu" data-zs-popup-menu-list data-ng-if="isPopupMenuOpen()">
			<li class="crud-action" data-ng-repeat="action in getItemActions() | filter:isVisible:action track by action.id " data-ng-include="'/html/core/crud/action-type-' + action.type + '.html'" data-ng-controller="nl.mintlab.core.crud.CrudActionController">
			</li>
		</ul>
	</div>
</script>

<script type="text/ng-template" id="/html/widget/search/column-management.html">
	<div class="crud-column-popup-menu-query">
		<input class="crud-column-popup-menu-query-input" type="text" data-ng-model="column.query" data-ng-required="false" data-zs-form-field data-zs-placeholder="'Zoek binnen alle kenmerken'" data-ng-change="handleQueryChange()">
		<div class="spot-enlighter-loading">
		</div>
	</div>
	<ul class="crud-column-popup-menu-list">
		<li class="crud-column-popup-menu-list-item" data-ng-repeat="column in columnResults track by column.id" data-ng-class="{ 'crud-column-popup-menu-list-item-active': isSelected(column) }">
			<button class="crud-column-popup-menu-list-item-button" data-ng-click="toggleColumn(column);closePopupMenu()" >
				<span class="crud-column-popup-menu-list-item-button-inner">
					<i class="crud-column-popup-menu-list-item-button-icon mdi mdi-check"></i>
					<span class="crud-column-popup-menu-list-item-button-label"><[column.label]></span>
					<span class="crud-column-popup-menu-list-item-button-description" ng-show="column.description"><[column.description]></span>
				</span> 
			</button>
		</li>
	</ul>
</script>

<script type="text/ng-template" id="/html/widget/search/search-stats.html">
	<div data-zs-modal data-zs-modal-title="Uitgebreide statistieken">
		<div data-ng-controller="nl.mintlab.widget.search.SearchWidgetStatsController as stats">
			<div data-zs-spinner="stats.isLoading()" class="zs-spinner-medium">
			</div>
			<div>
				Totaal aantal resultaten: <[stats.getCount()]>
			</div>
		</div>
	</div>
</script>
