/*global $,Highcharts,document,console,getOptions,ezra_dialog*/
(function () {
    'use strict';

    function loadPanorama() {
        var obj = $('.ezra_woz_object_picture'),
            rel = obj.attr('rel');

        if (!rel && rel.length) {
            return;
        }

        $.getJSON(
            '/plugins/woz/picture',
            getOptions(rel),
            function (data) {
                if (!(data && data.json && data.json.tid)) {
                    return false;
                }

                $('.ezra_woz_object_picture').show();

                $('a.ezra_woz_object_picture_popup').click(function () {
                    ezra_dialog({
                        url         : '/plugins/woz/picture_dialog',
                        title       : 'Panoramafoto',
                        layout      : 'xlarge',
                        cgi_params  : data.json
                    });
                    return false;
                });

            }
        );
    }

    function loadObjectPhoto() {

        var img = $('.woz-photo img')[0];

        if(!img) {
            return;
        }

        function showPhoto ( ) {
            $(img).closest('.woz-photo').removeClass('woz-photo-unavailable');
        }

        if(img.complete || img.naturalWidth) {
            showPhoto();
        } else {
            $(img).on('load', showPhoto);
        }
    }

    $(document).ready(function () {

        loadObjectPhoto();

        if (!$('.ezra_woz_object_picture').length) {
            return;
        }

        loadPanorama();

        $('.ezra_woz_photo_zoom').on('click', function () {
            var url = $('.woz-photo img').attr('src').replace('woz/photo', 'woz/photo_dialog');

            ezra_dialog({
                url         : url,
                title       : 'Details',
                layout      : 'xlarge'
            });
            return false;
        });
    });

}());
