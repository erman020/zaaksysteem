function attachImportFormHandlers() {
    var form = $('form.import');

    $('form.import input[type="radio"]').bind('change', function() {
        form.load(
            document.URL,
            form.serialize(),
            function() { initializeEverything(form); }
        );
    });
}
