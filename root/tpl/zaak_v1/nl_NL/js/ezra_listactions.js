/* ***** BEGIN LICENSE BLOCK ********************************************
 * Version: EUPL 1.1
 *
 * The contents of this file are subject to the EUPL, Version 1.1 or
 * - as soon they will be approved by the European Commission -
 * subsequent versions of the EUPL (the "Licence");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://joinup.ec.europa.eu/software/page/eupl
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Zaaksysteem
 *
 * The Initial Developer of the Original Code is
 * Mintlab B.V. <info@mintlab.nl>
 * 
 * Portions created by the Initial Developer are Copyright (C) 2009-2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * Michiel Ootjers <michiel@mintlab.nl>
 * Jonas Paarlberg <jonas@mintlab.nl>
 * Jan-Willem Buitenhuis <jw@mintlab.nl>
 * Peter Moen <peter@mintlab.nl>
 *
 * ***** END LICENSE BLOCK ******************************************** */


$(document).ready(function(){

    $(document).on('change', '.ezra_results_per_page', function() {
		var form_name = $(this).closest('form').attr('name');
	    updateResults(form_name);
    });

    installListActions($('.ezra_listactions'));

});

function installListActions(form) {

    enableListActions(form);


    $(document).on('click', '.ezra_listactions_display .ezra_listaction_select_all', function(event) {
        var obj = $(this);
        var form = obj.closest('form');


        var all_checked = 1;
        form.find('.ezra_listactions_checkbox').each( function() {
            $(this).attr('disabled', false);
            all_checked = all_checked && $(this).attr('checked');
        });

        if(all_checked) {
            obj.attr('checked', false);
        } else {
            obj.attr('checked', true);
        }

        form.find('.ezra_listactions_checkbox').each( function() {
            if(all_checked) {
                $(this).removeAttr('checked');
            } else {
                $(this).attr('checked', 1);
            }
        });
        enableListActions(form);
        event.stopPropagation();
        return all_checked;
    });

    $(document).on('click', '.ezra_listactions_display .ezra_listaction_select_all_search_results', function() {
        form.find('.ezra_listactions_checkbox').each( function() {
            $(this).attr('disabled', true);
        });
        form.find('.ezra_listaction_select_all').attr('disabled', true);
        form.find('.ezra_listactions_all_search_results').show();
        form.find('.ezra_listactions_all_search_results input[name="action_search_results"]').val(1);
        $(this).closest('.ezra_dropdown').hide();
        enableListActions(form);
        return false;
    });

    $(document).on('click', '.ezra_listactions_clear_all', function() {
        form.find('.ezra_listactions_all_search_results input[name="action_search_results"]').val('');        
        form.find('.ezra_listactions_all_search_results').hide();
        form.find('.ezra_listactions_checkbox').each( function() {
            $(this).attr('disabled', false);
        });
        form.find('.ezra_listaction_select_all').attr('disabled', false);
        enableListActions(form);
        return false;
    });

    $(document).on('change', '.ezra_listactions_checkbox', function() {
        enableListActions(form);
    });
    


    $(document).on('click', '.set_page_number', function(){
		var page_number = $(this).attr('rel');
		var form_name = $(this).closest('form').attr('name');
		$('form[name=' + form_name + '] input[name=page]').val(page_number);
		$('form[name=' + form_name + '] input[name=pager_request]').val(1);
	    updateResults(form_name);
	    
		return false;
	});

	$(document).on('click', ".set_sort_order", function(){
		var form_name = $(this).closest('form').attr('name');
		var old_sort_field = $('form[name=' + form_name + '] input[name=sort_field]').val();
		var old_sort_direction = $('form[name=' + form_name + '] input[name=sort_direction]').val();

	    var sort_field = $(this).attr('data-sort-field');
		var sort_direction = 'DESC'; // default
		
		// if the sort field is the same as was clicked last time, flip the sort direction
		if(sort_field == old_sort_field) {
			sort_direction = old_sort_direction == 'ASC' ? 'DESC': 'ASC';
		}

		$('form[name=' + form_name + '] input[name=sort_field]').val(sort_field);
		$('form[name=' + form_name + '] input[name=sort_direction]').val(sort_direction);
		
		updateResults(form_name);
		return false;
	});


    $(document).on('change', '.ezra_logging_component', function() {
        updateResults('logging_list');
    });

    $(document).on('click', '.ezra_listaction_bulk', function() {
        
        var obj     = $(this);
 
        var form    = obj.closest('form');
        var title   = obj.attr('title');
        var url     = obj.attr('href');

        var selected_case_ids = [];
        form.find('.ezra_listactions_checkbox:checked').each( function() {
            selected_case_ids.push($(this).val());
        });
        var selection = 'selected_cases';
        var search_query_id = '';
        var action_search_results = form.find('input[name="action_search_results"]').val();
        if(action_search_results && action_search_results != '0') {
            selection = 'search_results';
            search_query_id = form.find('input[name="SearchQuery_search_query_id"]').val();
        }

        var layout = url.match(/set_settings/) ? 'xlarge' : 'large';

        ezra_dialog({
            title       : title, 
            url         : url, 
            cgi_params  : {
                selected_case_ids   : selected_case_ids.join(","),
                selection           : selection,
                search_query_id     : search_query_id
            },
            layout      : layout
        }, function() {
            //var $form = $('form[action="/bulk/update/destroy"]');
            //if ($form.length) {
               // zvalidate($form);
            //}
            $(this).blur();
        });

        return false;
    });

	$(document).on('change', '.zaken_filter_form select#zaakfilter', function() {
		var form_name = $(this).closest('form').attr('name');
		
		return updateResults(form_name);
	});
}


//
// Master function to update the result list of 'zaken'.
// The resultset is defined in the Perl controller level. This adjusts a few 
// additional variables influencing output and a text-matching filter
//
// TODO: Because multiple views can be used on the same page, always refresh only the current one
// through a AJAX call. Only the HTML for the current view should be updated.
//
function updateResults(form_name) {

	if(!form_name || !$('form[name=' + form_name +']').length) {
		form_name = zaken_filter_form_name;
	}

	var form_selector = 'form[name=' + form_name + ']';
    $(form_selector).find('.spinner-groot').css('visibility', 'visible');


	var data = 'nowrapper=1&' + $(form_selector).serialize();
	var current_path = $(location).attr('pathname');

    var grouping_choice_selector = '';
    
    var grouping_choice_object = $(form_selector + ' input[name=grouping_choice]');
    if(grouping_choice_object && grouping_choice_object.val()) {
        grouping_choice_selector = " #" + grouping_choice_object.val();
    }
    $(form_selector + grouping_choice_selector + ' .zaken_filter_wrapper').load(current_path + ' ' + form_selector + ' .zaken_filter_inner', data,		
		function (responseText, textStatus, XMLHttpRequest) {
 			if(textStatus == 'success') {
				veldoptie_handling();
				$(".progress-value").each(function() {
					$(this).width( $(this).find('.perc').html() + "%");
				});
				var total_entries = $(form_selector + ' input[name=total_entries]').val();
				if(total_entries) {
					$(form_selector + ' span.total_entries').html(total_entries);
				} else {
					$(form_selector + ' span.total_entries').html('0');
				}
    			initializeEverything($(this));
			} else {
//				$(form_selector + ' .zaken_filter_inner').html('Er is iets misgegaan, laad de pagina opnieuw');
			}

            $.ztWaitStop();
		}
	);

	return false;
}



function enableListActions(form) {
    var enabled_items = 0;

    form.find('.ezra_listactions_checkbox').each( function() {
        if($(this).attr('checked')) {
            enabled_items++;
        }
    });
    
    var action_search_results = form.find('input[name="action_search_results"]').val();
    enabled_items += action_search_results;

    var obj = $('.ezra_listactions_display .ezra_management_actions');
    if(enabled_items > 0) {
        obj.removeClass('disabled');
    } else {
        obj.addClass('disabled');
    }
}

