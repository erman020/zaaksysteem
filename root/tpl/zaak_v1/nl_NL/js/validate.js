/*global _,$,document,window,angular*/
/* ***** BEGIN LICENSE BLOCK ********************************************
 * Version: EUPL 1.1
 *
 * The contents of this file are subject to the EUPL, Version 1.1 or
 * - as soon they will be approved by the European Commission -
 * subsequent versions of the EUPL (the "Licence");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://joinup.ec.europa.eu/software/page/eupl
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Zaaksysteem
 *
 * The Initial Developer of the Original Code is
 * Mintlab B.V. <info@mintlab.nl>
 *
 * Portions created by the Initial Developer are Copyright (C) 2009-2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * Michiel Ootjers <michiel@mintlab.nl>
 * Jonas Paarlberg <jonas@mintlab.nl>
 * Jan-Willem Buitenhuis <jw@mintlab.nl>
 * Peter Moen <peter@mintlab.nl>
 *
 * ***** END LICENSE BLOCK ******************************************** */

var invoked_validate_function = 0;

$(document).ready(function() {
    register_validation();

    $('form').each(function() {
        if (!(
            $(this).hasClass('zvalidate') ||
            $(this).hasClass('ezra_no_waitstart')
        )) {
            $(this).submit(function() {
               $.ztWaitStart();
            });
        }
    });
});

function register_validation(scope_obj) {
    $('form.zvalidate',scope_obj).each(function() {
        if ($(this).hasClass('zvalidate-initialized')) {
            return true;
        }

        $(this).submit(function() {
            return zvalidate($(this));
        });

        $(this).addClass('zvalidate-initialized');
    });

}

function request_validation(formelem, extraopt, options) {
    var action = formelem.attr('action');

    /* Loop prevention */
    if (formelem.hasClass('validated')) {
        return true;
    }

    if (formelem.hasClass('invalidation')) {
        return false;
    }

    /* Validation in progress */
    formelem.addClass('invalidation');

    var serialized = validate_serialize_items(formelem, extraopt);
    //$.ztWaitStart();

    $.ajax({
        dataType: "json",
        type: "POST",
        data: 'do_validation=1&' + serialized,
        url: action,
        success: function(rawdata) {
            var data = rawdata.json;

            if (data.success) {
                //$.ztWaitStop();

                formelem.addClass('validated');
                formelem.removeClass('invalidation');

                if (options && options['events']['submit']) {
                    options['events']['submit'](formelem);
                } else {
                    formelem.submit();
                }

                return true;
            }

            validate_form_items({
                container: formelem,
                validation_result: data
            });

           // $.ztWaitStop();
            formelem.removeClass('invalidation');

            return false;
        }
    });

    return false;
}


function zvalidate(container, options) {
    var extraopt,
        callback,
        error_callback;

    if (options) {
        extraopt        = options.extraopt;
        callback        = options.callback;
        error_callback  = options.error_callback;
    }

    var action = container.attr('action');

    // Loop prevention
    if (container.hasClass('validated')) {
        if (container.hasClass('ezra_spiffy_spinner')) {
            ezra_spiffy_spinner_submit(container);
        }

        return true;
    }

    if (container.hasClass('invalidation')) {
        return false;
    }

    container.addClass('invalidation');

    // Remove invalids
    container
        .find('span.invalid')
        .removeClass('invalid');
    container
        .find('span.valid')
        .removeClass('valid');

    var serialized = validate_serialize_items(container, extraopt);
    var url = action + '?' + serialized;

    $.ajax({
        dataType: 'json',
        type: 'POST',
        data: 'do_validation=1&' + serialized,
        url: action,
        success: function (rawdata) {
            var data = rawdata.json;

            invoked_validate_function = 1;

            if (data.success || (extraopt && extraopt.match(/&allow_cheat=1/))) {
                if (container.hasClass('ezra_spiffy_spinner')) {
                    $.ztWaitStop();
                }

                if (extraopt) {
                    extraopt = extraopt.replace(/&allow_cheat=1/, '');

                    if (container.attr('action').match(/\?/)) {
                        container.attr('action',
                            container.attr('action') + '&' + extraopt
                        );
                    } else {
                        container.attr('action',
                            container.attr('action') + '?' + extraopt
                        );
                    }
                }

                // form with a NEN2082 confirm message need another step. because this is an
                // asynchronous process we need to interfere here. used by ezra_kennisbank.js
                if (callback) {
                    container.removeClass('invalidation');
                    callback(data);
                } else {
                    $(document).trigger('ng:webform:presubmit');
                    container
                        .unbind('submit')
                        .submit();
                }

                return true;
            }

            var dont_focus_first_error = options != undefined && options.hasOwnProperty('dont_focus_first_error');

            validate_form_items({
                container: container,
                validation_result: data,
                dont_focus_first_error: dont_focus_first_error
            });

            if (error_callback) {
                error_callback(container, data);
            }

            $.ztWaitStop();
            container.removeClass('invalidation');

            return false;
        }
    })
        .fail(function (jqxhr, textStatus, error) {
            if (jqxhr.status == 401) {
                document.location.href = url;
            }
        });

    return false;
}


function validate_serialize_items(container, extraopt) {
    var values = container.serializeArray(),
        vals = [],
        serialized;

    // check file data (needed for IE8)
    container
        .find('input[type="file"]')
        .each(function () {
            var name = $(this)
                    .parents('.kenmerk-veld')
                    .find('[name="defined_kenmerk"]')
                    .val(),
                val = $(this).val(),
                ctrl = angular
                    .element($(this)[0])
                    .inheritedData('$zsCaseWebformFieldController');

            if (ctrl.isVisible()) {
                values.push({
                    name: name,
                    value: val
                });
            } else {
                values = _.reject(values, {
                    name: 'defined_kenmerk',
                    value: name
                });
            }
        });

    _.each(values, function (obj) {
        var name = obj.name,
            el,
            add = true,
            id,
            fieldEl,
            ctrl;

        if (name.indexOf('kenmerk_id_') === 0 || name === 'defined_kenmerk') {
            id = name === 'defined_kenmerk'
                ? obj.value
                : name;
            el = container.find('[name=' + id + ']');
            fieldEl = el.parents('.kenmerk-veld');
            ctrl = fieldEl.length
                ? angular
                    .element(fieldEl[0])
                    .inheritedData('$zsCaseWebformFieldController')
                : null;

            if (ctrl) {
                add = ctrl.isVisible();
            }
        }

        if (add) {
            vals.push(obj);
        }
    });

    serialized = $.param(vals);

    if (extraopt) {
        if (serialized) {
            serialized += '&';
        }

        serialized += extraopt;
    }

    return serialized;
}


function validate_form_items(options) {
    var container = options.container;
    var data = options.validation_result;

    container
        .find('input[type="submit"]')
        .attr('disabled',null);
    container
        .find('span.validator,div.validator')
        .hide();

    var val_errors  = {};

    for (var i in data.missing) {
        var constraint_key = data.missing[i];
        var containingtd = container
            .find('[name="' + constraint_key + '"], .' + constraint_key)
            .closest('td,.column');
        var validator = containingtd
            .parents('tr,.row')
            .find('.validator');

        val_errors[constraint_key] = 1;

        if (validator.length) {
            var label = containingtd
                .parents('tr,.row')
                .find('label');
            var msg = 'Let op, dit veld moet worden ingevuld.';

            // Nasty unpacking of <label>text<i></i></label> DOM
            if (label) {
                var node = label.get(0);

                if (node && node.childNodes[0] && node.childNodes[0].nodeType == 3) {
                    msg = 'Het veld "' + $.trim(node.childNodes[0].nodeValue) + '" is niet ingevuld.';
                }
            }

            validator
                .show()
                .addClass('invalid')
                .find('.validate-content')
                .html('<img src="/images/icon-error.svg" alt="Fout" />' + msg);
        } else {
            containingtd
                .find('span')
                .addClass('invalid')
                .html(data.msgs[constraint_key]);
        }

        containingtd
            .find('span.validator')
            .show();
    }

    for (var i in data.invalid) {
        var constraint_key = data.invalid[i];
        var containingtd = container
            .find('[name="' + constraint_key + '"], .' + constraint_key)
            .closest('td,.column');
        var validator = containingtd
            .parents('tr,.row')
            .find('.validator');

        val_errors[constraint_key] = 1;

        if (validator.length) {
            validator
                .show()
                .addClass('invalid')
                .find('.validate-content')
                .html('<img src="/images/icon-error.svg" alt="Fout" />' + data.msgs[constraint_key]);
        } else {
            containingtd
                .find('span')
                .addClass('invalid')
                .html(data.msgs[constraint_key]);
        }

        containingtd
            .find('span.validator')
            .show();
    }

    for (var i in data.valid) {
        var constraint_key = data.valid[i];

        if (val_errors[constraint_key]) {
            continue;
        }

        var containingtd = container
            .find('[name="' + constraint_key + '"], .' + constraint_key)
            .closest('td,.column');

        if (!containingtd.parents('tr,.row').hasClass('ignore-field-' + constraint_key)) {
            if (containingtd.parents('tr,.row').find('.validator').length) {
                containingtd
                    .parents('tr,.row')
                    .find('.validator')
                    .hide()
                    .addClass('valid')
                    .find('.validate-content')
                    .html('<span></span>');
            } else {
                containingtd
                    .find('span')
                    .addClass('valid');
            }

            containingtd
                .find('span.validator')
                .show();
        }
    }

    if (data.msgs.selection_extra) {
        var my_div = container.find('.ezra_selection_extra');

        my_div.html(data.msgs.selection_extra);
        my_div
            .closest('tr,.row')
            .show();
    }

    if(!options.dont_focus_first_error) {
        $('.validate-content:visible')
            .first()
            .closest('.row')
            .find('input, textarea, select')
            .focus();
    }
}



