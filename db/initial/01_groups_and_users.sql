BEGIN;

SET CLIENT_ENCODING TO 'UTF8';

INSERT INTO interface (name, active, max_retries, interface_config, multiple, module) VALUES ('Authenticatie', true, 10, '{"enable_user_login":"1"}', true, 'authldap');

INSERT INTO groups (id, path, name, description, date_created, date_modified) VALUES
    (1, '{1}',   'Development', 'Development root',    NOW(), NOW()),
    (2, '{1,2}', 'Backoffice',  'Default backoffice',  NOW(), NOW()),
    (3, '{1,3}', 'Frontoffice', 'Default frontoffice', NOW(), NOW());

INSERT INTO roles (parent_group_id, name, description, system_role, date_created, date_modified) VALUES
    ((SELECT id FROM groups WHERE name = 'Development'), 'Administrator',             'Systeemgroep: Administrator', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Zaaksysteembeheerder',      'Systeemgroep: Zaaksysteembeheerder', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Zaaktypebeheerder',         'Systeemgroep: Zaaktypebeheerder', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Zaakbeheerder',             'Systeemgroep: Zaakbeheerder', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Contactbeheerder',          'Systeemgroep: Contactbeheerder', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Basisregistratiebeheerder', 'Systeemgroep: Basisregistratiebeheerder', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Wethouder',                 'Systeemgroep: Wethouder', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Directielid',               'Systeemgroep: Directielid', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Afdelingshoofd',            'Systeemgroep: Afdelingshoofd', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Kcc-medewerker',            'Systeemgroep: Kcc-medewerker', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Zaakverdeler',              'Systeemgroep: Zaakverdeler', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Behandelaar',               'Systeemgroep: Behandelaar', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Gebruikersbeheerder',       'Systeemgroep: Gebruikersbeheerder', true, NOW(), NOW());

INSERT INTO subject(subject_type, properties, settings, username, role_ids, group_ids, nobody, system) VALUES
    (
        'employee',
        '{"givenname":"Ad","initials":"A.","cn":"admin","telephonenumber":"0612345678","sn":"Mïn","displayname":"Ad Mïn","mail":"devnull@zaaksysteem.nl"}',
        '{}',
        'admin',
        ARRAY(SELECT id FROM roles WHERE name IN('Administrator', 'Behandelaar')),
        ARRAY(SELECT id FROM groups WHERE name IN('Backoffice')),
        false,
        true
    ),
    (
        'employee',
        '{"initials":"beheerder","givenname":"beheerder","telephonenumber":"0612345678","cn":"beheerder","sn":"beheerder","mail":"devnull@zaaksysteem.nl","displayname":"beheerder"}',
        '{}',
        'beheerder',
        ARRAY(SELECT id FROM roles WHERE name IN('Administrator', 'Behandelaar')),
        ARRAY(SELECT id FROM groups WHERE name IN('Backoffice')),
        false,
        true
    ),
    (
        'employee',
        '{"givenname":"gebruiker","initials":"gebruiker","cn":"gebruiker","telephonenumber":"0612345678","sn":"gebruiker","displayname":"gebruiker","mail":"devnull@zaaksysteem.nl"}',
        '{}',
        'gebruiker',
        ARRAY(SELECT id FROM roles WHERE name IN('Behandelaar')),
        ARRAY(SELECT id FROM groups WHERE name IN('Frontoffice')),
        false,
        true
    );

INSERT INTO user_entity(source_interface_id, source_identifier, subject_id, date_created, properties, password) VALUES
    (
        (SELECT id FROM interface WHERE module = 'authldap'),
        'admin',
        (SELECT id FROM subject WHERE username = 'admin'),
        NOW(),
        '{}',
        '{SSHA}UePJXd0aWZtRJG9y2f0YEHkQkBo4xDZV' -- "admin"
    ),
    (
        (SELECT id FROM interface WHERE module = 'authldap'),
        'beheerder',
        (SELECT id FROM subject WHERE username = 'beheerder'),
        NOW(),
        '{}',
        '{SSHA}4RU5KnridRYjqJGOgC6T645PL87W81j4' -- "beheerder"
    ),
    (
        (SELECT id FROM interface WHERE module = 'authldap'),
        'gebruiker',
        (SELECT id FROM subject WHERE username = 'gebruiker'),
        NOW(),
        '{}',
        '{SSHA}ypTduQij55Q/K02zcRBZ3i/O3ThIxFzz' -- "gebruiker"
    );

COMMIT;
