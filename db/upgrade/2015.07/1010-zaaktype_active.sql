BEGIN;

    ALTER TABLE zaaktype ADD _active BOOLEAN DEFAULT true NOT NULL;
    UPDATE zaaktype SET _active = true where active = 1;
    UPDATE zaaktype SET _active = false where active = 0 or active is null;
    ALTER TABLE zaaktype DROP COLUMN active;
    ALTER TABLE zaaktype RENAME _active TO active;

COMMIT;
