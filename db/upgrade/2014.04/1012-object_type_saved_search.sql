BEGIN;

ALTER TABLE object_data DROP CONSTRAINT IF EXISTS object_data_object_class_check;
ALTER TABLE object_data ADD CONSTRAINT object_data_object_class_check CHECK(
    object_class ~ '^(case|saved_search)$'
);

COMMIT;
