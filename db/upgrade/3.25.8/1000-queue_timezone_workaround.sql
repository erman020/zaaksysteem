BEGIN;

ALTER TABLE queue ALTER COLUMN date_created SET DEFAULT statement_timestamp() at time zone 'UTC';

COMMIT;
