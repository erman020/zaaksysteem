BEGIN;

INSERT INTO config (parameter, value) VALUES(
    'case_distributor_group',
    (select id from groups where array_length(path, 1) = 1 limit 1)
);
INSERT INTO config (parameter, value) VALUES(
    'case_distributor_role',
    (select id from roles where name = 'Zaakverdeler' and system_role=true )
);

COMMIT;
