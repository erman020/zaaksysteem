BEGIN;

ALTER TABLE zaaktype_regel ADD COLUMN active BOOLEAN DEFAULT true;
UPDATE zaaktype_regel SET active=true;

COMMIT;