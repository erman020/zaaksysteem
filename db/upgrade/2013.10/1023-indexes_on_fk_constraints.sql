-- zaak_kenmerk
CREATE INDEX CONCURRENTLY idx_zaak_kenmerk_bibliotheek_kenmerken_id ON zaak_kenmerk(bibliotheek_kenmerken_id);

-- zaaktype_kenmerken
CREATE INDEX CONCURRENTLY idx_zaaktype_kenmerken_bibliotheek_kenmerken_id ON zaaktype_kenmerken(bibliotheek_kenmerken_id);
CREATE INDEX CONCURRENTLY idx_zaaktype_kenmerken_zaaktype_node_id ON zaaktype_kenmerken(zaaktype_node_id);
CREATE INDEX CONCURRENTLY idx_zaaktype_kenmerken_zaak_status_id ON zaaktype_kenmerken(zaak_status_id);

-- case_action
CREATE INDEX CONCURRENTLY idx_case_action_case_id ON case_action(case_id);
CREATE INDEX CONCURRENTLY idx_case_action_casetype_status_id ON case_action(casetype_status_id);

-- checklist
CREATE INDEX CONCURRENTLY idx_checklist_case_id ON checklist(case_id);

-- checklist_item
CREATE INDEX CONCURRENTLY idx_checklist_item_checklist_id ON checklist_item(checklist_id);

-- zaaktype_regel
CREATE INDEX CONCURRENTLY idx_zaaktype_regel_zaaktype_node_id ON zaaktype_regel(zaaktype_node_id);
CREATE INDEX CONCURRENTLY idx_zaaktype_regel_zaak_status_id ON zaaktype_regel(zaak_status_id);

-- zaaktype_sjablonen
CREATE INDEX CONCURRENTLY idx_zaaktype_sjablonen_zaaktype_node_id ON zaaktype_sjablonen(zaaktype_node_id);
CREATE INDEX CONCURRENTLY idx_zaaktype_sjablonen_zaak_status_id ON zaaktype_sjablonen(zaak_status_id);

-- zaaktype_notificatie
CREATE INDEX CONCURRENTLY idx_zaaktype_notificatie_zaaktype_node_id ON zaaktype_notificatie(zaaktype_node_id);
CREATE INDEX CONCURRENTLY idx_zaaktype_notificatie_zaak_status_id ON zaaktype_notificatie(zaak_status_id);

-- zaaktype_authorisation
CREATE INDEX CONCURRENTLY idx_zaaktype_authorisation_zaaktype_node_id ON zaaktype_authorisation(zaaktype_node_id);
CREATE INDEX CONCURRENTLY idx_zaaktype_authorisation_zaaktype_id ON zaaktype_authorisation(zaaktype_id);

-- bibliotheek_sjablonen_magic_string
CREATE INDEX CONCURRENTLY idx_bibliotheek_sjablonen_magic_string_bibliotheek_sjablonen_id ON bibliotheek_sjablonen_magic_string(bibliotheek_sjablonen_id);

-- zaak_subcase
CREATE INDEX CONCURRENTLY idx_zaak_subcase_zaak_id ON zaak_subcase(zaak_id);
CREATE INDEX CONCURRENTLY idx_zaak_subcase_relation_zaak_id ON zaak_subcase(relation_zaak_id);

-- zaaktype
CREATE INDEX CONCURRENTLY idx_zaaktype_bibliotheek_categorie_id ON zaaktype(bibliotheek_categorie_id);

-- bibliotheek_sjablonen
CREATE INDEX CONCURRENTLY idx_bibliotheek_sjablonen_bibliotheek_categorie_id ON bibliotheek_sjablonen(bibliotheek_categorie_id);

-- search_query_delen
CREATE INDEX CONCURRENTLY idx_search_query_delen_search_query_id ON search_query_delen(search_query_id);

-- bibliotheek_categorie
CREATE INDEX CONCURRENTLY idx_bibliotheek_categorie_pid ON bibliotheek_categorie(pid);
