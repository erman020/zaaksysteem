BEGIN;

CREATE TABLE object_subscription (
    id SERIAL PRIMARY KEY,
    interface_id INTEGER REFERENCES interface(id) NOT NULL,
    external_id VARCHAR(255) NOT NULL,
    local_table VARCHAR(100) NOT NULL,
    local_id VARCHAR(255) NOT NULL,
    date_created TIMESTAMP WITHOUT TIME ZONE default now(),
    date_deleted TIMESTAMP WITHOUT TIME ZONE,
    object_preview TEXT
);

COMMIT;