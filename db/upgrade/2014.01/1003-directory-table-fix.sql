BEGIN;

alter table directory alter column name type varchar(255);
alter table directory alter name set not null;

ALTER TABLE directory ADD CONSTRAINT name_case_id UNIQUE (name, case_id);

alter table directory add original_name text;
update directory set original_name = name;
alter table directory alter original_name set not null;

COMMIT;
