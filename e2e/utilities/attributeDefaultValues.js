export default [
    {
        type: 'bag_adres',
        value: ['straat']
    },
    {
        type: 'bag_openbareruimte',
        value: ['straat']
    },
    {
        type: 'bag_straat_adres',
        value: ['straat']
    },
    {
        type: 'bag_adressenmultiple',
        value: ['straat']
    },
    {
        type: 'bag_openbareruimtesmultiple',
        value: ['straat']
    },
    {
        type: 'bag_straat_adressenmultiple',
        value: ['straat']
    },
    {
        type: 'geolatlon',
        value: 'straat'
    },
    {
        type: 'googlemaps',
        value: 'straat'
    },
    {
        type: 'bankaccount',
        value: 'GB82WEST12345698765432'
    },
    {
        type: 'date',
        value: '13-01-2010'
    },
    {
        type: 'email',
        value: 'test@test.nl'
    },
    {
        type: 'image_from_url',
        value: 'https://wiki.zaaksysteem.nl/skins/common/images/bussum.png'
    },
    {
        type: 'text',
        value: 'this is random text'
    },
    {
        type: 'text_uc',
        value: 'this is random text'
    },
    {
        type: 'numeric',
        value: '1234'
    },
    {
        type: 'url',
        value: 'https://www.google.com'
    },
    {
        type: 'valuta',
        value: '12.34'
    },
    {
        type: 'valutaex',
        value: '12.34'
    },
    {
        type: 'valutaex21',
        value: '12.34'
    },
    {
        type: 'valutaex6',
        value: '12.34'
    },
    {
        type: 'valutain',
        value: '12.34'
    },
    {
        type: 'valutain21',
        value: '12.34'
    },
    {
        type: 'valutain6',
        value: '12.34'
    },
    {
        type: 'numericmultiple',
        value: ['1', '2', '3', '4']
    },
    {
        type: 'textmultiple',
        value: ['this', 'is', 'random', 'text']
    },
    {
        type: 'text_ucmultiple',
        value: ['this', 'is', 'random', 'text']
    },
    {
        type: 'option',
        value: 1
    },
    {
        type: 'richtext',
        value: 'this is random text\nand another line'
    },
    {
        type: 'select',
        value: 1
    },
    {
        type: 'selectmultiple',
        value: [1, 2, 3, 4]
    },
    {
        type: 'textarea',
        value: 'this is random text\nand another line'
    },
    {
        type: 'textareamultiple',
        value: ['this is random text\nand another line', 'and another text\nand another line']
    },
    {
        type: 'filemultiple',
        value: 'text.txt'
    },
    {
        type: 'checkbox',
        value: [1]
    }
];

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
