let secondsDone = 0;
let timeStart;
let failedExpectation;
let specCount = 0;
let executableSpecCount = 0;
let testResults = [];
let failureCount = 0;
const ansi = {
    green: '\x1B[32m',
    red: '\x1B[31m',
    yellow: '\x1B[33m',
    none: '\x1B[0m'
};
const colored = (color, str) => ansi[color] + str + ansi.none;
const leftPad = (x, str) => str.toString().padStart(x);
const writeResultMessage = result => {
    const devider = ' - ';
    const { number, status, fullName, description, failedExpectations } = result;
    const nameWithoutDescription = fullName.replace(description, '');
    const indentation = number.toString().length + devider.length;
    let filePath;
    let failedMessages;

    if ( failedExpectations.length > 0 ) {
        const stackTrace = result.failedExpectations[0].stack;

        filePath = /.*?(\/e2e\/tests\/)(.*?)(\.js)/.exec(stackTrace)[2];
        failedMessages = result.failedExpectations.map(expectation => `${leftPad(indentation, devider)}${expectation.message}`).join('\n');
    }

    const messages = {
        passed: {
            color: 'green',
            message: number % 10 ? '.' : '|'
        },
        pending: {
            color: 'yellow',
            message: '*'
        },
        failed: {
            color: 'red',
            message: `\n${number}${devider}${filePath}\n${leftPad(indentation, devider)}${nameWithoutDescription}\n${leftPad(indentation, '')}${description}\n${failedMessages}\n`
        }
    };
    const color = messages[status].color;
    const message = messages[status].message;

    process.stdout.write(colored(color, message));
};
const myReporter = {
        jasmineStarted: suiteInfo => {
            process.stdout.write(`\nStarted on ${suiteInfo.totalSpecsDefined} specs\n`);
            timeStart = new Date();
        },
        jasmineDone: result => {
            const timeEnd = new Date();
            const runtime = timeEnd - timeStart;
            const minutes = Math.floor(runtime / 60 / 1000);
            const seconds = Math.floor((runtime - minutes * 60 * 1000) / 1000);
            const failedSpecs = testResults.filter(testResult => testResult.status === 'failed');
            const pendingSpecs = testResults.filter(testResult => testResult.status === 'pending');
            const failure = leftPad(4, failedSpecs.length);
            const pending = leftPad(4, pendingSpecs.length);
            const success = leftPad(4, specCount - failure - pending);

            if (failedSpecs.length > 0) {
                process.stdout.write('\n\nFailures:');
            }

            failedSpecs.forEach(failedSpec => {
                writeResultMessage(failedSpec);
            });

            process.stdout.write(`\n\nRan ${executableSpecCount} of ${specCount} specs in ${minutes} minutes and ${seconds} seconds.`);
            process.stdout.write(`\n- Success: ${success}`);
            process.stdout.write(`\n- Failure: ${failure}`);
            process.stdout.write(`\n- Pending: ${pending}\n\n`);

            if (result && result.failedExpectations) {
                result.failedExpectations.forEach(failedExpectation => {
                    process.stdout.write(colored('red', `\nAn error was thrown in an afterAll:\n${failedExpectation.message}\n\n`));
                });
            }
        },
        specDone: result => {
            const timeEnd = new Date();
            const seconds = (timeEnd - timeStart) / 1000;

            if (seconds > secondsDone + 300) {
                secondsDone = seconds;
                process.stdout.write(` (${specCount} specs done at ${Math.floor(seconds / 60)} minutes)\n`);
            }

            if ( result.status !== 'disabled' ) {
                specCount++;
                executableSpecCount++;
                result.number = specCount;
                testResults.push(result);
                writeResultMessage(result);
            }
        }
    };

module.exports = myReporter;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
