import {
    openPageAs
} from './../../../functions/common/navigate';
import startForm from './../../../functions/common/startForm';
import {
    goNext
} from './../../../functions/common/form';
import {
    inputAttribute,
    getValue,
    getClosedValue,
    getValutaDisplay
} from './../../../functions/common/input/caseAttribute';
import {
    openPhase
} from './../../../functions/intern/caseView/caseNav';

describe('when starting a form with all attributes', () => {
    const regForm = $('form');
    const caseAttrList = $('.vorm-field-list');

    beforeAll(() => {
        openPageAs();

        const data = {
            casetype: 'Alle kenmerken registratie',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);
    });

    const attributesInfo = [
        {
            attr: 'adres_dmv_postcode',
            inputValue: '6367DB 7',
            expectedValue: 'Voerendaal - Florinstraat 7'
        },
        {
            attr: 'adressen_dmv_postcode',
            inputValue: ['6367db 7', '6343PM 3'],
            expectedValue: 'Voerendaal - Florinstraat 7\nKlimmen - Groeneweg 3'
        },
        {
            attr: 'adres_dmv_straatnaam',
            inputValue: 'Florinstraat 7',
            expectedValue: 'Voerendaal - Florinstraat 7'
        },
        {
            attr: 'adressen_dmv_straatnaam',
            inputValue: ['Florinstraat 7', 'Groeneweg 3'],
            expectedValue: 'Voerendaal - Florinstraat 7\nKlimmen - Groeneweg 3'
        },
        {
            attr: 'straat',
            inputValue: 'Florinstraat',
            expectedValue: 'Florinstraat, Voerendaal'
        },
        {
            attr: 'straten',
            inputValue: ['Florinstraat', 'Groeneweg'],
            expectedValue: 'Florinstraat, Voerendaal\nGroeneweg, Klimmen'
        },
        {
            attr: 'rekeningnummer',
            inputValue: 'NL39RABO0300065264',
            expectedValue: 'NL39RABO0300065264'
        },
        {
            attr: 'meervoudige_keuze',
            inputValue: [2, 4],
            expectedValue: [false, true, false, true, false],
            expectedReadValue: 'Optie 2\nOptie 4'
        },
        {
            attr: 'datum',
            inputValue: '25-11-2013',
            expectedValue: '2013-11-25',
            expectedReadValue: '25-11-2013'
        },
        {
            attr: 'email',
            inputValue: 'random@email.com',
            expectedValue: 'random@email.com'
        },
        {
            attr: 'document',
            inputValue: undefined,
            expectedValue: ['text.txt'],
            expectedReadValue: 'text.txt'
        },
        {
            attr: 'geocoordinaten',
            inputValue: 'Florinstraat 7',
            expectedValue: 'Locatie geselecteerd',
            expectedReadValue: ''
        },
        {
            attr: 'adres_google_maps',
            inputValue: 'Florinstraat 7',
            expectedValue: 'Florinstraat 7, 6367 DB Voerendaal, Nederland'
        },
        {
            attr: 'afbeelding',
            inputValue: 'https://wiki.zaaksysteem.nl/Bestand:Geactiveerd_contact.png',
            expectedValue: 'https://wiki.zaaksysteem.nl/Bestand:Geactiveerd_contact.png'
        },
        {
            attr: 'numeriek',
            inputValue: '2468',
            expectedValue: '2468'
        },
        {
            attr: 'numeriek_plus',
            inputValue: ['2', '4', '6', '8'],
            expectedValue: ['2', '4', '6', '8'],
            expectedReadValue: '2\n4\n6\n8'
        },
        {
            attr: 'enkelvoudige_keuze',
            inputValue: 3,
            expectedValue: 'Optie 3'
        },
        {
            attr: 'rich_text',
            inputValue: 'this is my test text\nand another line',
            expectedValue: 'this is my test text\nand another line'
        },
        {
            attr: 'keuzelijst',
            inputValue: 3,
            expectedValue: 'string:Optie 3',
            expectedReadValue: 'Optie 3'
        },
        {
            attr: 'keuzelijst_plus',
            inputValue: [3, 5, 1],
            expectedValue: ['string:Optie 3', 'string:Optie 5', 'string:Optie 1'],
            expectedReadValue: 'Optie 3\nOptie 5\nOptie 1'
        },
        {
            attr: 'tekstveld',
            inputValue: 'this is my test text',
            expectedValue: 'this is my test text'
        },
        {
            attr: 'tekstveld_plus',
            inputValue: ['this', 'is', 'test', 'text'],
            expectedValue: ['this', 'is', 'test', 'text'],
            expectedReadValue: 'this\nis\ntest\ntext'
        },
        {
            attr: 'tekstveld_hoofdletters',
            inputValue: 'this is my test text',
            expectedValue: 'THIS IS MY TEST TEXT'
        },
        {
            attr: 'tekstveld_hoofdletters_plus',
            inputValue: ['this', 'is', 'test', 'text'],
            expectedValue: ['THIS', 'IS', 'TEST', 'TEXT'],
            expectedReadValue: 'THIS\nIS\nTEST\nTEXT'
        },
        {
            attr: 'groot_tekstveld',
            inputValue: 'this is my test text\nand another line',
            expectedValue: 'this is my test text\nand another line'
        },
        {
            attr: 'groot_tekstveld_plus',
            inputValue: ['this is my test text\nand another line', 'and another test text\nand another line'],
            expectedValue: ['this is my test text\nand another line', 'and another test text\nand another line'],
            expectedReadValue: 'this is my test text\nand another line\nand another test text\nand another line'
        },
        {
            attr: 'webadres',
            inputValue: 'https://www.wikipedia.org',
            expectedValue: 'https://www.wikipedia.org'
        },
        {
            attr: 'valuta',
            inputValue: '12,34',
            expectedValue: '12,34',
            expectedReadValue: '€ 12,34'
        },
        {
            attr: 'valuta_exc_19',
            inputValue: '23,45',
            expectedValue: '23,45',
            expectedReadValue: '€ 23,45\nincl. btw: € 27,91',
            expectedBtwValue: 'incl. btw: € 27,91'
        },
        {
            attr: 'valuta_exc_21',
            inputValue: '34,56',
            expectedValue: '34,56',
            expectedReadValue: '€ 34,56\nincl. btw: € 41,82',
            expectedBtwValue: 'incl. btw: € 41,82'
        },
        {
            attr: 'valuta_exc_6',
            inputValue: '45,67',
            expectedValue: '45,67',
            expectedReadValue: '€ 45,67\nincl. btw: € 48,41',
            expectedBtwValue: 'incl. btw: € 48,41'
        },
        {
            attr: 'valuta_inc_19',
            inputValue: '56,78',
            expectedValue: '56,78',
            expectedReadValue: '€ 56,78\nexcl. btw: € 47,71',
            expectedBtwValue: 'excl. btw: € 47,71'
        },
        {
            attr: 'valuta_inc_21',
            inputValue: '67,89',
            expectedValue: '67,89',
            expectedReadValue: '€ 67,89\nexcl. btw: € 56,11',
            expectedBtwValue: 'excl. btw: € 56,11'
        },
        {
            attr: 'valuta_inc_6',
            inputValue: '78,90',
            expectedValue: '78,90',
            expectedReadValue: '€ 78,90\nexcl. btw: € 74,43',
            expectedBtwValue: 'excl. btw: € 74,43'
        }
    ];

    attributesInfo.forEach(attributeInfo => {
        const { attr, inputValue, expectedValue, expectedBtwValue } = attributeInfo;

        it(`and when filling out an attribute of type ${attr} it should have the given value`, () => {
            inputAttribute(regForm.$(`[data-name="${attr}"]`), inputValue);

            expect(getValue(regForm.$(`[data-name="${attr}"]`))).toEqual(expectedValue);

            if (expectedBtwValue) {
                expect(getValutaDisplay(regForm.$(`[data-name="${attr}"]`))).toEqual(expectedBtwValue);
            }
        });
    });

    describe('and when navigating to the control step', () => {
        beforeAll(() => {
            goNext();
        });

        attributesInfo.forEach(attributeInfo => {
            const { attr, expectedValue, expectedReadValue } = attributeInfo;

            it(`the attribute of type ${attr} should have the right values`, () => {
                const expected = expectedReadValue !== undefined ? expectedReadValue : expectedValue;

                expect(getClosedValue(regForm.$(`[data-name="${attr}"]`))).toEqual(expected);
            });
        });

        describe('and when registering the case', () => {
            beforeAll(() => {
                goNext();
                openPhase(1);
            });

            attributesInfo.forEach(attributeInfo => {
                const { attr, expectedValue } = attributeInfo;

                it(`the attribute of type ${attr} should have the correct values`, () => {
                    expect(getValue(caseAttrList.$(`[data-name="${attr}"]`))).toEqual(expectedValue);
                });
            });
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
