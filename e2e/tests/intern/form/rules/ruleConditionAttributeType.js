import {
    openPageAs
} from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    inputAttribute,
    getClosedValue
} from './../../../../functions/common/input/caseAttribute';

const attributeTypes = ['enkelvoudige_keuze', 'meervoudige_keuze', 'keuzelijst'];
const options = ['Woord', 'Woord met extra tekst', 'Nul', 'Een', 'Diakriët'];
// const addresses = [
//     {
//         type: 'lower than',
//         input: '6343AB',
//         output: 'Lower than 6343AC'
//     },
//     {
//         type: 'exact',
//         input: '6343AC',
//         output: 'Exact 6343AC'
//     },
//     {
//         type: 'higher than',
//         input: '6343AD',
//         output: 'Higher than 6343AC'
//     }
// ];

describe('when opening a registration form with attribute type testscenarios', () => {
    beforeAll(() => {
        const data = {
            casetype: 'Voorwaarden kenmerktypen',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        openPageAs();
        startForm(data);
    });

    attributeTypes.forEach(attributeType => {
        const attributeToInput = $(`[data-name="voorwaarde_${attributeType}"]`);
        const attribureWithOutput = $(`[data-name="voorwaarde_${attributeType}_resultaat"]`);

        options.forEach((option, index) => {
            describe(`and inputting the ${attributeType} attribute with ${option}`, () => {
                beforeAll(() => {
                    inputAttribute(attributeToInput, attributeType === 'meervoudige_keuze' ? [index + 1] : index + 1);
                });
            
                it(`the recipient attribute value should equal ${option}`, () => {
                    expect(getClosedValue(attribureWithOutput)).toEqual(option);
                });
            });
        });
    });

    // addresses.forEach(address => {
    //     const { type, input, output } = address;

    //     describe(`and setting the address to ${type} 6343AC`, () => {
    //         beforeAll(() => {
    //             inputAttribute($('[data-name="voorwaarde_adres_postcode"]'), input);
    //         });

    //         it(`the result of the rule should be ${output}`, () => {
    //             expect(getClosedValue($('[data-name="voorwaarde_adres_postcode_resultaat"]'))).toEqual(output);
    //         });

            // afterAll(() => {
            //     $('[data-name="voorwaarde_adres_postcode"] .mdi-close').click();
            // });
    //     });
    // });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
