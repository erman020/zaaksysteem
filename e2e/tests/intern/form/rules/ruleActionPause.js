import {
    openPageAs
} from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';

const choice = $('[data-name="pauze_keuze"]');
const placeholder1 = $('[data-name="placeholder_1"]');
const pauseMessage = $('zs-case-pause-application');

describe('when opening a registration form with pause testscenarios', () => {

    beforeAll(() => {

        openPageAs();

        const data = {
            casetype: 'Pauzeren',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

    });

    describe('and activating the pause', () => {
    
        beforeAll(() => {
    
            choice.$('[value="Pauze normaal"]').click();
    
        });

        it('the pause message should be present', () => {
    
            expect(pauseMessage.isPresent()).toBe(true);
    
        });
    
        it('the attribute following the pausing attribute should be present', () => {
    
            expect(placeholder1.isPresent()).toBe(false);
    
        });
    
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
