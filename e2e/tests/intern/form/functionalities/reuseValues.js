import {
    openPageAs
} from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    reuseValues
} from './../../../../functions/common/form';
import {
    getValue,
    inputAttribute
} from './../../../../functions/common/input/caseAttribute';

const attributesInfo = {
    tekstveld: 'Tweede zaak',
    tekstveld_plus: [ 'Tweede', 'zaak' ],
    groot_tekstveld: 'Tweede\nzaak',
    enkelvoudige_keuze: 'Optie 2',
    meervoudige_keuze: [ false, true, true, false, true ],
    datum: '2017-02-02',
    adres_dmv_postcode: 'Voerendaal - Florinstraat 2'
};

describe('when starting a registration form with the add reuse values functionality and reusing', () => {
    beforeAll(() => {
        const data = {
            casetype: 'Gegevens hergebruiken',
            requestorType: 'citizen',
            requestorId: '18',
            channelOfContact: 'behandelaar'
        };

        openPageAs();
        startForm(data);
        inputAttribute($('[data-name="tekstveld"]'));
        reuseValues();
    });

    Object.keys(attributesInfo)
        .forEach(key => {
            const value = attributesInfo[key];

            it(`the forms attribute "${key}" should contain the value "${value}" from the previous case 162 and not from deleted case 163`, () => {
                expect(getValue($(`[data-name="${key}"]`))).toEqual(value);
            });
        });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
