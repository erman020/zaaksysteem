import {
    openPageAs
} from './../../../functions/common/navigate';
import {
    createWidget
} from './../../../functions/intern/plusMenu';

describe('when viewing the dashboard', () => {
    beforeAll(() => {
        openPageAs('dashboardempty');
    });

    describe('and creating a case intake widget', () => {
        beforeAll(() => {
            createWidget('Zoekopdracht', 'Zaakintake');
        });

        it('the dashboard should contain a functional case intake', () => {
            expect($('[data-name="intake"] [href="/intern/zaak/44"]').isPresent()).toBe(true);
        });

        afterAll(() => {
            $('.widget-header-remove-button').click();
        });
    });

    describe('and creating a favorite casetype widget', () => {
        beforeAll(() => {
            createWidget('Favoriete zaaktypen');
        });

        it('the dashboard should contain a functional favorite casetype widget', () => {
            expect($('[data-name=""] .widget-favorite-link').getText()).toEqual('Dashboard');
        });

        afterAll(() => {
            $('.widget-header-remove-button').click();
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
