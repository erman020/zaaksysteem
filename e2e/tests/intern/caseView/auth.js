import { openPageAs } from './../../../functions/common/navigate';
import { openAllActions, actionButtons } from './../../../functions/intern/actionMenu';
import { callSpecificCase, checkCaseAttributes } from './../../../functions/common/auth/xhrRequests';

const confirmSearchRights = (response, authLevel) => {
    const expected = authLevel === 'search' ? 200 : 404;

    expect(response.status_code).toEqual(expected);
};

const checkButtons = (authLevel, expected) => {
    openAllActions();

    actionButtons.count().then(count => {
        expect(count).toEqual(expected.length);
        
        if(count === expected.length) {
            actionButtons
                .each((button, index) =>
                    button
                        .getText()
                        .then((text) => {
                            expect(text).toEqual(expected[index]);
                        })
                    );
        }
    });
};

const checkAttributeState = (authLevel, expected) => {
    $$('.phase-form .vorm-field-item')
        .each(attribute => {
            const attributeState = attribute
                .$('.value-list')
                .getAttribute('class')
                .then(classes => {
                    return classes.split(' ').includes('disabled') ? 'disabled' : 'enabled';
                });

            expect(attributeState).toEqual(expected);
        });
};

const checkResult = (authLevel, expected) => {
    $$('zs-case-result input')
        .each(resultInput => {
            const attributeState = resultInput
                .getAttribute('disabled')
                .then(state => {
                    return state === 'disabled' ? 'disabled' : 'enabled';
                });

            expect(attributeState).toEqual(expected);
        });
};

const caseActionsAccess = ['Toewijzing wijzigen'];
const caseActionsHandle = [
    ...caseActionsAccess,
    ...[
        'Opschorten',
        'Vroegtijdig afhandelen',
        'Termijn wijzigen',
        'Zaak relateren',
        'Zaak kopiëren'
    ]
];
const caseActionsManage = [
    ...caseActionsHandle,
    ...[
        'Beheeracties inklappen',
        'Aanvrager wijzigen',
        'Coordinator wijzigen',
        'Afdeling en rol wijzigen',
        'Registratiedatum wijzigen',
        'Streefafhandeldatum wijzigen',
        'Vernietigingsdatum wijzigen',
        'Kenmerken wijzigen',
        'Fase wijzigen',
        'Status wijzigen',
        'Rechten wijzigen',
        'Zaaktype wijzigen',
        'Betaalstatus wijzigen'
    ]
];

const authorityChecks = [
    {
        authChecker: checkButtons,
        expected: {
            access: caseActionsAccess,
            handle: caseActionsHandle,
            manage: caseActionsManage
        }
    },
    {
        authChecker: checkAttributeState,
        expected: {
            access: 'disabled',
            handle: 'enabled',
            manage: 'enabled'
        }
    },
    {
        authChecker: checkResult,
        expected: {
            access: 'disabled',
            handle: 'enabled',
            manage: 'enabled'
        }
    }
];

const scenarios = [
    {
        description: 'confirm casetype public search authorisation',
        user: 'caseauthfirst',
        caseNumber: 186,
        authLevel: 'search'
    },
    {
        description: 'confirm casetype confidential manage authorisation',
        user: 'caseauthfirst',
        caseNumber: 187,
        authLevel: 'manage'
    },
    {
        description: 'confirm casetype public access authorisation',
        user: 'caseauthsecond',
        caseNumber: 186,
        authLevel: 'access'
    },
    {
        description: 'confirm casetype confidential handle authorisation',
        user: 'caseauthsecond',
        caseNumber: 187,
        authLevel: 'handle'
    },
    {
        description: 'confirm casetype public handle authorisation',
        user: 'caseauththird',
        caseNumber: 186,
        authLevel: 'handle'
    },
    {
        description: 'confirm casetype confidential access authorisation',
        user: 'caseauththird',
        caseNumber: 187,
        authLevel: 'access'
    },
    {
        description: 'confirm casetype public manage authorisation',
        user: 'caseauthfourth',
        caseNumber: 186,
        authLevel: 'manage'
    },
    {
        description: 'confirm casetype confidential search authorisation',
        user: 'caseauthfourth',
        caseNumber: 187,
        authLevel: 'search'
    },
    {
        description: 'confirm zaaksysteembeheerder gets manage rights',
        user: 'caseauthbeheerder',
        caseNumber: 186,
        authLevel: 'manage'
    },
    {
        description: 'confirm normal users gets access rights from being the requestor (no handler / no coordinator)',
        user: 'caseauth',
        caseNumber: 188,
        authLevel: 'access'
    },
    {
        description: 'confirm normal users gets access rights from being the requestor (with handler / with coordinator)',
        user: 'caseauth',
        caseNumber: 189,
        authLevel: 'access'
    },
    {
        description: 'confirm normal users gets access rights from being the coordinator (no handler)',
        user: 'caseauth',
        caseNumber: 190,
        authLevel: 'access'
    },
    {
        description: 'confirm normal users gets access rights from being the handler (no coordinator)',
        user: 'caseauth',
        caseNumber: 191,
        authLevel: 'handle'
    },
    {
        description: 'confirm normal users gets handle rights from being the coordinator (with handler)',
        user: 'caseauth',
        caseNumber: 192,
        authLevel: 'handle'
    },
    {
        description: 'confirm normal users gets handle rights from being the handler (with coordinator)',
        user: 'caseauth',
        caseNumber: 193,
        authLevel: 'handle'
    },
    {
        description: 'confirm normal user gets no access when he has no caseRole',
        user: 'caseauth',
        caseNumber: 194,
        authLevel: 'none'
    },
    {
        description: 'confirm normal user gets access through case specific rights',
        user: 'caseauth',
        caseNumber: 195,
        authLevel: 'access'
    },
    {
        description: 'confirm user from upper department gets no access (public)',
        user: 'caseauthtop',
        caseNumber: 196,
        authLevel: 'none'
    },
    {
        description: 'confirm user from upper department gets no access (confidential)',
        user: 'caseauthtop',
        caseNumber: 197,
        authLevel: 'none'
    },
    {
        description: 'confirm user from lower department gets access rights (public)',
        user: 'caseauthbottom',
        caseNumber: 196,
        authLevel: 'handle'
    },
    {
        description: 'confirm user from lower department gets access rights (confidential)',
        user: 'caseauthbottom',
        caseNumber: 197,
        authLevel: 'handle'
    },
    {
        description: 'confirm that a user with handle rights gets access rights (no handler / no coordinator)',
        user: 'caseauth',
        caseNumber: 198,
        authLevel: 'access'
    },
    {
        description: 'confirm that a user with handle rights gets handle access rights (with handler / no coordinator)',
        user: 'caseauth',
        caseNumber: 199,
        authLevel: 'handle'
    },
    {
        description: 'confirm that a user with handle rights gets access rights (no handler / with coordinator)',
        user: 'caseauth',
        caseNumber: 200,
        authLevel: 'access'
    },
    {
        description: 'confirm that a user with handle rights gets handle access rights (with handler / with coordinator)',
        user: 'caseauth',
        caseNumber: 201,
        authLevel: 'handle'
    }
];

scenarios.forEach(scenario => {
    const { description, user, caseNumber, authLevel } = scenario;
    const accessibility = authLevel !== 'search' && authLevel !== 'none'; 

    describe(`when opening case ${caseNumber} as ${user}`, () => {
        beforeAll(() => {
            openPageAs(user, caseNumber);
        });

        it(`the case should ${accessibility ? '' : 'not '} be accessible`, () => {
            expect($('.case-view').isPresent()).toBe(accessibility);
        });

        if ( accessibility ) {
            authorityChecks.forEach(authorityCheck => {
                const { authChecker, expected } = authorityCheck;

                it(`it should ${description}`, () => {
                    authChecker(authLevel, expected[authLevel]);
                });
            });
        } else {
            it(`the case information should ${authLevel === 'search' ? '' : 'not '} be accessible through API call`, () => {
                callSpecificCase(caseNumber).then(response => {
                    confirmSearchRights(response, authLevel);
                });
            });
        }
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
