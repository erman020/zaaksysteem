import {
    openPage,
    openPageAs
} from './../../../../../functions/common/navigate';
import {
    inputAttribute,
    getValue,
    getValutaDisplay
} from './../../../../../functions/common/input/caseAttribute';
import waitForSave from './../../../../../functions/intern/caseView/waitForSave';

const form = $('.phase-form');
const testData = [
    {
        attr: 'adres_dmv_postcode',
        inputValue: '6367DB 7',
        expectedValue: 'Voerendaal - Florinstraat 7'
    },
    {
        attr: 'adressen_dmv_postcode',
        inputValue: ['6367db 7', '6343PM 3'],
        expectedValue: 'Voerendaal - Florinstraat 7\nKlimmen - Groeneweg 3'
    },
    {
        attr: 'straat',
        inputValue: 'Florinstraat',
        expectedValue: 'Florinstraat, Voerendaal'
    },
    {
        attr: 'straten',
        inputValue: ['Florinstraat', 'Groeneweg'],
        expectedValue: 'Florinstraat, Voerendaal\nGroeneweg, Klimmen'
    },
    {
        attr: 'adres_dmv_straatnaam',
        inputValue: 'Florinstraat 7',
        expectedValue: 'Voerendaal - Florinstraat 7'
    },
    {
        attr: 'adressen_dmv_straatnaam',
        inputValue: ['Florinstraat 7', 'Groeneweg 3'],
        expectedValue: 'Voerendaal - Florinstraat 7\nKlimmen - Groeneweg 3'
    },
    {
        attr: 'rekeningnummer',
        inputValue: 'NL39RABO0300065264',
        expectedValue: 'NL39RABO0300065264'
    },
    {
        attr: 'meervoudige_keuze',
        inputValue: [2, 4],
        expectedValue: [false, true, false, true, false]
    },
    {
        attr: 'datum',
        inputValue: '25-11-2013',
        expectedValue: '2013-11-25'
    },
    {
        attr: 'email',
        inputValue: 'random@email.com',
        expectedValue: 'random@email.com'
    },
    {
        attr: 'document',
        inputValue: undefined,
        expectedValue: ['text.txt']
    },
    {
        attr: 'geocoordinaten',
        inputValue: 'Florinstraat 7',
        expectedValue: 'Locatie geselecteerd'
    },
    {
        attr: 'adres_google_maps',
        inputValue: 'Florinstraat 7',
        expectedValue: 'Florinstraat 7, 6367 DB Voerendaal, Nederland'
    },
    {
        attr: 'afbeelding',
        inputValue: 'https://wiki.zaaksysteem.nl/Bestand:Geactiveerd_contact.png',
        expectedValue: 'https://wiki.zaaksysteem.nl/Bestand:Geactiveerd_contact.png'
    },
    {
        attr: 'numeriek',
        inputValue: '2468',
        expectedValue: '2468'
    },
    {
        attr: 'numeriek_plus',
        inputValue: ['2', '4', '6', '8'],
        expectedValue: ['2', '4', '6', '8']
    },
    {
        attr: 'enkelvoudige_keuze',
        inputValue: 3,
        expectedValue: 'Optie 3'
    },
    {
        attr: 'rich_text',
        inputValue: 'this is my test text\nand another line',
        expectedValue: 'this is my test text\nand another line'
    },
    {
        attr: 'keuzelijst',
        inputValue: 3,
        expectedValue: 'string:Optie 3'
    },
    {
        attr: 'keuzelijst_plus',
        inputValue: [3, 5, 1],
        expectedValue: ['string:Optie 3', 'string:Optie 5', 'string:Optie 1']
    },
    {
        attr: 'tekstveld',
        inputValue: 'this is my test text',
        expectedValue: 'this is my test text'
    },
    {
        attr: 'tekstveld_plus',
        inputValue: ['this', 'is', 'test', 'text'],
        expectedValue: ['this', 'is', 'test', 'text']
    },
    {
        attr: 'tekstveld_hoofdletters',
        inputValue: 'this is my test text',
        expectedValue: 'THIS IS MY TEST TEXT'
    },
    {
        attr: 'tekstveld_hoofdletters_plus',
        inputValue: ['this', 'is', 'test', 'text'],
        expectedValue: ['THIS', 'IS', 'TEST', 'TEXT']
    },
    {
        attr: 'groot_tekstveld',
        inputValue: 'this is my test text\nand another line',
        expectedValue: 'this is my test text\nand another line'
    },
    {
        attr: 'groot_tekstveld_plus',
        inputValue: ['this is my test text\nand another line', 'and another test text\nand another line'],
        expectedValue: ['this is my test text\nand another line', 'and another test text\nand another line']
    },
    {
        attr: 'webadres',
        inputValue: 'https://www.wikipedia.org',
        expectedValue: 'https://www.wikipedia.org'
    },
    {
        attr: 'valuta',
        inputValue: '12,34',
        expectedValue: '12,34',
        expectedReadValue: '€ 12,34'
    },
    {
        attr: 'valuta_exc_19',
        inputValue: '23,45',
        expectedValue: '23,45',
        expectedBtwValue: 'incl. btw: € 27,91'
    },
    {
        attr: 'valuta_exc_21',
        inputValue: '34,56',
        expectedValue: '34,56',
        expectedBtwValue: 'incl. btw: € 41,82'
    },
    {
        attr: 'valuta_exc_6',
        inputValue: '45,67',
        expectedValue: '45,67',
        expectedBtwValue: 'incl. btw: € 48,41'
    },
    {
        attr: 'valuta_inc_19',
        inputValue: '56,78',
        expectedValue: '56,78',
        expectedBtwValue: 'excl. btw: € 47,71'
    },
    {
        attr: 'valuta_inc_21',
        inputValue: '67,89',
        expectedValue: '67,89',
        expectedBtwValue: 'excl. btw: € 56,11'
    },
    {
        attr: 'valuta_inc_6',
        inputValue: '78,90',
        expectedValue: '78,90',
        expectedBtwValue: 'excl. btw: € 74,43'
    }
];

describe('when opening a case with all atrributes', () => {
    beforeAll(() => {
        openPageAs('admin', 48);
    });

    testData.forEach(data => {
        const { attr, inputValue, expectedValue, expectedBtwValue, expectedReadValue } = data;

        it(`and when filling out an attribute of type ${attr} it should have the given value`, () => {
            inputAttribute(form.$(`[data-name="${attr}"]`), inputValue);

            if (attr === 'tekstveld_hoofdletters' || attr === 'tekstveld_hoofdletters_plus') {
                waitForSave();
            }

            expect(getValue(form.$(`[data-name="${attr}"]`))).toEqual(expectedValue);

            if (expectedBtwValue) {
                expect(getValutaDisplay(form.$(`[data-name="${attr}"]`))).toEqual(expectedBtwValue);
            }
        });
    });

    afterAll(() => {
        openPage();
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
