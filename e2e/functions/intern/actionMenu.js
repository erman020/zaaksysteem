import {
    selectFirstSuggestion
} from './../common/select';
import inputDate from './../common/input/inputDate';

export const form = $('zs-case-admin-view form');
export const actionButtons = $$('zs-contextual-setting-menu li.contextual-setting.ng-scope');
export const expandIcon = $$('zs-contextual-setting-menu [icon-type="chevron-down"]');

export const toggle = () => {
    $('.top-bar-action-container .top-bar-button').click();
};

export const toggleManageActions = () => {
    expandIcon
        .isDisplayed()
        .then(isDisplayed => {
            if (isDisplayed) {
                expandIcon.click();
            }
        });
};

export const openAllActions = () => {
    toggle();
    toggleManageActions();
};

export const selectAction = action => {
    openAllActions();
    actionButtons
        .filter(elm =>
            elm.getText().then(text =>
                text === action
            )
        )
        .first()
        .click();
};

//dashboard actions

export const resetDashboard = () => {
    selectAction('Terug naar standaardinstelling');
};

//caseview actions

export const assignTo = ( type, data ) => {
    selectAction('Toewijzing wijzigen');

    switch (type) {

        case 'group':
            $('[value="org-unit"]').click();
            $('[data-name="department-picker"] select').sendKeys(data.department);
            $('[data-name="role-picker"] select').sendKeys(data.role);
            break;

        case 'person':
            $('[value="assignee"]').click();
            selectFirstSuggestion(form.$('[data-name="assignee"] input'), data.name);
            if ( !data.changeDepartment ) {
                form.$('[data-name="change_department"] input').click();
            }
            if ( !data.sendEmail ) {
                form.$('[data-name="send_email"] input').click();
            }
            break;

        case 'self':
            $('[value="assign_to_self"]').click();
            break;

        default:
            break;

    }

    form.submit();
};

export const stall = ( type, termAmountType, termAmount ) => {
    selectAction('Opschorten');

    $('[data-name="reason"] input').sendKeys('Reason');

    if (type === 'indeterminate') {
        $('[data-name="term"] [value="indeterminate"]').click();
    } else if ( type === 'determinate' ) {
        $('[data-name="term"] [value="determinate"]').click();
        $('[data-name="term_amount_type"] select').sendKeys(termAmountType);
        $('[data-name="term_amount"] input')
            .clear()
            .sendKeys(termAmount);
    }

    form.submit();
};

export const resume = ( data = {}) => {
    const startDateField = $('[data-name="start_date"]');
    const endDateField = $('[data-name="end_date"]');

    selectAction('Hervatten');

    $('[data-name="reason"] input').sendKeys('Reason');

    if (data.start) {
        inputDate(startDateField, data.start);
    }

    if (data.end) {
        inputDate(endDateField, data.end);
    }

    form.submit();
};

export const closeEarly = result => {
    selectAction('Vroegtijdig afhandelen');

    $('[data-name="reason"] input').sendKeys('Reason');
    $(`[value="${result}"]`).click();

    form.submit();
};

export const changeTerm = ( type, data ) => {
    const dateField = $('[data-name="new_term_date"]');

    selectAction('Termijn wijzigen');
    $('[data-name="reason"] input').sendKeys('Reason');

    if ( type === 'fixed' ) {
        $('[value="fixedDate"]').click();
        inputDate(dateField, data.date);
    } else if ( type === 'term' ) {
        $('[value="changeTerm"]').click();
        $('[data-name="term_amount_type"] select').sendKeys(data.termType);
        $('[data-name="term_amount"] input')
            .clear()
            .sendKeys(data.termAmount);
    }

    form.submit();
};

export const relateCase = caseToRelateTo => {
    selectAction('Zaak relateren');
    selectFirstSuggestion(form.$('[data-name="case_to_relate"] input'), caseToRelateTo);
    form.submit();
};

export const duplicate = () => {
    selectAction('Zaak kopiëren');
    $('.zs-modal-body .confirm-button').click();
};

export const relateObject = object => {
    selectAction('Object relateren');
    selectFirstSuggestion(form.$('[data-name="related_object"] input'), object);
    form.submit();
};

export const changeRequestor = ( type, requestor ) => {
    selectAction('Aanvrager wijzigen');
    $('zs-case-admin-view [data-name="requestor"] .vorm-clear').click();
    $(`zs-case-admin-view [data-name="type"] [value=${type}]`).click();
    selectFirstSuggestion(form.$('[data-name="requestor"] input'), requestor);
    form.submit();
};

export const changeCoordinator = username => {
    selectAction('Coordinator wijzigen');
    selectFirstSuggestion(form.$('[data-name="coordinator"] input'), username);
    form.submit();
};

export const changeDepartmentRole = ( department, role ) => {
    selectAction('Afdeling en rol wijzigen');
    $('[data-name="department-picker"] select').sendKeys(department);
    $('[data-name="role-picker"] select').sendKeys(role);
    form.submit();
};

export const changeRegistrationDate = date => {
    const dateField = $('[data-name="registratiedatum"]');

    selectAction('Registratiedatum wijzigen');
    inputDate(dateField, date);
    form.submit();
};

export const changeTargetDate = date => {
    const dateField = $('[data-name="streefafhandeldatum"]');

    selectAction('Streefafhandeldatum wijzigen');
    inputDate(dateField, date);
    form.submit();
};

export const changeDestructionDate = ( type, updateWith ) => {
    const fixedSection = $('[data-name="vernietigingsdatum"]');
    const termSection = $('[data-name="vernietigingstermijn"]');

    selectAction('Vernietigingsdatum wijzigen');

    if ( type === 'fixed') {
        fixedSection
            .$('input[type="radio"]')
            .click();
        inputDate($('[data-name="date"]'), updateWith);
    } else if ( type === 'term' ) {
        termSection
            .$('input[type="radio"]')
            .click();
        termSection
            .$('select')
            .sendKeys(updateWith);
    }

    form.submit();

};

export const changeAttributes = ( attribute, value ) => {
    selectAction('Kenmerken wijzigen');
    selectFirstSuggestion(form.$('[data-name="attributes"] input'), attribute);
    $('zs-case-admin-attribute-list .vorm-control').sendKeys(value);
    form.submit();
};

export const changePhase = phase => {
    selectAction('Fase wijzigen');
    $('[data-name="phase"] select').sendKeys(phase);
    form.submit();
};

export const changeStatus = status => {
    selectAction('Status wijzigen');
    $('[data-name="status"] select').sendKeys(status);
    form.submit();
};

export const changeAuth = ( department, role, authType ) => {
    let authLevel;
    let i;

    switch (authType) {
        case 'manage':
            authLevel = '4';
            break;
        case 'handle':
            authLevel = '3';
            break;
        case 'access':
            authLevel = '2';
            break;
        case 'search':
            authLevel = '1';
            break;
        default:
            authLevel = '0';
            break;
    }

    selectAction('Rechten wijzigen');
    $('[data-name="department-picker"] select').sendKeys(department);
    $('[data-name="role-picker"] select').sendKeys(role);
    for ( i = 0; i < authLevel; i++) {
        $(`.capabilities-list li:nth-child(${i + 1})`).click();
    }
    form.submit();
};

export const changeCasetype = casetype => {
    selectAction('Zaaktype wijzigen');
    selectFirstSuggestion(form.$('[data-name="casetype"] input'), casetype);
    form.submit();
};

export const changePaymentStatus = paymentStatus => {
    selectAction('Betaalstatus wijzigen');
    form.$('[data-name="payment_status"] select').sendKeys(paymentStatus);
    form.submit();
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
