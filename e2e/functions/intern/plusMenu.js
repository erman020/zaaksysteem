import {
    selectFirstSuggestion
} from './../common/select';
import getRandomBsn from './../common/getValue/getRandomBsn';
import getRandomKvk from './../common/getValue/getRandomKvk';
import inputFile from './../common/input/inputFile';
import inputDate from './../common/input/inputDate';
import {
    mouseOverCreateButton,
    mouseOut
} from './../common/mouse';
import waitForElement from './../common/waitForElement';

export const openAction = name => {
    mouseOverCreateButton();
    $(`zs-contextual-action-menu-button [data-name=${name}] button`).click();
};

export const fillCreateCase = params => {
    const form = $('zs-contextual-action-menu form');
    const casetypeClose = '[data-name="casetype"] .mdi-close';
    const requestorTypeClose = '[data-name="requestor_type"] .mdi-close';
    const recipientClose = '[data-name="recipient"] .mdi-close';
    const { rememberCasetype, casetypeClear, casetype, requestor, requestorType, requestorTypeClear, recipient, recipientType, recipientClear, source} = params;

    if ( rememberCasetype ) {
        form.$('.zs-switch-label').click();
    }

    if ( casetypeClear ) {
        form.$(casetypeClose).click();
    }

    if ( casetype ) {
        selectFirstSuggestion(form.$('[data-name="casetype"] .vorm-object-suggest input'), casetype);
    }

    if ( requestorTypeClear ) {
        form.$(requestorTypeClose).click();
    }

    if ( requestorType ) {
        form.$('[data-name="requestor_type"]')
            .$(`input[type=radio][value=${requestorType}]`)
            .click();
    }

    if ( requestor ) {
        selectFirstSuggestion(form.$('[data-name="requestor"] input'), requestor);
    }

    if ( requestorType === 'medewerker' ) {
        const recipientType = recipientType ? recipientType : 'medewerker';

        form.$('[data-name="recipient_type"]')
            .$(`input[type=radio][value=${recipientType}]`)
            .click();

        if ( recipientClear ) {
            form.$(recipientClose).click();
            selectFirstSuggestion(form.$('[data-name="recipient"] input'), recipient);
        }
    }

    if ( source ) {
        form.$('[data-name="source"] select').sendKeys(source);
    }

    form.submit();
    waitForElement('zs-case-registration');
};

export const createCase = params => {
    openAction('zaak');
    fillCreateCase(params);
};

export const createContact = newContact => {
    const fields = [
        // contactType: present in all cases
        {
            name: 'betrokkene_type',
            type: 'radio'
        },
        // basic citizen info: present when betrokkene_type is equal to citizen
        {
            name: 'np-burgerservicenummer',
            type: 'text',
            defaultValue: getRandomBsn()
        },
        {
            name: 'np-voornamen',
            type: 'text'
        },
        {
            name: 'np-voorvoegsel',
            type: 'text'
        },
        {
            name: 'np-geslachtsnaam',
            type: 'text',
            defaultValue: 'lastName'
        },
        {
            name: 'np-adellijke_titel',
            type: 'text'
        },
        {
            name: 'np-geslachtsaanduiding',
            type: 'radio',
            defaultValue: 'M'
        },
        {
            name: 'np-landcode',
            type: 'list'
        },
        // basic address info: present when np-landcode is equal to Nederland
        {
            name: 'briefadres',
            type: 'checkbox'
        },
        {
            name: 'np-postcode',
            type: 'text',
            defaultValue: '1111AA'
        },
        {
            name: 'np-huisnummer',
            type: 'text',
            defaultValue: '1'
        },
        {
            name: 'np-huisnummertoevoeging',
            type: 'text'
        },
        {
            name: 'np-straatnaam',
            type: 'text',
            defaultValue: 'streetName'
        },
        {
            name: 'np-woonplaats',
            type: 'text',
            defaultValue: 'city'
        },
        {
            name: 'np-in-gemeente',
            type: 'checkbox'
        },
        // correspondence address info: present when briefadres is true
        {
            name: 'np-correspondentie_postcode',
            type: 'text',
            defaultValue: '1111AA'
        },
        {
            name: 'np-correspondentie_huisnummer',
            type: 'text',
            defaultValue: '1'
        },
        {
            name: 'np-correspondentie_huisnummertoevoeging',
            type: 'text'
        },
        {
            name: 'np-correspondentie_straatnaam',
            type: 'text',
            defaultValue: 'streetName'
        },
        {
            name: 'np-correspondentie_woonplaats',
            type: 'text',
            defaultValue: 'city'
        },
        // foreign address info: present when np-landcode is not Nederland
        {
            name: 'np-adres_buitenland1',
            type: 'text',
            defaultValue: 'foreignAddress1'
        },
        {
            name: 'np-adres_buitenland2',
            type: 'text'
        },
        {
            name: 'np-adres_buitenland3',
            type: 'text'
        },
        // basic organisation info: present when betrokkene_type is equal to organisation
        {
            name: 'vestiging_landcode',
            type: 'list'
        },
        // specific organisation info: present when vestiging_landcode is equal to Nederland
        {
            name: 'rechtsvorm',
            type: 'list',
            defaultValue: 'Eenmanszaak'
        },
        {
            name: 'dossiernummer',
            type: 'text',
            defaultValue: getRandomKvk()
        },
        {
            name: 'vestigingsnummer',
            type: 'text'
        },
        // basic organisation info: present when betrokkene_type is equal to organisation
        {
            name: 'handelsnaam',
            type: 'text',
            defaultValue: 'tradeName'
        },
        {
            name: 'org-briefadres',
            type: 'checkbox'
        },
        // basic address info: present when vestiging_landcode is equal to Nederland
        {
            name: 'vestiging_postcode',
            type: 'text',
            defaultValue: '1111AA'
        },
        {
            name: 'vestiging_huisnummer',
            type: 'text',
            defaultValue: '1'
        },
        {
            name: 'vestiging_huisletter',
            type: 'text'
        },
        {
            name: 'vestiging_huisnummertoevoeging',
            type: 'text'
        },
        {
            name: 'vestiging_straatnaam',
            type: 'text',
            defaultValue: 'streetName'
        },
        {
            name: 'vestiging_woonplaats',
            type: 'text',
            defaultValue: 'city'
        },
        // foreign address info: present when vestiging_landcode is not Nederland
        {
            name: 'vestiging_adres_buitenland1',
            type: 'text',
            defaultValue: 'foreignAddress1'
        },
        {
            name: 'vestiging_adres_buitenland2',
            type: 'text'
        },
        {
            name: 'vestiging_adres_buitenland3',
            type: 'text'
        },
        // basic correspondence info: present when org-briefadres is checked
        {
            name: 'correspondentie_landcode',
            type: 'list'
        },
        // basic correspondence info: present when correspondentie_landcode is equal to Nederland
        {
            name: 'correspondentie_postcode',
            type: 'text',
            defaultValue: '1111AA'
        },
        {
            name: 'correspondentie_huisnummer',
            type: 'text',
            defaultValue: '1'
        },
        {
            name: 'correspondentie_huisletter',
            type: 'text'
        },
        {
            name: 'correspondentie_huisnummertoevoeging',
            type: 'text'
        },
        {
            name: 'correspondentie_straatnaam',
            type: 'text',
            defaultValue: 'streetName'
        },
        {
            name: 'correspondentie_woonplaats',
            type: 'text',
            defaultValue: 'city'
        },
        // foreign correspondence info: present when correspondentie_landcode is not Nederland
        {
            name: 'correspondentie_adres_buitenland1',
            type: 'text',
            defaultValue: 'foreignAddress1'
        },
        {
            name: 'correspondentie_adres_buitenland2',
            type: 'text'
        },
        {
            name: 'correspondentie_adres_buitenland3',
            type: 'text'
        },
        // contactInfo: present in all cases
        {
            name: 'npc-telefoonnummer',
            type: 'text'
        },
        {
            name: 'npc-mobiel',
            type: 'text'
        },
        {
            name: 'npc-email',
            type: 'text'
        }
    ];
    const form = $('zs-contextual-action-form form');

    openAction('contact');

    fields.forEach(field => {
        const attr = form.$(`[data-name="${field.name}"]`);
        const value = newContact[field.name] || field.defaultValue;
        const fillActions = {
            radio: () => attr.$(`[value="${value}"]`).click(),
            list: () => attr.$('select').sendKeys(value),
            checkbox: () => attr.$('input').click(),
            text: () => attr.$('input').sendKeys(value)
        };

        attr.isPresent().then(presence => {
            if ( value && presence ) {
                fillActions[field.type]();
            }
        });
    });

    form.submit();
    browser.sleep(2000);
    browser.get('/intern');
};

export const createContactMoment = newContactMoment => {
    const form = $('zs-contextual-action-form form');

    openAction('contact-moment');
    form.$(`[data-name="subject_type"] [value="${newContactMoment.type}"]`).click();
    selectFirstSuggestion(form.$('[data-name="subject"] input'), '111222333');
    selectFirstSuggestion(form.$('[data-name="case"] input'), '43');
    form.$('[data-name="message"] textarea').sendKeys(newContactMoment.message);
    form.$('[type="submit"]').click();
};

export const createWidget = ( type, name ) => {
    openAction('create_widget');
    $(`zs-dashboard-widget-create [data-name="${type}"]`).click();

    browser.waitForAngular();

    if ( type === 'Zoekopdracht') {
        $$('zs-dashboard-widget-search-select-list-group li')
            .filter(elm =>
                elm
                    .getText()
                    .then(text =>
                        text === name
                    )
            )
            .first()
            .click();
    }
};

export const uploadDocument = documentName => {
    const form = $('zs-contextual-action-form');

    openAction('upload');
    inputFile(form, documentName);
    mouseOut();
};

export const useTemplate = (data = {}) => {
    const form = $('zs-case-template-generate form');
    const { template, name, filetype, caseDocument } = data;

    openAction('sjabloon');

    if (template) {
        form.$('[data-name="template"] select').sendKeys(template);
    }

    if (name) {
        form.$('[data-name="name"] input').clear().sendKeys(name);
    }

    if (filetype) {
        form.$(`[data-name="filetype"] input[value="${filetype}"]`).click();
    }

    if (caseDocument) {
        form.$('[data-name="case_document"] select').sendKeys(caseDocument);
    }

    form.submit();
    mouseOut();
};

export const relateSubject = data => {
    const form = $('zs-case-add-subject form');
    const { subjectType, role, roleOther, subjectToRelate, authorisation, notification } = data;

    openAction('betrokkene');

    if ( subjectType ) {
        const type = subjectType === 'organisation' ? 'bedrijf' : 'natuurlijk_persoon';

        form.$(`[data-name="relation_type"] input[value="${type}"]`).click();
    }

    selectFirstSuggestion(form.$('[data-name="related_subject"] input'), subjectToRelate);

    if ( role ) {
        form.$('[data-name="related_subject_role"] select').sendKeys(role);
    }

    if ( roleOther ) {
        form.$('[data-name="related_subject_role_freeform"] input').sendKeys(roleOther);
    }

    if ( authorisation ) {
        form.$('[data-name="pip_authorized"] input').click();
    }

    if ( notification ) {
        form.$('[data-name="notify_subject"] input').click();
    }

    form.submit();
    mouseOut();
};

export const sendEmail = data => {
    const form = $('zs-case-send-email form');
    const { template, recipientType, recipientEmployee, recipientOther, cc, bcc, subject, content } = data;

    openAction('email');

    if ( template ) {
        form.$('[data-name="template"] select').sendKeys(template);
    }

    form.$('[data-name="recipient_type"] select').sendKeys(recipientType);

    if ( recipientEmployee ) {
        selectFirstSuggestion(form.$('[data-name="recipient_medewerker"] input'), recipientEmployee);
    }

    if ( recipientOther ) {
        form.$('[data-name="recipient_address"] input').sendKeys(recipientOther);
    }

    if ( cc ) {
        form.$('[data-name="recipient_cc"] input').sendKeys(cc);
    }

    if ( bcc ) {
        form.$('[data-name="recipient_bcc"] input').sendKeys(bcc);
    }

    if ( subject ) {
        form.$('[data-name="email_subject"] input').sendKeys(subject);
    }

    if (content ) {
        form.$('[data-name="email_content"] textarea').sendKeys(content);
    }

    form.submit();
};

export const planCase = data => {
    const form = $('zs-case-plan form');
    const { casetype, copyRelations, pattern, count, type, repeat } = data;

    openAction('geplande-zaak');
    selectFirstSuggestion(form.$('[data-name="casetype"] input'), casetype);

    if ( copyRelations ) {
        form.$('[data-name="copy_relations"] input').click();
    }

    inputDate(form.$('[data-name="from"]'), data.from);

    if ( pattern ) {
        form.$('[data-name="has_pattern"] input').click();

        if ( count ) {
            form.$('[data-name="count"] input').clear().sendKeys(count);
        }

        if ( type ) {
            form.$('[data-name="type"] select').sendKeys(type);
        }

        if ( repeat ) {
            form.$('[data-name="repeat"] input').clear().sendKeys(repeat);
        }
    }

    form.submit();
    mouseOut();
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
