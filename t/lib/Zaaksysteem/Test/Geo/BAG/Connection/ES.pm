use utf8;
package Zaaksysteem::Test::Geo::BAG::Connection::ES;
use Moose;
extends "Zaaksysteem::Test::Moose";

use Zaaksysteem::Geo::BAG::Connection::ES;
use Zaaksysteem::Test;

=head1 NAME

Zaaksysteem::Test::Geo::BAG::Connection::ES - Test BAG search Elasticsearch connection

=head1 DESCRIPTION

Test the BAG model's "Elasticsearch" connection type.

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Geo::BAG::Connection::ES

=cut

sub test_search {
    my $client = Zaaksysteem::Test::Geo::BAG::Connection::ES::FakeClient->new(
        logger => 1,
        transport => 1,
    );
    my $connection = Zaaksysteem::Geo::BAG::Connection::ES->new(
        es => $client,
    );

    my $override_parse_query = override(
        'Zaaksysteem::Geo::BAG::Connection::ES::_parse_query',
        sub {
            my $self = shift;
            my $query = shift;

            is($query, "search query", 'Search query passed to _parse_query correctly');

            return "the query goes here";
        }
    );
    my $override_search = override(
        'Zaaksysteem::Geo::BAG::Connection::ES::_search',
        sub {
            my $self = shift;
            my $index = shift;
            my $query = shift;

            is($index, 'bag_nummeraanduiding', 'Correct index was found');
            cmp_deeply(
                $query,
                {
                    query => 'the query goes here',
                    sort => [
                        '_score',
                        { "plaats" => "asc" },
                        { "huisnummer" => "asc" },
                        { "huisletter" => "asc" },
                        { "huisnummer_toevoeging" => "asc" },
                    ],
                },
                'Parsed query is passed to _search correctly'
            );

            return "result";
        }
    );

    my $result = $connection->search(
        type => 'nummeraanduiding',
        query => 'search query',
    );
    is($result, 'result', 'Search result is returned correctly');

    undef $override_parse_query;
    $override_parse_query = override(
        'Zaaksysteem::Geo::BAG::Connection::ES::_parse_query',
        sub {
            my $self = shift;
            my $query = shift;

            is($query, "search query", 'Search query passed to _parse_query correctly');

            return "the query goes here";
        }
    );

    $result = $connection->search(
        type => 'nummeraanduiding',
        query => 'sëärćh qùêrÿ',
    );
    is($result, 'result', 'Search result with accented characters is returned correctly');
}

sub test_get_exact {
    my $client = Zaaksysteem::Test::Geo::BAG::Connection::ES::FakeClient->new(
        logger    => 1,
        transport => 1,
    );
    my $connection = Zaaksysteem::Geo::BAG::Connection::ES->new(
        es => $client,
    );

    my %handles;
    $handles{_search} = override(
        'Zaaksysteem::Geo::BAG::Connection::ES::_search',
        sub {
            my $self = shift;
            my $index = shift;
            my $query = shift;

            is($index, 'bag_nummeraanduiding', 'Correct index was found');
            cmp_deeply(
                $query,
                {
                    query => {
                        bool => {
                            filter => [
                                { term => { postcode   => '1234AA' } },
                                { term => { huisnummer => '42' } },
                                { term => { huisletter => 'Q' } },
                                { term => { huisnummer_toevoeging => '2' } },
                            ],
                            should => {
                                bool => {
                                    must_not => {
                                        term => { status => 'Naamgeving ingetrokken' }
                                    }
                                }
                            },
                        }
                    }
                },
                '"Exact" query is translated to ES-style correctly',
            );

            return "result";
        }
    );

    my $rv = $connection->get_exact(
        type => 'nummeraanduiding',
        fields => {
            postcode => '1234AA',
            huisnummer => '42',
            huisletter => 'Q',
            huisnummer_toevoeging => '2',
        }
    );
    is ($rv, 'result', 'get_exact returned search result')
}

sub test_get {
    my $client = Zaaksysteem::Test::Geo::BAG::Connection::ES::FakeClient->new(
        logger    => 1,
        transport => 1,
    );
    my $connection = Zaaksysteem::Geo::BAG::Connection::ES->new(
        es => $client,
    );

    {
        $client->get_response(undef);
        my $result = $connection->get(
            type => 'nummeraanduiding',
            id   => '31337',
        );
        is($result, undef, "Result for 'get' on non-existant id is the undefined value");
    }
    {
        $client->get_response({
            _source => { some => 'object' }
        });
        my $result = $connection->get(
            type => 'nummeraanduiding',
            id   => '31337',
        );
        cmp_deeply($result, { some => 'object'}, 'When an object is found, _source is returned');
    }
}

sub test_find_nearest {
    my $client = Zaaksysteem::Test::Geo::BAG::Connection::ES::FakeClient->new(
        logger => 1,
        transport => 1,
    );
    my $connection = Zaaksysteem::Geo::BAG::Connection::ES->new(
        es => $client,
    );

    my %location_query = (
        latitude  => '42',
        longitude => '3',
    );
    my %handles;
    $handles{_search} = override(
        'Zaaksysteem::Geo::BAG::Connection::ES::_search',
        sub {
            my $self = shift;
            my $index = shift;
            my $query = shift;

            is($index, 'bag_nummeraanduiding', 'Correct index was found');
            cmp_deeply(
                $query,
                {
                    "size" => 1,
                    "query" => {
                        "bool" => {
                            "must" => {
                                "function_score" => {
                                    "gauss" => {
                                        "geo_lat_lon" => {
                                            "origin" => {
                                                "lat" => $location_query{latitude},
                                                "lon" => $location_query{longitude}
                                            },
                                            "offset" => '1m',
                                            "scale"  => '50m',
                                        }
                                    }
                                }
                            },
                            "filter" => {
                                "geo_distance" => {
                                    "distance" => '500m',
                                    "geo_lat_lon" => {
                                        "lat" => $location_query{latitude},
                                        "lon" => $location_query{longitude}
                                    }
                                }
                            }
                        }
                    }
                },
                'find_nearest uses the correct ES query to find the nearest BAG object',
            );
            
            return "result";
        }
    );

    my $rv = $connection->find_nearest(
        type => 'nummeraanduiding',
        %location_query,
    );
    is($rv, 'result', 'find_nearest returns the search result');
}

sub test__parse_query {
    my $priority = ['Priority'];
    my $client = Zaaksysteem::Test::Geo::BAG::Connection::ES::FakeClient->new(
        logger => 1,
        transport => 1,
    );
    my $connection = Zaaksysteem::Geo::BAG::Connection::ES->new(
        es => $client,
        priority_gemeentes => $priority,
    );

    {
        my %parsed = %{ $connection->_parse_query("") };

        cmp_deeply(
            \%parsed,
            {
                bool => {
                    must => {
                        bool => { should => [] }
                    },
                    should => [
                        { term => { gemeente => { value => 'Priority', boost => 0.5 } } }
                    ],
                    must_not => { term => { status => "Naamgeving ingetrokken" } }
                }
            },
            'Empty querystring with priority gemeente leads to only gemeente in query'
        );

        %parsed = %{ $connection->_parse_query("1114AD") };
        cmp_deeply(
            \%parsed,
            {
                bool => {
                    must => {
                        bool => {
                            should => [
                                { term => { postcode => { boost => 1.2, value => '1114AD' } } }
                            ] 
                        }
                    },
                    should => [
                        { term => { gemeente => { value => 'Priority', boost => 0.5 } } }
                    ],
                    must_not => { term => { status => "Naamgeving ingetrokken" } }
                }
            },
            'Query string with postcode does a postcode search'
        );

        %parsed = %{ $connection->_parse_query("1114 Ad") };
        cmp_deeply(
            \%parsed,
            {
                bool => {
                    must => {
                        bool => {
                            should => [
                                { term => { postcode => { boost => 1.2, value => '1114AD' } } }
                            ] 
                        }
                    },
                    should => [
                        { term => { gemeente => { value => 'Priority', boost => 0.5 } } }
                    ],
                    must_not => { term => { status => "Naamgeving ingetrokken" } }
                }
            },
            'Query string with postcode with mixed case and a space does a postcode search'
        );

        %parsed = %{ $connection->_parse_query("Kerkstraat 10") };
        cmp_deeply(
            \%parsed,
            {
                'bool' => {
                    'must' => {
                        'bool' => {
                            'should' => [
                                { 'prefix' => { 'plaats'     => 'Kerkstraat' } },
                                { 'term'   => { 'plaats'     => { 'boost' => '1.1', 'value' => 'Kerkstraat' } } },
                                { 'prefix' => { 'straatnaam' => 'Kerkstraat' } },
                                { 'term'   => { 'straatnaam' => { 'boost' => '1.1', 'value' => 'Kerkstraat' } } },
                                { 'term'   => { 'huisletter' => { 'value' => 'Kerkstraat' } } },
                                { 'term'   => { 'huisnummer_toevoeging' => { 'value' => 'Kerkstraat' } } },
                                { 'term'   => { 'huisnummer' => { 'boost' => '1.1', 'value' => '10' } } },
                                { 'prefix' => { 'plaats'     => '10' } },
                                { 'term'   => { 'plaats'     => { 'boost' => '1.1', 'value' => '10' } } },
                                { 'prefix' => { 'straatnaam' => '10' } },
                                { 'term'   => { 'straatnaam' => { 'boost' => '1.1', 'value' => '10' } } },
                                { 'term'   => { 'huisletter' => { 'value' => '10' } } },
                                { 'term'   => { 'huisnummer_toevoeging' => { 'value' => '10' } } },
                                { 'prefix' => { 'plaats'     => 'Kerkstraat 10' } },
                                { 'term'   => { 'plaats'     => { 'boost' => '1.1', 'value' => 'Kerkstraat 10' } } },
                                { 'prefix' => { 'straatnaam' => 'Kerkstraat 10' } },
                                { 'term'   => { 'straatnaam' => { 'boost' => '1.1', 'value' => 'Kerkstraat 10' } } },
                                { 'term'   => { 'huisletter' => { 'value' => 'Kerkstraat 10' } } },
                                { 'term'   => { 'huisnummer_toevoeging' => { 'value' => 'Kerkstraat 10' } } },
                            ]
                        }
                    },
                    'should' => [
                        {
                            'term' => { 'gemeente' => { 'boost' => '0.5', 'value' => 'Priority' } }
                        }
                    ],
                    must_not => { term => { status => "Naamgeving ingetrokken" } }
                }
            },
            'Query string with street + house number'
        );
    }

    {
        pop @$priority;
        my %parsed = %{ $connection->_parse_query("") };

        cmp_deeply(
            \%parsed,
            {
                bool => {
                    should   => [],
                    must_not => { term => { status => "Naamgeving ingetrokken" } }
                }
            },
            'Empty querystring without priority gemeente leads to empty query'
        );

        %parsed = %{ $connection->_parse_query("1114AD") };
        cmp_deeply(
            \%parsed,
            {
                bool => {
                    should => [
                        { term => { postcode => { boost => 1.2, value => '1114AD' } } }
                    ],
                    must_not => { term => { status => "Naamgeving ingetrokken" } }
                }
            },
            'Query string with postcode does a postcode search (no priority gemeente)'
        );

        %parsed = %{ $connection->_parse_query("1114 Ad") };
        cmp_deeply(
            \%parsed,
            {
                bool => {
                    should => [
                        { term => { postcode => { boost => 1.2, value => '1114AD' } } }
                    ],
                    must_not => { term => { status => "Naamgeving ingetrokken" } }
                }
            },
            'Query string with postcode with mixed case and a space does a postcode search (no priority gemeente)'
        );

        %parsed = %{ $connection->_parse_query("Kerkstraat 10") };
        cmp_deeply(
            \%parsed,
            {
                'bool' => {
                    'should' => [
                        { 'prefix' => { 'plaats' => 'Kerkstraat' } },
                        { 'term' => { 'plaats' => { 'boost' => '1.1', 'value' => 'Kerkstraat' } } },
                        { 'prefix' => { 'straatnaam' => 'Kerkstraat' } },
                        { 'term' => { 'straatnaam' => { 'boost' => '1.1', 'value' => 'Kerkstraat' } } },
                        { 'term'   => { 'huisletter' => { 'value' => 'Kerkstraat' } } },
                        { 'term'   => { 'huisnummer_toevoeging' => { 'value' => 'Kerkstraat' } } },
                        { 'term' => { 'huisnummer' => { 'boost' => '1.1', 'value' => '10' } } },
                        { 'prefix' => { 'plaats' => '10' } },
                        { 'term' => { 'plaats' => { 'boost' => '1.1', 'value' => '10' } } },
                        { 'prefix' => { 'straatnaam' => '10' } },
                        { 'term' => { 'straatnaam' => { 'boost' => '1.1', 'value' => '10' } } },
                        { 'term'   => { 'huisletter' => { 'value' => '10' } } },
                        { 'term'   => { 'huisnummer_toevoeging' => { 'value' => '10' } } },
                        { 'prefix' => { 'plaats' => 'Kerkstraat 10' } },
                        { 'term' => { 'plaats' => { 'boost' => '1.1', 'value' => 'Kerkstraat 10' } } },
                        { 'prefix' => { 'straatnaam' => 'Kerkstraat 10' } },
                        { 'term' => { 'straatnaam' => { 'boost' => '1.1', 'value' => 'Kerkstraat 10' } } },
                        { 'term'   => { 'huisletter' => { 'value' => 'Kerkstraat 10' } } },
                        { 'term'   => { 'huisnummer_toevoeging' => { 'value' => 'Kerkstraat 10' } } },
                    ],
                    must_not => { term => { status => "Naamgeving ingetrokken" } }
                }
            },
            'Query string with street + house number (no priority gemeente)'
        );
    }
}

__PACKAGE__->meta->make_immutable;

package Zaaksysteem::Test::Geo::BAG::Connection::ES::FakeClient {
    use Moose;

    with 'Search::Elasticsearch::Role::Client';

    has get_response => ( is => 'rw' );

    sub parse_request {}

    sub get {
        my $self = shift;
        return $self->get_response if defined $self->get_response;

        die "Nothing found test.";
    }

    __PACKAGE__->meta->make_immutable();
};

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
