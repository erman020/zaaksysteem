package Zaaksysteem::Test::Geo::BAG::Connection::Spoof;
use Moose;
extends "Zaaksysteem::Test::Moose";

use FindBin;
use Zaaksysteem::Test;
use Zaaksysteem::Geo::BAG::Connection::Spoof;

=head1 NAME

Zaaksysteem::Test::Geo::BAG::Connection::Spoof - Test BAG search spoof connection

=head1 DESCRIPTION

Test the BAG model's "spoof" connection type.

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Geo::BAG::Connection::Spoof

=cut

sub test_search {
    my $conn = Zaaksysteem::Geo::BAG::Connection::Spoof->new(
        home => "$FindBin::Bin/..",
        priority_gemeentes => [ 'Amsterdam' ],
    );

    {
        my $results = $conn->search(
            type => 'nummeraanduiding',
            query => '1431GG',
        );
        is(@$results, 1, 'Searching for postcode = 1 result');
    }

    {
        my $results = $conn->search(
            type => 'nummeraanduiding',
            query => 'Reg',
        );
        is(@$results, 3, 'Searching for street = multiple results');
    }

    {
        my $results = $conn->search(
            type => 'nummeraanduiding',
            query => 'Qqq',
        );
        is(@$results, 0, 'Searching for nonsense = no results');
    }
}

sub test_get_exact {
    my $conn = Zaaksysteem::Geo::BAG::Connection::Spoof->new(
        home => "$FindBin::Bin/.."
    );

    my $results = $conn->get_exact(
        type   => 'nummeraanduiding',
        fields => {
            postcode   => '1431GG',
            huisnummer => 60,
            huisletter => '',
            huisnummer_toevoeging => '',
        },
    );

    is(@$results, 1, 'Searching for postcode + huisnummer = 1 result');
}

sub test_get {
    my $conn = Zaaksysteem::Geo::BAG::Connection::Spoof->new(
        home => "$FindBin::Bin/.."
    );

    my $res1 = $conn->get(
        type => 'nummeraanduiding',
        id   => '758200000062258',
    );
    is($res1, undef, 'Unknown nummeraanduiding - "get" returns undef');

    my $res2 = $conn->get(
        type => 'nummeraanduiding',
        id   => '758200000062259',
    );
    isa_ok(
        $res2, 
        'HASH',
        'Nummeraanduiding - "get" returns object',
    );
    is(
        $res2->{woonplaats}{openbareruimte}{nummeraanduiding}{id},
        '758200000062259',
        'Correct BAG entry returned by "get"'
    );
}

sub test_find_nearest {
    my $conn = Zaaksysteem::Geo::BAG::Connection::Spoof->new(
        home => "$FindBin::Bin/.."
    );

    my $res = $conn->find_nearest(
        type => 'nummeraanduiding',
        latitude  => '51.92235',
        longitude => '4.474016',
    );

    is(
        $res->[0]{woonplaats}{openbareruimte}{nummeraanduiding}{id},
        '599200000323268',
        'Found correct nearest object',
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
