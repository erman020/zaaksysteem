package Zaaksysteem::Test::Geo::BAG::Model;
use Moose;
extends "Zaaksysteem::Test::Moose";

use FindBin;
use Sub::Override;
use Zaaksysteem::Test;
use Zaaksysteem::Test::Mocks;
use Zaaksysteem::Geo::BAG::Connection::Spoof;
use Zaaksysteem::Geo::BAG::Model;

=head1 NAME

Zaaksysteem::Test::Geo::BAG::Model - Tests for the BAG model

=head1 DESCRIPTION

Test the BAG model.

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Geo::BAG::Model

=cut

sub test_get {
    my $connection = Zaaksysteem::Test::Geo::BAG::Model::FakeConnection->new();
    my $model = Zaaksysteem::Geo::BAG::Model->new(
        connection => $connection,
        priority_gemeentes => [],
    );

    my $result = $model->get(
        nummeraanduiding => '31337',
    );
    cmp_deeply(
        $connection->calls,
        [
            [
                type => 'nummeraanduiding',
                id => '31337'
            ]
        ],
        'get method called correctly',
    );
    isa_ok($result, 'Zaaksysteem::Geo::BAG::Object', 'Return value is a BAG object');
}

sub test_find_nearest {
    my $connection = Zaaksysteem::Test::Geo::BAG::Model::FakeConnection->new();
    my $model = Zaaksysteem::Geo::BAG::Model->new(
        connection => $connection,
        priority_gemeentes => [],
    );

    my $result = $model->find_nearest(
        type      => 'openbareruimte',
        latitude  => 52.2,
        longitude => 4.5,
    );

    cmp_deeply(
        $connection->calls,
        [
            [
                type => 'openbareruimte',
                latitude => 52.2,
                longitude => 4.5,
            ]
        ],
        'find_nearest method called correctly',
    );
    isa_ok($result, 'Zaaksysteem::Geo::BAG::Object', 'Return value is a BAG object');
}

sub test_get_exact {
    my $connection = Zaaksysteem::Test::Geo::BAG::Model::FakeConnection->new(
        number_of_results => 0,
    );
    my $model = Zaaksysteem::Geo::BAG::Model->new(
        connection => $connection,
        priority_gemeentes => [],
    );

    throws_ok(
        sub { $model->get_exact(postcode => '1234AA', 'huisnummer' => 42) },
        qr{geo/bag/model/no_results},
        'Exact search with no results throws an exception',
    );

    $connection->number_of_results(2);
    my $result = $model->get_exact(postcode => '1234AA', 'huisnummer' => 42);
    isa_ok(
        $result,
        'Zaaksysteem::Geo::BAG::Object',
        'Exact search with a multiple results returns a single BAG object',
    );

    $connection->calls([]);
    $connection->number_of_results(1);
    my $rv = $model->get_exact(postcode => '1234AA', 'huisnummer' => 42);
    isa_ok(
        $rv,
        'Zaaksysteem::Geo::BAG::Object',
        'Exact search with a single result returns a single BAG object',
    );

    cmp_deeply(
        $connection->calls,
        [
            [
                type => 'nummeraanduiding',
                fields => {
                    postcode => '1234AA',
                    huisnummer => '42',
                    huisletter => '',
                    huisnummer_toevoeging => '',
                }
            ]
        ],
        'get_exact creates the a correct search'
    );
}

sub test_search {
    my $connection = Zaaksysteem::Test::Geo::BAG::Model::FakeConnection->new();
    my $model = Zaaksysteem::Geo::BAG::Model->new(
        connection => $connection,
        priority_gemeentes => [],
    );

    my $rv = $model->search(
        type  => 'nummeraanduiding',
        query => 'testing',
    );
    isa_ok($rv, 'ARRAY', 'Search returns an array reference');
    is(@$rv, 1, 'One result returned by BAG search');
    isa_ok($rv->[0], 'Zaaksysteem::Geo::BAG::Object', 'First object is a BAG object');

    cmp_deeply(
        $connection->calls,
        [
            [
                type => 'nummeraanduiding',
                query => 'testing',
            ]
        ]
    );
}

package Zaaksysteem::Test::Geo::BAG::Model::FakeConnection;
use Moose;

with 'Zaaksysteem::Geo::BAG::Connection';

has number_of_results => (
    isa     => 'Int',
    is      => 'rw',
    default => 1,
);

has calls => (
    isa => 'ArrayRef',
    is => 'rw',
    default => sub { [] },
);

my $bag_entry = {
    "land"        => "Nederland",
    "plaats"      => "Aalsmeer",
    "gemeente"    => "Aalsmeer",
    "geo_lat_lon" => "52.2559665,4.7780646",
    "geo_punt"    => {
        "type" => "Point",
        "coordinates" => [4.77806462756352, 52.2559665402755, 43.1847753608599]
    },
    "geo_vlak"   => undef,
    "postcode"   => "1431GG",
    "provincie"  => "Noord-Holland",
    "huisletter" => "",
    "huisnummer" => 60,
    "straatnaam" => "Lakenblekerstraat",
    "woonplaats" => {
        "id"             => 2571,
        "naam"           => "Aalsmeer",
        "status"         => "Woonplaats aangewezen",
        "inactief"       => 0,
        "correctie"      => 0,
        "einddatum"      => undef,
        "officieel"      => 0,
        "begindatum"     => "2009-06-01T00:00:00.01",
        "documentdatum"  => "2009-05-19",
        "in_onderzoek,"  => 0,
        "documentnummer" => "2009/7701",
        "openbareruimte" => {
            "id"               => 358300018848979,
            "naam"             => "Lakenblekerstraat",
            "type"             => "Weg",
            "status"           => "Naamgeving uitgegeven",
            "inactief"         => 0,
            "correctie"        => 0,
            "einddatum"        => undef,
            "officieel"        => 0,
            "begindatum"       => "1972-09-05T00:00:00.1",
            "documentdatum"    => "1972-09-05",
            "in_onderzoek,"    => 0,
            "verkorte_naam"    => undef,
            "documentnummer"   => "6308",
            "nummeraanduiding" => {
                "id"              => 358200027933405,
                "status"          => "Naamgeving uitgegeven",
                "inactief"        => 0,
                "postcode"        => "1431GG",
                "correctie"       => 0,
                "einddatum"       => undef,
                "officieel"       => 0,
                "begindatum"      => "2014-11-20T00:00:00",
                "huisletter"      => "",
                "huisnummer"      => 60,
                "documentdatum"   => "2014-11-20",
                "in_onderzoek,"   => 0,
                "documentnummer"  => "D-2014/319831",
                "verblijfsobject" => {
                    "id"   => 358010000114154,
                    "pand" => {
                        "id"             => 358100019050674,
                        "status"         => "Pand in gebruik",
                        "bouwjaar"       => 1974,
                        "inactief"       => 0,
                        "correctie"      => 0,
                        "einddatum"      => undef,
                        "officieel"      => 0,
                        "begindatum"     => "2010-11-15T00:00:00.1",
                        "documentdatum"  => "2010-11-15",
                        "in_onderzoek,"  => 0,
                        "documentnummer" => "2010/15203"
                    },
                    "status"       => "Verblijfsobject in gebruik",
                    "inactief"     => 0,
                    "correctie"    => 0,
                    "einddatum"    => undef,
                    "officieel"    => 0,
                    "begindatum"   => "2014-10-08T00:00:00",
                    "oppervlakte"  => 1236,
                    "gebruiksdoel" => [
                        "industriefunctie", "kantoorfunctie",
                        "winkelfunctie"
                    ],
                    "documentdatum"  => "2014-10-08",
                    "in_onderzoek,"  => 0,
                    "documentnummer" => "D-2014/277756"
                },
                "huisnummer_toevoeging" => ""
            }
        }
    },
    "export_date"           => "2018-01-25T09:20:09.372107+00:00",
    "huisnummer_toevoeging" => ""
};

sub search {
    my $self = shift;
    push @{ $self->{calls} }, \@_;

    return [map { $bag_entry } (1 .. $self->number_of_results)];
}

sub get_exact {
    my $self = shift;
    push @{ $self->{calls} }, \@_;

    return [map { $bag_entry } (1 .. $self->number_of_results)];
}

sub get {
    my $self = shift;
    push @{ $self->{calls} }, \@_;
    return $bag_entry;
}

sub find_nearest {
    my $self = shift;
    push @{ $self->{calls} }, \@_;
    return [$bag_entry];
}

__PACKAGE__->meta->make_immutable;

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
