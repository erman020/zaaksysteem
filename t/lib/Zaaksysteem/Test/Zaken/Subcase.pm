package Zaaksysteem::Test::Zaken::Subcase;

use Moose;
extends "Zaaksysteem::Test::Moose";

=head1 NAME

Zaaksysteem::Test::Zaken::Subcase - Test Zaaksysteem::Zaken::Subcase

=head1 DESCRIPTION

Test the subcase model

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Zaken::Subcase

=cut

use DateTime;
use Zaaksysteem::Test;
use BTTW::Tools::RandomData qw(:uuid);
use Data::Random::NL qw(:all);

use Zaaksysteem::Zaken::Subcase;

sub test_pod {
    pod_coverage_ok('Zaaksysteem::Zaken::Subcase');
}

sub _get_model {

    my $model = Zaaksysteem::Zaken::Subcase->new(
        casetype_rs          => mock_one(),
        casetype_relation_rs => mock_one(),
        context              => mock_one(),
        @_,
    );
    isa_ok($model, "Zaaksysteem::Zaken::Subcase");
    return $model;
}

sub test_get_subcase_from_case {

    my $args;
    my $model = _get_model(
        casetype_relation_rs => mock_one(
            search_rs => sub {
                $args = shift;
            }
        ),
    );

    my $case = mock_one(
        get_column => 999,
        milestone  => 2,
        search_rs  => sub {
            return mock_one(as_query => \[42, 666]);
        }
    );

    $model->get_subcase_from_case($case);
    cmp_deeply(
        $args,
        {
            show_in_pip         => 1,
            zaaktype_node_id    => 999,
            zaaktype_status_id  => { in => [42, 666] },
        },
        "Get subcase_from_case passes correct arguments to search_rs on casetype_relation_rs"
    );

    $model->get_subcase_from_case($case, 297);
    cmp_deeply(
        $args,
        {
            show_in_pip         => 1,
            zaaktype_node_id    => 999,
            zaaktype_status_id  => { in => [42, 666] },
            relatie_zaaktype_id => 297,
        },
        "Get subcase_from_case passes correct arguments to search_rs on casetype_relation_rs - including casetype id"
    );
}

sub _subcase_db_component {
    return mock_strict(
        get_column => sub {
            my $n = shift;
            if ($n eq 'relatie_zaaktype_id') {
                return 42;
            }
            else {
                die "Incorrect column name $n", $/;
            }
        },
        pip_label => "Here be labeling",
        @_,
    );
}

sub test_get_pip_subcases {

    my $model = _get_model();

    my @rs = ('my subcase');
    my $rs = mock_one(next => sub {shift @rs});

    my $override = override(
        'Zaaksysteem::Zaken::Subcase::get_subcase_from_case' =>
            sub { return $rs }
    );
    $override->override(
        'Zaaksysteem::Zaken::Subcase::_subcase_for_pip' =>
            sub { shift; my $args = {@_}; return $args }
    );

    my $betrokkene = mock_one(btype => 'natuurlijk_persoon');
    my $case = mock_case();

    my $rv = $model->get_pip_subcases(
        case       => $case,
        betrokkene => $betrokkene,
    );

    is(@$rv, 1, "Got one result from get_pip_subcases");

    cmp_deeply(
        $rv->[0],
        {
            btype   => 'persoon',
            uri     => '/form/subcase/%d/%d/persoon/%s',
            subcase => 'my subcase',
            case_id => $case->id,
        },
        'Pass through is correct'
    );


    @rs = ('my other subcase');
    $rv = $model->get_pip_subcases(
        case       => $case,
        betrokkene => mock_one(btype => 'bedrijf'),
    );

    cmp_deeply(
        $rv->[0],
        {
            btype   => 'organisatie',
            uri     => '/form/subcase/%d/%d/organisatie/%s',
            subcase => 'my other subcase',
            case_id => $case->id,
        },
        'Pass through is correct for company'
    );


    $rv = $model->get_pip_subcases(
        case       => $case,
        betrokkene => $betrokkene
    );
    is($rv, undef, "No subcases found");
}

sub test_subcase_for_pip {
    my $model = _get_model(
        context => mock_one(uri_for => sub { return shift }),
    );

    my $subcase_component = _subcase_db_component();

    my $override = override(
        'Zaaksysteem::Zaken::Subcase::assert_casetype' => sub {
            return mock_one(seo_friendly_title => "My SEO friendly title");
        }
    );

    my $rv = $model->_subcase_for_pip(
        subcase => $subcase_component,
        case_id => 1,
        btype   => 'foo',
        uri     => 'https://testsuite.zs.nl/%d/%d/%s',
    );

    cmp_deeply(
        $rv,
        {
            uri   => 'https://testsuite.zs.nl/1/42/My SEO friendly title',
            label => "Here be labeling",
        },
        "_subcase_for_pip returns correct datastructure",
    );

    $override = $override->replace(
        'Zaaksysteem::Zaken::Subcase::assert_casetype' => sub {
            die "Foo";
        }
    );

    $rv = $model->_subcase_for_pip(
        subcase => $subcase_component,
        case_id => 300,
        btype   => 'foo',
        uri     => 'https://testsuite.zs.nl/%d/%d/%s',
    );
    is($rv, undef, "No casetype for given betrokkene type is no pip label");
}

sub test_get_casetype {

    my @args;
    my $model = _get_model(
        casetype_rs => mock_one(
            search => sub { @args = @_; return mock_one }
        )
    );
    $model->_get_casetype(42);
    cmp_deeply(
        \@args,
        [
            {
                'me.id'                    => 42,
                'me.deleted'               => undef,
                'zaaktype_node_id.trigger' => ['extern', 'internextern'],
                'zaaktype_node_id.webform_toegang' => 1,
                'me.active'                        => 1,
            },
            { 'prefetch' => 'zaaktype_node_id' },
        ],
        "Search params for _get_casetype are correct");

    throws_ok(
        sub {
            my $model = _get_model(
                casetype_rs => mock_one(search => sub { return mock_one(first => undef) }),
            );
            $model->_get_casetype(42);
        },
        qr#zaken/subcase/casetype/invalid#,
        "Did not get a search result for extern/internextern casetype search"
    );
}

sub test_assert_casetype {

    my @args;
    my $model = _get_model();

    my $casetype = mock_strict(
        zaaktype_node_id => {
            zaaktype_betrokkenen => {
                search => sub { @args = @_; return mock_one },
            },
        }
    );

    my $override = override(
        'Zaaksysteem::Zaken::Subcase::_get_casetype' => sub {
            return $casetype;
        }
    );

    $model->assert_casetype(42, 'persoon');

    cmp_deeply(
        \@args,
        [
            {
                betrokkene_type => 'natuurlijk_persoon'
            }
        ],
        "Search params for casetype betrokkene are correct"
    );

    $casetype = mock_strict(
        zaaktype_node_id => {
            zaaktype_betrokkenen => {
                search => sub { @args = @_; return mock_one(first => undef) },
            },
        }
    );

    throws_ok(
        sub {
            $model->assert_casetype(42, 'persoon');
        },
        qr#zaken/subcase/casetype/betrokkene/invalid#,
        "Did not get the casetype for the required betrokkene type",
    );
}

sub test_duplicate_case_data {
    my $model = _get_model();
    my $case  = mock_case(field_values => \{ 123 => [123], 456 => [456,789] });
    my $relation = mock_one(
        get_column => sub {
            my $n = shift;
            if ($n eq 'relatie_zaaktype_id') {
                return 42;
            }
            elsif ($n eq 'relatie_type') {
                return "deelzaak",;
            }
            else {
                die "Invalid column name $n", $/,;
            }
        }
    );

    my $data = $model->duplicate_case_data(
        case            => $case,
        relation        => $relation,
        betrokkene_id   => 'aanvrager_id',
        betrokkene_type => 'aanvrager_type',
    );

    cmp_deeply(
        $data,
        {
            zaaktype_id        => 42,
            ztc_aanvrager_id   => 'aanvrager_id',
            ztc_aanvrager_type => 'aanvrager_type',
            form               => {
                kenmerken => {
                    123 => 123,
                    456 => [456, 789],
                },
            },
            raw_kenmerken => {
                123 => 123,
                456 => [456, 789],
            },
            milestone => 1,
            status          => 'new',
            contactkanaal   => 'webformulier',
            aangevraagd_via => 'webformulier',

            type_zaak       => 'deelzaak',
            zaak_relatie_id => $case->id,

        },
        "Duplicate case data for zaak create via /form"
    );
}

sub test_relate_subcase {

    my $model = _get_model();
    my $args;

    my $parent = mock_case(create => sub { $args = shift });
    my $child = mock_case;

    $model->relate_subcase(
        parent => $parent,
        child  => $child,
        type   => 'deelzaak',
    );

    cmp_deeply($args, { relation_zaak_id => $child->id }, "Deelzaak related");

    $model->relate_subcase(
        parent                 => $parent,
        child                  => $child,
        type                   => 'deelzaak',
        required_in_phase      => 2,
        parent_advance_results => 'Yep',
    );

    cmp_deeply(
        $args,
        {
            relation_zaak_id       => $child->id,
            required               => 2,
            parent_advance_results => 'Yep'
        },
        "Deelzaak related with required and parent_advance_result",
    );

    lives_ok(
        sub {
            my $parent = mock_case(create => sub { die "Deelzaak only" });
            $model->relate_subcase(
                parent                => $parent,
                child                 => $child,
                type                  => 'vervolgzaak',
                required_in_phase     => 2,
                parent_advance_result => 'Yep',
            );
        },
        "Only deelzaken have a relate_subcase action"
    );

}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
