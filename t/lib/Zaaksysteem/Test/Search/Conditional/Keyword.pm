package Zaaksysteem::Test::Search::Conditional::Keyword;

use Zaaksysteem::Test;

use Zaaksysteem::Search::Conditional::Keyword;
use Zaaksysteem::Mock::Backend::Object::Data::ResultSet;

sub test_search_conditional_keyword {
    my $test = shift;

    my $rs = Zaaksysteem::Mock::Backend::Object::Data::ResultSet->new;
    my $cond = Zaaksysteem::Search::Conditional::Keyword->new(value => 'a');

    is_deeply $cond->evaluate($rs), \[
        'tsvector_column @@ ?', [
            {} => 'a:*'
         ]
    ], 'simple keyword query sql';

    my $complex = Zaaksysteem::Search::Conditional::Keyword->new(
        value => 'foo __keyword__ bar & love'
    );

    is_deeply $complex->evaluate($rs), \[
        'tsvector_column @@ ?', [
            {} => 'foo:* & __keyword__:* & bar:* & love:*'
        ]
    ], 'complex keyword query sql';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
