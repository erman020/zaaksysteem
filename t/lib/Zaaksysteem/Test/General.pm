package Zaaksysteem::Test::General;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Test::Mock::Two qw(one_called_times_ok one_called_ok);
use Zaaksysteem::Test::Mocks;
use BTTW::Tools::RandomData qw(:uuid);

sub mock_catalyst {
    my ($opts, $args) = @_;

    my @general = qw(
        Zaaksysteem::General::Actions
        Zaaksysteem::General::Actions::API::v1
        Zaaksysteem::General::Actions::Dispatch
    );

    my $c = mock_moose_class({ superclasses => \@general },);

    $c->meta->add_attribute(
        config => (
            is      => 'rw',
            isa     => 'HashRef',
            default => sub {
                {
                    root       => 'myroot',
                    'View::TT' => { locale => 'nl_NL', }
                };
            },
            lazy => 1
        ),
    );
    $c->meta->add_attribute(
        res => (
            is      => 'rw',
            isa     => 'Defined',
            default => sub { mock_one() },
            lazy    => 1
        ),
    );
    $c->meta->add_attribute(
        req => (
            is      => 'rw',
            isa     => 'Defined',
            default => sub { mock_one() },
            lazy    => 1
        ),
    );

    $c->meta->add_attribute(
        log => (
            is      => 'rw',
            isa     => 'Defined',
            default => sub { Log::Log4perl->get_logger('Zaaksysteem') },
            lazy    => 1
        ),
    );

    $c->meta->add_attribute(
        model => (
            is      => 'rw',
            isa     => 'Defined',
        )
    );

    $c->meta->add_attribute(
        _additional_static => (
            is  => 'rw',
            isa => 'Defined',
        )
    );

    return $c;
}

sub test_has_client_type {

    my $c = mock_catalyst();

    my $header_set = 'X-Client-Type';
    $c->req(
        mock_one(
            header => sub {
                my $args = shift;
                return $args eq $header_set ? 1 : 0;
            },
        )
    );

    is($c->has_client_type(), 1, "Client type is set in header");

    $header_set = 'X-Client-Type-Not';
    is($c->has_client_type(), 0, "Client type is not set in header");
}

sub test_parse_es_query_params {

    my $c = mock_catalyst();
    $c->req(mock_one(params => sub { { 'query:match:foo' => 'bar' } },));

    cmp_deeply(
        $c->parse_es_query_params,
        { query => { match => { foo => 'bar' } } },
        "ES query processed"
    );

    $c->req(mock_one(params => sub { { 'match:foo' => 'bar' } },));

    throws_ok(
        sub {
            $c->parse_es_query_params,;
        },
        qr#^general/es_query_fault#,
        "No ES query params found in request",
    );

}

sub test_get_base_rs {

    my $c = mock_catalyst;

    is($c->get_base_rs(model => mock_one(rs => 'foo')),
        'foo', "Got base rs 'foo'");

    is(
        $c->get_base_rs(
            model     => mock_one(rs    => 'foo'),
            interface => mock_one(jpath => undef),
        ),
        'foo',
        "Got base rs 'foo', interface has no query constraint"
    );

    is(
        $c->get_base_rs(
            model => mock_one(
                inflate_from_row => {
                    zql => { apply_to_resultset => 'bar' }
                }
            ),
            interface => mock_one(
                jpath => sub {
                    return { id => 1 };
                },
            ),
            ),
        'bar',
        "Found query constraint 'bar'"
    );

    throws_ok(
        sub {
            $c->get_base_rs(
                model => mock_one(
                    rs               => 'foo',
                    inflate_from_row => undef
                ),
                interface => mock_one(
                    jpath => sub {
                        return { id => 1 };
                    },
                ),
                ),
                ;
        },
        qr#general/search/query_constraint_not_found#,
        "No query constraint found while defined",
    );
}

sub test_parse_search_query {
    my $c = mock_catalyst;

    my $base_rs = mock_one(
        'X-Mock-Called' => 1,
        search_rs       => mock_one(
            'X-Mock-Stringify' => 'base_resultset',

        ),
    );
    my $override = override(
        'Zaaksysteem::General::Actions::API::v1::get_base_rs' => sub {
            return $base_rs;
        }
    );

    my $params = {};
    $c->req(mock_one(params => \$params));

    my $iterator = $c->parse_search_query(
        model       => mock_one,
        object_type => 'foo',
    );

    isa_ok($iterator, "Zaaksysteem::Object::Iterator");

    my $rv = one_called_times_ok($base_rs, 'search_rs',
        'Zaaksysteem::General::Actions::API::v1::parse_search_query', 1);

    cmp_deeply(
        $rv->[0],
        [ { object_class => 'foo' } ],
        "search_rs called with object_class => 'foo'"
    );

    is($iterator->rs . '', 'base_resultset', "Got base rs 'base_resultset'");

    $params = { es_query => 1, 'query:match:_all' => 'foo' };

    $c->parse_search_query(
        model       => mock_one,
        object_type => 'case',
    );

    $rv = one_called_times_ok($base_rs, 'search',
        'Zaaksysteem::Search::ESQuery::apply_to_resultset', 1);

    cmp_deeply(
        $rv->[0],
        [ { object_class => 'case' } ],
        "search_rs called with object_class => 'case' from ESQuery module"
    );

    my $api_object_controller = mock_one(
        'X-Mock-Called'    => 1,
        'X-Mock-Stringify' => "RS from object controller via _search_intake"
    );
    $c->meta->add_method( 'controller' => sub { $api_object_controller } );

    $params = { zql => 'SELECT {} FROM case' };
    $c->parse_search_query(
        model       => mock_one,
        object_type => 'case',
    );


    $rv = one_called_ok($api_object_controller, '_search_intake',
        "Zaaksysteem::General::Actions::API::v1::parse_search_query");

    is(@{$rv->[0]}, 3, "three arguments passed");

    throws_ok(
        sub {
            $params = { zql => 'SELECT {} FROM foo' };
            $c->parse_search_query(
                model       => mock_one,
                object_type => 'case',
            );
        },
        qr#general/search/zql/query_fault#,
        "Search query does not match object_type"
    );
}

sub test_iterator_to_api_v1_response {

    my $c = mock_catalyst;

    my $iterator = mock_one;
    lives_ok(
        sub {
            $c->iterator_to_api_v1_response($iterator);
        },
        'Iterator to API v1 response works',
    );

}

sub test_customer_instance {

    my $c = mock_catalyst;
    my $request = mock_one(uri => { host => 'testsuite.zaaksysteem.nl' });
    $c->req($request);

    my $customer = {
        customer_id       => 'template',
        instance_uuid     => 'notauuid',
        instance_hostname => 'testsuite.zaaksysteem.nl',
        logging_id        => 'foo',
    };

    my $override = override("Zaaksysteem::General::Actions::load_customer_d_configs" => sub { return 1 });
    $override->override(
        "Zaaksysteem::General::Actions::_get_customer" => sub {
            my $self     = shift;
            my $hostname = shift;
            $self->config->{hostname} = $hostname;
            return $customer;
        }
    );

    my $copy = $c->customer_instance;

    cmp_deeply($copy, $customer, "Customer and copy are the same");

    cmp_deeply(
        $c->config,
        {
            'View::TT'          => { locale => 'nl_NL' },
            filestore_base_path => '/var/tmp/zs/storage',
            gemeente_id         => 'template',
            instance_hostname   => 'testsuite.zaaksysteem.nl',
            instance_uuid       => 'notauuid',
            logging_id          => 'foo',
            root                => 'myroot',
            hostname            => 'testsuite.zaaksysteem.nl',
            services_base => 'https://unconfigured.services.zaaksysteem.nl',
            static        => { include_path => [qw(myroot myroot/tpl/zaak_v1/nl_NL)] },
        },
        "c->config has correct values"
    );

    $c = mock_catalyst; # reset $c->config;
    $c->req($request);  # and set the uri correct

    # Set the virtualhost
    $customer->{VirtualHosts}{'testsuite.zaaksysteem.nl'}{customer_id} = 'overrides';

    $c->customer_instance;

    cmp_deeply(
        $c->config,
        {
            'View::TT'          => { locale => 'nl_NL' },
            filestore_base_path => '/var/tmp/zs/storage',
            gemeente_id         => 'overrides',
            instance_hostname   => 'testsuite.zaaksysteem.nl',
            instance_uuid       => 'notauuid',
            logging_id          => 'foo',
            root                => 'myroot',
            hostname            => 'testsuite.zaaksysteem.nl',
            services_base => 'https://unconfigured.services.zaaksysteem.nl',
            static        => { include_path => [qw(myroot myroot/tpl/zaak_v1/nl_NL)] },
        },
        "c->config has correct values"
    );
}

sub test_dispatch_query_statistics {

    my $c = mock_catalyst;
    use Log::Log4perl::MDC;

    my $schema = mock_strict(
        storage => {
            query_list => \[ ],
        },
    );

    $c->dispatch_query_statistics($schema);
    is(Log::Log4perl::MDC->get('sql_query_count'), 0, "Zero queries to be found");

    $schema = mock_strict(
        'X-Mock-CalledBy' => 1,
        storage => {
            query_list => \[ { took => 0.001001 }, { took => 0.001 } ],
            connect_info => \[ { dsn => 'dbi:Pg:dbname=testsuitedb;host=testsuitehost' } ],
        },
    );

    my %statsd = ();
    my $statsd = mock_strict(
        increment => sub {
            my ($key, $val) = @_;
            $statsd{$key} //= 0;
            $statsd{$key} += $val;
        },
        timing => sub {
            my ($key, $val) = @_;
            $statsd{$key} = $val;
        }
    );

    $c->meta->add_method('statsd' => sub { return $statsd });

    $c->dispatch_query_statistics($schema);

    is(Log::Log4perl::MDC->get('sql_query_time'), 2, "Two query took 2 ms");
    is(Log::Log4perl::MDC->get('sql_query_time_avg'), 1, "Avg query time is 1000 ms");

    my %want = (
        'database.num_queries'                         => 2,
        'database.query_time'                          => 2,
        'database.query_time_avg'                      => 1,

        'database.query_time.testsuitedb.testsuitehost'     => 2,
        'database.num_queries.testsuitedb.testsuitehost'    => 2,
        'database.query_time_avg.testsuitedb.testsuitehost' => 1,
    );
    cmp_deeply(\%statsd, \%want, "StatsD information is valid too");

    my $reset = 0;
    $schema = mock_strict(
        storage => { reset_query_counters => sub { $reset++ } },
    );

    $c->dispatch_query_statistics($schema,1);

    is($reset, 1, "We called reset_query_counters");
}

sub test_assert_uuid {
    my $c    = mock_catalyst;
    my $uuid = generate_uuid_v4;
    lives_ok(sub { $c->assert_uuid($uuid) }, "$uuid is a valid UUID");
    $uuid = 'not-a-uuid';
    throws_ok(sub { $c->assert_uuid($uuid) },
        qr#general/uuid/invalid#, "... and $uuid isn't");
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::General - Test everything related to ZS::General

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::General

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
