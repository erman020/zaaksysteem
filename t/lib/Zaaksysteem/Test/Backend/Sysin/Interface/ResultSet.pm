package Zaaksysteem::Test::Backend::Sysin::Interface::ResultSet;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;

use Zaaksysteem::Backend::Sysin::Interface::ResultSet;

sub _get_fake_self {
    return mock_one(
        schema => mock_dbix_schema(),
        @_,
    );
}

sub test_active_params {

    my $mt = 0;
    my $schema = mock_dbix_schema(is_multi_tenant => sub { return $mt });

    my $fake_self = _get_fake_self(schema => $schema);

    my $args
    = Zaaksysteem::Backend::Sysin::Interface::ResultSet::_build_active_params(
    $fake_self);

    cmp_deeply(
        $args,
        { active => 1, date_deleted => undef },
        "No multi-tenant args found"
    );

    # set multitenant on
    $mt = 1;
    $args
        = Zaaksysteem::Backend::Sysin::Interface::ResultSet::_build_active_params(
        $fake_self, 1);

    cmp_deeply(
        $args,
        {

            active       => 1,
            date_deleted => undef,
            -and         => [
                -or => [
                    {
                        -and => [
                            {
                                'interface_config::jsonb' => {
                                    '@>' => \
                                        "'{\"multi_tenant_host\":\"testsuite.zs.nl\"}'::jsonb"
                                },
                            },
                            {
                                'interface_config::jsonb' => {
                                    '@>' => \
                                        "'{\"is_multi_tenant\":1}\'::jsonb"
                                },
                            }
                        ],
                    },
                    {
                        'interface_config::jsonb' =>
                            { '@>' => \"'{\"is_multi_tenant\":0}'::jsonb" },
                    },
                    [
                        \"coalesce(interface_config::jsonb->>'is_multi_tenant', '0') = '0'"
                    ],
                ],

            ],

        },
        "Multi-tenant search options found!"
    );

    $args
        = Zaaksysteem::Backend::Sysin::Interface::ResultSet::_build_active_params(
        $fake_self, 0);

    cmp_deeply($args, { active => 1, date_deleted => undef }, "Unrestricted search made possible by..");
}

sub test_find_module_by_name {

    my @results   = ();
    my $fake_self = _get_fake_self(
        search_active => {
            all => sub {
                return @results;
            }
        },
    );

    my $found
        = Zaaksysteem::Backend::Sysin::Interface::ResultSet::_find_by_module_name(
        $fake_self, "foo");
    is($found, undef, "Found nothing");

    push(@results, mock_one('X-Mock-ISA' => 'foo'));

    $found
        = Zaaksysteem::Backend::Sysin::Interface::ResultSet::_find_by_module_name(
        $fake_self, "foo");
    isa_ok($found, 'foo', "Found one module");

    @results = (
        mock_one(
            'X-Mock-ISA'                  => 'baz',
            allow_multiple_configurations => 0,
        ),
        mock_one('X-Mock-ISA' => 'bar')
    );

    throws_ok(
        sub {
            Zaaksysteem::Backend::Sysin::Interface::ResultSet::_find_by_module_name(
                $fake_self, "foo");

        },
        qr#sysin/interfaces/find_by_module_name/multiple_found#,
        "Found too many interfaces"
    );
}

sub test_find_module_by_name_mt {

    my @results = (
        mock_one(
            'X-Mock-ISA'         => 'foo',
            'X-Mock-SelfArg'     => 1,
            supports_hostname    => 1,
            allow_multiple_configurations => 0,
        ),
        mock_one(
            'X-Mock-ISA'         => 'baz',
            supports_hostname    => 0,
            allow_multiple_configurations => 0,
        ),
        mock_one('X-Mock-ISA' => 'bar', supports_hostname => 0)
    );

    my $fake_self = _get_fake_self(
        schema => mock_dbix_schema(is_multi_tenant => 1),
        search_active => { all => sub { return @results } },
    );

    my $found
        = Zaaksysteem::Backend::Sysin::Interface::ResultSet::_find_by_module_name(
        $fake_self, "foo");
    isa_ok($found, 'foo', "Found one module");


    @results = (
        mock_one(
            'X-Mock-ISA'         => 'foo',
            supports_hostname    => sub {
                my ($hostname, $val) = @_;
                if ($val) {
                    return 1;
                }
                return 1;
            },
            allow_multiple_configurations => 0,
        ),
        mock_one(
            'X-Mock-ISA'         => 'baz',
            supports_hostname    => sub {
                my ($hostname, $val) = @_;
                if ($val) {
                    return 0;
                }
                return 1;
            },
            allow_multiple_configurations => 0,
        ),
        mock_one('X-Mock-ISA' => 'bar', supports_hostname => 0)
    );

    $found = Zaaksysteem::Backend::Sysin::Interface::ResultSet::_find_by_module_name(
        $fake_self, "foo");
    isa_ok($found, "foo", "Found most specific module");


    @results = (
        mock_one(
            'X-Mock-ISA'         => 'foo',
            allow_multiple_configurations => 0,
        ),
        mock_one(
            'X-Mock-ISA'         => 'foo',
        ),
    );

    throws_ok(
        sub {
            Zaaksysteem::Backend::Sysin::Interface::ResultSet::_find_by_module_name(
                $fake_self, "foo");

        },
        qr#sysin/interfaces/find_by_module_name/multiple_found#,
        "Found too many interfaces"
    );

    @results = (
        mock_one(
            'X-Mock-ISA'         => 'foo',
            allow_multiple_configurations => 1,
        ),
        mock_one(
            'X-Mock-ISA'         => 'foo',
        ),
    );

    my @found
        = Zaaksysteem::Backend::Sysin::Interface::ResultSet::_find_by_module_name(
        $fake_self, "foo");
    is(@found, 2, "Found multiple interfaces");
}

sub test_jpath_search {

    my @results = (
        mock_one(
            'X-Mock-ISA'      => 'foo',
            'X-Mock-SelfArg'  => 1,
            supports_hostname => 1,
            jpath             => sub {
                my $self = shift;
                return 1 if $self->isa(shift);
                return 0;
            },
            allow_multiple_configurations => 0,
        ),
        mock_one(
            'X-Mock-ISA'         => 'baz',
            'X-Mock-SelfArg'     => 1,
            supports_hostname    => 1,
            jpath                => sub {
                my $self = shift;
                return 1 if $self->isa(shift);
                return 0;
            },
            allow_multiple_configurations => 0,
        ),
        mock_one('X-Mock-ISA' => 'world', supports_hostname => 0)
    );

    my $fake_self = _get_fake_self(
        schema        => mock_dbix_schema(is_multi_tenant => 1),
        search_active => {
            all => sub { return @results }
        },
    );

    my $found = Zaaksysteem::Backend::Sysin::Interface::ResultSet::_find_by_module_name(
        $fake_self, "foo-module", 'foo'
    );
    isa_ok($found, 'foo', "Found something that returns true on jpath");

    $found = Zaaksysteem::Backend::Sysin::Interface::ResultSet::_find_by_module_name(
        $fake_self, "baz-module", 'baz'
    );
    isa_ok($found, 'baz', "Found something that returns true on jpath");

    $found = Zaaksysteem::Backend::Sysin::Interface::ResultSet::_find_by_module_name(
        $fake_self, "foo", "You've got nothing on me babe"
    );
    is($found, undef, "No JPATH found");
}

sub test_search_active_restrictive {

    my @args;
    my @active_params;
    my $fake_self = mock_strict(
        _build_active_params => sub {
            push(@active_params, shift);
            return { active => 1, date_deleted => undef };
        },
        search_rs            => sub {
            @args = @_;
            return mock_strict(search_rs => sub { return shift });
        },
    );
    my $search = Zaaksysteem::Backend::Sysin::Interface::ResultSet::search_active_restricted(
        $fake_self, { foo => 'bar'});

    cmp_deeply(\@args, [ { foo => 'bar' } ], "Search results passed to schema");
    cmp_deeply($search, { active => 1, date_deleted => undef }, "_build_active_params called");
    cmp_deeply(\@active_params, [ 1 ], "_build_active_params called with correct parameter");

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::Interface::ResultSet - An interface resultset tester

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Backend::Sysin::Interface::ResultSet

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
