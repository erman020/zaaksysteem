package Zaaksysteem::Test::Backend::Sysin::Auth::Remote;
use Moose;
use namespace::autoclean;

use HTTP::Response;
use Zaaksysteem::Backend::Sysin::Auth::Remote;
use Zaaksysteem::Constants qw(LOGGING_COMPONENT_USER);
use Zaaksysteem::Test;

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::Auth::Remote - Test remote session invitation requesting

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Backend::Sysin::Auth::Remote

=cut

sub test_request_login_url {
    my $last_post;
    my $mock_ua = mock_one(
        post => sub {
            $last_post = \@_;
            return HTTP::Response->new(
                200 => 'OK',
                undef,
                '{"login_url": "remote_login_url"}',
            );
        }
    );

    my $last_trigger;
    my $mock_logging_rs = mock_one(
        trigger => sub {
            $last_trigger = \@_;
        },
    );

    my $instance = mock_one(
        id => 123,
        fqdn => 'fqdn_here',
        customer_type => 'bloop',
    );
    my $subject = mock_one(
        id => 321,
        username => 'test_username',
    );

    my $override1 = override 'Zaaksysteem::Backend::Sysin::Auth::Remote::_build_remote_user' => sub {
        return 'remote_user_goes_here';
    };
    my $override2 = override 'Zaaksysteem::Backend::Sysin::Auth::Remote::_build_token_generation_url' => sub {
        return 'token_generation_url';
    };

    my $model = Zaaksysteem::Backend::Sysin::Auth::Remote->new(
        hostname => 'test.zaaksysteem.nl',
        platform_keys => {
            bloop => "bleep",
        },
        logging_rs => $mock_logging_rs,
        ua => $mock_ua,
        server_ca => '/dev/null',
    );

    my $url = $model->request_login_url($instance, $subject);

    is($url, 'remote_login_url', "Remote login URL correctly retrieved from HTTP response");
    
    cmp_deeply(
        $last_trigger,
        [
            'auth/login/token_request',
            {
                component => LOGGING_COMPONENT_USER,
                data => {
                    subject_id       => 321,
                    subject_username => 'test_username',
                    instance_id      => 123,
                    instance_fqdn    => 'fqdn_here',
                }
            }
        ],
        'Logging was triggered correctly'
    );

    cmp_deeply(
        $last_post,
        [
            'token_generation_url',
            'ZS-Platform-Key' => 'bleep',
            'Content' => {
                remote_user => 'remote_user_goes_here'
            }
        ],
        'Corrent HTTP request sent to retrieve authentication token',
    );
}

sub test__build_token_generation_url {
    my $instance = mock_one(
        fqdn => sub { 'testhostname' },
        services_domain => sub { 'services.testhostname' },
    );

    is(
        Zaaksysteem::Backend::Sysin::Auth::Remote::_build_token_generation_url($instance),
        'https://services.testhostname/auth/token/generate',
        'Internal function to generate auth token URLs works correctly.'
    );
}

sub test__build_remote_user {
    my $subject = mock_one(
        username => sub { 'username_here' },
        display_name => sub { 'display_name_here' },
    );

    is(
        Zaaksysteem::Backend::Sysin::Auth::Remote::_build_remote_user($subject, 'insert_hostname_here'),
        'display_name_here (username_here@insert_hostname_here)',
        'Username / hostname are combined correctly for "remote user" of target system'
    );
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
