package Zaaksysteem::Test::Backend::Object::Roles::ObjectComponent;
use Moose;
extends 'Zaaksysteem::Test::Moose';
use Zaaksysteem::Test;

sub test_csv_data {

    my $od = mock_moose_class(
        {
            roles => [
                qw(
                    Zaaksysteem::Backend::Object::Roles::ObjectComponent
                )
            ]
        }
    );
    $od->blacklisted_columns([qw(black list)]);

    is($od->_is_blacklisted( sub { $_ eq 'black' }), 'black', "Found blacklisted item");
    is($od->is_blacklisted('black'), 'black', "Found blacklisted item via public method");
    is($od->is_blacklisted('grey'), undef, "grey isn't blacklisted");


}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Object::Roles::ObjectComponent - Test objectcomponent role

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Backend::Object::Roles::ObjectComponent

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
