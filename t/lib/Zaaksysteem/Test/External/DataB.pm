package Zaaksysteem::Test::External::DataB;
use Moose;
extends 'Zaaksysteem::Test::Moose';

=head1 NAME

Zaaksysteem::Test::External::DataB - Test the plugin modules of ZS::DataB

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::External::DataB

=cut

use Zaaksysteem::Test;
use Zaaksysteem::Test::XML qw(:all);
use BTTW::Tools qw(dump_terse);
use IO::All;
use Digest::MD5 qw(md5_hex);

use Zaaksysteem::External::DataB;

sub test_setup {
    my $test = shift;

    $test->next::method;

    my $test_method = $test->test_report->current_method->name;
    if ($test_method =~ /live$/ && !grep /^DATAB/, keys %ENV) {
        $test->test_skip("Skipping $test_method: live test");
    }
}

sub test_datab {

    my $datab = Zaaksysteem::External::DataB->new(
        endpoint => 'https://localhost.example.com',
        cid      => 'some value',
        secret   => 'I see dead people',
    );

    lives_ok(
        sub {
            my $xml = io->catfile(qw(t data datab session_url.xml))->slurp;
            my $response = $datab->assert_session_xml($xml);
            is(
                $response,
                "https://answer.example.com/datab/session",
                "Got the correct response"
            );
        },
        'assert_session_xml: have data'
    );

    throws_ok(
        sub {
            my $xml
                = io->catfile(qw(t data datab session_url_bad.xml))->slurp;
            $datab->assert_session_xml($xml);
        },
        qr/MultiChannel session URL request did not succeed:/,
        'assert_session_xml: dies'
    );

    my $subject = mock_betrokkene;

    my $useragent_string = "Foo";
    my $digest           = md5_hex($useragent_string);

    my $req = $datab->build_session_request(
        subject    => $subject,
        useragent  => $useragent_string,
    );

    my $uri = $req->uri;
    is($req->method, 'GET', "GET request");

    my %query = $uri->query_form;

    cmp_deeply(
        \%query,
        {
            cid     => 'some value',
            secret  => 'I see dead people',
            clhash  => $digest,
            uid     => $subject->bsn,
            request => 'authenticate',
        },
        "Query form is correct"
    );

    use HTTP::Response;
    my $answer = io->catfile(qw(t data datab session_url.xml))->slurp;
    my $override = override("LWP::UserAgent::request" => sub {
        my $self = shift;
        return HTTP::Response->new(200, undef, undef, $answer);
    });

    lives_ok(
        sub {
            $datab->get_session_url(
                subject    => $subject,
                useragent => 'Foo',
            );
        },
        "get_session_uri"
    );
}

sub test_datab_live {

    my $datab = Zaaksysteem::External::DataB->new(
        endpoint => $ENV{DATAB_ENDPOINT},
        cid      => $ENV{DATAB_CID},
        secret   => $ENV{DATAB_SECRET},
    );

    isa_ok($datab, 'Zaaksysteem::External::DataB');

    my $betrokkene = mock_betrokkene({bsn => $ENV{DATAB_BSN}});

    my $uri = $datab->get_session_url(
        useragent  => $ENV{DATAB_USERAGENT},
        subject    => $betrokkene,
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
