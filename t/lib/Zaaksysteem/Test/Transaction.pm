package Zaaksysteem::Test::Transaction;
use Moose;
extends "Zaaksysteem::Test::Moose";

=head1 NAME

Zaaksysteem::Test::Transaction - Tests for Zaaksysteem::Transaction

=head1 DESCRIPTION

Test the L<Zaaksysteem::Transaction> model

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Transaction

=cut

use Zaaksysteem::Test;
use Zaaksysteem::Test::Mocks;
use Zaaksysteem::Transaction;

sub test_cleanup_old_transactions {
    my $deletes_seen = 0;
    my $fake_schema = mock_one(
        txn_do => sub {
            my $arg = shift;
            return $arg->();
        },
        resultset => sub {
            my $arg = shift;

            if ($arg eq 'Transaction') {
                return mock_one(
                    search => sub {
                        cmp_deeply(
                            shift,
                            {
                                'me.date_created::DATE' => { '<' => \"(CURRENT_DATE - '60 days'::INTERVAL)" }
                            },
                            'Transaction search called with proper intrval'
                        );
                        return mock_one(
                            get_column => sub {
                                my $arg = shift;
                                is($arg, 'id', 'Transaction->get_column("id") called');

                                return mock_one(
                                    as_query => 'transaction_as_query'
                                );
                            },
                            delete => sub {
                                $deletes_seen += 1;
                                pass("->delete called on transaction resultset");
                            }
                        )
                    },
                );
            }
            elsif ($arg eq 'TransactionRecord') {
                return mock_one(
                    search => sub {
                        cmp_deeply(
                            shift,
                            {
                                'transaction_id' => { -in => 'transaction_as_query' }
                            },
                            'TransactionRecord search called with transaction id list'
                        );
                        return mock_one(
                            get_column => sub {
                                my $arg = shift;
                                is($arg, 'id', 'TransactionRecord->get_column("id") called');

                                return mock_one(
                                    as_query => 'transaction_record_as_query',
                                );
                            },
                            delete => sub {
                                $deletes_seen += 1;
                                pass("->delete called on transaction_records resultset");                                
                            }
                        );
                    },
                );
            }
            elsif ($arg eq 'TransactionRecordToObject') {

                return mock_one(
                    search => sub {
                        cmp_deeply(
                            shift,
                            {
                                'transaction_record_id' => { -in => 'transaction_record_as_query' }
                            },
                            'TransactionRecordToObject search called with transaction record list'
                        );
                        return mock_one(
                            delete => sub {
                                $deletes_seen += 1;
                                pass("->delete called on transaction_record_to_object resultset");
                            }
                        );
                    },
                );
            }
            else {
                fail("Invalid resultset requested: '$arg'");
            }
        },
    );

    my $model = Zaaksysteem::Transaction->new(
        schema => $fake_schema,
        config => {},
    );
    $model->cleanup_old_transactions();
    is($deletes_seen, 3, "3 ->delete calls were seen");
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
