package Zaaksysteem::Test::Object::Types::Session::Invitation;

use Zaaksysteem::Test;

use JSON::XS ();
use MIME::Base64 qw[decode_base64];
use URI;

use Zaaksysteem::Object::Types::Session::Invitation;

sub test_session_invitation_uri {
    my $subject = mock_one(
        'X-Mock-ISA' => [qw[
            Zaaksysteem::Object::Types::Subject
            Zaaksysteem::Object::Reference::Instance
            Moose::Object
        ]],
        type => 'subject'
    );

    my $invitation = Zaaksysteem::Object::Types::Session::Invitation->new(
        subject => $subject,
        date_expires => DateTime->now,
        token => 'abc'
    );

    # v2 URIs
    {
        my $uri = $invitation->as_uri(URI->new('https://example.com/bar/baz'));

        note $uri;

        ok $uri =~ m#^zaaksysteem:\/\/(?<blob>.*)$#, 'zaaksysteem protocol uri';

        ok defined $+{blob}, 'zaaksysteem uri blob';

        my $json_str = decode_base64($+{blob});

        note $json_str;

        ok length $json_str, 'base64 decode returns content';

        my $data;

        lives_ok {
            $data = JSON::XS->new->decode($json_str);
        } 'json decode ok';

        is ref $data, 'HASH', 'blob data json string';

        is $data->{ auth_token }, 'abc', 'encoded auth token';
        is $data->{ base_uri }, 'example.com/bar/baz', 'encoded base_uri/hostname';
        is $data->{ version }, 2, 'encoded protocol uri version';

        ok(exists $data->{ $_ }, "$_: expected action exists") for qw[
            download
            upload
            lock_get
            lock_acquire
            lock_extend
            lock_release
        ];
    }

    # Old-style v1 URIs; to be removed in 2019
    {
        my $uri = $invitation->as_v1_uri(URI->new('https://example.com/bar/baz'));

        note $uri;
        ok $uri =~ m#^zaaksysteem:\/\/(?<blob>.*)$#, 'zaaksysteem protocol uri';
        ok defined $+{blob}, 'zaaksysteem uri blob';

        my $json_str = decode_base64($+{blob});

        note $json_str;
        ok length $json_str, 'base64 decode returns content';

        my $data;

        lives_ok {
            $data = JSON::XS->new->decode($json_str);
        } 'json decode ok';

        is ref $data, 'HASH', 'blob data json string';

        is $data->{ auth_token }, 'abc', 'encoded auth token';
        is $data->{ base_uri }, 'example.com', 'encoded base_uri';

        ok(exists $data->{ $_ }, "$_: expected action exists") for qw[
            download
            upload
            lock_get
            lock_acquire
            lock_extend
            lock_release
        ];

        like($data->{$_}, qr/^\Q$data->{base_uri}/, "action URLs '$_' contains the URL base") for qw[
            download
            upload
            lock_acquire
            lock_extend
            lock_get
            lock_release
        ];
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
