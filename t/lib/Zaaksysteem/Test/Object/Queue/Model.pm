package Zaaksysteem::Test::Object::Queue::Model;

use URI;

use Test::Mock::Two qw(one_called_ok);
use Zaaksysteem::Test;
use Zaaksysteem::StatsD;

use Zaaksysteem::Object::Queue::Model;

=head1 NAME

Zaaksysteem::Test::Object::Queue::Model - Test Queue model

=head1 DESCRIPTION

Test queue model

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Object::Queue::Model

=head2 test_run

=cut

sub test_run {
    my $log = {};

    ### Mock
    my $role  = mock_moose_class(
        {
            superclasses => ['Zaaksysteem::Object::Queue::Model'],
            methods => {
                test_handler => sub {
                    $log->{test_handler} = 1;
                }
            },
        },
        {
            table => mock_one(),
            statsd => mock_one(),
            base_uri => URI->new('http://localhost/'),
            instance_hostname => 'localhost',
            message_queue_factory => sub {},
            message_queue_exchange => 'amq.topic',
        }
    );

    my $status = 'pending';
    my $args;
    my $item = mock_one(
        data  => { natuurlijk_persoon_id => 44, },
        label => 'Update cases of deceased persons',
        type  => 'test_handler',
        update => sub {
            my $a = shift;
            foreach (keys %$a) {
                $args->{$_} = $a->{$_};
            }
            return mock_strict(discard_changes => 1);
        },
        status => sub { my $s = shift; $status = $s if defined $s; },
    );

    ok($role->run($item), "Item ran succesful");

    is($status, 'finished', 'Succesfully finished handler');

    cmp_deeply(
        $args,
        {
            date_started => \"statement_timestamp() at time zone 'UTC'",
            date_finished => \"statement_timestamp() at time zone 'UTC'",
            status        => 'running'
        },
        "Item update works"
    );

    ok($log->{test_handler}, 'Succesfully ran "test_handler" queue item');
}

sub test_broadcast_queued_items {
    my $log = [];

    my $fake_self = mock_one(
        'X-Mock-Called' => 1,

        queued_items => sub {
            return (
                mock_one('type' => 'foobar'),
                mock_one('type' => 'fake'),
                mock_one('type' => 'moar_queue_itemz'),

                mock_one('type' => 'run_ordered_item_set'),
                mock_one('type' => 'create_case'),
                mock_one('type' => 'touch_case'),
            );
        },
    );

    Zaaksysteem::Object::Queue::Model::broadcast_queued_items($fake_self);

    my $rv = one_called_ok(
        $fake_self,
        'broadcast_item',
        'Zaaksysteem::Object::Queue::Model::try {...} '
    );

    is(scalar @$rv, 6, "Expected number of queue-items broadcast");

    my @expected_types_order = qw(create_case run_ordered_item_set touch_case foobar fake moar_queue_itemz);

    while (my $item = shift @$rv) {
        my $expected_type = shift @expected_types_order;

        is($item->[0]->type, $expected_type, 'Found an item of the expected type, in the expected order: ' . $expected_type);
    }

    is(scalar @expected_types_order, 0, 'All expected queue items were found');
}

sub test_broadcast_item {
    my %result;

    my $fake_mq = mock_one(
        publish => sub {
            my $channel = shift;
            my $key     = shift;
            my $params  = shift;
            my $options = shift;

            $result{channel} = $channel;
            $result{key}     = $key;
            $result{params}  = $params;
            $result{options}  = $options;

            return;
        }
    );

    my $fake_self = mock_one(
        get_target_resolver => sub { return 1; },
        get_target_resolver => sub {
            my $target = shift;
            $result{target_resolver} = $target;

            return sub {
                my $item = shift;
                return sprintf("target_resolver(%s)(%d)", $target, $item->id);
            }
        },
        message_queue          => sub { return $fake_mq; },
        message_queue_channel  => sub { return 42; },
        message_queue_exchange => sub { return 'my_exchange'; },
        instance_hostname      => sub { return 'fake-instance'; },

        json => sub { return JSON::XS->new->canonical(1)->pretty(0) },
    );

    my $fake_item = mock_one(
        id     => sub { return 1337; },
        data   => sub { return { parameters => { "param1" => "one" } } },
        target => sub { return 'X' },
        type   => sub { return 'item_type' },
        stringify => sub { "stringified_item" },
        is_changed => sub { return 0 },
    );

    Zaaksysteem::Object::Queue::Model::broadcast_item($fake_self, $fake_item);

    cmp_deeply(
        \%result,
        {
            channel => 42,
            key     => 'zs.v0.item_type',
            params  => '{"instance_hostname":"fake-instance","param1":"one","url":"target_resolver(X)(1337)"}',
            options => { exchange => 'my_exchange' },
            target_resolver => 'X',
        },
        "Queue broadcast resulted in correct call to RabbitMQ instance"
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
