package Zaaksysteem::Test::Attributes;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::Attributes;

sub test_attributes_pm {

    my @things = Zaaksysteem::Attributes->predefined_case_attributes;

    my @secrets = map { $_->name }
        sort { $a->name cmp $b->name } grep { $_->is_sensitive } @things;


    my @sensitive = sort qw(
        case.requestor.a_number
        case.requestor.bsn
        case.requestor.date_of_birth
    );
    cmp_deeply(\@secrets, \@sensitive, "Found all three things");

}


__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Attributes - Test ZS::Attributes

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Attributes

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
