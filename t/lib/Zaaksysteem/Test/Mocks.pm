package Zaaksysteem::Test::Mocks;
use warnings;
use strict;

use feature 'state';

use Exporter qw[import];

use Data::Random::NL qw(generate_bsn);
use DateTime;
use Mock::Quick;
use Scalar::Util qw/blessed/;
use Sub::Override;
use Test::Mock::One;
use List::Util qw(any);
use JSON::XS;

our @EXPORT = qw(
    mock_one
    mock_strict
    override

    mock_dbix_schema
    mock_dbix_resultset
    mock_dbix_component

    mock_moose_class

    mock_case

    mock_interface
    mock_transaction
    mock_transaction_record

    mock_overheid_io_bag

    mock_natuurlijk_persoon
    mock_betrokkene
);

=head1 NAME

Zaaksysteem::Test::Mocks - Basic test mocks for Zaaksysteem-specific objects/classes

=head1 DESCRIPTION


=head1 SYNOPSIS

    my $fake = mock_case();
    $fake->id;

=head1 EXPORTED FUNCTIONS

=head2 mock_one

Returns a L<Test::Mock::One> object

=cut

sub mock_one {
    return Test::Mock::One->new(@_);
}

sub mock_strict {
    mock_one(@_, 'X-Mock-Strict' => 1);
}

=head2 override

Returns a L<Sub::Override> object

=cut

sub override {
    return Sub::Override->new(@_);
}

=head2 mock_case

Returns a mock case, that subclasses C<Zaaksysteem::Schema::Zaak> (so ISA
checks work)

=cut

sub mock_case {
    state $case_id_counter = 0;
    my %opts = @_;

    my $id = delete $opts{id};

    return mock_one(
        id => $id || ++$case_id_counter,
        %opts,
    );
}

=head2 mock_moose_class

    my $role = mock_moose_class(
        {
            roles   => ['Zaaksysteem::BR::Subject::Types::Person'],
        },
        {
            constructor_param1 => 'value1',
        }
    );

Mocks a Moose Role

=cut

sub mock_moose_class {
    my ($opts, $constructor_params)    = @_;
    $constructor_params //= {};

    return Moose::Meta::Class->create_anon_class(
        %$opts,
    )->new_object(%$constructor_params);
}

=head2 mock_dbix_schema

    my $schema = mock_dbix_schema();

Returns a mock schema.

=cut

sub mock_dbix_schema {
    return mock_one(
        storage      => { dbh_do => 1 },
        txn_do       => sub {
            $_[0]->()
        },
        encode_jsonb => sub {
            my $val = pop;
            return sprintf("'%s'::jsonb", encode_json($val));
        },
        is_multi_tenant => sub { return 0 },
        catalyst_config => sub { return {
            hostname => "testsuite.zs.nl"
        }},
        @_
    );
}

=head2 mock_dbix_resultset

=cut

sub mock_dbix_resultset {
    my $rs = shift; # Skip that
    return mock_one(@_);
}

=head2 mock_dbix_component

    my $row  = mock_dbix_component('Queue', { id => 55, label => 'blabla' });

Mocks a row, makes sure the component is loaded on it

=cut

sub mock_dbix_component {
    my ($rs, $params)   = @_;
    return mock_one(%$params);
}

sub mock_betrokkene {
    my $opts = shift // {};
    state $betrokkene_id = 0;

    my %args = @_;
    my $bsn = delete $args{bsn} // generate_bsn(9);

    return mock_one(
        id                  => ++$betrokkene_id,
        geslachtsaanduiding => 'M',
        bsn                 => $bsn,
        burgerservicenummer => $bsn,
        gm_np_id            => mock_natuurlijk_persoon(bsn => $bsn),
        %args,
    );
}


sub mock_natuurlijk_persoon {
    state $natuurlijk_persoon_id = 0;

    return mock_one(
        id => ++$natuurlijk_persoon_id,
        defined => sub { return 1 },
        geslachtsaanduiding => 'M',
        bsn                 => generate_bsn(9),
        geboortedatum       => DateTime->new(
            year => 1935,
            month => 7,
            day => 6,
            time_zone => 'Asia/Shanghai',
        ),

        # TT logic for if foo.defined('foo')
        defined => sub {
            my $a = shift;
            my @skip = qw(partner_a_nummer);
            if (any { $_ eq $a } @skip) {
                return 0;
            }
            return 1;
        },

        geslachtsnaam => 'Gyatso',
        voorvoegsel => 'Bar',
        voorletters => 'Foo',

        naamcode => 'E',
        aanduiding_naamgebruik => 'E',
        naamgebruik => 'E',

        adres_id => {
            defined => sub {
                my $a = shift;
                my @skip = qw(huisletter huisnummer_toevoeging);
                if (any { $_ eq $a } @skip) {
                    return 0;
                }
                return 1;
            },
            functie_adres => 'W',
            huisnummer => 90,
            postcode   => '1051JL',
            straatnaam => 'H.J.E Wenckebachweg',
            woonplaats => "Amsterdam",
        },

        as_object => sub {
            return 1;
        },

        _build_voorletters => sub {
            return 'T'
        },
        @_,
    );
}

sub mock_transaction {
    state $transaction_id_counter = 0;
    return mock_one(id => ++$transaction_id_counter, @_);
}

sub mock_transaction_record {
    state $transaction_record_counter = 0;
    return mock_one(id => ++$transaction_record_counter, @_);
}

sub mock_interface {
    state $interface_id = 0;

    return mock_one(
        id => ++$interface_id,
        jpath => sub { return 'foo' },
        get_mapped_attributes_from_case => sub { return {}},
        get_config_interface => sub { return {} },
        @_,
    );
}

sub mock_overheid_io_bag {

    return mock_one(
        search => sub {
            return {
                _embedded => {
                    adres => [
                        {
                            gemeentenaam       => "Amsterdam",
                            woonplaatsnaam     => "Amsterdam",
                            openbareruimtenaam => "Testsuitestraat",
                            huisnummer         => '42',
                            postcode           => '1011PZ',
                        },
                        {
                            gemeentenaam         => "Hilversum",
                            woonplaatsnaam       => "Amsterdam",
                            openbareruimtenaam   => "Testsuitestraat",
                            huisnummer           => '42',
                            huisletter           => 'A',
                            huisnummertoevoeging => "HS",
                            postcode             => '1011PZ',
                        }
                    ],
                }
            };
        }
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
