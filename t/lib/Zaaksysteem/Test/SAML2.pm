package Zaaksysteem::Test::SAML2;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;

use IO::All;
use Zaaksysteem::SAML2;
use Zaaksysteem::SAML2::Protocol::Assertion;
use Zaaksysteem::Constants qw(SAML_TYPE_ADFS);

sub test_eherkenning {

    my $saml = Zaaksysteem::SAML2->new();

    isa_ok($saml, "Zaaksysteem::SAML2");

    my %test_files = (
        '1.5'                  => '51902672000021881022',
        '1.7-vestigingsnummer' => '51902672000021881022',
        '1.7'                  => '51902672',
        #'1.9' => '123456789',
    );

    foreach my $name (keys %test_files) {

        subtest $name => sub {
            my ($xml, $assertion) = _get_xml_and_assertion(
                qw(t data eherkenning), "$name.xml"
            );

            my $kvk;

            my $ok = lives_ok(
                sub {
                    $kvk = $saml->_get_eherkenning_identifier(
                        $assertion->attributes);
                },
                "We have a valid $name xml"
            );

            if ($ok) {
                is($kvk, $test_files{$name},
                    "Assertion has correct identifier $test_files{$name}"
                );
            }
            else {
                note explain $assertion;
            }

        }
    }

}

sub test_adfs_upn {

    my $saml = Zaaksysteem::SAML2->new(
        idp => mock_one(
            interface => {
                get_interface_config => \{
                    saml_type => SAML_TYPE_ADFS,
                    use_upn   => 1,
                },
            }
        ),
    );

    isa_ok($saml, "Zaaksysteem::SAML2");

    my %test_files = (
        'saml-adfs-plain' => {
            used_profile => 'adfs',

            email     => 'gebruiker@testsuite.zaaksysteem.nl',
            givenname => "G\x{c3}\x{a9}",
            initials  => 'G.B.',
            name      => "G\x{c3}\x{a9} G.B.. Ruiker",
            nameid    => '',
            phone     => '0123123123',
            success   => 1,
            surname   => 'Ruiker',
            uid       => 'gebruiker@testsuite.dev.zaaksysteem.nl',
        },
        'saml-adfs' => {
            used_profile => 'adfs',

            email     => 'gebruiker@testsuite.zaaksysteem.nl',
            givenname => "G\x{c3}\x{a9}",
            initials  => 'G.B.',
            name      => "G\x{c3}\x{a9} G.B.. Ruiker",
            nameid    => '',
            phone     => '0123123123',
            success   => 1,
            surname   => 'Ruiker',
            uid       => 'gebruiker@testsuite.upn.zaaksysteem.nl',
        },
    );

    foreach my $name (keys %test_files) {

        subtest $name => sub {
            my ($xml, $assertion) = _get_xml_and_assertion(
                qw(t data adfs), "$name.xml"
            );

            my $identifier;
            my $ok = lives_ok(
                sub {
                    $identifier = $saml->_load_authenticated_identifier(
                        $assertion
                    );
                },
                "We have a valid $name xml"
            );

            if ($ok) {
                cmp_deeply($identifier, $test_files{$name},
                    "Assertion has correct identifier information for ADFS");
            }
            else {
                note explain $assertion;
            }
        }
    }

    subtest 'saml-adfs no upn' => sub {
        my $saml = Zaaksysteem::SAML2->new(
            idp => mock_one(
                interface => {
                    get_interface_config => \{
                        saml_type => SAML_TYPE_ADFS,
                        use_upn   => 0,
                    },
                }
            ),
        );

        isa_ok($saml, "Zaaksysteem::SAML2");

        my ($xml, $assertion) = _get_xml_and_assertion(
            qw(t data adfs), "saml-adfs.xml"
        );

        my $identifier = $saml->_load_authenticated_identifier(
            $assertion
        );

        cmp_deeply(
            $identifier,
            {
                used_profile => 'adfs',

                email     => 'gebruiker@testsuite.zaaksysteem.nl',
                givenname => "G\x{c3}\x{a9}",
                initials  => 'G.B.',
                name      => "G\x{c3}\x{a9} G.B.. Ruiker",
                nameid    => '',
                phone     => '0123123123',
                success   => 1,
                surname   => 'Ruiker',
                uid       => 'gebruiker',
                upn       => 'gebruiker@testsuite.upn.zaaksysteem.nl',
            },
            "Assertion has correct identifier information for ADFS"
        );

    }
}

sub _get_xml_and_assertion {
    my @path = @_;

    my $xml = io->catfile(@path)->slurp;
    my $assertion = Zaaksysteem::SAML2::Protocol::Assertion->new_from_xml(
        xml => $xml
    );
    isa_ok($assertion, "Zaaksysteem::SAML2::Protocol::Assertion");
    return ($xml, $assertion);
}


1;

__END__

=head1 NAME

Zaaksysteem::Test::SAML2 - Test ZS::SAML2 logic

=head1 DESCRIPTION

Test SAML2 implementations

=head1 SYNOPSIS

    prove -l :: Zaaksysteem::Test::SAML2

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
